const { log } = require("console");
const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');
const controller = {};



//**------------------------------------- view_user --------------------------------------**//
controller.membership_system = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            res.render("view_user/user", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
};

module.exports = controller;