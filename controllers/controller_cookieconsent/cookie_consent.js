const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');
const browser = require('browser-detect'); // get name browser
var ip = require('ip');
// console.log(session);
const controller = {};

controller.send_type = (req, res) => {

  const data = req.body;
  const result = browser(req.headers['user-agent']);
  var sid = req.sessionID;
  // var id_domain = data.id_file.replace("http://119.81.44.155:8081/dist/file_domain/", "")
  var id_domain = data.id_file.replace("http://127.0.0.51:8081/dist/file_domain/", "")

  var ids = id_domain.replace(".html", "")
  var idss = ids.split("_");
  var id_d = idss[idss.length - 1];
  var id_client = ip.address(); // ip_client 
  var browser_name = result.name; // browser client
  var sid = sid; // sessionid client 



  console.log("cookie consent", data);
  console.log(req);
  console.log("req.sessionID", req.sessionID);



  req.getConnection((err, conn) => {
    if (data.dataType == 0) { // กรณี user กด ปฏิเสธ
      conn.query("UPDATE  domain_setting_cookiepolicy SET approve='0'  where domain_id=?;",
        [id_d], (err, UPDATE_setting_cookiepolicy) => {

          conn.query("INSERT INTO pdpa_policylog SET policylog_cookieconsent='ปฏิเสธ',policylog_ip=?,policylog_browser=?,policylog_sessionid=?,domaingroup_id=?;",
            [id_client, browser_name, sid, id_d], (err, isnert_pdpa_policylog) => {

              conn.query("SELECT * FROM  domain_setting_cookiepolicy where domain_id=?;",
                [id_d], (err, domain_setting_cookiepolicy) => {

                  var number_notapprove = "";
                  for (var i = 0; i < domain_setting_cookiepolicy.length; i++) {
                    number_notapprove += 0;

                  }

                  conn.query("SELECT * FROM pdpa_policylog ORDER BY id_policylog DESC ;", (err, pdpa_policylog) => {
                    conn.query("SELECT * FROM pdpa_log_setting_cookiepolicy WHERE domaingroup_id=?;", [id_d], (err, pdpa_log_setting_cookiepolicy) => {
                      conn.query("SELECT * FROM domaingroup;", (err, domaingroup) => {

                        /// *** เช็คว่า id domain ตัวไหนที่ไม่มีใน pdpa_log_setting_cookiepolicy
                        if (pdpa_log_setting_cookiepolicy[0] === undefined) {
                          conn.query("INSERT INTO pdpa_log_setting_cookiepolicy SET policy_id=?,domaingroup_id=?,cookiepolicy=? ;",
                            [pdpa_policylog[0].id_policylog, id_d, number_notapprove], (err, domain_style_policy) => {
                            });

                        } else {
                          conn.query("SELECT * FROM pdpa_log_setting_cookiepolicy WHERE domaingroup_id=? ;", [id_d],
                            (err, select_setting_cookiepolicy) => {
                              conn.query("UPDATE pdpa_log_setting_cookiepolicy SET policy_id=?,cookiepolicy=? WHERE id_lsc=?;",
                                [pdpa_policylog[0].id_policylog, number_notapprove, select_setting_cookiepolicy[0].id_lsc], (err, update_pdpa_log_setting_cookiepolicy) => {
                                });
                            });
                        }
                      });
                    });
                  });
                  // console.log(domain_setting_cookiepolicy);

                  conn.query("SELECT * FROM   domain_style_policy as dsp  LEFT JOIN styles as st ON dsp.style_id=st.id_st where domaingroup_id=?;",
                    [id_d], (err, domain_style_policy) => {

                      var html_file = `<div class="modal-dialog modal-lg shadow" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                          <h4 class="modal-title">การตั้งค่าคุกกี้</h4>                      
                          </div>
                          <div class="modal-body">
                              <div class="card-body btn ${domain_style_policy[0].nameclass}" style="width: 100%;">
                              <p style="text-align: left;">
                                  ${domain_style_policy[0].detail_dialog} 
                              </p>
                              </div>
                          <br>
                          <div id="bccs-options" class="collapse">
                              <div class="bccs-option">
                              <table class="table mt-4">
                              <tbody> 
                              <hr>`;

                      for (var i = 0; i < domain_setting_cookiepolicy.length; i++) {
                        if (i + 1 != domain_setting_cookiepolicy.length) {
                          html_file += `
                      <tr>
                      <td>
                        <p class="text_color_p">
                        ${domain_setting_cookiepolicy[i].name_cookietype}
                        </p>
                      </td>
                      <td style="width: 76%">
                        <p class="text_color_p">
                        ${domain_setting_cookiepolicy[i].detail_cookie}
                        </p>
                      </td>
                      <td style="width: 11%;">
                        <div class="active_buttom">
                          <div class="form-check form-switch check_buttom">
                            <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${i + 1}" name="cookie"
                              value="${domain_setting_cookiepolicy[i].id_dsc},${domain_setting_cookiepolicy[i].approve}">
                          </div>
                        </div>
                        <div class="mt-2 text-center">
                          <label id="label_approve_${i + 1}">Active</label>
                        </div>
                      </td>
                    </tr>`;
                        } else {
                          html_file += `
                      <tr class="tr_none">
                      <td>
                        <p class="text_color_p">
                        ${domain_setting_cookiepolicy[i].name_cookietype}
                        </p>
                      </td>
                      <td style="width: 76%">
                        <p class="text_color_p">
                        ${domain_setting_cookiepolicy[i].detail_cookie}
                        </p>
                      </td>
                      <td style="width: 11%;">
                        <div class="active_buttom">
                          <div class="form-check form-switch check_buttom">
                            <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${i + 1}" name="cookie"
                              value="${domain_setting_cookiepolicy[i].id_dsc},${domain_setting_cookiepolicy[i].approve}">
                          </div>
                        </div>
                        <div class="mt-2 text-center">
                          <label id="label_approve_${i + 1}">Active</label>
                        </div>
                      </td>
                    </tr>`;
                        }
                      }
                      html_file += `
                                    </tbody>
                                    </table>
                                    </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                  <a href="#bccs-options" data-bs-toggle="collapse">เลือกประเภทคุกกี้</a>
                                  <button id="bccs-buttonDoNotAgree" type="button" class="btn btn-danger text-decoration-none">
                                    ปฏิเสธ
                                  </button>
                                  <button id="bccs-buttonAgree" type="button" class="btn btn-success">อนุญาต</button>
                                  <button id="bccs-buttonSave" type="button" class="btn btn-primary">
                                    อนุญาตตามที่เลือก
                                  </button>
                                  <button id="bccs-buttonAgreeAll" type="button" class="btn btn-success">อนุญาตทั้งหมด</button>
                                </div>
                                </div>
                              </div>
                              <style>
                                    .text_color_p {
                                        color: #6d7175;
                                    }
                                    .active_buttom {
                                        align-self: center;
                                        display: flex;
                                        justify-content: center;
                                    }
                                    .check_buttom {
                                        font-size: x-large;
                                    }
                                    .tr_none td {
                                        border: none;
                                    }
                                    table tr {
                                        height: 6rem;
                                    }
                                    </style>`;

                      fs.writeFile('./dist/file_domain/' + id_domain, html_file, function (err, file) {
                        if (err) throw err;
                      });
                    });
                });
            });
        });
    } else { // กรณี user กด อนุญาตตามที่เลือก หรือ  อนุญาตทั้งหมด
      var dataType = [];
      for (var i = 0; i < data.dataType.length; i++) {
        dataType.push(data.dataType[i].split(",")[0]);
      }
      // console.log(dataType);
      conn.query("SELECT * FROM cookiepolicy", (err, select_cookie) => {
        conn.query("UPDATE  domain_setting_cookiepolicy SET approve='0'  where domain_id=?;",
          [id_d], (err, domain_setting_cookiepolicy) => {
            // insert browsername,sessionid,id_client
            var consent;
            if (data.dataType.length != select_cookie.length) {
              consent = "อนุญาตตามที่เลือก";
            } else {
              consent = "อนุญาตทั้งหมด";
            }
            conn.query("INSERT INTO pdpa_policylog SET policylog_cookieconsent=?,policylog_ip=?,policylog_browser=?,policylog_sessionid=?,domaingroup_id=?;",
              [consent, id_client, browser_name, sid, id_d], (err, isnert_pdpa_policylog) => {

                for (let i = 0; i < data.dataType.length; i++) {
                  // update approve ประเภทคุกกี้
                  conn.query("UPDATE  domain_setting_cookiepolicy SET approve='1'  where id_dsc=?;",
                    [dataType[i]], (err, domain_setting_cookiepolicy) => {

                    });
                }

                conn.query("SELECT * FROM  domain_setting_cookiepolicy where domain_id=?;",
                  [id_d], (err, domain_setting_cookiepolicy) => {
                    var number_approve = "";
                    for (let i = 0; i < domain_setting_cookiepolicy.length; i++) {
                      number_approve += domain_setting_cookiepolicy[i].approve;

                    }
                    // console.log(number_approve);
                    conn.query("SELECT * FROM pdpa_policylog ORDER BY id_policylog DESC ;", (err, pdpa_policylog) => {
                      conn.query("SELECT * FROM pdpa_log_setting_cookiepolicy WHERE domaingroup_id=?;", [id_d], (err, pdpa_log_setting_cookiepolicy) => {
                        conn.query("SELECT * FROM domaingroup;", (err, domaingroup) => {
                          // console.log(pdpa_log_setting_cookiepolicy[0]);

                          /// *** เช็คว่า id domain ตัวไหนที่ไม่มีใน pdpa_log_setting_cookiepolicy
                          if (pdpa_log_setting_cookiepolicy[0] === undefined) {
                            conn.query("INSERT INTO pdpa_log_setting_cookiepolicy SET policy_id=?,domaingroup_id=?,cookiepolicy=? ;",
                              [pdpa_policylog[0].id_policylog, id_d, number_approve], (err, domain_style_policy) => {
                                // console.log(err);
                              });

                          } else {
                            conn.query("SELECT * FROM pdpa_log_setting_cookiepolicy WHERE domaingroup_id=? ;", [id_d],
                              (err, select_setting_cookiepolicy) => {
                                conn.query("UPDATE pdpa_log_setting_cookiepolicy SET policy_id=?,cookiepolicy=? WHERE id_lsc=?;",
                                  [pdpa_policylog[0].id_policylog, number_approve, select_setting_cookiepolicy[0].id_lsc], (err, update_pdpa_log_setting_cookiepolicy) => {
                                    // console.log(err);
                                  });
                              });
                          }
                        });
                      });
                    });

                    conn.query("SELECT * FROM   domain_style_policy as dsp  LEFT JOIN styles as st ON dsp.style_id=st.id_st where domaingroup_id=?;",
                      [id_d], (err, domain_style_policy) => {

                        var html_file = `<div class="modal-dialog modal-lg shadow" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h4 class="modal-title">การตั้งค่าคุกกี้</h4>                      
                            </div>
                            <div class="modal-body">
                                <div class="card-body btn ${domain_style_policy[0].nameclass}" style="width: 100%;">
                                <p style="text-align: left;">
                                    ${domain_style_policy[0].detail_dialog} 
                                </p>
                                </div>
                            <br>
                            <div id="bccs-options" class="collapse">
                                <div class="bccs-option">
                                <table class="table mt-4">
                                <tbody> 
                                <hr>`;

                        for (var i = 0; i < domain_setting_cookiepolicy.length; i++) {
                          if (i + 1 != domain_setting_cookiepolicy.length) {
                            html_file += `
                        <tr>
                        <td>
                          <p class="text_color_p">
                          ${domain_setting_cookiepolicy[i].name_cookietype}
                          </p>
                        </td>
                        <td style="width: 76%">
                          <p class="text_color_p">
                          ${domain_setting_cookiepolicy[i].detail_cookie}
                          </p>
                        </td>
                        <td style="width: 11%;">
                          <div class="active_buttom">
                            <div class="form-check form-switch check_buttom">
                              <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${i + 1}" name="cookie"
                                value="${domain_setting_cookiepolicy[i].id_dsc},${domain_setting_cookiepolicy[i].approve}">
                            </div>
                          </div>
                          <div class="mt-2 text-center">
                            <label id="label_approve_${i + 1}">Active</label>
                          </div>
                        </td>
                      </tr>`;
                          } else {
                            html_file += `
                        <tr class="tr_none">
                        <td>
                          <p class="text_color_p">
                          ${domain_setting_cookiepolicy[i].name_cookietype}
                          </p>
                        </td>
                        <td style="width: 76%">
                          <p class="text_color_p">
                          ${domain_setting_cookiepolicy[i].detail_cookie}
                          </p>
                        </td>
                        <td style="width: 11%;">
                          <div class="active_buttom">
                            <div class="form-check form-switch check_buttom">
                              <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${i + 1}" name="cookie"
                                value="${domain_setting_cookiepolicy[i].id_dsc},${domain_setting_cookiepolicy[i].approve}">
                            </div>
                          </div>
                          <div class="mt-2 text-center">
                            <label id="label_approve_${i + 1}">Active</label>
                          </div>
                        </td>
                      </tr>`;
                          }
                        }
                        html_file += `
                        </tbody>
                        </table>
                        </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                      <a href="#bccs-options" data-bs-toggle="collapse">เลือกประเภทคุกกี้</a>
                      <button id="bccs-buttonDoNotAgree" type="button" class="btn btn-danger text-decoration-none">
                        ปฏิเสธ
                      </button>
                      <button id="bccs-buttonAgree" type="button" class="btn btn-success">อนุญาต</button>
                      <button id="bccs-buttonSave" type="button" class="btn btn-primary">
                        อนุญาตตามที่เลือก
                      </button>
                      <button id="bccs-buttonAgreeAll" type="button" class="btn btn-success">อนุญาตทั้งหมด</button>
                    </div>
                    </div>
                  </div>
                  <style>
                        .text_color_p {
                            color: #6d7175;
                        }
                        .active_buttom {
                            align-self: center;
                            display: flex;
                            justify-content: center;
                        }
                        .check_buttom {
                            font-size: x-large;
                        }
                        .tr_none td {
                            border: none;
                        }
                        table tr {
                            height: 6rem;
                        }
                        </style>`;

                        fs.writeFile('./dist/file_domain/' + id_domain, html_file, function (err, file) {
                          if (err) throw err;
                        });
                      });
                  });
              });
          });
      });
    }
  });
};


module.exports = controller;