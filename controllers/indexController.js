var nodemailer = require('nodemailer');
const controller = {};


controller.show = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        console.log("asdasd");
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as new;', (err, date) => {
                    conn.query('SELECT * FROM doc_pdpa_document WHERE doc_action = 0 and user_id = ?;', [user], (err, doc) => {
                        conn.query("SELECT * FROM doc_pdpa_document_type;", (err, doc_type) => {
                            conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                                conn.query('SELECT * FROM doc_pdpa_document_type as pdt join doc_pdpa_document as pd on pd.doc_type_id = pdt.doc_type_id WHERE pd.doc_action = 0 group by pd.doc_type_id;', (err, have_doc) => {

                                    let word = []
                                    let word1 = []
                                    for (i in words) {
                                        word.push(words[i].words_id)
                                        word1.push(words[i].words_often)
                                    }
                                    if (err) {
                                        res.json(err)
                                    }
                                    let doc_i = []
                                    let doc_t = []
                                    let doc_n = []
                                    let doc_r = []
                                    let doc_status = []

                                    for (i in doc) {
                                        doc_i.push(doc[i].doc_id)
                                        doc_t.push(doc[i].doc_type_id)
                                        doc_n.push(doc[i].doc_name)
                                        doc_r.push(doc[i].doc_remark)
                                        doc_status.push(doc[i].doc_status)
                                    }

                                    let doc_type_id1 = []
                                    let doc_type_name1 = []
                                    for (i in doc_type) {
                                        doc_type_id1.push(doc_type[i].doc_type_id)
                                        doc_type_name1.push(doc_type[i].doc_type_name)
                                    }
                                    let have_doc1 = []
                                    for (i in have_doc) {
                                        have_doc1.push({ "have_doc_id": have_doc[i].doc_type_id, "have_doc_name": have_doc[i].doc_type_name })
                                    }

                                    let have_doc_type_name = []
                                    for (i in have_doc1) {
                                        for (j in doc_type) {
                                            if (have_doc1[i] == doc_type[j].doc_type_id) {
                                                have_doc_type_name.push(doc_type[j].doc_type_name)
                                            }
                                        }
                                    }

                                    res.render('./main_doc/index', {
                                        doc: doc,
                                        doc_i: doc_i,
                                        doc_n: doc_n,
                                        doc_r: doc_r,
                                        doc_t: doc_t,
                                        doc_status,
                                        doc_type: doc_type,
                                        doc_type_id1: doc_type_id1,
                                        doc_type_name1: doc_type_name1,
                                        have_doc1: have_doc1,
                                        have_doc_type_name: have_doc_type_name,
                                        data3: date,
                                        history: history,
                                        words: words,
                                        words1: word,
                                        words2: word1,
                                        session: req.session
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }
}

controller.sent_mail = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        var mail = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: '621998020@crru.ac.th',
                pass: '1579900892562'
            }
        });

        var mailOptions = {
            from: '621998020@crru.ac.th',
            to: req.body.email_outside,
            subject: req.body.subject,
            html: req.body.link_and_remark
        };

        mail.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });

        req.getConnection((err, conn) => {
            conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as new;', (err, date) => {
                var query1 = "INSERT INTO doc_pdpa_document_log SET user_id = '" + req.session.userid + "' ,doc_id = " + req.body.link_doc_id + ", log_date = '" + date[0].new + "', log_action = 7, log_detail = 'แชร์เอกสารไปที่ Email : " + req.body.email_outside + "'"
                conn.query(query1, (err, d1) => {
                    if (err) {
                        res.json(err)
                    }
                })
            })
        })
        res.redirect('back');
    }
}


controller.show_slide = (req, res) => {

    const { id } = req.params;
    const user = req.session.userid;

    req.getConnection((err, conn) => {
        conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
            conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE d.doc_id = ? and dp.page_action = 0 ', [id], (err, page) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    conn.query('SELECT * FROM doc_pdpa_data ', (err, pd) => {

                        let word = []
                        let word1 = []
                        for (i in words) {
                            word.push(words[i].words_id)
                            word1.push(words[i].words_often)
                        }
                        let code = []
                        let code1 = []
                        let code_name = []
                        for (i in pd) {
                            code.push(pd[i].data_id)
                            code1.push(pd[i].data_code)
                            code_name.push(pd[i].data_name)
                        }


                        if (err) {
                            res.json(err)
                        } else {
                            res.render('./main_doc/show_slide', {
                                // res.json({
                                data: page,
                                words: words,
                                words1: word,
                                words2: word1,
                                code: code,
                                code1: code1,
                                code_name: code_name,
                                history: history,
                                pd: pd,
                                session: req.session
                            });
                        }
                    })
                })
            })
        })
    })
}


controller.print_doc = (req, res) => {

    const { id } = req.params;
    const user = req.session.userid;

    req.getConnection((err, conn) => {
        conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
            conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE d.doc_id = ? and dp.page_action = 0 ', [id], (err, page) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    conn.query('SELECT * FROM doc_pdpa_data ', (err, pd) => {

                        let word = []
                        let word1 = []
                        for (i in words) {
                            word.push(words[i].words_id)
                            word1.push(words[i].words_often)
                        }
                        let code = []
                        let code1 = []
                        let code_name = []
                        for (i in pd) {
                            code.push(pd[i].data_id)
                            code1.push(pd[i].data_code)
                            code_name.push(pd[i].data_name)
                        }


                        if (err) {
                            res.json(err)
                        } else {
                            res.render('./main_doc/print_doc', {
                                // res.json({
                                data: page,
                                words: words,
                                words1: word,
                                words2: word1,
                                code: code,
                                code1: code1,
                                code_name: code_name,
                                history: history,
                                pd: pd,
                                session: req.session
                            });
                        }
                    })
                })
            })
        })
    })
}


controller.copydoc_save = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        const doc_find = req.body.doc_id;
        req.getConnection((err, conn) => {
            conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as new;', (err, date) => {
                conn.query("SELECT * FROM doc_pdpa_document WHERE doc_id = ?", [doc_find], (err, find_doc) => {
                    conn.query("INSERT INTO doc_pdpa_document SET doc_name = ?, doc_type_id = ?, doc_remark = ?, doc_date_create = ?", [find_doc[0].doc_name + '(คัดลอก)', find_doc[0].doc_type_id, find_doc[0].doc_remark, date[0].new], (err, d1) => {
                        conn.query("SELECT * FROM doc_pdpa_document  ORDER BY doc_id DESC LIMIT 1;", (err, document) => {
                            conn.query("UPDATE doc_pdpa_document SET user_id = ? WHERE doc_id = ?", [user, document[0].doc_id], (err, du) => {
                                conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE d.doc_id = ? and dp.page_action = 0', [doc_find], (err, page_doc_find) => {
                                    for (var j = 0; j < page_doc_find.length; j++) {
                                        conn.query("INSERT INTO doc_pdpa_document_page SET doc_id= ?, page_number = ?, page_content = ? ;", [document[0].doc_id, j + 1, page_doc_find[j].page_content], (err, d2) => {})
                                        if (err) {
                                            res.json(err)
                                        }
                                    }

                                    conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 9, log_detail = ? ,user_id = ?", [document[0].doc_id, document[0].doc_date_create, 'คัดลอกเอกสารจาก ' + find_doc[0].doc_name + ' เป็น ' + document[0].doc_name, user], (err, d3) => {
                                        if (err) {
                                            res.json(err)
                                        }
                                    })

                                    if (err) {
                                        res.json(err)
                                    }
                                    res.redirect('/paper/' + document[0].doc_id);
                                })
                            })
                        })
                    })
                })
            })
        })
    }
}

controller.api_link = (req, res) => {
    const { id } = req.params;
    const user = req.session.userid;

    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE d.doc_id = ? and dp.page_action = 0 ', [id], (err, page) => {
            conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                conn.query('SELECT * FROM doc_pdpa_data ', (err, pd) => {

                    let new_data = []
                    if (page.page_content != null) {
                        let page_id = []
                        let doc_id = []
                        let page_number = []
                        let page_content = []
                        let page_action = []
                        let doc_type_id = []
                        let doc_name = []
                        let doc_date_create = []
                        let user_id = []
                        let doc_status = []
                        let doc_remark = []
                        let doc_action = []

                        for (i in page) {
                            page_id.push(page[i].page_id)
                            doc_id.push(page[i].doc_id)
                            page_number.push(page[i].page_number)
                            page_content.push(page[i].page_content)
                            page_action.push(page[i].page_action)
                            doc_type_id.push(page[i].doc_type_id)
                            doc_name.push(page[i].doc_name)
                            doc_date_create.push(page[i].doc_date_create)
                            user_id.push(page[i].user_id)
                            doc_status.push(page[i].doc_status)
                            doc_remark.push(page[i].doc_remark)
                            doc_action.push(page[i].doc_action)
                        }

                        let page_content_replace = []
                        for (i in page_content) {
                            if (page_content[i] != null) {
                                let text = page[i].page_content;
                                for (j in pd) {
                                    let regex = new RegExp(pd[j].data_code, 'ig')
                                    text = text.replace(regex, pd[j].data_name)
                                }
                                page_content_replace.push(text)
                            } else {
                                page_content_replace.push(null)
                            }
                        }


                        for (i in page_id) {
                            new_data.push({
                                "page_id": page_id[i],
                                "doc_id": doc_id[i],
                                "page_number": page_number[i],
                                "page_content": page_content_replace[i],
                                "page_action": page_action[i],
                                "doc_type_id": doc_type_id[i],
                                "doc_name": doc_name[i],
                                "doc_date_create": doc_date_create[i],
                                "user_id": user_id[i],
                                "doc_status": doc_status[i],
                                "doc_remark": doc_remark[i],
                                "doc_action": doc_action[i]
                            })
                        }
                    }
                    if (err) {
                        res.json(err)
                    } else {
                        if (page.page_content != null) {
                            res.json({
                                data: new_data,
                            });
                        } else {
                            res.json({
                                data: page,
                            });
                        }
                    }
                })
            })
        })
    })
}

controller.api_support = (req, res) => {
    const { id } = req.params;
    const user = req.session.userid;

    req.getConnection((err, conn) => {
        conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
            conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE d.doc_id = ? and dp.page_action = 0 ', [id], (err, page) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    conn.query('SELECT * FROM doc_pdpa_data ', (err, pd) => {

                        let new_data = []
                        if (page.page_content != null) {
                            let page_id = []
                            let doc_id = []
                            let page_number = []
                            let page_content = []
                            let page_action = []
                            let doc_type_id = []
                            let doc_name = []
                            let doc_date_create = []
                            let user_id = []
                            let doc_status = []
                            let doc_remark = []
                            let doc_action = []

                            for (i in page) {
                                page_id.push(page[i].page_id)
                                doc_id.push(page[i].doc_id)
                                page_number.push(page[i].page_number)
                                page_content.push(page[i].page_content)
                                page_action.push(page[i].page_action)
                                doc_type_id.push(page[i].doc_type_id)
                                doc_name.push(page[i].doc_name)
                                doc_date_create.push(page[i].doc_date_create)
                                user_id.push(page[i].user_id)
                                doc_status.push(page[i].doc_status)
                                doc_remark.push(page[i].doc_remark)
                                doc_action.push(page[i].doc_action)
                            }

                            let page_content_replace = []
                            for (i in page_content) {
                                if (page_content[i] != null) {
                                    let text = page[i].page_content;
                                    for (j in pd) {
                                        let regex = new RegExp(pd[j].data_code, 'ig')
                                        text = text.replace(regex, pd[j].data_name)
                                    }
                                    page_content_replace.push(text)
                                } else {
                                    page_content_replace.push(null)
                                }
                            }


                            for (i in page_id) {
                                new_data.push({
                                    "page_id": page_id[i],
                                    "doc_id": doc_id[i],
                                    "page_number": page_number[i],
                                    "page_content": page_content_replace[i],
                                    "page_action": page_action[i],
                                    "doc_type_id": doc_type_id[i],
                                    "doc_name": doc_name[i],
                                    "doc_date_create": doc_date_create[i],
                                    "user_id": user_id[i],
                                    "doc_status": doc_status[i],
                                    "doc_remark": doc_remark[i],
                                    "doc_action": doc_action[i]
                                })
                            }
                        }
                        if (err) {
                            res.json(err)
                        } else {
                            res.render('./main_doc/api_support', {
                                data: id,
                                history: history,
                            });
                        }
                    })
                })
            })
        })
    })
}

controller.savedoc = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        const data = req.body;
        req.getConnection((err, conn) => {
            conn.query("INSERT INTO doc_pdpa_document SET doc_name = ?, doc_type_id = ?, doc_remark = ?, doc_date_create = ?", [data.doc_name, data.doc_type_id, data.doc_remark, data.doc_date_create], (err, d1) => {
                conn.query("SELECT * FROM doc_pdpa_document  ORDER BY doc_id DESC LIMIT 1;", (err, document) => {
                    conn.query("UPDATE doc_pdpa_document SET user_id = ? WHERE doc_id = ?", [user, document[0].doc_id], (err, du) => {
                        conn.query("INSERT INTO doc_pdpa_document_page SET page_number = 1, doc_id = ?", [document[0].doc_id], (err, d2) => {
                            if (err) {
                                res.json(err)
                            }
                        })
                        conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 0, log_detail = 'เพิ่มเอกสาร' ,user_id = ?", [document[0].doc_id, document[0].doc_date_create, user], (err, d3) => {
                            conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 1, log_detail = 'ดูเอกสาร' ,user_id = ?;", [document[0].doc_id, document[0].doc_date_create, user], (err, d4) => {
                                conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 2, log_detail = 'เพิ่มหน้า 1' ,user_id = ?;", [document[0].doc_id, document[0].doc_date_create, user], (err, d5) => {
                                    if (err) {
                                        res.json(err)
                                    }
                                })
                            })
                        })
                        if (err) {
                            res.json(err)
                        }
                        res.redirect('/paper/' + document[0].doc_id);
                    })
                })
            })
        })
    }
}

controller.updatedoc = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {

        const user = req.session.userid;
        const data_id = req.body.doc_id;
        const data = req.body;
        const status_doc = [{ status_id: '0', status_name: 'ร่าง' }, { status_id: '1', status_name: 'ต้นแบบ' }, { status_id: '2', status_name: 'ใช้งาน' }, { status_id: '3', status_name: 'ยกเลิก' }]
        req.getConnection((err, conn) => {
            conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as new;', (err, date) => {
                conn.query("SELECT * FROM doc_pdpa_document WHERE doc_id = ?", [data.doc_id], (err, doc_find) => {
                    if (doc_find[0].doc_status != data.doc_status) {
                        for (var i = 0; i < status_doc.length; i++) {
                            if (data.doc_status == 0 && (doc_find[0].doc_status == status_doc[i].status_id)) {
                                conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 8, log_detail = ? ,user_id = ?", [data.doc_id, date[0].new, 'เปลี่ยนสถานะจาก ' + status_doc[i].status_name + ' เป็น ร่าง', user], (err, d1) => {})
                                break
                            }
                            if (data.doc_status == 1 && (doc_find[0].doc_status == status_doc[i].status_id)) {
                                conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 8, log_detail = ? ,user_id = ?", [data.doc_id, date[0].new, 'เปลี่ยนสถานะจาก ' + status_doc[i].status_name + ' เป็น ต้นแบบ', user], (err, d2) => {})
                                break
                            }
                            if (data.doc_status == 2 && (doc_find[0].doc_status == status_doc[i].status_id)) {
                                conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 8, log_detail = ? ,user_id = ?", [data.doc_id, date[0].new, 'เปลี่ยนสถานะจาก ' + status_doc[i].status_name + ' เป็น ใช้งาน', user], (err, d3) => {})
                                break
                            }
                            if (data.doc_status == 3 && (doc_find[0].doc_status == status_doc[i].status_id)) {
                                conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 8, log_detail = ? ,user_id = ?", [data.doc_id, date[0].new, 'เปลี่ยนสถานะจาก ' + status_doc[i].status_name + ' เป็น ยกเลิก', user], (err, d4) => {})
                                break
                            }

                        }
                    }

                    conn.query("UPDATE doc_pdpa_document SET ? WHERE doc_id = ?", [data, data_id], (err, dt) => {
                        if (err) {
                            res.json(err)
                        }
                        res.redirect('/index');
                    });
                })
            });
        });
    }
}

controller.deletedoc = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data_id = req.body.doc_id;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM  doc_pdpa_document as d LEFT JOIN doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE d.doc_id = ?", [data_id], (err, doc) => {
                    conn.query("UPDATE doc_pdpa_document SET doc_action = 1 WHERE doc_id = ?", [data_id], (err, du) => {
                        conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as new;', (err, date) => {
                            conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 5, log_detail = 'ลบเอกสาร',user_id = ?;", [doc[0].doc_id, date[0].new, user], (err, logdoc) => {
                                if (err) {
                                    res.json(err)
                                }
                                res.redirect('back');
                            })
                        })
                    })
                })
            })
        })
    }
}

controller.doc_type = (req, res) => {
        let doc_type1 = []
        let doc_type2 = []
        let word = []
        let word1 = []
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/');
        } else {
            const user = req.session.userid;
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query("SELECT * FROM doc_pdpa_document_type ;", (err, doc_type) => {
                        conn.query('SELECT * FROM doc_pdpa_words', (err, words) => {
                            conn.query('SELECT * FROM doc_pdpa_document group by doc_type_id', (err, doc_active) => {
                                if (err) {
                                    res.json(err)
                                }
                                for (i in words) {
                                    word.push(words[i].words_id)
                                    word1.push(words[i].words_often)
                                }
                                var check_used_doc = new Array(doc_type.length).fill(0)

                                for (var i = 0; i < doc_type.length; i++) {
                                    doc_type1.push(doc_type[i].doc_type_id)
                                    doc_type2.push(doc_type[i].doc_type_name)
                                    for (j in doc_active) {
                                        if (doc_active[j].doc_type_id == doc_type[i].doc_type_id) {
                                            check_used_doc[i] = 1
                                        }
                                    }
                                }
                                res.render('./doc_type/doc_type', {
                                    history: history,
                                    words: words,
                                    doc_type: doc_type,
                                    doc_type1: doc_type1,
                                    doc_type2: doc_type2,
                                    check_used_doc: check_used_doc,
                                    words1: word,
                                    keyword: "",
                                    words2: word1,
                                    session: req.session
                                });
                            });
                        });
                    });
                });
            });
        }
    }
    // Post Search Find
controller.doc_type_find = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        const name = req.body.search;
        let convert_name = "%" + name + "%";
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_document_type WHERE doc_type_name LIKE ? ;', [convert_name], (err, doc_type) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query('SELECT * FROM doc_pdpa_document group by doc_type_id', (err, doc_active) => {
                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            let doc_type1 = []
                            let doc_type2 = []
                            var check_used_doc = new Array(doc_type.length).fill(0)

                            for (var i = 0; i < doc_type.length; i++) {
                                doc_type1.push(doc_type[i].doc_type_id)
                                doc_type2.push(doc_type[i].doc_type_name)
                                for (j in doc_active) {
                                    if (doc_active[j].doc_type_id == doc_type[i].doc_type_id) {
                                        check_used_doc[i] = 1
                                    }
                                }
                            }
                            if (err) {
                                res.json(err)
                            }
                            res.render('./doc_type/doc_type', {
                                history: history,
                                words: words,
                                doc_type: doc_type,
                                doc_type1: doc_type1,
                                doc_type2: doc_type2,
                                check_used_doc: check_used_doc,
                                keyword: name,
                                words1: word,
                                words2: word1,
                                session: req.session
                            });
                        });
                    });
                });
            });
        });
    }
}

controller.save_doc_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.doc_type_name;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("INSERT INTO doc_pdpa_document_type set doc_type_name = ?;", [data], (err, dt) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/doc_type')
            });
        });
    }
}


controller.update_doc_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.doc_type_id;
        const data1 = req.body.doc_type_name;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("UPDATE doc_pdpa_document_type SET doc_type_name =? WHERE doc_type_id = ?;", [data1, data], (err, dt) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/doc_type')
            });
        });
    }
}


controller.delete_doc_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.doc_type_id;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("DELETE FROM doc_pdpa_document_type WHERE doc_type_id = ?", [data], (err, dt) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('back')
            });
        });
    }
}



controller.paper = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE d.doc_id = ? and dp.page_action = 0', [id], (err, page) => {
                    if (page[0]) {
                        conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE dp.page_id = ? ;', [page[0].page_id], (err, page1) => {
                            conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                                conn.query('SELECT * FROM doc_pdpa_data ', (err, pd) => {
                                    let word = []
                                    let word1 = []
                                    for (i in words) {
                                        word.push(words[i].words_id)
                                        word1.push(words[i].words_often)
                                    }
                                    let code = []
                                    let code1 = []
                                    let code_name = []
                                    for (i in pd) {
                                        code.push(pd[i].data_id)
                                        code1.push(pd[i].data_code)
                                        code_name.push(pd[i].data_name)
                                    }
                                    if (err) {
                                        res.json(err)
                                    } else {
                                        res.render('./paper/paper', {
                                            doc_number: id,
                                            data: page,
                                            data1: page1,
                                            words: words,
                                            words1: word,
                                            words2: word1,
                                            code: code,
                                            code1: code1,
                                            code_name: code_name,
                                            history: history,
                                            pd: pd,
                                            session: req.session
                                        });
                                    }
                                })
                            })
                        })
                    } else {
                        conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                            conn.query('SELECT * FROM doc_pdpa_data ', (err, pd) => {
                                let word = []
                                let word1 = []
                                for (i in words) {
                                    word.push(words[i].words_id)
                                    word1.push(words[i].words_often)
                                }
                                let code = []
                                let code1 = []
                                let code_name = []
                                for (i in pd) {
                                    code.push(pd[i].data_id)
                                    code1.push(pd[i].data_code)
                                    code_name.push(pd[i].data_name)
                                }
                                if (err) {
                                    res.json(err)
                                } else {
                                    res.render('./paper/paper', {
                                        doc_number: id,
                                        data: page,
                                        data1: 0,
                                        words: words,
                                        words1: word,
                                        words2: word1,
                                        code: code,
                                        code1: code1,
                                        code_name: code_name,
                                        history: history,
                                        pd: pd,
                                        session: req.session
                                    });
                                }
                            })
                        })
                    }
                })
            })
        })
    }
}



controller.createpaper = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        const { id } = req.params;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT count(*) as lengthpage FROM pdpa.doc_pdpa_document_page where doc_id = ?", [id], (err, page) => {
                    conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as new;', (err, date) => {
                        conn.query("INSERT INTO doc_pdpa_document_page SET doc_id = ?, page_number = ?;", [id, page[0].lengthpage + 1], (err, pagecount) => {
                            if (err) {
                                res.json(err)
                            }
                        })
                        conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 2, log_detail = ? ,user_id = ?;", [id, date[0].new, "เพิ่มหน้า " + (Number(page[0].lengthpage) + 1), user], (err, logpage) => {
                            if (err) {
                                res.json(err)
                            }
                        })
                        conn.query("SELECT * FROM doc_pdpa_document_page as dp left join doc_pdpa_document as d on d.doc_id = dp.doc_id left join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dp.page_id =?", [id], (err, actionpaper) => {
                            if (err) {
                                res.json(err)
                            }
                            res.redirect('back')
                        })
                    })
                })
            })
        })
    }
}

controller.editpaper = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_document_page as p1 LEFT JOIN doc_pdpa_document as p2 ON p1.doc_id=p2.doc_id WHERE p1.page_id = ?;', [id], (err, page) => {
                    conn.query('SELECT * FROM doc_pdpa_document_page as p1 LEFT JOIN doc_pdpa_document as p2 ON p1.doc_id=p2.doc_id WHERE p2.doc_id = ?;', [page[0].doc_id], (err, page1) => {
                        conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                            conn.query('SELECT * FROM doc_pdpa_document as d join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id LEFT JOIN pdpa.doc_pdpa_document_page as dp ON dp.doc_id = d.doc_id WHERE d.doc_id = ? AND dl.log_action = 4 GROUP BY dl.log_id;', [page[0].doc_id], (err, page2) => {
                                conn.query('SELECT * FROM doc_pdpa_data;', (err, pd) => {
                                    let word = []
                                    let word1 = []
                                    for (i in words) {
                                        word.push(words[i].words_id)
                                        word1.push(words[i].words_often)
                                    }
                                    let code = []
                                    let code1 = []
                                    let code_name = []
                                    for (i in pd) {
                                        code.push(pd[i].data_id)
                                        code1.push(pd[i].data_code)
                                        code_name.push(pd[i].data_name)
                                    }
                                    if (err) {
                                        res.json(err)
                                    } else {
                                        res.render('./paper/paper', {
                                            doc_number: page[0].doc_id,
                                            data: page1,
                                            data1: page,
                                            words: words,
                                            words1: word,
                                            words2: word1,
                                            history: history,
                                            pd: pd,
                                            code: code,
                                            code1: code1,
                                            code_name: code_name,
                                            session: req.session
                                        });
                                    }
                                })
                            })
                        })
                    })
                })
            })
        })
    }
}

controller.updatepaper = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        const user = req.session.userid;
        const body = req.body;
        const page_number = req.body.page_number
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM doc_pdpa_document_page as p1 LEFT JOIN doc_pdpa_document as p2 ON p1.doc_id=p2.doc_id WHERE p1.page_id = ?;', [id], (err, page2) => {
                conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as new;', (err, date) => {
                    conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 3, log_detail = ? ,user_id = ?;", [page2[0].doc_id, date[0].new, "แก้ไขหน้า " + page_number, user], (err, logpage) => {
                        if (err) {
                            res.json(err);
                        }
                    });
                    conn.query('UPDATE doc_pdpa_document_page SET page_content=? WHERE page_id=?', [body.page_content, id], (err, page1) => {
                        if (err) {
                            res.json(err);
                        }
                    });
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/editpaper/' + page2[0].page_id)
                });
            });
        });
    }
};


controller.deletepaper = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_document_page as p1 LEFT JOIN doc_pdpa_document as p2 ON p1.doc_id=p2.doc_id WHERE p1.page_id = ?;', [id], (err, page2) => {
                    conn.query("UPDATE doc_pdpa_document_page SET page_action = 1 WHERE page_id = ?", [id], (err, du) => {
                        conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%d %H:%i:%s") as new;', (err, date) => {
                            conn.query("INSERT INTO doc_pdpa_document_log SET doc_id = ?, log_date = ?, log_action = 4, log_detail = ? ,user_id = ?;", [page2[0].doc_id, date[0].new, "ลบหน้า " + page2[0].page_number, user], (err, logpage) => {
                                if (err) {
                                    res.json(err)
                                }
                                res.redirect('back');
                            })
                        })
                    })
                })
            })
        })
    }
}



controller.words = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE d.doc_id = ?', [id], (err, page) => {
                    conn.query('SELECT * FROM doc_pdpa_document_page AS dp LEFT JOIN doc_pdpa_document AS d ON dp.doc_id = d.doc_id WHERE dp.page_id = ?;', [page[0].page_id], (err, page1) => {
                        conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            if (err) {
                                res.json(err)
                            } else {
                                res.render('./paper/paper', {
                                    data: page,
                                    data1: page1,
                                    words: words,
                                    words1: word,
                                    words2: word1,
                                    history: history,
                                    session: req.session
                                });
                            }
                        })
                    })
                })
            })
        })
    }
}

controller.new_word = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("INSERT INTO doc_pdpa_words SET ?;", [data], (err, words) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('back');
            })
        })
    }
}

controller.updateword = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.words_id;
        const data1 = req.body.words_often;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("UPDATE doc_pdpa_words SET words_often = ? WHERE words_id = ?;", [data1, data], (err, words) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('back');
            })
        })
    }
}

controller.deleteword = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.words_id;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("DELETE FROM doc_pdpa_words  WHERE words_id = ?;", [data], (err, words) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('back');
            })
        })
    }
}



controller.data_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_data_type ;", (err, data_type) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query("SELECT * FROM doc_pdpa_data group by data_type_id;", (err, data_active) => {
                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            let data_type1 = []
                            let data_type2 = []
                            var check_used_data = new Array(data_type.length).fill(0)

                            for (i in data_type) {
                                data_type1.push(data_type[i].data_type_id)
                                data_type2.push(data_type[i].data_type_name)
                                for (j in data_active) {
                                    if (data_active[j].data_type_id == data_type[i].data_type_id) {
                                        check_used_data[i] = 1
                                    }
                                }
                            }
                            if (err) {
                                res.json(err)
                            }
                            res.render('./data_type/data_type', {
                                history: history,
                                words: words,
                                words1: word,
                                words2: word1,
                                keyword: "",
                                data_type: data_type,
                                data_type1: data_type1,
                                data_type2: data_type2,
                                check_used_data: check_used_data,
                                session: req.session
                            });
                        });
                    });
                });
            })
        })
    }
}

controller.data_type_find = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        const name = req.body.search;
        const convert_name = '%' + name + '%'
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_data_type WHERE data_type_name LIKE ? ;", [convert_name], (err, data_type) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query("SELECT * FROM doc_pdpa_data group by data_type_id ;", (err, data_active) => {
                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            let data_type1 = []
                            let data_type2 = []
                            var check_used_data = new Array(data_type.length).fill(0)
                            for (i in data_type) {
                                data_type1.push(data_type[i].data_type_id)
                                data_type2.push(data_type[i].data_type_name)
                                for (j in data_active) {
                                    if (data_active[j].data_type_id == data_type[i].data_type_id) {
                                        check_used_data[i] = 1
                                    }
                                }
                            }
                            if (err) {
                                res.json(err)
                            }
                            res.render('./data_type/data_type', {
                                history: history,
                                words: words,
                                words1: word,
                                words2: word1,
                                keyword: name,
                                data_type: data_type,
                                data_type1: data_type1,
                                data_type2: data_type2,
                                check_used_data: check_used_data,
                                session: req.session
                            });
                        });
                    });
                });
            })
        })
    }
}

controller.new_data_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_data_type ;", (err, data_type) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        let word = []
                        let word1 = []
                        for (i in words) {
                            word.push(words[i].words_id)
                            word1.push(words[i].words_often)
                        }
                        if (err) {
                            res.json(err)
                        }
                        res.render('./data_type/new_data_type', {
                            history: history,
                            words: words,
                            words1: word,
                            words2: word1,
                            data_type: data_type,
                            session: req.session
                        });
                    });
                });
            })
        })
    }
}

controller.save_data_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.data_type_name;
        req.getConnection((err, conn) => {
            conn.query("INSERT INTO doc_pdpa_data_type set data_type_name = ? ", [data], (err, dt) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/data_type');
            });
        });
    }
}


controller.update_data_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.data_type_id;
        const data1 = req.body.data_type_name;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("UPDATE doc_pdpa_data_type SET data_type_name = ? WHERE data_type_id = ?", [data1, data], (err, dt) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/data_type');
            });
        });
    }
}


controller.delete_data_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.data_type_id;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("DELETE FROM doc_pdpa_data_type  WHERE data_type_id = ?;", [data], (err, delete_data_type) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/data_type');
            })
        })
    }
}






controller.level_type = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_level ;", (err, level) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query('SELECT * FROM doc_pdpa_data group by data_level_id;', (err, data_active) => {

                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            let level_type1 = []
                            let level_type2 = []
                            var check_used_level = new Array(level.length).fill(0)
                            for (i in level) {
                                level_type1.push(level[i].level_id)
                                level_type2.push(level[i].level_name)
                                for (j in data_active) {
                                    if (data_active[j].data_level_id == level[i].level_id) {
                                        check_used_level[i] = 1
                                    }
                                }

                            }
                            if (err) {
                                res.json(err)
                            }
                            res.render('./level/level_type', {
                                history: history,
                                words: words,
                                words1: word,
                                words2: word1,
                                level: level,
                                level_type1: level_type1,
                                level_type2,
                                level_type2,
                                keyword: "",
                                check_used_level,
                                check_used_level,
                                session: req.session
                            });
                        })
                    });
                });
            })
        })
    }
}


controller.level_type_find = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        const name = req.body.search;
        const convert_name = '%' + name + '%'
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_level WHERE level_name LIKE ? ;", [convert_name], (err, level) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query('SELECT * FROM doc_pdpa_data group by data_level_id;', (err, data_active) => {

                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            let level_type1 = []
                            let level_type2 = []
                            var check_used_level = new Array(level.length).fill(0)
                            for (i in level) {
                                level_type1.push(level[i].level_id)
                                level_type2.push(level[i].level_name)
                                for (j in data_active) {
                                    if (data_active[j].data_level_id == level[i].level_id) {
                                        check_used_level[i] = 1
                                    }
                                }

                            }
                            if (err) {
                                res.json(err)
                            }
                            res.render('./level/level_type', {
                                history: history,
                                words: words,
                                words1: word,
                                words2: word1,
                                level: level,
                                level_type1: level_type1,
                                level_type2,
                                level_type2,
                                keyword: name,
                                check_used_level,
                                check_used_level,
                                session: req.session
                            });
                        })
                    });
                });
            })
        })
    }
}

controller.new_level = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_level;", (err, level) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        let word = []
                        let word1 = []
                        for (i in words) {
                            word.push(words[i].words_id)
                            word1.push(words[i].words_often)
                        }
                        if (err) {
                            res.json(err)
                        }
                        res.render('./level/new_level', {
                            history: history,
                            words: words,
                            words1: word,
                            words2: word1,
                            level: level,
                            session: req.session
                        });
                    });
                });
            })
        })
    }
}

controller.save_level = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.level_name;
        req.getConnection((err, conn) => {
            conn.query("INSERT INTO doc_pdpa_level set level_name = ?", [data], (err, level) => {

                if (err) {
                    res.json(err)
                }
                res.redirect('/level_type');
            });
        });
    }
}



controller.update_level = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.level_id;
        const data1 = req.body.level_name;
        req.getConnection((err, conn) => {
            conn.query("UPDATE doc_pdpa_level SET  level_name = ? WHERE level_id = ?", [data1, data], (err, dt) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/level_type');
            });
        });
    }
}




controller.delete_level = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.level_id;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("DELETE FROM doc_pdpa_level WHERE level_id = ?", [data], (err, dt) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/level_type');
            });
        });
    }
}



controller.personal_data = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('SELECT DATE_FORMAT(NOW(), "%Y-%m-%dT%H:%i:%s" ) as new;', (err, date) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query("SELECT * FROM doc_pdpa_data_type;", (err, data_type) => {
                        conn.query('select pd.data_id ,pd.data_level_id,pd.data_type_id ,pd.data_name,pd.data_detail ,pd.data_location_name,pd.data_location_detail,pd.data_code,DATE_FORMAT(pd.data_date_start, "%Y-%m-%d %H:%i:%s" ) as data_date_start,DATE_FORMAT(pd.data_date_end, "%Y-%m-%d %H:%i:%s" ) as data_date_end from doc_pdpa_data as pd join doc_pdpa_data_type as pdt on pdt.data_type_id = pd.data_type_id join doc_pdpa_level as l on l.level_id = pd.data_level_id WHERE pd.data_date_start <= ?  AND  ? < pd.data_date_end;', [date[0].new, date[0].new], (err, personal) => {
                            conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                                conn.query("SELECT * FROM doc_pdpa_level;", (err, level) => {
                                    conn.query("SELECT * FROM doc_pdpa_tag;", (err, tag) => {
                                        let word = []
                                        let word1 = []
                                        for (i in words) {
                                            word.push(words[i].words_id)
                                            word1.push(words[i].words_often)
                                        }
                                        let data_id = []
                                        let data_type_id = []
                                        let data_code = []
                                        let data_name = []
                                        let data_level_id = []
                                        let data_detail = []
                                        let data_date_start = []
                                        let data_date_end = []
                                        let data_location_name = []
                                        let data_location_detail = []


                                        for (i in personal) {
                                            data_id.push(personal[i].data_id)
                                            data_type_id.push(personal[i].data_type_id)
                                            data_code.push(personal[i].data_code)
                                            data_name.push(personal[i].data_name)
                                            data_level_id.push(personal[i].data_level_id)
                                            data_detail.push(personal[i].data_detail)
                                            data_date_start.push(personal[i].data_date_start)
                                            data_date_end.push(personal[i].data_date_end)
                                            data_location_name.push(personal[i].data_location_name)
                                            data_location_detail.push(personal[i].data_location_detail)
                                        }

                                        //level
                                        let level_id = []
                                        let level_name = []
                                        for (i in level) {
                                            level_id.push(level[i].level_id)
                                            level_name.push(level[i].level_name)
                                        }
                                        //data_type
                                        let data_type_id1 = []
                                        let data_type_name = []
                                        for (i in data_type) {
                                            data_type_id1.push(data_type[i].data_type_id)
                                            data_type_name.push(data_type[i].data_type_name)
                                        }

                                        if (err) {
                                            res.json(err)
                                        }
                                        res.render('./personal/personal_data', {
                                            history: history,
                                            words: words,
                                            data_type: data_type,
                                            words1: word,
                                            words2: word1,
                                            //data splite
                                            data_id: data_id,
                                            data_type_id: data_type_id,
                                            data_code: data_code,
                                            data_name: data_name,
                                            data_level_id: data_level_id,
                                            data_detail: data_detail,
                                            data_date_start: data_date_start,
                                            data_date_end: data_date_end,
                                            data_location_name: data_location_name,
                                            data_location_detail: data_location_detail,
                                            //
                                            //level splite
                                            level_id: level_id,
                                            level_name: level_name,
                                            //
                                            //data_type splite
                                            data_type_id1: data_type_id1,
                                            data_type_name: data_type_name,
                                            //
                                            personal: personal,
                                            // personal1: "",
                                            keyword: "",
                                            level: level,
                                            tag: tag,
                                            date: date[0].new,
                                            session: req.session
                                        })
                                    })
                                });
                            });
                        });
                    });
                });
            })
        })
    }
}

controller.personal_data_find = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        const keyword = req.body.search;
        const search_datetime = req.body.search_datetime;
        const tag_007 = req.body.tag_007;
        const convert_name = "%" + keyword + "%"
        const tag_007za = "%" + tag_007 + "%"

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_data_type;", (err, data_type) => {
                    conn.query('select pd.data_tag ,pd.data_id ,pd.data_level_id,pd.data_type_id ,pd.data_name,pd.data_detail ,pd.data_location_name,pd.data_location_detail,pd.data_code,DATE_FORMAT(pd.data_date_start, "%Y-%m-%d %H:%i:%s" ) as data_date_start,DATE_FORMAT(pd.data_date_end, "%Y-%m-%d %H:%i:%s" ) as data_date_end from doc_pdpa_data as pd join doc_pdpa_data_type as pdt on pdt.data_type_id = pd.data_type_id join doc_pdpa_level as l on l.level_id = pd.data_level_id WHERE (data_date_start <= ? and data_date_end > ?) and (data_name LIKE ? or data_code LIKE ?) and (data_tag LIKE ? );', [search_datetime, search_datetime, convert_name, convert_name, tag_007za], (err, personal) => {
                        conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                            conn.query("SELECT * FROM doc_pdpa_level;", (err, level) => {
                                conn.query("SELECT * FROM doc_pdpa_tag;", (err, tag) => {
                                    let word = []
                                    let word1 = []
                                    for (i in words) {
                                        word.push(words[i].words_id)
                                        word1.push(words[i].words_often)
                                    }
                                    let data_id = []
                                    let data_type_id = []
                                    let data_code = []
                                    let data_name = []
                                    let data_level_id = []
                                    let data_detail = []
                                    let data_date_start = []
                                    let data_date_end = []
                                    let data_location_name = []
                                    let data_location_detail = []


                                    for (i in personal) {
                                        data_id.push(personal[i].data_id)
                                        data_type_id.push(personal[i].data_type_id)
                                        data_code.push(personal[i].data_code)
                                        data_name.push(personal[i].data_name)
                                        data_level_id.push(personal[i].data_level_id)
                                        data_detail.push(personal[i].data_detail)
                                        data_date_start.push(personal[i].data_date_start)
                                        data_date_end.push(personal[i].data_date_end)
                                        data_location_name.push(personal[i].data_location_name)
                                        data_location_detail.push(personal[i].data_location_detail)
                                    }
                                    //level
                                    let level_id = []
                                    let level_name = []
                                    for (i in level) {
                                        level_id.push(level[i].level_id)
                                        level_name.push(level[i].level_name)
                                    }
                                    //data_type
                                    let data_type_id1 = []
                                    let data_type_name = []
                                    for (i in data_type) {
                                        data_type_id1.push(data_type[i].data_type_id)
                                        data_type_name.push(data_type[i].data_type_name)
                                    }
                                    if (err) {
                                        res.json(err)
                                    }
                                    res.json({
                                        history: history,
                                        words: words,
                                        data_type: data_type,
                                        words1: word,
                                        words2: word1,
                                        //data splite
                                        data_id: data_id,
                                        data_type_id: data_type_id,
                                        data_code: data_code,
                                        data_name: data_name,
                                        data_level_id: data_level_id,
                                        data_detail: data_detail,
                                        data_date_start: data_date_start,
                                        data_date_end: data_date_end,
                                        data_location_name: data_location_name,
                                        data_location_detail: data_location_detail,
                                        //
                                        //level splite
                                        level_id: level_id,
                                        level_name: level_name,
                                        //
                                        //data_type splite
                                        data_type_id1: data_type_id1,
                                        data_type_name: data_type_name,
                                        //
                                        personal: personal,
                                        keyword: keyword,
                                        date: search_datetime,
                                        level: level,
                                        tag: tag,
                                        session: req.session
                                    });
                                })
                            });
                        });
                    });
                });
            });
        });
    }
}

controller.new_personal = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_data_type;", (err, data_type) => {
                    conn.query('select pd.data_id, l.level_name,pd.data_name, pdt.data_type_name,pd.data_detail,pd.data_location_name,pd.data_location_detail,pd.data_code,DATE_FORMAT(pd.data_date_start, "%Y-%m-%d %H:%i:%s" ) as data_date_start,DATE_FORMAT(pd.data_date_end, "%Y-%m-%d %H:%i:%s" ) as data_date_end from doc_pdpa_data as pd join doc_pdpa_data_type as pdt on pdt.data_type_id = pd.data_type_id join doc_pdpa_level as l on l.level_id = pd.data_level_id  order by pd.data_id Desc;', (err, personal) => {
                        conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                            conn.query("SELECT * FROM doc_pdpa_level;", (err, level) => {
                                conn.query("SELECT * FROM doc_pdpa_tag;", (err, tag) => {
                                    let word = []
                                    let word1 = []

                                    for (i in words) {
                                        word.push(words[i].words_id)
                                        word1.push(words[i].words_often)
                                    }
                                    if (err) {
                                        res.json(err)
                                    }

                                    let codes = []
                                    let codes1 = []
                                    for (i in personal) {
                                        codes.push(personal[i].data_id);
                                        codes1.push(personal[i].data_code);
                                    }
                                    let tag_name = []
                                    for (i in tag) {
                                        tag_name.push(tag[i].tag_name)
                                    }


                                    res.render('./personal/new_personal', {
                                        history: history,
                                        words: words,
                                        data_type: data_type,
                                        words1: word,
                                        words2: word1,
                                        personal: personal,
                                        tag_name: tag_name,
                                        level: level,
                                        codes: codes,
                                        tag: tag,
                                        codes1: codes1,
                                        session: req.session
                                    });
                                });
                            });
                        });
                    });
                });
            })
        })
    }
}


controller.save_personal = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body;
        let mixCode = "#" + data.data_code;
        req.getConnection((err, conn) => {
            conn.query("INSERT INTO doc_pdpa_data SET data_type_id=?, data_code=?, data_name=?, data_level_id=?, data_detail=?, data_date_start=?, data_date_end=?, data_location_name=?, data_location_detail=?,data_tag=?", [data.data_type_id, mixCode, data.data_name, data.data_level_id, data.data_detail, data.data_date_start, data.data_date_end, data.data_location_name, data.data_location_detail, data.data_tag], (err, vvv) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/personal_data');
            });
        });
    }
}
controller.file_personal = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_data_type;", (err, data_type) => {
                    conn.query('select pd.data_tag,pd.data_id, l.level_name, pd.data_name, pdt.data_type_name,pd.data_detail,pd.data_location_name,pd.data_location_detail,pd.data_code,DATE_FORMAT(pd.data_date_start, "%Y-%m-%d %H:%i:%s" ) as data_date_start,DATE_FORMAT(pd.data_date_end, "%Y-%m-%d %H:%i:%s" ) as data_date_end,pd.data_type_id from doc_pdpa_data as pd join doc_pdpa_data_type as pdt on pdt.data_type_id = pd.data_type_id join doc_pdpa_level as l on l.level_id = pd.data_level_id WHERE pd.data_id = ? ;', [id], (err, personal) => {
                        conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                            conn.query("SELECT * FROM doc_pdpa_level;", (err, level) => {
                                conn.query('SELECT doc_pdpa_data.data_code FROM doc_pdpa_data WHERE data_id != ?;', [personal[0].data_id], (err, personal_1) => {
                                    console.log(personal_1);
                                    let word = []
                                    let word1 = []
                                    for (i in words) {
                                        word.push(words[i].words_id)
                                        word1.push(words[i].words_often)
                                    }
                                    let codes = []
                                    let codes1 = []
                                    for (i in personal) {
                                        codes.push(personal[i].data_id);
                                        codes1.push(personal[i].data_code);
                                    }
                                    if (err) {
                                        res.json(err)
                                    }
                                    let personal_name = personal[0].data_code.split("#");
                                    var all_code = []
                                    if (typeof personal_1[0] == 'undefined') {
                                        all_code.push("empty")
                                    } else if (typeof personal_1[0] === 'undefined') {
                                        all_code.push(personal_1[0].data_code)
                                    }
                                    let personal_tag = ""
                                    for (i in personal) {
                                        personal_tag = (personal[i].data_tag.split(','));
                                    }
                                    res.render('./personal/file_personal', {
                                        history: history,
                                        words: words,
                                        data_type: data_type,
                                        words1: word,
                                        words2: word1,
                                        codes: codes,
                                        // codes1: codes1,
                                        personal_1: all_code,
                                        personal_name: personal_name,
                                        personal_tag: personal_tag,
                                        personal: personal,
                                        level: level,
                                        session: req.session
                                    });
                                });
                            });
                        });
                    });
                });
            })
        })
    }
}


controller.edit_personal = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query("SELECT * FROM doc_pdpa_data_type;", (err, data_type) => {
                    conn.query('select pd.data_tag,pd.data_id, l.level_name, pd.data_name, pdt.data_type_name,pd.data_detail,pd.data_location_name,pd.data_location_detail,pd.data_code,DATE_FORMAT(pd.data_date_start, "%Y-%m-%dT%H:%i:%s" ) as data_date_start,DATE_FORMAT(pd.data_date_end, "%Y-%m-%dT%H:%i:%s" ) as data_date_end,pd.data_type_id from doc_pdpa_data as pd join doc_pdpa_data_type as pdt on pdt.data_type_id = pd.data_type_id join doc_pdpa_level as l on l.level_id = pd.data_level_id WHERE pd.data_id = ? ;', [id], (err, personal) => {
                        conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                            conn.query("SELECT * FROM doc_pdpa_level;", (err, level) => {
                                conn.query('SELECT doc_pdpa_data.data_code FROM doc_pdpa_data WHERE data_id != ?;', [personal[0].data_id], (err, personal_1) => {
                                    conn.query("SELECT * FROM doc_pdpa_tag;", (err, tag) => {
                                        let word = []
                                        let word1 = []
                                        for (i in words) {
                                            word.push(words[i].words_id)
                                            word1.push(words[i].words_often)
                                        }
                                        let codes = []
                                        let codes1 = []
                                        for (i in personal) {
                                            codes.push(personal[i].data_id);
                                            codes1.push(personal[i].data_code);
                                        }
                                        if (err) {
                                            res.json(err)
                                        }
                                        let personal_name = personal[0].data_code.split("#");
                                        var all_code = []
                                        if (typeof personal_1[0] == 'undefined') {
                                            all_code.push("empty")
                                        } else {
                                            for (i in personal_1) {
                                                all_code.push(personal_1[i].data_code)
                                            }
                                        }
                                        let tag_name = []
                                        for (i in tag) {
                                            tag_name.push(tag[i].tag_name)
                                        }
                                        res.render('./personal/edit_personal', {
                                            history: history,
                                            words: words,
                                            data_type: data_type,
                                            words1: word,
                                            words2: word1,
                                            codes: codes,
                                            tag: tag,
                                            tag_name: tag_name,
                                            // codes1: codes1,
                                            personal_1: all_code,
                                            personal_name: personal_name,
                                            personal: personal,
                                            level: level,
                                            session: req.session
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            })
        })
    }
}


controller.update_personal = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        const data = req.body;
        let mixCode = "#" + data.data_code;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("UPDATE doc_pdpa_data SET data_type_id=?, data_code=?, data_name=?, data_level_id=?, data_detail=?, data_date_start=?, data_date_end=?, data_location_name=?, data_location_detail=?,data_tag=? WHERE data_id=?", [data.data_type_id, mixCode, data.data_name, data.data_level_id, data.data_detail, data.data_date_start, data.data_date_end, data.data_location_name, data.data_location_detail, data.data_tag, id], (err, vvv) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/personal_data');
            });
        });
    }
}



controller.delete_personal = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = req.body.data_id;
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query("DELETE FROM doc_pdpa_data WHERE data_id=?", [data], (err, vvv) => {
                if (err) {
                    res.json(err)
                }
                res.redirect('/personal_data');
            });
        });
    }
}



controller.system_setting = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./system_setting/system_setting', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}


controller.membership = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./membership/membership', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}

controller.protect_policy = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./policy/protect_policy', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}

controller.publish_policy = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./policy/publish_policy', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}


controller.test_policy = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./policy/test_policy', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}


controller.share_policy = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./policy/share_policy', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}

controller.monitoring = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./monitoring/monitoring', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}

controller.data_gateway = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;

        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,ac.name as name_history, dl.log_action as action_history,d.doc_name as docname_history  from pdpa.doc_pdpa_document as d  join pdpa.doc_pdpa_document_log as dl on dl.doc_id = d.doc_id join pdpa.account as ac on ac.acc_id=dl.user_id WHERE dl.user_id = 3 order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./data_protect/data_gateway', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}



controller.saveTag = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {

        const user = req.session.userid;
        const data_tag = req.body;
        var myObj = JSON.parse(data_tag.tag_dg); // เเปลง  [{"value":"5555"},{"value":"6666"}]' ให้เป็น ["5555","6666"]
        var dta_json_tag = [];
        for (var i = 0; i < myObj.length; i++) {
            dta_json_tag.push(myObj[i].value)
        }
        req.getConnection((err, conn) => {
            for (var j = 0; j < dta_json_tag.length; j++) {
                conn.query('INSERT INTO doc_pdpa_tag SET tag_name =?', [dta_json_tag[j]], (err, data_tag) => {})
            }
            if (err) {
                res.json(err)
            }
            res.redirect('/personal_data');
        });
    }
}



module.exports = controller;