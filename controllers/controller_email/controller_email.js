const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');
const nodemailer = require('nodemailer');
const multer = require('multer')
const csv = require('csv-parser');
// const Papa = require('papaparse');
// const { json } = require("body-parser");
const controller = {};


controller.Inbox = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT id_email,email_files,email_to,email_status,email_content,email_subject,DATE_FORMAT(email_date_send,'%I:%i %p') as time FROM pdpa_email  ORDER  BY id_email DESC",
                (err, pdpa_email) => {
                    res.render("views/view_email/email_inbox", {
                        pdpa_email: pdpa_email,
                        session: req.session
                    });
                });
        });
    } else {
        res.redirect("/");
    }
};


controller.consent = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT id_email,email_files,email_to,email_status,email_content,email_subject,DATE_FORMAT(email_date_send,'%I:%i %p') as time FROM pdpa_email  ORDER  BY id_email DESC",
                (err, pdpa_email) => {
                    res.render("cookie/views/view_email/email_consent", {
                        pdpa_email: pdpa_email,
                        session: req.session

                    });
                });
        });
    } else {
        res.redirect("/");
    }
}


controller.api_consent = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y %I:%i %p') as date_inbox,id_email,email_files,email_to,email_status,email_content,email_subject,DATE_FORMAT(email_date_send,'%I:%i %p') as time FROM pdpa_email  ORDER  BY id_email DESC",
                (err, pdpa_email) => {
                    if (pdpa_email[0]) {
                        res.send(pdpa_email)
                    } else {
                        var data_null = "ไม่มีข้อมูล";
                        res.send(data_null)
                    }
                });
        });
    } else {
        res.redirect("/");
    }
};

// ค้นหาข้อมูล
controller.api_consent_search = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        var search;
        if (data.data.indexOf("-") > 0) {
            var date_formate = ((data.data).replaceAll("-", ",")).split(",");
            search = '%' + date_formate[2] + "-" + date_formate[1] + "-" + date_formate[0] + '%';
            // console.log(date_formate);
        } else {
            search = '%' + data.data + '%';
        }
        req.getConnection((err, conn) => {
            conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y %I:%i %p') as date_inbox,id_email,email_files,email_to,email_status,email_content,email_subject,DATE_FORMAT(email_date_send,'%I:%i %p') as time FROM pdpa_email  WHERE email_subject LIKE ? OR email_to LIKE ? OR email_date_send LIKE ? ORDER  BY id_email DESC", [search, search, search],
                (err, pdpa_email) => {
                    if (pdpa_email[0]) {
                        res.send(pdpa_email)
                    } else {
                        var data_null = "ไม่มีข้อมูล";
                        res.send(data_null);
                    }
                });
        });
    } else {
        res.redirect("/");
    }
};

var host_name;
controller.send = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        var host = req.rawHeaders[17];
        host_name = host;
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM cookiepolicy ORDER BY id_cp DESC",
                (err, cookiepolicy) => {
                    var attachments_new = []; // สร้าง array ขึ้นมาใหม่เพื่อมาเก็บ ชื่อไฟลื กับ path 
                    if (req.files) {
                        var file = req.files.email_file;
                        if (req.files.email_file_csv && req.files.email_file) {  // กรณีที่มีไฟล์  req.files.email_file กับ req.files.email_file_csv
                            // console.log("req.files.email_file_csv && req.files.email_file");

                            var file_csv = req.files.email_file_csv;
                            var file_upload = uuidv4() + "_" + file.name;
                            var file_upload_csv = uuidv4() + "_" + file_csv.name;
                            // console.log("file", file);
                            if (!Array.isArray(file)) {
                                file.mv("./dist/files_upload/" + file_upload, function (err) {
                                    if (err) { console.log("err1", err); }
                                });
                                // console.log("file_upload_csv", file_upload_csv);
                                file_csv.mv("./dist/file_email_csv/" + file_upload_csv, function (err) {
                                    if (err) { console.log(err); }
                                })
                                attachments_new.push({
                                    filename: file.name,
                                    path: './dist/files_upload/' + file_upload
                                })
                                var results = []
                                fs.createReadStream('./dist/file_email_csv/' + file_upload_csv, 'utf8').pipe(csv())
                                    .on('data', (data) => results.push(data))
                                    .on('end', () => {
                                        for (let i = 0; i < results.length; i++) {
                                            conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files_csv=?,email_files=?",
                                                [data.email_content, data.email_subject, results[i].email_to, file_upload_csv, file_upload], (err, insert_email) => {
                                                    // console.log(err);
                                                    var id = insert_email.insertId;
                                                    send_mail_file(results[i].email_to, id, data.email_subject, data.email_content, host);
                                                });
                                        };
                                    });


                            } else {
                                // console.log("if");
                                var array_file;
                                for (var i = 0; i < file.length; i++) {
                                    var file_upload = uuidv4() + "_" + file[i].name;
                                    array_file += file_upload + ",";
                                    file[i].mv("./dist/files_upload/" + file_upload, function (err) {
                                        if (err) { console.log(err); }
                                    })
                                    attachments_new.push({
                                        filename: file[i].name,
                                        path: './dist/files_upload/' + file_upload
                                    })
                                }
                                file_csv.mv("./dist/file_email_csv/" + file_upload_csv, function (err) {
                                    if (err) { console.log(err); }
                                });
                                var results = []
                                fs.createReadStream('./dist/file_email_csv/' + file_upload_csv, 'utf8').pipe(csv())
                                    .on('data', (data) => results.push(data))
                                    .on('end', () => {
                                        for (let i = 0; i < results.length; i++) {
                                            conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files_csv=?,email_files=?",
                                                [data.email_content, data.email_subject, results[i].email_to, file_upload_csv, array_file], (err, insert_email) => {
                                                    var id = insert_email.insertId;
                                                    send_mail_file(results[i].email_to, id, data.email_subject, data.email_content, host);
                                                });
                                        };
                                    });
                            }
                            function send_mail_file(email, id, email_subject, email_content, host) {
                                // console.log(id);
                                var mail = nodemailer.createTransport({
                                    service: 'gmail',
                                    auth: {
                                        user: 'email.consent.18@gmail.com',
                                        pass: 'Passw0rd18'
                                    }
                                })

                                var mailOptions = {
                                    from: 'email.consent.18@gmail.com',
                                    to: email,
                                    subject: email_subject,
                                    text: email_content,
                                    html: `
                                    <div class="mt-4 " style="margin-left: 1%;">
                                            <h3>
                                                หนังสือให้ความยินยอมในการเก็บรวบรวม /ใช้/เปิดเผยข้อมลสู่วนบคคล
                                            </h3>
                                            <hr>
                                                <h5 style="text-align: left;">
                                                    บริษัท PDPA ให้ความสำคัญกับความเป็นส่วนตัวของท่านบริษัทจึงขอความยินยอมจากท่านเพื่อการเก็บรวบรวม
                                                    ใช้หรือเปิดเผยข้อมูลส่วนบุคคลของท่านที่ให้ใว้เเก่บริษัทหรือที่บริษัทได้รับมาจากเเหล่งอื่น
                                                </h5>
                                                <hr>
                                                    <form id="form">
                                                        <!-- <div class="form-check ">
                                                            <input class="form-check-input" type="checkbox" value="18" id="flexCheckDefault-cookieType"
                                                                name="respond_email">
                                                                <label class="form-check-label" for="flexCheckDefault-cookieType">
                                                                    ฉันยอมรับข้อมูล
                                                                </label>
                                                        </div> -->
                                                        <div class="col-12">
                                                            <a href="${host}/agree-email/${id}" class="btn btn-primary">ยืนยัน</a>
                                                            <a href="${host}/notagree-email_csv/${id}" id="not_agree" class="btn btn-danger">ไม่ยืนยัน</a>
                                                        </div>
                                                    </form>
                                                </div>
                                                     `,
                                    attachments: [],

                                }

                                for (let i = 0; i < attachments_new.length; i++) {
                                    mailOptions.attachments.push(attachments_new[i])
                                }
                                // console.log(mailOptions);

                                mail.sendMail(mailOptions, function (error, info) {
                                    // if (error) {
                                    //     console.log(error);
                                    // } else {
                                    //     console.log('Email sent: ' + info.response);
                                    // }
                                });
                            }
                        } else { // กรณีที่มีเเค่ไฟล์  req.files.email_file

                            if (req.files.email_file_csv) {
                                var file_csv = req.files.email_file_csv;
                                var file_upload_csv = uuidv4() + "_" + file_csv.name;
                                file_csv.mv("./dist/file_email_csv/" + file_upload_csv, function (err) {
                                    if (err) { console.log(err); }
                                })
                                var results = []
                                // console.log(file_upload_csv);
                                fs.createReadStream('./dist/file_email_csv/' + file_upload_csv, 'utf8').pipe(csv())
                                    .on('data', (data) => results.push(data))
                                    .on('end', () => {
                                        for (let i = 0; i < results.length; i++) {
                                            conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files_csv=?",
                                                [data.email_content, data.email_subject, results[i].email_to, file_upload_csv], (err, insert_email) => {
                                                    var id = insert_email.insertId;
                                                    send_mail(results[i].email_to, id, data.email_subject, data.email_content, host);
                                                });
                                        };
                                    });

                            } else {
                                // console.log("else");
                                var file_upload = uuidv4() + "_" + file.name;
                                if (!Array.isArray(file)) { // กรณีที่ไฟล์  req.files.email_file  ส่งมาไฟล์เดียว
                                    // console.log("ifififififififififififif");
                                    file.mv("./dist/files_upload/" + file_upload, function (err) {
                                        if (err) { console.log(err); }
                                    })
                                    conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files=?,email_files_csv=?",
                                        [data.email_content, data.email_subject, data.email_to, file_upload, file_upload_csv], (err, insert_email) => {
                                            // console.log(insert_email);
                                            send_mail_file_first(insert_email.insertId)
                                        })

                                    attachments_new.push({
                                        filename: file.name,
                                        path: './dist/files_upload/' + file_upload
                                    })
                                } else {  // กรณีที่ไฟล์  req.files.email_file  ส่งมาหลายไฟล์          
                                    // console.log("else if");
                                    var array_file;
                                    for (var i = 0; i < file.length; i++) {
                                        var file_upload = uuidv4() + "_" + file[i].name;
                                        array_file += file_upload + ",";
                                        file[i].mv("./dist/files_upload/" + file_upload, function (err) {
                                            if (err) { console.log(err); }
                                        });
                                        attachments_new.push({
                                            filename: file[i].name,
                                            path: './dist/files_upload/' + file_upload
                                        })
                                    }
                                    // console.log("array_file", array_file);
                                    conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files=?",
                                        [data.email_content, data.email_subject, data.email_to, array_file], (err, insert_email) => {
                                            send_mail_file_first(insert_email.insertId)
                                        });

                                }

                                function send_mail_file_first(id) {
                                    var mail = nodemailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: 'email.consent.18@gmail.com',
                                            pass: 'Passw0rd18'
                                        }
                                    })
                                    // console.log("id_mail", id_mail);
                                    var mailOptions = {
                                        from: 'email.consent.18@gmail.com',
                                        to: data.email_to,
                                        subject: data.email_subject,
                                        text: data.email_content,
                                        html: `
                                        <div class="mt-4 " style="margin-left: 1%;">
                                                <h3>
                                                    หนังสือให้ความยินยอมในการเก็บรวบรวม /ใช้/เปิดเผยข้อมลสู่วนบคคล
                                                </h3>
                                                <hr>
                                                    <h5 style="text-align: left;">
                                                        บริษัท PDPA ให้ความสำคัญกับความเป็นส่วนตัวของท่านบริษัทจึงขอความยินยอมจากท่านเพื่อการเก็บรวบรวม
                                                        ใช้หรือเปิดเผยข้อมูลส่วนบุคคลของท่านที่ให้ใว้เเก่บริษัทหรือที่บริษัทได้รับมาจากเเหล่งอื่น
                                                    </h5>
                                                    <hr>
                                                        <form id="form">
                                                            <!-- <div class="form-check ">
                                                                <input class="form-check-input" type="checkbox" value="18" id="flexCheckDefault-cookieType"
                                                                    name="respond_email">
                                                                    <label class="form-check-label" for="flexCheckDefault-cookieType">
                                                                        ฉันยอมรับข้อมูล
                                                                    </label>
                                                            </div> -->
                                                            <div class="col-12">
                                                                <a href="${host}/agree-email/${id}" class="btn btn-primary">ยืนยัน</a>
                                                                <a href="${host}/notagree-email_csv/${id}" id="not_agree" class="btn btn-danger">ไม่ยืนยัน</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                              `,
                                        attachments: []
                                    }

                                    for (let i = 0; i < attachments_new.length; i++) {
                                        mailOptions.attachments.push(attachments_new[i])
                                    }

                                    mail.sendMail(mailOptions, function (error, info) {
                                        // if (error) {
                                        //     console.log(error);
                                        // } else {
                                        //     console.log('Email sent: ' + info.response);
                                        // }
                                    });
                                }


                            }
                        }

                    } else {
                        console.log("else");
                        conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files=?",
                            [data.email_content, data.email_subject, data.email_to, file_upload], (err, insert_email) => { })
                        // var text_html = 'เพื่อให้เรานำเสนอประสบณ์ที่ดีเเละตรงใจคุณที่สุดให้เรารู้จักคุณมากขึ้นด้วยการส่งอีเมลเเละเช็กกล่องด้านล่างนี้หากท่านไม่ประสงค์จะรับการติดต่อ โปรดกด "ยืนยัน" โดยไม่เช็กกล่องด้านล่างนี้';
                        // var email = data.email_to;

                        conn.query("SELECT * FROM  pdpa_email ORDER BY id_email DESC ", (err, selecte_email) => {

                            var id = selecte_email[0].id_email;
                            var html_send = `

                                                        <div class="mt-4 " style="margin-left: 1%;">
                                                            <!-- <img src="https://media.geeksforgeeks.org/wp-content/cdn-uploads/20210203170945/HTML-Tutorials.png" width="30%"> -->
                                                                <!-- <h1>Test Send E-mail</h1> -->
                                                                <h3>
                                                                    หนังสือให้ความยินยอมในการเก็บรวบรวม /ใช้/เปิดเผยข้อมลสู่วนบคคล
                                                                </h3>
                                                                <hr>
                                                                    <h5 style="text-align: left;">
                                                                        บริษัท PDPA ให้ความสำคัญกับความเป็นส่วนตัวของท่านบริษัทจึงขอความยินยอมจากท่านเพื่อการเก็บรวบรวม
                                                                        ใช้หรือเปิดเผยข้อมูลส่วนบุคคลของท่านที่ให้ใว้เเก่บริษัทหรือที่บริษัทได้รับมาจากเเหล่งอื่น
                                                                    </h5>

                                                                    <!-- <h5>
                                                                        อีเมล : haam2543wittaya@gmail.com
                                                                    </h5> -->
                                                                    <hr>
                                                                        <form id="form">
                                                                            <!-- <div class="form-check ">
                                                                                <input class="form-check-input" type="checkbox" value="18" id="flexCheckDefault-cookieType"
                                                                                    name="respond_email">
                                                                                    <label class="form-check-label" for="flexCheckDefault-cookieType">
                                                                                        ฉันยอมรับข้อมูล
                                                                                    </label>
                                                                            </div> -->
                                                                            <div class="col-12">
                                                                                <a href="${host}/agree-email/${id}" class="btn btn-primary">ยืนยัน</a>
                                                                                <a href="${host}/notagree-email" id="not_agree" class="btn btn-danger">ไม่ยืนยัน</a>
                                                                            </div>
                                                                        </form>
                                                                    </div>


                             `;
                            var mail = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: 'email.consent.18@gmail.com',
                                    pass: 'Passw0rd18'
                                }
                            });
                            var mailOptions = {
                                from: 'email.consent.18@gmail.com',
                                to: data.email_to,
                                subject: data.email_subject,
                                text: data.email_content,
                                html: html_send,
                            }
                            // console.log(mailOptions);
                            mail.sendMail(mailOptions, function (error, info) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log('Email sent: ' + info.response);
                                    console.log("upload success");

                                }
                            });
                        });
                    }
                })
            res.redirect("/management/email_consent");
        });
    } else {
        res.redirect("/");
    }
};


// controller.send = (req, res) => {
//     if (req.session.userid) {
//         const data = req.body;
//         var host = req.rawHeaders[17];
//         host_name = host;
//         req.getConnection((err, conn) => {
//             conn.query("SELECT * FROM cookiepolicy ORDER BY id_cp DESC",
//                 (err, cookiepolicy) => {
//                     var attachments_new = []; // สร้าง array ขึ้นมาใหม่เพื่อมาเก็บ ชื่อไฟลื กับ path 
//                     if (req.files) {
//                         var file = req.files.email_file;
//                         var email_csv_to = [];
//                         if (req.files.email_file_csv && req.files.email_file) {  // กรณีที่มีไฟล์  req.files.email_file กับ req.files.email_file_csv
//                             // console.log("req.files.email_file_csv && req.files.email_file");

//                             var file_csv = req.files.email_file_csv;
//                             var file_upload = uuidv4() + "_" + file.name;
//                             var file_upload_csv = uuidv4() + "_" + file_csv.name;
//                             // console.log("file", file);
//                             if (!Array.isArray(file)) {
//                                 // console.log("if");
//                                 file.mv("./dist/files_upload/" + file_upload, function (err) {
//                                     if (err) { console.log(err); }
//                                 });
//                                 // console.log("file_upload_csv", file_upload_csv);
//                                 file_csv.mv("./dist/file_email_csv/" + file_upload_csv, function (err) {
//                                     if (err) { console.log(err); }
//                                 });
//                                 conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files=?,email_files_csv=?",
//                                     [data.email_content, data.email_subject, data.email_to, file_upload, file_upload_csv], (err, insert_email) => { })

//                                 attachments_new.push({
//                                     filename: file.name,
//                                     path: './dist/files_upload/' + file_upload
//                                 })
//                                 var results = []
//                                 fs.createReadStream('./dist/file_email_csv/' + file_upload_csv, 'utf8').pipe(csv())
//                                     .pipe(csv())
//                                     .on('data', (data) => results.push(data))
//                                     .on('end', () => {
//                                         for (var i = 0; i < results.length; i++) {
//                                             email_csv_to.push(results[i].to);
//                                         }
//                                     });
//                             } else {
//                                 var array_file;
//                                 for (var i = 0; i < file.length; i++) {
//                                     var file_upload = uuidv4() + "_" + file[i].name;
//                                     array_file += file_upload + ",";
//                                     file[i].mv("./dist/files_upload/" + file_upload, function (err) {
//                                         if (err) { console.log(err); }
//                                     })
//                                     attachments_new.push({
//                                         filename: file[i].name,
//                                         path: './dist/files_upload/' + file_upload
//                                     })
//                                 }
//                                 file_csv.mv("./dist/file_email_csv/" + file_upload_csv, function (err) {
//                                     if (err) { console.log(err); }
//                                 });
//                                 var results = []
//                                 fs.createReadStream('./dist/file_email_csv/' + file_upload_csv, 'utf8').pipe(csv())
//                                     .on('data', (data) => results.push(data))
//                                     .on('end', () => {
//                                         for (let i = 0; i < results.length; i++) {
//                                             conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files_csv=?,email_files=?",
//                                                 [data.email_content, data.email_subject, results[i].to, file_upload_csv, array_file], (err, insert_email) => {
//                                                     var id = insert_email.insertId;
//                                                     send_mail_file(results[i].to, id, data.email_subject, data.email_content, host);
//                                                 });
//                                         };
//                                     });
//                             }
//                             function send_mail_file(email, id, email_subject, email_content, host) {
//                                 // console.log(id);
//                                 var mail = nodemailer.createTransport({
//                                     service: 'gmail',
//                                     auth: {
//                                         user: 'email.consent.18@gmail.com',
//                                         pass: 'Passw0rd18'
//                                     }
//                                 })

//                                 var mailOptions = {
//                                     from: 'email.consent.18@gmail.com',
//                                     to: email,
//                                     subject: email_subject,
//                                     text: email_content,
//                                     html: `
//                                     <div class="mt-4 " style="margin-left: 1%;">
//                                             <h3>
//                                                 หนังสือให้ความยินยอมในการเก็บรวบรวม /ใช้/เปิดเผยข้อมลสู่วนบคคล
//                                             </h3>
//                                             <hr>
//                                                 <h5 style="text-align: left;">
//                                                     บริษัท PDPA ให้ความสำคัญกับความเป็นส่วนตัวของท่านบริษัทจึงขอความยินยอมจากท่านเพื่อการเก็บรวบรวม
//                                                     ใช้หรือเปิดเผยข้อมูลส่วนบุคคลของท่านที่ให้ใว้เเก่บริษัทหรือที่บริษัทได้รับมาจากเเหล่งอื่น
//                                                 </h5>
//                                                 <hr>
//                                                     <form id="form">
//                                                         <!-- <div class="form-check ">
//                                                             <input class="form-check-input" type="checkbox" value="18" id="flexCheckDefault-cookieType"
//                                                                 name="respond_email">
//                                                                 <label class="form-check-label" for="flexCheckDefault-cookieType">
//                                                                     ฉันยอมรับข้อมูล
//                                                                 </label>
//                                                         </div> -->
//                                                         <div class="col-12">
//                                                             <a href="${host}/agree-email/${id}" class="btn btn-primary">ยืนยัน</a>
//                                                             <a href="${host}/notagree-email_csv/${id}" id="not_agree" class="btn btn-danger">ไม่ยืนยัน</a>
//                                                         </div>
//                                                     </form>
//                                                 </div>
//                                                      `,
//                                     attachments: [],

//                                 }

//                                 for (let i = 0; i < attachments_new.length; i++) {
//                                     mailOptions.attachments.push(attachments_new[i])
//                                 }
//                                 // console.log(mailOptions);

//                                 mail.sendMail(mailOptions, function (error, info) {
//                                     // if (error) {
//                                     //     console.log(error);
//                                     // } else {
//                                     //     console.log('Email sent: ' + info.response);
//                                     // }
//                                 });
//                             }
//                         } else { // กรณีที่มีเเค่ไฟล์  req.files.email_file

//                             if (req.files.email_file_csv) {
//                                 var file_csv = req.files.email_file_csv;
//                                 var file_upload_csv = uuidv4() + "_" + file_csv.name;
//                                 file_csv.mv("./dist/file_email_csv/" + file_upload_csv, function (err) {
//                                     if (err) { console.log(err); }
//                                 })
//                                 var results = []
//                                 console.log(file_upload_csv);
//                                 fs.createReadStream('./dist/file_email_csv/' + file_upload_csv, 'utf8').pipe(csv())
//                                     .on('data', (data) => results.push(data))
//                                     .on('end', () => {
//                                         for (let i = 0; i < results.length; i++) {
//                                             conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files_csv=?",
//                                                 [data.email_content, data.email_subject, results[i].to, file_upload_csv], (err, insert_email) => {
//                                                     var id = insert_email.insertId;
//                                                     send_mail(results[i].to, id, data.email_subject, data.email_content, host);
//                                                 });
//                                         };
//                                     });

//                             } else {
//                                 // console.log("else");
//                                 var file_upload = uuidv4() + "_" + file.name;
//                                 if (!Array.isArray(file)) { // กรณีที่ไฟล์  req.files.email_file  ส่งมาไฟล์เดียว
//                                     // console.log("ifififififififififififif");
//                                     file.mv("./dist/files_upload/" + file_upload, function (err) {
//                                         if (err) { console.log(err); }
//                                     })
//                                     conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files=?,email_files_csv=?",
//                                         [data.email_content, data.email_subject, data.email_to, file_upload, file_upload_csv], (err, insert_email) => {
//                                             console.log(insert_email);
//                                             send_mail_file_first(insert_email.insertId)
//                                         })

//                                     attachments_new.push({
//                                         filename: file.name,
//                                         path: './dist/files_upload/' + file_upload
//                                     })
//                                 } else {  // กรณีที่ไฟล์  req.files.email_file  ส่งมาหลายไฟล์          
//                                     // console.log("else if");
//                                     var array_file;
//                                     for (var i = 0; i < file.length; i++) {
//                                         var file_upload = uuidv4() + "_" + file[i].name;
//                                         array_file += file_upload + ",";
//                                         file[i].mv("./dist/files_upload/" + file_upload, function (err) {
//                                             if (err) { console.log(err); }
//                                         });
//                                         attachments_new.push({
//                                             filename: file[i].name,
//                                             path: './dist/files_upload/' + file_upload
//                                         })
//                                     }
//                                     console.log("array_file", array_file);
//                                     conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files=?",
//                                         [data.email_content, data.email_subject, data.email_to, array_file], (err, insert_email) => {
//                                             send_mail_file_first(insert_email.insertId)
//                                         });

//                                 }

//                                 function send_mail_file_first(id) {
//                                     var mail = nodemailer.createTransport({
//                                         service: 'gmail',
//                                         auth: {
//                                             user: 'email.consent.18@gmail.com',
//                                             pass: 'Passw0rd18'
//                                         }
//                                     })
//                                     // console.log("id_mail", id_mail);
//                                     var mailOptions = {
//                                         from: 'email.consent.18@gmail.com',
//                                         to: data.email_to,
//                                         subject: data.email_subject,
//                                         text: data.email_content,
//                                         html: `
//                                         <div class="mt-4 " style="margin-left: 1%;">
//                                                 <h3>
//                                                     หนังสือให้ความยินยอมในการเก็บรวบรวม /ใช้/เปิดเผยข้อมลสู่วนบคคล
//                                                 </h3>
//                                                 <hr>
//                                                     <h5 style="text-align: left;">
//                                                         บริษัท PDPA ให้ความสำคัญกับความเป็นส่วนตัวของท่านบริษัทจึงขอความยินยอมจากท่านเพื่อการเก็บรวบรวม
//                                                         ใช้หรือเปิดเผยข้อมูลส่วนบุคคลของท่านที่ให้ใว้เเก่บริษัทหรือที่บริษัทได้รับมาจากเเหล่งอื่น
//                                                     </h5>
//                                                     <hr>
//                                                         <form id="form">
//                                                             <!-- <div class="form-check ">
//                                                                 <input class="form-check-input" type="checkbox" value="18" id="flexCheckDefault-cookieType"
//                                                                     name="respond_email">
//                                                                     <label class="form-check-label" for="flexCheckDefault-cookieType">
//                                                                         ฉันยอมรับข้อมูล
//                                                                     </label>
//                                                             </div> -->
//                                                             <div class="col-12">
//                                                                 <a href="${host}/agree-email/${id}" class="btn btn-primary">ยืนยัน</a>
//                                                                 <a href="${host}/notagree-email_csv/${id}" id="not_agree" class="btn btn-danger">ไม่ยืนยัน</a>
//                                                             </div>
//                                                         </form>
//                                                     </div>
//                                               `,
//                                         attachments: []
//                                     }

//                                     for (let i = 0; i < attachments_new.length; i++) {
//                                         mailOptions.attachments.push(attachments_new[i])
//                                     }

//                                     mail.sendMail(mailOptions, function (error, info) {
//                                         if (error) {
//                                             console.log(error);
//                                         } else {
//                                             console.log('Email sent: ' + info.response);
//                                         }
//                                     });
//                                 }


//                             }
//                         }

//                     } else {
//                         console.log("else");
//                         conn.query("INSERT INTO pdpa_email SET email_status=0,email_content=?,email_subject=?,email_to=?,email_files=?",
//                             [data.email_content, data.email_subject, data.email_to, file_upload], (err, insert_email) => { })
//                         // var text_html = 'เพื่อให้เรานำเสนอประสบณ์ที่ดีเเละตรงใจคุณที่สุดให้เรารู้จักคุณมากขึ้นด้วยการส่งอีเมลเเละเช็กกล่องด้านล่างนี้หากท่านไม่ประสงค์จะรับการติดต่อ โปรดกด "ยืนยัน" โดยไม่เช็กกล่องด้านล่างนี้';
//                         // var email = data.email_to;

//                         conn.query("SELECT * FROM  pdpa_email ORDER BY id_email DESC ", (err, selecte_email) => {

//                             var id = selecte_email[0].id_email;
//                             var html_send = `

//                                                         <div class="mt-4 " style="margin-left: 1%;">
//                                                             <!-- <img src="https://media.geeksforgeeks.org/wp-content/cdn-uploads/20210203170945/HTML-Tutorials.png" width="30%"> -->
//                                                                 <!-- <h1>Test Send E-mail</h1> -->
//                                                                 <h3>
//                                                                     หนังสือให้ความยินยอมในการเก็บรวบรวม /ใช้/เปิดเผยข้อมลสู่วนบคคล
//                                                                 </h3>
//                                                                 <hr>
//                                                                     <h5 style="text-align: left;">
//                                                                         บริษัท PDPA ให้ความสำคัญกับความเป็นส่วนตัวของท่านบริษัทจึงขอความยินยอมจากท่านเพื่อการเก็บรวบรวม
//                                                                         ใช้หรือเปิดเผยข้อมูลส่วนบุคคลของท่านที่ให้ใว้เเก่บริษัทหรือที่บริษัทได้รับมาจากเเหล่งอื่น
//                                                                     </h5>

//                                                                     <!-- <h5>
//                                                                         อีเมล : haam2543wittaya@gmail.com
//                                                                     </h5> -->
//                                                                     <hr>
//                                                                         <form id="form">
//                                                                             <!-- <div class="form-check ">
//                                                                                 <input class="form-check-input" type="checkbox" value="18" id="flexCheckDefault-cookieType"
//                                                                                     name="respond_email">
//                                                                                     <label class="form-check-label" for="flexCheckDefault-cookieType">
//                                                                                         ฉันยอมรับข้อมูล
//                                                                                     </label>
//                                                                             </div> -->
//                                                                             <div class="col-12">
//                                                                                 <a href="${host}/agree-email/${id}" class="btn btn-primary">ยืนยัน</a>
//                                                                                 <a href="${host}/notagree-email" id="not_agree" class="btn btn-danger">ไม่ยืนยัน</a>
//                                                                             </div>
//                                                                         </form>
//                                                                     </div>


//                              `;
//                             var mail = nodemailer.createTransport({
//                                 service: 'gmail',
//                                 auth: {
//                                     user: 'email.consent.18@gmail.com',
//                                     pass: 'Passw0rd18'
//                                 }
//                             });
//                             var mailOptions = {
//                                 from: 'email.consent.18@gmail.com',
//                                 to: data.email_to,
//                                 subject: data.email_subject,
//                                 text: data.email_content,
//                                 html: html_send,
//                             }
//                             // console.log(mailOptions);
//                             mail.sendMail(mailOptions, function (error, info) {
//                                 if (error) {
//                                     console.log(error);
//                                 } else {
//                                     console.log('Email sent: ' + info.response);
//                                     console.log("upload success");

//                                 }
//                             });
//                         });
//                     }
//                 })
//             res.redirect("/management/email_consent");
//         });
//     } else {
//         res.redirect("/");
//     }
// };




function send_mail(data_email, data_html, subject, content, host) {
    var mail = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'email.consent.18@gmail.com',
            pass: 'Passw0rd18'
        }
    });
    var mailOptions = {
        from: 'email.consent.18@gmail.com',
        to: data_email,
        subject: subject,
        text: content,
        html: `
        <div class="mt-4 " style="margin-left: 1%;">
                <h3>
                    หนังสือให้ความยินยอมในการเก็บรวบรวม /ใช้/เปิดเผยข้อมลสู่วนบคคล
                </h3>
                <hr>
                    <h5 style="text-align: left;">
                        บริษัท PDPA ให้ความสำคัญกับความเป็นส่วนตัวของท่านบริษัทจึงขอความยินยอมจากท่านเพื่อการเก็บรวบรวม
                        ใช้หรือเปิดเผยข้อมูลส่วนบุคคลของท่านที่ให้ใว้เเก่บริษัทหรือที่บริษัทได้รับมาจากเเหล่งอื่น
                    </h5>
                    <hr>
                        <form id="form">
                            <!-- <div class="form-check ">
                                <input class="form-check-input" type="checkbox" value="18" id="flexCheckDefault-cookieType"
                                    name="respond_email">
                                    <label class="form-check-label" for="flexCheckDefault-cookieType">
                                        ฉันยอมรับข้อมูล
                                    </label>
                            </div> -->
                            <div class="col-12">
                                <a href="${host}/agree-email/${data_html}" class="btn btn-primary">ยืนยัน</a>
                                <a href="${host}/notagree-email_csv/${data_html}" id="not_agree" class="btn btn-danger">ไม่ยืนยัน</a>
                            </div>
                        </form>
                    </div>
              `,
    };
    mail.sendMail(mailOptions, function (error, info) {
        // if (error) {
        //     console.log(error);
        // } else {
        //     console.log('Email sent: ' + info.response);
        // }
    });
};


controller.agree = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query("UPDATE pdpa_email SET  email_status=1,email_date_consent=NOW() WHERE  id_email=? ", [id],
            (err, update_pdpa_email) => {
                res.render("cookie/views/view_email/email_agree", {
                    session: req.session
                });
            });
    });
};


controller.not_agree = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM  pdpa_email ORDER BY id_email DESC",
            (err, select_pdpa_email) => {
                var id_email = select_pdpa_email[0].id_email;
                res.render("cookie/views/view_email/form_donot_agree", {
                    id_email,
                    host_name,
                    session: req.session
                });
            });
    });
};


controller.not_agree_csv = (req, res) => {
    var host = req.rawHeaders[17];
    req.getConnection((err, conn) => {
        const id = req.params.id;
        conn.query("SELECT * FROM  pdpa_email WHERE id_email=?", [id],
            (err, select_pdpa_email) => {
                var id_email = select_pdpa_email[0].id_email;
                res.render("cookie/views/view_email/form_donot_agree", {
                    id_email, host_name,
                    session: req.session
                });
            });
    });
};


controller.resend_not_agree = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM  pdpa_email WHERE id_email=? ", [id],
            (err, select_pdpa_email) => {
                var id_email = select_pdpa_email[0].id_email;
                res.render("cookie/views/view_email/form_donot_agree", {
                    id_email, host_name,
                    session: req.session
                });
            });
    });
};


controller.Dont_agree = (req, res) => {
    const { id } = req.params;
    // console.log("id " + id);
    req.getConnection((err, conn) => {
        conn.query("UPDATE pdpa_email SET  email_status=2,email_date_consent=NOW() WHERE  id_email=? ", [id],
            (err, update_pdpa_email) => {
                res.render("cookie/views/view_email/email_notAgree", {
                    session: req.session
                });
            });
    });
};

controller.resend = (req, res) => {
    const { id } = req.params;
    var host = (req.rawHeaders[25]).replace("/management/email_consent", "");
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM pdpa_email WHERE id_email=?", [id],
            (err, select_pdpa_email) => {
                // console.log(select_pdpa_email);
                var mail = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'email.consent.18@gmail.com',
                        pass: 'Passw0rd18'
                    }
                })
                var html_send = `
                <div class="mt-4 " style="margin-left: 1%;">
                    <!-- <img src="https://media.geeksforgeeks.org/wp-content/cdn-uploads/20210203170945/HTML-Tutorials.png" width="30%"> -->
                    <!-- <h1>Test Send E-mail</h1> -->
                    <h3>
                        หนังสือให้ความยินยอมในการเก็บรวบรวม /ใช้/เปิดเผยข้อมลสู่วนบคคล
                    </h3>
                        <hr>
                    <h5 style="text-align: left;">
                        บริษัท PDPA ให้ความสำคัญกับความเป็นส่วนตัวของท่านบริษัทจึงขอความยินยอมจากท่านเพื่อการเก็บรวบรวม
                        ใช้หรือเปิดเผยข้อมูลส่วนบุคคลของท่านที่ให้ใว้เเก่บริษัทหรือที่บริษัทได้รับมาจากเเหล่งอื่น
                    </h5>

                    <!-- <h5>
                        อีเมล : haam2543wittaya@gmail.com
                    </h5> -->
                        <hr>
                    <form id="form">
                        <!-- <div class="form-check ">
                            <input class="form-check-input" type="checkbox" value="18" id="flexCheckDefault-cookieType"
                                name="respond_email">
                            <label class="form-check-label" for="flexCheckDefault-cookieType">
                                ฉันยอมรับข้อมูล
                            </label>
                        </div> -->
                        <div class="col-12">
                            <a href="${host}/agree-email/${select_pdpa_email[0].id_email}" class="btn btn-primary">ยืนยัน</a>
                            <a href="${host}/resend-notagree-email/${select_pdpa_email[0].id_email}" id="not_agree" class="btn btn-danger">ไม่ยืนยัน</a>
                        </div>
                    </form>
                </div>


                                        `
                var mailOptions = {
                    from: 'email.consent.18@gmail.com',
                    to: select_pdpa_email[0].email_to,
                    subject: select_pdpa_email[0].email_subject,
                    text: select_pdpa_email[0].email_content,
                    html: html_send
                }
                mail.sendMail(mailOptions, function (error, info) {
                    // if (error) {
                    //     console.log(error);
                    // } else {
                    //     console.log('Email sent: ' + info.response);
                    //     console.log("upload success");
                    // }
                });
            });
        res.redirect("/management/email_consent");
    });
};

module.exports = controller;
