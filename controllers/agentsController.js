const controller = {}
const fs = require('fs')
const path = require('path')
const { validationResult } = require('express-validator')

// Index Manage
controller.manager = (req, res) => {
        if (req.session.userid == "undefined") {
            res.redirect('/')
        } else {
            const user = req.session.userid
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from doc_pdpa_document as d  join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        let word = []
                        let word1 = []
                        for (i in words) {
                            word.push(words[i].words_id)
                            word1.push(words[i].words_often)
                        }
                        if (err) {
                            res.json(err)
                        }
                        if (err) { res.json(err) } else {
                            res.render('./agent/manager_config', {
                                history: history,
                                words: words,
                                words1: word,
                                words2: word1,
                                session: req.session
                            })
                        }
                    })
                })
            })
        }
    }
    // Add manage
controller.addManage = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            const body = req.body
            req.getConnection((err, conn) => {
                conn.query('INSERT INTO doc_pdpa_agent_manage SET ?', [body], (err, pass) => {
                    if (err) { res.json(err) } else {
                        res.redirect('back')
                    }
                })
            })
        }
    }
    // Send AJAX (POST)
controller.manager1 = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT agent_manage_id FROM doc_pdpa_agent_manage', (err, id_manage) => {
                conn.query('SELECT * FROM doc_pdpa_agent_manage as pam JOIN doc_pdpa_agent_type as pat ON pam.agent_type_id = pat.agent_type_id JOIN doc_pdpa_agent_type_name as patn ON pat.agent_type_name_id = patn.agent_type_name_id JOIN account as a ON pam.acc_id = a.acc_id;', (err, manage) => {
                    if (manage.length > 0) {
                        for (i in manage) {
                            manage[i].agent_manage_id = (parseInt(i) + 1)
                        }
                    }
                    if (err) { res.json(err) } else {
                        res.json({ id_manage, manage })
                    }
                })
            })
        })
    }
}
controller.selectAgentType = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const id = req.body.id
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM doc_pdpa_agent_type as pat JOIN doc_pdpa_agent_type_name as patn ON pat.agent_type_name_id = patn.agent_type_name_id WHERE agent_type_id = ?', [id], (err, select) => {
                if (err) { res.json(err) } else {
                    res.json(select)
                }
            })
        })
    }
}
controller.file_log_ag1 = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const dir = path.normalize('/Users/kuzan04/Documents/workSpace/project_sgc/file_server/');
        const dir_length = fs.readdirSync(dir).length;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM doc_pdpa_agent_file_dir', (err, dir_device) => {
                if (err) {
                    res.json(err)
                } else {
                    _path_ = fs.readdirSync(dir, 'utf8')
                    _path_.sort(function(a, b) {
                        return fs.statSync(dir + a).mtime.getTime() -
                            fs.statSync(dir + b).mtime.getTime();
                    });
                    obj = []
                    for (var i = 0; i < _path_.length; i++) {
                        obj.push({ "no": (i + 1), "name": _path_[i], "extension": path.extname(dir + _path_[i]), "size": fs.statSync(dir + _path_[i]).size, "date": fs.statSync(dir + _path_[i]).mtime })
                            // in dir file_server
                            // for (j in _path_[i]) {
                            //     obj.push({ "name": _path_[i], "extension": path.extname(dir + _path_[i]), "size": fs.statSync(dir + _path_[i]).size, "date": fs.statSync(dir + _path_[i]).mtime })
                            // }
                    }
                    res.json({
                        dir: dir_length,
                        dir_device: dir_device,
                        files: obj,
                    });
                }
            })
        });
    }
}
controller.file_log_ag1_detail = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        if (res.redirect('/'));
    } else {
        const id = req.body.id
        const dir = path.normalize('/Users/kuzan04/Documents/workSpace/project_sgc/file_server/');
        const dir_length = fs.readdirSync(dir).length;
        _path_ = fs.readdirSync(dir, 'utf8')
        _path_.sort(function(a, b) {
            return fs.statSync(dir + a).mtime.getTime() -
                fs.statSync(dir + b).mtime.getTime();
        });
        obj = []
        for (var i = 0; i < _path_.length; i++) {
            obj.push({ "no": (i + 1), "name": _path_[i], "extension": path.extname(dir + _path_[i]), "size": fs.statSync(dir + _path_[i]).size, "date": fs.statSync(dir + _path_[i]).mtime })
                // in dir file_server
                // for (j in _path_[i]) {
                //     obj.push({ "name": _path_[i], "extension": path.extname(dir + _path_[i]), "size": fs.statSync(dir + _path_[i]).size, "date": fs.statSync(dir + _path_[i]).mtime })
                // }
        }
        res.json(obj[parseInt(id) - 1])
    }
}
controller.database_ag1 = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM doc_pdpa_agent_database_check', (err, database) => {
                conn.query('SELECT id FROM doc_pdpa_agent_database_check', (err, id_database) => {
                    if (err) {
                        res.json(err)
                    } else {
                        for (i in database) {
                            database[i].id = parseInt(i) + 1
                        }
                        res.json({
                            data: database,
                            id_data: id_database,
                        });
                    }
                })
            })
        });
    }
}
controller.logger_ag1 = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/');
        } else {
            req.getConnection((err, conn) => {
                conn.query('SELECT id, device_name,os_name,path,name_file,total_line,DATE_FORMAT(date_now, "%d/%m/%Y %H:%i") as date_now, value FROM doc_pdpa_agent_log0_hash', (err, hash) => {
                    conn.query('SELECT id FROM doc_pdpa_agent_log0_hash', (err, hash_id) => {
                        conn.query('SELECT path, count(path) as cpath FROM doc_pdpa_agent_log0_hash GROUP BY path', (err, path) => {
                            if (err) {
                                res.json(err)
                            } else {
                                for (i in hash) {
                                    hash[i].id = parseInt(i) + 1
                                }
                                res.json({
                                    hash: hash,
                                    // id_hash:hash_id,
                                    path: path,
                                });
                            }
                        })
                    })
                })
            });
        }
    }
    // File/Log 
controller.file_log_ag = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/');
        } else {
            const user = req.session.userid;
            const dir = path.normalize('/Users/kuzan04/Documents/workSpace/project_sgc/file_server/');
            const dir_all_length = fs.readdirSync(dir).length;
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from doc_pdpa_document as d join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query('SELECT * FROM doc_pdpa_agent_file_dir', (err, dir_device) => {
                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            if (err) {
                                res.json(err)
                            } else {
                                _path_ = fs.readdirSync(dir, 'utf8')
                                _path_.sort(function(a, b) {
                                    return fs.statSync(dir + a).mtime.getTime() -
                                        fs.statSync(dir + b).mtime.getTime();
                                });
                                obj = []
                                for (var i = 0; i < _path_.length; i++) {
                                    obj.push({ id: (i + 1), "name": _path_[i], "extension": path.extname(dir + _path_[i]), "size": fs.statSync(dir + _path_[i]).size, "date": fs.statSync(dir + _path_[i]).mtime })
                                        // in dir file_server
                                        // for (j in _path_[i]) {
                                        //     obj.push({ "name": _path_[i], "extension": path.extname(dir + _path_[i]), "size": fs.statSync(dir + _path_[i]).size, "date": fs.statSync(dir + _path_[i]).mtime })
                                        // }
                                }
                                res.render('./agent/file_log_ag', {
                                    dir: dir_all_length,
                                    dir_device: dir_device,
                                    files: obj,
                                    history: history,
                                    words: words,
                                    words1: word,
                                    words2: word1,
                                    session: req.session
                                });
                            }
                        })
                    });
                });
            })
        }
    }
    // Datacheck
controller.database_ag = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/');
        } else {
            const user = req.session.userid;
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from doc_pdpa_document as d join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query('SELECT * FROM doc_pdpa_agent_database_check', (err, database) => {
                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            if (err) {
                                res.json(err)
                            }
                            res.render('./agent/database_ag', {
                                data: database,
                                history: history,
                                words: words,
                                words1: word,
                                words2: word1,
                                session: req.session
                            });
                        })
                    });
                });
            })
        }
    }
    // Delete Record (Datacheck)
controller.delDatabase_ag = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/');
        } else {
            const data_id = req.body.id;
            const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] }
            req.getConnection((err, conn) => {
                conn.query("DELETE FROM doc_pdpa_agent_datebase_check WHERE id = ?", [data_id], (err, dt) => {
                    if (err) {
                        req.session.errors = errorss;
                        req.session.success = false;
                    } else {
                        req.session.success = true;
                        req.session.topic = "ลบข้อมูลเสร็จแล้ว";
                    }
                    res.redirect(req.get('referrer'));
                });
            });
        }
    }
    // Log0 hash
controller.logger_ag = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/');
        } else {
            const user = req.session.userid;
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from doc_pdpa_document as d join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query('SELECT id, device_name,os_name,path,name_file,total_line,DATE_FORMAT(date_now, "%d/%m/%Y %H:%i") as date_now, value FROM doc_pdpa_agent_log0_hash', (err, hash) => {
                            conn.query('SELECT path, count(path) as cpath FROM doc_pdpa_agent_log0_hash GROUP BY path', (err, path) => {
                                let word = []
                                let word1 = []
                                for (i in words) {
                                    word.push(words[i].words_id)
                                    word1.push(words[i].words_often)
                                }
                                if (err) {
                                    res.json(err)
                                }
                                res.render('./agent/logger_hash_ag', {
                                    hash: hash,
                                    path: path,
                                    history: history,
                                    words: words,
                                    words1: word,
                                    words2: word1,
                                    session: req.session
                                });
                            })
                        })
                    });
                });
            })
        }
    }
    // Hardware (Option)
controller.hardware_ag = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from doc_pdpa_document as d join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    res.render('./agent/hardware_ag', {
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                });
            });
        })
    }
}

module.exports = controller