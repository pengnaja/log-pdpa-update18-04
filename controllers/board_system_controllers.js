const controller = {};
const { validationResult } = require("express-validator");
const path = require("path");
const uuidv4 = require("uuid/v4");

function addDate() {
    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    const months = [
        "01",
        "02",
        "03",
        "04",
        "05",
        "06",
        "07",
        "08",
        "09",
        "10",
        "11",
        "12",
    ];
    let current_datetime = new Date();
    let formatted_date =
        current_datetime.getFullYear() +
        "-" +
        months[current_datetime.getMonth()] +
        "-" +
        current_datetime.getDate();
    let current_time = new Date();
    let formatted_time =
        addZero(current_time.getHours()) +
        ":" +
        addZero(current_time.getMinutes()) +
        ":" +
        addZero(current_time.getSeconds());
    date = formatted_date + " " + formatted_time;
    return date;
}



controller.monitor = (req, res) => {
    //const data = req.body;
    id = req.session.userid;
    //console.log(req.session);
    var logperhour = [];

    if (typeof req.session.userid == "undefined") {
        res.redirect("/");
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT f.name,f.status FROM favorite as f", (err, favorite) => {
                conn.query('SELECT * FROM log as l WHERE DATE_FORMAT(l.date, "%Y-%m-%d") = DATE_FORMAT(now(), "%Y-%m-%d")', (err, data1) => {
                    conn.query('SELECT * FROM log as l WHERE NOT DATE_FORMAT(l.date, "%Y-%m-%d") = DATE_FORMAT(now(), "%Y-%m-%d")', (err, data2) => {
                        conn.query('SELECT a.alert_id as id,a.msg as msg,d.hostname as name,DATE_FORMAT(a.datetime, "%Y-%m-%d") as date FROM alert as a JOIN device as d on a.device_id = d.device_id WHERE a.datetime BETWEEN adddate(now(),-7) and now()', (err, data_chart_3) => {
                            conn.query('SELECT a.alert_id as id,DATE_FORMAT(a.datetime, "%Y-%m-%d") as date FROM alert as a JOIN device as d on a.device_id = d.device_id WHERE a.datetime BETWEEN adddate(now(),-7) and now() group by a.datetime', (err, data5) => {
                                conn.query('SELECT a.alert_id as id,DATE_FORMAT(a.datetime, "%Y-%m-%d") as date FROM alert as a JOIN device as d on a.device_id = d.device_id WHERE NOT a.datetime BETWEEN adddate(now(),-7) and now() group by a.datetime', (err, data6) => {
                                    conn.query('SELECT CONCAT(d.name, ": ปกติ") as title , DATE_FORMAT(l.date, "%Y-%m-%d") as start, "bg-success" as className FROM log as l join device as d on l.device_id = d.device_id group by DATE_FORMAT(l.date, "%Y-%m-%d"), l.device_id', (err, data7) => {
                                        conn.query('SELECT sum(f.size * 0.001) as size FROM exporthistory as ex INNER JOIN file as f on ex.file_id = f.file_id WHERE ex.date BETWEEN adddate(now(),-7) and now()', (err, data8) => { //จำนวนการ dowsnload 20 วัน
                                            conn.query('SELECT sum(f.size * 0.001) as size FROM exporthistory as ex INNER JOIN file as f on ex.file_id = f.file_id WHERE ex.date BETWEEN adddate(now(),-7) and now()', (err, data9) => { //จำนวนก่อน dowsnload 20 วัน
                                                if (err) {
                                                    res.json(err);
                                                }
                                                v_average_1 = 0;
                                                v_1 = 0;
                                                v_compare_1 = 0;

                                                v_average_3 = 0;
                                                v_3 = 0;
                                                v_compare_3 = 0;

                                                v_average_6 = 0;
                                                v_6 = 0;
                                                v_compare_6 = 0;

                                                if (data1, data2) {
                                                    v_average_1 = ((data1.length / data2.length) * 100).toFixed(2);
                                                    v_1 = data1.length;
                                                    v_compare_1 = data2.length;
                                                }

                                                if (data5, data6) {
                                                    v_average_3 = ((data5.length / data6.length) * 100).toFixed(2);
                                                    v_3 = data5.length;
                                                    v_compare_3 = data6.length;
                                                }

                                                if (data8, data9) {
                                                    v_average_6 = ((data8[0].size / data9[0].size) * 100).toFixed(2);
                                                    v_6 = data8[0].size;
                                                    v_compare_6 = data9[0].size;
                                                }
                                                // console.log(v_average_3);
                                                // console.log(v_3);
                                                // console.log(v_compare_3);
                                                console.log(data8[0].size);

                                                res.render("monitor/monitor", {
                                                    v_average_1: v_average_1,
                                                    v_1: v_1,
                                                    v_compare_1: v_compare_1,

                                                    v_average_3: v_average_3,
                                                    v_3: v_3,
                                                    v_compare_3: v_compare_3,

                                                    v_average_6: v_average_6,
                                                    v_6: v_6,
                                                    v_compare_6: v_compare_6,

                                                    data: favorite,
                                                    data7: data7,
                                                    data8: data8,
                                                    data_chart_3: data_chart_3,
                                                    session: req.session
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

    }
};

controller.favorite = (req, res) => {
    //const data = req.body;
    id = req.session.userid;
    //console.log(req.session);
    var logperhour = [];

    if (typeof req.session.userid == "undefined") {
        res.redirect("/");
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT f.name,f.status FROM favorite as f", (err, favorite) => {
                conn.query('SELECT * FROM log as l WHERE DATE_FORMAT(l.date, "%Y-%m-%d") = DATE_FORMAT(now(), "%Y-%m-%d")', (err, data1) => {
                    conn.query('SELECT * FROM log as l WHERE NOT DATE_FORMAT(l.date, "%Y-%m-%d") = DATE_FORMAT(now(), "%Y-%m-%d")', (err, data2) => {
                        conn.query('SELECT a.alert_id as id,a.msg as msg,d.hostname as name,DATE_FORMAT(a.datetime, "%Y-%m-%d") as date FROM alert as a JOIN device as d on a.device_id = d.device_id WHERE a.datetime BETWEEN adddate(now(),-7) and now()', (err, data_chart_3) => {
                            conn.query('SELECT a.alert_id as id,DATE_FORMAT(a.datetime, "%Y-%m-%d") as date FROM alert as a JOIN device as d on a.device_id = d.device_id WHERE a.datetime BETWEEN adddate(now(),-7) and now() group by a.datetime', (err, data5) => {
                                conn.query('SELECT a.alert_id as id,DATE_FORMAT(a.datetime, "%Y-%m-%d") as date FROM alert as a JOIN device as d on a.device_id = d.device_id WHERE NOT a.datetime BETWEEN adddate(now(),-7) and now() group by a.datetime', (err, data6) => {
                                    conn.query('SELECT CONCAT(d.name, ": ปกติ") as title , DATE_FORMAT(l.date, "%Y-%m-%d") as start, "bg-success" as className FROM log as l join device as d on l.device_id = d.device_id group by DATE_FORMAT(l.date, "%Y-%m-%d"), l.device_id', (err, data7) => {
                                        conn.query('SELECT sum(f.size * 0.001) as size FROM exporthistory as ex INNER JOIN file as f on ex.file_id = f.file_id WHERE ex.date BETWEEN adddate(now(),-7) and now()', (err, data8) => { //จำนวนการ dowsnload 20 วัน
                                            conn.query('SELECT sum(f.size * 0.001) as size FROM exporthistory as ex INNER JOIN file as f on ex.file_id = f.file_id WHERE ex.date BETWEEN adddate(now(),-7) and now()', (err, data9) => { //จำนวนก่อน dowsnload 20 วัน
                                                if (err) {
                                                    res.json(err);
                                                }
                                                v_average_1 = 0;
                                                v_1 = 0;
                                                v_compare_1 = 0;

                                                v_average_3 = 0;
                                                v_3 = 0;
                                                v_compare_3 = 0;

                                                v_average_6 = 0;
                                                v_6 = 0;
                                                v_compare_6 = 0;

                                                if (data1, data2) {
                                                    v_average_1 = ((data1.length / data2.length) * 100).toFixed(2);
                                                    v_1 = data1.length;
                                                    v_compare_1 = data2.length;
                                                }

                                                if (data5, data6) {
                                                    v_average_3 = ((data5.length / data6.length) * 100).toFixed(2);
                                                    v_3 = data5.length;
                                                    v_compare_3 = data6.length;
                                                }

                                                if (data8, data9) {
                                                    v_average_6 = ((data8[0].size / data9[0].size) * 100).toFixed(2);
                                                    v_6 = data8[0].size;
                                                    v_compare_6 = data9[0].size;
                                                }
                                                // console.log(v_average_3);
                                                // console.log(v_3);
                                                // console.log(v_compare_3);
                                                console.log(data8[0].size);

                                                res.render("monitor/monitor", {
                                                    v_average_1: v_average_1,
                                                    v_1: v_1,
                                                    v_compare_1: v_compare_1,

                                                    v_average_3: v_average_3,
                                                    v_3: v_3,
                                                    v_compare_3: v_compare_3,

                                                    v_average_6: v_average_6,
                                                    v_6: v_6,
                                                    v_compare_6: v_compare_6,

                                                    data: favorite,
                                                    data7: data7,
                                                    data8: data8,
                                                    data_chart_3: data_chart_3,
                                                    session: req.session
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

    }
};

controller.compare = (req, res) => {
    //const data = req.body;
    id = req.session.userid;
    //console.log(req.session);
    var logperhour = [];

    if (typeof req.session.userid == "undefined") {
        res.redirect("/");
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM device", (err, data) => {
                conn.query('SELECT DATE_FORMAT(device.rmfile, "%Y-%m-%d") as date FROM device', (err, data2) => {
                    conn.query('SELECT COUNT(l.log_id) FROM log as l JOIN device as d on l.device_id = d.device_id group by l.device_id', (err, data3) => {
                        conn.query('SELECT d.device_id as did FROM log as l JOIN device as d on l.device_id = d.device_id group by l.date', (err, data4) => {
                            conn.query('SELECT COUNT(l.log_id) as sum_c FROM log as l JOIN device as d on l.device_id = d.device_id group by l.date', (err, data5) => {
                                conn.query('SELECT DATE_FORMAT(l.date, "%Y-%m-%d %H:%i:%s") as date FROM log as l JOIN device as d on l.device_id = d.device_id group by l.date', (err, data6) => {
                                    conn.query('SELECT f.file_id as fid, d.device_id as did,ft.path FROM file as f JOIN device as d on f.device_id = d.device_id JOIN ftp as ft on ft.file_id = f.file_id', (err, data8) => { //ที่เก็บไฟล์
                                        conn.query('SELECT d.keep,DATE_FORMAT(adddate(now(),-90), "%Y-%m-%d") as start_file FROM device as d', (err, data9) => {
                                            conn.query('SELECT d.device_id as did FROM device as d group by d.device_id', (err, ar) => {
                                                conn.query('SELECT f.device_id,sum(f.size) as s_size FROM file as f group by f.device_id', (err, data10) => {

                                                    if (err) {
                                                        res.json(err);
                                                    }

                                                    var element = [];
                                                    var element2 = [];
                                                    var element3 = [];
                                                    var element4 = [];
                                                    for (let a = 0; a < data4.length; a++) {
                                                        element.push(data4[a].did)
                                                    }
                                                    for (let b = 0; b < data5.length; b++) {
                                                        element2.push(data5[b].sum_c)
                                                    }
                                                    for (let c = 0; c < data6.length; c++) {
                                                        element3.push(data6[c].date)
                                                    }

                                                    for (let d = 0; d < ar.length; d++) {
                                                        element4.push(ar[d].did)
                                                    }




                                                    // console.log(element);
                                                    // console.log(element2);
                                                    // console.log(element3);

                                                    var arr = element4
                                                    var uniqueArr = [...new Set(arr)]
                                                        // console.log(uniqueArr)

                                                    // console.log(uniqueArr);

                                                    res.render("monitor/compare", {
                                                        data: data,
                                                        data2: data2,
                                                        data3: data3,


                                                        data4: element,
                                                        data5: element2,
                                                        data6: element3,
                                                        data7: uniqueArr,
                                                        data8: data8,
                                                        data9: data9,
                                                        data10: data10,

                                                        session: req.session
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }
};

controller.watchdog = (req, res) => {
    //const data = req.body;
    id = req.session.userid;
    //console.log(req.session);
    var logperhour = [];

    if (typeof req.session.userid == "undefined") {
        res.redirect("/");
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM log", (err, log) => {
                if (err) {
                    res.json(err);
                }
                res.render("monitor/watchdog", {
                    data: log,
                    session: req.session
                });
            });
        });
    }
};

controller.datain = (req, res) => {
    //const data = req.body;
    id = req.session.userid;
    //console.log(req.session);
    var logperhour = [];

    if (typeof req.session.userid == "undefined") {
        res.redirect("/");
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM device as d", (err, data) => {

                conn.query('SELECT DATE_FORMAT(l.date, "%Y-%m-%d") as date FROM log as l group by DATE_FORMAT(l.date, "%Y-%m-%d")', (err, data10) => { //date รวมทั้งหมดของ log
                    conn.query('SELECT COUNT(l.log_id) as s,l.device_id as did, DATE_FORMAT(l.date, "%Y-%m-%d") as date FROM log as l group by DATE_FORMAT(l.date, "%Y-%m-%d"),did', (err, data11) => { //device id และ date ของ log
                        conn.query('SELECT * FROM device as d group by d.device_id', (err, data12) => { //device id ทั้งหมด

                            if (err) {
                                res.json(err);
                            }
                            data_10 = []; //date
                            data_10_1 = []; //log id

                            datax = [];
                            data_date = [];
                            size = [];
                            color_r = [];


                            for (let b = 0; b < data12.length; b++) { //id deice
                                data_p = {
                                    name: '',
                                    type: 'line',
                                    data: [],
                                }
                                data_p.name = (data12[b].name).toString();

                                for (let a = 0; a < data10.length; a++) { // date
                                    for (let c = 0; c < data11.length; c++) {

                                        if (data11[c].date == data10[a].date && data11[c].did == data12[b].device_id) { // date,count
                                            data_p.data.push(data11[c].s)
                                        } else if (data11[c].date == data10[a].date && data11[c].did != data12[b].device_id) {
                                            data_p.data.push(0)
                                        }
                                    }

                                }
                                datax.push(data_p);
                            }
                            for (let d = 0; d < data11.length; d++) {
                                data_date.push(data11[d].date)

                            }
                            for (let e = 0; e < data12.length; e++) {
                                size.push(data12[e].name)
                                color_r.push(generateRandomColor())

                            }

                            function generateRandomColor() {
                                let maxVal = 0xFFFFFF; // 16777215
                                let randomNumber = Math.random() * maxVal;
                                randomNumber = Math.floor(randomNumber);
                                randomNumber = randomNumber.toString(16);
                                let randColor = randomNumber.padStart(6, 0);
                                return `#${randColor.toUpperCase()}`
                            }

                            console.log(datax);
                            console.log(data_date);
                            console.log(size);
                            console.log(color_r);

                            // data_test.data.push(5)
                            // data_test.data.push(12)



                            res.render("monitor/datain", {
                                data: data,
                                data10: data10,
                                data2: datax, //value ของกราฟ
                                data3: data_date, //date
                                data4: size, //name
                                data5: color_r, //color
                                session: req.session
                            });
                        });
                    });
                });
            });
        });
    }
};

controller.dataout = (req, res) => {
    //const data = req.body;
    id = req.session.userid;
    //console.log(req.session);
    var logperhour = [];

    if (typeof req.session.userid == "undefined") {
        res.redirect("/");
    } else {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM log", (err, log) => {
                if (err) {
                    res.json(err);
                }
                res.render("monitor/dataout", {
                    data: log,
                    session: req.session
                });
            });
        });
    }
};

controller.up = (req, res) => {
    const data = req.body;
    // console.log(data);
    req.getConnection((err, conn) => {
        conn.query('UPDATE favorite SET favorite.status = ? WHERE favorite.name = ?', [data.status, data.name], (err, favorite) => {
            res.send({ favorite: favorite })
        });
    });
};


controller.log_chart_1 = (req, res) => {

    req.getConnection((err, conn) => {
        conn.query('SELECT COUNT(l.log_id) as sum, DATE_FORMAT(now(), "%Y-%m-%d") as now,DATE_FORMAT(l.date, "%Y-%m-%d") as ldate,DATE_FORMAT(l.date, "%H:%i:%s") as time FROM log as l WHERE DATE_FORMAT(l.date, "%Y-%m-%d") = DATE_FORMAT(now(), "%Y-%m-%d") group by time ORDER by time ASC', (err, log_chart1) => {
            conn.query('SELECT l.log_id, d.hostname, d.de_ip, DATE_FORMAT(l.date, "%H:%i:%s") as date,l.msg FROM log as l JOIN device as d on l.device_id = d.device_id ORDER BY l.date DESC LIMIT 1', (err, data_chart1) => {
                conn.query('SELECT COUNT(datetime) as data_chart_3,DATE_FORMAT(a.datetime, "%Y-%m-%d") as data_date_3 FROM alert as a WHERE a.datetime BETWEEN adddate(now(),-7) and now() group BY DATE_FORMAT(a.datetime, "%Y-%m-%d")', (err, data_chart3) => {
                    conn.query('SELECT pf.performance_id as id, pf.cpu, pf.cpuused, pf.mem, ROUND(pf.memused,2) as memused, ROUND(sum(memused*100/(mem+memused)), 0) as sum_mem, pf.storage,pf.storageused, pf.storageper FROM performance as pf', (err, pf) => {
                        conn.query('SELECT COUNT(l.log_id) as sum,DATE_FORMAT(l.date, "%Y-%m-%d") as ldate FROM log as l group by ldate order by ldate asc', (err, data_chart5) => {



                            res.send({ data: log_chart1, data_chart1, data_chart3, pf, data_chart5 })
                        });
                    });
                });
            });
        });
    });
};

controller.data_compare = (req, res) => {
    const { id } = req.params;
    // console.log(id);
    req.getConnection((err, conn) => {
        conn.query('SELECT COUNT(l.log_id) as cs FROM log as l JOIN device as d on l.device_id = d.device_id WHERE d.device_id = ?', [id], (err, count_d) => {
            conn.query('SELECT COUNT(l.log_id) as s FROM log as l JOIN device as d on l.device_id = d.device_id ', (err, count_sum) => {

                var data_sum = ((count_d[0].cs / count_sum[0].s) * 100).toFixed(2);

                // console.log(data_sum);
                res.send({ data: data_sum })
            });
        });
    });
};


module.exports = controller;