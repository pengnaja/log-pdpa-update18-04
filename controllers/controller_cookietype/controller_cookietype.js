const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');
const requestIp = require('request-ip'); // get session id 
const browser = require('browser-detect'); // get name browser
var ip = require('ip');
const controller = {};

controller.settingcookietypes = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM cookiepolicy ORDER BY id_cp DESC",
                (err, cookiepolicy) => {
                    res.render("cookie/views/view_cookietype/cookietype", {
                        cookiepolicy: cookiepolicy,
                        session: req.session
                    });
                });
        });
    } else {
        res.redirect("/");
    }
};


controller.save = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        req.getConnection((err, conn) => {
            conn.query("INSERT INTO cookiepolicy SET name_cp=?,detail_cp=?,approve_cp=1", [data.name_cp, data.detail_cp],
                (err, insert_cookirtype) => {

                    conn.query("SELECT * FROM cookiepolicy ORDER BY id_cp DESC ", (err, select_cookiepolicy) => {
                        conn.query("SELECT * FROM domaingroup ", (err, select_domaingroup) => {

                            conn.query("INSERT INTO log_history SET log_action ='เพิ่มประเภทคุกกี้' ,log_detail=?,user_id=?",
                                [data.name_cp, req.session.user_id_u], (err, log_history) => {

                                    for (var i = 0; i < select_domaingroup.length; i++) {
                                        conn.query("INSERT INTO domain_setting_cookiepolicy SET name_cookietype=?,detail_cookie=?,approve=?,domain_id=?,cookiepolicy_id=?",
                                            [select_cookiepolicy[0].name_cp, select_cookiepolicy[0].detail_cp, select_cookiepolicy[0].approve_cp, select_domaingroup[i].id_dg, select_cookiepolicy[0].id_cp],
                                            (err, insert_domain_setting_cookiepolicy) => {

                                            });
                                    }
                                    conn.query("SELECT *FROM domaingroup ", (err, select_domaingroup) => {
                                        var domain = [];

                                        for (let i = 0; i < select_domaingroup.length; i++) {
                                            var id_domain = select_domaingroup[i].namedomain_dg.replace("https://", "");
                                            var id_g = id_domain.replace("www", "").replace("com", "").replace(".", "").replace(".", "");
                                            domain.push(id_g + "_" + select_domaingroup[i].id_dg + ".html")
                                        }

                                        ///**** เหลือสร้างไฟล์ html ตอนเพิ่มประเภทคุกกี้
                                        for (let i = 0; i < select_domaingroup.length; i++) {

                                            conn.query("SELECT * FROM  domain_style_policy where domaingroup_id=?;",
                                                [select_domaingroup[i].id_dg], (err, domain_style_policy) => {

                                                    conn.query("SELECT * FROM  domain_setting_cookiepolicy where domain_id=?;",
                                                        [select_domaingroup[i].id_dg], (err, domain_setting_cookiepolicy) => {


                                                            var html_file = ` <div class="modal-dialog modal-lg shadow" role = "document" >  
                                                            <div class= "modal-content" >  
                                                                <div class="modal-header" > 
                                                                    <h4 class="modal-title">การตั้งค่าคุกกี้</h4> 
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div> 
                                                             <div class="modal-body"> 
                                                             <div class="bccs-body-text" style="font-size: 100%"> 
                                                                 <p>  
                                                                 ${domain_style_policy[0].detail_dialog}                                            
                                                                    </p> 
                                                             </div> 
                                                                    <p class="d-flex justify-content-between"> 
                                                                          <a href="#bccs-options" data-bs-toggle="collapse">เลือกประเภทคุกกี้</a> 
                                                                     </p> 
                                                                 <div id="bccs-options" class="collapse"> 
                                                                        <div class="bccs-option" > `;

                                                            for (var j = 0; j < domain_setting_cookiepolicy.length; j++) {
                                                                html_file += `
                                                                     <div class="form-check mb-1" data-name="${domain_setting_cookiepolicy[j].approve}">
                                                                      <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${j + 1}"  name="cookie" value="${domain_setting_cookiepolicy[j].id_dsc}">
                                                                      <label class="form-check-label" for="bccs-checkboxNecessary">
                                                                        <b>${domain_setting_cookiepolicy[j].name_cookietype}</b>
                                                                   
                                                                       </label>
                                                                       </div>
                                                                       <ul>
                                                                       <li>${domain_setting_cookiepolicy[j].detail_cookie}</li>
                                                                    </ul>`;
                                                            }
                                                            html_file += `
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button id="bccs-buttonDoNotAgree" type="button" class="btn btn-link text-decoration-none">
                                                          ปฏิเสธ
                                                    </button>
                                                    <button id="bccs-buttonAgree" type="button" class="btn btn-primary">อนุญาต</button>
                                                    <button id="bccs-buttonSave" type="button" class="btn btn-outline-dark">
                                                    อนุญาตตามที่เลือก
                                                            </button>
                                                <button id="bccs-buttonAgreeAll" type="button" class="btn btn-primary">อนุญาตทั้งหมด</button>
                                                 </div>
                                                </div >
                                                                        </div > `;


                                                            fs.writeFileSync('./dist/file_domain/' + domain[i], html_file, function (err, file) {
                                                                if (err) throw err;
                                                                // console.log(err);
                                                            });
                                                        });
                                                });
                                        }
                                    });
                                });
                        });
                    });

                });
            res.redirect('/setting/cookietypes');
        });
    } else {
        res.redirect("/");
    }
};

controller.edit = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        req.getConnection((err, conn) => {
            conn.query("UPDATE  cookiepolicy SET name_cp=?,detail_cp=? WHERE id_cp =?", [data.name_cp, data.detail_cp, data.id_cp],
                (err, update_dialog) => {
                    conn.query("INSERT INTO log_history SET log_action ='เเก้ไขข้อมูลประเภทคุกกี้' ,log_detail=?,user_id=?",
                        [data.name_cp, req.session.user_id_u], (err, log_history) => {
                            res.redirect('/setting/cookietypes');
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};

controller.delete = (req, res) => {
    if (req.session.userid) {
        const { id } = req.params;
        delete_file(req);
        req.getConnection((err, conn) => {
            conn.query("DELETE FROM domain_setting_cookiepolicy WHERE cookiepolicy_id =?", [id], (err, select_domain_setting_cookiepolicy) => {
                conn.query('DELETE FROM cookiepolicy WHERE id_cp = ? ', [id],
                    (err, delete_cookiepolicy) => {
                        conn.query("INSERT INTO log_history SET log_action ='ลบข้อมูลประเภทคุกกี้' ,user_id=?",
                            [req.session.user_id_u], (err, log_history) => {
                                setTimeout(() => {
                                    res.redirect('/setting/cookietypes');
                                }, 500);
                            });
                    });
            });
        });
    } else {
        res.redirect("/");
    }
};



function delete_file(req) { // function ในการเเก้ไข้ file.html  หลังจากลบประเภทคุกกี้หลังบ้าน
    req.getConnection((err, conn) => {
        conn.query("SELECT *FROM domaingroup", (err, select_domaingroup) => {
            var domain = [];
            for (let i = 0; i < select_domaingroup.length; i++) { //
                var id_domain = select_domaingroup[i].namedomain_dg.replace("https://", "");
                var id_g = id_domain.replace("www", "").replace("com", "").replace(".", "").replace(".", "");
                domain.push(id_g + "_" + select_domaingroup[i].id_dg + ".html")
            }

            for (let i = 0; i < select_domaingroup.length; i++) { // เเก้ไข้ file.html เมื่อหลังบ้านลบประเภทคุกกี้
                conn.query("SELECT * FROM  domain_style_policy where domaingroup_id=?;",
                    [select_domaingroup[i].id_dg], (err, domain_style_policy) => {
                        conn.query("SELECT * FROM  domain_setting_cookiepolicy where domain_id=?;",
                            [select_domaingroup[i].id_dg], (err, domain_setting_cookiepolicy) => {
                                var html_file = ` <div class="modal-dialog modal-lg shadow" role = "document" >  
                                <div class= "modal-content" >  
                                    <div class="modal-header" > 
                                        <h4 class="modal-title">การตั้งค่าคุกกี้</h4> 
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div> 
                                 <div class="modal-body"> 
                                 <div class="bccs-body-text" style="font-size: 100%"> 
                                     <p>  
                                     ${domain_style_policy[0].detail_dialog}                                            
                                        </p> 
                                 </div> 
                                        <p class="d-flex justify-content-between"> 
                                              <a href="#bccs-options" data-bs-toggle="collapse">เลือกประเภทคุกกี้</a> 
                                         </p> 
                                     <div id="bccs-options" class="collapse"> 
                                            <div class="bccs-option" > `;

                                for (var j = 0; j < domain_setting_cookiepolicy.length; j++) {
                                    html_file += `
                                         <div class="form-check mb-1" data-name="${domain_setting_cookiepolicy[j].approve}">
                                          <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${j + 1}"  name="cookie" value="${domain_setting_cookiepolicy[j].id_dsc}">
                                          <label class="form-check-label" for="bccs-checkboxNecessary">
                                            <b>${domain_setting_cookiepolicy[j].name_cookietype}</b>
                                       
                                           </label>
                                           </div>
                                           <ul>
                                           <li>${domain_setting_cookiepolicy[j].detail_cookie}</li>
                                        </ul>`;
                                }
                                html_file += `
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="bccs-buttonDoNotAgree" type="button" class="btn btn-link text-decoration-none">
                              ปฏิเสธ
                        </button>
                        <button id="bccs-buttonAgree" type="button" class="btn btn-primary">อนุญาต</button>
                        <button id="bccs-buttonSave" type="button" class="btn btn-outline-dark">
                        อนุญาตตามที่เลือก
                                </button>
                    <button id="bccs-buttonAgreeAll" type="button" class="btn btn-primary">อนุญาตทั้งหมด</button>
                     </div>
                    </div >
                                            </div > `;


                                fs.writeFileSync('./dist/file_domain/' + domain[i], html_file, function (err, file) {
                                    if (err) throw err;
                                    // console.log(err);
                                });
                            });
                    });
            }
        });
    });
}


controller.api_cookietypes = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM cookiepolicy ORDER BY id_cp DESC",
                (err, cookiepolicy) => {
                    res.send(cookiepolicy);
                });
        });
    } else {
        res.redirect("/");
    }
};

controller.api_cookietypes_search = (req, res) => {
    if (req.session.userid) {
        const search = '%' + req.body.data + '%';
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM cookiepolicy WHERE name_cp LIKE ? OR  detail_cp LIKE ?  ORDER BY id_cp DESC", [search, search],
                (err, cookiepolicy) => {
                    if (cookiepolicy[0]) {
                        res.send(cookiepolicy)
                    } else {
                        var data_null = "ไม่มีข้อมูล";
                        res.send(data_null);
                    }
                });
        });
    } else {
        res.redirect("/");
    }
};





module.exports = controller;