const controller = {};
const { validationResult } = require("express-validator");
const path = require("path");
const uuidv4 = require("uuid/v4");
// const csvjson = require("csvjson");
const fs = require("fs");
//var csv = require("csvtojson");
// Convert a csv file with csvtojson
controller.list = (req, res) => {
  var data = imcsv(path.join(__dirname, "eieimark.csv"));
  //data.pop();
  const user = req.session.userid;
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
} else {
  console.log(user);
  req.getConnection((err, conn) => {
    conn.query(
      "SELECT * FROM doc_pdpa_classification as cla INNER JOIN doc_pdpa_pattern as pat on pat.pattern_id=cla.pattern_id INNER JOIN account as ac on ac.acc_id=cla.acc_id WHERE ac.acc_id=?;",[user],
      (err, classi) => {
        conn.query(
          "SELECT *,DATE_FORMAT(dout.date_create ,'%d-%m-%Y') as datecreate FROM doc_pdpa_classification as cla INNER JOIN doc_pdpa_pattern as pat on pat.pattern_id=cla.pattern_id INNER JOIN account as ac on ac.acc_id=cla.acc_id INNER join doc_pdpda_data_out as dout ON dout.classify_id=cla.classify_id WHERE ac.acc_id=?;",[user],
          (err, dataout) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history  from doc_pdpa_document as d  join doc_pdpa.pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
           // console.log(classi);
            res.render("./data_out/data_out_list", {
              session: req.session,
              data: data,
              classi: classi,
              dataout: dataout,
              history: history,
            });
          }
        );
      }
    );
  });
});
}
};

controller.add = (req, res) => {
  //data.pop();
  const data = req.body;
  data.date_create = addDate();
  if(data.type_res == 0){
    data.res_link = data.linkemail
    delete data.linkToken
    delete data.linkemail
  }else{
    data.res_link = data.linkToken
    delete data.linkToken
    delete data.linkemail
  }
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
} else {
  console.log(data);
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO doc_pdpda_data_out set ?;',[data], (err, classi) => {
    console.log(classi);
    res.redirect('/dataoutlist');
    });
  });
}
};

controller.del = (req, res) => {
  //data.pop();
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
} else {
  const { id } = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM doc_pdpda_data_out where doc_pdpda_data_out.data_out_id=?;',[id], (err, classi) => {
    console.log(classi);
    res.redirect('/dataoutlist');
    });
  });
}
};

module.exports = controller;
function imcsv(filepath) {
  var data = fs
    .readFileSync(filepath)
    .toString() // convert Buffer to string
    .split("\n") // split string to lines
    .map((e) => e.trim()) // remove white spaces for each line
    .map((e) => e.split(",").map((e) => e.trim())); // split each line to array
  return data;
}
function addDate() {
  const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
  let current_datetime = new Date()
  let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
  console.log("formatted_date : " + formatted_date);
  return formatted_date;
}

//   var data = fs.readFileSync(path.join(__dirname, "eiei.csv"), {
//     encoding: "utf8",
//   });
//   var options = {
//     delimiter: ",", // optional
//     quote: '"', // optional
//   };
//   let test = csvjson.toColumnArray(data, options);
