const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');

const controller = {};

controller.report_emailconsent = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email WHERE email_status=1",
                (err, log_pdpa_email) => {
                    conn.query("SELECT  count(email_date_send) as count_send,count(email_date_consent) as count_consent,DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email  GROUP BY  date_consent",
                        (err, area_pdpa_email) => {
                            res.render("./cookie/views/view_report_emailconsent/report_emailconsent", {
                                log_pdpa_email: log_pdpa_email,
                                area_pdpa_email: area_pdpa_email,
                                session: req.session
                            });
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};

controller.api_report_emailconsent = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email WHERE email_status=1",
                (err, log_pdpa_email) => {
                    conn.query("SELECT  count(email_date_send) as count_send,count(email_date_consent) as count_consent,DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email  GROUP BY  date_consent",
                        (err, area_pdpa_email) => {
                            conn.query("SELECT  count(email_date_send) as count_0 ,DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_sent FROM  pdpa_email  WHERE email_status=0 GROUP BY date_sent",
                                (err, count_0) => {
                                    conn.query("SELECT  count(email_date_consent) as count_1 ,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent FROM  pdpa_email  WHERE email_status=1 GROUP BY date_consent",
                                        (err, count_1) => {
                                            conn.query("SELECT  count(email_date_consent) as count_2 ,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent FROM  pdpa_email  WHERE email_status=2 GROUP BY date_consent",
                                                (err, count_2) => {
                                                    res.send({
                                                        log_pdpa_email,
                                                        area_pdpa_email,
                                                        count_0,
                                                        count_1,
                                                        count_2
                                                    });
                                                });
                                        });
                                })
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};


module.exports = controller;