const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');

const controller = {};

controller.report_emailconsent = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email WHERE email_status=1",
                (err, log_pdpa_email) => {
                    conn.query("SELECT  count(email_date_send) as count_send,count(email_date_consent) as count_consent,DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email  GROUP BY  date_consent",
                        (err, area_pdpa_email) => {
                            res.render("cookie/views/view_report_emailconsent/report_emailconsent", {
                                log_pdpa_email: log_pdpa_email,
                                area_pdpa_email: area_pdpa_email,
                                session: req.session
                            });
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};

controller.api_report_emailconsent = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email WHERE email_status=1 ",
                // conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email WHERE email_status=1 OR email_status=2",
                (err, log_pdpa_email) => {
                    conn.query("SELECT  count(email_date_send) as count_send,count(email_date_consent) as count_consent,DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email  GROUP BY  date_consent",
                        (err, area_pdpa_email) => {
                            conn.query("SELECT  count(email_date_send) as count_0 ,DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_sent FROM  pdpa_email  WHERE email_status=0 GROUP BY date_sent",
                                (err, count_0) => {
                                    conn.query("SELECT  count(email_date_consent) as count_1 ,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent FROM  pdpa_email  WHERE email_status=1 GROUP BY date_consent",
                                        (err, count_1) => {
                                            conn.query("SELECT  count(email_date_consent) as count_2 ,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent FROM  pdpa_email  WHERE email_status=2 GROUP BY date_consent",
                                                (err, count_2) => {
                                                    if (log_pdpa_email[0] && area_pdpa_email[0]) {
                                                        res.send({ log_pdpa_email, area_pdpa_email, count_0, count_1, count_2 });
                                                    } else {
                                                        var data_null = "ไม่มีข้อมูล";
                                                        res.send(data_null);
                                                    }
                                                });
                                        });
                                })
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};

controller.api_report_emailconsent_search = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        if (data.data.indexOf("@") >= 0) {
            search = '%' + data.data + '%';
            req.getConnection((err, conn) => {
                conn.query("SELECT id_email,DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email WHERE  email_to LIKE ? AND email_status=1 AND email_date_consent IS NOT NULL",
                    [search], (err, log_pdpa_email) => {
                        if (log_pdpa_email[0]) {
                            res.send(log_pdpa_email)
                        } else {
                            var data_null = "ไม่มีข้อมูล";
                            res.send(data_null);
                        }
                    });
            });
        } else {
            search = '%' + data.data + '%';
            req.getConnection((err, conn) => {
                conn.query("SELECT id_email,DATE_FORMAT(email_date_send,'%d-%m-%Y') as date_send,DATE_FORMAT(email_date_consent,'%d-%m-%Y') as date_consent,DATE_FORMAT(NOW(),'%m-%Y') as date_now,email_to,email_subject,email_date_send,email_subject,email_status FROM  pdpa_email WHERE  email_subject LIKE ?  AND email_status=1 AND email_date_consent IS NOT NULL",
                    [search], (err, log_pdpa_email) => {
                        if (log_pdpa_email[0]) {
                            res.send(log_pdpa_email)
                        } else {
                            var data_null = "ไม่มีข้อมูล";
                            res.send(data_null);
                        }
                    });
            });
        }
    } else {
        res.redirect("/");
    }
};

module.exports = controller;
