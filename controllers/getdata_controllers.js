const controller = {};
const { validationResult } = require('express-validator');
const path = require('path');
const uuidv4 = require('uuid/v4');

function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())
    date = formatted_date + ' ' + formatted_time;
    return date;
}

controller.getperformance = (req, res) => {
    const data = null;

    // if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM `performance`', (err, performance) => {
            conn.query('SELECT `traffic`.`rxkB/s` as rxkB,`traffic`.`txkB/s` as txkB FROM `traffic` WHERE iface != "lo" GROUP BY iface ORDER BY traffic_id DESC', (err, traffic) => {

                res.send({ data: performance, data2: traffic })
            });
        });
    });
    // }
};

module.exports = controller;