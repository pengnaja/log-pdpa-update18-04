const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');

const controller = {};


controller.api_tag = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM  domain_setting_tag WHERE tag_id = ?", [req.params.id],
                (err, pdpa_tag) => {
                    if (pdpa_tag[0]) {
                        res.send(pdpa_tag);
                    } else {
                        var data_null = "ไม่มีข้อมูล";
                        res.send(data_null);
                    }
                });
        });
    } else {
        res.redirect("/");
    }
};


controller.api_cookie = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM domain_setting_tag as dst LEFT JOIN domaingroup as dg ON dst.domaingroup_id=dg.id_dg LEFT JOIN pdpa_tag as pt ON dst.tag_id=pt.id_tag",
                (err, domain_setting_tag) => {
                    conn.query("SELECT date_dg,namedomain_dg,id_dg,message FROM  domaingroup  ORDER BY id_dg DESC",
                        (err, domaingroup) => {
                            if (domain_setting_tag[0]) {
                                res.send({ domain_setting_tag, domaingroup });
                            } else {
                                var data_null = "ไม่มีข้อมูล";
                                res.send(data_null);
                            }
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};



controller.dialogs = (req, res) => {
    if (req.session.userid) {
        const { id } = req.params;
        // console.log(id);
        req.getConnection((err, conn) => {
            // select  ในส่วนของ cookiepolicy
            conn.query("SELECT id_cp,id_dg,id_dsc,name_cookietype,approve,detail_cookie FROM domain_setting_cookiepolicy as dscp LEFT JOIN domaingroup as d ON dscp.domain_id=d.id_dg LEFT JOIN cookiepolicy as cp ON dscp.cookiepolicy_id=cp.id_cp WHERE d.id_dg=?", [id],
                (err, cookiepolicy) => {
                    // select  ในส่วนของ ชื่อdomain เเเละ  dialog
                    conn.query("SELECT height,id_dl,id_dg,id_st,id_dsp,namelocation,nameclass,detail_dialog,namedomain_dg FROM domain_style_policy as dsp LEFT JOIN domaingroup as d  ON dsp.domaingroup_id=d.id_dg  LEFT JOIN dialog as dl ON dsp.dialog_id=dl.id_dl LEFT JOIN styles as s ON dsp.style_id=s.id_st WHERE d.id_dg=?", [id],
                        (err, domaingroup) => {
                            //select log_history
                            conn.query("SELECT DATE_FORMAT( Date_Sub(NOW(), INTERVAL 1 DAY),'%Y-%m-%d') as day_sub_1,DATE_FORMAT(NOW(),'%Y-%m-%d') as day,DATE_FORMAT(log_date,'%Y-%m-%d') as date_log,log_date,DATE_FORMAT(log_date,'%I:%i %p') as time,log_action from log_history  WHERE user_id=? ORDER BY log_date DESC", [req.session.userid], (err, Log_history) => {
                                req.session.log = Log_history;
                                // log_history(res, conn);
                                res.render("cookie/views/view_cookie_managemet/dialog", {
                                    domaingroup: domaingroup[0],
                                    cookiepolicy: cookiepolicy,
                                    session: req.session
                                });
                            });
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};


controller.home = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT date_dg,namedomain_dg,id_dg,message FROM  domaingroup WHERE status_dg = 1  ORDER BY id_dg DESC",
                // conn.query("SELECT date_dg,namedomain_dg,id_dg,message FROM  domaingroup   ORDER BY id_dg DESC",
                (err, domaingroup) => {
                    conn.query("SELECT * FROM pdpa_tag",
                        (err, tag) => {
                            conn.query("SELECT * FROM cookiepolicy ",
                                (err, cookiepolicy) => {
                                    conn.query("SELECT namedomain_dg,DATE_FORMAT(policylog_date,'%Y-%m-%d %I:%i %p') as day,cookiepolicy,policylog_ip,policylog_sessionid,policylog_browser FROM pdpa_log_setting_cookiepolicy as lsc LEFT JOIN pdpa_policylog as pl ON lsc.policy_id=pl.id_policylog LEFT JOIN domaingroup as dg ON lsc.domaingroup_id=dg.id_dg",
                                        (err, log_cookiepolicy) => {
                                            conn.query("SELECT DATE_FORMAT(policylog_date,'%Y-%m-%d %I:%i %p') as day,policylog_ip,policylog_sessionid,policylog_browser,policylog_date FROM pdpa_policylog",
                                                (err, pdpa_policylog) => {
                                                    // SELECT session_date
                                                    conn.query("SELECT DATE_FORMAT( Date_Sub(NOW(), INTERVAL 1 DAY),'%Y-%m-%d') as day_sub_1,DATE_FORMAT(NOW(),'%Y-%m-%d') as day,DATE_FORMAT(log_date,'%Y-%m-%d') as date_log,log_date,DATE_FORMAT(log_date,'%I:%i %p') as time,log_action from log_history  WHERE user_id=? ORDER BY log_date DESC",
                                                        [req.session.userid], (err, Log_history) => {
                                                            //  day วันที่  เดือน(กุมภาพันธ์) (พ.ศ.) toThaiString
                                                            var day = [];
                                                            for (var i = 0; i < Log_history.length; i++) {
                                                                if (Log_history[i].date_log == Log_history[i].day) {
                                                                    day.push("วันนนี้");
                                                                } else {
                                                                    if (Log_history[i].date_log == Log_history[i].day_sub_1) {
                                                                        day.push("เมื่อวาน");
                                                                    } else {
                                                                        day.push(Log_history[i].log_date.toThaiString(2));
                                                                    }
                                                                }
                                                            }
                                                            req.session.log = Log_history;
                                                            req.session.day = day;

                                                            // day_thai วันที่  เดือน(กุมภาพันธ์) พ.ศ.(2565) toThaiString
                                                            var day_thai = [];
                                                            for (var i = 0; i < domaingroup.length; i++) {
                                                                day_thai.push(domaingroup[i].date_dg.toThaiString(4));
                                                            }

                                                            res.render("cookie/views/view_cookie_managemet/manage_cookies", {
                                                                domaingroup: domaingroup,
                                                                day_thai: day_thai,
                                                                tag: tag,
                                                                cookiepolicy: cookiepolicy,
                                                                pdpa_policylog: pdpa_policylog,
                                                                log_cookiepolicy: log_cookiepolicy,
                                                                session: req.session
                                                            });
                                                        });

                                                });
                                        });
                                });
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};


controller.Save = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        // console.log(data);
        req.getConnection((err, conn) => {
            var id = data.id_dg;
            conn.query("SELECT id_cp,id_dg,id_dsc,name_cookietype,approve,detail_cookie FROM domain_setting_cookiepolicy as dscp LEFT JOIN domaingroup as d ON dscp.domain_id=d.id_dg LEFT JOIN cookiepolicy as cp ON dscp.cookiepolicy_id=cp.id_cp WHERE d.id_dg=?", [id],
                (err, select_cookiepolicy) => {
                    conn.query("SELECT height,id_dl,id_dg,id_st,id_dsp,namelocation,nameclass,detail_dialog,namedomain_dg FROM domain_style_policy as dsp LEFT JOIN domaingroup as d  ON dsp.domaingroup_id=d.id_dg  LEFT JOIN dialog as dl ON dsp.dialog_id=dl.id_dl LEFT JOIN styles as s ON dsp.style_id=s.id_st WHERE d.id_dg=?", [id],
                        (err, select_domaingroup) => {

                            //*----------- ปรับเเต่งค่าประเภทคุกกี้ -------------*//
                            var id_dsc = data.id_dsc;
                            var approve_cp = data.approve_cp;
                            var name_cp_append = data.name_cp_append;

                            if (data.name_cp_append) {
                                //*-----------start insert เพิ่มค่าCookieจัดเก็บ -------------*//
                                for (var i = 0; i < name_cp_append.length; i++) {
                                    conn.query("INSERT INTO cookiepolicy SET name_cp = ?,detail_cp=?",
                                        [data.name_cp_append[i], data.detail_policy_append[i]], (err, insertstyles) => {
                                        });
                                }
                            } else {
                                //*----------- update ปรับเเต่งค่าประเภทคุกกี้ -------------*//
                                for (var i = 0; i < id_dsc.length; i++) {
                                    conn.query("UPDATE  domain_setting_cookiepolicy SET name_cookietype = ?,detail_cookie=? WHERE id_dsc= ? ",
                                        [data.name_cp[i], data.detail_policy[i], data.id_dsc[i]], (err, update_cookiepolicy) => {
                                        });
                                }

                                //*----------- update อนุญาต cookie_policy -------------*//
                                if (approve_cp) {
                                    conn.query("update domain_setting_cookiepolicy set approve=0   WHERE domain_id=? ", [id], (err, update_approve) => {
                                    });
                                    for (var i = 0; i < approve_cp.length; i++) {
                                        conn.query("UPDATE  domain_setting_cookiepolicy SET approve=1 WHERE id_dsc= ? ",
                                            [approve_cp[i]], (err, update_cookiepolicy_approve) => {
                                            });
                                    }
                                } else {
                                    conn.query("update domain_setting_cookiepolicy set approve=0   WHERE domain_id=? ", [id], (err, update_approve) => {
                                    });
                                }
                            }
                            // //*-----------END ปรับเเต่งค่าประเภทคุกกี้ -------------*//


                            //*-----------START  INSERT styles ของตำเเหน่งกับสี theme -------------*//
                            if (data.id_st == "") {
                                //*-----------  INSERT styles ที่เลือกมาinsert (กรณีที่เริ่มต้นเลย) -------------*//
                                // var class_css = "btn";
                                var namelocation = data.namelocation;
                                conn.query("INSERT INTO styles SET nameclass = ?,namelocation=? ,height=?",
                                    [data.class, namelocation, data.height], (err, insertstyles) => {
                                        conn.query("SELECT * FROM styles ORDER BY id_st DESC ", (err, select_style) => {
                                            conn.query("UPDATE  domain_style_policy SET style_id = ? WHERE domaingroup_id=?",
                                                [select_style[0].id_st, data.id_dg], (err, update_domain_style_policy) => {
                                                });
                                        });
                                    });
                            } else {
                                //*-----------  upate styles -------------*//
                                conn.query(
                                    'UPDATE  styles SET nameclass=?,namelocation=?,height=? WHERE id_st=? ',
                                    [data.class, data.namelocation, data.height, data.id_st], (err, updatestyle) => {
                                        //เก็บ Log_theme
                                        var class_theme = "";
                                        if (data.class == "btn-dark") {
                                            class_theme = "ดำ";
                                        } else {
                                            class_theme = "ขาว";
                                        }
                                        if (select_domaingroup[0].nameclass != data.class) {
                                            conn.query("INSERT INTO log_history SET log_action ='สีธีม' ,log_detail=?,domain_id=?,user_id=?",
                                                [class_theme, id, req.session.userid], (err, log_history) => {

                                                });
                                        }
                                        //เก็บ log_ตำเเหน่ง
                                        var location = "";
                                        if (data.namelocation == "under") {
                                            location = "ด้านล่าง";
                                        } else if (data.namelocation == "top") {
                                            location = "ด้านบน";
                                        } else {
                                            location = "ตรงกลาง";
                                        }
                                        if (select_domaingroup[0].namelocation != data.namelocation) {
                                            conn.query("INSERT INTO log_history SET log_action ='ตำเเหน่ง' ,log_detail=?,domain_id=?,user_id=?",
                                                [location, id, req.session.userid], (err, log_history) => {

                                                });
                                        }
                                    });
                            }
                            //*-----------END INSERT class ของตำเเหน่งกับสี theme -------------*//

                            //*-----------START ส่วนของ Domaingroup -------------*//


                            if (data.dialogs != "") {
                                conn.query(
                                    'UPDATE  domain_style_policy SET detail_dialog=? WHERE domaingroup_id=? ', [data.dialogs, data.id_dg], (err, update_dialog) => {
                                    });
                            }

                            conn.query("SELECT * FROM domaingroup ORDER BY id_dg DESC ", (err, id_domain) => {

                                res.redirect('/dialogs' + "/" + id);
                                update_style_file(id, req, conn)
                            });
                            //*-----------END ส่วนของ Domaingroup -------------*//
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};


function update_style_file(id, req) {
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM  domain_setting_cookiepolicy where domain_id=?;",
            [id], (err, domain_setting_cookiepolicy) => {
                conn.query("SELECT * FROM domain_style_policy as dsp  LEFT JOIN styles as st ON dsp.style_id=st.id_st LEFT JOIN domaingroup as dg ON dsp.domaingroup_id=dg.id_dg where domaingroup_id=?;",
                    [id], (err, domain_style_policy) => {
                        var domain = (domain_style_policy[0].namedomain_dg.replace("https://www.", "").replace(".com", "")) + "_" + id + ".html"

                        var html_file = `
                            <div class="modal-dialog modal-lg shadow" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h4 class="modal-title">การตั้งค่าคุกกี้</h4>                      
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-body btn ${domain_style_policy[0].nameclass}" style="width: 100%;">
                                        <p style="text-align: left;">
                                            ${domain_style_policy[0].detail_dialog} 
                                        </p>
                                        </div>
                                    <br>
                                    <div id="bccs-options" class="collapse">
                                        <div class="bccs-option">
                                        <table class="table mt-4">
                                        <tbody> 
                                        <hr>`;
                        for (var i = 0; i < domain_setting_cookiepolicy.length; i++) {
                            if (i + 1 != domain_setting_cookiepolicy.length) {
                                html_file += `
                            <tr>
                            <td>
                              <p class="text_color_p">
                              ${domain_setting_cookiepolicy[i].name_cookietype}
                              </p>
                            </td>
                            <td style="width: 76%">
                              <p class="text_color_p">
                              ${domain_setting_cookiepolicy[i].detail_cookie}
                              </p>
                            </td>
                            <td style="width: 11%;">
                              <div class="active_buttom">
                                <div class="form-check form-switch check_buttom">
                                  <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${i + 1}" name="cookie"
                                    value="${domain_setting_cookiepolicy[i].id_dsc},${domain_setting_cookiepolicy[i].approve}">
                                </div>
                              </div>
                              <div class="mt-2 text-center">
                                <label id="label_approve_${i + 1}">Active</label>
                              </div>
                            </td>
                          </tr>`;
                            } else {
                                html_file += `
                            <tr class="tr_none">
                            <td>
                              <p class="text_color_p">
                              ${domain_setting_cookiepolicy[i].name_cookietype}
                              </p>
                            </td>
                            <td style="width: 76%">
                              <p class="text_color_p">
                              ${domain_setting_cookiepolicy[i].detail_cookie}
                              </p>
                            </td>
                            <td style="width: 11%;">
                              <div class="active_buttom">
                                <div class="form-check form-switch check_buttom">
                                  <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${i + 1}" name="cookie"
                                    value="${domain_setting_cookiepolicy[i].id_dsc},${domain_setting_cookiepolicy[i].approve}">
                                </div>
                              </div>
                              <div class="mt-2 text-center">
                                <label id="label_approve_${i + 1}">Active</label>
                              </div>
                            </td>
                          </tr>`;
                            }
                        };
                        html_file += `
                        </tbody>
                        </table>
                        </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                      <a href="#bccs-options" data-bs-toggle="collapse">เลือกประเภทคุกกี้</a>
                      <button id="bccs-buttonDoNotAgree" type="button" class="btn btn-danger text-decoration-none">
                        ปฏิเสธ
                      </button>
                      <button id="bccs-buttonAgree" type="button" class="btn btn-success">อนุญาต</button>
                      <button id="bccs-buttonSave" type="button" class="btn btn-primary">
                        อนุญาตตามที่เลือก
                      </button>
                      <button id="bccs-buttonAgreeAll" type="button" class="btn btn-success">อนุญาตทั้งหมด</button>
                    </div>
                    </div>
                  </div>
                  <style>
                        .text_color_p {
                            color: #6d7175;
                        }
                        .active_buttom {
                            align-self: center;
                            display: flex;
                            justify-content: center;
                        }
                        .check_buttom {
                            font-size: x-large;
                        }
                        .tr_none td {
                            border: none;
                        }
                        table tr {
                            height: 6rem;
                        }
                        </style>`;

                        console.log(domain);
                        fs.writeFile('./dist/file_domain/' + domain, html_file, function (err, file) {
                            if (err) throw err;
                        });
                    });
            });

    });
}


controller.testdomain = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM  domaingroup as dg LEFT JOIN styles as sty ON dg.styles_id=sty.id_st",
                (err, domaingroup) => {
                    res.render("cookie/views/view_cookie_managemet/cookieuser", {
                        domaingroup: domaingroup[0],
                        session: req.session

                    });
                });
        });
    } else {
        res.redirect("/");
    }
};


controller.group_domain = (req, res) => {
    if (req.session.userid) {
        const id = req.params.id;
        const id_d = req.params.id_d;

        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM domain_setting_tag WHERE domaingroup_id=? and tag_id=?", [id_d, id],
                (err, select_setting_tag) => {
                    if (select_setting_tag[0]) {
                        conn.query(" DELETE FROM domain_setting_tag WHERE domaingroup_id=? and tag_id=?", [id_d, id],
                            (err, domain_setting_tag) => {
                                var id = id_d;
                                res.send(id)
                            });
                    } else {
                        conn.query("INSERT INTO domain_setting_tag SET  domaingroup_id=?,tag_id=?", [id_d, id],
                            (err, domain_setting_tag) => {
                                var success = "success";
                                res.send(success)
                            });
                    }
                });
        });
    } else {
        res.redirect("/");
    }
}


controller.cookieuser = (req, res) => {
    if (req.session.userid) {
        const url = req.params;
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/cookieuser", {
                url,
                session: req.session,
            });
        });
    } else {
        res.redirect("/");
    }
}


controller.settings_system = (req, res) => {
    if (req.session.userid) {
        const url = req.params;
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.membership_system = (req, res) => {
    if (req.session.userid) {
        const url = req.params;
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.monitoring = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session,
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.Protect_mark = (req, res) => {
    if (req.session.user) {
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.export_data = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.export_data_search = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.export_data_setup_export = (req, res) => {
    if (req.session.userid) {
        const url = req.params;
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.import_data = (req, res) => {
    if (req.session.userid) {
        const url = req.params;
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.management_Cookie = (req, res) => {
    if (req.session.userid) {
        const url = req.params;
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.management_Email = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session,
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.management_Customer = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            res.render("cookie/views/view_cookie_managemet/under_construction", {
                session: req.session,
            });
        });
    } else {
        res.redirect("/");
    }
}

controller.User_report = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            res.render("cookie/cookie/views/view_cookie_managemet/under_construction", {
                session: req.session,
            });
        });
    } else {
        res.redirect("/");
    }
}




module.exports = controller;