const controller = {};
const { json } = require('body-parser');
const { validationResult } = require('express-validator');
const { lchown } = require('fs');

function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())
    date = formatted_date + ' ' + formatted_time;
    return date;
}
controller.list = (req, res) => {
    const data = null;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT `group`.`group_id`,`group`.`g_name`,COUNT(`user_member`.`acc_id`) as num,user_member.acc_id FROM `group` LEFT JOIN user_member ON user_member.group_id=`group`.group_id GROUP BY `group`.`group_id` ORDER BY group_id', (err, group_list) => {
                conn.query('SELECT * FROM `account`', (err, account_list) => {
                    conn.query('SELECT group_id,COUNT(*) AS num FROM `user_member` GROUP BY group_id', (err, group2_list) => {
                        conn.query('SELECT `group`.`group_id`,`group`.`g_name`,COUNT(`device_member`.`dm_id`) as num,device_member.dm_id FROM `group` LEFT JOIN device_member ON device_member.group_id=`group`.group_id GROUP BY `group`.`group_id` ORDER BY group_id', (err, davice_list) => {

                            res.render('./group/group_list', {
                                data: group_list,
                                data3: group2_list,
                                data2: account_list,
                                data4: davice_list,
                                session: req.session
                            });
                        });
                    });
                });
            });
        });
    }
};
controller.new = (req, res) => {
    const { id } = req.params;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM account WHERE admin != 1 AND acc_id NOT IN (SELECT acc_id FROM user_member WHERE group_id = ? )AND acc_id NOT IN (SELECT acc_id FROM `del_acc`)', [id], (err, account_list) => {
                conn.query('SELECT * FROM `device` WHERE device_id NOT IN (SELECT de_id FROM `device_member` WHERE group_id = ?)', [id], (err, device_list) => {
                    conn.query('SELECT * FROM `group` WHERE `group`.`group_id`=?', [id], (err, group_list) => {
                        conn.query('SELECT * FROM `user_member` JOIN `account` ON account.acc_id=user_member.acc_id WHERE `user_member`.`group_id`=?', [id], (err, user_member) => {
                            conn.query('SELECT * FROM `device_member` JOIN device ON device_member.de_id=device.device_id WHERE `device_member`.`group_id`=?', [id], (err, device_member) => {

                                res.render('./group/group_new', {
                                    data: account_list,
                                    data2: device_list,
                                    data3: group_list,
                                    data4: user_member,
                                    data5: device_member,
                                    session: req.session
                                });
                            });
                        });
                    });
                });
            });
        });
    }
};

controller.adduser = (req, res) => {

    const data = req.body;
    //res.json(data);
    const errors = validationResult(req);
    console.log("data", data);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/admin/new');
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";

            var pushacc_id = []
            req.getConnection((err, conn) => {

                conn.query('SELECT * FROM `user_member` WHERE `group_id`= ? ', [data.group_id[1]], (err, group) => {
                    for (var i = 1; i < (data.acc_id).length; i++) {
                        var FOUND1 = group.find(function(post, index) {
                            if (post.acc_id == data.acc_id[i])
                                return true;
                        });
                        console.log(FOUND1);
                        if (FOUND1) {
                            console.log("data.acc_id[i]");
                        } else {
                            console.log("INSERT");
                            conn.query('INSERT INTO `user_member` (`um_id`, `acc_id`, `group_id`) VALUES (NULL, ?,?);', [data.acc_id[i], data.group_id[1]], (err, group) => {
                                if (err) {
                                    res.json(err);
                                }
                            });

                        }
                    }
                });
                conn.query('SELECT * FROM `device_member` WHERE `group_id`= ? ', [data.group_id[1]], (err, group) => {
                    for (var j = 1; j < (data.de_id).length; j++) {
                        var FOUND2 = group.find(function(post, index) {
                            if (post.de_id == data.de_id[j])
                                return true;

                        });
                        console.log(FOUND2);
                        if (FOUND2) {
                            console.log("data.de_id[i]");
                        } else {
                            console.log("INSERT", data.de_id[j]);
                            conn.query('INSERT INTO `device_member` (`dm_id`, `de_id`, `group_id`) VALUES (NULL, ?, ?);', [data.de_id[j], data.group_id[1]], (err, group) => {
                                if (err) {
                                    res.json(err);
                                }
                            });

                        }
                    }
                });
                var getde_id = []
                var getacc_id = []
                for (var j = 1; j < (data.de_id).length; j++) {
                    getde_id.push(data.de_id[j])
                }
                for (var i = 1; i < (data.acc_id).length; i++) {
                    getacc_id.push(data.acc_id[i])
                }
                console.log("push getde_id", getde_id);
                console.log("push getacc_id", getacc_id);

                if (getde_id.length > 0) {
                    conn.query('DELETE FROM `device_member` WHERE de_id NOT IN (?) AND group_id = ?', [getde_id, data.group_id[1]], (err, DELETE2) => {
                        if (err) {
                            res.json(err);
                        }
                    });

                } else {
                    conn.query('DELETE FROM `device_member` WHERE group_id = ?', [data.group_id[1]], (err, DELETE2) => {
                        if (err) {
                            res.json(err);
                        }
                    });

                }
                if (getacc_id.length > 0) {
                    conn.query('DELETE FROM `user_member` WHERE acc_id NOT IN (?) AND group_id = ?', [getacc_id, data.group_id[1]], (err, DELETE) => {
                        if (err) {
                            res.json(err);
                        }
                    });
                } else {
                    conn.query('DELETE FROM `user_member` WHERE group_id = ?', [data.group_id[1]], (err, DELETE2) => {
                        if (err) {
                            res.json(err);
                        }
                    });

                }




                res.redirect('/group/list');

            });
        }
    }
};
controller.add = (req, res) => {
    const data = req.body;
    const errors = validationResult(req);
    let val = Object.keys(data)[0];
    const myval = val.split('"');
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO `group` (`group_id`, `g_name`) VALUES (NULL, ?)', [myval[1]], (err, admin_add) => {
            console.log(admin_add);
            if (err) {
                res.json(err);
            }
            res.redirect('/group/list');
        });
    });
};

controller.check = (req, res) => {
    const { id } = req.params;
    //res.json(id);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `group`as g JOIN user_member as u ON u.group_id=g.group_id JOIN account as a ON a.acc_id=u.acc_id WHERE g.group_id = ? GROUP BY u.acc_id', [id], (err, acc_list) => {
                conn.query('SELECT * FROM `group`as g JOIN device_member as dm ON dm.group_id=g.group_id JOIN device as d ON d.device_id=dm.de_id WHERE g.group_id = ? GROUP BY d.device_id', [id], (err, device_list) => {

                    if (err) {
                        res.json(err);
                    }
                    res.render('./group/group_check', {
                        data: acc_list,
                        data2: device_list,
                        session: req.session
                    });
                });
            });
        });
    }
};

module.exports = controller;