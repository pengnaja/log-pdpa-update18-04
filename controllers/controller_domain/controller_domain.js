const { log } = require("console");
const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');
// var  path = require('../../views/view_CookieConsent/content');
// console.log(path);

const controller = {};

//  var fs = require('fs');
//**------------------------------------- add-domain --------------------------------------**//

controller.adddomain = (req, res) => {
  if (req.session.userid) {
    const data = req.body;
    req.getConnection((err, conn) => {

      // var test = (data.namedomain_dg.replace(/^https?\:\/\//i, "")).replaceAll(".", "_") + "." + "html"; // output =  www_buildingware_cf.html
      // console.log(test);
      // console.log(test.replaceAll(".","_"));

      var urlNoProtocol = data.namedomain_dg.replace(/^https?\:\/\//i, "");
      var url = urlNoProtocol.replace("www.", "");
      var url = url.replace(".com", "");
      conn.query("INSERT INTO domaingroup SET namedomain_dg = ?,message=? ,status_dg=1",
        [data.namedomain_dg, data.message], (err, insert_domaingroup) => {
          conn.query("SELECT * FROM domaingroup ORDER BY id_dg DESC", (err, select_domaingroup) => {
            conn.query("SELECT * FROM cookiepolicy", (err, select_cookiepolicy) => {
              var data_cookirpolicy = select_cookiepolicy;
              var data_domaingroup = select_domaingroup[0];
              // console.log(select_domaingroup);
              for (var i = 0; i < select_cookiepolicy.length; i++) {
                conn.query("INSERT INTO domain_setting_cookiepolicy SET domain_id=?,cookiepolicy_id=?,name_cookietype=?,detail_cookie=?,approve=?",
                  [data_domaingroup.id_dg, data_cookirpolicy[i].id_cp, data_cookirpolicy[i].name_cp, data_cookirpolicy[i].detail_cp, data_cookirpolicy[i].approve_cp], (err, insert_domain_setting_cookiepolicy) => {
                  });
              }
              //-----------------------การเพิ่ม Dialog กับเเต่ละdomain---------------------------///

              conn.query("SELECT * FROM dialog", (err, select_dialog) => {
                conn.query("INSERT INTO domain_style_policy SET domaingroup_id = ?,dialog_id=?,detail_dialog=?",
                  [data_domaingroup.id_dg, select_dialog[0].id_dl, select_dialog[0].dialog_dl], (err, insert_domaingroup) => {
                  });
              });
              conn.query("INSERT INTO log_history SET log_action = 'เพิ่มโดเมน',log_detail=?,user_id=?",
                [data.namedomain_dg, req.session.user_id_u], (err, insert_log_history) => {
                });

              // console.log(select_domaingroup[0].id_dg);
              conn.query("SELECT * FROM  domain_setting_cookiepolicy where domain_id=?;",
                [select_domaingroup[0].id_dg], (err, domain_setting_cookiepolicy) => {
                  conn.query("SELECT * FROM  domain_style_policy where domaingroup_id=?;",
                    [select_domaingroup[0].id_dg], (err, domain_style_policy) => {

                      var html_file = `
                                            <div class="modal-dialog modal-lg shadow" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h4 class="modal-title">การตั้งค่าคุกกี้</h4>                      
                                              </div>
                                              <div class="modal-body">
                                                  <div class="card-body btn " style="width: 100%;">
                                                    <p style="text-align: left;">
                                                      ${domain_style_policy[0].detail_dialog} 
                                                    </p>
                                                  </div>
                                                <br>
                                                <div id="bccs-options" class="collapse">
                                                  <div class="bccs-option">
                                                  <table class="table mt-4">
                                                  <tbody> 
                                                  <hr>`;

                      for (var i = 0; i < domain_setting_cookiepolicy.length; i++) {
                        if (i + 1 != domain_setting_cookiepolicy.length) {
                          html_file += `
                                                    <tr>
                                                    <td>
                                                      <p class="text_color_p">
                                                      ${domain_setting_cookiepolicy[i].name_cookietype}
                                                      </p>
                                                    </td>
                                                    <td style="width: 76%">
                                                      <p class="text_color_p">
                                                      ${domain_setting_cookiepolicy[i].detail_cookie}
                                                      </p>
                                                    </td>
                                                    <td style="width: 11%;">
                                                      <div class="active_buttom">
                                                        <div class="form-check form-switch check_buttom">
                                                          <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${i + 1}" name="cookie"
                                                            value="${domain_setting_cookiepolicy[i].id_dsc},${domain_setting_cookiepolicy[i].approve}">
                                                        </div>
                                                      </div>
                                                      <div class="mt-2 text-center">
                                                        <label id="label_approve_${i + 1}">Active</label>
                                                      </div>
                                                    </td>
                                                  </tr>`;
                        } else {
                          html_file += `
                                                    <tr class="tr_none">
                                                    <td>
                                                      <p class="text_color_p">
                                                      ${domain_setting_cookiepolicy[i].name_cookietype}
                                                      </p>
                                                    </td>
                                                    <td style="width: 76%">
                                                      <p class="text_color_p">
                                                      ${domain_setting_cookiepolicy[i].detail_cookie}
                                                      </p>
                                                    </td>
                                                    <td style="width: 11%;">
                                                      <div class="active_buttom">
                                                        <div class="form-check form-switch check_buttom">
                                                          <input type="checkbox" class="form-check-input" id="bccs-checkboxNecessary_${i + 1}" name="cookie"
                                                            value="${domain_setting_cookiepolicy[i].id_dsc},${domain_setting_cookiepolicy[i].approve}">
                                                        </div>
                                                      </div>
                                                      <div class="mt-2 text-center">
                                                        <label id="label_approve_${i + 1}">Active</label>
                                                      </div>
                                                    </td>
                                                  </tr>`;
                        }
                      };
                      html_file += `
                                            </tbody>
                                            </table>
                                            </div>
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                          <a href="#bccs-options" data-bs-toggle="collapse">เลือกประเภทคุกกี้</a>
                                          <button id="bccs-buttonDoNotAgree" type="button" class="btn btn-danger text-decoration-none">
                                            ปฏิเสธ
                                          </button>
                                          <button id="bccs-buttonAgree" type="button" class="btn btn-success">อนุญาต</button>
                                          <button id="bccs-buttonSave" type="button" class="btn btn-primary">
                                            อนุญาตตามที่เลือก
                                          </button>
                                          <button id="bccs-buttonAgreeAll" type="button" class="btn btn-success">อนุญาตทั้งหมด</button>
                                        </div>
                                        </div>
                                      </div>
                                      <style>
                                            .text_color_p {
                                                color: #6d7175;
                                            }
                                            .active_buttom {
                                                align-self: center;
                                                display: flex;
                                                justify-content: center;
                                            }
                                            .check_buttom {
                                                font-size: x-large;
                                            }
                                            .tr_none td {
                                                border: none;
                                            }
                                            table tr {
                                                height: 6rem;
                                            }
                                            </style>`;
                      fs.appendFile('./dist/file_domain/' + url + "_" + data_domaingroup.id_dg + '.html', html_file
                        , function (err) {
                          if (err) throw err;
                        });
                    });
                });
            });
            res.redirect('/home');
          });
        });
    });
  } else {
    res.redirect("/");
  }
};


//**------------------------------------- delete-domain --------------------------------------**//
// controller.delete = (req, res) => {
//     if (req.session.user) {
//         var { id } = req.params;
//         req.getConnection((err, conn) => {
//             conn.query('SELECT * FROM domain_style_policy as dsp LEFT JOIN styles as s ON dsp.style_id=s.id_st LEFT JOIN domaingroup as dg ON dsp.domaingroup_id=dg.id_dg WHERE dsp.domaingroup_id=?',
//                 [id], (err, select_styles) => {
//                     conn.query("SELECT * FROM domaingroup WHERE id_dg =?", [id],
//                         (err, SELECT_domaingroup) => {
//                             conn.query('DELETE FROM domain_setting_tag WHERE domaingroup_id = ? ',
//                                 [id], (err, domain_setting_tag) => {
//                                     conn.query('DELETE FROM domain_setting_cookiepolicy WHERE domain_id= ? ',
//                                         [id], (err, delete_domaingroup) => {
//                                             conn.query("UPDATE  log_history SET domain_id=null WHERE domain_id=?",
//                                                 [id], (err, delete_log_history) => {
//                                                     conn.query('DELETE FROM domain_style_policy WHERE domaingroup_id= ? ',
//                                                         [id], (err, delete_domain_style_policy) => {
//                                                             conn.query('DELETE FROM domaingroup WHERE id_dg = ? ',
//                                                                 [id], (err, delete_domaingroup) => {
//                                                                     conn.query('DELETE FROM styles WHERE id_st = ? ',
//                                                                         [select_styles[0].id_st], (err, delete_styles) => {
//                                                                             conn.query("INSERT INTO log_history SET log_action = 'ลบโดเมน',log_detail=?,user_id=?",
//                                                                                 [select_styles[0].namedomain_dg, req.session.user_id_u], (err, insert_log_history) => {
//                                                                                     var urlNoProtocol = SELECT_domaingroup[0].namedomain_dg.replace(/^https?\:\/\//i, "");
//                                                                                     var url = urlNoProtocol.replace("www.", "");
//                                                                                     var url = url.replace(".com", "");
//                                                                                     if (id) {
//                                                                                         fs.unlink('./dist/file_domain/' + url + "_" + id + '.html', function (err) {
//                                                                                             if (err) throw err;
//                                                                                             console.log('File deleted!');
//                                                                                         });
//                                                                                     }
//                                                                                     setTimeout(() => {
//                                                                                         res.redirect('/home');
//                                                                                     }, 500);
//                                                                                 });
//                                                                         });
//                                                                 });
//                                                         });
//                                                 });
//                                         });
//                                 });
//                         });
//                 });
//         });
//     } else {
//         res.redirect("/");
//     }
// };

controller.delete = (req, res) => {
  if (req.session.userid) {
    var { id } = req.params;
    req.getConnection((err, conn) => {
      conn.query('UPDATE domaingroup SET status_dg=0 WHERE  id_dg=?',
        [id], (err, update_domaingroup) => {
          res.redirect("/home");
        });
    });
  }
  else {
    res.redirect("/");
  }
};
//**------------------------------------- edit-domain --------------------------------------**//
controller.edit = (req, res) => {
  if (req.session.userid) {
    const data = req.body;
    // console.log(data);
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM   domaingroup  WHERE id_dg = ? ',
        [data.id_dg], (err, select_domaingroup) => {
          // console.log(select_domaingroup);
          if (data.namedomain_dg != select_domaingroup[0].namedomain_dg) {
            conn.query("INSERT INTO log_history SET log_action = 'เเก้ไขโดเมน',log_detail=?,user_id=?",
              [data.namedomain_dg, req.session.user_id_u], (err, insert_log_history) => { });
          }
          if (data.message != select_domaingroup[0].message) {
            conn.query("INSERT INTO log_history SET log_action = 'เเก้ไขข้อความ',log_detail=?,user_id=?",
              [data.message, req.session.user_id_u], (err, insert_log_history) => { });
          }
          conn.query('UPDATE  domaingroup SET namedomain_dg=?, message=? WHERE id_dg = ? ',
            [data.namedomain_dg, data.message, data.id_dg], (err, editdomaingroup) => {
              res.redirect('/home');
            });
        });
    });
  } else {
    res.redirect("/");
  }
};

module.exports = controller;