const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');

const controller = {};

controller.report_cookieconsent = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM cookiepolicy ",
                (err, cookiepolicy) => {
                    conn.query("SELECT namedomain_dg,DATE_FORMAT(policylog_date,'%Y-%m-%d %I:%i %p') as day,cookiepolicy,policylog_ip,policylog_sessionid,policylog_browser FROM pdpa_log_setting_cookiepolicy as lsc LEFT JOIN pdpa_policylog as pl ON lsc.policy_id=pl.id_policylog LEFT JOIN domaingroup as dg ON lsc.domaingroup_id=dg.id_dg",
                        (err, log_cookiepolicy) => {
                            conn.query("SELECT count(policylog_cookieconsent) as count,policylog_cookieconsent FROM pdpa_policylog  GROUP BY policylog_cookieconsent",
                                (err, pie_setting_cookiepolicy) => {
                                    res.render("cookie/views/view_report_cookieconsent/report_cookieconsent", {
                                        cookiepolicy: cookiepolicy,
                                        log_cookiepolicy: log_cookiepolicy,
                                        pie_setting_cookiepolicy: pie_setting_cookiepolicy,
                                        session: req.session
                                    });

                                });
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};


controller.api_cookiesconsent = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM cookiepolicy ",
                (err, cookiepolicy) => {
                    conn.query("SELECT id_dg,id_policylog,id_lsc,namedomain_dg,DATE_FORMAT(policylog_date,'%Y-%m-%d %I:%i %p') as day,cookiepolicy,policylog_ip,policylog_sessionid,policylog_browser FROM pdpa_log_setting_cookiepolicy as lsc LEFT JOIN pdpa_policylog as pl ON lsc.policy_id=pl.id_policylog LEFT JOIN domaingroup as dg ON lsc.domaingroup_id=dg.id_dg WHERE dg.status_dg = 1",
                        (err, log_cookiepolicy) => {
                            conn.query("SELECT count(policylog_cookieconsent) as count,policylog_cookieconsent FROM pdpa_policylog  GROUP BY policylog_cookieconsent",
                                (err, pie_setting_cookiepolicy) => {
                                    res.send({ cookiepolicy, log_cookiepolicy, pie_setting_cookiepolicy })
                                });
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};


controller.api_search_cookiesconsent = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        var search = '%' + data.data + '%';
        req.getConnection((err, conn) => {
            conn.query("SELECT * FROM cookiepolicy ",
                (err, cookiepolicy) => {
                    conn.query("SELECT id_dg,id_policylog,id_lsc,namedomain_dg,DATE_FORMAT(policylog_date,'%Y-%m-%d %I:%i %p') as day,cookiepolicy,policylog_ip,policylog_sessionid,policylog_browser FROM pdpa_log_setting_cookiepolicy as lsc LEFT JOIN pdpa_policylog as pl ON lsc.policy_id=pl.id_policylog LEFT JOIN domaingroup as dg ON lsc.domaingroup_id=dg.id_dg WHERE dg.namedomain_dg  LIKE ? ",
                        [search], (err, log_cookiepolicy) => {
                            if (log_cookiepolicy[0]) {
                                conn.query("SELECT count(policylog_cookieconsent) as count,policylog_cookieconsent FROM pdpa_policylog  GROUP BY policylog_cookieconsent",
                                    (err, pie_setting_cookiepolicy) => {
                                        res.send({ cookiepolicy, log_cookiepolicy, pie_setting_cookiepolicy })
                                    });
                            } else {
                                var data_null = "ไม่มีข้อมูล";
                                res.send(data_null)
                            }
                        });
                });
        });
    } else {
        res.redirect("/");
    }
};


module.exports = controller;
