const controller = {}
const checkDiskSpace = require('check-disk-space').default
const { validationResult } = require('express-validator');
const crypto = require('crypto')

function checkMatch(str, arr, index) {
    if (str.includes(arr[index])) {
        return true
    } else {
        checkMatch(str, arr, (index + 1))
    }
}

// Index1 Page
controller.indexPattern = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/');
        } else {
            const user = req.session.userid;
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history  from doc_pdpa_document as d  join doc_pdpa.pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query('SELECT * FROM doc_pdpa_pattern;', (err, pm) => {
                            conn.query('SELECT * FROM doc_pdpa_pattern_processing_base', (err, process) => {
                                conn.query('SELECT pd.doc_id, pd.doc_type_id, pd.doc_name, pdt.doc_type_name, pd.doc_date_create, pu.firstname, pu.lastname, pd.doc_status, pd.doc_remark, pd.doc_action FROM doc_pdpa_document as pd JOIN doc_pdpa_document_type as pdt ON pd.doc_type_id = pdt.doc_type_id JOIN account as pu ON pd.user_id = pu.acc_id WHERE doc_action is NOT TRUE AND doc_status = 2 ORDER BY doc_id ASC;', (err, policy) => {
                                    conn.query('SELECT * FROM doc_pdpa_data as pd JOIN doc_pdpa_data_type as pdt ON pd.data_type_id = pdt.data_type_id JOIN doc_pdpa_level as pl ON pd.data_level_id = pl.level_id;', (err, data) => {
                                        let word = []
                                        let word1 = []
                                        for (i in words) {
                                            word.push(words[i].words_id)
                                            word1.push(words[i].words_often)
                                        }
                                        if (err) {
                                            res.json(err)
                                        }
                                        checkDiskSpace('/').then((diskSpace) => {
                                            res.render('./pattern/index', {
                                                pm: pm,
                                                process: process,
                                                policy: policy,
                                                data: data,
                                                checkDiskSpace: diskSpace,
                                                history: history,
                                                words: words,
                                                words1: word,
                                                words2: word1,
                                                session: req.session
                                            });
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        }
    }
    // SEND TO AJAX
controller.ajaxIndexPattern = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM doc_pdpa_pattern ORDER BY pattern_id DESC;', (err, id_pattern) => {
                conn.query('SELECT * FROM doc_pdpa_pattern as pp JOIN doc_pdpa_document as pd ON pp.doc_id = pd.doc_id JOIN account as pu ON pp.acc_id = pu.acc_id JOIN doc_pdpa_pattern_processing_base as pppb ON pp.pattern_processing_base_id = pppb.pattern_processing_base_id ORDER BY pattern_id DESC;', (err, pattern) => {
                    conn.query('SELECT * FROM doc_pdpa_data', (err, data) => {
                        conn.query('SELECT * FROM doc_pdpa_document', (err, document) => {
                            count = 0
                            var tag_name = []
                            for (i in pattern) {
                                pattern[i].pattern_id = count += 1
                                tag_name.push(pattern[i].pattern_tag)
                            }
                            var data_name_total = []
                            for (i in tag_name) {
                                var data_name = []
                                for (j in data) {
                                    var data_tag_split = ""
                                    if (data[j].data_tag != null) {
                                        if (data[j].data_tag.includes(',')) {
                                            data_tag_split = data[j].data_tag.split(',')
                                        } else {
                                            data_tag_split = data[j].data_tag
                                        }
                                    }
                                    if (typeof data_tag_split == "string") {
                                        if (tag_name[i].includes(data[j].data_tag)) {
                                            data_name.push(data[j].data_name)
                                        }
                                    } else {
                                        if (checkMatch(tag_name[i], data_tag_split, 0) == true) {
                                            data_name.push(data[j].data_name)
                                        }
                                    }
                                }
                                data_name_total.push(data_name.join(','))
                            }
                            if (err) { res.json(err) } else {
                                res.json({ pattern, id_pattern, data_name_total, document })
                            }
                        })
                    })
                })
            })
        })
    }
}
controller.ajaxIndexPolicy = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM doc_pdpa_document WHERE doc_action is NOT TRUE AND doc_status = 2 ORDER BY doc_id ASC;', (err, id_document) => {
                conn.query('SELECT pd.doc_id, pdg.page_id, pdg.page_number, pdg.page_content, pdg.page_action FROM doc_pdpa_document_page as pdg JOIN doc_pdpa_document as pd ON pdg.doc_id = pd.doc_id WHERE page_action is NOT TRUE ORDER BY doc_id ASC;', (err, page) => {
                    conn.query('SELECT pd.doc_id, pd.doc_type_id, pd.doc_name, pdt.doc_type_name, pd.doc_date_create, pu.firstname, pu.lastname, pd.doc_status, pd.doc_remark, pd.doc_action FROM doc_pdpa_document as pd JOIN doc_pdpa_document_type as pdt ON pd.doc_type_id = pdt.doc_type_id JOIN account as pu ON pd.user_id = pu.acc_id WHERE doc_action is NOT TRUE AND doc_status = 2 ORDER BY doc_id ASC;', (err, policy) => {
                        count = 0
                        for (i in policy) {
                            policy[i].doc_id = count += 1
                        }
                        if (err) { res.json(err) } else {
                            res.json({ policy, id_document, page })
                        }
                    })
                })
            })
        })
    }
}
controller.ajaxSelectedPolicy = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const get_value = req.body.value
        const id = req.body.id
        const hash = crypto.createHash('sha256').update(get_value).digest('base64')
        if (hash == "MN7qA2QWlA/LDT0ZMNws0IphO8EmdJikPuqjMq3NL2g=") {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM doc_pdpa_document WHERE doc_action IS NOT TRUE AND doc_status = 2;', (err, policy) => {
                    if (err) { res.json(err) } else {
                        let select_policy = policy[parseInt(id) - 1]
                        res.json(select_policy)
                    }
                })
            })
        } else {
            console.log(req.body)
        }
    }
}
controller.ajaxAddPolicy = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const get_value = req.body.id;
        // const regex = /([\u0E00-\u0E7F]+)/gmu;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM doc_pdpa_document_page WHERE doc_id = ? AND page_action is NOT TRUE', [get_value], (err, select_page) => {
                conn.query('SELECT pd.data_id, pd.data_type_id, pd.data_level_id, pd.data_code, pd.data_name, pd.data_detail, pd.data_date_start, pd.data_date_end, pd.data_location_name, pd.data_location_detail, pd.data_tag, pdt.data_type_name, pl.level_name FROM doc_pdpa_data as pd JOIN doc_pdpa_data_type as pdt ON pd.data_type_id = pdt.data_type_id JOIN doc_pdpa_level as pl ON pd.data_level_id = pl.level_id;', (err, data) => {
                    if (err) { res.json(err) } else {
                        var data_code = []
                        for (i in select_page) {
                            if (select_page[i].page_content != null) {
                                let remove_tags = select_page[i].page_content.replace(/(<([^>]+)>)/ig, '')
                                let get_code = remove_tags.split(" ")
                                for (j in get_code) {
                                    if (get_code[j].includes('#') == true) {
                                        while (get_code[j].charAt(0) != "#") {
                                            get_code[j] = get_code[j].substring(1);
                                        }
                                        data_code.push(get_code[j])
                                    }
                                }
                            }
                        }
                        var _data_ = []
                        for (i in data_code) {
                            for (j in data) {
                                if (data_code[i] == data[j].data_code) {
                                    _data_.push(data[j])
                                }
                            }
                        }
                        res.json({ _data_, get_value })
                    }
                })
            })
        })
    }
}
controller.ajaxAddTag = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT tag_id FROM doc_pdpa_tag;', (err, id_tag) => {
                conn.query('SELECT * FROM doc_pdpa_tag;', (err, tag) => {
                    if (err) { res.json(err) } else {
                        for (var i = 0; i < tag.length; i++) {
                            tag[i].tag_id = (i + 1)
                        }
                        res.json({ tag, id_tag })
                    }
                })
            })
        })
    }
}
controller.ajaxAddProcessingBase = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const get_value = req.body;
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO doc_pdpa_pattern_processing_base SET ?', [get_value], (err, pass) => {
                if (err) { res.json(err) } else { res.json('success') }
            })
        })
    }
}
controller.ajaxAddUser = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const get_value = req.body.value
        const hash = crypto.createHash('sha256').update(get_value).digest('base64')
        if (hash == "1w+jpXjplWAW6mAsD+CCul4u4rSH8+4KhecP8Byp54E=") {
            req.getConnection((err, conn) => {
                conn.query('SELECT acc_id FROM account', (err, id_users) => {
                    conn.query('SELECT * FROM account', (err, users) => {
                        for (var i = 0; i < users.length; i++) {
                            users[i].acc_id = (i + 1)
                        }
                        if (err) { res.json(err) } else {
                            res.json({ id_users, users })
                        }
                    })
                })
            })
        } else {
            console.log(req.body)
        }
    }
}
controller.ajaxSelectedUser = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const get_value = req.body.value
        const id = req.body.id
        const hash = crypto.createHash('sha256').update(get_value).digest('base64')
        if (hash == "dSLgr7/Su48s3lyXfoVCEbFDosB5YTauq22/D348lO4=") {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM account', (err, users) => {
                    if (err) { res.json(err) } else {
                        let select_users = users[parseInt(id) - 1]
                        res.json(select_users)
                    }
                })
            })
        } else {
            console.log(req.body)
        }
    }
}
controller.ajaxDeletePattern = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            const id = req.body.id;
            const get_value = req.body.value;
            const hash = crypto.createHash('sha256').update(get_value).digest('base64')
            if (hash == "lsxuSSUAUS4IHurp/AxUc8IpYbv40qhdJZx/AvVL0DM=") {
                req.getConnection((err, conn) => {
                    conn.query('SELECT * FROM doc_pdpa_pattern as pp JOIN doc_pdpa_document as pd ON pp.doc_id = pd.doc_id JOIN account as pu ON pp.acc_id = pu.acc_id JOIN doc_pdpa_pattern_processing_base as pppb ON pp.pattern_processing_base_id = pppb.pattern_processing_base_id WHERE pp.pattern_id = ?;', [id], (err, pattern) => {
                        conn.query('SELECT * FROM doc_pdpa_data', (err, data) => {
                            count = 0
                            var tag_name = []
                            for (i in pattern) {
                                pattern[i].pattern_id = count += 1
                                tag_name.push(pattern[i].pattern_tag)
                            }
                            var data_name_total = []
                            for (i in tag_name) {
                                var data_name = []
                                for (j in data) {
                                    var data_tag_split = ""
                                    if (data[j].data_tag != null) {
                                        if (data[j].data_tag.includes(',')) {
                                            data_tag_split = data[j].data_tag.split(',')
                                        } else {
                                            data_tag_split = data[j].data_tag
                                        }
                                    }
                                    if (typeof data_tag_split == "string") {
                                        if (tag_name[i].includes(data[j].data_tag)) {
                                            data_name.push(data[j].data_name)
                                        }
                                    } else {
                                        if (checkMatch(tag_name[i], data_tag_split, 0) == true) {
                                            data_name.push(data[j].data_name)
                                        }
                                    }
                                }
                                data_name_total.push(data_name.join(','))
                            }
                            if (err) { res.json(err) } else {
                                res.json({ pattern, data_name_total })
                            }
                        })
                    })
                })
            } else {
                console.log(req.body)
            }
        }
    }
    // Index1 Add (Success_V.1)
controller.addPattern = (req, res) => {
        if (typeof req.session.userid == "undefined") {
            res.redirect('/')
        } else {
            const user = req.session.userid;
            const _pattern_type_data_file_ = req.body.pattern_type_data_file;
            var pattern_type_data_file = -1
            const _pattern_type_data_database_ = req.body.pattern_type_data_database;
            var pattern_type_data_database = -1
            const _pattern_storage_method_inside_ = req.body.pattern_storage_method_inside;
            var pattern_storage_method_inside = -1
            const _pattern_storage_method_outside_ = req.body.pattern_storage_method_outside;
            var pattern_storage_method_outside = -1
            const _pattern_storage_method_outside_device_ = req.body.pattern_storage_method_outside_device;
            var pattern_storage_method_outside_device = -1
            const _pattern_storage_method_outside_agent_ = req.body.pattern_storage_method_outside_agent;
            var pattern_storage_method_outside_agent = -1
            const _pattern_storage_method_outside_database_outside_ = req.body.pattern_storage_method_outside_database_outside;
            var pattern_storage_method_outside_database_outside = -1
            const _pattern_processor_inside_ = req.body.pattern_processor_inside;
            var pattern_processor_inside = -1
            const _pattern_processor_outside_ = req.body.pattern_processor_outside;
            var pattern_processor_outside = -1
            const _pattern_set_end_date_ = req.body.pattern_set_end_date;
            var pattern_set_end_date = -1
            const _pattern_dpo_approve_process_ = req.body.pattern_dpo_approve_process
            var pattern_dpo_approve_process = -1
            const _pattern_dpo_approve_raw_file_out_ = req.body.pattern_dpo_approve_raw_file_out
            var pattern_dpo_approve_raw_file_out = -1
            const _pattern_type_data_file_of_path_ = req.body.pattern_type_data_file_of_path
            var pattern_type_data_file_of_path = -1
            if (typeof _pattern_type_data_file_ == 'undefined') {
                pattern_type_data_file = 0
            } else {
                pattern_type_data_file = 1
            }
            if (typeof _pattern_type_data_database_ == 'undefined') {
                pattern_type_data_database = 0
            } else {
                pattern_type_data_database = 1
            }
            if (typeof _pattern_storage_method_inside_ == 'undefined') {
                pattern_storage_method_inside = 0
            } else {
                pattern_storage_method_inside = 1
            }
            if (typeof _pattern_storage_method_outside_ == 'undefined') {
                pattern_storage_method_outside = 0
            } else {
                pattern_storage_method_outside = 1
            }
            if (typeof _pattern_storage_method_outside_device_ == "undefined") {
                pattern_storage_method_outside_device = 0
            } else {
                pattern_storage_method_outside_device = 1
            }
            if (typeof _pattern_storage_method_outside_agent_ == 'undefined') {
                pattern_storage_method_outside_agent = 0
            } else {
                pattern_storage_method_outside_agent = 1
            }
            if (typeof _pattern_storage_method_outside_database_outside_ == "undefined") {
                pattern_storage_method_outside_database_outside = 0
            } else {
                pattern_storage_method_outside_database_outside = 1
            }
            if (typeof _pattern_processor_inside_ == 'undefined') {
                pattern_processor_inside = 0
            } else {
                pattern_processor_inside = 1
            }
            if (typeof _pattern_processor_outside_ == 'undefined') {
                pattern_processor_outside = 0
            } else {
                pattern_processor_outside = 1
            }
            if (typeof _pattern_set_end_date_ == 'undefined') {
                pattern_set_end_date = 0
            } else {
                pattern_set_end_date = 1
            }
            if (typeof _pattern_dpo_approve_process_ == 'undefined') {
                pattern_dpo_approve_process = 0
            } else {
                pattern_dpo_approve_process = 1
            }
            if (typeof _pattern_dpo_approve_raw_file_out_ == 'undefined') {
                pattern_dpo_approve_raw_file_out = 0
            } else {
                pattern_dpo_approve_raw_file_out = 1
            }
            if (typeof _pattern_type_data_file_of_path_ == 'undefined') {
                pattern_type_data_file_of_path = 0
            } else {
                pattern_type_data_file_of_path = req.body.pattern_type_data_file_of_path
            }
            const body = {
                "pattern_name": req.body.pattern_name,
                'doc_id': req.body.doc_id,
                "pattern_tag": req.body.pattern_tag,
                "pattern_label": req.body.pattern_label,
                "pattern_start_date": req.body.pattern_start_date,
                "pattern_total_date": req.body.pattern_total_date,
                "acc_id": user,
                "pattern_type_data_file": pattern_type_data_file,
                "pattern_type_data_file_of_path": pattern_type_data_file_of_path,
                "pattern_type_data_file_path": req.body.pattern_type_data_file_path,
                "pattern_type_data_database": pattern_type_data_database,
                "pattern_type_data_database_name": req.body.pattern_type_data_database_name,
                "pattern_storage_method_inside": pattern_storage_method_inside,
                "pattern_storage_method_outside": pattern_storage_method_outside,
                "pattern_storage_method_outside_name": req.body.pattern_storage_method_outside_name,
                "pattern_storage_method_outside_device": pattern_storage_method_outside_device,
                "pattern_storage_method_outside_device_name": req.body.pattern_storage_method_outside_device_name,
                "pattern_storage_method_outside_agent": pattern_storage_method_outside_agent,
                "pattern_storage_method_outside_agent_name": req.body.pattern_storage_method_outside_agent_name,
                "pattern_storage_method_outside_database_outside": pattern_storage_method_outside_database_outside,
                "pattern_storage_method_outside_database_outside_name": req.body.pattern_storage_method_outside_database_outside_name,
                "pattern_processing_base_id": req.body.pattern_processing_base_id,
                "pattern_processor_inside": pattern_processor_inside,
                "pattern_processor_inside_total": req.body.pattern_processor_inside_total,
                "pattern_processor_inside_id": req.body.pattern_processor_inside_id,
                "pattern_processor_outside": pattern_processor_outside,
                "pattern_processor_outside_total": req.body.pattern_processor_outside_total,
                "pattern_processor_outside_id": req.body.pattern_processor_outside_id,
                "pattern_set_end_date": pattern_set_end_date,
                "pattern_set_end_date_total": req.body.pattern_set_end_date_total,
                "pattern_dpo_approve_process": pattern_dpo_approve_process,
                "pattern_dpo_approve_raw_file_out": pattern_dpo_approve_raw_file_out
            }
            req.getConnection((err, conn) => {
                if (body.doc_id != "") {
                    conn.query('INSERT INTO doc_pdpa_pattern SET ?;', [body], (err, pass) => {
                        if (err) { res.json(err) } else {
                            res.redirect(req.get('referer'))
                        }
                    })
                } else {
                    res.redirect(req.get('referer'))
                }
            })
        }
    }
    // Detail from Index1 
controller.detailPattern = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const user = req.session.userid
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from doc_pdpa_document as d join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words;', (err, words) => {
                    conn.query('SELECT * FROM doc_pdpa_pattern as pp JOIN doc_pdpa_document as pd ON pp.doc_id = pd.doc_id JOIN account as pu ON pp.acc_id = pu.acc_id JOIN doc_pdpa_pattern_processing_base as pppb ON pp.pattern_processing_base_id = pppb.pattern_processing_base_id WHERE pattern_id = ?', [id], (err, pattern) => {
                        conn.query('SELECT * FROM account', (err, users) => {
                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            let total_users = pattern[0].pattern_processor_inside_id.split(',')
                            let total_name = ""
                            if (typeof total_users != 'object') {
                                console.log('ok')
                                total_name = users.filter(function(item) { return item.acc_id == total_users })
                            } else {
                                total_name = users.filter(function(item) { return total_users.toString().indexOf(item.acc_id) != -1 })
                            }
                            let total_users1 = pattern[0].pattern_processor_outside_id.split(',')
                            let total_name1 = ""
                            if (typeof total_users1 != 'object') {
                                total_name1 = users.filter(function(item) { return item.acc_id == total_users1 })
                            } else {
                                total_name1 = users.filter(function(item) { return total_users1.toString().indexOf(item.acc_id) != -1 })
                            }
                            if (err) { res.json(err) } else {
                                res.render('./pattern/detail', {
                                    pattern: pattern,
                                    users_inside: total_name,
                                    users_outside: total_name1,
                                    history: history,
                                    words: words,
                                    words1: word,
                                    words2: word1,
                                    session: req.session
                                })
                            }
                        })
                    })
                })
            })
        })
    }
}
controller.deletePattern = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            const { id } = req.params;
            req.getConnection((err, conn) => {
                conn.query('DELETE FROM doc_pdpa_classification WHERE pattern_id = ?', [id], (err, pass) => {
                    if (err) { res.json(err) }
                })
                conn.query('DELETE FROM doc_pdpa_pattern WHERE pattern_id = ?', [id], (err, pass) => {
                    if (err) { res.json(err) } else {
                        res.redirect(req.get('referer'))
                    }
                })
            })
        }
    }
    // Statistics (All)
controller.datatypePattern = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            const user = req.session.userid;
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history  from doc_pdpa_document as d  join doc_pdpa.pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        // conn.query('SELECT pm.pm_id, pm.pm_name, pd.data_name, pm.pm_start_time, pm.pm_used_time, pm.pm_total_time, pu.user_fullname, pm.pm_device_agent FROM pdpa_policy_management as pm JOIN pdpa_user as pu ON pm.user_id = pu.user_id JOIN pdpa_policy_management_data as pmd ON pmd.pm_id = pm.pm_id JOIN pdpa_data as pd ON pmd.data_id = pd.data_id;', (err, pm) => {
                        //     conn.query('SELECT * FROM pdpa_data', (err, data) => {
                        let word = []
                        let word1 = []
                        for (i in words) {
                            word.push(words[i].words_id)
                            word1.push(words[i].words_often)
                        }
                        if (err) {
                            res.json(err)
                        }
                        // checkDiskSpace('D:/').then((diskSpace) => {
                        res.render('./pattern/datatype', {
                            // pm: pm,
                            // data: data,
                            // checkDiskSpace: diskSpace,
                            history: history,
                            words: words,
                            words1: word,
                            words2: word1,
                            session: req.session
                        });
                        // })
                        //     })
                        // })
                    })
                })
            })
        }
    }
    // Statistics (Disk)
controller.diskPattern = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            const user = req.session.userid;
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history  from doc_pdpa_document as d  join doc_pdpa.pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        // conn.query('SELECT pm.pm_id, pm.pm_name, pd.data_name, pm.pm_start_time, pm.pm_used_time, pm.pm_total_time, pu.user_fullname, pm.pm_device_agent FROM pdpa_policy_management as pm JOIN pdpa_user as pu ON pm.user_id = pu.user_id JOIN pdpa_policy_management_data as pmd ON pmd.pm_id = pm.pm_id JOIN pdpa_data as pd ON pmd.data_id = pd.data_id;', (err, pm) => {
                        //     conn.query('SELECT * FROM pdpa_data', (err, data) => {
                        let word = []
                        let word1 = []
                        for (i in words) {
                            word.push(words[i].words_id)
                            word1.push(words[i].words_often)
                        }
                        if (err) {
                            res.json(err)
                        }
                        // checkDiskSpace('D:/').then((diskSpace) => {
                        res.render('./pattern/disk', {
                            // pm: pm,
                            // data: data,
                            // checkDiskSpace: diskSpace,
                            history: history,
                            words: words,
                            words1: word,
                            words2: word1,
                            session: req.session
                        });
                        // })
                        //     })
                        // })
                    })
                })
            })
        }
    }
    // Statistics (Used)
controller.usedPattern = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const user = req.session.userid;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history  from doc_pdpa_document as d  join doc_pdpa.pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    // conn.query('SELECT pm.pm_id, pm.pm_name, pd.data_name, pm.pm_start_time, pm.pm_used_time, pm.pm_total_time, pu.user_fullname, pm.pm_device_agent FROM pdpa_policy_management as pm JOIN pdpa_user as pu ON pm.user_id = pu.user_id JOIN pdpa_policy_management_data as pmd ON pmd.pm_id = pm.pm_id JOIN pdpa_data as pd ON pmd.data_id = pd.data_id;', (err, pm) => {
                    //     conn.query('SELECT * FROM pdpa_data', (err, data) => {
                    let word = []
                    let word1 = []
                    for (i in words) {
                        word.push(words[i].words_id)
                        word1.push(words[i].words_often)
                    }
                    if (err) {
                        res.json(err)
                    }
                    // checkDiskSpace('D:/').then((diskSpace) => {
                    res.render('./pattern/used', {
                        // pm: pm,
                        // data: data,
                        // checkDiskSpace: diskSpace,
                        history: history,
                        words: words,
                        words1: word,
                        words2: word1,
                        session: req.session
                    });
                    // })
                    //     })
                    // })
                })
            })
        })
    }
}

module.exports = controller;