const { log } = require("console");
const session = require("express-session");
const uuidv4 = require('uuid').v4;
const fs = require('fs');
const controller = {};

controller.appeal = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT DATE_FORMAT(appeal_date,'%d-%m-%Y') appeal_date,appeal_prefix,appeal_firstname,appeal_lastname,appeal_address,appeal_contact,appeal_detail,appeal_share,appeal_approved_complaint FROM  pdpa_appeal ORDER BY id_ap DESC",
                (err, pdpa_appeal) => {
                    var json_data = JSON.stringify(pdpa_appeal);
                    res.render("cookie/views/view_appeal/appeal", {
                        json_data,
                        pdpa_appeal: pdpa_appeal,
                        session: req.session,
                    });
                });
        });
    } else {
        res.redirect("/");
    }
}

controller.appreal_information = (req, res) => {
    if (req.session.userid) {
        const id = req.params.id;
        req.getConnection((err, conn) => {
            conn.query("SELECT id_ap,DATE_FORMAT(appeal_date,'%d-%m-%Y') appeal_date,appeal_prefix,appeal_firstname,appeal_lastname,appeal_address,appeal_detail,appeal_contact,appeal_share,appeal_email,appeal_detail_summary,appeal_revoke,appeal_access,appeal_edit_information,appeal_delet_information,appeal_suspend,appeal_transfer,appeal_oppose,appeal_decision,appeal_approved_complaint FROM  pdpa_appeal WHERE id_ap=? ", [id],
                (err, pdpa_appeal) => {
                    res.render("cookie/views/view_appeal/appreal_information", {
                        pdpa_appeal: pdpa_appeal,
                        session: req.session
                    });
                })
        });
    } else {
        res.redirect("/");
    }
}

controller.aprrove = (req, res) => {
    if (req.session.userid) {
        const id = req.params.id;
        req.getConnection((err, conn) => {
            conn.query("UPDATE  pdpa_appeal SET appeal_approved_complaint=1,appeal_date_approve=NOW() WHERE id_ap=? ", [id],
                (err, pdpa_appeal) => {
                    setTimeout(() => {
                        res.redirect("/appreal_information/" + id);
                    }, 100);
                })
        });
    } else {
        res.redirect("/");
    }
}

controller.deny = (req, res) => {
    if (req.session.userid) {
        const id = req.params.id;
        req.getConnection((err, conn) => {
            conn.query("UPDATE  pdpa_appeal SET appeal_approved_complaint=2,appeal_date_approve=NOW() WHERE id_ap=? ", [id],
                (err, pdpa_appeal) => {
                    setTimeout(() => {
                        res.redirect("/appreal_information/" + id);
                    }, 100);
                })
        });
    } else {
        res.redirect("/");
    }
}





controller.api_appeal = (req, res) => {
    if (req.session.userid) {
        req.getConnection((err, conn) => {
            conn.query("SELECT id_ap,DATE_FORMAT(appeal_date,'%d-%m-%Y') appeal_date,appeal_prefix,appeal_firstname,appeal_lastname,appeal_address,appeal_contact,appeal_detail,appeal_share,appeal_approved_complaint FROM  pdpa_appeal ORDER BY id_ap DESC",
                (err1, pdpa_appeal) => {
                    conn.query("SELECT id_ap,count(appeal_approved_complaint) count_0,DATE_FORMAT(appeal_date,'%d-%m-%Y') appeal_date FROM  pdpa_appeal where appeal_approved_complaint =0 GROUP BY appeal_date;",
                        (err, count_0) => {
                            conn.query("SELECT id_ap,count(appeal_approved_complaint) count_1,DATE_FORMAT(appeal_date,'%d-%m-%Y') appeal_date FROM  pdpa_appeal where appeal_approved_complaint =1 GROUP BY appeal_date;",
                                (err, count_1) => {
                                    conn.query("SELECT id_ap,count(appeal_approved_complaint) count_2,DATE_FORMAT(appeal_date,'%d-%m-%Y') appeal_date FROM  pdpa_appeal where appeal_approved_complaint =2 GROUP BY appeal_date;",
                                        (err, count_2) => {
                                            if (pdpa_appeal[0]) {
                                                res.send({ pdpa_appeal, count_0, count_1, count_2 });
                                            } else {
                                                var data_null = "ไม่มีข้อมูล";
                                                res.send(data_null);
                                            }
                                        });
                                });
                        });
                });
        });
    } else {
        res.redirect("/");
    }
}



controller.api_appeal_search = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        var search;
        if (data.data.indexOf("-") > 0) {
            var date_formate = ((data.data).replaceAll("-", ",")).split(",");
            search = '%' + date_formate[2] + "-" + date_formate[1] + "-" + date_formate[0] + '%';
        } else {
            search = '%' + data.data + '%';
        }
        req.getConnection((err, conn) => {
            conn.query("SELECT id_ap,DATE_FORMAT(appeal_date,'%d-%m-%Y') appeal_date,appeal_prefix,appeal_firstname,appeal_lastname,appeal_address,appeal_contact,appeal_detail,appeal_share,appeal_approved_complaint FROM  pdpa_appeal WHERE appeal_firstname LIKE ? OR appeal_lastname  LIKE ? OR appeal_detail LIKE ? OR appeal_date LIKE ? ORDER BY id_ap DESC",
                [search, search, search, search],
                (err, pdpa_appeal) => {
                    if (pdpa_appeal[0]) {
                        res.send(pdpa_appeal)
                    } else {
                        var data_null = "ไม่มีข้อมูล";
                        res.send(data_null);
                    }
                });
        });
    } else {
        res.redirect("/");
    }
};




controller.save = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        var complaint;
        if (data.appeal_approved_complaint) {
            complaint = data.appeal_approved_complaint;
        } else {
            complaint = "0";
        }
        req.getConnection((err, conn) => {
            conn.query("INSERT INTO pdpa_appeal SET  appeal_date=?,appeal_email=?,appeal_prefix=?,appeal_firstname=?,appeal_lastname=?,appeal_address=?,appeal_detail=?,appeal_contact=?,appeal_detail_summary=?,appeal_revoke=?,appeal_access=?,appeal_edit_information=?,appeal_delet_information=?,appeal_suspend=?,appeal_transfer=?,appeal_oppose=?,appeal_decision=?,appeal_approved_complaint=?",
                [data.appeal_date, data.appeal_email, data.appeal_prefix, data.appeal_firstname, data.appeal_lastname, data.appeal_address, data.appeal_detail, data.appeal_contact, data.appeal_detail_summary, data.appeal_revoke, data.appeal_access, data.appeal_edit_information, data.appeal_delet_information, data.appeal_suspend, data.appeal_transfer, data.appeal_oppose, data.appeal_decision, complaint], (err, insert_pdpa_appeal) => {
                    res.redirect("/appeal");
                });
        });
    } else {
        res.redirect("/");
    }
};

controller.api_personal_data_search = (req, res) => {
    if (req.session.userid) {
        const data = req.body;
        console.log(data);

        // var search;
        // if (data.data.indexOf("-") > 0) {
        //     var date_formate = ((data.data).replaceAll("-", ",")).split(",");
        //     search = '%' + date_formate[2] + "-" + date_formate[1] + "-" + date_formate[0] + '%';
        //     console.log(date_formate);
        // } else {
        //     search = '%' + data.data + '%';
        // }
        // req.getConnection((err, conn) => {
        //     conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y %I:%i %p') as date_inbox,id_email,email_files,email_to,email_status,email_content,email_subject,DATE_FORMAT(email_date_send,'%I:%i %p') as time FROM pdpa_email  WHERE email_subject LIKE ? OR email_to LIKE ? OR email_date_send LIKE ? ORDER  BY id_email DESC", [search, search, search],
        //         (err, pdpa_email) => {
        //             if (pdpa_email[0]) {
        //                 res.send(pdpa_email)
        //             } else {
        //                 var data_null = "ไม่มีข้อมูล";
        //                 res.send(data_null);
        //             }
        //         });
        // });

    } else {
        res.redirect("/");
    }
};


// controller.api_appeal_search = (req, res) => {
//     if (req.session.userid) {
//         const data = req.body;
//         console.log(data);
//         var search;
//         if (data.data.indexOf("-") > 0) {
//             var date_formate = ((data.data).replaceAll("-", ",")).split(",");
//             search = '%' + date_formate[2] + "-" + date_formate[1] + "-" + date_formate[0] + '%';
//             console.log(date_formate);
//         } else {
//             search = '%' + data.data + '%';
//         }
//         req.getConnection((err, conn) => {
//             conn.query("SELECT DATE_FORMAT(email_date_send,'%d-%m-%Y %I:%i %p') as date_inbox,id_email,email_files,email_to,email_status,email_content,email_subject,DATE_FORMAT(email_date_send,'%I:%i %p') as time FROM pdpa_email  WHERE email_subject LIKE ? OR email_to LIKE ? OR email_date_send LIKE ? ORDER  BY id_email DESC", [search, search, search],
//                 (err, pdpa_email) => {
//                     if (pdpa_email[0]) {
//                         res.send(pdpa_email)
//                     } else {
//                         var data_null = "ไม่มีข้อมูล";
//                         res.send(data_null);
//                     }
//                 });
//         });
//     } else {
//         res.redirect("/");
//     }
// };

module.exports = controller;
