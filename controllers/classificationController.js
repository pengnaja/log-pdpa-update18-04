const controller = {}
const checkDiskSpace = require('check-disk-space').default
const { validationResult } = require('express-validator');
const crypto = require('crypto');

function checkMatch(str, arr, index) {
    if (str.includes(arr[index])) {
        return true
    } else {
        if (arr.length == index) {
            return false
        }
        return checkMatch(str, arr, (index + 1))
    }
}

function checkMatch1(str, arr, index) {
    if (str.includes(arr[index])) {
        return true
    } else {
        if (index == arr.length) {
            if (str.includes(arr[index]) == false) {
                return false
            }
        } else {
            return checkMatch1(str, arr, (index + 1))
        }
    }
}

// Index Page
controller.index = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            const user = req.session.userid;
            req.getConnection((err, conn) => {
                conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from doc_pdpa_document as d join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                    conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                        conn.query('SELECT * FROM doc_pdpa_pattern;', (err, pattern) => {
                            conn.query('SELECT * FROM doc_pdpa_classification;', (err, classification) => {
                                conn.query('SELECT * FROM doc_pdpa_pattern_processing_base', (err, process) => {
                                    conn.query('SELECT * FROM doc_pdpa_classification_special_conditions', (err, special) => {
                                        let word = []
                                        let word1 = []
                                        for (i in words) {
                                            word.push(words[i].words_id)
                                            word1.push(words[i].words_often)
                                        }
                                        if (err) {
                                            res.json(err)
                                        }
                                        checkDiskSpace('/').then((diskSpace) => {
                                            res.render('./classification/index', {
                                                classification: classification,
                                                special: special,
                                                pattern: pattern,
                                                process: process,
                                                checkDiskSpace: diskSpace,
                                                history: history,
                                                words: words,
                                                words1: word,
                                                words2: word1,
                                                session: req.session
                                            })
                                        });
                                    })
                                })
                            })
                        })
                    })
                })
            });
        }
    }
    // Send Ajax
controller.getIndexClassification = (req, res) => {
    const get_auth = req.body.value;
    const hash = crypto.createHash('sha256').update(get_auth).digest('base64') //or hex
    if (hash == 'eHzafb3gKD2xPFL/XTqPlnztGeZ9BvBHPZCCrnXUNrI=') {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
            req.getConnection((err, conn) => {
                conn.query('SELECT classify_id FROM doc_pdpa_classification ORDER BY classify_id DESC;', (err, id_classify) => {
                    conn.query("SELECT * FROM doc_pdpa_classification as pc JOIN doc_pdpa_pattern as pp ON pc.pattern_id = pp.pattern_id JOIN doc_pdpa_event_process as pep ON pc.event_process_id = pep.event_process_id JOIN doc_pdpa_pattern_processing_base as pppb ON pc.pattern_processing_base_id = pppb.pattern_processing_base_id JOIN doc_pdpa_document as pd ON pp.doc_id = pd.doc_id JOIN account as pu ON pc.acc_id = pu.acc_id ORDER BY classify_id DESC;", (err, classify) => {
                        conn.query('SELECT * FROM doc_pdpa_data', (err, data) => {
                            count = 0
                            var name = []
                            var tag_name = []
                            var label = []
                            var name_except = []
                            var tag_except = []
                            var label_except = []
                            for (i in classify) {
                                classify[i].classify_id = count += 1
                                name.push(classify[i].pattern_name)
                                tag_name.push(classify[i].pattern_tag)
                                label.push(classify[i].pattern_label)
                                name_except.push(classify[i].classify_data_exception_or_unnecessary_filter_name)
                                tag_except.push(classify[i].classify_data_exception_or_unnecessary_filter_tag)
                                label_except.push(classify[i].classify_data_exception_or_unnecessary_filter_label)
                            }
                            var name_convert = new Array(name.length)
                            var tag_name_convert = new Array(tag_name.length)
                            var label_convert = new Array(label.length)
                            for (i in tag_name) {
                                if (name[i] == name_except[i]) {
                                    name_convert[i] = "ถูกยกเว้น"
                                } else {
                                    name_convert[i] = name[i]
                                }
                                if (tag_name[i] == tag_except[i] || tag_name[i] != tag_except[i]) {
                                    var arr_tag = tag_name[i].split(',')
                                    var arr_tag2 = tag_except[i].split(',')
                                    var newTag = arr_tag.filter(function(item) { return arr_tag2.indexOf(item) === -1 })
                                        // or String(Array)
                                    tag_name_convert[i] = newTag.join(',')
                                }
                                if (label[i] != label_except[i] || label[i] == label_except[i]) {
                                    var arr_label = label[i].split(',')
                                    var arr_label2 = label_except[i].split(',')
                                    var newLabel = arr_label.filter(function(item) { return arr_label2.indexOf(item) === -1 })
                                    label_convert[i] = String(newLabel)
                                }
                            }
                            var data_name_total = []
                            var data_expect_total = []
                            for (i in tag_name_convert) {
                                var data_name = []
                                var data_expect = []
                                for (j in data) {
                                    var data_tag_split = ""
                                    if (data[j].data_tag != null) {
                                        if (data[j].data_tag.includes(',')) {
                                            data_tag_split = data[j].data_tag.split(',')
                                        } else {
                                            data_tag_split = data[j].data_tag
                                        }
                                    }
                                    if (typeof data_tag_split == "string") {
                                        if (tag_name_convert[i].includes(data[j].data_tag) == true && tag_except[i].includes(data[j].data_tag) == false) {
                                            data_name.push(data[j].data_name)
                                        } else if (tag_name_convert[i].includes(data[j].data_tag) == false && tag_except[i].includes(data[j].data_tag) == true || (tag_name_convert[i].includes(data[j].data_tag) == true && tag_except[i].includes(data[j].data_tag) == true)) {
                                            data_expect.push(data[j].data_name)
                                        }
                                    } else {
                                        if (typeof tag_except[i] == 'string') {
                                            var option1 = checkMatch(tag_name_convert[i], data_tag_split, 0)
                                            var option2 = checkMatch1(tag_except[i], data_tag_split, 0)
                                            if (option1 == true && option2 == false) {
                                                data_name.push(data[j].data_name)
                                            } else if (option1 == false && option2 == true || (option1 == true && option2 == true)) {
                                                data_expect.push(data[j].data_name)
                                            }
                                        }
                                    }
                                }
                                data_name_total.push([data_name.join(',')])
                                var data_expect_convert = data_expect.filter(function(e) { return e; })
                                data_expect_total.push([data_expect_convert.join(',')])
                            }
                            if (err) { res.json(err) } else {
                                res.json({ id_classify, classify, name_convert, tag_name_convert, label_convert, data_name_total, data_expect_total })
                            }
                        })
                    })
                })
            })
        }
    } else {
        console.dir(req.body)
    }
}
controller.getIndexPattern = (req, res) => {
    const get_auth = req.body.value;
    const hash = crypto.createHash('sha256').update(get_auth).digest('base64')
    if (hash == "H/uH8bGV60IRYSsYJg/ZbmCSYfz8SMEZW6BfXp7BLag=") {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM doc_pdpa_pattern;', (err, id_pattern) => {
                    // conn.query('SELECT * FROM pdpa_pattern ORDER BY pattern_id ASC;', (err, pattern) => {
                    conn.query('SELECT * FROM doc_pdpa_pattern as pp JOIN doc_pdpa_document as pd ON pp.doc_id = pd.doc_id JOIN account as pu ON pp.acc_id = pu.acc_id JOIN doc_pdpa_pattern_processing_base as pppb ON pp.pattern_processing_base_id = pppb.pattern_processing_base_id ORDER BY pattern_id ASC;', (err, pattern) => {
                        conn.query('SELECT * FROM doc_pdpa_data', (err, data) => {
                            count = 0
                            var tag_name = []
                            for (i in pattern) {
                                pattern[i].pattern_id = count += 1
                                tag_name.push(pattern[i].pattern_tag)
                            }
                            var data_name_total = []
                            for (i in tag_name) {
                                var data_name = []
                                for (j in data) {
                                    var data_tag_split = ""
                                    if (data[j].data_tag != null) {
                                        if (data[j].data_tag.includes(',')) {
                                            data_tag_split = data[j].data_tag.split(',')
                                        } else {
                                            data_tag_split = data[j].data_tag
                                        }
                                    }
                                    if (typeof data_tag_split == "string") {
                                        if (tag_name[i].includes(data[j].data_tag)) {
                                            data_name.push(data[j].data_name)
                                        }
                                    } else {
                                        if (checkMatch(tag_name[i], data_tag_split, 0) == true) {
                                            data_name.push(data[j].data_name)
                                        }
                                    }
                                }
                                data_name_total.push(data_name.join(','))
                            }
                            if (err) { res.json(err) } else {
                                res.json({ pattern, id_pattern, data_name_total })
                            }
                        })
                    })
                })
            })
        }
    } else {
        console.log(req.body)
    }
}
controller.selectPattern = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const get_value = req.body.value
        const id = req.body.id
        const hash = crypto.createHash('sha256').update(get_value).digest('base64')
        if (hash == "CvsH3xR0vhTCmFqAhNy99ZHt3DpxCj8kCjlG2q0V05Q=") {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM doc_pdpa_pattern', (err, pattern) => {
                    conn.query('SELECT * FROM account', (err, users) => {
                        if (err) { res.json(err) } else {
                            let selected_pattern = pattern[parseInt(id) - 1]
                            let total_users = pattern[parseInt(id) - 1].pattern_processor_inside_id.split(',')
                            let total_name = ""
                            if (typeof total_users != 'object') {
                                total_name = users.filter(function(item) { return item.user_id == total_users })
                            } else {
                                total_name = users.filter(function(item) { return total_users.toString().indexOf(item.acc_id) != -1 })
                            }
                            let total_users1 = pattern[parseInt(id) - 1].pattern_processor_outside_id.split(',')
                            let total_name1 = ""
                            if (typeof total_users1 != 'object') {
                                total_name1 = users.filter(function(item) { return item.user_id == total_users })
                            } else {
                                total_name1 = users.filter(function(item) { return total_users1.toString().indexOf(item.acc_id) != -1 })
                            }
                            let _map_ = ['firstname', 'lastname']
                            let total_name_inside = total_name.map(o => _map_.map(k => o[k]))
                            let total_name_outside = total_name1.map(o => _map_.map(k => o[k]))
                            res.json({ selected_pattern, total_name_inside, total_name_outside })
                        }
                    })
                })
            })
        } else {
            console.log(req.body)
        }
    }
}
controller.addEventProcess = (req, res) => {
    const get_value = req.body.value
    const hash = crypto.createHash('sha256').update(get_value).digest('base64')
    if (hash == "gl76BkX3pTWsHPg3ehBXlCm1JkCSwTpnwWOGaudvsTA=") {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            req.getConnection((err, conn) => {
                conn.query('SELECT event_process_id FROM doc_pdpa_event_process', (err, id_event) => {
                    conn.query('SELECT * FROM doc_pdpa_event_process', (err, event) => {
                        for (var i = 0; i < event.length; i++) {
                            event[i].event_process_id = (i + 1)
                        }
                        if (err) { res.json(err) } else {
                            res.json({ id_event, event })
                        }
                    })
                })
            })
        }
    } else {
        console.dir(req.body)
    }

}
controller.selectEventProcess = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const get_value = req.body.value
        const id = req.body.id
        const hash = crypto.createHash('sha256').update(get_value).digest('base64')
        if (hash == "xgDoEl3WL3koDnlkwFApFmkOaitV9aJ4AxUQj1fQC6M=") {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM doc_pdpa_event_process', (err, event) => {
                    if (err) { res.json(err) } else {
                        let select_event = event[parseInt(id) - 1]
                        res.json(select_event)
                    }
                })
            })
        } else {
            console.log(req.body)
        }
    }
}
controller.addUsers = (req, res) => {
    const get_value = req.body.value;
    const hash = crypto.createHash('sha256').update(get_value).digest('base64')
    if (hash == "ZUjLQnBPqdQD3ZKOtUfP/86euzuY7SJ2B919d+8S8fE=") {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            req.getConnection((err, conn) => {
                conn.query('SELECT acc_id FROM account', (err, id_users) => {
                    conn.query('SELECT * FROM account', (err, users) => {
                        for (var i = 0; i < users.length; i++) {
                            users[i].acc_id = (i + 1)
                        }
                        if (err) { res.json(err) } else {
                            res.json({ id_users, users })
                        }
                    })
                })
            })
        }
    } else {
        console.dir(req.body)
    }
}
controller.selectUser = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const get_value = req.body.value
        const id = req.body.id
        const hash = crypto.createHash('sha256').update(get_value).digest('base64')
        if (hash == "LsOV7c539EZd3c5dfBpZx0yBwRjxU02YAGr7zXuQe74=") {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM account', (err, user) => {
                    if (err) { res.json(err) } else {
                        let select_user = user[parseInt(id) - 1]
                        res.json(select_user)
                    }
                })
            })
        } else {
            console.log(req.body)
        }
    }
}
controller.InsertEventProcess = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/')
    } else {
        const body = req.body;
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO doc_pdpa_classification_special_conditions SET ?;', [body], (err, pass) => {
                if (err) { res.json(err) } else { res.json('success') }
            })
        })
    }
}
controller.getClassification = (req, res) => {
        if (typeof req.session.userid == "undefined") {
            res.redirect('/')
        } else {
            const id = req.body.id
            const get_value = req.body.value
            const hash = crypto.createHash('sha256').update(get_value).digest('base64')
            if (hash == "FKZT43i08dGly9mX5c+DHjk/MN5TPGFwFg05tMktNss=") {
                req.getConnection((err, conn) => {
                    conn.query('SELECT * FROM doc_pdpa_classification as pc JOIN account as pu ON pc.acc_id = pu.acc_id JOIN doc_pdpa_pattern as pp ON pc.pattern_id = pp.pattern_id WHERE classify_id = ?;', [id], (err, classify) => {
                        conn.query('SELECT * FROM doc_pdpa_data;', (err, data) => {
                            count = 0
                            var name = []
                            var tag_name = []
                            var label = []
                            var name_except = []
                            var tag_except = []
                            var label_except = []
                            for (i in classify) {
                                classify[i].classify_id = count += 1
                                name.push(classify[i].pattern_name)
                                tag_name.push(classify[i].pattern_tag)
                                label.push(classify[i].pattern_label)
                                name_except.push(classify[i].classify_data_exception_or_unnecessary_filter_name)
                                tag_except.push(classify[i].classify_data_exception_or_unnecessary_filter_tag)
                                label_except.push(classify[i].classify_data_exception_or_unnecessary_filter_label)
                            }
                            var name_convert = new Array(name.length)
                            var tag_name_convert = new Array(tag_name.length)
                            var label_convert = new Array(label.length)
                            for (i in tag_name) {
                                if (name[i] == name_except[i]) {
                                    name_convert[i] = "ถูกยกเว้น"
                                } else {
                                    name_convert[i] = name[i]
                                }
                                if (tag_name[i] == tag_except[i] || tag_name[i] != tag_except[i]) {
                                    var arr_tag = tag_name[i].split(',')
                                    var arr_tag2 = tag_except[i].split(',')
                                    var newTag = arr_tag.filter(function(item) { return arr_tag2.indexOf(item) === -1 })
                                        // or String(Array)
                                    tag_name_convert[i] = newTag.join(',')
                                }
                                if (label[i] != label_except[i] || label[i] == label_except[i]) {
                                    var arr_label = label[i].split(',')
                                    var arr_label2 = label_except[i].split(',')
                                    var newLabel = arr_label.filter(function(item) { return arr_label2.indexOf(item) === -1 })
                                    label_convert[i] = String(newLabel)
                                }
                            }
                            var data_name_total = []
                            var data_expect_total = []
                            for (i in tag_name_convert) {
                                var data_name = []
                                var data_expect = []
                                for (j in data) {
                                    var data_tag_split = ""
                                    if (data[j].data_tag != null) {
                                        if (data[j].data_tag.includes(',')) {
                                            data_tag_split = data[j].data_tag.split(',')
                                        } else {
                                            data_tag_split = data[j].data_tag
                                        }
                                    }
                                    if (typeof data_tag_split == "string") {
                                        if (tag_name_convert[i].includes(data[j].data_tag) == true && tag_except[i].includes(data[j].data_tag) == false) {
                                            data_name.push(data[j].data_name)
                                        } else if (tag_name_convert[i].includes(data[j].data_tag) == false && tag_except[i].includes(data[j].data_tag) == true || (tag_name_convert[i].includes(data[j].data_tag) == true && tag_except[i].includes(data[j].data_tag) == true)) {
                                            data_expect.push(data[j].data_name)
                                        }
                                    } else {
                                        if (typeof tag_except[i] == 'string') {
                                            var option1 = checkMatch(tag_name_convert[i], data_tag_split, 0)
                                            var option2 = checkMatch1(tag_except[i], data_tag_split, 0)
                                            if (option1 == true && option2 == false) {
                                                data_name.push(data[j].data_name)
                                            } else if (option1 == false && option2 == true || (option1 == true && option2 == true)) {
                                                data_expect.push(data[j].data_name)
                                            }
                                        }
                                    }
                                }
                                data_name_total.push([data_name.join(',')])
                                var data_expect_convert = data_expect.filter(function(e) { return e; })
                                data_expect_total.push([data_expect_convert.join(',')])
                            }
                            if (err) { res.json(err) } else {
                                res.json({ classify, data_name_total })
                            }
                        })
                    })
                })
            } else {
                console.log(req.body)
            }
        }
    }
    // Add Classification
controller.addClassification = (req, res) => {
        if (typeof req.session.userid == 'undefined') {
            res.redirect('/')
        } else {
            const _classify_user_access_info_process_inside_ = req.body.classify_user_access_info_process_inside
            var classify_user_access_info_process_inside = ""
            if (typeof _classify_user_access_info_process_inside_ == "undefined") {
                classify_user_access_info_process_inside = "0"
            } else {
                classify_user_access_info_process_inside = "1"
            }
            const _classify_user_access_info_process_outside_ = req.body.classify_user_access_info_process_outside
            var classify_user_access_info_process_outside = ""
            if (typeof _classify_user_access_info_process_outside_ == 'undefined') {
                classify_user_access_info_process_outside = "0"
            } else {
                classify_user_access_info_process_outside = '1'
            }
            const _classify_period_proccess_follow_policy_ = req.body.classify_period_proccess_follow_policy
            var classify_period_proccess_follow_policy = ""
            if (typeof _classify_period_proccess_follow_policy_ == "undefined") {
                classify_period_proccess_follow_policy = "0"
            } else {
                classify_period_proccess_follow_policy = "1"
            }
            const _classify_period_end_follow_pattern_ = req.body.classify_period_end_follow_pattern
            var classify_period_end_follow_pattern = ""
            if (typeof _classify_period_end_follow_pattern_ == "undefined") {
                classify_period_end_follow_pattern = "0"
            } else {
                classify_period_end_follow_pattern = "1"
            }
            const _classify_type_data_in_event_personal_ = req.body.classify_type_data_in_event_personal
            var classify_type_data_in_event_personal = ""
            if (typeof _classify_type_data_in_event_personal_ == "undefined") {
                classify_type_data_in_event_personal = "0"
            } else {
                classify_type_data_in_event_personal = "1"
            }
            const _classify_type_data_in_event_special_personal_sensitive_ = req.body.classify_type_data_in_event_special_personal_sensitive
            var classify_type_data_in_event_special_personal_sensitive = ""
            if (typeof _classify_type_data_in_event_special_personal_sensitive_ == "undefined") {
                classify_type_data_in_event_special_personal_sensitive = "0"
            } else {
                classify_type_data_in_event_special_personal_sensitive = "1"
            }
            const _classify_protect_data_limit_follow_datetime_ = req.body.classify_protect_data_limit_follow_datetime
            var classify_protect_data_limit_follow_datetime = ""
            if (typeof _classify_protect_data_limit_follow_datetime_ == "undefined") {
                classify_protect_data_limit_follow_datetime = "0"
            } else {
                classify_protect_data_limit_follow_datetime = "1"
            }
            const _classify_approach_protect_used_two_factor_from_google_authen_ = req.body.classify_approach_protect_used_two_factor_from_google_authen
            var classify_approach_protect_used_two_factor_from_google_authen = ""
            if (typeof _classify_approach_protect_used_two_factor_from_google_authen_ == 'undefined') {
                classify_approach_protect_used_two_factor_from_google_authen = "0"
            } else {
                classify_approach_protect_used_two_factor_from_google_authen = "1"
            }
            const _classify_approach_protect_used_two_factor_from_email_ = req.body.classify_approach_protect_used_two_factor_from_email
            var classify_approach_protect_used_two_factor_from_email = ""
            if (typeof _classify_approach_protect_used_two_factor_from_email_ == "undefined") {
                classify_approach_protect_used_two_factor_from_email = "0"
            } else {
                classify_approach_protect_used_two_factor_from_email = "1"
            }
            const _classify_approach_protect_used_two_factor_from_sms_ = req.body.classify_approach_protect_used_two_factor_from_sms
            var classify_approach_protect_used_two_factor_from_sms = ""
            if (typeof _classify_approach_protect_used_two_factor_from_sms_ == 'undefined') {
                classify_approach_protect_used_two_factor_from_sms = "0"
            } else {
                classify_approach_protect_used_two_factor_from_sms = "1"
            }
            const _classify_type_data_in_event_personal_datamark_ = req.body.classify_type_data_in_event_personal_datamark;
            var classify_type_data_in_event_personal_datamark = ""
            if (typeof _classify_type_data_in_event_personal_datamark_ == 'undefined') {
                classify_type_data_in_event_personal_datamark = -1
            } else {
                classify_type_data_in_event_personal_datamark = req.body.classify_type_data_in_event_personal_datamark
            }
            const body = {
                classify_name: req.body.classify_name_part1 + " (" + req.body.classify_name_part2 + ")",
                pattern_id: req.body.pattern_id,
                event_process_id: req.body.event_process_id,
                classify_explain_process: req.body.classify_explain_process,
                classify_user_access_info_process_inside: classify_user_access_info_process_inside,
                classify_user_access_info_process_inside_from_pattern: req.body.classify_user_access_info_process_inside_from_pattern,
                classify_user_access_info_process_inside_from_new_id: req.body.classify_user_access_info_process_inside_from_new_id,
                classify_user_access_info_process_inside_from_new_total: req.body.classify_user_access_info_process_inside_from_new_total,
                classify_user_access_info_process_outside: classify_user_access_info_process_outside,
                classify_user_access_info_process_outside_from_pattern: req.body.classify_user_access_info_process_outside_from_pattern,
                classify_user_access_info_process_outside_from_new_id: req.body.classify_user_access_info_process_outside_from_new_id,
                classify_user_access_info_process_outside_from_new_total: req.body.classify_user_access_info_process_outside_from_new_total,
                classify_period_process: req.body.classify_period_process,
                classify_period_proccess_follow_policy: classify_period_proccess_follow_policy,
                classify_period_proccess_follow_policy_total: req.body.classify_period_proccess_follow_policy_total,
                classify_period_end: req.body.classify_period_end,
                classify_period_end_follow_pattern: classify_period_end_follow_pattern,
                classify_period_end_follow_pattern_total: req.body.classify_period_end_follow_pattern_total,
                classify_type_data_in_event_personal: classify_type_data_in_event_personal,
                classify_type_data_in_event_personal_datamark: classify_type_data_in_event_personal_datamark,
                classify_type_data_in_event_personal_datamark_total: req.body.classify_type_data_in_event_personal_datamark_total,
                classify_type_data_in_event_special_personal_sensitive: classify_type_data_in_event_special_personal_sensitive,
                pattern_processing_base_id: req.body.pattern_processing_base_id,
                classify_processing_base_explain: req.body.classify_processing_base_explain,
                classification_special_conditions_id: req.body.classification_special_conditions_id,
                classify_special_conditiion_explain: req.body.classify_special_conditiion_explain,
                classify_protect_data_limit_process: req.body.classify_protect_data_limit_process,
                classify_protect_data_limit_follow_datetime: classify_protect_data_limit_follow_datetime,
                classify_approach_protect_used_two_factor_from_google_authen: classify_approach_protect_used_two_factor_from_google_authen,
                classify_approach_protect_used_two_factor_from_email: classify_approach_protect_used_two_factor_from_email,
                classify_approach_protect_used_two_factor_from_sms: classify_approach_protect_used_two_factor_from_sms,
                classify_data_exception_or_unnecessary_filter_name: req.body.classify_data_exception_or_unnecessary_filter_name,
                classify_data_exception_or_unnecessary_filter_tag: req.body.classify_data_exception_or_unnecessary_filter_tag,
                classify_data_exception_or_unnecessary_filter_label: req.body.classify_data_exception_or_unnecessary_filter_label,
                classify_risk_assess_only_dpo_data_personal_can_specify: req.body.classify_risk_assess_only_dpo_data_personal_can_specify,
                classify_risk_assess_only_dpo_data_number_all_used_process_many: req.body.classify_risk_assess_only_dpo_data_number_all_used_process_many,
                classify_risk_assess_only_dpo_data_number_all_used_process_total: req.body.classify_risk_assess_only_dpo_data_number_all_used_process_total,
                classify_risk_assess_only_dpo_access_control_inside: req.body.classify_risk_assess_only_dpo_access_control_inside,
                classify_risk_assess_only_dpo_access_control_outside: req.body.classify_risk_assess_only_dpo_access_control_outside,
                classify_risk_assess_only_dpo_protect_data_inside: req.body.classify_risk_assess_only_dpo_protect_data_inside,
                classify_risk_assess_only_dpo_protect_data_outside: req.body.classify_risk_assess_only_dpo_protect_data_outside,
                classify_risk_assess_only_dpo_assess_the_impact_of_data: req.body.classify_risk_assess_only_dpo_assess_the_impact_of_data,
                classify_risk_assess_only_dpo_fix_a_leak_of_data: req.body.classify_risk_assess_only_dpo_fix_a_leak_of_data,
                classify_risk_assess_only_dpo_assess_the_impact_of_organization: req.body.classify_risk_assess_only_dpo_assess_the_impact_of_organization,
                classify_risk_assess_only_dpo_fix_a_leak_of_organization: req.body.classify_risk_assess_only_dpo_fix_a_leak_of_organization,
                acc_id: req.body.acc_id
            };
            // console.log(body)
            // classify_type_data_in_event_personal_datamark not success (after next version)
            req.getConnection((err, conn) => {
                conn.query('INSERT INTO doc_pdpa_classification SET ?;', [body], (err, pass) => {
                    if (err) { res.json(err) } else { res.redirect('back') }
                })
            })
        }
    }
    // Detail Classification
controller.detailClassification = (req, res) => {
    if (typeof req.session.userid == "undefined") {
        res.redirect('/')
    } else {
        const user = req.session.userid
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('select DATE_FORMAT(dl.log_date, "%Y-%m-%d %H:%i:%s" ) as date_history,dl.log_detail as detail_history ,dl.user_id as user_history,dl.log_action as action_history,d.doc_name as docname_history from doc_pdpa_document as d join doc_pdpa_document_log as dl on dl.doc_id = d.doc_id WHERE dl.user_id = ? order by dl.log_date DESC;', [user], (err, history) => {
                conn.query('SELECT * FROM doc_pdpa_words ', (err, words) => {
                    conn.query('SELECT * FROM account', (err, users) => {
                        conn.query('SELECT * FROM doc_pdpa_classification as pc JOIN doc_pdpa_pattern as pp ON pc.pattern_id = pp.pattern_id JOIN doc_pdpa_event_process as pep ON pc.event_process_id = pep.event_process_id JOIN doc_pdpa_pattern_processing_base as pppb ON pc.pattern_processing_base_id = pppb.pattern_processing_base_id JOIN doc_pdpa_classification_special_conditions as pcsc ON pc.classification_special_conditions_id = pcsc.classification_special_conditions_id JOIN doc_pdpa_document as pd ON pp.doc_id = pd.doc_id JOIN account as pu ON pc.acc_id = pu.acc_id WHERE classify_id = ?;', [id], (err, classify) => {
                            let word = []
                            let word1 = []
                            for (i in words) {
                                word.push(words[i].words_id)
                                word1.push(words[i].words_often)
                            }
                            if (err) {
                                res.json(err)
                            }
                            let arr_users = []
                            for (i in users) {
                                arr_users.push({ 'id': users[i].acc_id, 'name': users[i].firstname + " " + users[i].lastname, 'image': users[i].image })
                            }
                            let classify_user = classify[0].classify_user_access_info_process_inside_from_new_id.split(',')
                            let classifiy_user_outside = classify[0].classify_user_access_info_process_outside_from_new_id.split(',')
                            let classify_user_convert = arr_users.filter(function(item) { return classify_user.toString().indexOf(item.id) != -1 })
                            let classify_user_convert_outside = arr_users.filter(function(item) { return classifiy_user_outside.toString().indexOf(item.id) != -1 })
                            res.render('./classification/detail', {
                                classify: classify,
                                users: classify_user_convert,
                                users_outside: classify_user_convert_outside,
                                history: history,
                                words: words,
                                words1: word,
                                words2: word1,
                                session: req.session
                            })
                        })
                    })
                })
            })
        })
    }
}
controller.deleteClassification = (req, res) => {
    if (typeof req.session.user_id == 'undefined') {
        res.redirect('/')
    } else {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('DELETE FROM doc_pdpa_classification WHERE classify_id = ?', [id], (err, pass) => {
                if (err) { res.json(err) } else {
                    res.redirect('back')
                }
            })
        })
    }
}
module.exports = controller