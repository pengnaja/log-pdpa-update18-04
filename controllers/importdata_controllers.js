const controller = {};
const { validationResult } = require('express-validator');

function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())

    date = formatted_date + ' ' + formatted_time;
    return date;
}
var num = [];
controller.download = (req, res) => {
    const { id } = req.params;
    const errors = validationResult(req);
    var acc_id = req.session.userid;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '1') { res.redirect('/'); } else {
        res.download('E:\Web\sniff copy\snifflog.rar');
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO `exporthistory` (`exp_id`, `acc_id`, `file_id`) VALUES (NULL, ?, ?);', [acc_id, id], (err, exporthistory) => {
                conn.query('SELECT MAX(date_format(date,"%Y-%m-%d")) as date,log.device_id,DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL 1 DAY),"%Y-%m-%d") as date2 FROM `log`', (err, date) => {
                    day1 = date[0].date + ' 00:00:00';
                    day2 = date[0].date + ' 23:59:59';
                    conn.query('SELECT log.msg as msg,date_format(log.date,"%Y-%m-%d %T" ) as date,device.name as name,device.de_ip as ip,log.file_name FROM `log` JOIN device ON device.device_id=log.device_id WHERE log.device_id = ? and log.date BETWEEN ? AND ? ORDER BY `log`.`date` DESC LIMIT 1000 ', [date[0].device_id, day1, day2], (err, log_list) => {
                        conn.query('SELECT hour(log.date) as no,COUNT(*) as num FROM `log` WHERE device_id = ? and log.date BETWEEN ? AND ? GROUP BY hour(log.date)', [date[0].device_id, day1, day2], (err, count_list) => {
                            console.log(count_list);
                            conn.query('SELECT date_format(log.date,"%Y-%m-%d %T" ) as date,device.name as name,device.de_ip as ip,log.device_id as device_id FROM `log` JOIN device ON log.device_id=device.device_id GROUP BY device.device_id', (err, device_list) => {
                                console.log(exporthistory);
                                if (err) {
                                    res.json(err);
                                }
                                res.render('./log/filelog', {
                                    data: log_list,
                                    data2: count_list,
                                    data3: device_list,
                                    session: req.session
                                });
                            });

                        });
                    });
                });
            });
        });
    }
};
controller.list = (req, res) => {
    const data = null;
    id = req.session.userid;
    console.log(id);
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT ftp_id,date_format(create_date,"%Y-%m-%d %T") as create_date ,date_format(import_date,"%Y-%m-%d %T") as import_date,name,descrip,path,type_import,password,ip,username FROM `ftp` ORDER BY create_date', (err, ftp) => {
                res.render('./import/ftp_list', {
                    data: ftp,
                    session: req.session
                });

            });
        });
    }
};
controller.create = (req, res) => {
    const data = req.body;
    var md5 = require('md5');
    username = md5(data.name + data.date)
    password = md5(username)
    data.username = username
    data.password = password
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO ftp set ?', [data], (err, ftp) => {
            if (err) {
                res.json(err);
            }
            res.redirect('/import');
        });


    });
};
controller.deleteftp = (req, res) => {
    const { id } = req.params;
    // res.json(id)

    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('DELETE FROM `ftp` WHERE `ftp`.`ftp_id` = ?', [id], (err, device_delete) => {
                if (err) {
                    res.json(err);
                }
                res.redirect('/import');

            });
        });
    }
};
controller.alert = (req, res) => {
    const data = null;
    id = req.session.userid;
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `device`', (err, device) => {
                conn.query('SELECT alert.name,alert.type_alert,alert.services,alert.disk,alert.system,alert.web,alert.day,alert.enable,device.name as devicename,device.image FROM `alert`JOIN device ON alert.device_id=device.device_id', (err, ftp) => {
                    conn.query('SELECT * FROM `account`', (err, account) => {

                        res.render('./import/alert_list', {
                            data: ftp,
                            data2: account,
                            data6: device,
                            session: req.session
                        });
                    });
                });
            });
        });
    }
};
controller.createalert = (req, res) => {
    const data = req.body;

    if (data.day < 1) {
        data.day == 0
    }
    console.log(data);
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO alert set ?', [data], (err, alert) => {
                if (err) {
                    res.json(err);

                }
                console.log(alert);

                res.redirect('/alert');
            });
        });
    }
};

module.exports = controller;