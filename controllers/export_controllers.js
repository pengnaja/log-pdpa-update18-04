const controller = {};
const bodyParser = require('body-parser');
const e = require('express');
const { validationResult } = require('express-validator');

function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())

    date = formatted_date + ' ' + formatted_time;
    return date;
}
var num = [];

controller.dataout = (req, res) => {
    const data = null;
    id = req.session.userid;
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT out_id,date_format(create_date,"%b %d %T")as date,name,descrip,filters,destination,port,type_alert,alert_status,phone FROM `data_out`', (err, data_out) => {
                res.render('./export/dataout_list', {
                    data: data_out,
                    session: req.session
                });

            });
        });
    }
};
controller.createdataout = (req, res) => {
    const data = req.body;
    var date = ((data.create_date).replace('T', ' '));
    //res.json(data);
    id = req.session.userid;
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        data.create_date = date
        req.getConnection((err, conn) => {
            console.log(data);

            conn.query('INSERT INTO data_out set ?', [data], (err, data_out) => {
                console.log(data_out);
                res.redirect('./dataout')

            });
        });
    }
};
controller.deldataout = (req, res) => {
    const data = req.body;
    console.log(data);
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        data.create_date = date
        req.getConnection((err, conn) => {
            conn.query('DELETE FROM `data_out` WHERE `data_out`.`out_id` =?', [data.out_id], (err, data_out) => {
                console.log(data_out);
                res.redirect('./dataout')

            });
        });
    }
};
controller.dataoutckeck = (req, res) => {
    const { id } = req.params;
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT ftp_id,date_format(create_date,"%Y-%m-%d %T") as create_date ,date_format(import_date,"%Y-%m-%d %T") as import_date,name,descrip,path,type_import,password,ip,username FROM `ftp` ORDER BY create_date', (err, ftp) => {
                res.render('./export/dataout_check', {
                    data: ftp,
                    session: req.session
                });

            });
        });
    }
};
controller.sumhost = (req, res) => {
    const { id } = req.params;
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT MAX(date_format(log.date,"%Y-%m-%d %T")) as date,device.device_id,de_type,device.name as name,device.de_ip as ip,log.device_id as device_id FROM `log` JOIN device ON log.device_id=device.device_id GROUP BY device.device_id', (err, device) => {
                conn.query('SELECT date_format(date,"%Y-%m-%d %T")as date,name,hash FROM `file` WHERE device_id = ?', [device[0].device_id], (err, file) => {
                    conn.query('SELECT COUNT(*) as num,device.device_id,device.name,device.de_ip,device.image,log.date FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.device_id ORDER BY COUNT(*) DESC', (err, device2) => {

                        res.render('./export/sumhost_list', {
                            data: file,
                            data2: device2,
                            session: req.session
                        });
                    });
                });
            });
        });
    }
};
controller.comparehost = (req, res) => {
    const data = req.body;
    // res.json(data)
    var de_id = [];
    de_id = data.comparehost_id;
    var pushdata = []
    var seltect_de = de_id[de_id.length - 1];
    var comparehost_id = []
    console.log('de_id' + de_id);

    if (de_id.length > 1) {
        for (var i = 1; i < de_id.length; i++) {
            comparehost_id = de_id[0].split(',');
            console.log(de_id[i].split(','));
        }
        comparehost_id.push(de_id[1])
    }
    console.log('comparehost_id' + comparehost_id);
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT MAX(date_format(log.date,"%Y-%m-%d %T")) as date,device.device_id,de_type,device.name as name,device.de_ip as ip,log.device_id as device_id FROM `log` JOIN device ON log.device_id=device.device_id GROUP BY device.device_id', (err, device) => {
                conn.query('SELECT date_format(date,"%Y-%m-%d %T")as date,name,hash FROM `file` WHERE device_id = ?', [device[0].device_id], (err, file) => {
                    conn.query('SELECT COUNT(*) as num,device.device_id,device.name,device.de_ip,device.image,log.date FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.device_id ORDER BY COUNT(*) DESC', (err, device2) => {
                        req.session.comparehost_id = comparehost_id;

                        res.render('./export/sumhost_list', {
                            data: file,
                            data2: device2,
                            session: req.session
                        });
                    });
                });
            });
        });
    }
};
controller.overall = (req, res) => {
    const { id } = req.params;
    de_id = []
    if (req.session.device_id) {
        de_id = req.session.device_id

    } else {
        de_id = 0
    }
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT COUNT(*) as num,device.name,device.image,log.date FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.device_id ORDER BY COUNT(*) DESC LIMIT 5', (err, Top_Total_Event) => {
                conn.query('SELECT COUNT(*) as num,device.name,device.image,log.date FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.date ORDER BY  COUNT(*) DESC LIMIT 5', (err, Top_EPS) => {
                    conn.query('SELECT COUNT(*) as num FROM `log` JOIN device ON device.device_id=log.device_id', (err, Total_Events) => {
                        conn.query('SELECT COUNT(*) as num FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.date ORDER BY log.date DESC LIMIT 15', (err, Total_Events2) => {
                            conn.query('SELECT COUNT(*) as num,device.de_type FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY device.de_type', (err, pie) => {
                                conn.query('SELECT * FROM `device` WHERE device_id NOT IN (?)', [de_id], (err, device) => {

                                    res.render('./export/overall_list', {
                                        data: Top_Total_Event,
                                        data2: Top_EPS,
                                        data3: Total_Events,
                                        data4: Total_Events2,
                                        data5: pie,
                                        data6: device,
                                        session: req.session
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }
};
controller.comparedevice = (req, res) => {
    const data = req.body;
    var de_id = [];
    de_id = data.device_id;
    var pushdata = []
    var seltect_de = de_id[de_id.length - 1];
    var device_id = []
    if (de_id.length > 1) {
        for (var i = 0; i < de_id.length; i++) {
            device_id = de_id[0].split(',');
        }
        device_id.push(de_id[1])
    }

    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT COUNT(*) as num,device.name,device.image,log.date FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.device_id ORDER BY COUNT(*) DESC LIMIT 5', (err, Top_Total_Event) => {
                conn.query('SELECT COUNT(*) as num,device.name,device.image,log.date FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.date ORDER BY  COUNT(*) DESC LIMIT 5', (err, Top_EPS) => {
                    conn.query('SELECT COUNT(*) as num FROM `log` JOIN device ON device.device_id=log.device_id', (err, Total_Events) => {
                        conn.query('SELECT COUNT(*) as num FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.date ORDER BY log.date DESC LIMIT 15', (err, Total_Events2) => {
                            conn.query('SELECT COUNT(*) as num,device.de_type FROM `log` JOIN device ON device.device_id=log.device_id GROUP BY log.device_id', (err, pie) => {
                                conn.query('SELECT * FROM `device` WHERE device_id NOT IN (?) ', [device_id], (err, device) => {
                                    req.session.device_id = device_id;
                                    conn.query('SELECT COUNT(*) as num,device_id FROM `log` WHERE device_id IN (?) GROUP BY device_id', [device_id], (err, COUNT_file) => {
                                        conn.query('SELECT COUNT(*) as num,device_id FROM `file` WHERE device_id IN (?) GROUP BY device_id', [device_id], (err, COUNT_events) => {
                                            conn.query('SELECT device.keep,device.rmfile,(SELECT DATE_FORMAT(DATE_ADD((device.rmfile),INTERVAL -(device.keep) DAY),"%Y-%m-%d") )as date,DATEDIFF(CURDATE(),(SELECT DATE_FORMAT(DATE_ADD((device.rmfile),INTERVAL -(device.keep) DAY),"%Y-%m-%d") )) as startdate FROM `device` WHERE device_id in (?)', [device_id], (err, startdate) => {
                                                conn.query('SELECT COUNT(*) as num,device_id FROM log WHERE device_id IN (?) GROUP BY device_id', [device_id], (err, COUNT_log) => {

                                                    // console.log(COUNT_file);
                                                    // console.log(COUNT_events);
                                                    var log = []
                                                    var pushlog = []
                                                    for (var i = 0; i < COUNT_file.length; i++) {
                                                        pushdata.push({ file: COUNT_file[i].num, event: COUNT_events[i].num, date: startdate[i].startdate, log: COUNT_log[i].num });
                                                    }
                                                    for (var i = 1; i < device_id.length; i++) {
                                                        var id = device_id[i]
                                                        conn.query('SELECT hour(log.date) AS hour,COUNT(*) as num,device.name FROM `log` JOIN device on device.device_id=log.device_id WHERE log.device_id= ? GROUP BY hour(log.date) ORDER BY hour(log.date)', [id], (err, COUNT_log2) => {
                                                            if (COUNT_log2) {
                                                                for (var j = 0; j < COUNT_log2.length; j++) {
                                                                    log.push(COUNT_log2[j].num)

                                                                }
                                                            }

                                                            pushlog.push(log);
                                                            console.log("COUNT_log2" + log);

                                                        });
                                                        console.log("COUNT_log2" + pushlog);


                                                    }
                                                    console.log(log);

                                                    console.log(pushdata);
                                                    req.session.pushdata = pushdata;
                                                    res.render('./export/compare_list', {
                                                        data: Top_Total_Event,
                                                        data2: Top_EPS,
                                                        data3: Total_Events,
                                                        data4: Total_Events2,
                                                        data5: pie,
                                                        data6: device,
                                                        data7: pushdata,
                                                        session: req.session
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }
};
module.exports = controller;