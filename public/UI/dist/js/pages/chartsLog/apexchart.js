

var Revenue_Statistics = {
  series: [
    {
      name: "Product A ",
      data: [0, 2, 3.5, 0, 13, 1, 4, 1],
    },
    {
      name: "Product B ",
      data: [0, 13, 0, 4, 0, 4, 0, 4],
    },
  ],
  chart: {
    fontFamily: "Rubik,sans-serif",
    height: 350,
    type: "area",
    toolbar: {
      show: false,
    },
  },
  fill: {
    type: "solid",
    opacity: 0.2,
    colors: ["#009efb", "#39c449"],
  },
  grid: {
    show: true,
    borderColor: "rgba(0,0,0,0.1)",
    strokeDashArray: 3,
    xaxis: {
      lines: {
        show: true,
      },
    },
  },
  colors: ["#39c449", "#009efb"],
  dataLabels: {
    enabled: false,
  },
  stroke: {
    curve: "smooth",
    width: 1,
    colors: ["#009efb", "#39c449"],
  },
  markers: {
    size: 3,
    colors: ["#009efb", "#39c449"],
    strokeColors: "transparent",
  },
  xaxis: {
    axisBorder: {
      show: true,
    },
    axisTicks: {
      show: true,
    },
    categories: ["0", "4", "8", "12", "16", "20", "24", "30"],
    labels: {
      style: {
        colors: "#a1aab2",
      },
    },
  },
  yaxis: {
    tickAmount: 9,
    labels: {
      style: {
        colors: "#a1aab2",
      },
    },
  },
  tooltip: {
    x: {
      format: "dd/MM/yy HH:mm",
    },
    theme: "dark",
  },
  legend: {
    show: false,
  },
};
var chart_area_spline = new ApexCharts(
  document.querySelector("#revenue-statistics"),
  Revenue_Statistics
);
chart_area_spline.render();
