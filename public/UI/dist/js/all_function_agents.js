function convert_datetime(date) {
    var month = date.getMonth() + 1
    var day = date.getDate()
    var hours = date.getHours()
    var minutes = date.getMinutes()
    var seconds = date.getSeconds()
    if (month.toString().length == 1) {
        month = "0" + month.toString()
    }
    if (day.toString().length == 1) {
        day = "0" + day.toString()
    }
    if (hours.toString().length == 1) {
        hours = "0" + hours.toString()
    }
    if (minutes.toString().length == 1) {
        minutes = "0" + minutes.toString()
    }
    if (seconds.toString().length == 1) {
        seconds = "0" + seconds.toString()
    }
    return day + "/" + month + "/" + date.getFullYear() + " " + hours + ":" + minutes
}
function convert_size(number) {
    let kb = number / 1000
    let mb = gb = 0;
    if (kb > 1024) {
        mb = kb / 1000
    } else {
        return kb.toFixed(1) + " KB"
    }
    if (mb > 1024) {
        gb = mb / 1000
    } else {
        return mb.toFixed(1) + " MB"
    }
    if (gb < 1024) {
        return gb.toFixed(1) + "GB"
    }
}
if (document.getElementById('index')) {
    // ==================== Table Show =========================
    $.post('/agent_manage').done(function(result){
        // ============================== Create Prepare ============================
        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
        var total = 0 // จำนวนทั้งหมดของข้อมูล

        var state = {
            'querySet': result.manage,
            'page': 1,
            'rows': 20,
            'window': 10,
        }

        buildTable()

        function pagination(querySet, page, rows) {

            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows

            var trimmedData = querySet.slice(trimStart, trimEnd)

            var pages = Math.ceil(querySet.length / rows);

            var start_count = 1
            document.getElementById('start').innerHTML = start_count

            return {
                'querySet': trimmedData,
                'pages': pages,

            }
        }
        // ============================== Create Pagination ============================
        function pageButtons(pages) {
            var wrapper = document.getElementById('pagination-wapper')
            wrapper.innerHTML = ``
            // console.log('Pages:', pages)

            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }

            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)

                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }
            for (var page = maxLeft; page <= maxRight; page++) {
                wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
            }

            if (state.page == 1) {
                wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link" >&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
            } else {
                wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link">&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
            }

            if (state.page != pages) {
                wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
            } else {
                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
            }

            $('.page').on('click', function () {
                $('#table-body').empty()

                state.page = Number($(this).val())

                buildTable()
            })

        }
        // ============================== Create Table ============================
        function buildTable() {
            var table = $('#table-body')
            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            for (y in myList) {
                if (myList[y].agent_manage_id != "") {
                    var date = ""
                    //Keep in mind we are using "Template Litterals to create rows"
                    var row = '<tr>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                        '</b> <span class="tablesaw-cell-content">' + myList[y].agent_manage_id + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">รหัสของ Agent</b> <span class="tablesaw-cell-content">' + myList[y].code + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อของ Agent</b> <span class="tablesaw-cell-content">' + myList[y].agent_name + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ประเภท Agent</b> <span class="tablesaw-cell-content">' + myList[y].agent_type_name+ '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">คำอธิบาย</b> <span class="tablesaw-cell-content">' + myList[y].description + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">อุปกรณ์ที่ติดตั้ง</b> <span class="tablesaw-cell-content">' + myList[y].implement_device + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">IP Address</b> <span class="tablesaw-cell-content">' + myList[y].ip_address + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content">' + convert_datetime(new Date(myList[y].agent_create)) + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].firstname + " " + myList[y].lastname + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ดูข้อมูล</b> <span class="tablesaw-cell-content"><a href="javascript:void(0)"class="text-info" ><i class="fas fa-file-alt fa-2x"></i></a></span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลบช้อมูล</b> <span class="tablesaw-cell-content"><a href="javascript:void(0)"class="text-danger" ><i class="fas fa-trash-alt fa-2x"></i></a></span></td>' +
                        '</tr>'
                        //<a href="javascript:void(0)" id="'+myList[y].no+'" onclick="detail(this.id)" class="text-info" data-bs-toggle="modal" data-bs-target="#detail"><i class="fas fa-file-alt fa-2x"></i></a>
                    table.append(row)
                    end_count = myList[y].agent_manage_id
                } else {
                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                    table.append(row)
                }
            }
            $('#end').html(end_count)
            if(myList.length > 0){
                if (myList[0].agent_manage_id != "") {
                    $('#start').html(myList[0].agent_manage_id)
                    $('#total-agent').html(result.id_manage.length)
                } else {
                    $('#start').html(0)
                    $('#total-agent').html(0)
                }
            }
            pageButtons(data.pages)
            // end_count = table.find('tr').length
            
            if (myList.length == 0) {
                var table = $('#table-body')
                var row = '<tr>' +
                    '<td class="text-center" colspan="11" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                    '</tr>'
                table.append(row)
                end_count = 0
                $('#end').html(end_count)
                $('#start').html(0)
                pageButtons(data.pages)
            }
        }
    })
    // ======================= Add =============================
    $('a[href="#new_agent"]').on('click',function(){
        $.post('/agent_manage').done(function(result){
            for(i in result.agent){
                $('select#select-agent').append($('<option>',{value:result.agent[i].agent_type_id, text:result.agent[i].agent_name}))
            }
        })
    })
    $('select#select-agent').on('change',function(){
        $('select#select-agent option').each(function(){
            if($(this).is(':selected')){
                $.post('/agent_manage/select',{id: $(this).val()}).done(function(result){
                    $('input#select-code').val(result[0].code)
                    console.log(result[0], result[0].description)
                    $('input#select-type').val(result[0].agent_type_name)
                    $('textarea#select-des').text(result[0].description)
                })
            }
        })
    })
} else if (document.getElementById('file_log_ag')) {
    $.post('/file_log_ag').done(function (result) {
        let total_size = 0;
        for (key in result.files) {
            total_size += result.files[key].size
        }
        $('span#total-size').text(convert_size(total_size))
        // ============================== Create Prepare ============================
        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
        var total = 0 // จำนวนทั้งหมดของข้อมูล

        var state = {
            'querySet': result.files,
            'page': 1,
            'rows': 20,
            'window': 10,
        }

        buildTable()

        function pagination(querySet, page, rows) {

            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows

            var trimmedData = querySet.slice(trimStart, trimEnd)

            var pages = Math.ceil(querySet.length / rows);

            var start_count = 1
            document.getElementById('start').innerHTML = start_count

            return {
                'querySet': trimmedData,
                'pages': pages,

            }
        }
        // ============================== Create Pagination ============================
        function pageButtons(pages) {
            var wrapper = document.getElementById('pagination-wapper')
            wrapper.innerHTML = ``
            // console.log('Pages:', pages)

            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }

            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)

                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }
            for (var page = maxLeft; page <= maxRight; page++) {
                wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
            }

            if (state.page == 1) {
                wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link" >&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
            } else {
                wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link">&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
            }

            if (state.page != pages) {
                wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
            } else {
                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
            }

            $('.page').on('click', function () {
                $('#table-body').empty()

                state.page = Number($(this).val())

                buildTable()
            })

        }
        // ============================== Create Table ============================
        function buildTable() {
            var table = $('#table-body')
            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            for (y in myList) {
                if (myList[y].no != "") {
                    var date = ""
                    //Keep in mind we are using "Template Litterals to create rows"
                    var row = '<tr>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                        '</b> <span class="tablesaw-cell-content">' + myList[y].no + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อไฟล์</b> <span class="tablesaw-cell-content">' + myList[y].name + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">นามสกุลไฟล์</b> <span class="tablesaw-cell-content">' + myList[y].extension + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ขนาดไฟล์</b> <span class="tablesaw-cell-content">' + convert_size(myList[y].size) + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">เวลา/วันที่ แก้ไขล่าสุด</b> <span class="tablesaw-cell-content">' + convert_datetime(new Date(myList[y].date)) + '</span></td>' +
                        '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ค่าแฮชของไฟล์</b> <span class="tablesaw-cell-content"><a href="javascript:void(0)"class="text-info" ><i class="fas fa-file-alt fa-2x"></i></a></span></td>' +
                        '</tr>'
                        //<a href="javascript:void(0)" id="'+myList[y].no+'" onclick="detail(this.id)" class="text-info" data-bs-toggle="modal" data-bs-target="#detail"><i class="fas fa-file-alt fa-2x"></i></a>
                    table.append(row)
                    end_count = myList[y].no
                } else {
                    var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                    table.append(row)
                }
            }
            $('#end').html(end_count)
            if(myList.length > 0){
                if (myList[0].no != "") {
                    $('#start').html(myList[0].no)
                } else {
                    $('#start').html(0)
                }  
            }
            pageButtons(data.pages)
            // end_count = table.find('tr').length
            
            if (myList.length == 0) {
                var table = $('#table-body')
                var row = '<tr>' +
                    '<td class="text-center" colspan="6" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                    '</tr>'
                table.append(row)
                end_count = 0
                $('#end').html(end_count)
                $('#start').html(0)
                pageButtons(data.pages)
            }
        }
    })
    function detail(id){
        $.post('/file_log_ag/detail',{id: id}).done(function(result){
            $('span#name').text(result.name)
        })
    }
} else if (document.getElementById('logger_hash_ag')) {
    var result = $.ajax({
        method: "POST",
        url: '/logger_ag',
        data: { value: 1 },
        success: function (result) {
            if (result.hash.length > 1) {
                // ============================== Create Prepare ============================
                var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                var total = [] // จำนวนทั้งหมดของข้อมูล
                for (i in result.hash) {
                    total.push(parseInt(i) + 1)
                }
                // state = 
                // querySet : array(15) ,
                // page : rows  = 1:5 

                var state = {
                    'querySet': result.hash,
                    'page': 1,
                    'rows': 20,
                    'window': 10,
                }

                function myFunction() {
                    var input, filter, table, tr, td, i, j, cellValue;
                    input = document.getElementById("myInput");

                    filter = input.value.toUpperCase();
                    table = document.getElementById("myTable");
                    // table = state.querySet;
                    tr = table.getElementsByTagName("tr");
                    for (i = 0; i < tr.length; i++) {
                        td = tr[i].getElementsByTagName("td");
                        if (td) {
                            for (j = 0; j < td.length; j++) {
                                cellValue = td[j].textContent || td[j].innerText;
                                if (cellValue.trim().toUpperCase().match(filter) > -1) {
                                    tr[i].style.display = "";
                                    break;
                                } else {
                                    tr[i].style.display = "none";
                                }
                            }
                        }
                    }
                }

                function SelectedTextValue(ele) {
                    if (ele.selectedIndex > 0) {
                        var selectedText = ele.options[ele.selectedIndex].innerHTML;
                        var selectedValue = ele.value;
                        document.getElementById("myInput").value = selectedText;
                    }
                    else {
                        document.getElementById("myInput").value = "";
                    }
                }


                buildTable()

                function pagination(querySet, page, rows) {

                    var trimStart = (page - 1) * rows
                    var trimEnd = trimStart + rows

                    var trimmedData = querySet.slice(trimStart, trimEnd)

                    var pages = Math.ceil(querySet.length / rows);

                    var start_count = 1
                    document.getElementById('start').innerHTML = start_count

                    return {
                        'querySet': trimmedData,
                        'pages': pages,

                    }
                }
                // ============================== Create Pagination ============================
                function pageButtons(pages) {
                    var wrapper = document.getElementById('pagination-wapper')
                    wrapper.innerHTML = ``
                    // console.log('Pages:', pages)

                    var maxLeft = (state.page - Math.floor(state.window / 2))
                    var maxRight = (state.page + Math.floor(state.window / 2))

                    if (maxLeft < 1) {
                        maxLeft = 1
                        maxRight = state.window
                    }

                    if (maxRight > pages) {
                        maxLeft = pages - (state.window - 1)

                        if (maxLeft < 1) {
                            maxLeft = 1
                        }
                        maxRight = pages
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                    }

                    if (state.page == 1) {
                        wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link" >&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
                    } else {
                        wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link">&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
                    }

                    if (state.page != pages) {
                        wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
                    } else {
                        wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
                    }

                    $('.page').on('click', function () {
                        $('#table-body').empty()

                        state.page = Number($(this).val())

                        buildTable()
                    })

                }
                // ============================== Create Table ============================
                function buildTable() {
                    var table = $('#table-body')
                    var data = pagination(state.querySet, state.page, state.rows)
                    var myList = data.querySet
                    for (y in myList) {
                        if (myList[y].id != "") {
                            //Keep in mind we are using "Template Litterals to create rows"
                            var row = '<tr>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                                '</b> <span class="tablesaw-cell-content">' + myList[y].id + '</span></td>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่ออุปกรณ์ที่ส่งมา</b> <span class="tablesaw-cell-content">' + myList[y].device_name + '</span></td>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ระบบปฏิบัติการ</b> <span class="tablesaw-cell-content">' + myList[y].os_name + '</span></td>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ที่อยู่ของไฟล์</b> <span class="tablesaw-cell-content">' + myList[y].path + '</span></td>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อไฟล์</b> <span class="tablesaw-cell-content">' + myList[y].name_file + '</span></td>' +
                                // '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">จำนวนบรรทัดทั้งหมด</b> <span class="tablesaw-cell-content">' + myList[y].total_line + '</span></td>' +
                                // '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่/เวลา ที่ส่งมา</b> <span class="tablesaw-cell-content">' + myList[y].date_now + '</span></td>' +
                                // '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ค่าแฮชของไฟล์</b> <span class="tablesaw-cell-content" style="width: 10%">' + myList[y].value + '</span></td>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">รายละเอียด</b> <span class="tablesaw-cell-content"><a id="' + myList[y].id + '" href="javascript:void(0)" onclick="detail(this.id)" class="text-info" data-bs-toggle="modal" data-bs-target="#detail"><i class="fas fa-file-alt fa-2x"></i></a></span></td>' +
                                '</tr>'
                            table.append(row)
                            end_count = myList[y].no
                        } else {
                            var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                            table.append(row)
                        }
                    }
                    $('#end').html(end_count)
                    if(myList.length > 0){
                        if (myList[0].no != "") {
                            $('#start').html(myList[0].no)
                        } else {
                            $('#start').html(0)
                        }
                    }
                    pageButtons(data.pages)
                    // end_count = table.find('tr').length
                }
            } else {
                var table = $('#table-body')
                var row = '<tr>' +
                    '<td class="text-center" colspan="6" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                    '</tr>'
                table.append(row)
                end_count = 0
                $('#end').html(end_count)
                $('#start').html(0)
                pageButtons(data.pages)
            }
        }
    })
    function detail(id) {
        $.ajax({
            method: "POST",
            url: "/logger_ag",
            data: { value: 1 },
            success: function (result) {
                for (var j = 0; j < (result.hash).length; j++) {
                    if (parseInt(id) == parseInt(result.hash[j].id)) {
                        // console.log(result.hash[j])
                        $('#header').html("ข้อมูลของ: " + result.hash[j].device_name + ",<br/>" + result.hash[j].name_file)
                        $('#size_logger_ag').html(result.hash[j].total_line)
                        $('#recording_time_logger_ag').html(result.hash[j].date_now)
                        $('#hash_value_logger_ag').html(result.hash[j].value)
                    }
                }
            }
        })
    }

} else if (document.getElementById('database_ag')) {
    $.ajax({
        method: "POST",
        url: "/database_ag",
        data: { value: 1 },
        success: function (result) {
            if (result.data.length > 1) {
                // ============================== Create Prepare ============================
                var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                var total = 0 // จำนวนทั้งหมดของข้อมูล


                // state = 
                // querySet : array(15) ,
                // page : rows  = 1:5 

                var state = {
                    'querySet': result.data,
                    'page': 1,
                    'rows': 5,
                    'window': 10,
                }

                function myFunction() {
                    var input, filter, table, tr, td, i, j, cellValue;
                    input = document.getElementById("myInput");

                    filter = input.value.toUpperCase();
                    table = document.getElementById("myTable");
                    // table = state.querySet;
                    tr = table.getElementsByTagName("tr");
                    for (i = 0; i < tr.length; i++) {
                        td = tr[i].getElementsByTagName("td");
                        console.log(td);
                        if (td) {
                            for (j = 0; j < td.length; j++) {
                                cellValue = td[j].textContent || td[j].innerText;
                                if (cellValue.trim().toUpperCase().match(filter) > -1) {
                                    tr[i].style.display = "";
                                    break;
                                } else {
                                    tr[i].style.display = "none";
                                }
                            }
                        }
                    }
                }

                function SelectedTextValue(ele) {
                    if (ele.selectedIndex > 0) {
                        var selectedText = ele.options[ele.selectedIndex].innerHTML;
                        var selectedValue = ele.value;
                        document.getElementById("myInput").value = selectedText;
                    }
                    else {
                        document.getElementById("myInput").value = "";
                    }
                }


                buildTable()

                function pagination(querySet, page, rows) {

                    var trimStart = (page - 1) * rows
                    var trimEnd = trimStart + rows

                    var trimmedData = querySet.slice(trimStart, trimEnd)

                    var pages = Math.ceil(querySet.length / rows);

                    var start_count = 1
                    document.getElementById('start').innerHTML = start_count

                    return {
                        'querySet': trimmedData,
                        'pages': pages,

                    }
                }
                // ============================== Create Pagination ============================
                function pageButtons(pages) {
                    var wrapper = document.getElementById('pagination-wapper')
                    wrapper.innerHTML = ``
                    // console.log('Pages:', pages)

                    var maxLeft = (state.page - Math.floor(state.window / 2))
                    var maxRight = (state.page + Math.floor(state.window / 2))

                    if (maxLeft < 1) {
                        maxLeft = 1
                        maxRight = state.window
                    }

                    if (maxRight > pages) {
                        maxLeft = pages - (state.window - 1)

                        if (maxLeft < 1) {
                            maxLeft = 1
                        }
                        maxRight = pages
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        wrapper.innerHTML += `<li class="page-item"><button class="page page-link" value=${page}>${page}</button></li>`
                    }

                    if (state.page == 1) {
                        wrapper.innerHTML = `<li class="page-item disabled"><button value=${1} class="page page-link" >&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
                    } else {
                        wrapper.innerHTML = `<li class="page-item"><button value=${1} class="page page-link">&#171; ย้อนกลับ</button></li>` + wrapper.innerHTML
                    }

                    if (state.page != pages) {
                        wrapper.innerHTML += `<li class="page-item"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
                    } else {
                        wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${pages} class="page page-link" >ถัดไป &#187;</button>`
                    }

                    $('.page').on('click', function () {
                        $('#table-body').empty()

                        state.page = Number($(this).val())

                        buildTable()
                    })

                }
                // ============================== Create Table ============================
                function buildTable() {
                    var table = $('#table-body')
                    var data = pagination(state.querySet, state.page, state.rows)
                    var myList = data.querySet
                    for (y in myList) {
                        if (myList[y].id != "") {
                            //Keep in mind we are using "Template Litterals to create rows"
                            var row = '<tr>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                                '</b> <span class="tablesaw-cell-content">' + myList[y].id + '</span></td>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ข้อมูล</b> <span class="tablesaw-cell-content">' + myList[y].status + '</span></td>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">เวลาที่บันทึก</b> <span class="tablesaw-cell-content">' + convert_datetime(new Date(myList[y].get_date)) + '</span></td>' +
                                '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ตัวเลือก</b> <a href="#" id="' + result.id_data[y].id + '" onclick="_delete(this.id)" class="text-danger" data-bs-toggle="modal" data-bs-target="#delete"><i class="fas fa-trash-alt fa-2x"></i></a></span></td>' +
                                '</tr>'
                            table.append(row)
                            end_count = myList[y].no
                        } else {
                            var row = '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty text-center" style="color: red;">ไม่ข้อมูล</td></tr>'
                            table.append(row)
                        }
                    }
                    $('#end').html(end_count)
                    if(myList.length > 0){
                        if (myList[0].no != "") {
                            $('#start').html(myList[0].no)
                        } else {
                            $('#start').html(0)
                        }
                    }
                    pageButtons(data.pages)
                    // end_count = table.find('tr').length
                }
            } else {
                var table = $('#table-body')
                var row = '<tr>' +
                    '<td class="text-center" colspan="4" style="color: red;"><span class="text-center">ไม่พบข้อมูล</span></td>' +
                    '</tr>'
                table.append(row)
                end_count = 0
                $('#end').html(end_count)
                $('#start').html(0)
                pageButtons(data.pages)
            }
        }
    })
    function _delete(id) {
        $.ajax({
            method: "POST",
            url: "/database_ag",
            data: { value: 1 },
            success: function (result) {
                for (var j = 0; j < (result.data).length; j++) {
                    if (parseInt(id) == parseInt(result.data[j].id)) {
                        // console.log(result.hash[j])
                        $('#header').html("ยืนยันการลบข้อมูล: " + result.data[j].status)
                        $('#id_delete').html(result.data[j].id)
                        $('#status_delete').html(result.data[j].status)
                        $('#date_delete').html(convert_datetime(new Date(result.data[j].get_date)))
                        $('#value').html('<input name="id" value=' + result.id_data[j].id + ' hidden readonly/>')
                    }
                }
            }
        })
    }
}






