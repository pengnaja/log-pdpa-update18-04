// ================================ Data Format =================================
function convert_date(date) {
    var month = date.getMonth();
    var day = date.getDate();
    if (month.toString().length == 1) {
        month = "0" + month.toString()
    }
    if (day.toString().length == 1) {
        day = "0" + day.toString()
    }
    return day.toString() + "/" + month.toString() + "/" + date.getFullYear()
}
Date.prototype.addDays = function (days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}
function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
        dateArray.push(currentDate)
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}
function date_diff(date, total) {
    var dateArray = getDates(new Date(date), (new Date()).addDays(total));
    return dateArray[dateArray.length - 1]
}
function convert_datetime(date) {
    var month = date.getMonth();
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    if (month.toString().length == 1) {
        month = "0" + month.toString()
    }
    if (day.toString().length == 1) {
        day = "0" + day.toString()
    }
    if (hours.toString().length == 1) {
        hours = "0" + hours.toString()
    }
    if (minutes.toString().length == 1) {
        minutes = "0" + minutes.toString()
    }
    if (seconds.toString().length == 1) {
        seconds = "0" + seconds.toString()
    }
    return day.toString() + "/" + month.toString() + "/" + date.getFullYear() + " " + hours.toString() + ":" + minutes.toString() + ":" + seconds.toString()
}
function checkUsers(all, name, index) {
    if (all[index] == name) {
        return true
    } else {
        if (all.length - 1 == index) {
            return false
        }
        return checkUsers(all, name, (index + 1))
    }
}
// API in pages
if (document.getElementById('index')) {
    // ================================ Data Format =================================
    var date_now = new Date();
    $('#start_time').val(convert_date(date_now));
    // ================================ Statics =====================================
    var convert1 = $('#bar1').text().split('%')
    $('#progress-bar1').css({ 'width': $('#bar1').text(), 'height': '6px' })
    $('#progress-bar1').attr('aria-valuenow', convert1[0])
    var convert2 = $('#bar2').text().split('%')
    $('#progress-bar2').css({ 'width': $('#bar2').text(), 'height': '6px' })
    $('#progress-bar2').attr('aria-valuenow', convert2[0])
    var convert3 = $('#bar3').text().split('%')
    $('#progress-bar3').css({ 'width': $('#bar3').text(), 'height': '6px' })
    $('#progress-bar3').attr('aria-valuenow', convert3[0])
    // ======================== Convert Bytes to GB ==================================
    var convert_gb = parseInt($('#diskSpace').text()) * (10 ** (-9))
    $('#diskSpace').text(convert_gb.toFixed(0))
    // ======================== Build Table Classification ===========================
    $.ajax({
        url: "/classification",
        method: "POST",
        data: { value: "@lltr@Cl@ssific@ti0n" },
        success: function (result) {
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า

            var state = {
                'querySet': result.classify,
                'page': 1,
                'rows': 5,
                'window': 128 * 1024,
            }

            buildTable()

            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);
                var start_count = 1
                document.getElementById('start').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }
            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body').empty()
                    state.page = Number($(this).val())
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet
                for (y in myList) {
                    if (myList[y].classify_id != "") {
                        //Keep in mind we are using "Template Litterals to create rows"
                        var row = '<tr>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                            '</b> <span class="tablesaw-cell-content">' + myList[y].classify_id + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อประเภท</b> <span class="tablesaw-cell-content">' + myList[y].classify_name + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:250px"><b class="tablesaw-cell-label">ข้อมูลที่แยกประเภท</b> <span class="tablesaw-cell-content">' + String(result.data_name_total[y]).replaceAll(',', ", ") + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content">' + convert_datetime(new Date(myList[y].classify_create)) + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].firstname + " " + myList[y].lastname + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:110px"><b class="tablesaw-cell-label">ดูประเภท</b> <span class="tablesaw-cell-content"><a href="/classification/detail' + result.id_classify[y].classify_id + '" class="text-info"><i class="fas fa-file-alt fa-2x"></i></a></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:120px"><b class="tablesaw-cell-label">ลบประเภท</b> <span class="tablesaw-cell-content"><a href="#delete-classify" id='+result.id_classify[y].classify_id+' onClick="delClassify(this.id)" data-bs-toggle="modal"><i class="fas fa-trash-alt fa-2x" style="color: red;"></i></a></td>' +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].classify_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล</td></tr>'
                    table.append(row)
                    $('#start').text(0)
                    $('#total').text(0)
                } else {
                    if (myList[0].classify_id != "") {
                        $('#start').text(myList[0].classify_id)
                        $('#total').text(result.classify.length)
                    }
                }
                $('#end').text(end_count)
                pageButtons(data.pages)
                // end_count = table.find('tr').length
                // Modal Delete Classification
                
            }
        }
    })
    // ======================== Build Table Pattern ==================================
    $.ajax({
        url: "/classification_pattern",
        method: "POST",
        data: { value: "@lltr@Cl@ssific@ti0nP@ttern" },
        success: function (result) {
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า

            var state = {
                'querySet': result.pattern,
                'page': 1,
                'rows': 5,
                'window': 128 * 1024,
            }

            buildTable()

            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);
                var start_count = 1
                document.getElementById('start-pattern').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }


            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper-pattern')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body-pattern').empty()
                    state.page = Number($(this).val())
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body-pattern')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet
                for (y in myList) {
                    if (myList[y].pattern_id != "") {
                        var device = ""
                        var agent = ""
                        var database_out = ""
                        if (myList[y].pattern_storage_method_outside == 1) {
                            if (myList[y].pattern_storage_method_outside_device == 1) {
                                device = myList[y].pattern_storage_method_outside_device_name
                            } else {
                                device = "<span style='color:red;'>ไม่มี</span>"
                            }
                            if (myList[y].pattern_storage_method_outside_agent == 1) {
                                agent = myList[y].pattern_storage_method_outside_agent_name
                            } else {
                                agent = "<span style='color:red;'>ไม่มี</span>"
                            }
                            if (myList[y].pattern_storage_method_outside_database_outside == 1) {
                                database_out = myList[y].pattern_storage_method_outside_database_outside_name
                            } else {
                                database_out = "<span style='color:red;'>ไม่มี</span>"
                            }
                        }
                        //Keep in mind we are using "Template Litterals to create rows"
                        var row = '<tr>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ลำดับที่:' +
                            '</b> <span class="tablesaw-cell-content">' + myList[y].pattern_id + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ชื่อรูปแบบ</b> <span class="tablesaw-cell-content">' + myList[y].pattern_name + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:250px"><b class="tablesaw-cell-label">ข้อมูลที่ใช้</b> <span class="tablesaw-cell-content">' + String(result.data_name_total).replaceAll(',', ", ") + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่สร้าง</b> <span class="tablesaw-cell-content">' + convert_datetime(new Date(myList[y].pattern_create)) + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่ใช้</b> <span class="tablesaw-cell-content">' + convert_date(new Date(myList[y].pattern_start_date)) + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">วันที่สิ้นสุด</b> <span class="tablesaw-cell-content">' + convert_date(new Date(date_diff(myList[y].pattern_start_date, myList[y].pattern_total_date))) + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">ผู้สร้าง</b> <span class="tablesaw-cell-content">' + myList[y].firstname +" "+ myList[y].lastname + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" ><b class="tablesaw-cell-label">อุปกรณ์/Agent</b> <span class="tablesaw-cell-content">' + device + " / " + agent + " / " + database_out + '</span></td>' +
                            '<td class="title align-top tablesaw-swipe-cellhidden tablesaw-swipe-cellpersist text-center" style="width:110px"><b class="tablesaw-cell-label">ดูรูปแบบ</b> <span class="tablesaw-cell-content"><a href="/pattern/detail' + result.id_pattern[y].pattern_id + '" class="text-info"><i class="fas fa-file-alt fa-2x"></i></a></td>' +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].pattern_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="10" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล</td></tr>'
                    table.append(row)
                    $('#start-pattern').text(0)
                    $('#total-pattern').text(0)
                } else {
                    if (myList[0].pattern_id != "") {
                        $('#start-pattern').text(myList[0].pattern_id)
                        $('#total-pattern').text(result.pattern.length)
                    }
                }
                $('#end-pattern').text(end_count)
                pageButtons(data.pages)
                // end_count = table.find('tr').length
            }
        }
    })
    $('input:checkbox').on('click', function () {
        if ($(this).attr('id') == 'customCheck1') {
            if ($(this).is(':checked') == true) {
                $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').removeAttr('hidden')
                $('span#classification_user_access_information_process_inside').html(`
                &nbsp; 
                    <a href="#select-users-inside" class="option-pattern" data-bs-toggle="modal">เลือกสมาชิกในระบบ Alltra</a>&nbsp; &nbsp;
                    <span class="h6" style="font-weight: bold;">รวม</span>
                    <input type="text" name="classify_user_access_info_process_inside_from_new_total" class="form-control input-all-number" readonly/>&nbsp;
                    <span class="h6" style="font-weight: bold;">คน</span>&nbsp;&nbsp;&nbsp;
                    <span class="span-image" id='user_inside'>
                        
                    </span>
                    <input type="text" name="classify_user_access_info_process_inside_from_new_id" class="form-control input-all-number" readonly hidden/>&nbsp;
                `)
                var total_users = []
                var total_image = []
                var count_click2 = 0
                $('a[href="#select-users-inside"]').on('click', function () {
                    $.post('/users', { value: "@lltr@Cl@ssific@ti0nUser" }, function (result) {
                        count_click2 += 1
                        // ============================== Create Prepare ============================
                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                        var state = {
                            'querySet': result.users,
                            'page': 1,
                            'rows': 5,
                            'window': 128 * 1024,
                        }
                        if (count_click2 == 1) {
                            buildTable()
                        }
                        function pagination(querySet, page, rows) {

                            var trimStart = (page - 1) * rows
                            var trimEnd = trimStart + rows

                            var trimmedData = querySet.slice(trimStart, trimEnd)

                            var pages = Math.ceil(querySet.length / rows);

                            var start_count = 1
                            document.getElementById('start-user-inside-add').innerHTML = start_count

                            return {
                                'querySet': trimmedData,
                                'pages': pages,
                            }
                        }
                        // ============================== Create Pagination ============================
                        function pageButtons(pages) {
                            var wrapper = document.getElementById('pagination-wapper-user-inside-add')
                            wrapper.innerHTML = ``
                            var maxLeft = (state.page - Math.floor(state.window / 2))
                            var maxRight = (state.page + Math.floor(state.window / 2))

                            if (maxLeft < 1) {
                                maxLeft = 1
                                maxRight = state.window
                            }

                            if (maxRight > pages) {
                                maxLeft = pages - (state.window - 1)
                                if (maxLeft < 1) {
                                    maxLeft = 1
                                }
                                maxRight = pages
                            }

                            if (state.page > 1) {
                                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                            } else {
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                            }


                            num = 1
                            if (maxRight > 5) {
                                if (state.page > (maxRight / 2)) {
                                    if ((state.page + 1) > (maxRight / 2)) {
                                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                    }
                                }
                                for (var page = maxLeft; page <= maxRight; page++) {
                                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                                        if (page == state.page) {
                                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                                        }
                                        else {
                                            p = page - 1;
                                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                                        }
                                    }
                                }
                                if ((state.page) <= (maxRight / 2)) {
                                    mp = maxRight - 1;
                                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                                }
                            }
                            else {
                                for (var page = maxLeft; page <= maxRight; page++) {
                                    if (state.page == page) {
                                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                                    } else {
                                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                                    }
                                }
                            }

                            if (state.page < pages) {
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                            } else {
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                            }

                            $('.page').on('click', function () {
                                $('#table-body-user-inside-add').empty()
                                state.page = Number($(this).val())
                                // document.getElementById('page').value = state.page
                                buildTable()
                            })
                        }
                        // ============================== Create Table ============================
                        function buildTable() {
                            var table = $('#table-body-user-inside-add')
                            var data = pagination(state.querySet, state.page, state.rows)
                            var myList = data.querySet
                            for (y in myList) {
                                if (myList[y].acc_id != "") {
                                    let buttonAdd = ""
                                    let buttonDel = ""
                                    let total_users = $('input[name="classify_user_access_info_process_inside_from_new_id"]').val().split(',')
                                    if (checkUsers(total_users, result.id_users[y].acc_id, 0) == true) {
                                        buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        buttonDel = '<a id="_del_users_inside" href="javascript:void(0);" data-value=' + myList[y].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                    } else {
                                        buttonAdd = '<a id="_add_users_inside" href="javascript:void(0);" data-value=' + myList[y].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                    }
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<th style="vertical-align: center;">' +
                                        'ชื่อ-นามสกุล: ' + myList[y].firstname + " " + myList[y].lastname + " <br/>" +
                                        '</th>' +
                                        '<td style="vertical-align: middle; width: 50px;">' +
                                        buttonAdd +
                                        '</td>' +
                                        '<td style="vertical-align: middle; width: 50px;">' +
                                        buttonDel +
                                        '</td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].acc_id
                                }
                            }
                            if (myList.length == 0) {
                                var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                                table.append(row)
                                $('#total-user-inside-add').text(0)
                                $('#start-user-inside-add').html(0)
                            } else {
                                if (myList[0].user_id) {
                                    $('#start-user-inside-add').text(myList[0].acc_id)
                                }
                                pageButtons(data.pages)
                                $('#total-user-inside-add').text(result.users.length)
                            }
                            $('#end-user-inside-add').html(end_count)

                            $('a#_add_users_inside').on('click', function () {
                                $.post('/users/get', { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                    total_users.push(result.acc_id)
                                    total_image.push(result.image)
                                    $('span#user_inside').empty()
                                    var length_users = new Set(total_users)
                                    var name_image = new Set(total_image)
                                    $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(length_users.size)
                                    $('input[name="classify_user_access_info_process_inside_from_new_id"]').val([...length_users])
                                    for (var k = 0; k < Array.from(name_image).length; k++) {
                                        // Image = 600x600
                                        if (k < Array.from(name_image).length - 1) {
                                            $('span#user_inside').append('<img class="image-users" src="/UI/assets/images/users/' + Array.from(name_image)[k] + '"/> &nbsp;')
                                        } else {
                                            $('span#user_inside').append('<img class="image-users" src="/UI/assets/images/users/' + Array.from(name_image)[k] + '"/>')
                                        }
                                    }
                                    $('#table-body-user-inside-add').empty()
                                    buildTable()
                                })
                            })
                            $('a#_del_users_inside').on('click', function () {
                                $.post('/users/get', { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                    let new_total_user = total_users.filter(function (item) { return item.toString().indexOf(result.acc_id) })
                                    let new_total_image = total_image.filter(function (item) { return item.indexOf(result.image) })
                                    total_users = new_total_user
                                    total_image = new_total_image
                                    $('input[name="classify_user_access_info_process_inside_from_new_total"]').val(total_users.length)
                                    $('input[name="classify_user_access_info_process_inside_from_new_id"]').val([...total_users])
                                    for (var k = 0; k < $('span#user_inside').children().length; k++) {
                                        let old_image = $('span#user_inside').children()[k].getAttribute('src').split('/')
                                        if (old_image[old_image.length - 1] == result.image) {
                                            $('span#user_inside').children()[k].remove()
                                        }
                                    }
                                    $('#table-body-user-inside-add').empty()
                                    buildTable()
                                    if ($('input[name="classify_user_access_info_process_inside_from_new_total"]').val() == "0") {
                                        $('span#user_inside').empty()
                                    }
                                })
                            })
                            $('input[name="classify_user_access_info_process_inside_from_new_total"]').on('click keyup keydown focus', function () {
                                if ($(this).val() == "" || $(this).val() == null) {
                                    $('span#user_inside').empty()
                                } else if ($(this).val() != String(total_image.length)) {
                                    $('span#user_inside').last().remove()
                                }
                            })
                        }
                    })
                })
            } else {
                $('input[name="classify_user_access_info_process_inside_from_pattern"]').attr('hidden', 'hidden')
                $('span#classification_user_access_information_process_inside').empty()
                $('tbody#table-body-user-inside-add').empty()
                $('ul#pagination-wapper-user-inside-add').empty()
                $('span#start-user-inside-add').empty()
                $('span#end-user-inside-add').empty()
                $('span#total-user-inside-add').empty()
            }
        }
        if ($(this).attr('id') == 'customCheck2') {
            if ($(this).is(':checked') == true) {
                $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').removeAttr('hidden')
                $('span#classification_user_access_information_process_outside').html(`
                &nbsp;
                    <a href="#select-users-outside" class="option-pattern" data-bs-toggle="modal">เลือกสมาชิกในระบบ Alltra</a>&nbsp; &nbsp;
                    <span class="h6" style="font-weight: bold;">รวม</span>
                    <input type="text" name="classify_user_access_info_process_outside_from_new_total" class="form-control input-all-number" readonly/>&nbsp;
                    <span class="h6" style="font-weight: bold;">คน</span>&nbsp;&nbsp;&nbsp;
                    <span class="span-image" id="user_outside">
                        
                    </span>
                    <input type="text" name="classify_user_access_info_process_outside_from_new_id" class="form-control input-all-number" readonly hidden/>&nbsp;
                `)
                var total_users = []
                var total_image = []
                var count_click3 = 0
                $('a[href="#select-users-outside"]').on('click', function () {
                    $.post('/users', { value: "@lltr@Cl@ssific@ti0nUser" }, function (result) {
                        count_click3 += 1
                        // ============================== Create Prepare ============================
                        var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
                        var state = {
                            'querySet': result.users,
                            'page': 1,
                            'rows': 5,
                            'window': 128 * 1024,
                        }
                        if (count_click3 == 1) {
                            buildTable()
                        }
                        function pagination(querySet, page, rows) {

                            var trimStart = (page - 1) * rows
                            var trimEnd = trimStart + rows

                            var trimmedData = querySet.slice(trimStart, trimEnd)

                            var pages = Math.ceil(querySet.length / rows);

                            var start_count = 1
                            document.getElementById('start-users-outside-add').innerHTML = start_count

                            return {
                                'querySet': trimmedData,
                                'pages': pages,
                            }
                        }
                        // ============================== Create Pagination ============================
                        function pageButtons(pages) {
                            var wrapper = document.getElementById('pagination-wapper-users-outside-add')
                            wrapper.innerHTML = ``
                            var maxLeft = (state.page - Math.floor(state.window / 2))
                            var maxRight = (state.page + Math.floor(state.window / 2))

                            if (maxLeft < 1) {
                                maxLeft = 1
                                maxRight = state.window
                            }

                            if (maxRight > pages) {
                                maxLeft = pages - (state.window - 1)
                                if (maxLeft < 1) {
                                    maxLeft = 1
                                }
                                maxRight = pages
                            }

                            if (state.page > 1) {
                                wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                            } else {
                                wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                            }

                            num = 1
                            if (maxRight > 5) {
                                if (state.page > (maxRight / 2)) {
                                    if ((state.page + 1) > (maxRight / 2)) {
                                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                    }
                                }
                                for (var page = maxLeft; page <= maxRight; page++) {
                                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                                        if (page == state.page) {
                                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                                        }
                                        else {
                                            p = page - 1;
                                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                                        }
                                    }
                                }
                                if ((state.page) <= (maxRight / 2)) {
                                    mp = maxRight - 1;
                                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                                }
                            }
                            else {
                                for (var page = maxLeft; page <= maxRight; page++) {
                                    if (state.page == page) {
                                        wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                                    } else {
                                        wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                                    }
                                }
                            }

                            if (state.page < pages) {
                                wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                            } else {
                                wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                            }

                            $('.page').on('click', function () {
                                $('#table-body-users-outside-add').empty()
                                state.page = Number($(this).val())
                                // document.getElementById('page').value = state.page
                                buildTable()
                            })
                        }
                        // ============================== Create Table ============================
                        function buildTable() {
                            var table = $('#table-body-users-outside-add')
                            var data = pagination(state.querySet, state.page, state.rows)
                            var myList = data.querySet
                            for (y in myList) {
                                if (myList[y].acc_id != "") {
                                    let buttonAdd = ""
                                    let buttonDel = ""
                                    let total_users = $('input[name="classify_user_access_info_process_outside_from_new_id"]').val().split(',')
                                    if (checkUsers(total_users, result.id_users[y].acc_id, 0) == true) {
                                        buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                        buttonDel = '<a id="_del_users_outside" href="javascript:void(0);" data-value=' + myList[y].acc_id + ' class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                    } else {
                                        buttonAdd = '<a id="_add_users_outside" href="javascript:void(0);" data-value=' + myList[y].acc_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                                        buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                                    }
                                    //Keep in mind we are using "Template Litterals to create rows"
                                    var row = '<tr>' +
                                        '<th style="vertical-align: center;">' +
                                        'ชื่อ-นามสกุล: ' + myList[y].firstname + " " + myList[y].lastname + " <br/>" +
                                        '</th>' +
                                        '<td style="vertical-align: middle; width: 50px;">' +
                                        buttonAdd +
                                        '</td>' +
                                        '<td style="vertical-align: middle; width: 50px;">' +
                                        buttonDel +
                                        '</td>' +
                                        '</tr>'
                                    table.append(row)
                                    end_count = myList[y].acc_id
                                }
                            }
                            if (myList.length == 0) {
                                var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                                table.append(row)
                                $('#total-users-outside-add').text(0)
                                $('#start-users-outside-add').html(0)
                            } else {
                                if (myList[0].acc_id) {
                                    $('#start-users-outside-add').text(myList[0].acc_id)
                                }
                                pageButtons(data.pages)
                                $('#total-users-outside-add').text(result.users.length)
                            }

                            $('#end-users-outside-add').html(end_count)

                            $('a#_add_users_outside').on('click', function () {
                                $.post('/users/get', { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                    $('span#user_outside').empty()
                                    total_users.push(result.acc_id)
                                    total_image.push(result.image)
                                    var length_users = new Set(total_users)
                                    var name_image = new Set(total_image)
                                    $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(length_users.size)
                                    $('input[name="classify_user_access_info_process_outside_from_new_id"]').val([...length_users])
                                    for (var k = 0; k < Array.from(name_image).length; k++) {
                                        // Image = 600x600
                                        if (k < Array.from(name_image).length - 1) {
                                            $('span#user_outside').append('<img class="image-users" src="/UI/assets/images/users/' + Array.from(name_image)[k] + '"/> &nbsp;')
                                        } else {
                                            $('span#user_outside').append('<img class="image-users" src="/UI/assets/images/users/' + Array.from(name_image)[k] + '"/>')
                                        }
                                    }
                                    $('#table-body-users-outside-add').empty()
                                    buildTable()
                                })
                            })
                            $('a#_del_users_outside').on('click', function () {
                                $.post('/users/get', { value: "@lltr@Cl@ssifyGetUser", id: $(this).attr('data-value') }).done(function (result) {
                                    let new_total_users = total_users.filter(function (item) { return item.toString().indexOf(result.acc_id) })
                                    let new_total_image = total_image.filter(function (item) { return item.toString().indexOf(result.image) })
                                    total_users = new_total_users
                                    total_image = new_total_image
                                    $('input[name="classify_user_access_info_process_outside_from_new_total"]').val(new_total_users.length)
                                    $('input[name="classify_user_access_info_process_outside_from_new_id"]').val([...new_total_users])
                                    for (var k = 0; k < $('span#user_outside').children().length; k++) {
                                        let old_image = $('span#user_outside').children()[k].getAttribute('src').split('/')
                                        if (old_image[old_image.length - 1] == result.image) {
                                            $('span#user_outside').children()[k].remove()
                                        }
                                    }
                                    if ($('input[name="classify_user_access_info_process_outside_from_new_total"]').val() == 0) {
                                        $('span#user_outside').empty()
                                    }
                                    $('#table-body-users-outside-add').empty()
                                    buildTable()
                                })
                            })
                            $('input[name="classify_user_access_info_process_outside_from_new_total"]').on('click keyup keydown focus', function () {
                                if ($(this).val() == "" || $(this).val() == null) {
                                    $('span#user_outside').empty()
                                }
                            })
                        }
                    })
                })
            } else {
                $('input[name="classify_user_access_info_process_outside_from_pattern"]').attr('hidden', 'hidden')
                $('span#classification_user_access_information_process_outside').empty()
                $('tbody#table-body-users-outside-add').empty()
                $('ul#pagination-wapper-users-outside-add').empty()
                $('span#start-users-outside-add').empty()
                $('span#end-users-outside-add').empty()
                $('span#total-users-outside-add').empty()
            }
        }
        if ($(this).attr('id') == 'customCheck3') {
            if ($(this).is(':checked') == true) {
                $('span#classification_period_proccess_follow_policy').html(`
                &nbsp;<input type="text" name="classify_period_proccess_follow_policy_total" placeholder="" class="form-control input-checkbox1" value="180" readonly/> &nbsp; 
                    <span class="h6" style="font-weight: bold;">วัน</span>
                `)
            } else {
                $('span#classification_period_proccess_follow_policy').empty()
            }
        }
        if ($(this).attr('id') == 'customCheck4') {
            if ($(this).is(':checked') == true) {
                $('input[name="classify_period_end_follow_pattern_total"]').removeAttr('hidden')
                $('span#day_end_pattern').css('display', 'contents')
            } else {
                $('input[name="classify_period_end_follow_pattern_total"]').attr('hidden', true)
                $('span#day_end_pattern').css('display', 'none')
            }
        }
    })
    if ($('input:radio[name="classify_risk_assess_only_dpo_data_number_all_used_process_many"]').find('checked')) {
        $('span#classification_risk_assessment_only_dpo_data_number_all_used_process_total').html(`
                 &nbsp;<span class='h6' style="font-weight: bold;">ระบุ</span>&nbsp;&nbsp;<input type="text" name="classify_risk_assess_only_dpo_data_number_all_used_process_total" placeholder="" class="form-control input-radio1" /> &nbsp; 
        `)
    }
    if ($('input:radio[name="classify_risk_assess_only_dpo_assess_the_impact_of_data"]').find('checked')) {
        $('div#classification_risk_assessment_only_dpo_fix_a_leak_of_data').html(`
            <label class="form-check-label h6"
                                                                    style="font-weight: bold;">วิธีแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อเจ้าของข้อมูล</label>
                                                                <div>
                                                                    <input type="text" name="classify_risk_assess_only_dpo_fix_a_leak_of_data" id="" style="width: 47%;" placeholder="กรุณาป้อนวิธีแก้ไข..." class="form-control">
                                                                </div>
            `)
    }
    if ($('input:radio[name="classify_risk_assess_only_dpo_assess_the_impact_of_organization"]').find('checked')) {
        $('div#classification_risk_assessment_only_dpo_fix_a_leak_of_organization').html(`
            <label class="form-check-label h6"
            style="font-weight: bold;">วิธีแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อองค์กร</label>
        <div>
            <input type="text" name="classify_risk_assess_only_dpo_fix_a_leak_of_organization" id="" style="width: 47%;" placeholder="กรุณาป้อนวิธีแก้ไข..." class="form-control">
        </div>
            `)
    }
    if ($('input:radio[name="classify_type_data_in_event_personal_datamark"]:checked'.length > 0)){
        let seleced = $('input:radio[name="classify_type_data_in_event_personal_datamark"]:checked')
        if (seleced.attr('name') == 'classify_type_data_in_event_personal_datamark') {
            if (seleced.attr('id') == 'datamark1') {
                $('span#example-datamark').html('สมมุติป้อนจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["****ย่าง"]')
            } else if (seleced.attr('id') == 'datamark2') {
                $('span#example-datamark').html('สมมุติป้อนจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["***ย***"]')
            } else if (seleced.attr('id') == 'datamark3') {
                $('span#example-datamark').html('สมมุติป้อนจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["ตัวอ****"]')
            }
        }
    }
    $('input:radio').on('click', function () {
        // edit last
        if ($(this).attr("name") == 'classify_risk_assess_only_dpo_data_number_all_used_process_many') {
            if ($(this).attr('id') == "customRadio5" || $(this).attr('id') == "customRadio4") {
                $('span#classification_risk_assessment_only_dpo_data_number_all_used_process_total').html(`
                 &nbsp;<span class='h6' style="font-weight: bold;">ระบุ</span>&nbsp;&nbsp;<input type="text" name="classify_risk_assess_only_dpo_data_number_all_used_process_total" placeholder="" class="form-control input-radio1" /> &nbsp; 
                `)
            }
        }
        if ($(this).attr('name') == 'classify_risk_assess_only_dpo_assess_the_impact_of_data') {
            if ($(this).attr('id') == "customRadio14" || $(this).attr('id') == "customRadio15" || $(this).attr('id') == "customRadio16") {
                $('div#classification_risk_assessment_only_dpo_fix_a_leak_of_data').html(`
            <label class="form-check-label h6"
                                                                    style="font-weight: bold;">วิธีแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อเจ้าของข้อมูล</label>
                                                                <div>
                                                                    <input type="text" name="classify_risk_assess_only_dpo_fix_a_leak_of_data" id="" style="width: 47%;" placeholder="กรุณาป้อนวิธีแก้ไข..." class="form-control">
                                                                </div>
            `)
            }
        }
        if ($(this).attr('name') == 'classify_risk_assess_only_dpo_assess_the_impact_of_organization') {
            if ($(this).attr('id') == "customRadio17" || $(this).attr('id') == "customRadio18" || $(this).attr('id') == "customRadio19") {
                $('div#classification_risk_assessment_only_dpo_fix_a_leak_of_organization').html(`
            <label class="form-check-label h6"
            style="font-weight: bold;">วิธีแก้ไขเมื่อเกิดข้อมูลรั่วไหลหรือผลกระทบต่อองค์กร</label>
        <div>
            <input type="text" name="classify_risk_assess_only_dpo_fix_a_leak_of_organization" id="" style="width: 47%;" placeholder="กรุณาป้อนวิธีแก้ไข..." class="form-control">
        </div>
            `)
            }
        }
        if ($(this).attr('name') == 'classify_type_data_in_event_personal_datamark') {
            if ($(this).attr('id') == 'datamark1') {
                $('span#example-datamark').html('สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["****ย่าง"]')
            } else if ($(this).attr('id') == 'datamark2') {
                $('span#example-datamark').html('สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">3</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["***ย***"]')
            } else if ($(this).attr('id') == 'datamark3') {
                $('span#example-datamark').html('สมมุติจำนวนแสดงตัวอักษร = <span style="color: red;">4</span> <br/> ["ตัวอย่าง"] <span style="color: red;">ทำเครื่องหมาย</span> ["ตัวอ****"]')
            }
        }
    })
    // Modal Add Pattern
    var count_click = 0
    $('a[href="#select-pattern"]').on('click', function () {
        $.post('/pattern', function (result) {
            count_click += 1
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
            var state = {
                'querySet': result.pattern,
                'page': 1,
                'rows': 5,
                'window': 128 * 1024,
            }
            if (count_click == 1) {
                buildTable()
            }
            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);

                var start_count = 1
                document.getElementById('start-pattern-add').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }
            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper-pattern-add')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body-pattern-add').empty()
                    state.page = Number($(this).val())
                    // document.getElementById('page').value = state.page
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body-pattern-add')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet
                for (y in myList) {
                    if (myList[y].pattern_id != "") {
                        let buttonAdd = ""
                        let buttonDel = ""
                        if (result.id_pattern[y].pattern_id == $('input[name="pattern_id"]').val()) {
                            buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            buttonDel = '<a id="_del_pattern" href="javascript:void(0);" data-value=' + myList[y].pattern_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                        } else {
                            buttonAdd = '<a id="_add_pattern" href="javascript:void(0);" data-value=' + myList[y].pattern_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                        }
                        //Keep in mind we are using "Template Litterals to create rows"
                        var row = '<tr>' +
                            '<th style="vertical-align: center;">' +
                            'ชื่อรูปแบบ: ' + myList[y].pattern_name + " <br/>" +
                            '</th>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonAdd +
                            '</td>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonDel +
                            '</td>' +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].pattern_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                    table.append(row)
                    $('#total-pattern-add').text(0)
                    $('#start-pattern-add').html(0)
                } else {
                    if (myList[0].doc_id != "" && myList[0].doc_action != 1 && myList[0].doc_status == 2) {
                        $('#start-pattern-add').text(myList[0].pattern_id)
                    }
                    pageButtons(data.pages)
                    $('#total-pattern-add').text(result.pattern.length)
                }
                $('#end-pattern-add').html(end_count)
                $('a#_add_pattern').on('click', function () {
                    $.post('/pattern/select-pattern', { value: "@lltr@Se1ectP@ttern", id: $(this).attr('data-value') }).done(function (result) {
                        $('input[name="pattern_id"]').val(result.selected_pattern.pattern_id)
                        $('select[name="pattern_processing_base_id"]').find('option:selected').removeAttr('selected')
                        $('input[name="classify_name_part1"]').val(result.selected_pattern.pattern_name)
                        $('input[name="classify_data_exception_or_unnecessary_filter_name"]').val(result.selected_pattern.pattern_name)
                        $('input[name="classify_data_exception_or_unnecessary_filter_tag"]').val(result.selected_pattern.pattern_tag)
                        $('input[name="classify_data_exception_or_unnecessary_filter_label"').val(result.selected_pattern.pattern_label)
                        let fullname = result.total_name_inside[0]
                        $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').val(fullname[0]+" "+fullname[1])
                        $('input[name="classify_user_access_info_process_inside_from_pattern"]').val(result.selected_pattern.pattern_processor_inside_id)
                        let fullname_outside = result.total_name_outside[0]
                        $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').val(fullname[0]+" "+fullname[1])
                        $('input[name="classify_user_access_info_process_outside_from_pattern"]').val(result.selected_pattern.pattern_processor_outside_id)
                        var _file_ = ""
                        var _database_ = ""
                        if (result.selected_pattern.pattern_type_data_file == 1) {
                            _file_ = "มี"
                        } else {
                            _file_ = "ไม่มี"
                        }
                        if (result.selected_pattern.pattern_type_database == 1) {
                            _database_ = result.selected_pattern.pattern_type_database_name
                        } else {
                            _database_ = "ไม่มี"
                        }
                        var _inside_ = ""
                        var _outside_0 = ""
                        var _outside_1 = ""
                        var _outside_2 = ""
                        var _outside_3 = ""
                        if (result.selected_pattern.pattern_storage_method_inside == 1) {
                            _inside_ = "มี"
                        } else {
                            _inside_ = "ไม่มี"
                        }
                        if (result.selected_pattern.pattern_storage_method_outside == 1) {
                            _outside_0 = "มี"
                            if (result.selected_pattern.pattern_storage_method_outside_device == 1) {
                                _outside_1 = result.selected_pattern.pattern_storage_method_outside_device_name
                            } else {
                                _outside_1 = "ไม่มี"
                            }
                            if (result.selected_pattern.pattern_storage_method_outside_agent == 1) {
                                _outside_2 = result.selected_pattern.pattern_storage_method_outside_agent_name
                            } else {
                                _outside_2 = "ไม่มี"
                            }
                            if (result.selected_pattern.pattern_storage_method_outside_database_outside == 1) {
                                _outside_3 = result.selected_pattern.pattern_storage_method_outside_database_outside_name
                            } else {
                                _outside_3 = "ไม่มี"
                            }
                        } else {
                            _outside_0 = "ไม่มี"
                        }
                        var convert_data_name_total = String(result.selected_pattern.data_name_total).replace(/,/ig, ", ")
                        $('textarea[name="example_pattern"]').text(`ชื่อ: ${result.selected_pattern.pattern_name}
ข้อมูล: ${convert_data_name_total}
ระยะเวลา: ${convert_date(new Date(result.selected_pattern.pattern_start_date))} ทั้งหมด: ${result.selected_pattern.pattern_total_date} วัน
ชนิดข้อมูล:
    File Ms Excel/CSV: ${_file_}
    Database: ${_database_}
วิธีการจัดเก็บข้อมูลส่วนบุคคล:
    ภายใน Alltra: ${_inside_}
    ภายนอก: ${_outside_0}, [ (อุปกรณ์: ${_outside_1}),  (Agent: ${_outside_2}),  (Database: ${_outside_3}) ]
`)
                        $('input[name="classify_period_end_follow_pattern_total"]').val(result.selected_pattern.pattern_total_date)
                        $('select[name="pattern_processing_base_id"]').find('option[value=' + result.selected_pattern.pattern_processing_base_id + ']').attr('selected', 'selected')
                        $('div#select-pattern').modal('hide')
                        $('#table-body-pattern-add').empty()
                        buildTable()
                    })
                })
                $('a#_del_pattern').on('click', function () {
                    let selected = $(this).attr('data-value')
                    $('input[name="pattern_id"]').val(null)
                    $('select[name="pattern_processing_base_id"]').find('option:selected').removeAttr('selected')
                    $('input[name="classify_name_part1"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_name"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_tag"]').val(null)
                    $('input[name="classify_data_exception_or_unnecessary_filter_label"').val(null)
                    $('input[name="classify_user_access_info_process_inside_from_pattern"]').val(null)
                    $('input[name="classify_user_access_info_process_outside_from_pattern"]').val(null)
                    $('input[name="classify_user_access_info_process_inside_from_pattern_not_use"]').val(null)
                    $('input[name="classify_user_access_info_process_outside_from_pattern_not_use"]').val(null)
                    $('textarea[name="example_pattern"]').text(null)
                    $('select[name="pattern_processing_base_id"]').find('option:disabled').attr('selected', 'selected')
                    $('input[name="classify_period_end_follow_pattern_total"]').val(null)
                    $('#table-body-pattern-add').empty()
                    buildTable()
                })
            }
        })
    })
    // Modal Select Event-process
    var count_click1 = 0
    $('a[href="#select-event-process"]').on('click', function () {
        $.post('/event_process', { value: "@lltr@Cl@ssific@ti0nEventPr0cess" }, function (result) {
            count_click1 += 1
            // ============================== Create Prepare ============================
            var end_count = 0 // จำนวนทั้งหมดในแต่ละหน้า
            var state = {
                'querySet': result.event,
                'page': 1,
                'rows': 5,
                'window': 128 * 1024,
            }
            if (count_click1 == 1) {
                buildTable()
            }
            function pagination(querySet, page, rows) {

                var trimStart = (page - 1) * rows
                var trimEnd = trimStart + rows

                var trimmedData = querySet.slice(trimStart, trimEnd)

                var pages = Math.ceil(querySet.length / rows);

                var start_count = 1
                document.getElementById('start-event-add').innerHTML = start_count

                return {
                    'querySet': trimmedData,
                    'pages': pages,
                }
            }
            // ============================== Create Pagination ============================
            function pageButtons(pages) {
                var wrapper = document.getElementById('pagination-wapper-event-add')
                wrapper.innerHTML = ``
                var maxLeft = (state.page - Math.floor(state.window / 2))
                var maxRight = (state.page + Math.floor(state.window / 2))

                if (maxLeft < 1) {
                    maxLeft = 1
                    maxRight = state.window
                }

                if (maxRight > pages) {
                    maxLeft = pages - (state.window - 1)
                    if (maxLeft < 1) {
                        maxLeft = 1
                    }
                    maxRight = pages
                }

                if (state.page > 1) {
                    wrapper.innerHTML = `<li class="page-item"><button value=${state.page - 1} class="page page-link"> ย้อนกลับ</button></li>`
                } else {
                    wrapper.innerHTML = `<li class="page-item disabled"><button value=${state.page - 1} class="page page-link" > ย้อนกลับ</button></li>`
                }


                num = 1
                if (maxRight > 5) {
                    if (state.page > (maxRight / 2)) {
                        if ((state.page + 1) > (maxRight / 2)) {
                            wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                            wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        }
                    }
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if ((page >= state.page - 2) && (page <= state.page + 2)) {
                            if (page == state.page) {
                                wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                            else {
                                p = page - 1;
                                wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`;
                            }
                        }
                    }
                    if ((state.page) <= (maxRight / 2)) {
                        mp = maxRight - 1;
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                        wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                    }
                }
                else {
                    for (var page = maxLeft; page <= maxRight; page++) {
                        if (state.page == page) {
                            wrapper.innerHTML += `<li class="page-item active"><button class="page page-link" value=${page}>${page}</button></li>`
                        } else {
                            wrapper.innerHTML += `<li class="page-item "><button class="page page-link" value=${page}>${page}</button></li>`
                        }
                    }
                }

                if (state.page < pages) {
                    wrapper.innerHTML += `<li class="page-item"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                } else {
                    wrapper.innerHTML += `<li class="page-item disabled"></i><button value=${state.page + 1} class="page page-link" >ถัดไป </button>`
                }

                $('.page').on('click', function () {
                    $('#table-body-event-add').empty()
                    state.page = Number($(this).val())
                    // document.getElementById('page').value = state.page
                    buildTable()
                })
            }
            // ============================== Create Table ============================
            function buildTable() {
                var table = $('#table-body-event-add')
                var data = pagination(state.querySet, state.page, state.rows)
                var myList = data.querySet
                for (y in myList) {
                    if (myList[y].event_process_id != "") {
                        let buttonAdd = ""
                        let buttonDel = ""
                        if (myList[y].event_process_name == $('input[name="classify_name_part2"]').val() && result.id_event[y].event_process_id == $('input[name="event_process_id"]').val()) {
                            buttonAdd = '<span class="btn btn-secondary"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></span>'
                            buttonDel = '<a id="_del_event" href="javascript:void(0);" data-value=' + myList[y].event_process_id + '  class="btn btn-danger"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></a>'
                        } else {
                            buttonAdd = '<a id="_add_event" href="javascript:void(0);" data-value=' + myList[y].event_process_id + ' class="btn btn-success"><i class="icon-plus" style="font-size: 25px; vertical-align: middle;"></i></a>'
                            buttonDel = '<span class="btn btn-secondary"><i class="icon-close" style="font-size: 25px; vertical-align: middle;"></i></span>'
                        }
                        //Keep in mind we are using "Template Litterals to create rows"
                        var row = '<tr>' +
                            '<th style="vertical-align: center;">' +
                            'ชื่อกิจกรรม: ' + myList[y].event_process_name + " <br/>" +
                            '</th>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonAdd +
                            '</td>' +
                            '<td style="vertical-align: middle; width: 50px;">' +
                            buttonDel +
                            '</td>' +
                            '</tr>'
                        table.append(row)
                        end_count = myList[y].event_process_id
                    }
                }
                if (myList.length == 0) {
                    var row = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty text-center" style="color: red;">ไม่พบข้อมูล </td></tr>'
                    table.append(row)
                    $('#total-event-add').text(0)
                    $('#start-event-add').html(0)
                } else {
                    if (myList[0].event_process_id != "") {
                        $('#start-event-add').text(myList[0].event_process_id)
                    }
                    pageButtons(data.pages)
                    $('#total-event-add').text(result.event.length)
                }
                $('#end-event-add').html(end_count)
                $('a#_add_event').on('click', function () {
                    $.post('/event_process/get', { value: "@lltr@Cl@ssific@iti0nGetEventPr@cess", id: $(this).attr('data-value') }).done(function (result) {
                        $('input[name="event_process_id"]').val(result.event_process_id)
                        $('input[name="classify_name_part2"]').val(result.event_process_name)
                        $('div#select-event-process').modal('hide')
                        $('#table-body-event-add').empty()
                        buildTable()
                    })
                })
                $('a#_del_event').on('click', function () {
                    $('input[name="event_process_id"]').val(null)
                    $('input[name="classify_name_part2"]').val(null)
                    $('#table-body-event-add').empty()
                    buildTable()
                })
            }
        })
    })
    // Modal Add Processing-base
    $('a#btn_add_processing_base').on('click', function () {
        $('a#add_processing_base').on('click', function () {
            if ($('input[name="pattern_processing_base_name"]').val() != "") {
                var name = $('input[name="pattern_processing_base_name"]').attr('name')
                var value = $('input[name="pattern_processing_base_name"]').val()
                var obj = {}
                obj[name] = value
                $.ajax({
                    url: "/processing_base",
                    method: "POST",
                    data: obj,
                }).done(function () {
                    $('input[name="pattern_processing_base_name"]').val(null);
                    $('div#add-processing-base').modal('hide');
                    $('#select_processing_base').load(document.URL + ' #select_processing_base ');
                })
            }
        })
    })
    // Modal Add Special-conditions
    $('a#btn_add_special_conditions').on('click', function () {
        $('a#add-special-conditions').on('click', function () {
            if ($('input[name="classification_special_conditions_name"]').val() != "") {
                var name = $('input[name="classification_special_conditions_name"]').attr('name')
                var value = $('input[name="classification_special_conditions_name"]').val()
                var obj = {}
                obj[name] = value
                $.ajax({
                    url: "/event_process/add",
                    method: "POST",
                    data: obj,
                }).done(function () {
                    $('input[name="classification_special_conditions_name"]').val(null)
                    $('div#add_special_conditions').modal('hide')
                    $('#select_event_process').load(document.URL + ' #select_event_process')
                })
            }
        })
    })
    function delClassify(id){
        let url = '/classification/delete'+id
        $.post('/classification/getDel',{value: "@lltr@Cl@ssifyGetDe1", id: id}).done(function(result){
            $('td#c-name').text(result.classify[0].classify_name)
            $('td#c-data').text(result.data_name_total[0])
            $('td#c-create').text(convert_date(new Date(result.classify[0].classify_create)))
            $('td#c-creator').text(result.classify[0].firstname+" "+result.classify[0].lastname)
        })
        $("button#sub-del-classify").on('click',function(){
            $('form#del-classify').attr('action', url).submit();
        })
    }
} else if (document.getElementById('detail')) {
    // Sidebar
    $('li#li-data-project-action').addClass('selected')
    $('a#li-data-project-action').addClass('active')
    $('ul#ul-data-project-action').addClass('in')
    $('a#a-export').addClass('active')
    $('ul#ul-export').addClass('in')
    $('a#a-classification').addClass('active')
}