
// var today = new Date();
// var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
// $('#date').text(date);

$(function () {
    get_data()
    function get_data(data) {
        if (data == "reface") {
            $.ajax({ // ready get api date
                type: 'GET',
                url: '/api/report/emailconsent',
                success: function (result) {
                    if (result == "ไม่มีข้อมูล") {
                        $('#table_sortable').remove()
                        $('#table-body').remove()
                        ready_datanull()
                    } else {
                        $('#table_sortable').remove()
                        $('#table-body').remove()
                        Tabeldata_ajax(result.log_pdpa_email)
                    }

                    // Graph_email(result);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        } else {
            $.ajax({ // ready get api date
                type: 'GET',
                url: '/api/report/emailconsent',
                success: function (result) {
                    if (result == "ไม่มีข้อมูล") {
                        ready_datanull()
                    } else {
                        Tabeldata_ajax(result.log_pdpa_email)
                        Graph_email(result);
                    }

                },
                error: function (e) {
                    console.log(e);
                }
            });
        }
    };

    $("#reface").on("click", function (e) { // กรณีกดปุ่ม reface
        $('#Srearch').val(null)
        get_data("reface")
    });

    $('#Srearch').on("keyup", function (e) { // ค้นหา ข้อมูล
        var data = ({ "data": $(this).val() });
        if ($(this).val() == "") {
            get_data("reface")
        } else {
            $.ajax({ // Srearch get api date
                type: "post",
                contentType: "application/json",
                url: '/api_report_emailconsent_search',
                data: JSON.stringify(data),
                dataType: 'json',
                success: function (result) {
                    $('#table_sortable').remove();
                    $('#table-body').remove();
                    Tabeldata_ajax(result);
                },
                error: function (e) {
                    $('#table-body').empty()
                    $('#table-body').append('<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่พบข้อมูล</b></div></div></td>'
                    );
                }
            });
        }
    });

    function ready_datanull() {
        var content = '<table class="tablesaw no-wrap table-striped table-bordered table-hover table" id="table_sortable">'
        content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
        content += '</table>'
        $('#table_show').append(content);
        var table_thead = $('#table-thead');
        var thead =
            `<tr>
     <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>ลำดับ</th>
     <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>อีเมล</th>
     <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>หัวข้ออีเมล</th>
     <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>วันที่ส่ง</th>
     <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>วันที่ตอบกลับ</th>
     <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>สถานะ</th>
    </tr>`;
        table_thead.append(thead)
        $('#table-body').empty()
        $('#table-body').append('<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่พบข้อมูล</b></div></div></td>'
        );
    };



    function Tabeldata_ajax(data) {
        var dataTable = [];
        for (var i = 0; i < data.length; i++) {

            dataTable.push({
                "no": (i + 1),
                "date_send": data[i].date_send,
                "date_consent": data[i].date_consent,
                "subject": data[i].email_subject,
                "status": data[i].email_status,
                "to": data[i].email_to
            });
        }

        var state = {
            'querySet': dataTable,
            'page': 1,
            'rows': 10, // จำนวน row
            'window': 10000, // จำนวนหน้าที่เเสดง
        }

        buildTable()
        function pagination(querySet, page, rows) {
            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows
            var trimmedData = querySet.slice(trimStart, trimEnd)
            var pages = Math.ceil(querySet.length / rows); // Math.ceil ปัดทศนิยมขึ้น Math.round ปัดทศนิยมลง
            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        }

        function pageButtons(pages) {
            var wrapper = document.querySelector('.pagination')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }
            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            // เช็คหน้าเเรก (ปุ่มย้อนกลับ)
            var num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                        }
                        else {
                            wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                    }
                }
            }
            if (state.page == 1) {
                wrapper.innerHTML = `<li  class="page-item disabled"><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            } else {
                wrapper.innerHTML = `<li value=${state.page - 1} class="page page-item "><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            }
            // เช็คหน้าสุดท้าย (ปุ่มถัดไป)
            if (state.page == pages) {
                wrapper.innerHTML += `<li  class=" page-item disabled"><button class="page-link">ถัดไป</button></li>`
            } else {
                wrapper.innerHTML += `<li value=${state.page + 1} class="page page-item"><button class="page-link">ถัดไป</button></li>`
            }

            $('.page').on('click', function () {
                $('#table-body').empty()
                $('#table-thead').empty()
                $('#table_sortable').remove()
                state.page = Number($(this).val())
                buildTable()
            })
        }

        function buildTable() {
            var content = '<table class="tablesaw no-wrap table-striped table-bordered table-hover table" id="table_sortable">'
            content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
            content += '</table>'
            $('#table_show').append(content);
            var table = $('#table-body');
            var table_thead = $('#table-thead');
            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            var show = [];
            var thead =
                `<tr>
         <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>ลำดับ</th>
         <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>อีเมล</th>
         <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>หัวข้ออีเมล</th>
         <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>วันที่ส่ง</th>
         <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>วันที่ตอบกลับ</th>
         <th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>สถานะ</th>
        </tr>`;
            table_thead.append(thead)
            for (var i in myList) {
                var row = '<tr><td>' + myList[i].no +
                    '</td><td>' + myList[i].to +
                    '</td><td>' + myList[i].subject +
                    '</td><td>' + myList[i].date_send +
                    '</td><td>' + myList[i].date_consent +
                    '</td><td>' + '<span class="badge bg-success">เรีบยร้อย</span>' +
                    '</td></tr>'
                table.append(row)
                show.push(myList[i].no)
            }
            document.querySelector("#show").innerHTML = show[0];  //  แสดงถึง row เเรกของหน้า 
            document.querySelector("#to_show").innerHTML = show[show.length - 1];  //  แสดงถึง row สุดท้ายของหน้า
            document.querySelector("#show_all").innerHTML = state.querySet.length;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
            pageButtons(data.pages)
        }
    };


    function Graph_email(data_graph) {

        var count_0 = data_graph.count_0;
        var count_1 = data_graph.count_1;
        var count_2 = data_graph.count_2;

        var date_0 = [];
        var count_data_0 = [];
        for (let i = 0; i < count_0.length; i++) {
            date_0.push(count_0[i].date_sent);
            count_data_0.push(count_0[i].count_0);

        };
        var date_1 = [];
        var count_data_1 = [];
        for (let i = 0; i < count_1.length; i++) {
            date_0.push(count_1[i].date_consent);
            count_data_1.push(count_1[i].count_1);
        };

        var date_2 = [];
        var count_data_2 = [];
        for (let i = 0; i < count_2.length; i++) {
            date_0.push(count_2[i].date_consent);
            date_2.push(count_2[i].date_consent);
            count_data_2.push(count_2[i].count_2);
        };



        var area_pdpa_email = data_graph.area_pdpa_email;
        var log_pdpa_email = data_graph.log_pdpa_email;

        var dataGraphsend = [];
        var dataGraphconsent = [];
        var day_Graphsend = [];
        var day_Graphconsent = [];
        // console.log(area_pdpa_email);

        for (var i = 0; i < area_pdpa_email.length; i++) {
            dataGraphsend.push(area_pdpa_email[i].count_send);
            dataGraphconsent.push(area_pdpa_email[i].count_consent);
            if (area_pdpa_email[i].date_send == area_pdpa_email[i].date_consent) {
                day_Graphsend.push(area_pdpa_email[i].date_send)
            } else {
                day_Graphsend.push(area_pdpa_email[i].date_send)
                day_Graphsend.push(area_pdpa_email[i].date_consent)
            }
        }
        var date_update = [];
        for (let i = 0; i < day_Graphsend.length; i++) {
            if (day_Graphsend[i] == day_Graphsend[i + 1]) {
            } else if (day_Graphsend[i] == null) {
                date_update.push('');
            } else {
                date_update.push(day_Graphsend[i]);
            }
        }
        var date_consent = [];
        var date_send = [];
        for (let i = 0; i < log_pdpa_email.length; i++) {
            date_consent.push(log_pdpa_email[i].date_consent);
            date_send.push(log_pdpa_email[i].date_send);
        }
        // console.log(date_update);

        var date_update_0 = [];
        for (var i = 0; i < date_0.length; i++) {
            if (date_update_0.indexOf(date_0[i]) < 0) {
                date_update_0.push(date_0[i]);
            }
        }


        var Revenue_Statistics = {
            series: [
                // {
                //     name: "ส่งไม่ผ่าน",
                //     data: [6, 10, 5, 1, 5, 6]
                // },
                {
                    name: "จำนวนที่ส่ง",
                    data: dataGraphsend
                },
                {
                    name: "จำนวนที่ตอบกลับ",
                    data: dataGraphconsent
                },

            ],
            chart: {
                fontFamily: 'Rubik,sans-serif',
                height: 350,
                type: "area",
                toolbar: {
                    show: false,
                },
            },
            fill: {
                type: 'solid',
                opacity: 0.2,
                colors: ["#009efb", "#39c449"],
            },
            grid: {
                show: true,
                borderColor: "rgba(0,0,0,0.1)",
                strokeDashArray: 3,
                xaxis: {
                    lines: {
                        show: true
                    }
                },
            },
            colors: ["#009efb", "#39c449"],
            dataLabels: {
                enabled: false,
            },
            stroke: {
                curve: "smooth",
                width: 2,
                colors: ["#009efb", "#39c449"],
            },
            markers: {
                size: 5,
                colors: ["#009efb", "#39c449"],
                strokeColors: "transparent",
            },
            xaxis: {
                axisBorder: {
                    show: true,
                },
                axisTicks: {
                    show: true,
                },
                // categories: [day_Graphsend][day_Graphsend], // จำนวน ข้อมูล แกน x
                categories: date_update_0, // จำนวน ข้อมูล แกน x
                labels: {
                    style: {
                        colors: "#a1aab2",
                    },
                },
            },
            yaxis: {
                // categories: day_Graphsend, // จำนวน ข้อมูล แกน x
                tickAmount: 9,
                labels: {
                    style: {
                        colors: "#a1aab2",
                    },
                },
            },
            tooltip: {
                x: {
                    format: "dd/MM/yy HH:mm",
                },
                theme: "dark",
            },
            legend: {
                show: false,
            },
        };

        var chart_area_spline = new ApexCharts(document.querySelector("#revenue-statistics"), Revenue_Statistics);
        chart_area_spline.render();
    };


});
