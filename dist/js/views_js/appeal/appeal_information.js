$('#approve').on("click", function () {
    var id = $(this).val()
    console.log($(this).val());
    Swal.fire({
        title: 'อนุมัติ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#39c449',
        confirmButtonText: 'ตกลง'
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
                'อนุมัติสำเร็จ',
                '',
                'success'
            )
            timer: 50000,
                window.location = '/update/approve/' + id;
        }
    })
});

$('#deny').on("click", function () {
    var id = $(this).val()
    console.log($(this).val());
    Swal.fire({
        title: 'อนุมัติ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#39c449',
        confirmButtonText: 'ตกลง'
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
                'อนุมัติสำเร็จ',
                '',
                'success'
            )
            timer: 50000,
                window.location = '/update/deny/' + id;
        }
    })
});