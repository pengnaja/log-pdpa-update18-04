//  function ในการดูข้อมูล modal popup

$(function () {
    // function ต่างๆที่อาจใช้งานใน   $(function () {  ได้ 
    var utc = new Date().toJSON().slice(0, 10);
    document.getElementById('date').value = utc;

    $.ajax({ // ready get api date
        type: 'GET',
        url: '/api/get/appeal',
        success: function (result) {
            if (result == "ไม่มีข้อมูล") {
                ready_datanull()
            } else {
                Tabeldata_ajax(result.pdpa_appeal)
                chart_line_complaint(result)
            }
        },
        error: function (e) {
            console.log(e);
        }
    });


    function ready_datanull() {
        var content = '<table class="tablesaw no-wrap table-striped table-bordered table-hover table" id="table_sortable" data-tablesaw-sortable>'
        content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
        content += '</table>'
        $('#table_Data-appeal').append(content);
        // var table = $('#table-body');
        var table_thead = $('#table-thead');
        var thead =
            `<tr>
                <th data-tablesaw-sortable-col>ลำดับ</th>
                <th data-tablesaw-sortable-col> ชื่อผู้ร้องเรียน นามสกุล</th>
                <th data-tablesaw-sortable-col>ที่อยู่</th>
                <th data-tablesaw-sortable-col>ช่องทางติดต่อ</th>
                <th data-tablesaw-sortable-col>รายละเอียดร้องเรียน</th>
                <th data-tablesaw-sortable-col>วันที่ร้องเรียน</th>
                <th data-tablesaw-sortable-col>เเชร์ข้อมูล</th>
                <th data-tablesaw-sortable-col>ดูข้อมูล</th>
                <th data-tablesaw-sortable-col>สถานะ</th>
                </tr>`;
        table_thead.append(thead)

        $('#table-body').empty()
        $('#table-body').append('<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่มีข้อมูล</b></div></div></td>'
        );

    };


    $("#reface").on("click", function (e) { // กรณีกดปุ่ม reface
        $('#Srearch').val(null)
        data_null()
    });

    function data_null() { // กรณีค้นหาไม่เจอข้อมูล
        $.ajax({ // ready get api date
            type: 'GET',
            url: '/api/get/appeal',
            success: function (result) {
                if (result == "ไม่มีข้อมูล") {
                    $('#table_sortable').remove()
                    $('#table-body').remove()
                    ready_datanull()
                } else {
                    $('#table_sortable').remove()
                    $('#table-body').remove()
                    Tabeldata_ajax(result.pdpa_appeal)
                }

            },
            error: function (e) {
                console.log(e);
            }
        });
    };


    $('#Srearch').on("keyup", function (e) { // ค้นหา ข้อมูล
        var data = ({ "data": $(this).val() });
        if (data.data.indexOf(" ") > 0) {
            var date_formate = ((data.data).replaceAll(" ", ",")).split(",");
            data = ({ "data": date_formate[1] });
        }
        if ($(this).val() == "") {
            data_null()
        } else {
            $.ajax({ // Srearch get api date
                type: "post",
                contentType: "application/json",
                url: '/api/get/appeal/search',
                data: JSON.stringify(data),
                dataType: 'json',
                success: function (result) {
                    $('#table_sortable').remove();
                    $('#table-body').remove();
                    Tabeldata_ajax(result);
                },
                error: function (e) {
                    $('#table-body').empty()
                    $('#table-body').append('<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่พบข้อมูล</b></div></div></td>'
                    );
                }
            });
        }
    });

    $('#Srearch_personal').on("keyup", function (e) { // ค้นหาข้อมูลจากข้อมูลส่วนบุคคลทั้งหมด
        var data = ({ "data": $(this).val() });
        if ($(this).val() == "") {
            console.log("ไม่มีข้อความ");
            // data_null()
        } else {
            console.log("มีข้อความ");
            $.ajax({ // Srearch get api date
                type: "post",
                contentType: "application/json",
                url: '/api/get/personal_data/search',
                data: JSON.stringify(data),
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    // $('#table_sortable').remove();
                    // $('#table-body').remove();
                    // Tabeldata_ajax(result);
                },
                error: function (e) {
                    console.log(e);
                    // $('#table-body').empty()
                    // $('#table-body').append('<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่พบข้อมูล</b></div></div></td>'
                    // );
                }
            });
        }
    });


    function Tabeldata_ajax(data) {
        var Getdata = [];
        for (var i = 0; i < data.length; i++) {
            Getdata.push({
                "no": (i + 1),
                "id_ap": data[i].id_ap,
                "address": data[i].appeal_address,
                "approved_complaint": data[i].appeal_approved_complaint,
                "contact": data[i].appeal_contact,
                "date": data[i].appeal_date,
                "detail": data[i].appeal_detail,
                "firstname": data[i].appeal_firstname,
                "lastname": data[i].appeal_lastname,
                "prefix": data[i].appeal_prefix,
                "share": data[i].appeal_share,
            });
        }

        var state = {
            'querySet': Getdata,
            'page': 1,
            'rows': 10, // จำนวน row
            'window': 10000, // จำนวนหน้าที่เเสดง
        }

        buildTable()

        function pagination(querySet, page, rows) {
            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows
            var trimmedData = querySet.slice(trimStart, trimEnd)
            var pages = Math.ceil(querySet.length / rows); // Math.ceil ปัดทศนิยมขึ้น Math.round ปัดทศนิยมลง
            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        };

        function pageButtons(pages) {
            // var wrapper = document.getElementById('pagination-wrapper')
            var wrapper = document.querySelector('.pagination')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }
            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            // เช็คหน้าเเรก (ปุ่มย้อนกลับ)
            var num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                        }
                        else {
                            wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                    }
                }
            }

            if (state.page == 1) {
                wrapper.innerHTML = `<li  class="page-item disabled"><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            } else {
                wrapper.innerHTML = `<li value=${state.page - 1} class="page page-item "><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            }


            // เช็คหน้าสุดท้าย (ปุ่มถัดไป)
            if (state.page == pages) {
                wrapper.innerHTML += `<li  class=" page-item disabled"><button class="page-link">ถัดไป</button></li>`
            } else {
                wrapper.innerHTML += `<li value=${state.page + 1} class="page page-item"><button class="page-link">ถัดไป</button></li>`
            }


            $('.page').on('click', function () {
                $('#table-body').empty()
                $('#table-thead').empty()
                $('#table_sortable').remove() // ลบ เพื่อไม่ให้มันสรา้ง row ใน table เปล่าขึ้นมา
                state.page = Number($(this).val())
                buildTable()
            })
        };


        function buildTable() {
            var content = '<table class="tablesaw no-wrap table-striped table-bordered table-hover table" id="table_sortable" data-tablesaw-sortable>'
            content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
            content += '</table>'
            $('#table_Data-appeal').append(content);
            // $('#table_sortable').attr('data-tablesaw-sortable');
            var table = $('#table-body');
            var table_thead = $('#table-thead');
            var thead =
                `<tr>
                    <th data-tablesaw-sortable-col>ลำดับ</th>
                    <th data-tablesaw-sortable-col> ชื่อผู้ร้องเรียน นามสกุล</th>
                    <th data-tablesaw-sortable-col>ที่อยู่</th>
                    <th data-tablesaw-sortable-col>ช่องทางติดต่อ</th>
                    <th data-tablesaw-sortable-col>รายละเอียดร้องเรียน</th>
                    <th data-tablesaw-sortable-col>วันที่ร้องเรียน</th>
                    <th data-tablesaw-sortable-col>เเชร์ข้อมูล</th>
                    <th data-tablesaw-sortable-col>ดูข้อมูล</th>
                    <th data-tablesaw-sortable-col>สถานะ</th>
                    </tr>`;
            table_thead.append(thead)


            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            var show = [];


            for (var i in myList) {
                var text_status = "";
                if (myList[i].approved_complaint == 1) {
                    text_status = ' <span class="badge bg-success">เรียบร้อย</span>'
                } else if (myList[i].approved_complaint == 0) {
                    text_status = '<span class="badge bg-warning">รออนุมัติ</span>'
                } else {
                    text_status = '<span class="badge bg-danger">ปฏิเสธ</span>'
                }
                var row = '<tr><td>' + myList[i].no +
                    '</td><td>' + myList[i].prefix + myList[i].firstname + " " + myList[i].lastname +
                    '</td><td>' + myList[i].address +
                    '</td><td>' + myList[i].contact +
                    '</td><td>' + myList[i].detail +
                    '</td><td>' + myList[i].date +
                    '</td><td>' + 'เเชร์ข้อมูล' +
                    '</td><td>' + '<a class="text-info"  href="/appreal_information/' + myList[i].id_ap + '"><i class=" fas fa-file-alt fa-2x"></i></a>' +
                    '</td><td>' + text_status +
                    '</td></tr>'
                table.append(row)
                show.push(myList[i].no)
            }
            document.querySelector("#show").innerHTML = show[0];  //  แสดงถึง row เเรกของหน้า 
            document.querySelector("#to_show").innerHTML = show[show.length - 1];  //  แสดงถึง row สุดท้ายของหน้า
            document.querySelector("#show_all").innerHTML = state.querySet.length;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
            pageButtons(data.pages)
        };

    };

    function chart_line_complaint(data) {

        var count_true = data.count_1
        var count_false = data.count_0
        var count_refuse = data.count_2
        // console.log("count_refuse", count_refuse);
        var date_all = [];
        var date_refuse = [];


        // จัดรูปเเบบข้อมูลของ count_true ให้อยู่เเบบ array 
        var sum_count_1 = 0;
        var date_true = [];
        for (let i = 0; i < count_true.length; i++) {
            date_all.push(count_true[i].appeal_date)
            date_true.push(count_true[i].appeal_date)
            sum_count_1 += count_true[i].count_1
        }
        // จัดรูปเเบบข้อมูลของ count_false ให้อยู่เเบบ array 
        var sum_count_0 = 0;
        var date_false = [];
        for (let i = 0; i < count_false.length; i++) {
            date_all.push(count_false[i].appeal_date)
            date_false.push(count_false[i].appeal_date)
            sum_count_0 += count_false[i].count_0
        }
        // จัดรูปเเบบข้อมูลของ count_refuse ให้อยู่เเบบ array 
        var sum_count_2 = 0;
        var date_refuse = [];
        for (let i = 0; i < count_refuse.length; i++) {
            date_all.push(count_refuse[i].appeal_date)
            date_refuse.push(count_refuse[i].appeal_date)
            sum_count_2 += count_refuse[i].count_2
        }


        var date_show_lineChart = [];
        for (var i = 0; i < date_all.length; i++) {
            if (date_show_lineChart.indexOf(date_all[i]) < 0) {
                date_show_lineChart.push(date_all[i]);
            }
        }

        var data_count_false = [];
        var data_count_true = [];
        var data_count_refuse = [];
        var count_round_false = 0;
        var count_round_true = 0;
        var count_round_refuse = 0;

        for (let j = 0; j < date_show_lineChart.sort().length; j++) {
            if (date_false.find(element => element == date_show_lineChart[j])) {
                data_count_false.push(count_false[count_round_false].count_0);
                count_round_false++;
            }
            else {
                data_count_false.push("0");
            }
        };
        for (let j = 0; j < date_show_lineChart.sort().length; j++) {
            if (date_true.find(element => element == date_show_lineChart[j])) {
                data_count_true.push(count_true[count_round_true].count_1);
                count_round_true++;
            }
            else {
                data_count_true.push("0");
            }
        };
        for (let j = 0; j < date_show_lineChart.sort().length; j++) {
            if (date_refuse.find(element => element == date_show_lineChart[j])) {
                data_count_refuse.push(count_refuse[count_round_refuse].count_2);
                count_round_refuse++;
            }
            else {
                data_count_refuse.push("0");
            }
        };

        new Chart(document.getElementById("line-chart"), {
            type: "line",
            data: {
                labels: date_show_lineChart.sort(),
                datasets: [
                    {
                        data: data_count_true,
                        label: "เรียบร้อย",
                        borderColor: "#39c449",
                        fill: false,

                    },
                    {
                        data: data_count_false,
                        // data: [" ", 1, 2, " ", ""],
                        label: "รออนุมัติ",
                        borderColor: "#ffbc34",
                        fill: false,
                    },
                    {
                        data: data_count_refuse,
                        label: "ปฏิเสธ",
                        borderColor: "red",
                        fill: false,
                    },
                ],
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "#b2b9bf",
                    },
                },
                title: {
                    display: true,
                    fontColor: "#b2b9bf",
                    text: "ข้อมูลการรับเรื่องร้องเรียน",
                },
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                fontColor: "#b2b9bf",
                                fontSize: 12,
                            },
                        },
                    ],
                    xAxes: [
                        {
                            ticks: {
                                fontColor: "#b2b9bf",
                                fontSize: 12,
                            },
                        },
                    ],
                },
            },
        });



        new Chart(document.getElementById("pie-chart-appeal"), {
            type: "pie",
            data: {
                labels: ["รออนุมัติ", "เรียบร้อย", "ปฏิเสธ"],
                datasets: [
                    {
                        label: "Population (millions)",
                        backgroundColor: [
                            "#ffbc34",
                            "#39c449",
                            "red",
                        ],
                        data: [sum_count_0, sum_count_1, sum_count_2],
                    },
                ],
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "#b2b9bf",
                    },
                },
                title: {
                    display: true,
                    fontColor: "#b2b9bf",
                    text: "Predicted world population (millions) in 2050",
                },
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            fontColor: "#b2b9bf",
                            fontSize: 12,
                        },
                    },
                ],
                xAxes: [
                    {
                        ticks: {
                            fontColor: "#b2b9bf",
                            fontSize: 12,
                        },
                    },
                ],
            },
        });

    };

});