
$(function () {

    $.ajax({ // ready get api date
        type: 'GET',
        url: '/api/report/cookiesconsent',
        success: function (result) {
            Tabeldata_ajax(result)
            build_Pie_Chart(result.pie_setting_cookiepolicy)
        },
        error: function (e) {
            console.log(e);
        }
    });

    function data_null() { // function ถ้าค้นหาเเล้วไม่มีข้อมูล
        $.ajax({ // ready get api date
            type: 'GET',
            url: '/api/report/cookiesconsent',
            success: function (result) {
                $('#table_sortable').remove()
                $('#table-body').remove()
                Tabeldata_ajax(result)
            },
            error: function (e) {
                console.log(e);
            }
        });
    };

    $("#reface").on("click", function (e) { // กรณีกดปุ่ม reface
        $('#Srearch').val(null)
        data_null()
    });

    $("#Srearch").on("keyup", function (e) {
        var data = ({ "data": $(this).val() });
        var data_sendSearch = "";
        if ($(this).val() == "") {
            data_sendSearch = 'ว่างเปล่า';
            data_null()
        } else {
            $.ajax({ // Srearch get api date
                type: "post",
                contentType: "application/json",
                url: '/api/search/report/cookiesconsent',
                data: JSON.stringify(data),
                dataType: 'json',
                success: function (result) {
                    $('#table_sortable').remove()
                    $('#table-body').remove()
                    Tabeldata_ajax(result)
                },
                error: function (e) {
                    $('#table-body').empty()
                    $('#table-body').append(
                        '<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่พบข้อมูล</b></div></div></td>'
                    );
                }
            });
        };
    });


    function Tabeldata_ajax(data) {
        var data_cookiepolicy = data.cookiepolicy
        var data_log_cookiepolicy = data.log_cookiepolicy

        var log_cookiepolicy = [];
        for (var i = 0; i < data_log_cookiepolicy.length; i++) {
            log_cookiepolicy.push({
                "no": (i + 1),
                "id_dg": data_log_cookiepolicy[i].id_dg,
                "id_lsc": data_log_cookiepolicy[i].id_lsc,
                "id_policylog": data_log_cookiepolicy[i].id_policylog,
                "cookiepolicy": data_log_cookiepolicy[i].cookiepolicy,
                "day": data_log_cookiepolicy[i].day,
                "namedomain_dg": data_log_cookiepolicy[i].namedomain_dg,
                "policylog_browser": data_log_cookiepolicy[i].policylog_browser,
                "policylog_ip": data_log_cookiepolicy[i].policylog_ip,
                "policylog_sessionid": data_log_cookiepolicy[i].policylog_sessionid,
            });
        }

        var state = {
            'querySet': log_cookiepolicy,
            'page': 1,
            'rows': 10, // จำนวน row
            'window': 10000, // จำนวนหน้าที่เเสดง
        }

        buildTable()

        function pagination(querySet, page, rows) {
            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows
            var trimmedData = querySet.slice(trimStart, trimEnd)
            var pages = Math.ceil(querySet.length / rows); // Math.ceil ปัดทศนิยมขึ้น Math.round ปัดทศนิยมลง
            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        };

        function pageButtons(pages) {
            var wrapper = document.querySelector('.pagination')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }
            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            // เช็คหน้าเเรก (ปุ่มย้อนกลับ)
            var num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                        }
                        else {
                            wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                    }
                }
            }

            if (state.page == 1) {
                wrapper.innerHTML = `<li  class="page-item disabled"><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            } else {
                wrapper.innerHTML = `<li value=${state.page - 1} class="page page-item "><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            }


            // เช็คหน้าสุดท้าย (ปุ่มถัดไป)
            if (state.page == pages) {
                wrapper.innerHTML += `<li  class=" page-item disabled"><button class="page-link">ถัดไป</button></li>`
            } else {
                wrapper.innerHTML += `<li value=${state.page + 1} class="page page-item"><button class="page-link">ถัดไป</button></li>`
            }


            $('.page').on('click', function () {
                $('#table-body').empty()
                $('#table-thead').empty()
                $('#table_sortable').remove() // ลบ เพื่อไม่ให้มันสรา้ง row ใน table เปล่าขึ้นมา
                state.page = Number($(this).val())
                buildTable()
            })
        };


        function buildTable() {
            var content = '<table class="tablesaw no-wrap table-striped table-bordered table-hover table" id="table_sortable" data-tablesaw-sortable>'
            content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
            content += '</table>'
            $('#table_Data-cookie').append(content);
            $('#table_sortable').attr('data-tablesaw-sortable');
            var table = $('#table-body');
            var table_thead = $('#table-thead');
            var thead_cookie;
            for (let i = 0; i < data_cookiepolicy.length; i++) {
                thead_cookie += '<th>' + data_cookiepolicy[i].name_cp + '</th>';
            }
            var thead =
                '<tr>' +
                '<th>ลำดับ</th>' +
                '<th>ชื่อโดเมน</th>' +
                '<th>ไอพีแอดเดรส</th>' +
                '<th>เบราว์เซอร์</th>' +
                '<th>เซสชั่นไอดี</th>' +
                '<th>วันที่</th>' +
                thead_cookie
                +
                '</tr >';
            table_thead.append(thead)


            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            var show = [];

            for (var i in myList) {
                var td_cookie = [];
                for (var k in data_cookiepolicy) {
                    if (myList[i].cookiepolicy[k] == 1) {
                        td_cookie.push('<td><span class="badge bg-success">ยินยอม</span></td>');
                    } else {
                        td_cookie.push('<td><span class="badge bg-danger">ไม่ยินยอม</span></td>');
                    }
                }

                var row = '<tr>' +
                    '<td>' + myList[i].no + '</td>' +
                    '<td>' + myList[i].namedomain_dg + '</td>' +
                    '<td>' + myList[i].policylog_ip + '</td>' +
                    '<td>' + myList[i].policylog_browser + '</td>' +
                    '<td>' + myList[i].policylog_sessionid + '</td>' +
                    '<td>' + myList[i].day + '</td>' +
                    td_cookie +
                    '</tr>';
                table.append(row)
                show.push(myList[i].no)
            }
            document.querySelector("#show").innerHTML = show[0];  //  แสดงถึง row เเรกของหน้า 
            document.querySelector("#to_show").innerHTML = show[show.length - 1];  //  แสดงถึง row สุดท้ายของหน้า
            document.querySelector("#show_all").innerHTML = state.querySet.length;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
            pageButtons(data.pages)
        };

    };

    function build_Pie_Chart(data) {
        var data_pie_setting_cookiepolicy = data
        // ถ้าเข้าใจไม่ผิดคือการเอา log ที่ user กด (ปฏิเสธ,อนุญาตตามที่เลือก,อนุญาตทั้งหมด) มาเเสดงผลเป็น pie chart
        new Chart(document.getElementById("pie-chart-report-cookieconsent"), {
            type: "pie",
            data: {
                labels: ["ปฏิเสธ", "อนุญาตตามที่เลือก", "อนุญาตทั้งหมด"],
                datasets: [
                    {
                        label: "Population (millions)",
                        backgroundColor: [
                            "red",
                            "#18E11A",
                            "#4bc0c0",
                        ],
                        data: [data_pie_setting_cookiepolicy[0].count, data_pie_setting_cookiepolicy[1].count, data_pie_setting_cookiepolicy[2].count],
                    },
                ],
            },
            options: {
                legend: {
                    labels: {
                        fontColor: "#b2b9bf",
                    },
                },
                title: {
                    display: true,
                    fontColor: "#b2b9bf",
                    text: "Predicted world population (millions) in 2050",
                },
            },
            scales: {
                yAxes: [
                    {
                        ticks: {
                            fontColor: "#b2b9bf",
                            fontSize: 12,
                        },
                    },
                ],
                xAxes: [
                    {
                        ticks: {
                            fontColor: "#b2b9bf",
                            fontSize: 12,
                        },
                    },
                ],
            },
        });
    }
});