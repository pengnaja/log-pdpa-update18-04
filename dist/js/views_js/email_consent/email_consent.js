$(function () {
    $.ajax({ // ready get api date
        type: 'GET',
        url: '/api/email_consent',
        success: function (result) {
            if (result == "ไม่มีข้อมูล") {
                ready_datanull()
            } else {
                Tabeldata_ajax(result)
            }
        },
        error: function (e) {
            console.log(e);
        }
    });

    $("#reface").on("click", function (e) { // กรณีกดปุ่ม reface
        $('#Srearch').val(null)
        data_null()
    });


    function ready_datanull() {
        var content = '<table class="tablesaw no-wrap table-striped table-bordered table-hover table" id="table_sortable" data-tablesaw-sortable>'
        content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
        content += '</table>'
        $('#table_Data_Email_consent').append(content);
        // $('#table_sortable').attr('data-tablesaw-sortable');
        var table = $('#table-body');
        var table_thead = $('#table-thead');
        var thead =
            `<tr>
                <th data-tablesaw-sortable-col>ลำดับ</th>
                <th data-tablesaw-sortable-col>อีเมล</th>
                <th data-tablesaw-sortable-col>หัวเรื่องอีเมล</th>
                <th data-tablesaw-sortable-col>วันที่จัดส่ง</th>
                <th data-tablesaw-sortable-col>สถานะ</th>
                <th data-tablesaw-sortable-col>ส่งใหม่</th>
                </tr>`;
        table_thead.append(thead)
        $('#table-body').empty()
        $('#table-body').append('<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่พบข้อมูล</b></div></div></td>'
        );
    };


    function data_null() { // กรณีค้นหาไม่เจอข้อมูล
        $.ajax({ // ready get api date
            type: 'GET',
            url: '/api/email_consent',
            success: function (result) {
                if (result == "ไม่มีข้อมูล") {
                    $('#table_sortable').remove()
                    $('#table-body').remove()
                    ready_datanull()
                } else {
                    $('#table_sortable').remove()
                    $('#table-body').remove()
                    Tabeldata_ajax(result)
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
    };


    $('#Srearch').on("keyup", function (e) { // ค้นหา ข้อมูล
        e.preventDefault();
        var data = ({ "data": $(this).val() });
        var data_sendSearch = "";
        if ($(this).val() == "") {
            data_sendSearch = 'ว่างเปล่า';
            data_null()
        } else {
            $.ajax({ // Srearch get api date
                type: "post",
                contentType: "application/json",
                url: '/api/email_consent/search',
                data: JSON.stringify(data),
                dataType: 'json',
                success: function (result) {
                    $('#table_sortable').remove()
                    $('#table-body').remove()
                    Tabeldata_ajax(result)
                },
                error: function (e) {
                    $('#table-body').empty()
                    $('#table-body').append('<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่พบข้อมูล</b></div></div></td>'
                    );
                }
            });
        }
    });


    function Tabeldata_ajax(data) {
        var Getdata = [];
        for (var i = 0; i < data.length; i++) {
            Getdata.push({
                "no": (i + 1),
                "id_email": data[i].id_email,
                "content": data[i].email_content,
                "files": data[i].email_files,
                "status": data[i].email_status,
                "subject": data[i].email_subject,
                "email_to": data[i].email_to,
                "date_inbox": data[i].date_inbox,
                "time": data[i].time,
            });
        }

        var state = {
            'querySet': Getdata,
            'page': 1,
            'rows': 10, // จำนวน row
            'window': 10000, // จำนวนหน้าที่เเสดง
        }

        buildTable()

        function pagination(querySet, page, rows) {
            var trimStart = (page - 1) * rows
            var trimEnd = trimStart + rows
            var trimmedData = querySet.slice(trimStart, trimEnd)
            var pages = Math.ceil(querySet.length / rows); // Math.ceil ปัดทศนิยมขึ้น Math.round ปัดทศนิยมลง
            return {
                'querySet': trimmedData,
                'pages': pages,
            }
        };

        function pageButtons(pages) {
            // var wrapper = document.getElementById('pagination-wrapper')
            var wrapper = document.querySelector('.pagination')
            wrapper.innerHTML = ``
            var maxLeft = (state.page - Math.floor(state.window / 2))
            var maxRight = (state.page + Math.floor(state.window / 2))

            if (maxLeft < 1) {
                maxLeft = 1
                maxRight = state.window
            }
            if (maxRight > pages) {
                maxLeft = pages - (state.window - 1)
                if (maxLeft < 1) {
                    maxLeft = 1
                }
                maxRight = pages
            }

            // เช็คหน้าเเรก (ปุ่มย้อนกลับ)
            // var num = 1
            if (maxRight > 5) {
                if (state.page > (maxRight / 2)) {
                    if ((state.page + 1) > (maxRight / 2)) {
                        wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                        wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    }
                }
                for (var page = maxLeft; page <= maxRight; page++) {
                    if ((page >= state.page - 2) && (page <= state.page + 2)) {
                        if (page == state.page) {
                            wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                        }
                        else {
                            wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                        }
                    }
                }
                if ((state.page) <= (maxRight / 2)) {
                    mp = maxRight - 1;
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                    wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
                }
            }
            else {
                for (var page = maxLeft; page <= maxRight; page++) {
                    if (state.page == page) {
                        wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                    } else {
                        wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                    }
                }
            }

            if (state.page == 1) {
                wrapper.innerHTML = `<li  class="page-item disabled"><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            } else {
                wrapper.innerHTML = `<li value=${state.page - 1} class="page page-item "><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
            }


            // เช็คหน้าสุดท้าย (ปุ่มถัดไป)
            if (state.page == pages) {
                wrapper.innerHTML += `<li  class=" page-item disabled"><button class="page-link">ถัดไป</button></li>`
            } else {
                wrapper.innerHTML += `<li value=${state.page + 1} class="page page-item"><button class="page-link">ถัดไป</button></li>`
            }


            $('.page').on('click', function () {
                $('#table-body').empty()
                $('#table-thead').empty()
                $('#table_sortable').remove() // ลบ เพื่อไม่ให้มันสรา้ง row ใน table เปล่าขึ้นมา
                state.page = Number($(this).val())
                buildTable()
            })
        };


        function buildTable() {
            var content = '<table class="tablesaw no-wrap table-striped table-bordered table-hover table" id="table_sortable" data-tablesaw-sortable>'
            content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
            content += '</table>'
            $('#table_Data_Email_consent').append(content);
            // $('#table_sortable').attr('data-tablesaw-sortable');
            var table = $('#table-body');
            var table_thead = $('#table-thead');
            var thead =
                `<tr>
                    <th data-tablesaw-sortable-col>ลำดับ</th>
                    <th data-tablesaw-sortable-col>อีเมล</th>
                    <th data-tablesaw-sortable-col>หัวเรื่องอีเมล</th>
                    <th data-tablesaw-sortable-col>วันที่จัดส่ง</th>
                    <th data-tablesaw-sortable-col>สถานะ</th>
                    <th data-tablesaw-sortable-col>ส่งใหม่</th>
                    </tr>`;
            table_thead.append(thead)


            var data = pagination(state.querySet, state.page, state.rows)
            var myList = data.querySet
            var show = [];


            for (var i in myList) {
                var text_status = "";
                if (myList[i].status == 1) {
                    text_status = ' <span class="badge bg-success">เรียบร้อย</span>'
                } else if (myList[i].status == 0) {
                    text_status = '<span class="badge bg-warning">รอยินยอม</span>'
                } else {
                    text_status = '<span class="badge bg-danger">ส่งไม่ผ่าน</span>'
                }
                var row = '<tr><td>' + myList[i].no +
                    '</td><td>' + myList[i].email_to +
                    '</td><td>' + myList[i].subject +
                    '</td><td>' + myList[i].date_inbox +
                    '</td><td>' + text_status +
                    '</td><td>' + '<a class="text-info"  href="/resend-email/' + myList[i].id_email + '"><i class=" fas fas fa-sync-alt fa-2x"></i></a>' +
                    '</td></tr>'



                table.append(row)
                show.push(myList[i].no)
            }
            document.querySelector("#show").innerHTML = show[0];  //  แสดงถึง row เเรกของหน้า 
            document.querySelector("#to_show").innerHTML = show[show.length - 1];  //  แสดงถึง row สุดท้ายของหน้า
            document.querySelector("#show_all").innerHTML = state.querySet.length;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
            pageButtons(data.pages)
        };

    };
});