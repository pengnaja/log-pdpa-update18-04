
$.ajax({
    type: 'GET',
    url: '/api/get/Tag',
    success: function (result) {
        Tabeldata_ajax(result);
    },
    error: function (e) {
        console.log("error", e);
    }
});

$("#reface").on("click", function (e) { // กรณีกดปุ่ม reface
    $('#Srearch').val(null)
    data_null()
});

$('#Srearch').on("keyup", function () {
    var data = ({ "data": $(this).val() });
    if ($(this).val() == "") {
        data_null()
    } else {
        $.ajax({ // Srearch get api date
            type: "post",
            contentType: "application/json",
            url: '/api/get/Tag_search',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (result) {
                $('#table_sortable').remove();
                $('#table-body').remove();
                Tabeldata_ajax(result);
            },
            error: function (e) {
                $('#table-body').empty()
                $('#table-body').append('<td colspan="10"><div class="col-12"><div class="text-danger" style="text-align: center;min-height: 290px;align-items: center;display: flex;justify-content: center;font-size: larger;"><b>ไม่พบข้อมูล</b></div></div></td>');
            }
        });
    }
});



function data_null() { // กรณีค้นหาไม่เจอข้อมูล
    $.ajax({
        type: 'GET',
        url: '/api/get/Tag',
        success: function (result) {
            $('#table_sortable').remove()
            $('#table-body').remove()
            Tabeldata_ajax(result);
        },
        error: function (e) {
            console.log("error", e);
        }
    });
};

function Tabeldata_ajax(data) {
    var Getdata = [];
    for (var i = 0; i < data.length; i++) {
        Getdata.push({
            "no": (i + 1),
            "id_tag": data[i].id_tag,
            "tag_name": data[i].tag_name,
            "tag_styles": data[i].tag_styles
        });
    }

    var state = {
        'querySet': Getdata,
        'page': 1,
        'rows': 10, // จำนวน row
        'window': 10000, // จำนวนหน้าที่เเสดง
    }

    buildTable()

    function pagination(querySet, page, rows) {
        var trimStart = (page - 1) * rows
        var trimEnd = trimStart + rows
        var trimmedData = querySet.slice(trimStart, trimEnd)
        var pages = Math.ceil(querySet.length / rows); // Math.ceil ปัดทศนิยมขึ้น Math.round ปัดทศนิยมลง
        return {
            'querySet': trimmedData,
            'pages': pages,
        }
    };

    function pageButtons(pages) {
        // var wrapper = document.getElementById('pagination-wrapper')
        var wrapper = document.querySelector('.pagination')
        wrapper.innerHTML = ``
        var maxLeft = (state.page - Math.floor(state.window / 2))
        var maxRight = (state.page + Math.floor(state.window / 2))

        if (maxLeft < 1) {
            maxLeft = 1
            maxRight = state.window
        }
        if (maxRight > pages) {
            maxLeft = pages - (state.window - 1)
            if (maxLeft < 1) {
                maxLeft = 1
            }
            maxRight = pages
        }

        // เช็คหน้าเเรก (ปุ่มย้อนกลับ)
        var num = 1
        if (maxRight > 5) {
            if (state.page > (maxRight / 2)) {
                if ((state.page + 1) > (maxRight / 2)) {
                    wrapper.innerHTML += '<li class="page-item"><button class="page page-link" value=1>1</button></li>';
                    wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                }
            }
            for (var page = maxLeft; page <= maxRight; page++) {
                if ((page >= state.page - 2) && (page <= state.page + 2)) {
                    if (page == state.page) {
                        wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                    }
                    else {
                        wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                    }
                }
            }
            if ((state.page) <= (maxRight / 2)) {
                mp = maxRight - 1;
                wrapper.innerHTML += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">..</a></li>';
                wrapper.innerHTML += '<li class="page-item "><button class="page page-link" value=' + maxRight + '>' + maxRight + '</button></li>';
            }
        }
        else {
            for (var page = maxLeft; page <= maxRight; page++) {
                if (state.page == page) {
                    wrapper.innerHTML += `<li value=${page} class="page page-item active"><button class="page-link">${page}</button></li>`
                } else {
                    wrapper.innerHTML += `<li value=${page} class="page page-item "><button class="page-link">${page}</button></li>`
                }
            }
        }

        if (state.page == 1) {
            wrapper.innerHTML = `<li  class="page-item disabled"><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
        } else {
            wrapper.innerHTML = `<li value=${state.page - 1} class="page page-item "><button class="page-link" tabindex="-1">ย้อนกลับ</button></li>` + wrapper.innerHTML
        }


        // เช็คหน้าสุดท้าย (ปุ่มถัดไป)
        if (state.page == pages) {
            wrapper.innerHTML += `<li  class=" page-item disabled"><button class="page-link">ถัดไป</button></li>`
        } else {
            wrapper.innerHTML += `<li value=${state.page + 1} class="page page-item"><button class="page-link">ถัดไป</button></li>`
        }


        $('.page').on('click', function () {
            $('#table-body').empty()
            $('#table-thead').empty()
            $('#table_sortable').remove() // ลบ เพื่อไม่ให้มันสรา้ง row ใน table เปล่าขึ้นมา
            state.page = Number($(this).val())
            buildTable()
        })
    };


    function buildTable() {
        var content = '<table class="table-striped table-bordered table-hover table" id="table_sortable">'
        content += '<thead id="table-thead"> </thead>' + '<tbody id="table-body"></tbody>';
        content += '</table>'
        $('#table-data-tag').append(content);
        // $('#table_sortable').attr('data-tablesaw-sortable');
        var table = $('#table-body');
        var table_thead = $('#table-thead');
        var thead =
            `<tr>
                <th width="10%">ลำดับ</th>
                <th>ชื่อTag</th>
                <th width="18%">เเก้ไขข้อมูล </th>
                <th width="18%">ลบข้อมูล</th>
                    </tr>`;
        table_thead.append(thead)
        var data = pagination(state.querySet, state.page, state.rows)
        var myList = data.querySet
        var show = [];
        for (var i in myList) {
            var row = '<tr><td class="' + myList[i].id_tag + ' ">' + myList[i].no +
                '</td><td>' + myList[i].tag_name +
                '</td><td>' + ' <a class="text-warning" onclick="edit_tag(this)"data-bs-toggle="modal" data-bs-target="#edit-tag-modal"><i class="fas fa-pencil-alt fa-2x"></i></a>' +
                '</td><td>' + '<a class="text-danger" onclick="delete_Tag(' + myList[i].id_tag + ')"><i class="fas fa-trash-alt fa-2x"></i></a>' +
                '</td></tr>'
            table.append(row);
            show.push(myList[i].no);
        }
        document.querySelector("#show").innerHTML = show[0];  //  แสดงถึง row เเรกของหน้า 
        document.querySelector("#to_show").innerHTML = show[show.length - 1];  //  แสดงถึง row สุดท้ายของหน้า
        document.querySelector("#show_all").innerHTML = state.querySet.length;  //  แสดงถึงจำนวนข้อมูลทั้งหมดของหน้า
        pageButtons(data.pages)
    };
};

tag_class()
function tag_class() {
    $.ajax({
        type: 'GET',
        url: '/api/cookie_management',
        success: function (result) {
            if (result != "ไม่มีข้อมูล") {
                var domain_setting_tag = result.domain_setting_tag;
                var domaingroup = result.domaingroup;
                var data = [];
                for (let i = 0; i < domain_setting_tag.length; i++) {
                    for (let j = 0; j < domaingroup.length; j++) {
                        if (domain_setting_tag[i].id_dg == domaingroup[j].id_dg) {
                            $('#tag_select_domain_' + domaingroup[j].id_dg).empty();
                            $('#badge-group-item' + domaingroup[j].id_dg).addClass("tag_show_" + domain_setting_tag[i].id_tag);
                        }
                    }
                }
                for (let i = 0; i < domain_setting_tag.length; i++) {
                    $('#tag_select_domain_' + domain_setting_tag[i].id_dg).append('<i class="fab fa-font-awesome-alt" style="font-size: 25px;color:' + domain_setting_tag[i].tag_styles + '"></i> ')
                }
            }
        },
        error: function (e) {
            console.log("error", e);
        }
    });
}



function edit_tag(data) {
    $('#id_tag_model').val($(data).closest("tr").find("td").eq(0).attr('class'));
    $('#edit_tag_model').val($(data).closest("tr").find("td").eq(1).text().trim());
    // $('#detail_cp_id').val($(data).closest("tr").find("td").eq(2).text().trim());
}

function Delete(id) {
    // var test = document.querySelector(".domain_" + id);
    Swal.fire({
        title: 'คุณแน่ใจไหม',
        // text: 'test',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#39c449',
        // cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง'
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
                'ลบข้อมูลสำเร็จ',
                '',
                'success'
            )
            timer: 50000,
                window.location = '/delete/' + id;
        }
    })
}

function Edit(data) {
    var text_message = document.getElementById("h6-" + data).innerText; // เอา text ที่อยู่ใน Element ที่เรียกข้อมูลมา
    var text_namedomain = document.getElementById("p-namedomain-" + data).innerText; // เอา text ที่อยู่ใน Element ที่เรียกข้อมูลมา
    var id = document.getElementById("id-" + data).innerText; // เอาะ text ที่อยู่ใน Element ที่เรียกข้อมูลมา
    document.getElementById("domain_edit").value = text_namedomain;
    document.getElementById("message_edit").value = text_message;
    document.getElementById("id_edit").value = id;
}

function tag(id) {
    // console.log("id " + id);
    document.getElementById("id_dg_domain").value = id;

}

function delete_Tag(params) {
    // console.log(params);
    Swal.fire({
        title: 'คุณแน่ใจไหม',
        // text: 'test',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#39c449',
        // cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง'
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
                'ลบข้อมูลสำเร็จ',
                '',
                'success'
            )
            timer: 50000,
                window.location = '/deleteTag/' + params;
        }
    })
}

function tag_selecte(data) {
    var tag = data.split(",");
    var id_t = tag[0];
    var id_d = tag[1];
    // var color = tag[2];
    // console.log(color);
    // var tag_select = document.querySelector('#tag_select_domain_' + id_d);
    // var tag_select_1 = tag_select.getElementsByClassName('fab fa-font-awesome-alt');

    // // var tag_select_2 = tag_select_1.getAttribute("style").replace("font-size: 25px;color:", "")
    // var tes = [];
    // for (let i = 0; i < tag_select_1.length; i++) {
    //     if (color == tag_select_1[i].getAttribute("style").replace("font-size: 25px;color:", "")) {
    //         console.log("เท่า");
    //         console.log(i + 1);
    //         tes.push(tag_select_1[i].getAttribute("style").replace("font-size: 25px;color:", ""))
    //     }
    //     // console.log(tag_select_1[i].getAttribute("style").replace("font-size: 25px;color:", ""));
    // };
    // console.log(tes);

    $.ajax({
        type: 'GET',
        url: '/api/group_domain/' + id_t + '/' + id_d,
        success: function (result) {
            if (result == "success") {
                tag_class()
            } else {
                $('#badge-group-item' + id_d).removeClass("tag_show_" + id_t) // ลบ class ไม่เลือกออก
                $('#tag_select_domain_' + result).empty() // ถ้าไม่ได้เลือกอะไรสักอย่างให่มันว่าง
                tag_class()
            }
        },
        error: function (e) {
            console.log("error" + e);
        }
    });

}

function add_Group(id_group) {
    // var id_tag = id_group[0];
    var id_domain = id_group[1];
    var color = id_group[2];
    var icon = '<i class="fab fa-font-awesome-alt" style="font-size: 25px;color:' + color + '"></i> '
    $('#tag_select_domain_' + id_domain).append(icon);

};

//สร้าง tag
var input = document.querySelector('input[name=tag_dg]');
var tagify = new Tagify(input);
