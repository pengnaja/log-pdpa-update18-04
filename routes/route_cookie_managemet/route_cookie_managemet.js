const express = require('express');
const router = express.Router();

const dialog = require('../../controllers/controller_cookie_managemet/controller_cookie_managemet');

const Tag = require('../../controllers/controller_tag/controller_tag');
router.post('/saveTag', Tag.saveTag);
router.get('/deleteTag/:id', Tag.deleteTag);
router.post('/editTag', Tag.editTag);
router.get('/api/get/Tag', Tag.api_tag);
router.post('/api/get/Tag_search', Tag.api_tag_search);




router.get('/dialogs/:id', dialog.dialogs);
router.get('/home', dialog.home);
router.get('/testdomain', dialog.testdomain);

router.get('/settings_system', dialog.settings_system);
// router.get('/membership_system', dialog.membership_system);
router.get('/monitoring', dialog.monitoring);

router.get('/import_data', dialog.import_data);
router.get('/Protect_mark', dialog.Protect_mark);

router.get('/export_data/search', dialog.export_data);
router.get('/export_data/setup_export', dialog.export_data_setup_export);

router.get('/management/Cookie_Consent', dialog.management_Cookie);
// router.get('/management/Email_Consent', dialog.management_Email);
router.get('/management/Customer_Consent', dialog.management_Customer);


router.get('/report/User_report', dialog.User_report);


router.post('/Savedialogs', dialog.Save);

// router.get('/cookieuser/:token', dialog.cookieuser);


router.get('/api/group_domain/:id/:id_d', dialog.group_domain);
// router.get('/api/tag/cookie_management/:id', dialog.api_tag);
router.get('/api/cookie_management', dialog.api_cookie);


module.exports = router;