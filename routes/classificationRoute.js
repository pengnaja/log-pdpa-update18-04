const express = require('express');
const router = express.Router();

const classificationRoute = require('../controllers/classificationController');
const validation = require('../controllers/validator');

// Index
router.get('/classification',classificationRoute.index);
// Send Ajax
router.post('/classification',classificationRoute.getIndexClassification);
router.post('/classification_pattern',classificationRoute.getIndexPattern);
router.post('/pattern/select-pattern',classificationRoute.selectPattern);
router.post('/event_process',classificationRoute.addEventProcess);
router.post('/event_process/get',classificationRoute.selectEventProcess);
router.post('/users',classificationRoute.addUsers);
router.post('/users/get',classificationRoute.selectUser);
router.post('/event_process/add',classificationRoute.InsertEventProcess);
router.post('/classification/getDel',classificationRoute.getClassification);
// Add Classification
router.post('/classification/add',classificationRoute.addClassification);
// Detail Classification
router.get('/classification/detail:id',classificationRoute.detailClassification);
// Delete Classification
router.post('/classification/delete:id',classificationRoute.deleteClassification);

module.exports = router;