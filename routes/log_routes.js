const express = require('express');
const router = express.Router();
const log_controllers = require('../controllers/log_controllers');
const validator = require('../controllers/validator');


router.get('/log', log_controllers.list);
router.get('/filelog', log_controllers.filelog);
router.get('/filelogcheck/:id', log_controllers.filelog);
router.post('/log/add', log_controllers.add);
router.post('/log/adduser', log_controllers.adduser);
router.get('/log/check/:id', log_controllers.check);
router.get('/log/del/:id', log_controllers.del);
router.get('/log/del/:id', log_controllers.del);
router.get('/log/download/:id', log_controllers.download);
router.post('/log/save/:id', log_controllers.save);
router.get('/exporthistory', log_controllers.exporthistory);
router.get('/access_history', log_controllers.access_history);
router.post('/log/search', log_controllers.search);
router.post('/filter', log_controllers.filter);
module.exports = router;