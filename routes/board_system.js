const express = require('express');
const router = express.Router();
const board_system_controllers = require('../controllers/board_system_controllers');

const validator = require('../controllers/validator');

router.get('/monitor', board_system_controllers.monitor);

//favorite
router.get('/favorite', board_system_controllers.favorite);

//compair
router.get('/compare', board_system_controllers.compare);


//watchdog
router.get('/watchdog', board_system_controllers.watchdog);

//watchdog
router.get('/datain', board_system_controllers.datain);

//watchdog
router.get('/pagedataout', board_system_controllers.dataout);


router.post('/up', board_system_controllers.up);

router.get('/log_chart_1', board_system_controllers.log_chart_1);

router.get('/data_compare/:id', board_system_controllers.data_compare);


//router.get('/change', account_controllers.pw_change);
module.exports = router;