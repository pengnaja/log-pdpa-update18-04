const express = require('express');
const router = express.Router();

const patternController = require('../controllers/patternController');
const validator = require('../controllers/validator');

// Index
router.get('/pattern',patternController.indexPattern);
router.post('/pattern',patternController.ajaxIndexPattern);
router.post('/policy',patternController.ajaxIndexPolicy);
router.post('/policy/select-policy',patternController.ajaxSelectedPolicy);
router.post('/policy/get',patternController.ajaxAddPolicy);
router.post('/tag',patternController.ajaxAddTag);
router.post('/processing_base',patternController.ajaxAddProcessingBase);
router.post('/pattern/user',patternController.ajaxAddUser);
router.post('/pattern/select-users',patternController.ajaxSelectedUser);
router.post('/pattern/del',patternController.ajaxDeletePattern);
// Add Data
router.post('/pattern/add',patternController.addPattern);
// Detail Page
router.get('/pattern/detail:id',patternController.detailPattern)
// Delete Data
router.post('/pattern/delete:id',patternController.deletePattern);
// Statistics (All)
router.get('/pattern/datatype',patternController.datatypePattern);
// Statistics (Disk)
router.get('/pattern/disk',patternController.diskPattern);
// Statistics (Used)
router.get('/pattern/used',patternController.usedPattern);

module.exports = router;