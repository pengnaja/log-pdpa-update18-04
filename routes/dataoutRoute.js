const express = require('express');
const router = express.Router();
const dataout_controllers = require('../controllers/dataout_controllers');

//const validator = require('../controllers/validator');
//const upload = require('express-fileupload')
//const multer = require('multer');
//const path = require('path');

router.get('/dataoutlist', dataout_controllers.list);
router.post('/dataoutadd', dataout_controllers.add);
router.get('/dataoutdel/:id', dataout_controllers.del);

module.exports = router;