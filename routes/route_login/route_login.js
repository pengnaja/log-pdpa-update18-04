const express = require('express');
const router = express.Router();

const Login = require('../../controllers/controller_login/controller_login');

router.get('/', Login.formlogin);
router.get('/logout', Login.logout);
router.post('/login', Login.login);

module.exports = router;