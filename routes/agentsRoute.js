const express = require('express');
const router = express.Router();

const agentController = require('../controllers/agentsController');
const validator = require('../controllers/validator')

// Manager Page
router.get('/agent_manage',agentController.manager);
router.post('/agent_manage/add',agentController.addManage);
//Agent Client 2
router.get('/file_log_ag',agentController.file_log_ag);
// SEND AJAX
router.post('/agent_manage',agentController.manager1);
router.post('/agent_manage/select',agentController.selectAgentType);
router.post('/file_log_ag',agentController.file_log_ag1);
router.post('/file_log_ag/detail',agentController.file_log_ag1_detail);
router.post('/database_ag',agentController.database_ag1);
router.post('/logger_ag',agentController.logger_ag1);
//Agent Client 3
router.get('/database_ag',agentController.database_ag);
// Delete Record
router.post('/database_ag/delete',agentController.delDatabase_ag)
//Agent Client 1
router.get('/logger_ag',agentController.logger_ag);
// Option
router.get('/hardware_ag',agentController.hardware_ag);

module.exports = router