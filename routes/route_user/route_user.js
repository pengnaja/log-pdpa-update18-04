const express = require('express');
const router = express.Router();

const user = require('../../controllers/controller_user/controller_user');

router.get('/membership_system', user.membership_system);

module.exports = router;