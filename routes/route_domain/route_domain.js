const express = require('express');
const router = express.Router();

const domain = require('../../controllers/controller_domain/controller_domain');
// const Tag = require('../../controllers/controller_tag/controller_tag');

router.get('/delete/:id', domain.delete);
router.post('/adddomain', domain.adddomain);
router.post('/edit-domain', domain.edit);
module.exports = router;