from datetime import datetime, timedelta
from tabnanny import check
import mysql.connector
import subprocess as sub
import time
import hashlib


while True:
    time1 = (datetime.now() - timedelta(hours=1)).strftime("%H:00,%Y-%m-%d")
    time2 = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%d")
    # check device input with file log
    a = 'ls /var/log/alltra|grep' + ' ".log"'
    p = sub.Popen((a,), stdout=sub.PIPE, shell=True)
    for row in iter(p.stdout.readline, b''):
        mydb = mysql.connector.connect(
            host="127.0.0.1",
            user="alltra",
            passwd="rHCBGz4BnmYw@",
            database="alltra"
        )
        time3 = (datetime.now()).strftime("%Y-%m-%d %H:%M:%S")
        # x is file
        x = row.rstrip()
        z = str(x).split(',')
        # y is IP Adresse
        y = z[0].replace("b'", "")
        f = str(x).split('b')
        mvf = f[1]
        print(time1)

        if x:
            with open(x, "rb") as f:
                bytes = f.read()
                hash = hashlib.sha256(bytes).hexdigest()
                print(hash)
                msg = hash.encode('ascii')

            mycursor = mydb.cursor()
            # check input
            sql1 = "SELECT input_ip  FROM input WHERE input_ip = %s"
            val1 = (y)
            mycursor.execute(sql1, (val1,))
            input_ip = mycursor.fetchall()
            mydb.commit()
            print("input_ip:", input_ip)

            if len(input_ip) == 0:
                # insert input to device
                sql = "INSERT INTO `input` (`input_id`, `input_ip`, `date`,`hostname`,`type`) VALUES (NULL, %s, %s,%s,'upd')"
                mycursor.execute(sql, (y, time3, y))
                mydb.commit()

            sql2 = "SELECT device_id FROM `device` WHERE de_ip = %s"
            mycursor.execute(sql2, (y,))
            device_id = mycursor.fetchall()
            mydb.commit()
            print("device_id",len(device_id))

            #check file readed
            if len(device_id) == 1:
                sql2 = "SELECT `file_id` FROM `file` WHERE `name` = %s AND `file_id` NOT IN (SELECT `file_id` FROM `readed` )"
                val1 = (x)
                mycursor.execute(sql2, (x,))
                checkfile = mycursor.fetchall()
                mydb.commit()
                print("checkfile_id",checkfile)

                #Insert log and file readed
                if len(checkfile) == 0:
                    sql = "INSERT INTO `file` (`file_id`, `date`, `name`,`hash`,`device_id`,`size`) VALUES (NULL, %s, %s,%s,%s,'0')"
                    val1 = (time3)
                    val2 = (x)
                    val3 = (msg)
                    val4 = str(device_id[0][0])

                    mycursor.execute(sql, (time3, x, msg,val4))
                    mydb.commit()
                    print("insert file to DB ")

                    print("reading file")
                    file1 = open(x, 'r',encoding='utf-8',errors='ignore')
                    count = 0
                    while True:
                        count += 1
                        line = file1.readline()
                        if not line:
                            break
                        readline = str(
                            "{}".format(line.strip()))

                        sql = "INSERT INTO `log` (`log_id`, `msg`, `date`, `device_id`,`file_name`) VALUES (NULL, %s,%s,%s,%s)"
                        val1 = (readline)
                        val2 = time3
                        val3 = str(device_id[0][0])
                        mycursor.execute(sql, (val1, val2, val3, x))
                        mydb.commit()
                    file1.close()
                    print("read file successfully")
                    
                    sql1 = "SELECT `file_id` FROM `file` WHERE `name` = %s"
                    mycursor.execute(sql1, (x,))
                    file_id = mycursor.fetchall()
                    mydb.commit()
                    print("file_id",file_id)
                   
                    sql2 = "INSERT INTO `readed` (`readed_id`, `file_id`,`classify`,`check_size`) VALUES (NULL, %s,%s,'0');"
                    val1 = (str(file_id[0][0]))
                    mycursor.execute(sql2, (val1,"0"))
                    mydb.commit()
                    print("readed")
                    b = 'mv '+mvf+' /var/log/alltra/readed'
                    p = sub.Popen((b,), stdout=sub.PIPE, shell=True)  
                    print("file name: ",mvf)

                    # classify
                    # if(p):
                    #     sql = "SELECT * FROM `readed` JOIN file ON `file`.`file_id`= `readed`.`file_id` WHERE `readed`.`classify` = 0"
                    #     mycursor.execute(sql,)
                    #     myresult = mycursor.fetchall()
                    #     mydb.commit()
                        
                    #     for j in (myresult):
                    #         readed_id = j[0]
                    #         if j :
                    #             filename = j[5]
                    #             # print(i)
                    #             sql = "SELECT * FROM `log` WHERE `file_name` = %s ORDER BY `log`.`date` ASC"
                    #             mycursor.execute(sql,(filename,))
                    #             result = mycursor.fetchall()
                    #             mydb.commit()
                    #             for i in (result):
                    #                 count += 1
                    #                 index1 = (i[1]).find('"GET')
                    #                 index2 = (i[1]).find('"POST')
                    #                 index3 = (i[1]).find('"OPTIONS')
                    #                 index4 = (i[1]).find('TCP_')
                    #                 index5 = (i[1]).find('firewall')
                    #                 index6 = (i[1]).find('NAT')
                    #                 index7 = (i[1]).find('CONNECT')
                    #                 index8 = (i[1]).find('TAG_')

                    #                 result = i[1].split(' ') #mgs
                    #                 device_id = i[3]
                    #                 if(index1 > -1 or  index2 > -1 or index3 > -1 ):#Web Server
                    #                     source = result[1] #source IP
                    #                     date = result[4]+" "+result[5]
                    #                     result2 = i[1].split('"')
                    #                     destination = result2[3] #destination
                    #                     service = result2[1] #service
                    #                     port = '-'
                    #                     mgs = '-'
                    #                     if(result2[3]=='-'):
                    #                         destination = result2[5]

                    #                 elif(index4 > -1): # Proxy Server
                    #                     result3 = i[1].split('TCP_')
                    #                     r = result3[0].split(' ')
                    #                     x =len(r)-2
                    #                     source = r[x] #source IP
                    #                     result4 = i[1].split(source)
                    #                     result4 = result4[1].split(' ')
                    #                     destination = result4[4] #destination
                    #                     cp = destination.find('http')
                    #                     if(cp > -1):
                    #                         destination = result4[4]
                    #                     else:
                    #                         destination = destination.split(':')
                    #                         destination = destination[0]
                    #                     service = '-'
                    #                     if(index7 > -1):
                    #                         port = i[1].split('CONNECT')
                    #                         port = port[1].split(':')
                    #                         port = port[1].split(' ')
                    #                         port = port[0]
                    #                     else:
                    #                         port = '-'
                    #                     mgs = '-'
                    #                     # date = result[5]+" "+result4[1]+" "+result4[3]+" "+result4[4]+result4[5]+result4[6]
                    #                     date = result[5]
                    #                     # print("result: ",date,result)

                    #                 elif(index8 > -1): # Proxy Server
                    #                     result3 = i[1].split('TAG_')
                    #                     r = result3[0].split(' ')
                    #                     x =len(r)-2
                    #                     source = r[x] #source IP
                    #                     result4 = i[1].split(source)
                    #                     result4 = result4[1].split(' ')
                    #                     destination = result4[4] #destination
                    #                     destination = destination.split(':')
                    #                     destination = destination[0]
                    #                     service = '-'
                    #                     if(index7 > -1):
                    #                         port = i[1].split('CONNECT')
                    #                         port = port[1].split(':')
                    #                         port = port[1].split(' ')
                    #                         port = port[0]
                    #                     else:
                    #                         port = '-'
                    #                     mgs = '-'
                    #                     # date = result[5]+" "+result4[1]+" "+result4[3]+" "+result4[4]+result4[5]+result4[6]
                    #                     date = result[5]
                        
                    #                 elif(index5 > -1):
                    #                     ip = result[11].split('->')
                    #                     port = i[1].split('->')
                    #                     port = port[1].split(':')
                    #                     port = port[1].split(',')
                    #                     port = port[0]
                    #                     source = ip[0].split(':')
                    #                     source = source[0] #source IP
                    #                     destination = ip[1].split(':')
                    #                     destination = destination[0]
                    #                     service = '-'
                    #                     mgs = '-'
                    #                     result2 = i[1].split('from')
                    #                     r = result2[0]
                    #                     r = r.split(' ')
                    #                     x =len(r)-2
                    #                     y =len(r)-3
                    #                     date = r[y]+" "+r[x]
                    #                     if(index6 > -1):
                    #                         service =  result[12]+result[13]
                                    
                    #                 else:
                    #                     mgs = i
                    #                     date = '-'
                    #                     source = '-'
                    #                     destination = '-'
                    #                     service = '-'
                    #                     port = '-'
                    #                 # print(i)
                    #                 # print("count: ",count,device_id,date,source,destination,service,port,mgs)
                    #                 sql = "UPDATE `readed` SET `classify` = %s WHERE `readed`.`readed_id` = %s"
                    #                 mycursor.execute(sql,('1',readed_id,))
                    #                 mydb.commit()

                    #                 sql = "INSERT INTO `views` (`views_id`, `timestamp`, `source`, `destination`, `route`, `packet`, `time`, `protocol`, `other`, `msg`, `device_id`,`date`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s);"
                    #                 mycursor.execute(sql,(date,source,destination,'-','-','-',port,'-',mgs,device_id,time3))
                    #                 mydb.commit()
                    #             print("classify")
                       
        time.sleep(1)
