#!/bin/bash

read -p "NamePort: " port
read -p "IP: " ip
read -p "SubMark (ex: 24): " sub_net
read -p "Gateway: " gateway

`sudo rm -rf /etc/netplan/00-installer-config.yaml`
path="/var/log/sniff/00-installer-config.yaml"
`echo "# This is the network config written by 'subiquity'" >> $path`
`echo "network:" >> $path`
`echo "   ethernets:" >> $path`
`echo "      ${port}:" >> $path`
`echo "         addresses: " >> $path`
`echo "         - ${ip}/${sub_net}" >> $path`
`echo "         gateway4: ${gateway}" >> $path`
`echo "         nameservers:" >> $path`
`echo "            addresses: " >> $path`
`echo "            - 8.8.8.8" >> $path`
`echo "   version: 2" >> $path`

`sudo chmod 644 $path`
`sudo mv $path /etc/netplan/00-installer-config.yaml`
`sudo netplan apply`

echo "Success...!!"
