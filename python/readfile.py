from datetime import datetime, timedelta
import mysql.connector
import subprocess as sub
import time
import hashlib
mydb = mysql.connector.connect(
    host="127.0.0.1",
    user="sniff",
    passwd="P@ssw0rd",
    database="sniff"
)
mycursor = mydb.cursor()
while True:
    time1 = (datetime.now() - timedelta(hours=1)).strftime("%H:00,")
    time2 = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%d")
    time3 = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%d %H:%M:%S")
    a = 'ls /var/log/sniff|grep' + ' ".log"'
    p = sub.Popen((a,), stdout=sub.PIPE, shell=True)
    for row in iter(p.stdout.readline, b''):
        time3 = (datetime.now()).strftime("%Y-%m-%d %H:%M:%S")
        x = row.rstrip()
        y = str(x).split(',')
        y = y[0].replace("b'", "")
        print("IP ", y)
        print("XX ", x)
        sql1 = "SELECT device_id FROM `device` WHERE de_ip = %s"
        val1 = (y)
        mycursor.execute(sql1, (val1,))
        myresult = mycursor.fetchall()
        mydb.commit()
        print("device_id", myresult)
        if len(myresult) > 0:
            sql1 = "SELECT file_id FROM file WHERE name = %s AND file_id NOT IN (SELECT file_id FROM readed )"
            val1 = (x)
            mycursor.execute(sql1, (val1,))
            myresult1 = mycursor.fetchall()
            mydb.commit()
            print("check file", myresult1)
            if len(myresult1) > 0:
                print("reading file")
                file1 = open(x, 'r')
                count = 0
                while True:
                    count += 1
                    line = file1.readline()
                    if not line:
                        break
                    readline = str("Line{}: {}".format(count, line.strip()))
                    sql = "INSERT INTO `log` (`log_id`, `msg`, `date`, `device_id`,`file_name`) VALUES (NULL, %s,%s,%s,%s)"
                    val1 = (readline)
                    val2 = time3
                    val3 = str(myresult[0][0])
                    mycursor.execute(sql, (val1, val2, val3, x))
                    mydb.commit()
                file1.close()
                sql = "INSERT INTO `readed` (`readed_id`, `file_id`) VALUES (NULL, %s);"
                val1 = (str(myresult1[0][0]))
                mycursor.execute(sql, (val1,))
                mydb.commit()
            time.sleep(2)
        time.sleep(1)
