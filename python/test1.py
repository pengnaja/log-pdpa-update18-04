from datetime import datetime, timedelta
import mysql.connector
import subprocess as sub
import time
import hashlib

while True:
    time1 = (datetime.now() - timedelta(hours=1)).strftime("%H:00,")
    time2 = (datetime.now() - timedelta(hours=1)).strftime("%Y-%m-%d")
    #a = 'ls /var/log/sniff|grep' +' "'+time1+'\|'+time2+'"'
    a = 'ls /var/log/sniff|grep' + ' ".log"'
    p = sub.Popen((a,), stdout=sub.PIPE, shell=True)
#    print (a)
#    p = sub.run("ls /var/log/test", shell=True, check=True)
    for row in iter(p.stdout.readline, b''):
        mydb = mysql.connector.connect(
    host="127.0.0.1",
    user="sniff",
    passwd="P@ssw0rd",
    database="sniff"
)
        time3 = (datetime.now()).strftime("%Y-%m-%d %H:%M:%S")
        x = row.rstrip()
        y = str(x).split(',')
        y = y[0].replace("b'", "")
        print(x)
        if x:
            with open(x, "rb") as f:
                bytes = f.read()
                hash = hashlib.sha256(bytes).hexdigest()
                print(hash)
                msg = hash.encode('ascii')

            mycursor = mydb.cursor()
            sql1 = "SELECT input_ip  FROM input WHERE input_ip = %s"
            val1 = (y)
            mycursor.execute(sql1, (val1,))
            myresult1 = mycursor.fetchall()

            sql2 = "SELECT name FROM file WHERE name = %s"
            val2 = (x)
            mycursor.execute(sql2, (val2,))
            myresult2 = mycursor.fetchall()
            mydb.commit()

            if len(myresult1) == 0:
                sql = "INSERT INTO `input` (`input_id`, `input_ip`, `date`,`hostname`,`type`) VALUES (NULL, %s, %s,%s,'upd')"
                val1 = (time3)
                val2 = (y)
                #val3 = (msg)
                mycursor.execute(sql, (val2, val1, val2))

            if len(myresult2) == 0:
                sql = "INSERT INTO `file` (`file_id`, `date`, `name`,`hash`) VALUES (NULL, %s, %s,%s)"
                val1 = (time3)
                val2 = (x)
                val3 = (msg)
                mycursor.execute(sql, (val1, val2, val3))

        time.sleep(1)
