import mysql.connector
import re

web_sessionArr = []

mydb = mysql.connector.connect(
    host="127.0.0.1",
    user="sniff",
    passwd="P@ssw0rd",
    database="sniff"
)

mycursor = mydb.cursor()
sql = "SELECT * FROM `log` LIMIT 10"
mycursor.execute(sql)
myresult = mycursor.fetchall()
mydb.commit()

web_pattern ={'date': '', 'source': '', 'duration': '','un': '', 'client_address': '', 'protocol': '', 'status': '', 'bytes': '', 'request_mothod': '', 'url': '', 'dest_port': '', 'user': '', 'hierarchy_code': '', 'dest': '', 'http_content_type': ''}
regex = r"(?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?P<rfc931>[^ ]+) (?P<authuser>[^ ]+) \[(?P<http_timestamp>[^\[\]]+)\] \"(?P<request_method>[^ ]+) (?P<request_path>.*) (?P<request_version>.*)\" (?P<http_status>[^ ]+) (?P<http_bytes>[^ ]+) \"(?P<http_referer>.+)\" \"(?P<http_user_agent>.+)\""
proxy_pattern1 = r"(?P<date>\w+ \w+ \d+:\d+:\d+) (?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?P<duration>\d+.\d+)\s+(?P<un>\d+) (?P<client_address>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?P<protocol>TCP|UDP|NONE)_\w+[\/]+(?P<status>\d+)\s(?P<bytes>\d+)\s(?P<request_mothod>\w+) (?P<url>[^\s:]+):(?P<dest_port>[^\s]+) (?P<hierarchy_code>[^\s]) (?P<dest>[^\s]+) (?P<http_content_type>.*)"
proxy_pattern2 = r"(?P<date>\w+ \w+ \d+:\d+:\d+) (?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?P<duration>\d+.\d+)\s+(?P<un>\d+) (?P<client_address>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?P<protocol>TCP|UDP|NONE)_\w+[\/]+(?P<status>\d+)\s(?P<bytes>\d+)\s(?P<request_mothod>\w+) (?P<url>[^\s]+) ?(?P<dest_port>\d+)?\s+(?P<hierarchy_code>[^\s]) (?P<dest>[^\s]+) (?P<http_content_type>.*)"
for x in myresult:
    txt = str(x[1])
    # print(x[1],(txt).find('CONNECT'))
    
    if (txt.find('CONNECT') == -1):

        matches = re.finditer(proxy_pattern2, txt, re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1):
            web_pattern['date'] = match.groups()[0]
            web_pattern['source'] = match.groups()[1]
            web_pattern['duration'] = match.groups()[2]
            web_pattern['un'] = match.groups()[3]
            web_pattern['client_address'] = match.groups()[4]
            web_pattern['protocol'] = match.groups()[5]
            web_pattern['status'] = match.groups()[6]
            web_pattern['reqbytesuest_mothod'] = match.groups()[7]
            web_pattern['request_mothod'] = match.groups()[8]
            web_pattern['url'] = match.groups()[9]
            web_pattern['port'] = '-'
            web_pattern['user'] = match.groups()[10]
            web_pattern['hierarchy_code'] = match.groups()[11]
            web_pattern['dest'] = match.groups()[12]
            web_pattern['http_content_type'] = match.groups()[13]
            web_sessionArr.append(web_pattern)

            for groupNum in range(0, len(match.groups())):
                groupNum = groupNum + 1
                group = match.group(groupNum)
            # print (web_sessionArr)        
    elif(txt.find('CONNECT') > -1):    
        matches = re.finditer(proxy_pattern1, txt, re.MULTILINE)
        for matchNum, match in enumerate(matches, start=1):
            web_pattern['date'] = match.groups()[0]
            web_pattern['source'] = match.groups()[1]
            web_pattern['duration'] = match.groups()[2]
            web_pattern['un'] = match.groups()[3]
            web_pattern['client_address'] = match.groups()[4]
            web_pattern['protocol'] = match.groups()[5]
            web_pattern['status'] = match.groups()[6]
            web_pattern['reqbytesuest_mothod'] = match.groups()[7]
            web_pattern['request_mothod'] = match.groups()[8]
            web_pattern['url'] = match.groups()[9]
            web_pattern['dest_port'] = match.groups()[10]
            web_pattern['user'] = match.groups()[11]
            web_pattern['hierarchy_code'] = match.groups()[12]
            web_pattern['dest'] = match.groups()[13]
            # web_pattern['http_content_type'] = match.groups()[14]
            web_sessionArr.append(web_pattern)

            for groupNum in range(0, len(match.groups())):
                groupNum = groupNum + 1
                group = match.group(groupNum)
    print (web_sessionArr)
    
