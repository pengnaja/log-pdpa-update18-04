import re


proxy_pattern1 ='(?<date>\w+ \w+ \d+:\d+:\d+) (?<s>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?<duration>\d+.\d+)\s+(\d+) (?<client_address>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?<protocol>TCP|UDP|NONE)_\w+[\/]+(?<status>\d+)\s(?<bytes>\d+)\s(?<request_mothod>\w+) (?<url>[^\s:]+):(?<port>[^\s]+) (?<hierarchy_code>[^\s]) (?<dest>[^\s]+) (?<http_content_type>.*)'
proxy_pattern2 = '(?<date>\w+ \w+ \d+:\d+:\d+) (?<s>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?<duration>\d+.\d+)\s+(\d+) (?<client_address>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?<protocol>TCP|UDP|NONE)_\w+[\/]+(?<status>\d+)\s(?<bytes>\d+)\s(?<request_mothod>\w+) (?<url>[^\s]+) ?(?<dest_port>\d+)?\s+(?<user>[^\s]+)\s+(?<hierarchy_code>[^]+)(?<dest>[^\s]+)\s+(?<http_content_type>.*)'
web_sessionArr = []
web_pattern ={'source': '', 'rfc931': '', 'authuser': '', 'http_timestamp': '', 'request_method': '', 'request_path': '', 'request_version': '', 'http_status': '', 'http_bytes': '', 'http_referer': '', 'http_user_agent': ''}
regex = r"(?P<source>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) (?P<rfc931>[^ ]+) (?P<authuser>[^ ]+) \[(?P<http_timestamp>[^\[\]]+)\] \"(?P<request_method>[^ ]+) (?P<request_path>.*) (?P<request_version>.*)\" (?P<http_status>[^ ]+) (?P<http_bytes>[^ ]+) \"(?P<http_referer>.+)\" \"(?P<http_user_agent>.+)\""

test_str = ("122.154.225.207 - - [25/Mar/2022:09:20:42 +0700] \"GET /wp-content/plugins/wordpress-seo/js/dist/edit-page.js?ver=35b571d98a2c55ffb3a1e3a42cc84dff HTTP/1.1\" 200 473 \"https://luckykings168.com/wp-admin/edit.php\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:20:42 +0700] \"GET /wp-content/plugins/wordpress-seo/images/readability-icon.svg HTTP/1.1\" 200 967 \"https://luckykings168.com/wp-content/plugins/wordpress-seo/css/dist/admin-global-1841.css\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:20:42 +0700] \"GET /wp-content/plugins/wordpress-seo/images/link-in-icon.svg HTTP/1.1\" 200 964 \"https://luckykings168.com/wp-content/plugins/wordpress-seo/css/dist/admin-global-1841.css\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"Mar 18 00:10:17 172.16.20.221 1647537017.490     17 172.16.10.81 TCP_MISS/200 634 GET http://www.msftconnecttest.com/connecttest.txt - HIER_DIRECT/13.107.4.52 text/plain"
  "122.154.225.207 - - [25/Mar/2022:09:20:42 +0700] \"GET /wp-content/plugins/wordpress-seo/images/Yoast_SEO_negative_icon.svg HTTP/1.1\" 200 1080 \"https://luckykings168.com/wp-content/plugins/wordpress-seo/css/dist/admin-global-1841.css\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:20:42 +0700] \"GET /wp-content/plugins/wordpress-seo/images/link-out-icon.svg HTTP/1.1\" 200 969 \"https://luckykings168.com/wp-content/plugins/wordpress-seo/css/dist/admin-global-1841.css\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:20:42 +0700] \"POST /wp-admin/admin-ajax.php HTTP/1.1\" 200 773 \"https://luckykings168.com/wp-admin/edit.php\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:20:56 +0700] \"GET /wp-admin/admin.php?action=duplicate_post_clone&post=254&_wpnonce=5d5d33e2e1 HTTP/1.1\" 302 632 \"https://luckykings168.com/wp-admin/edit.php\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:20:57 +0700] \"GET /wp-admin/edit.php?cloned=1&ids=254 HTTP/1.1\" 200 120085 \"https://luckykings168.com/wp-admin/edit.php\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:20:58 +0700] \"POST /wp-admin/admin-ajax.php HTTP/1.1\" 200 773 \"https://luckykings168.com/wp-admin/edit.php\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:21:13 +0700] \"POST /wp-admin/admin-ajax.php HTTP/1.1\" 200 773 \"https://luckykings168.com/wp-admin/edit.php\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:21:28 +0700] \"POST /wp-admin/admin-ajax.php HTTP/1.1\" 200 773 \"https://luckykings168.com/wp-admin/edit.php\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
	"122.154.225.207 - - [25/Mar/2022:09:21:43 +0700] \"POST /wp-admin/admin-ajax.php HTTP/1.1\" 200 773 \"https://luckykings168.com/wp-admin/edit.php\" \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.46\"\n"
  )

matches = re.finditer(regex, test_str, re.MULTILINE)

for matchNum, match in enumerate(matches, start=1):
    web_pattern['source'] = match.groups()[0]
    web_pattern['rfc931'] = match.groups()[1]
    web_pattern['authuser'] = match.groups()[2]
    web_pattern['http_timestamp'] = match.groups()[3]
    web_pattern['request_method'] = match.groups()[4]
    web_pattern['request_path'] = match.groups()[5]
    web_pattern['request_version'] = match.groups()[6]
    web_pattern['http_status'] = match.groups()[7]
    web_pattern['http_bytes'] = match.groups()[8]
    web_pattern['http_referer'] = match.groups()[9]
    web_pattern['http_user_agent'] = match.groups()[10]
    web_sessionArr.append(web_pattern)
    for groupNum in range(0, len(match.groups())):
        groupNum = groupNum + 1
        group = match.group(groupNum)
    print (web_sessionArr)
        # print(group)
        # print ("Group {groupNum} found at {start}-{end}: {group}".format(groupNum = groupNum, start = match.start(groupNum), end = match.end(groupNum), group = match.group(groupNum)))
