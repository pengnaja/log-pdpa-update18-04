const controller = {};
const { json } = require('body-parser');
const { validationResult } = require('express-validator');
const path = require('path');
const uuidv4 = require('uuid/v4');

function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())
    date = formatted_date + ' ' + formatted_time;
    return date;
}
controller.list = (req, res) => {
    const data = req.body;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `device` WHERE device_id NOT IN (SELECT device_id FROM `del_device`)', (err, device_list) => {
                conn.query('SELECT date_format(history.datetime,"%b %d %H:%m:%S")as date,history.msg,account.name FROM `history` JOIN account ON account.acc_id=history.acc_id ORDER BY history.datetime DESC', (err, history) => {
                    req.session.history = history;
                    if (err) {
                        res.json(err);
                    }

                    res.render('./device/device_list', {
                        data: device_list,
                        session: req.session

                    });
                });
            });
        });
    }
};
controller.blocklist = (req, res) => {
    const data = req.body;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `blocked`', (err, blocked_list) => {
                conn.query('SELECT date_format(history.datetime,"%b %d %H:%m:%S")as date,history.msg,account.name FROM `history` JOIN account ON account.acc_id=history.acc_id ORDER BY history.datetime DESC', (err, history) => {
                    req.session.history = history;
                    if (err) {
                        res.json(err);
                    }

                    res.render('./device/blocked_list', {
                        data: blocked_list,
                        session: req.session

                    });
                });
            });
        });
    }
};
controller.allow = (req, res) => {
    const data = req.body;
    const { id } = req.params;
    console.log(data);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `blocked` WHERE input_id =? ', [id], (err, allow_list) => {
                conn.query('SELECT date_format(history.datetime,"%b %d %H:%m:%S")as date,history.msg,account.name FROM `history` JOIN account ON account.acc_id=history.acc_id ORDER BY history.datetime DESC', (err, history) => {
                    req.session.history = history;
                    if (err) {
                        res.json(err);
                    }

                    res.render('./device/device_allow', {
                        data: allow_list,
                        session: req.session

                    });
                });
            });
        });
    }
};
controller.input = (req, res) => {
    const data = req.body;

    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT i.input_id,i.input_ip,date_format(i.date,"%Y-%m-%d") as date,i.hostname,i.type FROM input as i WHERE i.input_id NOT IN (SELECT input_id FROM `blocked`) AND i.input_id NOT IN (SELECT input_id FROM `device`)', (err, device_list) => {
                conn.query('SELECT date_format(history.datetime,"%b %d %H:%m:%S")as date,history.msg,account.name FROM `history` JOIN account ON account.acc_id=history.acc_id ORDER BY history.datetime DESC', (err, history) => {
                    req.session.history = history;
                    if (err) {
                        res.json(err);
                    }

                    res.render('./device/device_input', {
                        data: device_list,
                        session: req.session

                    });
                });
            });
        });
    }
};
controller.block = (req, res) => {
    const { id } = req.params;
    const data = req.body;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT `input_id`, `input_ip`,date_format(date,"%Y-%m-%d") as date, `hostname`, `type` FROM `input` WHERE input_id = ?', [id], (err, device_block) => {
                conn.query('SELECT date_format(history.datetime,"%b %d %H:%m:%S")as date,history.msg,account.name FROM `history` JOIN account ON account.acc_id=history.acc_id ORDER BY history.datetime DESC', (err, history) => {
                    req.session.history = history;
                    if (err) {
                        res.json(err);
                    }

                    res.render('./device/device_block', {
                        data: device_block,
                        session: req.session

                    });
                });
            });
        });
    }
};
controller.blocked = (req, res) => {
    const { id } = req.params;
    id2 = req.session.userid
    const data = req.body;
    date2 = addDate();
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO `blocked` (`b_id`, `b_ip`, `hostname`, `type`,`input_id`) VALUES (NULL, ?, ?, ?,?)', [data.input_ip, data.hostname, data.type, data.id], (err, device_blocked) => {
                conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time,DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL ? DAY),"%Y-%m-%d") as mouth2', [data.keep], (err, date) => {
                    var day1 = [date[0].mouth + " " + date[0].time]
                    var day2 = [date[0].mouth2 + " " + date[0].time]
                    conn.query('SELECT * FROM account WHERE acc_id = ?  ', [req.session.userid], (err, account) => {
                        var msg = account[0].name + " ระงับอุปกรณ์ " + data.hostname
                        type = "blocked";
                        conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [id2, date2, msg, type], (err, history_add) => {
                            // console.log(device_blocked);
                            if (err) {
                                res.json(err);
                            }

                            res.redirect('/device/input');
                        });
                    });
                });
            });
        });

    }
};
controller.delblocked = (req, res) => {
    id2 = req.session.userid
    const { id } = req.params;
    const data = req.body;
    date2 = addDate();
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `blocked` WHERE `blocked`.`input_id` = ?;', [id], (err, blocked) => {
                conn.query('DELETE FROM `blocked` WHERE `blocked`.`input_id` = ?;', [id], (err, device_delblocked) => {
                    conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time,DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL ? DAY),"%Y-%m-%d") as mouth2', [data.keep], (err, date) => {
                        var day1 = [date[0].mouth + " " + date[0].time]
                        var day2 = [date[0].mouth2 + " " + date[0].time]
                        conn.query('SELECT * FROM account WHERE acc_id = ?  ', [req.session.userid], (err, account) => {
                            //console.log(account);
                            var msg = [account[0].name + " ยกเลิกการระงับอุปกรณ์ " + blocked[0].hostname]
                            type = "delblocked";
                            conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [id2, date2, msg, type], (err, history_add) => {

                                if (err) {
                                    res.json(err);
                                }

                                res.redirect('/device/blocklist');
                            });
                        });
                    });
                });
            });
        });
    }
};
controller.network = (req, res) => {
    const data = req.body;
    //res.json(data);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO set_network set ?', [data], (err, network_new) => {
                console.log(network_new);
                if (err) {
                    res.json(err);
                }
                res.redirect('index');
            });
        });
    }
};
controller.new = (req, res) => {
    const { id } = req.params;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT `input_id`, `input_ip`,date_format(date,"%Y-%m-%d") as date, `hostname`, `type` FROM `input` WHERE input_id = ?', [id], (err, device_delete) => {
                if (err) {
                    res.json(err);
                }
                res.render('./device/device_new', {
                    data: device_delete,
                    session: req.session
                });
            });
        });
    }
};
controller.add = (req, res) => {
    const data = req.body;
    date = addDate();
    const errors = validationResult(req);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/device/new/' + data.input_id);
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
            req.getConnection((err, conn) => {
                if (req.files) {
                    var filename = req.files.img;
                    if (!filename.map) {
                        var newfilename = uuidv4() + "." + filename.name.split(".")[1];
                        console.log(newfilename);
                        const savePath = path.join(__dirname, '../public/UI/image', newfilename);
                        filename.mv(savePath);
                        console.log(savePath);
                    }
                    data.image = newfilename;
                }
                conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time,DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL ? DAY),"%Y-%m-%d") as mouth2', [data.keep], (err, date) => {
                    var day1 = [date[0].mouth + " " + date[0].time]
                    var day2 = [date[0].mouth2 + " " + date[0].time]
                    conn.query('INSERT INTO device set ?', [data], (err, device_add) => {
                        conn.query('SELECT * FROM account WHERE acc_id = ?  ', [req.session.userid], (err, account) => {
                            //console.log(account);
                            var msg = [account[0].name + " เพิ่มอุปกรณ์ในระบบโดยใช้ชื่อ " + data.name]
                            type = "adddevice"
                            conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [req.session.userid, date, msg, type], (err, history_add) => {
                                res.redirect('/device/input');
                            });
                        });
                    });
                });
            });
        }
    }
};

controller.delete = (req, res) => {
    const { id } = req.params;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM device where device_id = ?', [id], (err, device_delete) => {
                if (err) {
                    res.json(err);
                }
                res.render('./device/device_del', {
                    data: device_delete,
                    session: req.session
                });
            });
        });
    }
};

controller.confirmdelete = (req, res) => {
    const { id } = req.params;

    const errors = validationResult(req);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {

        req.getConnection((err, conn) => {
            conn.query('INSERT INTO `del_device` (`del_id`, `device_id`) VALUES (NULL, ?);', [id], (err, device_confirmdelete) => {

                console.log(device_confirmdelete);
                res.redirect('/device/list');
            });
        });

    }
};

controller.view = (req, res) => {
    const data = req.body;
    const { id } = req.params;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM device WHERE device_id = ?', [id], (err, device) => {
                //conn.query('SELECT u.id,u.fname,u.lname,date_format(u.age,"%Y-%m-%d")as age,u.weight,u.high,u.sex,u.address,u.phone FROM `device` as u where id = ?', [id], (err, device_edit) => {
                // console.log(device);
                res.render('./device/device_view', {
                    // data: device_edit,
                    data: device,
                    session: req.session
                });
            });
            // });
        });
    }
};
controller.edit = (req, res) => {
    const data = req.body;
    const { id } = req.params;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM device WHERE device_id = ?', [id], (err, device) => {
                //conn.query('SELECT u.id,u.fname,u.lname,date_format(u.age,"%Y-%m-%d")as age,u.weight,u.high,u.sex,u.address,u.phone FROM `device` as u where id = ?', [id], (err, device_edit) => {
                // console.log(device);
                res.render('./device/device_edit', {
                    // data: device_edit,
                    data: device,
                    session: req.session
                });
            });
            // });
        });
    }
};
controller.save = (req, res) => {
    const { id } = req.params;
    const data = req.body;
    //res.json(data)
    const errors = validationResult(req);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/device/edit/' + id)
        } else {
            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลสำเร็จ";
            req.getConnection((err, conn) => {
                if (req.files) {
                    var filename = req.files.img;
                    if (!filename.map) {
                        var newfilename = uuidv4() + "." + filename.name.split(".")[1];
                        console.log(newfilename);
                        const savePath = path.join(__dirname, '../public/UI/image', newfilename);
                        filename.mv(savePath);
                        console.log(savePath);
                    }
                    data.image = newfilename;
                }

                conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time,DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL ? DAY),"%Y-%m-%d") as mouth2', [data.keep], (err, date) => {
                    var day1 = [date[0].mouth + " " + date[0].time]
                    var day2 = [date[0].mouth2 + " " + date[0].time]
                    if (data) {
                        data.keep = day2;
                    }
                    console.log(data);
                    //res.json(data);
                    conn.query('UPDATE device SET ? WHERE device_id = ? ', [data, id], (err, device_save) => {
                        console.log(device_save);
                        if (err) {
                            res.json(err);
                        }
                        //console.log(data);
                        res.redirect('/device/list');
                    });
                });
            });
        }
    }

}

module.exports = controller;