const controller = {};
const fs = require('fs');
//const sha256 = require('js-sha256');
const { validationResult } = require('express-validator');
// const { format } = require('path/posix');
// const path = require('path/posix');
function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())
    date = formatted_date + ' ' + formatted_time;
    return date;
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hours = d.getHours(),
        minutes = d.getMinutes(),
        seconds = d.getSeconds();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    if (hours.length < 2)
        hours = '0' + hours;
    if (minutes.length < 2)
        minutes = '0' + minutes;
    if (seconds.length < 2)
        seconds = '0' + seconds;

    var full = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
    return full;
}

// All List
controller.list = (req, res) => {
    const data = [];
    const s_stats = [];
    const e_stats = [];
    const status = [];
    const dir = 'E:\\Web\\log\\blocks';
    const dir_log = 'E:\\Web\\log\\logs';
    const config_file = 'E:\\Web\\log\\shell_script\\config.conf';
    const history_file = 'E:\\Web\\log\\shell_script\\history.conf';
    const path_length = fs.readdirSync(dir).length;
    const log_length = fs.readdirSync(dir_log).length;
    const chain_file = fs.readdirSync(dir)
        // const history_length = fs.readFileSync(history_file).length;
    const config = fs.readFileSync(config_file, 'utf-8').split(/\r?\n/);
    const total_run_chain = config[6].split("=");
    const status_chain = config[7].split("=");
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        var name = [];
        var chain = [];
        var s_format = [];
        var e_format = [];
        var status_format = [];
        for (var i = 1; i <= path_length; i++) {
            data.push(fs.readFileSync(dir + "/block" + i + ".microb", 'utf-8').split("\n"));
            s_stats.push(fs.statSync(dir_log + "/" + i).mtime);
            e_stats.push(fs.statSync(dir + "/block" + i + ".microb", "utf-8").mtime);
        }
        for (var j = 0; j < data.length; j++) {
            if (typeof data[j] != 'undefined') {
                name.push(data[j][0]);
                chain.push(data[j][2]);
                s_format.push(formatDate(s_stats[j]));
                e_format.push(formatDate(e_stats[j]));
            }
        }
        status.push(fs.readFileSync(history_file, 'utf-8').split("\n"));
        for (var k = 0; k < path_length; k++) {
            status_format.push(status[0][k]);
        }
        status_format.pop();
        status_format.push("Inprocess");
        res.render('./block/block_list', {
            path_length: path_length,
            log_length: log_length,
            total_run_chain: total_run_chain[1],
            time_start: s_format,
            time_end: e_format,
            status_chain: status_chain[1],
            status_format: status_format,
            data: data,
            data1: name,
            data2: chain,
            data3: chain_file,
            session: req.session
        });
    }
};

// Select show
controller.select = (req, res) => {
    const { name } = req.params;
    const dir = "E:\\Web\\log\\blocks"
    const statement = fs.readdirSync(dir);
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        var select = [];
        var detail = [];
        for (var i = 0; i < statement.length; i++) {
            if (name == statement[i]) {
                select.push(fs.readFileSync(dir + "/" + name, 'utf-8').split("\n"));
            }
        }
        for (var j = 0; j < select[0].length; j++) {
            detail.push(select[0][j]);
        }
        res.render('./block/block_detail', {
            data: detail,
            session: req.session
        });
    }
};

module.exports = controller;