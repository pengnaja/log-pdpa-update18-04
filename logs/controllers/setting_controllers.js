const controller = {};
const { validationResult } = require('express-validator');
const speakeasy = require('speakeasy')
const qrcode = require('qrcode')

function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())
    date = formatted_date + ' ' + formatted_time;
    return date;
}
// controller.list = (req, res) => {
//     const data = req.body;
//     if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
//         req.getConnection((err, conn) => {
//             conn.query('SELECT * FROM `timezone`', (err, setting_list) => {
//                 if (err) {
//                     res.json(err);
//                 }

//                 res.render('setting_list', {
//                     data: setting_list,
//                     session: req.session

//                 });
//             });
//         });
//     }
// };
controller.new = (req, res) => {
    const data = req.body;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `set_system`ORDER BY sys_id DESC LIMIT 1', (err, setting_list) => {
                conn.query('SELECT * FROM `timezone`', (err, setting) => {
                    conn.query('SELECT * FROM `set_network` ORDER BY nw_id DESC LIMIT 1', (err, set_network) => {
                        if (err) {
                            res.json(err);
                        }

                        res.render('./setting/setting_list', {
                            data: setting_list,
                            data2: setting,
                            data3: set_network,
                            session: req.session

                        });
                    });
                });
            });
        });
    }
};
controller.view = (req, res) => {
    const data = req.body;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `set_system`ORDER BY sys_id DESC LIMIT 1', (err, setting_list) => {
                conn.query('SELECT * FROM `timezone`', (err, setting) => {
                    conn.query('SELECT * FROM `set_network` ORDER BY nw_id DESC LIMIT 1', (err, set_network) => {
                        if (err) {
                            res.json(err);
                        }

                        res.render('./setting/setting_view', {
                            data: setting_list,
                            data2: setting,
                            data3: set_network,
                            session: req.session

                        });
                    });
                });
            });
        });
    }
};
controller.construction = (req, res) => {
    const data = req.body;
    // if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM `set_system`ORDER BY sys_id DESC LIMIT 1', (err, setting_list) => {
            conn.query('SELECT * FROM `timezone`', (err, setting) => {
                conn.query('SELECT * FROM `set_network` ORDER BY nw_id DESC LIMIT 1', (err, set_network) => {
                    if (err) {
                        res.json(err);
                    }

                    res.render('./setting/construction', {
                        data: setting_list,
                        data2: setting,
                        data3: set_network,
                        session: req.session

                    });
                });
            });
        });
    });
    // }
};
controller.change = (req, res) => {
    const data = req.body;
    id = req.session.userid;

    //res.json(data);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `account` WHERE acc_id = ?', [id], [data], (err, account) => {
                console.log(account);
                if (err) {
                    res.json(err);
                }
                res.redirect('./index2');
            });
        });
    }
};
controller.changepw = (req, res) => {
    const data = req.body;
    const id = req.session.userid;

    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            console.log(id, data.password);
            conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time,DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL ? DAY),"%Y-%m-%d") as mouth2', [data.pw_keep], (err, date) => {
                var day1 = [date[0].mouth + " " + date[0].time]
                var day2 = [date[0].mouth2 + " " + date[0].time]
                    // res.json(id, day2, data.password)
                    // console.log(date);
                conn.query('INSERT INTO `pw_change` (`change_id`, `acc_id`, `date`) VALUES (NULL, ?, ?);', [id, day2], (err, pw_change) => {
                    conn.query('UPDATE account SET password=? WHERE acc_id = ? ', [data.password, id], (err, setting_save) => {
                        // res.json(pw_change);
                        console.log(pw_change);
                        if (err) {
                            res.json(err);
                        }
                        res.redirect('/index2');
                        // res.render('index2', {

                        //     session: req.session
                        // });
                    });
                });
            });
        });
    }
};
controller.network = (req, res) => {
    const data = req.body;
    //res.json(data);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO set_network set ?', [data], (err, network_new) => {
                console.log(network_new);
                if (err) {
                    res.json(err);
                }
                res.redirect('./index2');
            });
        });
    }
};
controller.settingadd = (req, res) => {
    const data = req.body;
    const date2 = addDate();
    const errors = validationResult(req);
    id = req.session.userid;
    //res.json(data);

    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/setting');
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
            req.getConnection((err, conn) => {
                conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time,DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL ? DAY),"%Y-%m-%d") as mouth2', [data.pw_keep], (err, date) => {
                    var day1 = [date[0].mouth + " " + date[0].time]
                    var day2 = [date[0].mouth2 + " " + date[0].time]
                    console.log(day1 + day2);
                    type = "setting"
                        // conn.query('SELECT DATE_FORMAT(CURRENT_TIMESTAMP(),"%Y-%m-%d %H:%m:%s") as date', (err, date2) => {
                    conn.query('INSERT INTO set_system (sys_id,datetime,pw_keep,ntp,num_admin,num_user,timezone,day_keep) VALUES (null,?,?,?,?,?,?,?) ', [day1, day2, data.ntp, data.num_admin, data.num_user, data.timezone, data.pw_keep], (err, set_system) => {
                        conn.query('INSERT INTO `set_network` (`nw_id`, `ip`, `netmark`, `gateway`, `dns`) VALUES (NULL, ?, ?, ?, ?);', [data.ip, data.netmark, data.gateway, data.dns], (err, set_network) => {
                            conn.query('INSERT INTO `pw_change` (`change_id`, `acc_id`, `date`) VALUES (NULL, ?, ?);', [id, day2], (err, pw_change) => {
                                conn.query('SELECT * FROM account WHERE acc_id = ?  ', [id], (err, account) => {
                                    var msg = [account[0].name + " Config Number of Admin " + data.num_admin + ",Number of User " + data.num_user + ",Time for Password " + data.pw_keep + " day,NTP Server " + data.ntp]
                                    conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [id, date2, msg, type], (err, history_add) => {

                                        console.log(history_add);
                                        if (err) {
                                            res.json(err);
                                        }
                                        res.redirect('./setting/view');
                                    });
                                });
                            });
                        });
                    });
                    // });
                });
            });
        }
    }
};
controller.add = (req, res) => {
    const data = req.body;
    const errors = validationResult(req);
    // res.json(data);
    id = req.session.userid;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/setting/new');
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
            req.getConnection((err, conn) => {
                conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time,DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL ? DAY),"%Y-%m-%d") as mouth2', [data.pw_keep], (err, date) => {
                    var day1 = [date[0].mouth + " " + date[0].time]
                    var day2 = [date[0].mouth2 + " " + date[0].time]
                    conn.query('INSERT INTO set_system (sys_id,datetime,pw_keep,ntp,num_admin,num_user,timezone) VALUES (null,?,?,?,?,?,?) ', [day1, day2, data.ntp, data.num_admin, data.num_user, data.timezone], (err, setting_add) => {
                        //console.log(setting_add);
                        conn.query('INSERT INTO `pw_change` (`change_id`, `acc_id`, `date`) VALUES (NULL, ?, ?);', [id, day2], (err, pw_change) => {

                            conn.query('SELECT * FROM account WHERE acc_id = ?  ', [id], (err, account) => {
                                var msg = [account[0].name + " Config Number of Admin " + data.num_admin + ",Number of User " + data.num_user + ",Time for Password " + data.pw_keep + " day,NTP Server " + data.ntp]
                                    //console.log(msg);
                                conn.query('INSERT INTO history (`acc_id`, `datetime`, `msg`, `ht_id`) VALUES (?,?,?,null) ', [id, day1, msg], (err, history_add) => {
                                    //console.log(history_add);
                                });
                                conn.query('INSERT INTO set_system (sys_id,datetime,pw_keep,ntp,num_admin,num_user,timezone,day_keep) VALUES (null,?,?,?,?,?,?,?) ', [day1, day2, data.ntp, data.num_admin, data.num_user, data.timezone, data.pw_keep], (err, set_system) => {
                                    conn.query('INSERT INTO `set_network` (`nw_id`, `ip`, `netmark`, `gateway`, `dns`) VALUES (NULL, ?, ?, ?, ?);', [data.ip, data.netmark, data.gateway, data.dns], (err, set_network) => {
                                        res.redirect('/');
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }
    }
};

controller.qr = (req, res) => {
    //const id = req.session.userid
    const data = req.body;
    var val = data.value;
    var id = data.userid;
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        var secret = speakeasy.generateSecret({
            name: id
        })
        qrcode.toDataURL(secret.otpauth_url, function(err, data) {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM `qrcode` WHERE acc_id = ?', [id], (err, check) => {
                    conn.query('SELECT * FROM `account`WHERE acc_id = ? AND password = ?', [id, val], (err, account) => {
                        console.log("rrrrrrrrrrrrrrrrr" + check.length + account.length);
                        if (check.length == 0 && account.length == 1) {
                            console.log("if");
                            conn.query('INSERT INTO `qrcode` (`qr_id`, `acc_id` , `image`, `ascii`) VALUES (NULL, ?, ?, ?);', [id, data, secret.ascii], (err, qrcode) => {
                                conn.query('SELECT * FROM `qrcode` WHERE acc_id= ?', [id], (err, qrcodecheck) => {
                                    console.log(qrcode);
                                    if (err) {
                                        res.json(err);
                                    }
                                    // res.render('./setting/qr', {
                                    //     data1: qrcodecheck,
                                    //     session: req.session
                                    // });
                                    res.send({ data: qrcodecheck, data2: [] })
                                });
                            });
                        } else {
                            console.log("else");
                            conn.query('SELECT qrcode.image FROM `qrcode`JOIN account ON account.acc_id=qrcode.acc_id WHERE account.acc_id =? AND account.password =?', [id, val], (err, qrcodecheck) => {
                                console.log("else" + qrcodecheck.length);
                                if (qrcodecheck.length == 0) {
                                    res.send({ data: [] })
                                } else {
                                    res.send({ data: qrcodecheck })

                                }
                            });
                        }

                    });
                });
            });
        })
    }
};
controller.checkqr = (req, res) => {
    const data = req.body;
    //res.json(data);
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        var verified = speakeasy.totp.verify({
            secret: data.ascii,
            encoding: 'ascii',
            token: data.token
        })
        console.log(verified);
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM set_system  ', [id], (err, setting_delete) => {
                if (err) {
                    res.json(err);
                }
                // res.render('./setting/qr', {
                //     data: setting_delete,
                //     session: req.session
                // });
                res.redirect('/qr');
            });
        });
    }
};
controller.confirmdelete = (req, res) => {
    const { id } = req.params;
    const errors = validationResult(req);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/setting/delete' + id);
        } else {
            req.getConnection((err, conn) => {
                conn.query('DELETE FROM setting WHERE id=?', [id], (err, setting_confirmdelete) => {
                    if (err) {

                        req.session.test = "ไม่สามารถลบได้";
                        req.session.success = false;

                        res.redirect('/setting');
                        return;
                    } else {
                        req.session.success = true;
                        req.session.topic = "ลบข้อมูลสำเร็จ";

                    }
                    console.log(setting_confirmdelete);
                    res.redirect('/setting/');
                });
            });
        }
    }
};

controller.edit = (req, res) => {
    const data = req.body;
    const { id } = req.params;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM setting ', [id], (err, setting) => {
                conn.query('SELECT u.id,u.fname,u.lname,date_format(u.age,"%Y-%m-%d")as age,u.weight,u.high,u.sex,u.address,u.phone FROM `setting` as u where id = ?', [id], (err, setting_edit) => {
                    res.render('setting_edit', {
                        data: setting_edit,
                        data1: setting,
                        session: req.session
                    });
                });
            });
        });
    }
};

controller.save = (req, res) => {
    const { id } = req.params;
    const data = req.body;
    const errors = validationResult(req);

    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/setting/edit/' + id)
        } else {
            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลสำเร็จ";
            req.getConnection((err, conn) => {
                conn.query('UPDATE setting SET ? WHERE id = ? ', [data, id], (err, setting_save) => {
                    if (err) {
                        res.json(err);
                    }
                    console.log(data);
                    res.redirect('/setting/');
                });
            });
        }
    }

}


controller.ajaxcheckuser = (req, res) => {
    const data = req.body;

    var val = data.value;
    var userid = data.userid;

    var token = val;
    console.log(val);
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM account WHERE acc_id = ? ', [userid, val[0]], (err, dataresult) => {
            conn.query('SELECT * FROM `qrcode` WHERE acc_id = ?', [userid], (err, qrcode) => {
                var verified = speakeasy.totp.verify({
                    secret: qrcode[0].ascii,
                    encoding: 'ascii',
                    token: token
                })
                if (err) {
                    res.json(err);
                }
                console.log(verified);
                console.log(dataresult.length);
                if (verified == true && dataresult.length > 0) {
                    res.send({ data: dataresult })

                } else {
                    res.send({ data: [] })

                }
            });
        });
    });
}

module.exports = controller;