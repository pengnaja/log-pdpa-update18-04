const controller = {};
const { validationResult } = require('express-validator');
const path = require('path');
const uuidv4 = require('uuid/v4');

function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())
    date = formatted_date + ' ' + formatted_time;
    return date;
}
controller.profile = (req, res) => {
    const data = req.body;
    id = req.session.userid;
    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM account as a LEFT JOIN muitifactor as m ON m.acc_id=a.acc_id WHERE a.acc_id = ?', [id], (err, account) => {
                conn.query('SELECT * FROM `history`WHERE acc_id = ?', [id], (err, history) => {

                    res.render('./account/profile', {
                        data: account,
                        data2: history,
                        session: req.session
                    });

                });
            });
        });
    }

};
controller.login = (req, res) => {
    const data = req.body;
    req.getConnection((err, conn) => {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM account ', (err, data) => {
                //res.json(data);
                if (data.length > 0) {

                    res.render('login', { session: req.session });
                } else {
                    res.render('./account/wizard', { session: req.session });
                }
            });
        });
    });

};

controller.admin = (req, res) => {
    //const data = req.body;
    id = req.session.userid;
    //console.log(req.session);
    var logperhour = []


    if (typeof req.session.userid == 'undefined') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            req.getConnection((err, conn) => {
                conn.query('SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM account as a LEFT JOIN muitifactor as m ON m.acc_id=a.acc_id WHERE a.acc_id= ?', [id], (err, account) => {
                    conn.query('SELECT COUNT(*) as num,admin FROM account GROUP BY account.admin ORDER BY admin ASC', (err, account2) => {
                        conn.query('SELECT MAX(date_format(log.date,"%Y-%m-%d %H:%m:%S")) as date,status,device.name as name,image,device.de_ip as ip,log.device_id as device_id FROM `log` JOIN device ON log.device_id=device.device_id GROUP BY device.device_id', (err, device_list) => {
                            conn.query('SELECT DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL 0 DAY),"%Y-%m-01") as date', (err, date1) => {
                                conn.query('SELECT DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL 1 DAY),"%Y-%m-%d") as date', (err, date2) => {
                                    conn.query('SELECT day(log.date) as no,COUNT(*) as num,device.device_id,device.name FROM `log` JOIN device ON device.device_id=log.device_id WHERE log.date BETWEEN ? AND ? GROUP BY device_id', [date1[0].date, date2[0].date], (err, countlog) => {
                                        conn.query('SELECT date_format(history.datetime,"%b %d %H:%m:%S")as date,history.type,history.msg,account.name,account.image FROM `history` JOIN account ON account.acc_id=history.acc_id ORDER BY history.datetime DESC', (err, history) => {
                                            conn.query('SELECT date_format(history.datetime,"%b %d %H:%m:%S")as date,history.msg,account.name,history.type,account.image FROM `history` JOIN account ON account.acc_id=history.acc_id WHERE history.acc_id = ? ORDER BY history.datetime DESC', [id], (err, history2) => {
                                                conn.query('SELECT date_format(pw_keep,"%Y-%m-%d %H:%m:%S")as date,ntp,num_admin,num_user,timezone,day_keep FROM `set_system` ORDER BY `set_system`.`sys_id` DESC LIMIT 1', (err, set_system) => {
                                                    conn.query('SELECT date_format(CURDATE(),"%Y-%m-%d") as mouth,CURTIME() as time', (err, date) => {
                                                        conn.query('SELECT date_format(date,"%Y-%m-%d %H:%m:%S")as date FROM `pw_change` WHERE acc_id = ? ORDER BY `pw_change`.`change_id`  DESC', [id], (err, pw_change) => {
                                                            conn.query('SELECT * FROM `device` JOIN log ON log.device_id=device.device_id GROUP BY log.device_id', (err, device) => {
                                                                if (device) {
                                                                    for (var i = 0; i < device.length; i++) {
                                                                        console.log(device[i].device_id, date1[0].date, date2[0].date);

                                                                        conn.query('SELECT hour(log.date) AS hour,COUNT(*) as num,device.name FROM `log` JOIN device on device.device_id=log.device_id WHERE log.device_id= ? AND log.date BETWEEN "2022-02-01" AND "2022-03-25" GROUP BY hour(log.date)', [device[i].device_id, date1[0].date, date2[0].date], (err, logdate) => {
                                                                            console.log(logdate);
                                                                            var lognum = []
                                                                            var hour = []
                                                                            for (var j = 0; j < logdate.length; j++) {
                                                                                lognum.push(logdate[j].num);
                                                                                hour.push(logdate[j].hour);
                                                                            }
                                                                            console.log(logdate[0]);
                                                                            logperhour.push({ name: logdate[0].name, data: lognum, hours: hour });
                                                                        });


                                                                    }
                                                                }

                                                                conn.query('SELECT * FROM `set_network` ORDER BY `set_network`.`nw_id`  DESC LIMIT 1', (err, set_network) => {
                                                                    console.log(logperhour);

                                                                    var day1 = [date[0].mouth + " " + date[0].time]
                                                                    req.session.acc_name = account[0].name;
                                                                    req.session.acc_email = account[0].email;
                                                                    req.session.acc_id = account[0].acc_id;
                                                                    req.session.admin = account[0].admin;
                                                                    req.session.image = account[0].image;
                                                                    req.session.otp_system = account[0].otp_system;

                                                                    req.session.ntp = set_system[0].ntp;
                                                                    req.session.num_admin = set_system[0].num_admin;
                                                                    req.session.num_user = set_system[0].num_user;
                                                                    req.session.timezone = set_system[0].timezone;
                                                                    req.session.day_keep = set_system[0].day_keep;
                                                                    req.session.set_system = set_system;

                                                                    req.session.ip = set_network[0].ip;
                                                                    req.session.netmark = set_network[0].netmark;
                                                                    req.session.gateway = set_network[0].gateway;
                                                                    req.session.dns = set_network[0].dns;
                                                                    req.session.set_network = set_network;
                                                                    req.session.logperhour = logperhour;

                                                                    if (account[0].admin == 1) {
                                                                        req.session.history = history2;
                                                                    } else {
                                                                        req.session.history = history;
                                                                    }
                                                                    // console.log("data" + account);
                                                                    // console.log("data2" + device_list);
                                                                    // console.log("data3" + countlog);

                                                                    if (pw_change.length > 0) {
                                                                        if (day1[0] > pw_change[0].date) {

                                                                            res.render('./setting/change', {
                                                                                data: account,
                                                                                data2: device_list,
                                                                                data3: countlog,
                                                                                session: req.session
                                                                            });

                                                                        }
                                                                    }
                                                                    if (account2.length > 1) {
                                                                        req.session.acc_admin = account2[1].num;
                                                                        req.session.acc_user = account2[0].num;
                                                                    } else if (account2.length == 1) {
                                                                        req.session.acc_admin = account2[0].num;
                                                                        req.session.acc_user = "0";
                                                                    }
                                                                    if (err) {
                                                                        res.json(err);
                                                                    }
                                                                    res.render('index2', {
                                                                        data: account,
                                                                        data2: device_list,
                                                                        data3: countlog,
                                                                        session: req.session
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }
};
controller.check = (req, res) => {
    const data = req.body;
    const date = addDate();
    //console.log(date);
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM account WHERE username = ? AND password = ? AND  acc_id NOT IN (SELECT acc_id FROM `del_acc`)', [data.username, data.password], (err, data) => {
            conn.query('SELECT * FROM `timezone`', (err, data3) => {
                conn.query('SELECT * FROM history ', (err, data2) => {
                    // conn.query('SELECT DATE_FORMAT(CURRENT_TIMESTAMP(),"%Y-%m-%d %H:%m:%s") as date', (err, date) => {
                    conn.query('SELECT MAX(date_format(log.date,"%Y-%m-%d %H:%m:%S")) as date,device.device_id,de_type,device.name as name,device.de_ip as ip,log.device_id as device_id FROM `log` JOIN device ON log.device_id=device.device_id GROUP BY device.device_id', (err, device_list) => {
                        if (data.length == 0) {
                            res.redirect('/');
                        } else {
                            msg = data[0].name + ' เข้าใช้งานเมื่อ ' + date;
                            type = "login"
                            conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [data[0].acc_id, date, msg, type], (err, history) => {

                                if (data.length == 0) {
                                    res.redirect('/');
                                    console.log("if1");
                                } else {
                                    if (data2.length > 0) {
                                        console.log("if2");
                                        req.session.userid = data[0].acc_id;
                                        console.log(req.session.userid);
                                        req.session.admin = data[0].admin;
                                        res.redirect('/index2');
                                    } else if (data2.length == 0 && data[0].admin == 1) {
                                        console.log("if3");
                                        req.session.userid = data[0].acc_id;
                                        req.session.admin = data[0].admin;
                                        res.render('./setting/setting', {
                                            data2: data3,
                                            session: req.session
                                        });
                                    }
                                }
                            });
                        }
                    });
                    // });
                });
            });
        });
    });
};
controller.list = (req, res) => {
    const data = null;

    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM account as a LEFT JOIN muitifactor as m ON m.acc_id=a.acc_id WHERE a.acc_id NOT IN (SELECT acc_id FROM `del_acc`)', (err, account_list) => {
                conn.query('SELECT date_format(history.datetime,"%Y-%m-%d %H:%m:%S")as date,history.msg,account.name FROM `history` JOIN account ON account.acc_id=history.acc_id ORDER BY history.datetime DESC', (err, history) => {
                    res.render('./account/account_list', {
                        data: account_list,
                        history: history,
                        session: req.session
                    });
                });
            });
        });
    }
};
controller.quest = (req, res) => {
    const data = null;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `questionnaire` ORDER BY `questionnaire`.`quest_id` DESC', (err, account_list) => {
                res.render('./account/questionnaire', {
                    data: account_list,
                    session: req.session
                });
            });
        });
    }
};
controller.questview = (req, res) => {
    const data = null;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM `questionnaire` ORDER BY `questionnaire`.`quest_id` DESC', (err, account_list) => {
                res.render('./account/questionnaire_view', {
                    data: account_list,
                    session: req.session
                });
            });
        });
    }
};
controller.ans = (req, res) => {
    const data = req.body;
    //  res.json(data);
    if (data.md_checkbox_21 == "on") {
        data.md_checkbox_21 = 1;
    } else {
        data.md_checkbox_21 = 0;
    }
    if (data.md_checkbox_22 == "on") {
        data.md_checkbox_22 = 1;
    } else {
        data.md_checkbox_22 = 0;
    }
    if (data.md_checkbox_23 == "on") {
        data.md_checkbox_23 = 1;
    } else {
        data.md_checkbox_23 = 0;
    }
    if (data.md_checkbox_24 == "on") {
        data.md_checkbox_24 = 1;
    } else {
        data.md_checkbox_24 = 0;
    }
    // if (data.md_checkbox_25 == "on") {
    //     data.md_checkbox_25 = 1;
    // } else {
    //     data.md_checkbox_25 = 0;
    // }
    // if (data.md_checkbox_26 == "on") {
    //     data.md_checkbox_26 = 1;
    // } else {
    //     data.md_checkbox_26 = 0;
    // }
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            console.log(data);
            conn.query('INSERT INTO questionnaire set ?', [data], (err, questionnaire) => {
                console.log(questionnaire);
                conn.query('SELECT * FROM `questionnaire` ORDER BY `questionnaire`.`quest_id` DESC', (err, account_list) => {

                    // res.redirect('/index2');
                    res.render('./account/questionnaire', {
                        data: account_list,
                        session: req.session
                    });
                });
            });
        });
    }
};
controller.new = (req, res) => {
    const data = null;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM account WHERE admin = 1', (err, account_list1) => {
                conn.query('SELECT * FROM account WHERE admin = 0', (err, account_list2) => {
                    conn.query('SELECT * FROM account WHERE admin = 2', (err, account_list3) => {
                        conn.query('SELECT * FROM `set_system`  ORDER BY `set_system`.`sys_id` DESC LIMIT 1', (err, set_system) => {
                            res.render('./account/account_new', {
                                data1: account_list1,
                                data2: account_list2,
                                data3: account_list3,
                                data4: set_system,
                                session: req.session
                            });
                        });
                    });
                });
            });
        });
    }
};

controller.adduser = (req, res) => {
    const data = req.body;
    const errors = validationResult(req);
    date = addDate();
    if (data.otp_email == "on") {
        data.otp_email = 1;
    } else {
        data.otp_email = 0;
    }
    if (data.otp_sms == "on") {
        data.otp_sms = 1;
    } else {
        data.otp_sms = 0;
    }
    if (data.otp_2fa == "on") {
        data.otp_2fa = 1;
    } else {
        data.otp_2fa = 0;
    }
    if (data.otp_login == "on") {
        data.otp_login = 1;
    } else {
        data.otp_login = 0;
    }
    if (data.otp_system == "on") {
        data.otp_system = 1;
    } else {
        data.otp_system = 0;
    }
    //res.json(data);
    id = req.session.userid;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/account/new');
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
            req.getConnection((err, conn) => {
                if (req.files) {
                    var filename = req.files.img;
                    if (!filename.map) {
                        var newfilename = uuidv4() + "." + filename.name.split(".")[1];
                        console.log(newfilename);
                        const savePath = path.join(__dirname, '../public/UI/image', newfilename);
                        filename.mv(savePath);
                        console.log(savePath);
                    }
                    data.image = newfilename;
                }
                conn.query('SELECT * FROM `account` WHERE acc_id = ?', [id], (err, account) => {
                    conn.query('INSERT INTO account (firstname,lastname,name,position,descrip,contact,ext,phone,email,line,username,password,admin ,bd,image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [data.firstname, data.lastname, data.name, data.position, data.descrip, data.contact, data.ext, data.phone, data.email, data.line, data.username, data.password, data.admin, data.bd, data.image], (err, admin_add) => {
                        msg = account[0].name + ' เพิ่มผู้ใช้งาน ' + data.name;
                        type = "adduser";
                        conn.query('SELECT * FROM `account` WHERE username= ? AND password=?', [data.username, data.password], (err, LIMIT) => {
                            conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [id, date, msg, type], (err, history_add) => {
                                conn.query('INSERT INTO `muitifactor` (`acc_id`, `otp_sms`, `otp_email`, `otp_2fa`, `otp_login`, `otp_system`, `policy`) VALUES (?, ?, ?, ?, ?, ?, "1");', [LIMIT[0].acc_id, data.otp_sms, data.otp_email, data.otp_2fa, data.otp_login, data.otp_system], (err, muitifactor) => {
                                    if (err) {
                                        res.json(err);
                                    }
                                    res.redirect('/account/list');
                                });
                            });
                        });
                    });
                });
            });
        }
    }
};
controller.add = (req, res) => {
    const data = req.body;
    const errors = validationResult(req);

    if (data.otp_email == "on") {
        data.otp_email = 1;
    } else {
        data.otp_email = 0;
    }
    if (data.otp_sms == "on") {
        data.otp_sms = 1;
    } else {
        data.otp_sms = 0;
    }
    if (data.otp_2fa == "on") {
        data.otp_2fa = 1;
    } else {
        data.otp_2fa = 0;
    }
    if (data.otp_login == "on") {
        data.otp_login = 1;
    } else {
        data.otp_login = 0;
    }
    if (data.otp_system == "on") {
        data.otp_system = 1;
    } else {
        data.otp_system = 0;
    }
    if (data.md_checkbox_21 == "on") {
        data.md_checkbox_21 = 1;
    } else {
        data.md_checkbox_21 = 0;
    }
    if (data.md_checkbox_22 == "on") {
        data.md_checkbox_22 = 1;
    } else {
        data.md_checkbox_22 = 0;
    }
    if (data.md_checkbox_23 == "on") {
        data.md_checkbox_23 = 1;
    } else {
        data.md_checkbox_23 = 0;
    }
    if (data.md_checkbox_24 == "on") {
        data.md_checkbox_24 = 1;
    } else {
        data.md_checkbox_24 = 0;
    }
    // res.json(data);
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO account (firstname,lastname,name,position,descrip,contact,ext,phone,email,line,username,password,admin ,bd,image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,"user_pic.png")', [data.firstname, data.lastname, data.name, data.position, data.descrip, data.contact, data.ext, data.phone, data.email, data.line, data.username, data.password, 1, data.bd], (err, admin_add) => {
            conn.query('SELECT * FROM `account` WHERE username= ? AND password=?', [data.username, data.password], (err, account_list) => {
                // conn.query('SELECT DATE_FORMAT(DATE_ADD(( CURDATE()),INTERVAL 0 DAY),"%Y-%m-%d %H:%m:%S") as date', (err, date) => {
                conn.query('INSERT INTO `muitifactor` (`acc_id`, `otp_sms`, `otp_email`, `otp_2fa`, `otp_login`, `otp_system`, `policy`) VALUES (?, ?, ?, ?, ?, ?, "1");', [account_list[0].acc_id, data.otp_sms, data.otp_email, data.otp_2fa, data.otp_login, data.otp_system], (err, muitifactor) => {
                    conn.query('INSERT INTO `questionnaire` (`quest_id`, `Name1`, `firstName1`, `lastName1`, `emailAddress1`, `phone1`, `shortDescription1`, `service`, `md_checkbox_21`, `md_checkbox_22`, `md_checkbox_23`, `md_checkbox_24`, `store`, `shortDescription2`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);', [data.Name1, data.firstName1, data.lastName1, data.emailAddress1, data.phone1, data.shortDescription1, data.service, data.md_checkbox_21, data.md_checkbox_22, data.md_checkbox_23, data.md_checkbox_24, data.store, data.shortDescription2], (err, questionnaire) => {
                        console.log(questionnaire);
                        if (err) {
                            res.json(err);
                        }
                        res.redirect('/login');
                    });
                });
                // });
            });
        });
    });
};

controller.delete = (req, res) => {
    const { id } = req.params;
    //res.json(id);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM account as a LEFT JOIN muitifactor as m ON m.acc_id=a.acc_id WHERE a.acc_id=?', [id], (err, admin_delete) => {
                if (err) {
                    res.json(err);
                }
                res.render('./account/del_account', {
                    data: admin_delete,
                    session: req.session
                });
            });
        });
    }
};

controller.del = (req, res) => {
    const { id } = req.params;
    id2 = req.session.userid;
    date = addDate();

    const errors = validationResult(req);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/account/list' + id);
            console.log("if");
        } else {

            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM `account` WHERE acc_id = ?', [id], (err, account) => {
                    conn.query('SELECT * FROM `account` WHERE acc_id = ?', [id2], (err, account2) => {
                        msg = account2[0].name + ' ลบผู้ใช้งาน ' + account[0].name + 'ออกจากระบบ';
                        type = "deleteuser";
                        conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [id2, date, msg, type], (err, history_add) => {
                            conn.query('INSERT INTO `del_acc` (`del_id`, `acc_id`) VALUES (NULL, ?);', [id], (err, admin_confirmdelete) => {
                                console.log("admin_confirmdelete" + admin_confirmdelete);
                                res.redirect('/account/list');
                            });
                        });
                    });
                });
            });
        }
    }
};

controller.edit = (req, res) => {
    const data = req.body;

    const { id } = req.params;
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM account WHERE admin = 1', (err, account_list1) => {
                conn.query('SELECT * FROM account WHERE admin = 0', (err, account_list2) => {
                    conn.query('SELECT * FROM account WHERE admin = 2', (err, account_list3) => {
                        conn.query('SELECT * FROM `set_system`  ORDER BY `set_system`.`sys_id` DESC LIMIT 1', (err, set_system) => {

                            conn.query('SELECT a.firstname,a.lastname,a.name,a.image,a.acc_id,a.position,a.descrip,a.contact,a.ext,a.phone,a.email,a.line,a.username,a.password,a.admin,date_format(a.bd,"%Y-%m-%d") as bd,m.otp_sms,m.otp_email,m.otp_2fa,m.otp_login,m.otp_system FROM account as a LEFT JOIN muitifactor as m ON m.acc_id=a.acc_id WHERE a.acc_id=?', [id], (err, admin_edit) => {
                                res.render('./account/edit_account', {
                                    data: admin_edit,
                                    data1: account_list1,
                                    data2: account_list2,
                                    data3: account_list3,
                                    data4: set_system,
                                    session: req.session
                                });
                            });
                        });
                    });
                });
            });
        });
    }
};

controller.save = (req, res) => {
    const { id } = req.params;
    const data = req.body;
    id2 = req.session.userid;
    date = addDate();
    const errors = validationResult(req);
    if (data.otp_email == "on") {
        data.otp_email = 1;
    } else {
        data.otp_email = 0;
    }
    if (data.otp_sms == "on") {
        data.otp_sms = 1;
    } else {
        data.otp_sms = 0;
    }
    if (data.otp_2fa == "on") {
        data.otp_2fa = 1;
    } else {
        data.otp_2fa = 0;
    }
    if (data.otp_login == "on") {
        data.otp_login = 1;
    } else {
        data.otp_login = 0;
    }
    if (data.otp_system == "on") {
        data.otp_system = 1;
    } else {
        data.otp_system = 0;
    }
    console.log(data);
    if (typeof req.session.userid == 'undefined' || req.session.admin == '0') { res.redirect('/'); } else {
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/account/edit/' + id)
        } else {
            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลสำเร็จ";
            req.getConnection((err, conn) => {
                if (req.files) {
                    var filename = req.files.img;
                    if (!filename.map) {
                        var newfilename = uuidv4() + "." + filename.name.split(".")[1];
                        console.log(newfilename);
                        const savePath = path.join(__dirname, '../public/UI/image', newfilename);
                        filename.mv(savePath);
                        console.log(savePath);
                    }
                    data.image = newfilename;
                }
                console.log(data.image);
                conn.query('SELECT * FROM `account` WHERE acc_id = ?', [id2], (err, account) => {
                    msg = account[0].name + ' แก้ไขข้อมูลผู้ใช้งาน ' + data.name;
                    type = "edituser";
                    conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [id2, date, msg, type], (err, history_add) => {
                        conn.query('UPDATE account set firstname =?,lastname =?,name =?,position =?,descrip =?,contact =?,ext =?,phone =?,email =?,line =?,username =?,password =?,admin =? ,bd =?,image=? where acc_id = ?', [data.firstname, data.lastname, data.name, data.position, data.descrip, data.contact, data.ext, data.phone, data.email, data.line, data.username, data.password, data.admin, data.bd, data.image, id], (err, admin_save) => {
                            conn.query('UPDATE `muitifactor` set `otp_sms`=?, `otp_email`=?, `otp_2fa`=?, `otp_login`=?, `otp_system`=? where acc_id = ?', [data.otp_sms, data.otp_email, data.otp_2fa, data.otp_login, data.otp_system, id], (err, muitifactor) => {
                                console.log(muitifactor);
                                if (err) {
                                    res.json(err);
                                }
                                res.redirect('/account/list');
                            });
                        });
                    });
                });
            });
        }
    }

}

module.exports = controller;