const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const path = require("path");
const fileUpload = require("express-fileupload");
const util = require("util");
const app = express();
const fs = require('fs');
const http = require('http');
const thai = require('node-datetime-thai');
// const session_id = require('express-session-id'); // npm get session_id no browser
const cors = require('cors');

app.use(cors());
app.use(express.json())
app.use(fileUpload());
app.use(cookie());
app.use('/assets', express.static('assets')) // เเ
app.use('/dist', express.static('dist'))


const { check } = require('express-validator');
const { validationResult } = require('express-validator');
const multer = require('multer')

function addDate() {
    function addZero(i) {
        if (i < 10) { i = "0" + i }
        return i;
    }
    const months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    let current_datetime = new Date()
    let formatted_date = current_datetime.getFullYear() + "-" + months[current_datetime.getMonth()] + "-" + current_datetime.getDate()
    let current_time = new Date()
    let formatted_time = addZero(current_time.getHours()) + ":" + addZero(current_time.getMinutes()) + ":" + addZero(current_time.getSeconds())

    console.log("formatted_date" + formatted_date);
    console.log("formatted_time" + formatted_time);
    date = formatted_date + ' ' + formatted_time;
    return date;
}
//app.use(express.static('public'));
app.use(express.static(path.join(__dirname, '/public')));
app.use(fileUpload());
app.use(express.json());
//setting
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(body.json({ limit: '50mb' }))
app.use(body.urlencoded({ limit: '50mb', extended: true }));
//app.set('views' , 'views');
app.set('views', path.join(__dirname, '/views'));
app.use(express.static(__dirname + '/public'))
    //middlewares

app.use(body.urlencoded({ extended: true }));
app.use(cookie());

app.use(session({
    secret: 'Passw0rd',
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 60000000 }
}));
app.use(connection(mysql, {
    host: 'localhost',
    user: 'alltra',
    password: 'rHCBGz4BnmYw@',
    port: 3306,
    database: 'alltra',
    timezone: 'utc'
}, 'single'));



app.get('/', function(req, res) {

    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM account ', (err, data) => {
            //res.json(data);
            if (data.length > 0) {

                res.render('login', { session: req.session });
            } else {
                res.render('./account/wizard', { session: req.session });
            }
        });
    });
});

app.get('/logout', function(req, res) {
    id = req.session.userid;
    const date = addDate();
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM account WHERE acc_id = ? ', [id], (err, data) => {
            // date = formatted_date + ' ' + formatted_time;
            msg = data[0].name + ' ออกจะระบบเมื่อ ' + date;
            type = "logout"
            conn.query('INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES (?, ?, ?, NULL, ?);', [data[0].acc_id, date, msg, type], (err, access_history) => {

            });
        });
    });
    req.session.destroy(function(error) {
        res.redirect('/');
    });

});
const log_routes = require('./routes/log_routes');
app.use('/', log_routes);
const group_routes = require('./routes/group_routes');
app.use('/', group_routes);
const device_routes = require('./routes/device_routes');
app.use('/', device_routes);
const setting_routes = require('./routes/setting_routes');
app.use('/', setting_routes);
const account_routes = require('./routes/account_routes');
app.use('/', account_routes);
const block_routes = require('./routes/block_routes');
app.use('/', block_routes);
const views_routes = require('./routes/views_routes');
app.use('/', views_routes);
const importdata_routes = require('./routes/importdata_routes');
app.use('/', importdata_routes);
const getdata_routes = require('./routes/getdata_routes');
app.use('/', getdata_routes);
const exportdata_routes = require('./routes/exportdata_routes');
app.use('/', exportdata_routes);
const board_system = require('./routes/board_system');
app.use('/', board_system);

const route_cookie_managemet = require('./routes/route_cookie_managemet/route_cookie_managemet');
app.use('/', route_cookie_managemet);

// const route_login = require('./routes/route_login/route_login');
// app.use('/', route_login);

const route_cookietype = require('./routes/route_cookietype/route_cookietype');
app.use('/', route_cookietype);

const route_domain = require('./routes/route_domain/route_domain');
app.use('/', route_domain);

const route_user = require('./routes/route_user/route_user');
app.use('/', route_user);

const route_report_cookieconsent = require('./routes/route_report_cookieconsent/route_report_cookieconsent');
app.use('/', route_report_cookieconsent);

const route_emailconsent = require('./routes/route_report_emailconsent/route_report_emailconsent');
app.use('/', route_emailconsent);


// route Model_cookie_consent
const route_cookieconsent = require('./routes/route_cookieconsent/route_cookieconsent');
app.use('/', route_cookieconsent);

const route_email = require('./routes/route_email/route_email');
app.use('/', route_email);

const route_appeal = require('./routes/route_appeal/route_appeal');
app.use('/', route_appeal);

const indexRoute = require('./routes/indexRoute');
app.use('/', indexRoute);
// Agent Monitor
const agentRoute = require('./routes/agentsRoute');
app.use('/', agentRoute);
// Pattern
const patternRoute = require('./routes/patternRoute');
app.use('/', patternRoute);
// Classification
const classificationRoute = require('./routes/classificationRoute');
app.use('/', classificationRoute);

const dataoutRoute = require('./routes/dataoutRoute');
app.use('/', dataoutRoute);

app.listen('8081');