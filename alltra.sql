-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2022 at 11:09 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alltra`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `descrip` varchar(255) DEFAULT NULL,
  `contact` varchar(255) NOT NULL,
  `ext` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `line` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin` varchar(255) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `bd` date NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`firstname`, `lastname`, `name`, `position`, `descrip`, `contact`, `ext`, `phone`, `email`, `line`, `username`, `password`, `admin`, `acc_id`, `bd`, `image`) VALUES
('Wasin', 'Wachiropakorn', 'Wasin Wachiropakorn', 'Network Engineer', '', '0824819953', '1', '0824819953', 'wasin_w@sense-infotech.com', 'twasin', 'wasin', 'wasin', '1', 175, '1993-09-03', '9ce242ca-2552-4a99-8d37-4558498093e3.png'),
('Artit', 'Aungpraphapornchai', 'Artit Aungpraphapornchai', 'Engineer', '', '00000000', '0', '0864271163', 'foxeel@gmail.com', 'foxeel', 'data', 'data', '0', 176, '2022-02-18', 'user_pic.png'),
('Wasin2', '2', 'Wasin2', 'NW', '', '1111', '111', '0824819953', 'wasin_w@sense-infotech.com', 'twasin', 'wasin2', 'wasin2', '2', 177, '2022-03-01', 'user_pic.png'),
('wasin3', 'wasin3', 'Wasin3', 'test', '', '.', '.', '.', 'test@mail.com', '.', 'wasin3', 'wasin3', '2', 178, '0003-02-12', 'user_pic.png');

-- --------------------------------------------------------

--
-- Table structure for table `alert`
--

CREATE TABLE `alert` (
  `alert_id` int(11) NOT NULL,
  `device_id` int(11) DEFAULT NULL,
  `msg` varchar(255) NOT NULL,
  `datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `blocked`
--

CREATE TABLE `blocked` (
  `b_id` int(11) NOT NULL,
  `b_ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `input_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `checktime`
--

CREATE TABLE `checktime` (
  `check_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `sendmail` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `checktime`
--

INSERT INTO `checktime` (`check_id`, `device_id`, `time`, `sendmail`) VALUES
(5, 88, 3, 1),
(9, 89, 3, 1),
(19, 93, 1, 0),
(20, 94, 10, 0),
(21, 95, 10, 0),
(22, 96, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cookiepolicy`
--

CREATE TABLE `cookiepolicy` (
  `id_cp` int(11) NOT NULL,
  `name_cp` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `detail_cp` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `approve_cp` tinyint(4) DEFAULT NULL,
  `date_cp` datetime DEFAULT NULL,
  `domaingroup_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cookiepolicy`
--

INSERT INTO `cookiepolicy` (`id_cp`, `name_cp`, `detail_cp`, `approve_cp`, `date_cp`, `domaingroup_id`) VALUES
(138, 'จำเป็น', 'คุกกี้ที่จำเป็นจะช่วยทำให้เว็บไซต์สามารถใช้งานได้โดยเปิดใช้งานฟังก์ชันพื้นฐาน เช่น ระบบนำทางของหน้าและการเข้าถึงพื้นที่ปลอดภัยของเว็บไซต์ เว็บไซต์จะไม่สามารถทำงานได้อย่างถูกต้องหากไม่มีคุกกี้เหล่านี้', 1, NULL, NULL),
(139, 'การกำหนดค่า', 'คุกกี้ประเภทการตั้งค่าจะช่วยให้เว็บไซต์จดจำข้อมูลซึ่งเปลี่ยนวิธีการที่เว็บไซต์ทำงานหรือแสดงผล เช่น ภาษาที่คุณกำหนดเป็นค่าที่ชื่นชอบหรือภูมิภาคที่คุณอาศัยอยู่', 1, NULL, NULL),
(140, 'สถิติ', 'คุกกี้ประเภทสถิติจะช่วยให้เจ้าของเว็บไซต์ทำความเข้าใจว่าผู้เยี่ยมชมมีปฏิสัมพันธ์กับเว็บไซต์เช่นใด โดยการเก็บและการรายงานข้อมูลโดยไม่ระบุชื่อ', 1, NULL, NULL),
(141, 'การตลาด', 'คุกกี้ประเภทการตลาดใช้สำหรับการติดตามผู้ใช้ทั่วทั้งเว็บไซต์ เป้าหมายของมันคือเพื่อแสดงโฆษณาที่เกี่ยวข้องและน่าสนใจสำหรับผู้ใช้แต่ละคน และดังนั้นจึงเป็นการเพิ่มคุณค่าให้กับผู้เผยแพร่และผู้โฆษณาบุคคลที่สาม', 1, NULL, NULL),
(142, 'ไม่ได้จำแนกประเภท', 'คุกกี้ที่ไม่ได้รับการจำแนกประเภทคือคุกกี้ที่เรากำลังอยู่ในขั้นตอนการจำแนกประเภท โดยร่วมมือกับผู้ให้บริการคุกกี้แต่ละรายการ', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_out`
--

CREATE TABLE `data_out` (
  `out_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `descrip` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `datetime` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `filters` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `type_alert` tinyint(1) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `alert_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_out`
--

INSERT INTO `data_out` (`out_id`, `name`, `descrip`, `create_date`, `datetime`, `status`, `filters`, `destination`, `type_alert`, `phone`, `alert_status`) VALUES
(1, 'Remotelog', 1, '0000-00-00 00:00:00', 0, NULL, 'email server', '1.1.1.2', 0, '+66886904229', 1),
(2, '1', 1, '0000-00-00 00:00:00', 0, NULL, '1', '1', 0, '', 1),
(3, 'Ridtirong Vongpiarpun', 0, '0000-00-00 00:00:00', 0, NULL, 'a', 'a', 1, '+66886904229', 0),
(4, '1', 1, '2022-03-22 11:21:00', 0, NULL, '1', '1', 0, '', 0),
(5, '3', 3, '2022-03-22 11:22:00', 0, NULL, '3', '3', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `del_acc`
--

CREATE TABLE `del_acc` (
  `del_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `del_device`
--

CREATE TABLE `del_device` (
  `del_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `device_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `de_type` varchar(255) NOT NULL,
  `data_type` varchar(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `keep` int(11) DEFAULT NULL,
  `rmfile` datetime DEFAULT NULL,
  `hostname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `spec` varchar(255) DEFAULT NULL,
  `location_bu` varchar(255) DEFAULT NULL,
  `backup` tinyint(1) DEFAULT NULL,
  `de_ip` varchar(255) NOT NULL,
  `eth` varchar(255) NOT NULL,
  `hash` tinyint(1) DEFAULT NULL,
  `input_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`device_id`, `status`, `de_type`, `data_type`, `sender`, `keep`, `rmfile`, `hostname`, `name`, `spec`, `location_bu`, `backup`, `de_ip`, `eth`, `hash`, `input_id`, `image`) VALUES
(94, 'ใช้งาน', 'Firewall', 'ข้อมูลจราจรทางคอมพิวเตอร์จากการเชื่อต่อเข้าถึงระบบเครือข่าย (Internet Access)', 'UDP', 90, '2022-07-05 01:31:16', '172.16.20.221', 'Remotelog', '', NULL, NULL, '172.16.20.221', 'Eth0', 1, 96, '1200px-Circle-icons-computer.svg.png'),
(95, 'เตรียม', 'เครื่องแม่ข่าย', 'ข้อมูลจราจรทางคอมพิวเตอร์จากการเชื่อต่อเข้าถึงระบบเครือข่าย (Internet Access)', 'UDP', 90, '2022-07-05 01:33:28', '103.136.249.119', '103.136.249.119', '', NULL, NULL, '103.136.249.119', 'Eth0', 1, 91, '1200px-Circle-icons-computer.svg.png'),
(96, 'เตรียม', 'อื่นๆ', 'ข้อมูลจราจรทางคอมพิวเตอร์จากเครื่องผู้ให้บริการเว็บ (Web)', 'UDP', 90, '2022-07-05 01:34:49', '209.97.136.164', '209.97.136.164', '', NULL, NULL, '209.97.136.164', 'Eth0', 1, 106, '1200px-Circle-icons-computer.svg.png');

-- --------------------------------------------------------

--
-- Table structure for table `device_member`
--

CREATE TABLE `device_member` (
  `dm_id` int(11) NOT NULL,
  `de_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `dialog`
--

CREATE TABLE `dialog` (
  `id_dl` int(11) NOT NULL,
  `dialog_dl` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `general_dl` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `date_dl` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `dialog`
--

INSERT INTO `dialog` (`id_dl`, `dialog_dl`, `general_dl`, `date_dl`) VALUES
(1, 'แสดงข้อความในหน้าแรกของ Cookie โดยข้อความเริ่มต้น คือค่าเริ่มต้น', NULL, '2022-02-04 21:49:44');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_chat`
--

CREATE TABLE `doc_pdpa_chat` (
  `chat_id` int(11) NOT NULL,
  `ref_type` int(11) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `chat_data` int(11) DEFAULT NULL,
  `chat_text` text DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `user_id_dest` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_classification`
--

CREATE TABLE `doc_pdpa_classification` (
  `classify_id` int(11) NOT NULL,
  `pattern_id` int(11) DEFAULT NULL,
  `classify_name` varchar(255) DEFAULT NULL,
  `event_process_id` int(11) DEFAULT NULL,
  `classify_create` datetime NOT NULL DEFAULT current_timestamp(),
  `classify_explain_process` varchar(255) DEFAULT NULL,
  `classify_user_access_info_process_inside` tinyint(1) DEFAULT NULL,
  `classify_user_access_info_process_inside_from_pattern` varchar(255) DEFAULT NULL,
  `classify_user_access_info_process_inside_from_pattern_id` varchar(255) DEFAULT NULL,
  `classify_user_access_info_process_inside_from_pattern_total` int(11) DEFAULT NULL,
  `classify_user_access_info_process_outside` tinyint(1) DEFAULT NULL,
  `classify_user_access_info_process_outside_from_pattern` varchar(255) DEFAULT NULL,
  `classify_user_access_info_process_outside_from_pattern_id` varchar(255) DEFAULT NULL,
  `classify_user_access_info_process_outside_from_pattern_total` int(11) DEFAULT NULL,
  `classify_period_process` int(11) DEFAULT NULL,
  `classify_period_proccess_follow_policy` tinyint(1) DEFAULT NULL,
  `classify_period_proccess_follow_policy_total` int(11) DEFAULT NULL,
  `classify_period_end` int(11) DEFAULT NULL,
  `classify_period_end_follow_pattern` tinyint(1) DEFAULT NULL,
  `classify_period_end_follow_pattern_total` int(11) DEFAULT NULL,
  `classify_type_data_in_event_personal` tinyint(1) DEFAULT NULL,
  `classify_type_data_in_event_personal_datamark` varchar(255) DEFAULT NULL,
  `classify_type_data_in_event_special_personal_sensitive` tinyint(1) DEFAULT NULL,
  `pattern_processing_base_id` int(11) DEFAULT NULL,
  `classify_processing_base_explain` varchar(255) DEFAULT NULL,
  `classification_special_conditions_id` int(11) DEFAULT NULL,
  `classify_special_conditiion_explain` varchar(255) DEFAULT NULL,
  `classify_protect_data_limit_process` int(11) DEFAULT NULL,
  `classify_protect_data_limit_follow_datetime` tinyint(1) DEFAULT NULL,
  `classify_approach_protect_used_two_factor_from_google_authen` tinyint(1) DEFAULT NULL,
  `classify_approach_protect_used_two_factor_from_email` tinyint(1) DEFAULT NULL,
  `classify_approach_protect_used_two_factor_from_sms` tinyint(1) DEFAULT NULL,
  `classify_data_exception_or_unnecessary_filter_name` varchar(255) DEFAULT NULL,
  `classify_data_exception_or_unnecessary_filter_tag` varchar(255) DEFAULT NULL,
  `classify_data_exception_or_unnecessary_filter_label` varchar(255) DEFAULT NULL,
  `classify_risk_assess_only_dpo_data_personal_can_specify` int(11) DEFAULT NULL,
  `classify_risk_assess_only_dpo_data_number_all_used_process_many` tinyint(1) DEFAULT NULL,
  `classify_risk_assess_only_dpo_data_number_all_used_process_total` varchar(255) DEFAULT NULL,
  `classify_risk_assess_only_dpo_access_control_inside` tinyint(1) DEFAULT NULL,
  `classify_risk_assess_only_dpo_access_control_outside` tinyint(1) DEFAULT NULL,
  `classify_risk_assess_only_dpo_protect_data_inside` tinyint(1) DEFAULT NULL,
  `classify_risk_assess_only_dpo_protect_data_outside` tinyint(1) DEFAULT NULL,
  `classify_risk_assess_only_dpo_assess_the_impact_of_data` int(11) DEFAULT NULL,
  `classify_risk_assess_only_dpo_fix_a_leak_of_data` varchar(255) DEFAULT NULL,
  `classify_risk_assess_only_dpo_assess_the_impact_of_organization` int(11) DEFAULT NULL,
  `classify_risk_assess_only_dpo_fix_a_leak_of_organization` varchar(255) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_classification_special_conditions`
--

CREATE TABLE `doc_pdpa_classification_special_conditions` (
  `classification_special_conditions_id` int(11) NOT NULL,
  `classification_special_conditions_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_classification_special_conditions`
--

INSERT INTO `doc_pdpa_classification_special_conditions` (`classification_special_conditions_id`, `classification_special_conditions_name`) VALUES
(1, 'ความยินยอมโดยชัดแจ้ง มาตรา 26'),
(2, 'อันตรายต่อชีวิต มาตรา 26'),
(3, 'องค์กรไม่แสวงหากำไรเฉพาะเรื่อง มาตรา 26'),
(4, 'เปิดเผยต่อสาธารณะด้วยความยินยอมโดยชัดแจ้ง มาตรา 26'),
(5, 'จำเป็นเพื่อก่อตั้งสิทธิเรียกร้องตามกฎหมาย มาตรา 26'),
(6, 'แพทย์ มาตรา 26'),
(7, 'สาธารณสุข มาตรา 26'),
(8, 'วิจัย มาตรา 26'),
(9, 'ประโยชน์สาธารณะที่สำคัญ มาตรา 26');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_data`
--

CREATE TABLE `doc_pdpa_data` (
  `data_id` int(11) NOT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `data_code` varchar(50) DEFAULT NULL,
  `data_name` varchar(255) DEFAULT NULL,
  `data_level_id` int(11) NOT NULL,
  `data_detail` longtext DEFAULT NULL,
  `data_date_start` datetime DEFAULT NULL,
  `data_date_end` datetime DEFAULT NULL,
  `data_location_name` varchar(255) DEFAULT NULL,
  `data_location_detail` longtext DEFAULT NULL,
  `data_tag` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_data`
--

INSERT INTO `doc_pdpa_data` (`data_id`, `data_type_id`, `data_code`, `data_name`, `data_level_id`, `data_detail`, `data_date_start`, `data_date_end`, `data_location_name`, `data_location_detail`, `data_tag`) VALUES
(12, 10, '#id_card_number', 'เลขบัตรประชาชน', 35, 'เลขบัตรประชาชนของคนในสำนักงาน', '2022-02-24 22:38:00', '2022-02-24 22:38:00', '/folder/server/id_card_number', 'เก็บใน server สำนักงาน', NULL),
(20, 10, '#mixcode', 'ที่อยู่', 36, 'คำอธิบาย', '2022-03-01 02:47:00', '2022-03-03 02:47:00', 'สวัสดี', 'สวัสดี', NULL),
(23, 14, '#vvv', 'ที่อยู่', 35, 'คำอธิบาย', '2022-03-02 17:07:00', '2022-03-02 17:07:00', '/folder/server/id_card_number', 'mixCode', NULL),
(26, 35, '#mixcodez', 'mixCode', 42, 'aaa', '2022-02-16 17:36:00', '2022-02-28 17:36:00', 'aaa', 'aaa', NULL),
(28, 14, '#religion', 'ศาสนา', 36, 'ศาสนาของคนในองค์กร', '2022-03-04 01:28:00', '2022-03-12 01:28:00', '/file/sever/religion', 'เก็บใน server สำนักงาน', 'g'),
(29, 10, '#test', 'test1', 35, 'test', '2022-03-04 22:08:00', '2022-04-09 22:08:00', '/folfer/server', 'personnala', 'g'),
(30, 10, '#test1', 'test2', 35, 'test', '2022-03-04 22:08:00', '2022-04-09 22:08:00', '/folfer/server', 'personnala', 'สำคัญ'),
(31, 10, '#test2', 'test3', 35, 'test', '2022-03-04 22:08:00', '2022-04-09 22:08:00', '/folfer/server', 'personnala', 'ธงแดง,g'),
(32, 14, '#test123', 'test123', 36, 'test123', '2022-03-01 00:42:00', '2022-04-09 00:42:00', 'test123', 'test123', 'สำคัญ,xx,oo'),
(33, 10, '#test12345', 'test12345', 36, 'test12345', '2022-03-05 12:00:00', '2022-03-26 12:00:00', 'test12345', 'test12345', 'สำคัญ,g');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_database_check`
--

CREATE TABLE `doc_pdpa_database_check` (
  `id` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `get_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_database_check`
--

INSERT INTO `doc_pdpa_database_check` (`id`, `status`, `get_date`) VALUES
(1, 'a', '2022-03-06 21:04:20'),
(2, 'about', '2022-03-06 21:04:20'),
(3, 'all', '2022-03-06 21:04:20'),
(4, 'also', '2022-03-06 21:04:20'),
(5, 'and', '2022-03-06 21:04:20'),
(6, 'as', '2022-03-06 21:13:40'),
(7, 'at', '2022-03-06 21:13:40'),
(8, 'be', '2022-03-06 21:13:40'),
(9, 'because', '2022-03-06 21:13:40'),
(10, 'but', '2022-03-06 21:13:40'),
(11, 'by', '2022-03-06 21:13:40'),
(12, 'can', '2022-03-06 21:13:40'),
(13, 'come', '2022-03-06 21:13:40'),
(14, 'could', '2022-03-06 21:13:40'),
(15, 'day', '2022-03-06 21:13:40'),
(16, 'do', '2022-03-06 21:13:40'),
(17, 'even', '2022-03-06 21:13:40'),
(18, 'find', '2022-03-06 21:13:40'),
(19, 'first', '2022-03-06 21:13:40'),
(20, 'for', '2022-03-06 21:13:40'),
(21, 'from', '2022-03-06 21:13:40'),
(22, 'get', '2022-03-06 21:13:40'),
(23, 'give', '2022-03-06 21:13:40'),
(24, 'go', '2022-03-06 21:13:40'),
(25, 'have', '2022-03-06 21:13:40'),
(26, 'WAT?!!', '2022-03-06 21:13:40'),
(27, 'her', '2022-03-07 22:13:19'),
(28, 'here', '2022-03-07 22:13:19'),
(29, 'him', '2022-03-07 22:13:19'),
(30, 'his', '2022-03-07 22:13:19'),
(31, 'how', '2022-03-07 22:13:19'),
(32, 'I', '2022-03-07 22:13:19'),
(33, 'if', '2022-03-07 22:13:19'),
(34, 'in', '2022-03-07 22:13:19'),
(35, 'into', '2022-03-07 22:13:19'),
(36, 'it', '2022-03-07 22:13:19'),
(37, 'its', '2022-03-07 22:20:45'),
(38, 'just', '2022-03-07 22:20:45'),
(39, 'know', '2022-03-07 22:20:45'),
(40, 'like', '2022-03-07 22:20:45'),
(41, 'look', '2022-03-07 22:20:45'),
(42, 'make', '2022-03-07 22:20:45'),
(43, 'man', '2022-03-07 22:20:45'),
(44, 'many', '2022-03-07 22:20:45'),
(45, 'me', '2022-03-07 22:20:45'),
(46, 'more', '2022-03-07 22:25:52'),
(47, 'my', '2022-03-07 22:25:52'),
(48, 'new', '2022-03-07 22:25:52'),
(49, 'no', '2022-03-07 22:25:52'),
(50, 'not', '2022-03-07 22:25:52'),
(51, 'now', '2022-03-07 22:25:52'),
(52, 'of', '2022-03-07 22:25:52'),
(53, 'on', '2022-03-07 22:25:52'),
(54, 'one', '2022-03-07 22:25:52'),
(55, 'only', '2022-03-07 22:25:52'),
(56, 'or', '2022-03-07 22:25:52'),
(57, 'other', '2022-03-07 22:25:52'),
(58, 'our', '2022-03-07 22:25:52'),
(59, 'out', '2022-03-07 22:25:52'),
(60, 'people', '2022-03-07 22:25:52'),
(61, 'say', '2022-03-07 22:25:52'),
(62, 'see', '2022-03-07 22:25:52'),
(63, 'she', '2022-03-07 22:25:52'),
(64, 'so', '2022-03-07 22:28:56'),
(65, 'some', '2022-03-07 22:28:56'),
(66, 'take', '2022-03-07 22:28:56'),
(67, 'tell', '2022-03-07 22:28:56'),
(68, 'than', '2022-03-07 22:28:56'),
(69, 'that', '2022-03-07 22:28:56'),
(70, 'her', '2022-03-07 22:43:55'),
(71, 'here', '2022-03-07 22:43:55'),
(72, 'him', '2022-03-07 22:43:55'),
(73, 'his', '2022-03-07 22:43:55'),
(74, 'how', '2022-03-07 22:43:55'),
(75, 'I', '2022-03-07 22:43:55'),
(76, 'if', '2022-03-07 22:43:55'),
(77, 'in', '2022-03-07 22:43:55'),
(78, 'into', '2022-03-07 22:43:55'),
(79, 'it', '2022-03-07 22:43:55'),
(81, 'very', '2022-03-07 22:43:55'),
(82, 'want', '2022-03-07 22:43:55'),
(83, 'way', '2022-03-07 22:43:55'),
(84, 'we', '2022-03-07 22:43:55'),
(85, 'well', '2022-03-07 22:43:55'),
(86, 'their', '2022-03-07 22:45:52'),
(87, 'their', '2022-03-07 22:45:52'),
(88, 'then', '2022-03-07 22:45:52'),
(89, 'there', '2022-03-07 22:45:52'),
(90, 'these', '2022-03-07 22:45:52'),
(91, 'they', '2022-03-07 22:50:33'),
(92, 'thing', '2022-03-07 22:50:33'),
(93, 'think', '2022-03-07 22:50:33'),
(94, 'this', '2022-03-07 22:50:33'),
(95, 'those', '2022-03-07 22:50:33'),
(96, 'time', '2022-03-07 22:50:33'),
(97, 'to', '2022-03-07 22:50:33'),
(98, 'two', '2022-03-07 22:50:33'),
(99, 'up', '2022-03-07 22:50:33'),
(100, 'use', '2022-03-07 22:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_data_type`
--

CREATE TABLE `doc_pdpa_data_type` (
  `data_type_id` int(11) NOT NULL,
  `data_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_data_type`
--

INSERT INTO `doc_pdpa_data_type` (`data_type_id`, `data_type_name`) VALUES
(10, 'ข้อมูลส่วนบุคคล'),
(14, 'ข้อมูลส่วนบุคคลที่มีความละเอียดอ่อน'),
(35, 'a'),
(36, 'aa'),
(38, 'b'),
(40, 'bb'),
(41, 'bbb'),
(42, 'c'),
(43, 'cc'),
(44, 'ccc'),
(45, 'd'),
(46, 'dd'),
(47, 'ddd'),
(48, 'e'),
(49, 'ee'),
(50, 'f'),
(51, 'ff'),
(52, 'fff'),
(53, 'g'),
(54, 'gg'),
(55, 'ggg');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_document`
--

CREATE TABLE `doc_pdpa_document` (
  `doc_id` int(11) NOT NULL,
  `doc_type_id` int(11) DEFAULT NULL,
  `doc_name` varchar(255) DEFAULT NULL,
  `doc_date_create` datetime DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `doc_status` int(11) DEFAULT 0,
  `doc_remark` text DEFAULT NULL,
  `doc_action` tinyint(1) DEFAULT 0,
  `doc_pattern_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_document`
--

INSERT INTO `doc_pdpa_document` (`doc_id`, `doc_type_id`, `doc_name`, `doc_date_create`, `user_id`, `doc_status`, `doc_remark`, `doc_action`, `doc_pattern_status`) VALUES
(273, 152, 'เอกสาร_2', '2022-02-23 22:04:49', 'รัชชานนท์', 0, '', NULL, 1),
(274, 153, 'เอกสาร_2', '2022-02-23 22:06:46', 'รัชชานนท์', 1, '', NULL, NULL),
(275, 152, 'sdasd', '2022-02-25 20:36:25', 'รัชชานนท์', 1, '', NULL, NULL),
(276, 154, 'เอกสาร_3', '2022-02-26 08:23:11', 'รัชชานนท์', 2, '', NULL, NULL),
(277, 152, 'aaaaa', '2022-02-26 10:27:21', 'รัชชานนท์', 3, '', NULL, NULL),
(278, 170, 'ข้อกำหนด', '2022-03-23 22:12:01', 'รัชชานนท์', 0, '', 1, NULL),
(279, NULL, NULL, NULL, 'รัชชานนท์', 0, NULL, NULL, NULL),
(280, NULL, NULL, NULL, 'รัชชานนท์', 0, NULL, NULL, NULL),
(281, NULL, NULL, NULL, 'รัชชานนท์', 0, NULL, NULL, NULL),
(282, NULL, NULL, NULL, 'รัชชานนท์', 0, NULL, NULL, NULL),
(283, NULL, NULL, NULL, 'รัชชานนท์', 0, NULL, NULL, NULL),
(284, 152, 'sdasd', '2022-03-26 03:45:34', NULL, 0, '', NULL, NULL),
(285, 153, 'เอกสาร_2', '2022-03-26 03:48:29', NULL, 0, '', NULL, NULL),
(286, 153, 'เอกสาร_2', '2022-03-26 03:51:58', NULL, 0, '', NULL, NULL),
(287, 153, 'เอกสาร_2', '2022-03-26 03:52:54', NULL, 0, '', NULL, NULL),
(288, 152, 'sdasd', '2022-03-26 03:53:09', NULL, 0, '', NULL, NULL),
(289, 152, 'sdasd', '2022-03-26 03:54:42', 'รัชชานนท์', 0, '', 1, NULL),
(290, 153, 'เอกสาร_2', '2022-03-26 03:56:38', 'รัชชานนท์', 0, '', 1, NULL),
(291, 153, 'เอกสาร_2\'(คัดลอก)', '2022-03-26 03:57:30', 'รัชชานนท์', 0, '', 1, NULL),
(292, 153, 'เอกสารทดสอบ', '2022-03-26 03:59:13', 'รัชชานนท์', 0, '', 1, NULL),
(293, 153, 'เอกสารทดสอบ', '2022-03-26 13:04:48', 'รัชชานนท์', 0, '', 1, NULL),
(294, 153, 'เอกสาร_2(คัดลอก)', '2022-03-26 13:36:20', 'รัชชานนท์', 0, '', 1, NULL),
(295, 194, 'ทดสอบ', '2022-03-27 21:17:35', 'รัชชานนท์', 1, '', NULL, NULL),
(296, 194, 'ทดสอบ(คัดลอก)', '2022-03-27 21:18:22', 'รัชชานนท์', 0, '', NULL, NULL),
(297, 152, 'เอกสาร_2(คัดลอก)', '2022-03-27 21:20:48', 'รัชชานนท์', 2, '', NULL, 1),
(298, 153, 'เอกสาร_2(คัดลอก)', '2022-03-27 21:46:41', 'รัชชานนท์', 0, '', NULL, NULL),
(299, 152, 'sdasd(คัดลอก)', '2022-03-27 21:47:21', 'รัชชานนท์', 2, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_document_log`
--

CREATE TABLE `doc_pdpa_document_log` (
  `log_id` int(11) NOT NULL,
  `doc_id` int(11) DEFAULT NULL,
  `log_date` datetime DEFAULT NULL,
  `log_action` int(11) DEFAULT NULL,
  `log_detail` varchar(255) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `user_id_dest` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_document_log`
--

INSERT INTO `doc_pdpa_document_log` (`log_id`, `doc_id`, `log_date`, `log_action`, `log_detail`, `user_id`, `user_id_dest`) VALUES
(683, 273, '2022-02-23 22:04:49', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(684, 273, '2022-02-23 22:04:49', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(685, 273, '2022-02-23 22:04:49', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(686, 274, '2022-02-23 22:06:46', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(687, 274, '2022-02-23 22:06:46', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(688, 274, '2022-02-23 22:06:46', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(689, 275, '2022-02-25 20:36:25', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(690, 275, '2022-02-25 20:36:25', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(691, 275, '2022-02-25 20:36:25', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(703, 276, '2022-02-26 08:23:11', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(704, 276, '2022-02-26 08:23:11', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(705, 276, '2022-02-26 08:23:11', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(706, 277, '2022-02-26 10:27:21', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(707, 277, '2022-02-26 10:27:21', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(708, 277, '2022-02-26 10:27:21', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(748, 273, '2022-03-11 22:25:11', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(749, 273, '2022-03-11 22:25:27', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(750, 273, '2022-03-11 22:25:49', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(751, 273, '2022-03-11 22:26:08', 2, 'เพิ่มหน้า 2', 'รัชชานนท์', NULL),
(752, 273, '2022-03-11 22:26:20', 3, 'แก้ไขหน้า 2', 'รัชชานนท์', NULL),
(753, 273, '2022-03-11 22:28:12', 2, 'เพิ่มหน้า 3', 'รัชชานนท์', NULL),
(754, 273, '2022-03-11 22:28:14', 2, 'เพิ่มหน้า 4', 'รัชชานนท์', NULL),
(755, 273, '2022-03-11 22:28:26', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(756, 273, '2022-03-11 22:28:46', 3, 'แก้ไขหน้า 2', 'รัชชานนท์', NULL),
(757, 273, '2022-03-11 22:29:00', 3, 'แก้ไขหน้า 3', 'รัชชานนท์', NULL),
(758, 273, '2022-03-11 22:29:11', 3, 'แก้ไขหน้า 4', 'รัชชานนท์', NULL),
(759, 278, '2022-03-23 22:12:01', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(760, 278, '2022-03-23 22:12:01', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(761, 278, '2022-03-23 22:12:01', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(762, 273, '2022-03-24 01:44:54', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(763, 273, '2022-03-24 02:30:06', 7, 'แชร์เอกสารไปที่ Email:huahomnoiz331@gmail.com', 'รัชชานนท์', NULL),
(764, 278, '2022-03-25 23:19:41', 5, 'ลบเอกสาร', 'รัชชานนท์', NULL),
(765, 279, NULL, 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(766, 279, NULL, 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(767, 279, NULL, 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(768, 280, NULL, 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(769, 280, NULL, 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(770, 280, NULL, 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(771, 281, NULL, 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(772, 281, NULL, 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(773, 281, NULL, 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(774, 282, NULL, 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(775, 282, NULL, 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(776, 282, NULL, 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(777, 283, NULL, 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(778, 283, NULL, 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(779, 283, NULL, 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(780, 289, '2022-03-26 03:54:42', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(781, 289, '2022-03-26 03:54:42', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(782, 289, '2022-03-26 03:54:42', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(783, 290, '2022-03-26 03:56:38', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(784, 290, '2022-03-26 03:56:38', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(785, 290, '2022-03-26 03:56:38', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(786, 291, '2022-03-26 03:57:30', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(787, 291, '2022-03-26 03:57:30', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(788, 291, '2022-03-26 03:57:30', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(789, 289, '2022-03-26 03:57:50', 5, 'ลบเอกสาร', 'รัชชานนท์', NULL),
(790, 290, '2022-03-26 03:57:53', 5, 'ลบเอกสาร', 'รัชชานนท์', NULL),
(791, 291, '2022-03-26 03:57:56', 5, 'ลบเอกสาร', 'รัชชานนท์', NULL),
(792, 291, '2022-03-26 03:57:30', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(793, 291, '2022-03-26 03:57:30', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(794, 291, '2022-03-26 03:57:30', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(795, 292, '2022-03-26 03:59:13', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(796, 292, '2022-03-26 03:59:13', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(797, 292, '2022-03-26 03:59:13', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(798, 292, '2022-03-26 13:04:48', 5, 'ลบเอกสาร', 'รัชชานนท์', NULL),
(799, 293, '2022-03-26 13:04:48', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(800, 293, '2022-03-26 13:04:48', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(801, 293, '2022-03-26 13:04:48', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(802, 293, '2022-03-26 13:35:05', 7, 'แชร์เอกสารไปที่ Email : huahomnoiz331@gmail.com', 'รัชชานนท์', NULL),
(803, 294, '2022-03-26 13:36:20', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(804, 294, '2022-03-26 13:36:20', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(805, 294, '2022-03-26 13:36:20', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(806, 293, '2022-03-27 21:17:32', 5, 'ลบเอกสาร', 'รัชชานนท์', NULL),
(807, 294, '2022-03-27 21:17:35', 5, 'ลบเอกสาร', 'รัชชานนท์', NULL),
(808, 295, '2022-03-27 21:17:35', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(809, 295, '2022-03-27 21:17:35', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(810, 295, '2022-03-27 21:17:35', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(811, 296, '2022-03-27 21:18:22', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(812, 296, '2022-03-27 21:18:22', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(813, 296, '2022-03-27 21:18:22', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(814, 297, '2022-03-27 21:20:48', 0, 'เพิ่มเอกสาร', 'รัชชานนท์', NULL),
(815, 297, '2022-03-27 21:20:48', 1, 'ดูเอกสาร', 'รัชชานนท์', NULL),
(816, 297, '2022-03-27 21:20:48', 2, 'เพิ่มหน้า 1', 'รัชชานนท์', NULL),
(817, 274, '2022-03-27 21:45:49', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(818, 274, '2022-03-27 21:45:52', 2, 'เพิ่มหน้า 2', 'รัชชานนท์', NULL),
(819, 274, '2022-03-27 21:46:08', 3, 'แก้ไขหน้า 2', 'รัชชานนท์', NULL),
(820, 298, '2022-03-27 21:46:41', 9, 'คัดลอกเอกสารจากเอกสาร_2เป็นเอกสาร_2(คัดลอก)', 'รัชชานนท์', NULL),
(821, 299, '2022-03-27 21:47:21', 9, 'คัดลอกเอกสารจาก sdasd เป็น sdasd(คัดลอก)', 'รัชชานนท์', NULL),
(822, 273, '2022-03-27 22:50:21', 8, 'เปลี่ยนสถานะจาก 1 เป็น 3', 'รัชชานนท์', NULL),
(823, 273, '2022-03-27 23:01:33', 8, 'เปลี่ยนสถานะจาก ยกเลิก เป็น ต้นแบบ', 'รัชชานนท์', NULL),
(824, 273, '2022-03-27 23:04:02', 8, 'เปลี่ยนสถานะจาก ยกเลิก เป็น ใช้งาน', 'รัชชานนท์', NULL),
(825, 273, '2022-03-27 23:04:51', 8, 'เปลี่ยนสถานะจาก undefined เป็น ร่าง', 'รัชชานนท์', NULL),
(826, 273, '2022-03-27 23:24:26', 8, 'เปลี่ยนสถานะจาก ต้นแบบ เป็น ยกเลิก', 'รัชชานนท์', NULL),
(827, 273, '2022-03-27 23:24:37', 8, 'เปลี่ยนสถานะจาก ยกเลิก เป็น ร่าง', 'รัชชานนท์', NULL),
(828, 273, '2022-03-28 01:08:12', 3, 'แก้ไขหน้า 2', 'รัชชานนท์', NULL),
(829, 273, '2022-03-28 01:08:25', 3, 'แก้ไขหน้า 3', 'รัชชานนท์', NULL),
(830, 273, '2022-03-28 20:48:54', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(831, 273, '2022-03-28 20:49:13', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(832, 273, '2022-03-28 20:49:35', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(833, 273, '2022-03-28 20:58:32', 2, 'เพิ่มหน้า 5', 'รัชชานนท์', NULL),
(834, 273, '2022-03-28 23:16:29', 3, 'แก้ไขหน้า 3', 'รัชชานนท์', NULL),
(835, 273, '2022-03-28 23:28:34', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(836, 273, '2022-03-28 23:30:31', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(837, 273, '2022-03-28 23:30:44', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(838, 273, '2022-03-28 23:30:53', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(839, 273, '2022-03-28 23:35:34', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(840, 276, '2022-03-29 00:32:52', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(841, 276, '2022-03-29 00:35:17', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(842, 276, '2022-03-29 00:35:43', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(843, 276, '2022-03-29 00:37:00', 3, 'แก้ไขหน้า 1', 'รัชชานนท์', NULL),
(844, 297, '2022-03-29 00:37:22', 8, 'เปลี่ยนสถานะจาก ร่าง เป็น ใช้งาน', 'รัชชานนท์', NULL),
(845, 297, '2022-03-29 00:37:47', 8, 'เปลี่ยนสถานะจาก ใช้งาน เป็น ร่าง', 'รัชชานนท์', NULL),
(846, 299, '2022-03-29 00:37:53', 8, 'เปลี่ยนสถานะจาก ร่าง เป็น ใช้งาน', 'รัชชานนท์', NULL),
(847, 297, '2022-03-29 00:44:15', 8, 'เปลี่ยนสถานะจาก ร่าง เป็น ใช้งาน', 'รัชชานนท์', NULL),
(848, 273, '2022-03-29 17:48:45', 7, 'แชร์เอกสารไปที่ Email : huahomnoiz331@gmail.com', 'รัชชานนท์', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_document_page`
--

CREATE TABLE `doc_pdpa_document_page` (
  `page_id` int(11) NOT NULL,
  `doc_id` int(11) DEFAULT NULL,
  `page_number` int(11) DEFAULT NULL,
  `page_content` longtext DEFAULT NULL,
  `page_action` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_document_page`
--

INSERT INTO `doc_pdpa_document_page` (`page_id`, `doc_id`, `page_number`, `page_content`, `page_action`) VALUES
(510, 273, 1, '<h2 class=\"titre_de_page document\" style=\"margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 22.4px; line-height: 1.5em; font-family: inherit; vertical-align: baseline;\"><p class=\"MsoNormal\" style=\"text-align: center; margin-bottom: 12pt; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><span lang=\"TH\" style=\"font-size: 17pt; font-family: Tahoma, sans-serif;\">นโยบายความเป็นส่วนตัว</span><span style=\"font-size: 17pt; font-family: &quot;inherit&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#131418\">นโยบายความเป็นส่วนตัว</span></b><span lang=\"TH\" style=\"font-size:\r\n10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#343843\">หรือ</span><b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#131418\">นโยบายคุ้มครองข้อมูลส่วนบุคคล</span></b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">เป็นสัญญาอย่างหนึ่ง โดยมีคู่สัญญาฝ่ายหนึ่งคือ</span><b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#131418\">ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชัน</span></b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ซึ่งเป็นผู้เก็บรวบรวม ใช้\r\nเปิดเผยข้อมูลส่วนบุคคลของคู่สัญญาอีกฝ่ายหนึ่งซึ่งเรียกว่า</span><b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#131418\">ผู้ใช้งานเว็บไซต์หรือแอปพลิเคชัน</span></b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ซึ่งเข้ามาใช้บริการในเว็บไซต์หรือแอปพลิเคชันนั้น\r\nโดยเนื้อหาในสัญญาจะกล่าวถึงสิทธิและหน้าที่ของคู่สัญญาแต่ละฝ่ายเกี่ยวกับ</span><b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#131418\">การให้ข้อมูล การเก็บรวบรวมข้อมูล การใช้ข้อมูล\r\nรวมถึงการเผยแพร่ข้อมูลของผู้ใช้งาน</span></b><span lang=\"TH\" style=\"font-size:\r\n10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#343843\">เว็บไซต์หรือแอปพลิเคชัน</span><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">โดยที่</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">ข้อมูลส่วนบุคคลของผู้ใช้งาน</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ได้แก่ ข้อมูลที่</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">สามารถระบุตัวบุคคลซึ่งเป็นเจ้าของข้อมูลนั้นได้</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ไม่ว่าจะเป็น</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">ข้อมูลส่วนบุคคลทั่วไป</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">เช่น ชื่อ นามสกุล ที่อยู่ วันเดือนปีเกิด เบอร์โทรศัพท์ อายุ\r\nวุฒิการศึกษา งานที่ทำ หรือ</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">ข้อมูลส่วนบุคคลที่มีความอ่อนไหว\r\n(</span></b><b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-bidi-font-family:Tahoma;color:#131418\">Sensitive Data)</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">เช่น เชื้อชาติ เผ่าพันธ์ุ ความคิดเห็นทางการเมือง ความเชื่อ\r\n(ลัทธิ ศาสนา ปรัชญา) พฤติกรรมทางเพศ ประวัติอาชญากรรม สุขภาพ ความพิการ พันธุกรรม\r\nข้อมูลชีวภาพ ข้อมูลภาพจำลองใบหน้า ม่านตา หรือลายนิ้วมือ สหภาพแรงงานของผู้ใช้งาน</span><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ทั้งนี้ ข้อกำหนดเงื่อนไขต่างๆ\r\nที่กำหนดในนโยบายคุ้มครองข้อมูลส่วนบุคคล (เช่น\r\nการได้รับความยินยอมจากผู้ใช้งานในการเก็บรวบรวม ใช้\r\nและ/หรือเผยแพร่ข้อมูลส่วนบุคคลของผู้ใช้งาน)</span><span style=\"font-size:10.5pt;\r\nfont-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:\r\nTahoma;color:#343843\">&nbsp;</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">จะต้องสอดคล้องและไม่ขัดต่อกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ในทางกลับกันอาจกล่าวได้ว่าผู้ให้บริการหรือเจ้าของมักจัดทำนโยบายคุ้มครองข้อมูลส่วนบุคคลเพื่อให้การเก็บรวบรวม\r\nใช้ เปิดเผยข้อมูลนั้นถูกต้องและเป็นไปตามกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#131418\">ข้อมูล รวมถึงการเผยแพร่ข้อมูลของผู้ใช้งาน</span></b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">เว็บไซต์หรือแอปพลิเคชัน</span><span style=\"font-size:10.5pt;\r\nfont-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:\r\nTahoma;color:#343843\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">โดยที่</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">ข้อมูลส่วนบุคคลของผู้ใช้งาน</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ได้แก่ ข้อมูลที่</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">สามารถระบุตัวบุคคลซึ่งเป็นเจ้าของข้อมูลนั้นได้</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ไม่ว่าจะเป็น</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">ข้อมูลส่วนบุคคลทั่วไป</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">เช่น ชื่อ นามสกุล ที่อยู่ วันเดือนปีเกิด เบอร์โทรศัพท์ อายุ\r\nวุฒิการศึกษา งานที่ทำ หรือ</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">ข้อมูลส่วนบุคคลที่มีความอ่อนไหว\r\n(</span></b><b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-bidi-font-family:Tahoma;color:#131418\">Sensitive Data)</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">เช่น เชื้อชาติ เผ่าพันธ์ุ ความคิดเห็นทางการเมือง ความเชื่อ\r\n(ลัทธิ ศาสนา ปรัชญา) พฤติกรรมทางเพศ ประวัติอาชญากรรม สุขภาพ ความพิการ พันธุกรรม\r\nข้อมูลชีวภาพ ข้อมูลภาพจำลองใบหน้า ม่านตา หรือลายนิ้วมือ สหภาพแรงงานของผู้ใช้งาน</span><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ทั้งนี้ ข้อกำหนดเงื่อนไขต่างๆ\r\nที่กำหนดในนโยบายคุ้มครองข้อมูลส่วนบุคคล (เช่น\r\nการได้รับความยินยอมจากผู้ใช้งานในการเก็บรวบรวม ใช้\r\nและ/หรือเผยแพร่ข้อมูลส่วนบุคคลของผู้ใช้งาน)</span><span style=\"font-size:10.5pt;\r\nfont-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:\r\nTahoma;color:#343843\">&nbsp;</span><b><span lang=\"TH\" style=\"font-size:10.5pt;\r\nfont-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">จะต้องสอดคล้องและไม่ขัดต่อกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span></b><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\">&nbsp;</span><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">ในทางกลับกันอาจกล่าวได้ว่าผู้ให้บริการหรือเจ้าของมักจัดทำนโยบายคุ้มครองข้อมูลส่วนบุคคลเพื่อให้การเก็บรวบรวม\r\nใช้ เปิดเผยข้อมูลนั้นถูกต้องและเป็นไปตามกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#131418\">ข้อมูล รวมถึงการเผยแพร่ข้อมูลของผู้ใช้งาน</span></b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">เว็บไซต์หรือแอปพลิเคชัน</span><span style=\"font-size:10.5pt;\r\nfont-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:\r\nTahoma;color:#343843\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#131418\">ข้อมูล รวมถึงการเผยแพร่ข้อมูลของผู้ใช้งาน</span></b><span lang=\"TH\" style=\"font-size:10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:\r\nRoboto;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;\r\ncolor:#343843\">เว็บไซต์หรือแอปพลิเคชัน</span><b><span lang=\"TH\" style=\"font-size:\r\n10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#131418\">ข้อมูล\r\nรวมถึงการเผยแพร่ข้อมูลของผู้ใช้งาน</span></b><span lang=\"TH\" style=\"font-size:\r\n10.5pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-ascii-font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-hansi-font-family:Roboto;color:#343843\">เว็บไซต์หรือแอปพลิเคชัน</span><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-font-family:Tahoma;color:#343843\"><o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 12pt; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" style=\"margin: 6pt 0in; line-height: 18pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; vertical-align: baseline;\"><span style=\"font-size:12.0pt;font-family:&quot;Tahoma&quot;,sans-serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;color:black\">#id_card_number&nbsp; #religion #mixcode&nbsp;\r\n#mixcodez</span><span style=\"font-size:10.5pt;font-family:Roboto;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;mso-bidi-font-family:Tahoma;color:#343843\"><o:p></o:p></span></p></h2>', NULL),
(511, 274, 1, '<p>วัตถุประสงค์\r\nบริ ษัทให้ความส าคัญกับการเปิ ดเผยข้อมูลตามหลักการก ากับดูแลกิจการที่ดีผ่านระบบข่าวของ\r\nตลาดหลักทรัพย์แห่งประเทศไทย(“ตลาดหลักทรัพย์”) และการรายงานต่อส านักงานคณะกรรมการก ากับ\r\nหลักทรัพย์และตลาดหลักทรัพย์(“ส านักงาน ก.ล.ต.”) รวมถึงหน่วยงานที่เกี่ยวข้อง ตลอดจนการน าเสนอ\r\nผ่านสื่อเว็บไซต์ของบริษัท เพื่อให้ผู้ถือหุ้น นักลงทุน และผู้มีส่วนได้เสียทุกภาคส่วน ได้รับข้อมูลที่ถูกต้อง \r\nอย่างเท่าเทียมกัน เพื่อให้เชื่อมั่นได้ว่าข้อมูลที่ได้รับน้ัน ถูกต้อง โปร่งใส มีรายละเอียดเพียงพอ ทันต่อ\r\nเหตุการณ์และสอดคล้องกับกฎหมาย จึงได้ก าหนดแนวทางการเปิ ดเผยและใช้ข้อมูลภายในของบริษัท\r\nเป็ นลายลักษณ์อักษรภายใต้ “นโยบายการเปิดเผยข้อมูล” (Disclosure Information Policy)\r\nขอบเขต\r\nการเปิ ดเผยข้อมูลจะต้องค านึงถึงความถูกต้อง ครบถ้วน โปร่งใส และทันต่อเหตุการณ์ เพื่อให้ผู้ถือหุ้น ผู้มีส่วน\r\nไดเ้สีย ตลอดจนบุคคลทวั่ ไปไดร้ับทราบขอ้ มูลอย่างเท่าเทียมกัน โดยมีรายละเอียดการเปิ ดเผยข้อมูลและ\r\nความโปร่งใส ดงัน้ี\r\n(1) ประเภทข้อมูลและสารสนเทศ\r\n- ขอ้มูลทวั่ ไปของบริษัท\r\n- ข้อมูลและรายงานทางการเงิน \r\n- ข้อมูลที่ต้องเปิ ดเผยตามรอบระยะเวลา\r\n- ข้อมูลตามเหตุการณ์ที่ส าคัญ หรือข้อมูลที่มีผลกระทบต่อราคาหลักทรัพย์ของบริ ษัทตาม\r\nหลักเกณฑ์ของส านักงาน ก.ล.ต. และตลาดหลักทรัพย์ ซึ่ งมีผลกระทบต่อกระบวนการ\r\nตัดสินใจของผู้ลงทุนและผู้มีส่วนได้เสียของบริษัท \r\n(2) การจัดท าและน าส่งรายงานทางการเงิน\r\nคณะกรรมการบริษัทตระหนักถึงความรับผิดชอบต่อรายงานงบการเงินที่มีข้อมูลถูกต้อง ครบถ้วน \r\nเป็ นจริง สมเหตุสมผล และโปร่งใส ซึ่ งสามารถป้องกันการทุจริตและตรวจสอบการด าเนินการ\r\nที่ผิดปกติและรักษาผลประโยชน์ของผู้ถือหุ้นรายย่อย โดยมีเกณฑ์การจัดท าและน าส่งรายงาน\r\nทางการเงิน ดังน้ี\r\n1. ก าหนดให้งบการเงินของบริษทัจดั ทา ข้ึนตามมาตรฐานการบญั ชีที่รับรองทวั่ ไปในประเทศไทย\r\nและปฏิบัติถูกต้องตามกฎหมายและประกาศที่เกี่ยวข้อง โดยใช้นโยบายบัญชีที่เหมาะสมและ<br></p>', NULL),
(512, 275, 1, NULL, NULL),
(513, 276, 1, '<p style=\"margin-right: 0px; margin-bottom: 25px; margin-left: 0px; color: rgb(83, 86, 88); font-family: Thonburi, tahoma, Arial, sans-serif; font-size: 14px; background-image: initial !important; background-position: 0px 0px !important; background-size: initial !important; background-repeat: initial !important; background-attachment: initial !important; background-origin: initial !important; background-clip: initial !important;\"><br></p>', NULL),
(514, 277, 1, NULL, NULL),
(524, 273, 2, '<p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\"><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การนำไปใช้&nbsp; &nbsp;</span></p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ในการจัดทำนโยบายความเป็นส่วนตัวหรือนโยบายคุ้มครองข้อมูลส่วนบุคคล ผู้จัดทำควรพิจารณาดังต่อไปนี้</p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ระบุ&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">สิทธิ หน้าที่ และความรับผิดชอบต่างๆ ระหว่างผู้ให้บริการหรือเจ้าของกับผู้ใช้งาน</span>เว็บไซต์หรือแอปพลิเคชัน รวมถึงข้อมูลต่างๆ เกี่ยวกับความเป็นส่วนตัวที่ผู้ใช้งานควรทราบ เช่น&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ข้อมูลของผู้ให้บริการหรือเจ้าของ</span>เว็บไซต์หรือแอปพลิเคชัน&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">สถานที่ตั้ง</span>&nbsp;ช่องทางการ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ติดต่อสอบถาม การร้องเรียน</span>&nbsp;และแจ้งปัญหา&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การโฆษณาประชาสัมพันธ์</span>&nbsp;การ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ควบคุมการใช้งานโดยผู้ปกครอง</span>&nbsp;(Parental Control)&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การใช้คุกกี้</span>&nbsp;(Cookies) เป็นต้น</p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันควรจัด<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ให้ผู้ใช้งานสามารถเข้าถึงนโยบายความเป็นส่วนตัวนี้ได้โดยง่ายและเปิดเผย</span>ไว้ที่หน้าแรกของเว็บไซต์หรือแอปพลิเคชัน นอกจากนี้ยังควรให้ผู้ใช้งาน<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">อ่านและตอบยอมรับ</span>นโยบายความเป็นส่วนตัวของเว็บไซต์หรือแอปพลิเคชัน<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ก่อนการใช้งานครั้งแรก</span>ด้วยเพื่อเป็นการแสดงถึงความยินยอมและความตกลงที่จะยอมรับปฏิบัติตามเงื่อนไขของนโยบายความเป็นส่วนตัวโดยชัดแจ้ง</p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ในกรณีที่ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันต้องการกำหนดเงื่อนไขการใช้งานเว็บไซต์หรือแอปพลิเคชันโดยละเอียด เช่น&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ระเบียบ ข้อกำจัดการใช้งาน เงื่อนไขการเป็นสมาชิก เงื่อนไขการขาย หรือให้บริการ</span>ผ่านเว็บไซต์หรือแอปพลิเคชัน ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชัน<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">อาจจัดให้มี<a href=\"https://www.wonder.legal/th/modele/%E0%B8%82%E0%B9%89%E0%B8%AD%E0%B8%81%E0%B8%B3%E0%B8%AB%E0%B8%99%E0%B8%94%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B9%80%E0%B8%87%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%99%E0%B9%84%E0%B8%82%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B9%80%E0%B8%A7%E0%B9%87%E0%B8%9A%E0%B9%84%E0%B8%8B%E0%B8%95%E0%B9%8C-%E0%B9%81%E0%B8%AD%E0%B8%9B%E0%B8%9E%E0%B8%A5%E0%B8%B4%E0%B9%80%E0%B8%84%E0%B8%8A%E0%B8%B1%E0%B8%99\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">ข้อกำหนดและเงื่อนไขการใช้เว็บไซต์/แอปพลิเคชัน</a>&nbsp;ควบคู่ไปพร้อมกัน</span>กับนโยบายความเป็นส่วนตัวนี้ด้วยก็ได้ ในกรณีนี้ข้อกำหนดและเงื่อนไขการใช้เว็บไซต์/แอปพลิเคชันดังกล่าวควรจะสอดคล้องและไม่มีข้อความหรือเงื่อนไขที่ขัดแย้งกัน</p><div> #id_card_number&nbsp; #id_card_number <br></div>', NULL),
(525, 273, 3, '<p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\"><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ข้อควรคำนึงตามกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span></p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ในกรณีที่ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันมี<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การเก็บรวบรวม ใช้ หรือเผยแพร่ข้อมูลส่วนบุคคลของผู้ใช้งาน</span><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ตามกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span>&nbsp;ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันซึ่งเก็บรวบรวม ใช้ และ/หรือเผยแพร่ข้อมูลส่วนบุคลลจะ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ถือว่าเป็น ผู้ควบคุมข้อมูล&nbsp;</span>ซึ่งจะ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">มีหน้าที่ตามกฎหมาย</span>ดังกล่าว เช่น</p><ul style=\"margin: 0.5em 0px 0.5em 2em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; list-style: none; color: rgb(52, 56, 67); text-align: start;\"><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">ผู้ควบคุมข้อมูล<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ต้องได้รับความยินยอมอย่างชัดแจ้งจากผู้ใช้งาน</span>เว็บไซต์หรือแอปพลิเคชันซึ่งเป็นเจ้าของข้อมูลนั้นก่อนจะเก็บรวบรวม ใช้ หรือเผยแพร่ข้อมูลดังกล่าว โดยความยินยอมนั้นจะ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ต้องระบุข้อมูลสำคัญในการขอความยินยอมนั้นด้ว</span>ย เช่น&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">วัตถุประสงค์</span>การนำข้อมูลไปใช้&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ระยะเวลา</span>ในการเก็บรวบรวมข้อมูล&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">หน่วยงานที่อาจได้รับการเปิดเผยข้อมูล</span>&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">สิทธิของเจ้าของข้อมูล</span>ที่มีต่อผู้ให้บริการเกี่ยวกับข้อมูลส่วนบุคคลที่เก็บรวบรวม ทั้งนี้ ในการให้<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ความยินยอมของบุคคลที่มีความบกพร่องทางความสามารถ</span>&nbsp;(เช่น ผู้เยาว์ คนไร้ความสามารถ หรือคนเสมือนไร้ความสามารถ)&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ผู้ใช้อำนาจปกครองบุคคลเหล่านั้นจะต้องเป็นผู้ให้ความยินยอม</span>แทนผู้ใช้งานผู้เป็นเจ้าของข้อมูลด้วย เช่น ผู้ปกครอง ผู้อนุบาล หรือผู้พิทักษ์แล้วแต่กรณีด้วย</li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">ผู้ควบคุมข้อมูลต้องดำเนินการซึ่งเป็นหน้าที่ต่างๆ ของผู้ควบคุมข้อมูลที่สอดคล้องกับกฎหมายคุ้มครองข้อมูลส่วนบุคคล เช่น จัด<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ให้ผู้ใช้งานเจ้าของข้อมูลสามารถถอนความยินยอมได้ง่าย</span>และได้รับข้อมูลเพียงพอในการถอนความยินยอม&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การแจ้งข้อมูลผลกระทบสำคัญต่อเจ้าของข้อมูล</span>&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">จัดให้มีวิธี มาตรการ มาตรฐาน หรือระบบเพื่อรักษาความมั่นคงปลอดภัยของข้อมูลที่เหมาะสม</span>&nbsp;จัดให้มี<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การแก้ไขปรับปรุงข้อมูลให้เป็นปัจจุบันและ/หรือที่ถูกต้อง</span>&nbsp;ลบ ทำลายข้อมูลที่เกินระยะเวลาเก็บรวบรวมหรือที่ไม่เกี่ยวข้อง&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">บันทึกรายการสำคัญเกี่ยวกับการจัดเก็บ การใช้ หรือการเปิดเผย</span>ข้อมูลเพื่อการตรวจสอบจากผู้ใช้งานเจ้าของข้อมูลหรือจากหน่วยงานของรัฐ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">&nbsp;จัดให้มีเจ้าหน้าที่คุ้มครองข้อมูลส่วนบุคคล</span>เพื่อปฏิบัติหน้าที่ตามที่กฎหมายกำหนดในกรณีที่มีการดำเนินกิจกรรมที่เข้าเงื่อนไขของกฎหมายคุ้มครองข้อมูลส่วนบุคคล และหน้าที่อื่นๆ ตามกฎหมายดังกล่าว</li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\"><span style=\"background-color: transparent; font-family: inherit; font-size: inherit; font-style: inherit; font-variant-ligatures: inherit; font-variant-caps: inherit; font-weight: inherit;\">&nbsp; &nbsp;#religion&nbsp; #religion</span><br></li></ul>', NULL),
(526, 273, 4, '<p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\"><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">กฎหมายที่เกี่ยวข้อง</span></p><ul style=\"margin: 0.5em 0px 0.5em 2em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; list-style: none; color: rgb(52, 56, 67); text-align: start;\"><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\"><a href=\"https://ictlawcenter.etda.or.th/laws/detail/DP-Act-2562\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">พระราชบัญญัติคุ้มครองข้อมูลส่วนบุคคล</a></li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">กฎหมายคุ้มครองข้อมูลส่วนบุคคลของสหภาพยุโรป หรือ&nbsp;<a href=\"https://gdpr-info.eu/\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">European Union General Data Protection Regulation (GDPR)</a>&nbsp;ในกรณีที่<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">เว็บไซต์หรือแอปพลิเคชันมีการให้บริการหรือเสนอขายสินค้าแก่บุคคลธรรมดาซึ่งอยู่ในสหภาพยุโรปหรือมีการเก็บข้อมูลพฤติกรรมของผู้ใช้งานซึ่งอยู่ในสหภาพยุโรป</span></li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\"><a href=\"http://web.krisdika.go.th/data/law/law2/%c763/%c763-20-9999-update.pdf\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">พระราชบัญญัติว่าด้วยธุรกรรมทางอิเล็กทรอนิกส์</a></li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันอาจใช้แนวปฏิบัติที่ปรากฏใน<a href=\"http://web.krisdika.go.th/data/law/law2/%c763/%c763-2e-2553-a0003.pdf\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">ประกาศคณะกรรมการธุรกรรมทางอิเล็กทรอนิกส์ เรื่อง แนวนโยบายและแนวปฏิบัติในการคุ้มครองข้อมูลส่วนบุคคลของหน่วยงานของรัฐ</a>&nbsp;เป็นแนวทางเบื้องต้นในการพิจารณา</li></ul>', NULL),
(527, 278, 1, NULL, NULL),
(528, 279, 1, NULL, NULL),
(529, 280, 1, NULL, NULL),
(530, 281, 1, NULL, NULL),
(531, 282, 1, NULL, NULL),
(532, 283, 1, NULL, NULL),
(533, 289, 1, NULL, NULL),
(534, 290, 1, NULL, NULL),
(535, 291, 1, NULL, NULL),
(537, 292, 1, NULL, NULL),
(538, 293, 1, NULL, NULL),
(539, 294, 1, NULL, NULL),
(540, 295, 1, NULL, NULL),
(541, 296, 1, NULL, NULL),
(542, 297, 1, '<h2 class=\"titre_de_page document\" style=\"margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 22.4px; line-height: 1.5em; font-family: inherit; vertical-align: baseline;\">นโยบายความเป็นส่วนตัว</h2><div id=\"description\" style=\"margin: 0px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline;\"><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\"><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">นโยบายความเป็นส่วนตัว</span>หรือ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">นโยบายคุ้มครองข้อมูลส่วนบุคคล</span>เป็นสัญญาอย่างหนึ่ง โดยมีคู่สัญญาฝ่ายหนึ่งคือ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชัน</span>ซึ่งเป็นผู้เก็บรวบรวม ใช้ เปิดเผยข้อมูลส่วนบุคคลของคู่สัญญาอีกฝ่ายหนึ่งซึ่งเรียกว่า<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ผู้ใช้งานเว็บไซต์หรือแอปพลิเคชัน</span>ซึ่งเข้ามาใช้บริการในเว็บไซต์หรือแอปพลิเคชันนั้น โดยเนื้อหาในสัญญาจะกล่าวถึงสิทธิและหน้าที่ของคู่สัญญาแต่ละฝ่ายเกี่ยวกับ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การให้ข้อมูล การเก็บรวบรวมข้อมูล การใช้ข้อมูล รวมถึงการเผยแพร่ข้อมูลของผู้ใช้งาน</span>เว็บไซต์หรือแอปพลิเคชัน</p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">โดยที่<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ข้อมูลส่วนบุคคลของผู้ใช้งาน</span>&nbsp;ได้แก่ ข้อมูลที่<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">สามารถระบุตัวบุคคลซึ่งเป็นเจ้าของข้อมูลนั้นได้</span>&nbsp;ไม่ว่าจะเป็น<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ข้อมูลส่วนบุคคลทั่วไป</span>&nbsp;เช่น ชื่อ นามสกุล ที่อยู่ วันเดือนปีเกิด เบอร์โทรศัพท์ อายุ วุฒิการศึกษา งานที่ทำ หรือ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ข้อมูลส่วนบุคคลที่มีความอ่อนไหว (Sensitive Data)</span>&nbsp;เช่น เชื้อชาติ เผ่าพันธ์ุ ความคิดเห็นทางการเมือง ความเชื่อ (ลัทธิ ศาสนา ปรัชญา) พฤติกรรมทางเพศ ประวัติอาชญากรรม สุขภาพ ความพิการ พันธุกรรม ข้อมูลชีวภาพ ข้อมูลภาพจำลองใบหน้า ม่านตา หรือลายนิ้วมือ สหภาพแรงงานของผู้ใช้งาน</p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ทั้งนี้ ข้อกำหนดเงื่อนไขต่างๆ ที่กำหนดในนโยบายคุ้มครองข้อมูลส่วนบุคคล (เช่น การได้รับความยินยอมจากผู้ใช้งานในการเก็บรวบรวม ใช้ และ/หรือเผยแพร่ข้อมูลส่วนบุคคลของผู้ใช้งาน)&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">จะต้องสอดคล้องและไม่ขัดต่อกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span>&nbsp;ในทางกลับกันอาจกล่าวได้ว่าผู้ให้บริการหรือเจ้าของมักจัดทำนโยบายคุ้มครองข้อมูลส่วนบุคคลเพื่อให้การเก็บรวบรวม ใช้ เปิดเผยข้อมูลนั้นถูกต้องและเป็นไปตามกฎหมายคุ้มครองข้อมูลส่วนบุคคล</p><div> #id_card_number&nbsp; #religion  #mixcode&nbsp; #mixcodez <br></div><div><br></div></div>', NULL),
(543, 297, 2, '<p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\"><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การนำไปใช้</span></p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ในการจัดทำนโยบายความเป็นส่วนตัวหรือนโยบายคุ้มครองข้อมูลส่วนบุคคล ผู้จัดทำควรพิจารณาดังต่อไปนี้</p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ระบุ&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">สิทธิ หน้าที่ และความรับผิดชอบต่างๆ ระหว่างผู้ให้บริการหรือเจ้าของกับผู้ใช้งาน</span>เว็บไซต์หรือแอปพลิเคชัน รวมถึงข้อมูลต่างๆ เกี่ยวกับความเป็นส่วนตัวที่ผู้ใช้งานควรทราบ เช่น&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ข้อมูลของผู้ให้บริการหรือเจ้าของ</span>เว็บไซต์หรือแอปพลิเคชัน&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">สถานที่ตั้ง</span>&nbsp;ช่องทางการ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ติดต่อสอบถาม การร้องเรียน</span>&nbsp;และแจ้งปัญหา&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การโฆษณาประชาสัมพันธ์</span>&nbsp;การ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ควบคุมการใช้งานโดยผู้ปกครอง</span>&nbsp;(Parental Control)&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การใช้คุกกี้</span>&nbsp;(Cookies) เป็นต้น</p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันควรจัด<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ให้ผู้ใช้งานสามารถเข้าถึงนโยบายความเป็นส่วนตัวนี้ได้โดยง่ายและเปิดเผย</span>ไว้ที่หน้าแรกของเว็บไซต์หรือแอปพลิเคชัน นอกจากนี้ยังควรให้ผู้ใช้งาน<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">อ่านและตอบยอมรับ</span>นโยบายความเป็นส่วนตัวของเว็บไซต์หรือแอปพลิเคชัน<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ก่อนการใช้งานครั้งแรก</span>ด้วยเพื่อเป็นการแสดงถึงความยินยอมและความตกลงที่จะยอมรับปฏิบัติตามเงื่อนไขของนโยบายความเป็นส่วนตัวโดยชัดแจ้ง</p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ในกรณีที่ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันต้องการกำหนดเงื่อนไขการใช้งานเว็บไซต์หรือแอปพลิเคชันโดยละเอียด เช่น&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ระเบียบ ข้อกำจัดการใช้งาน เงื่อนไขการเป็นสมาชิก เงื่อนไขการขาย หรือให้บริการ</span>ผ่านเว็บไซต์หรือแอปพลิเคชัน ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชัน<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">อาจจัดให้มี<a href=\"https://www.wonder.legal/th/modele/%E0%B8%82%E0%B9%89%E0%B8%AD%E0%B8%81%E0%B8%B3%E0%B8%AB%E0%B8%99%E0%B8%94%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B9%80%E0%B8%87%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%99%E0%B9%84%E0%B8%82%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B9%80%E0%B8%A7%E0%B9%87%E0%B8%9A%E0%B9%84%E0%B8%8B%E0%B8%95%E0%B9%8C-%E0%B9%81%E0%B8%AD%E0%B8%9B%E0%B8%9E%E0%B8%A5%E0%B8%B4%E0%B9%80%E0%B8%84%E0%B8%8A%E0%B8%B1%E0%B8%99\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">ข้อกำหนดและเงื่อนไขการใช้เว็บไซต์/แอปพลิเคชัน</a>&nbsp;ควบคู่ไปพร้อมกัน</span>กับนโยบายความเป็นส่วนตัวนี้ด้วยก็ได้ ในกรณีนี้ข้อกำหนดและเงื่อนไขการใช้เว็บไซต์/แอปพลิเคชันดังกล่าวควรจะสอดคล้องและไม่มีข้อความหรือเงื่อนไขที่ขัดแย้งกัน</p><div><br></div>', NULL);
INSERT INTO `doc_pdpa_document_page` (`page_id`, `doc_id`, `page_number`, `page_content`, `page_action`) VALUES
(544, 297, 3, '<p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\"><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ข้อควรคำนึงตามกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span></p><p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\">ในกรณีที่ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันมี<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การเก็บรวบรวม ใช้ หรือเผยแพร่ข้อมูลส่วนบุคคลของผู้ใช้งาน</span><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ตามกฎหมายคุ้มครองข้อมูลส่วนบุคคล</span>&nbsp;ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันซึ่งเก็บรวบรวม ใช้ และ/หรือเผยแพร่ข้อมูลส่วนบุคลลจะ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ถือว่าเป็น ผู้ควบคุมข้อมูล&nbsp;</span>ซึ่งจะ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">มีหน้าที่ตามกฎหมาย</span>ดังกล่าว เช่น</p><ul style=\"margin: 0.5em 0px 0.5em 2em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; list-style: none; color: rgb(52, 56, 67); text-align: start;\"><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">ผู้ควบคุมข้อมูล<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ต้องได้รับความยินยอมอย่างชัดแจ้งจากผู้ใช้งาน</span>เว็บไซต์หรือแอปพลิเคชันซึ่งเป็นเจ้าของข้อมูลนั้นก่อนจะเก็บรวบรวม ใช้ หรือเผยแพร่ข้อมูลดังกล่าว โดยความยินยอมนั้นจะ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ต้องระบุข้อมูลสำคัญในการขอความยินยอมนั้นด้ว</span>ย เช่น&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">วัตถุประสงค์</span>การนำข้อมูลไปใช้&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ระยะเวลา</span>ในการเก็บรวบรวมข้อมูล&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">หน่วยงานที่อาจได้รับการเปิดเผยข้อมูล</span>&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">สิทธิของเจ้าของข้อมูล</span>ที่มีต่อผู้ให้บริการเกี่ยวกับข้อมูลส่วนบุคคลที่เก็บรวบรวม ทั้งนี้ ในการให้<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ความยินยอมของบุคคลที่มีความบกพร่องทางความสามารถ</span>&nbsp;(เช่น ผู้เยาว์ คนไร้ความสามารถ หรือคนเสมือนไร้ความสามารถ)&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ผู้ใช้อำนาจปกครองบุคคลเหล่านั้นจะต้องเป็นผู้ให้ความยินยอม</span>แทนผู้ใช้งานผู้เป็นเจ้าของข้อมูลด้วย เช่น ผู้ปกครอง ผู้อนุบาล หรือผู้พิทักษ์แล้วแต่กรณีด้วย</li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">ผู้ควบคุมข้อมูลต้องดำเนินการซึ่งเป็นหน้าที่ต่างๆ ของผู้ควบคุมข้อมูลที่สอดคล้องกับกฎหมายคุ้มครองข้อมูลส่วนบุคคล เช่น จัด<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">ให้ผู้ใช้งานเจ้าของข้อมูลสามารถถอนความยินยอมได้ง่าย</span>และได้รับข้อมูลเพียงพอในการถอนความยินยอม&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การแจ้งข้อมูลผลกระทบสำคัญต่อเจ้าของข้อมูล</span>&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">จัดให้มีวิธี มาตรการ มาตรฐาน หรือระบบเพื่อรักษาความมั่นคงปลอดภัยของข้อมูลที่เหมาะสม</span>&nbsp;จัดให้มี<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การแก้ไขปรับปรุงข้อมูลให้เป็นปัจจุบันและ/หรือที่ถูกต้อง</span>&nbsp;ลบ ทำลายข้อมูลที่เกินระยะเวลาเก็บรวบรวมหรือที่ไม่เกี่ยวข้อง&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">บันทึกรายการสำคัญเกี่ยวกับการจัดเก็บ การใช้ หรือการเปิดเผย</span>ข้อมูลเพื่อการตรวจสอบจากผู้ใช้งานเจ้าของข้อมูลหรือจากหน่วยงานของรัฐ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">&nbsp;จัดให้มีเจ้าหน้าที่คุ้มครองข้อมูลส่วนบุคคล</span>เพื่อปฏิบัติหน้าที่ตามที่กฎหมายกำหนดในกรณีที่มีการดำเนินกิจกรรมที่เข้าเงื่อนไขของกฎหมายคุ้มครองข้อมูลส่วนบุคคล และหน้าที่อื่นๆ ตามกฎหมายดังกล่าว</li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">ผู้ควบคุมข้อมูลต้องจัดให้มีการดำเนินการและ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">มีมาตรการของพนักงานผู้ที่มีส่วนเกี่ยวข้องกับการเก็บรวบรวม ใช้ หรือเผยแพร่ข้อมูลเพื่อให้มีการดำเนินการที่สอดคล้องและเป็นไปตามกฎหมาย</span>คุ้มครองข้อมูลส่วนบุคคลด้วย</li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">ในกรณีที่มีการให้ข้อมูลแก่บุคคลอื่น เช่น ผู้ประมวลผลข้อมูลหรือบุคคลภายนอก จะต้อ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">งมีมาตรการควบคุมไม่ให้บุคคลอื่นนั้นใช้ข้อมูลที่ไม่เป็นไปตามกฎหมายด้วย</span>&nbsp;และในกรณีที่มี<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">การส่งข้อมูลออกไปนอกประเทศจะต้องมั่นใจว่าผู้รับข้อมูลนั้นมีมาตรการคุ้มครองความปลอดภัยของข้อมูลที่ไม่ด้อยไปกว่ามาตรฐานที่กำหนดในประเทศไทย</span></li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">อนึ่ง หากผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันไม่ปฏิบัติตามกฎหมายดังกล่าว อาจ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">มีโทษทั้งทางแพ่ง</span>&nbsp;(เช่น ชดใช้ค่าเสียหายและค่าเสียหายเชิงลงโทษแก่เจ้าของข้อมูลที่ได้รับความเสียหาย)&nbsp;<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">โทษทางอาญา</span>&nbsp;(เช่น จำคุก ปรับ) และ<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">โทษทางปกครอง</span>&nbsp;(เช่น สั่งให้ดำเนินการแก้ไข ตักเตือน หรือปรับ)</li></ul>', NULL),
(545, 297, 4, '<p style=\"margin: 0.5em 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; color: rgb(52, 56, 67);\"><span style=\"font-weight: 700; color: rgb(19, 20, 24);\">กฎหมายที่เกี่ยวข้อง</span></p><ul style=\"margin: 0.5em 0px 0.5em 2em; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 14px; line-height: 1.5em; font-family: Roboto, Arial, Helvetica, Verdana, Arial, sans-serif; vertical-align: baseline; list-style: none; color: rgb(52, 56, 67); text-align: start;\"><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\"><a href=\"https://ictlawcenter.etda.or.th/laws/detail/DP-Act-2562\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">พระราชบัญญัติคุ้มครองข้อมูลส่วนบุคคล</a></li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">กฎหมายคุ้มครองข้อมูลส่วนบุคคลของสหภาพยุโรป หรือ&nbsp;<a href=\"https://gdpr-info.eu/\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">European Union General Data Protection Regulation (GDPR)</a>&nbsp;ในกรณีที่<span style=\"font-weight: 700; color: rgb(19, 20, 24);\">เว็บไซต์หรือแอปพลิเคชันมีการให้บริการหรือเสนอขายสินค้าแก่บุคคลธรรมดาซึ่งอยู่ในสหภาพยุโรปหรือมีการเก็บข้อมูลพฤติกรรมของผู้ใช้งานซึ่งอยู่ในสหภาพยุโรป</span></li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\"><a href=\"http://web.krisdika.go.th/data/law/law2/%c763/%c763-20-9999-update.pdf\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">พระราชบัญญัติว่าด้วยธุรกรรมทางอิเล็กทรอนิกส์</a></li><li style=\"margin: 0px; padding: 0.3em 0px 0px 1em; border: 0px; font: inherit; vertical-align: baseline; background: url(&quot;../img/fleche_droite.png&quot;) 0px 12px no-repeat scroll transparent; list-style: outside none none;\">ผู้ให้บริการหรือเจ้าของเว็บไซต์หรือแอปพลิเคชันอาจใช้แนวปฏิบัติที่ปรากฏใน<a href=\"http://web.krisdika.go.th/data/law/law2/%c763/%c763-2e-2553-a0003.pdf\" style=\"margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(0, 0, 0); border-left-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; vertical-align: baseline; color: rgb(0, 0, 0); text-decoration: none; transition: all 0.1s ease 0s; cursor: pointer;\">ประกาศคณะกรรมการธุรกรรมทางอิเล็กทรอนิกส์ เรื่อง แนวนโยบายและแนวปฏิบัติในการคุ้มครองข้อมูลส่วนบุคคลของหน่วยงานของรัฐ</a>&nbsp;เป็นแนวทางเบื้องต้นในการพิจารณา</li></ul>', NULL),
(546, 274, 2, '<p>ถือปฏิบตัิอย่างสม่า เสมอและใช้ดุลยพินิจอย่างระมดัระวงั รวมท้งัมีการเปิดเผยขอ้ มูลอย่าง\r\nเพียงพอครบถ้วนในหมายเหตุประกอบงบการเงิน \r\n2. ก าหนดให้มีเกณฑ์การคัดเลือกผู้สอบบัญชีตามความรู้ความช านาญในวิชาชีพ ข้ันตอนการ\r\nปฏิบัติงานที่เป็ นมาตรฐาน และมีชื่อเสียงเป็ นที่ยอมรับในระดับสากล รวมถึงไม่มีความขัดแย้ง\r\nทางผลประโยชน์กับบริษัทที่จะส่งผลท าให้ขาดความเป็ นอิสระท าหน้าที่เป็ นผู้สอบบัญชีของ\r\nบริษัทเพื่อท าหน้าที่ตรวจสอบงบการเงินของบริษัท และมีการหมุนเวียนผู้สอบบัญชีเมื่อปฏิบัติ\r\nหน้าที่ครบ 5 ปี เพื่อเป็ นความโปร่งใสในการปฏิบัติหน้าที่อย่างอิสระและเป็ นไปตามหลักการ\r\nก ากับดูแลกิจการ \r\n3. ก าหนดให้มีการจัดท าค าอธิบายและการวิเคราะห์ของฝ่ ายจัดการ(“Management Discussion and \r\nAnalysis”) เพื่ออธิบายเชิงวิเคราะห์เกี่ยวกับฐานะการเงิน ผลการด าเนินงาน และการเปลี่ยนแปลง\r\nที่ส าคัญทางการเงินในช่วงไตรมาสที่ผ่านมาให้กับผู้ถือหุ้นและนักลงทุนทราบเป็ นรายไตรมาส\r\nทุกไตรมาส โดยเปิ ดเผยผ่านระบบข่าวของตลาดหลักทรัพย์พร้อมการน าส่งงบการเงิน และเปิ ดเผย\r\nบนเว็บไซต์ของบริษัท\r\nท้งัน้ีคณะกรรมการบริษัทมีการสอบทานระบบควบคุมภายในให้มีประสิทธิภาพ ผ่านการปฏิบัติงาน\r\nโดยคณะกรรมการตรวจสอบ เพื่อให้มนั่ ใจไดว้่ามีการควบคุมดูแลการดา เนินงานให้สามารถป้องกัน\r\nทรัพย์สินของบริ ษัทและบริ ษัทย่อย ไม่ให้เกิดการทุจริ ตหรื อการด าเนินการที่ผิดปกติอย่างมี\r\nสาระส าคัญ และรายงานทางการเงินของบริษัทมีความถูกต้องเชื่อถือได้\r\n(3) ช่องทางในการเปิ ดเผยข้อมูล\r\nบริษัทก าหนดให้การเปิ ดเผยผลการด าเนินงานตามรอบระยะเวลา และเหตุการณ์ส าคัญต่าง ๆ เป็ นไป\r\nตามหลักเกณฑ์ของส านักงานคณะกรรมการก ากับหลักทรัพย์และตลาดหลักทรัพย์ (“ส านักงาน\r\nก.ล.ต.”)และตลาดหลักทรัพย์แห่งประเทศไทย (“ตลาดหลักทรัพย์”) เพื่อให้การเปิ ดเผยข้อมูลโดย\r\nบริษัทมีประสิทธิภาพและผู้มีส่วนได้เสียสามารถเข้าถึงได้อย่างเท่าเทียม ในการน้ีบริษทั ได้มีการ\r\nปรับปรุงข้อมูลให้เป็ นปัจจุบันอย่างสม ่าเสมอเสมอผ่านช่องทางการเปิ ดเผยข้อมูลต่าง ๆ ดงัน้ี\r\n1. การรายงานผ่านระบบข่าวของตลาดหลักทรัพย์(www.set.or.th) ซึ่งจะใช้เป็ นช่องทางหลัก\r\nในการเปิ ดเผยข้อมูลของบริษัท&nbsp;&nbsp;<br></p>', NULL),
(547, 298, 1, '<p>วัตถุประสงค์\r\nบริ ษัทให้ความส าคัญกับการเปิ ดเผยข้อมูลตามหลักการก ากับดูแลกิจการที่ดีผ่านระบบข่าวของ\r\nตลาดหลักทรัพย์แห่งประเทศไทย(“ตลาดหลักทรัพย์”) และการรายงานต่อส านักงานคณะกรรมการก ากับ\r\nหลักทรัพย์และตลาดหลักทรัพย์(“ส านักงาน ก.ล.ต.”) รวมถึงหน่วยงานที่เกี่ยวข้อง ตลอดจนการน าเสนอ\r\nผ่านสื่อเว็บไซต์ของบริษัท เพื่อให้ผู้ถือหุ้น นักลงทุน และผู้มีส่วนได้เสียทุกภาคส่วน ได้รับข้อมูลที่ถูกต้อง \r\nอย่างเท่าเทียมกัน เพื่อให้เชื่อมั่นได้ว่าข้อมูลที่ได้รับน้ัน ถูกต้อง โปร่งใส มีรายละเอียดเพียงพอ ทันต่อ\r\nเหตุการณ์และสอดคล้องกับกฎหมาย จึงได้ก าหนดแนวทางการเปิ ดเผยและใช้ข้อมูลภายในของบริษัท\r\nเป็ นลายลักษณ์อักษรภายใต้ “นโยบายการเปิดเผยข้อมูล” (Disclosure Information Policy)\r\nขอบเขต\r\nการเปิ ดเผยข้อมูลจะต้องค านึงถึงความถูกต้อง ครบถ้วน โปร่งใส และทันต่อเหตุการณ์ เพื่อให้ผู้ถือหุ้น ผู้มีส่วน\r\nไดเ้สีย ตลอดจนบุคคลทวั่ ไปไดร้ับทราบขอ้ มูลอย่างเท่าเทียมกัน โดยมีรายละเอียดการเปิ ดเผยข้อมูลและ\r\nความโปร่งใส ดงัน้ี\r\n(1) ประเภทข้อมูลและสารสนเทศ\r\n- ขอ้มูลทวั่ ไปของบริษัท\r\n- ข้อมูลและรายงานทางการเงิน \r\n- ข้อมูลที่ต้องเปิ ดเผยตามรอบระยะเวลา\r\n- ข้อมูลตามเหตุการณ์ที่ส าคัญ หรือข้อมูลที่มีผลกระทบต่อราคาหลักทรัพย์ของบริ ษัทตาม\r\nหลักเกณฑ์ของส านักงาน ก.ล.ต. และตลาดหลักทรัพย์ ซึ่ งมีผลกระทบต่อกระบวนการ\r\nตัดสินใจของผู้ลงทุนและผู้มีส่วนได้เสียของบริษัท \r\n(2) การจัดท าและน าส่งรายงานทางการเงิน\r\nคณะกรรมการบริษัทตระหนักถึงความรับผิดชอบต่อรายงานงบการเงินที่มีข้อมูลถูกต้อง ครบถ้วน \r\nเป็ นจริง สมเหตุสมผล และโปร่งใส ซึ่ งสามารถป้องกันการทุจริตและตรวจสอบการด าเนินการ\r\nที่ผิดปกติและรักษาผลประโยชน์ของผู้ถือหุ้นรายย่อย โดยมีเกณฑ์การจัดท าและน าส่งรายงาน\r\nทางการเงิน ดังน้ี\r\n1. ก าหนดให้งบการเงินของบริษทัจดั ทา ข้ึนตามมาตรฐานการบญั ชีที่รับรองทวั่ ไปในประเทศไทย\r\nและปฏิบัติถูกต้องตามกฎหมายและประกาศที่เกี่ยวข้อง โดยใช้นโยบายบัญชีที่เหมาะสมและ<br></p>', NULL),
(548, 298, 2, '<p>ถือปฏิบตัิอย่างสม่า เสมอและใช้ดุลยพินิจอย่างระมดัระวงั รวมท้งัมีการเปิดเผยขอ้ มูลอย่าง\r\nเพียงพอครบถ้วนในหมายเหตุประกอบงบการเงิน \r\n2. ก าหนดให้มีเกณฑ์การคัดเลือกผู้สอบบัญชีตามความรู้ความช านาญในวิชาชีพ ข้ันตอนการ\r\nปฏิบัติงานที่เป็ นมาตรฐาน และมีชื่อเสียงเป็ นที่ยอมรับในระดับสากล รวมถึงไม่มีความขัดแย้ง\r\nทางผลประโยชน์กับบริษัทที่จะส่งผลท าให้ขาดความเป็ นอิสระท าหน้าที่เป็ นผู้สอบบัญชีของ\r\nบริษัทเพื่อท าหน้าที่ตรวจสอบงบการเงินของบริษัท และมีการหมุนเวียนผู้สอบบัญชีเมื่อปฏิบัติ\r\nหน้าที่ครบ 5 ปี เพื่อเป็ นความโปร่งใสในการปฏิบัติหน้าที่อย่างอิสระและเป็ นไปตามหลักการ\r\nก ากับดูแลกิจการ \r\n3. ก าหนดให้มีการจัดท าค าอธิบายและการวิเคราะห์ของฝ่ ายจัดการ(“Management Discussion and \r\nAnalysis”) เพื่ออธิบายเชิงวิเคราะห์เกี่ยวกับฐานะการเงิน ผลการด าเนินงาน และการเปลี่ยนแปลง\r\nที่ส าคัญทางการเงินในช่วงไตรมาสที่ผ่านมาให้กับผู้ถือหุ้นและนักลงทุนทราบเป็ นรายไตรมาส\r\nทุกไตรมาส โดยเปิ ดเผยผ่านระบบข่าวของตลาดหลักทรัพย์พร้อมการน าส่งงบการเงิน และเปิ ดเผย\r\nบนเว็บไซต์ของบริษัท\r\nท้งัน้ีคณะกรรมการบริษัทมีการสอบทานระบบควบคุมภายในให้มีประสิทธิภาพ ผ่านการปฏิบัติงาน\r\nโดยคณะกรรมการตรวจสอบ เพื่อให้มนั่ ใจไดว้่ามีการควบคุมดูแลการดา เนินงานให้สามารถป้องกัน\r\nทรัพย์สินของบริ ษัทและบริ ษัทย่อย ไม่ให้เกิดการทุจริ ตหรื อการด าเนินการที่ผิดปกติอย่างมี\r\nสาระส าคัญ และรายงานทางการเงินของบริษัทมีความถูกต้องเชื่อถือได้\r\n(3) ช่องทางในการเปิ ดเผยข้อมูล\r\nบริษัทก าหนดให้การเปิ ดเผยผลการด าเนินงานตามรอบระยะเวลา และเหตุการณ์ส าคัญต่าง ๆ เป็ นไป\r\nตามหลักเกณฑ์ของส านักงานคณะกรรมการก ากับหลักทรัพย์และตลาดหลักทรัพย์ (“ส านักงาน\r\nก.ล.ต.”)และตลาดหลักทรัพย์แห่งประเทศไทย (“ตลาดหลักทรัพย์”) เพื่อให้การเปิ ดเผยข้อมูลโดย\r\nบริษัทมีประสิทธิภาพและผู้มีส่วนได้เสียสามารถเข้าถึงได้อย่างเท่าเทียม ในการน้ีบริษทั ได้มีการ\r\nปรับปรุงข้อมูลให้เป็ นปัจจุบันอย่างสม ่าเสมอเสมอผ่านช่องทางการเปิ ดเผยข้อมูลต่าง ๆ ดงัน้ี\r\n1. การรายงานผ่านระบบข่าวของตลาดหลักทรัพย์(www.set.or.th) ซึ่งจะใช้เป็ นช่องทางหลัก\r\nในการเปิ ดเผยข้อมูลของบริษัท&nbsp;&nbsp;<br></p>', NULL),
(549, 299, 1, NULL, NULL),
(550, 273, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_document_type`
--

CREATE TABLE `doc_pdpa_document_type` (
  `doc_type_id` int(11) NOT NULL,
  `doc_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_document_type`
--

INSERT INTO `doc_pdpa_document_type` (`doc_type_id`, `doc_type_name`) VALUES
(152, 'คำประกาศความเป็นส่วนตัว'),
(153, 'นโยบายคุ้มครองความเป็นส่วนตัว'),
(154, 'bd'),
(155, 'bbb'),
(161, 'ccc'),
(162, 'bb'),
(163, 'c'),
(164, 'c'),
(165, 'cc'),
(167, 'qqqq'),
(169, 'qq'),
(170, 'eeee'),
(171, 'n'),
(172, 'nn'),
(173, 'nnn'),
(174, 'nnnn'),
(175, 'nnnnn'),
(176, 'm'),
(177, 'mm'),
(178, 'mmm'),
(179, 'mmmm'),
(180, '8888'),
(182, 'kk'),
(187, 'bbbb'),
(194, 'sdjfklsjlsajlfas'),
(199, 'aaaa'),
(200, 'aaa'),
(201, 'aaaa'),
(202, 'bb'),
(203, 'aa'),
(204, 'bbbb'),
(205, 'dsfjsak'),
(206, 'f'),
(207, 'g'),
(208, 'c'),
(209, 'e'),
(210, 'd'),
(211, 'aa'),
(213, 'คำยินยอม');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_event_process`
--

CREATE TABLE `doc_pdpa_event_process` (
  `event_process_id` int(11) NOT NULL,
  `event_process_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_event_process`
--

INSERT INTO `doc_pdpa_event_process` (`event_process_id`, `event_process_name`) VALUES
(1, 'ทำแบบสอบถาม'),
(2, 'เสนอราคา'),
(3, 'จองคอนโด'),
(4, 'ทำสัญญาจะซื้อจะขาย'),
(5, 'ออกใบเสร็จรับเงิน'),
(6, 'โอนกรรมสิทธิ์');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_file_dir`
--

CREATE TABLE `doc_pdpa_file_dir` (
  `id` int(11) NOT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `os_name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `name_file` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_file_dir`
--

INSERT INTO `doc_pdpa_file_dir` (`id`, `device_name`, `os_name`, `path`, `name_file`) VALUES
(1815, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.200,08:00,2022-01-19.log'),
(1816, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,01:00,2022-01-08.log'),
(1817, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,02:00,2022-01-13.log'),
(1818, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,06:00,2022-01-14.log'),
(1819, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,07:00,2022-01-14.log'),
(1820, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,12:00,2021-12.17.log'),
(1821, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,13:00,2021-12.17.log'),
(1822, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,14:00,2021-12.17.log'),
(1823, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,16:00,2021-12.17.log'),
(1824, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,21:00,2022-01-14.log'),
(1825, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,22:00,2022-01-14.log'),
(1826, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.223,22:00,2022-01-14.log'),
(1827, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.230,09:00,2021-12.17.log'),
(1828, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.233,04:00,2022-01-15.log'),
(1829, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.233,05:00,2022-01-15.log'),
(1830, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.233,06:00,2022-01-14.log'),
(1831, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.233,06:00,2022-01-15.log'),
(1832, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.4,00:00,2021-12-18.log'),
(1833, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.4,01:00,2021-01-08.log'),
(1834, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.4,12:00,2021-12-25.log'),
(1835, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.18.196.89,17:00,2022-01-21.log'),
(1836, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.18.196.89,18:00,2022-01-21.log'),
(1837, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Application.evtx'),
(1838, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'HardwareEvents.evtx'),
(1839, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Intel-GFX-Info%4Application.evtx'),
(1840, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Intel-GFX-Info%4System.evtx'),
(1841, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Internet Explorer.evtx'),
(1842, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Key Management Service.evtx'),
(1843, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Client-Licensing-Platform%4Admin.evtx'),
(1844, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-System-Diagnostics-DiagnosticInvoker%4Operational.evtx'),
(1845, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AAD%4Operational.evtx'),
(1846, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-All-User-Install-Agent%4Admin.evtx'),
(1847, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AllJoyn%4Operational.evtx'),
(1848, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppHost%4Admin.evtx'),
(1849, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppID%4Operational.evtx'),
(1850, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ApplicabilityEngine%4Operational.evtx'),
(1851, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application Server-Applications%4Admin.evtx'),
(1852, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application Server-Applications%4Operational.evtx'),
(1853, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Program-Compatibility-Assistant.evtx'),
(1854, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Program-Compatibility-Troubleshooter.evtx'),
(1855, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Program-Inventory.evtx'),
(1856, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Program-Telemetry.evtx'),
(1857, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Steps-Recorder.evtx'),
(1858, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppLocker%4EXE and DLL.evtx'),
(1859, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppLocker%4MSI and Script.evtx'),
(1860, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppLocker%4Packaged app-Deployment.evtx'),
(1861, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppLocker%4Packaged app-Execution.evtx'),
(1862, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppModel-Runtime%4Admin.evtx'),
(1863, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppReadiness%4Admin.evtx'),
(1864, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppReadiness%4Operational.evtx'),
(1865, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppXDeployment%4Operational.evtx'),
(1866, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppXDeploymentServer%4Operational.evtx'),
(1867, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppXDeploymentServer%4Restricted.evtx'),
(1868, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppxPackaging%4Operational.evtx'),
(1869, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Audio%4CaptureMonitor.evtx'),
(1870, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Audio%4Operational.evtx'),
(1871, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Audio%4PlaybackManager.evtx'),
(1872, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Authentication User Interface%4Operational.evtx'),
(1873, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-BackgroundTaskInfrastructure%4Operational.evtx'),
(1874, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Backup.evtx'),
(1875, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Biometrics%4Operational.evtx'),
(1876, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-BitLocker%4BitLocker Management.evtx'),
(1877, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Bits-Client%4Operational.evtx'),
(1878, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Bluetooth-BthLEPrepairing%4Operational.evtx'),
(1879, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Bluetooth-MTPEnum%4Operational.evtx'),
(1880, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CertificateServicesClient-Lifecycle-System%4Operational.evtx'),
(1881, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CertificateServicesClient-Lifecycle-User%4Operational.evtx'),
(1882, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Cleanmgr%4Diagnostic.evtx'),
(1883, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CloudStore%4Operational.evtx'),
(1884, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CodeIntegrity%4Operational.evtx'),
(1885, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Compat-Appraiser%4Operational.evtx'),
(1886, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Containers-BindFlt%4Operational.evtx'),
(1887, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Containers-Wcifs%4Operational.evtx'),
(1888, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CoreApplication%4Operational.evtx'),
(1889, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CoreSystem-SmsRouter-Events%4Operational.evtx'),
(1890, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CorruptedFileRecovery-Client%4Operational.evtx'),
(1891, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CorruptedFileRecovery-Server%4Operational.evtx'),
(1892, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Crypto-DPAPI%4BackUpKeySvc.evtx'),
(1893, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Crypto-DPAPI%4Operational.evtx'),
(1894, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Crypto-NCrypt%4Operational.evtx'),
(1895, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DAL-Provider%4Operational.evtx'),
(1896, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DataIntegrityScan%4Admin.evtx'),
(1897, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DataIntegrityScan%4CrashRecovery.evtx'),
(1898, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DateTimeControlPanel%4Operational.evtx'),
(1899, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceGuard%4Operational.evtx'),
(1900, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider%4Admin.evtx'),
(1901, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider%4Autopilot.evtx'),
(1902, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider%4Operational.evtx'),
(1903, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Devices-Background%4Operational.evtx'),
(1904, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceSetupManager%4Admin.evtx'),
(1905, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceSetupManager%4Operational.evtx'),
(1906, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceSync%4Operational.evtx'),
(1907, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceUpdateAgent%4Operational.evtx'),
(1908, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Dhcp-Client%4Admin.evtx'),
(1909, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Dhcpv6-Client%4Admin.evtx'),
(1910, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-DPS%4Operational.evtx'),
(1911, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-PCW%4Operational.evtx'),
(1912, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-PLA%4Operational.evtx'),
(1913, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-Scheduled%4Operational.evtx'),
(1914, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-Scripted%4Admin.evtx'),
(1915, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-Scripted%4Operational.evtx'),
(1916, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-ScriptedDiagnosticsProvider%4Operational.evtx'),
(1917, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnostics-Networking%4Operational.evtx'),
(1918, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnostics-Performance%4Operational.evtx'),
(1919, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DiskDiagnostic%4Operational.evtx'),
(1920, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DiskDiagnosticDataCollector%4Operational.evtx'),
(1921, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DiskDiagnosticResolver%4Operational.evtx'),
(1922, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DSC%4Admin.evtx'),
(1923, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DSC%4Operational.evtx'),
(1924, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DxgKrnl-Admin.evtx'),
(1925, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DxgKrnl-Operational.evtx'),
(1926, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapHost%4Operational.evtx'),
(1927, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapMethods-RasChap%4Operational.evtx'),
(1928, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapMethods-RasTls%4Operational.evtx'),
(1929, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapMethods-Sim%4Operational.evtx'),
(1930, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapMethods-Ttls%4Operational.evtx'),
(1931, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EDP-Application-Learning%4Admin.evtx'),
(1932, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EDP-Audit-Regular%4Admin.evtx'),
(1933, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EDP-Audit-TCB%4Admin.evtx'),
(1934, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EnhancedStorage-EhStorClass%4Operational.evtx'),
(1935, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EventCollector%4Operational.evtx'),
(1936, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Fault-Tolerant-Heap%4Operational.evtx'),
(1937, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-FeatureConfiguration%4Operational.evtx'),
(1938, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-FileHistory-Core%4WHC.evtx'),
(1939, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-FileHistory-Engine%4BackupLog.evtx'),
(1940, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-FMS%4Operational.evtx'),
(1941, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Folder Redirection%4Operational.evtx'),
(1942, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Forwarding%4Operational.evtx'),
(1943, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-GenericRoaming%4Admin.evtx'),
(1944, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-GroupPolicy%4Operational.evtx'),
(1945, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-HelloForBusiness%4Operational.evtx'),
(1946, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Help%4Operational.evtx'),
(1947, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-hidcfu%4Operational.evtx'),
(1948, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-HomeGroup Control Panel%4Operational.evtx'),
(1949, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Host-Network-Service-Admin.evtx'),
(1950, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Host-Network-Service-Operational.evtx'),
(1951, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-HotspotAuth%4Operational.evtx'),
(1952, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Compute-Admin.evtx'),
(1953, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Compute-Operational.evtx'),
(1954, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Guest-Drivers%4Admin.evtx'),
(1955, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Hypervisor-Admin.evtx'),
(1956, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Hypervisor-Operational.evtx'),
(1957, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-StorageVSP-Admin.evtx'),
(1958, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-VID-Admin.evtx'),
(1959, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-VmSwitch-Operational.evtx'),
(1960, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Worker-Admin.evtx'),
(1961, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Worker-Operational.evtx'),
(1962, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-IdCtrls%4Operational.evtx'),
(1963, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-IKE%4Operational.evtx'),
(1964, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-International-RegionalOptionsControlPanel%4Operational.evtx'),
(1965, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Iphlpsvc%4Operational.evtx'),
(1966, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-IPxlatCfg%4Operational.evtx'),
(1967, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-KdsSvc%4Operational.evtx'),
(1968, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-ApphelpCache%4Operational.evtx'),
(1969, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-Boot%4Operational.evtx'),
(1970, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-Cache%4Operational.evtx'),
(1971, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-CPU-Starvation%4Operational.evtx'),
(1972, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-Dump%4Operational.evtx'),
(1973, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-EventTracing%4Admin.evtx'),
(1974, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-IO%4Operational.evtx'),
(1975, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-LiveDump%4Operational.evtx'),
(1976, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-PnP%4Configuration.evtx'),
(1977, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-PnP%4Device Management.evtx'),
(1978, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-PnP%4Driver Watchdog.evtx'),
(1979, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-Power%4Thermal-Operational.evtx'),
(1980, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-PRM%4Operational.evtx'),
(1981, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-ShimEngine%4Operational.evtx'),
(1982, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-StoreMgr%4Operational.evtx'),
(1983, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-WDI%4Operational.evtx'),
(1984, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-WHEA%4Errors.evtx'),
(1985, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-WHEA%4Operational.evtx'),
(1986, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Known Folders API Service.evtx'),
(1987, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-LanguagePackSetup%4Operational.evtx'),
(1988, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-LiveId%4Operational.evtx'),
(1989, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-MemoryDiagnostics-Results%4Debug.evtx'),
(1990, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ModernDeployment-Diagnostics-Provider%4Admin.evtx'),
(1991, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ModernDeployment-Diagnostics-Provider%4Autopilot.evtx'),
(1992, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ModernDeployment-Diagnostics-Provider%4Diagnostics.evtx'),
(1993, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ModernDeployment-Diagnostics-Provider%4ManagementService.evtx'),
(1994, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Mprddm%4Operational.evtx'),
(1995, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-MUI%4Admin.evtx'),
(1996, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-MUI%4Operational.evtx'),
(1997, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NcdAutoSetup%4Operational.evtx'),
(1998, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NCSI%4Operational.evtx'),
(1999, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NdisImPlatform%4Operational.evtx'),
(2000, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NetworkLocationWizard%4Operational.evtx'),
(2001, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NetworkProfile%4Operational.evtx'),
(2002, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NetworkProvider%4Operational.evtx'),
(2003, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NetworkProvisioning%4Operational.evtx'),
(2004, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NlaSvc%4Operational.evtx'),
(2005, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Ntfs%4Operational.evtx'),
(2006, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Ntfs%4WHC.evtx'),
(2007, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NTLM%4Operational.evtx'),
(2008, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-OneBackup%4Debug.evtx'),
(2009, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-OOBE-Machine-DUI%4Operational.evtx'),
(2010, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ParentalControls%4Operational.evtx'),
(2011, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Partition%4Diagnostic.evtx'),
(2012, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PerceptionRuntime%4Operational.evtx'),
(2013, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PerceptionSensorDataService%4Operational.evtx'),
(2014, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PersistentMemory-Nvdimm%4Operational.evtx'),
(2015, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PersistentMemory-PmemDisk%4Operational.evtx'),
(2016, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PersistentMemory-ScmBus%4Certification.evtx'),
(2017, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PersistentMemory-ScmBus%4Operational.evtx'),
(2018, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Policy%4Operational.evtx'),
(2019, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PowerShell%4Admin.evtx'),
(2020, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PowerShell%4Operational.evtx'),
(2021, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PowerShell-DesiredStateConfiguration-FileDownloadManager%4Operational.evtx'),
(2022, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PrintService%4Admin.evtx'),
(2023, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Privacy-Auditing%4Operational.evtx'),
(2024, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Program-Compatibility-Assistant%4CompatAfterUpgrade.evtx'),
(2025, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Provisioning-Diagnostics-Provider%4Admin.evtx'),
(2026, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Provisioning-Diagnostics-Provider%4AutoPilot.evtx'),
(2027, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Provisioning-Diagnostics-Provider%4ManagementService.evtx'),
(2028, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PushNotification-Platform%4Admin.evtx'),
(2029, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PushNotification-Platform%4Operational.evtx'),
(2030, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ReadyBoost%4Operational.evtx'),
(2031, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ReadyBoostDriver%4Operational.evtx'),
(2032, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ReFS%4Operational.evtx'),
(2033, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Regsvr32%4Operational.evtx'),
(2034, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteApp and Desktop Connections%4Admin.evtx'),
(2035, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteApp and Desktop Connections%4Operational.evtx'),
(2036, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteAssistance%4Admin.evtx'),
(2037, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteAssistance%4Operational.evtx'),
(2038, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteDesktopServices-RdpCoreTS%4Admin.evtx'),
(2039, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteDesktopServices-RdpCoreTS%4Operational.evtx'),
(2040, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteDesktopServices-SessionServices%4Operational.evtx'),
(2041, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Resource-Exhaustion-Detector%4Operational.evtx'),
(2042, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Resource-Exhaustion-Resolver%4Operational.evtx'),
(2043, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RestartManager%4Operational.evtx'),
(2044, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RetailDemo%4Admin.evtx'),
(2045, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RetailDemo%4Operational.evtx'),
(2046, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SearchUI%4Operational.evtx'),
(2047, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-Audit-Configuration-Client%4Operational.evtx'),
(2048, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-EnterpriseData-FileRevocationManager%4Operational.evtx'),
(2049, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-LessPrivilegedAppContainer%4Operational.evtx'),
(2050, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-Mitigations%4KernelMode.evtx'),
(2051, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-Mitigations%4UserMode.evtx'),
(2052, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-Netlogon%4Operational.evtx'),
(2053, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-SPP-UX-GenuineCenter-Logging%4Operational.evtx'),
(2054, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-SPP-UX-Notifications%4ActionCenter.evtx'),
(2055, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-UserConsentVerifier%4Audit.evtx'),
(2056, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SecurityMitigationsBroker%4Operational.evtx'),
(2057, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-ConnectedAccountState%4ActionCenter.evtx'),
(2058, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-Core%4ActionCenter.evtx'),
(2059, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-Core%4AppDefaults.evtx'),
(2060, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-Core%4LogonTasksChannel.evtx'),
(2061, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-Core%4Operational.evtx'),
(2062, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ShellCommon-StartLayoutPopulation%4Operational.evtx'),
(2063, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmartCard-Audit%4Authentication.evtx'),
(2064, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmartCard-DeviceEnum%4Operational.evtx'),
(2065, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmartCard-TPM-VCard-Module%4Admin.evtx'),
(2066, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmartCard-TPM-VCard-Module%4Operational.evtx'),
(2067, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmbClient%4Audit.evtx'),
(2068, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmbClient%4Connectivity.evtx'),
(2069, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBClient%4Operational.evtx'),
(2070, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmbClient%4Security.evtx'),
(2071, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBServer%4Audit.evtx'),
(2072, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBServer%4Connectivity.evtx'),
(2073, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBServer%4Operational.evtx'),
(2074, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBServer%4Security.evtx'),
(2075, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBWitnessClient%4Admin.evtx'),
(2076, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBWitnessClient%4Informational.evtx'),
(2077, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StateRepository%4Operational.evtx'),
(2078, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StateRepository%4Restricted.evtx'),
(2079, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-ClassPnP%4Operational.evtx'),
(2080, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-NvmeDisk%4Operational.evtx'),
(2081, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-Storport%4Health.evtx'),
(2082, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-Storport%4Operational.evtx'),
(2083, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-Tiering%4Admin.evtx'),
(2084, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageManagement%4Operational.evtx'),
(2085, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageManagement-PartUtil%4Operational.evtx'),
(2086, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSettings%4Diagnostic.evtx'),
(2087, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Api%4Operational.evtx'),
(2088, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Driver%4Diagnostic.evtx'),
(2089, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Driver%4Operational.evtx'),
(2090, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-ManagementAgent%4WHC.evtx'),
(2091, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Parser%4Diagnostic.evtx'),
(2092, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Parser%4Operational.evtx'),
(2093, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-SpaceManager%4Diagnostic.evtx'),
(2094, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-SpaceManager%4Operational.evtx'),
(2095, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Store%4Operational.evtx'),
(2096, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storsvc%4Diagnostic.evtx'),
(2097, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SystemSettingsThreshold%4Operational.evtx'),
(2098, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TaskScheduler%4Maintenance.evtx'),
(2099, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TCPIP%4Operational.evtx'),
(2100, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-ClientUSBDevices%4Admin.evtx'),
(2101, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-ClientUSBDevices%4Operational.evtx'),
(2102, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-LocalSessionManager%4Admin.evtx'),
(2103, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-LocalSessionManager%4Operational.evtx'),
(2104, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-PnPDevices%4Admin.evtx'),
(2105, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-PnPDevices%4Operational.evtx'),
(2106, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-Printers%4Admin.evtx'),
(2107, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-Printers%4Operational.evtx'),
(2108, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-RDPClient%4Operational.evtx'),
(2109, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-RemoteConnectionManager%4Admin.evtx'),
(2110, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-RemoteConnectionManager%4Operational.evtx'),
(2111, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Time-Service%4Operational.evtx'),
(2112, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Time-Service-PTP-Provider%4PTP-Operational.evtx'),
(2113, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Troubleshooting-Recommended%4Admin.evtx'),
(2114, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Troubleshooting-Recommended%4Operational.evtx'),
(2115, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TWinUI%4Operational.evtx'),
(2116, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TZSync%4Operational.evtx'),
(2117, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TZUtil%4Operational.evtx'),
(2118, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UAC%4Operational.evtx'),
(2119, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UAC-FileVirtualization%4Operational.evtx'),
(2120, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UniversalTelemetryClient%4Operational.evtx'),
(2121, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-User Control Panel%4Operational.evtx'),
(2122, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-User Device Registration%4Admin.evtx');
INSERT INTO `doc_pdpa_file_dir` (`id`, `device_name`, `os_name`, `path`, `name_file`) VALUES
(2123, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-User Profile Service%4Operational.evtx'),
(2124, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-User-Loader%4Operational.evtx'),
(2125, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UserPnp%4ActionCenter.evtx'),
(2126, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UserPnp%4DeviceInstall.evtx'),
(2127, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VDRVROOT%4Operational.evtx'),
(2128, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VerifyHardwareSecurity%4Admin.evtx'),
(2129, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VHDMP-Operational.evtx'),
(2130, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VolumeSnapshot-Driver%4Operational.evtx'),
(2131, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VPN%4Operational.evtx'),
(2132, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VPN-Client%4Operational.evtx'),
(2133, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Wcmsvc%4Operational.evtx'),
(2134, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WebAuthN%4Operational.evtx'),
(2135, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WER-Diag%4Operational.evtx'),
(2136, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WER-PayloadHealth%4Operational.evtx'),
(2137, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WerKernel%4Operational.evtx'),
(2138, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WFP%4Operational.evtx'),
(2139, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Win32k%4Operational.evtx'),
(2140, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Defender%4Operational.evtx'),
(2141, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Defender%4WHC.evtx'),
(2142, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Firewall With Advanced Security%4ConnectionSecurity.evtx'),
(2143, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Firewall With Advanced Security%4Firewall.evtx'),
(2144, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Firewall With Advanced Security%4FirewallDiagnostics.evtx'),
(2145, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WindowsBackup%4ActionCenter.evtx'),
(2146, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WindowsSystemAssessmentTool%4Operational.evtx'),
(2147, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WindowsUpdateClient%4Operational.evtx'),
(2148, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WinHttp%4Operational.evtx'),
(2149, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WinINet%4Operational.evtx'),
(2150, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WinINet-Config%4ProxyConfigChanged.evtx'),
(2151, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Winlogon%4Operational.evtx'),
(2152, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WinRM%4Operational.evtx'),
(2153, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Winsock-WS2HELP%4Operational.evtx'),
(2154, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Wired-AutoConfig%4Operational.evtx'),
(2155, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WLAN-AutoConfig%4Operational.evtx'),
(2156, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WMI-Activity%4Operational.evtx'),
(2157, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WMPNSS-Service%4Operational.evtx'),
(2158, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WorkFolders%4Operational.evtx'),
(2159, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WorkFolders%4WHC.evtx'),
(2160, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Workplace Join%4Admin.evtx'),
(2161, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WPD-ClassInstaller%4Operational.evtx'),
(2162, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WPD-CompositeClassDriver%4Operational.evtx'),
(2163, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WPD-MTPClassDriver%4Operational.evtx'),
(2164, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WWAN-SVC-Events%4Operational.evtx'),
(2165, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-WindowsPhone-Connectivity-WiFiConnSvc-Channel.evtx'),
(2166, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'OAlerts.evtx'),
(2167, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'OneApp_IGCC.evtx'),
(2168, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'OpenSSH%4Admin.evtx'),
(2169, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'OpenSSH%4Operational.evtx'),
(2170, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Parameters.evtx'),
(2171, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Security.evtx'),
(2172, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Setup.evtx'),
(2173, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'SMSApi.evtx'),
(2174, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'State.evtx'),
(2175, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'System.evtx'),
(2176, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'USER_ESRV_SVC_QUEENCREEK.evtx'),
(2177, 'Code_Zero_Four', 'Linux-5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Windows PowerShell.evtx');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_level`
--

CREATE TABLE `doc_pdpa_level` (
  `level_id` int(11) NOT NULL,
  `level_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_level`
--

INSERT INTO `doc_pdpa_level` (`level_id`, `level_name`) VALUES
(35, 'ธงขาว'),
(36, 'ธงแดง'),
(37, 'ธงฟ้า'),
(41, 'a'),
(42, 'aa'),
(43, 'aaa'),
(44, 'b'),
(45, 'bb'),
(46, 'bbb'),
(47, 'c'),
(48, 'cc'),
(49, 'ccc'),
(50, 'd'),
(51, 'dd'),
(52, 'ddd'),
(53, 'e'),
(54, 'ee'),
(55, 'eee');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_log0_hash`
--

CREATE TABLE `doc_pdpa_log0_hash` (
  `id` int(11) NOT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `os_name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `name_file` varchar(255) DEFAULT NULL,
  `total_line` varchar(255) DEFAULT NULL,
  `date_now` datetime NOT NULL DEFAULT current_timestamp(),
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_log0_hash`
--

INSERT INTO `doc_pdpa_log0_hash` (`id`, `device_name`, `os_name`, `path`, `name_file`, `total_line`, `date_now`, `value`) VALUES
(86, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.200,08:00,2022-01-19.log', '124', '2022-03-04 23:42:39', 'ce42ef446ecbc5e013a44937d19e0a7aaccd3edd8f08417e0cbae11883b63819'),
(87, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,01:00,2022-01-08.log', '26175', '2022-03-04 23:42:39', '01511cff6db1caeea1482256850958a6fab3a8da536c5ab33ee7eb976c8e2fdc'),
(88, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,02:00,2022-01-13.log', '369', '2022-03-04 23:42:39', 'bef57f20042142dc220fee7fb2bbd140a723bb0e0166d80f4da5b2855dcf3e2d'),
(89, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,06:00,2022-01-14.log', '10900', '2022-03-04 23:42:39', '03f5fd755734cacfbdd13a101a84871804a427c22753134e5b30ffc35cee661c'),
(90, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,07:00,2022-01-14.log', '4475', '2022-03-04 23:42:40', '10e98b7ae9656b585f426c1da22faa1f6b8a49e0fbcf2d53248896f28ea1692e'),
(91, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,12:00,2021-12.17.log', '501', '2022-03-04 23:42:40', '1bd0b894c9f8ff82608f5db3a51b6547a5e04bf173a787dd9e41c85b71edc0ac'),
(92, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,13:00,2021-12.17.log', '618', '2022-03-04 23:42:40', 'e27baf6bfc362c113552334312f8e66d46111c37446530cf7116feccacf49c1b'),
(93, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,14:00,2021-12.17.log', '105', '2022-03-04 23:42:40', 'c6d0725b55ef89325b054748215f6d1550c38c0744c9d963c8576d4f2354257a'),
(94, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,16:00,2021-12.17.log', '123', '2022-03-04 23:42:40', 'af9807137dc61944753a49cf799b32657a4722d623f4672e25da4103fdad252f'),
(95, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,21:00,2022-01-14.log', '27', '2022-03-04 23:42:40', 'c5953297e7a0fa29ffcd950462547a32499058cbe5845125ff0c2e1d6874d6ee'),
(96, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.221,22:00,2022-01-14.log', '13', '2022-03-04 23:42:40', 'bd21e92a5402efcda79f37830cc6584184f6bbcb876943732405abfdbb5b4c9a'),
(97, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.223,22:00,2022-01-14.log', '15', '2022-03-04 23:42:40', 'c068b273988d00460b31af6565a34c36669dc9916b2f7ce98c7913d949dc6797'),
(98, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.230,09:00,2021-12.17.log', '145', '2022-03-04 23:42:40', '49cde177a1fb594d6f97887baf49f3e4b0a4f574df4f99bc45f2dbe373efd915'),
(99, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.233,04:00,2022-01-15.log', '549', '2022-03-04 23:42:40', '0ed8d1b47ebdcdb5bee221fee1dbad84dfcef5660099b27aacd73d49cf4bb12d'),
(100, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.233,05:00,2022-01-15.log', '26700', '2022-03-04 23:42:40', 'e184224e17e1865481e09ad2c64b1b918f33f08e0a802832ba78d382c2b85f6c'),
(101, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.233,06:00,2022-01-14.log', '12', '2022-03-04 23:42:40', '6a5aad2ddf9111b4ecf2d38282dc57c7a9dc55c34624b9cacfc22851b55ab94d'),
(102, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.233,06:00,2022-01-15.log', '348', '2022-03-04 23:42:40', 'ddd01a14736323a9170cb3bddc980c8f3a5fd03e2827573a891f758d9f7b6158'),
(103, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.4,00:00,2021-12-18.log', '96', '2022-03-04 23:42:40', '88daa00fb94951e4e7341f69b156092d2c0fb582bf4ba14fb505274236b4f1e7'),
(104, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.4,01:00,2021-01-08.log', '95', '2022-03-04 23:42:40', 'ab04bb73462b24aa20f7ce592ff7f231d07c10afe1ec2afa4cfb917a6dd35ea0'),
(105, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.16.20.4,12:00,2021-12-25.log', '45', '2022-03-04 23:42:40', '275f9c485be02cff93ea98935a37d6f2ea2e96e073e1207600fdb1d0feacf599'),
(106, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.18.196.89,17:00,2022-01-21.log', '10', '2022-03-04 23:42:40', '1620c83eed6ff117787a64ba457d2ba9ca937d601144b75f60f161f2ddb24aba'),
(107, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/d/project_sgc/file_client/', '172.18.196.89,18:00,2022-01-21.log', '19', '2022-03-04 23:42:40', '05d96f598f5a3d9ac9b2e17fefa0575d406656f08e53c14a4808fbaf45746851'),
(108, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Application.evtx', '78435', '2022-03-04 23:42:52', 'f0b2354bf5ba1fb07bf0f012863ff7ebbff66f9d0e74d15ddb599c5c9eb18ab9'),
(109, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'HardwareEvents.evtx', '1', '2022-03-04 23:42:52', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(110, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Intel-GFX-Info%4Application.evtx', '111', '2022-03-04 23:42:52', 'c44f844ddf24cba81f6321abd5b87616897ea28ca6f47865e1ab238d6be78eca'),
(111, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Intel-GFX-Info%4System.evtx', '1', '2022-03-04 23:42:52', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(112, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Internet Explorer.evtx', '1', '2022-03-04 23:42:52', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(113, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Key Management Service.evtx', '1', '2022-03-04 23:42:52', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(114, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Client-Licensing-Platform%4Admin.evtx', '4323', '2022-03-04 23:42:53', '6f0db0b627cb80505870dd15d449e98cb48eeef7e712da89046dd318802ada7c'),
(115, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-System-Diagnostics-DiagnosticInvoker%4Operational.evtx', '1', '2022-03-04 23:42:53', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(116, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AAD%4Operational.evtx', '484', '2022-03-04 23:42:53', 'ddf4cbf8c89f5b1d02de9d040539a244e198e135b8dbdc53c0373d84219a93d3'),
(117, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-All-User-Install-Agent%4Admin.evtx', '1', '2022-03-04 23:42:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(118, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AllJoyn%4Operational.evtx', '1', '2022-03-04 23:42:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(119, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppHost%4Admin.evtx', '1', '2022-03-04 23:42:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(120, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppID%4Operational.evtx', '1', '2022-03-04 23:42:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(121, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ApplicabilityEngine%4Operational.evtx', '1', '2022-03-04 23:42:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(122, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application Server-Applications%4Admin.evtx', '1', '2022-03-04 23:42:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(123, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application Server-Applications%4Operational.evtx', '1', '2022-03-04 23:42:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(124, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Program-Compatibility-Assistant.evtx', '223', '2022-03-04 23:42:54', '936fb84132f2e1f3cd87f35ca6f92bf53e24b2d25ad761823190f137176626ae'),
(125, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Program-Compatibility-Troubleshooter.evtx', '1', '2022-03-04 23:42:54', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(126, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Program-Inventory.evtx', '1', '2022-03-04 23:42:55', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(127, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Program-Telemetry.evtx', '1779', '2022-03-04 23:42:55', '28208fa17317ab67dcd60c38e43ef5747c554994478920a1053dbf82af69866b'),
(128, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Application-Experience%4Steps-Recorder.evtx', '1', '2022-03-04 23:42:55', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(129, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppLocker%4EXE and DLL.evtx', '1', '2022-03-04 23:42:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(130, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppLocker%4MSI and Script.evtx', '1', '2022-03-04 23:42:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(131, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppLocker%4Packaged app-Deployment.evtx', '1', '2022-03-04 23:42:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(132, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppLocker%4Packaged app-Execution.evtx', '1', '2022-03-04 23:42:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(133, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppModel-Runtime%4Admin.evtx', '3827', '2022-03-04 23:42:56', '69deace60645a9b80b9766fbfc787ff29ae2771049a9b4002d57a26a80370724'),
(134, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppReadiness%4Admin.evtx', '1551', '2022-03-04 23:42:57', 'b312379e6b245fe4006588497442b3b3fe26bb157ad874697459470834b83eed'),
(135, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppReadiness%4Operational.evtx', '216', '2022-03-04 23:42:57', 'aad6beffd6e7cbe827cde26aa1c69d25ec8932fab21c7e46f26203171c75c1be'),
(136, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppXDeployment%4Operational.evtx', '1414', '2022-03-04 23:42:57', '6839a4c6a635e6b49b81a959e05bfa5a7a3c1c09fe172c55e1075414dc5ebae2'),
(137, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppXDeploymentServer%4Operational.evtx', '9418', '2022-03-04 23:43:00', '29472af4e82cf23387547fa35d618f902deb40e08a2802608e28875b40651bec'),
(138, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppXDeploymentServer%4Restricted.evtx', '1', '2022-03-04 23:43:00', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(139, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-AppxPackaging%4Operational.evtx', '2463', '2022-03-04 23:43:01', '312a246177659a90f4c6c58d9a7e0ef455ff0361c02c0146528fe983d61d5c95'),
(140, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Audio%4CaptureMonitor.evtx', '1', '2022-03-04 23:43:01', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(141, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Audio%4Operational.evtx', '3636', '2022-03-04 23:43:02', 'cffd604e8cb373a1df8a14adfcb7d5ecf2c27f9c47162ad99ccead5e05dbc2e5'),
(142, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Audio%4PlaybackManager.evtx', '585', '2022-03-04 23:43:02', 'c2d8559f047d256ebbd44461705dfcf703b7b8f03a689e7f705884a8769d91d7'),
(143, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Authentication User Interface%4Operational.evtx', '10', '2022-03-04 23:43:02', '7fde344fafc586a572000b54f9600a3989ed612ecb41d8e55924edfd063b201c'),
(144, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-BackgroundTaskInfrastructure%4Operational.evtx', '979', '2022-03-04 23:43:03', '5288392cbef5e2fdfb175c4bc15c43ee92275e07c69215916178e6c415489a48'),
(145, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Backup.evtx', '1', '2022-03-04 23:43:03', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(146, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Biometrics%4Operational.evtx', '558', '2022-03-04 23:43:04', '1a2960ace13cdbcdac1d5644508f7bc07f3deb20ec50bff4aa8283f40b111eec'),
(147, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-BitLocker%4BitLocker Management.evtx', '616', '2022-03-04 23:43:04', '868294f637cff64017b3667f5161cfbdacea5c92e7e4e95c74da27a1c7c01de4'),
(148, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Bits-Client%4Operational.evtx', '7056', '2022-03-04 23:43:05', '2274afdc84f0cb996121ae83946656b7b711a89bcae0e52d4b2c408eff1fa746'),
(149, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Bluetooth-BthLEPrepairing%4Operational.evtx', '1', '2022-03-04 23:43:05', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(150, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Bluetooth-MTPEnum%4Operational.evtx', '1', '2022-03-04 23:43:05', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(151, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CertificateServicesClient-Lifecycle-System%4Operational.evtx', '21', '2022-03-04 23:43:05', '273a745c4c827df3ed1a714b4484e3554a43dacff33de15de40afcb92c9b7a25'),
(152, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CertificateServicesClient-Lifecycle-User%4Operational.evtx', '1', '2022-03-04 23:43:05', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(153, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Cleanmgr%4Diagnostic.evtx', '1', '2022-03-04 23:43:05', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(154, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CloudStore%4Operational.evtx', '1570', '2022-03-04 23:43:06', 'ab9399d26336e931c9bf0c707a8ef72753cdbe6cfbb4002af2cfbb0ff4e6a65b'),
(155, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CodeIntegrity%4Operational.evtx', '143', '2022-03-04 23:43:06', '4fc160a1660a8a232226ea760582680cf9287aeafc3ed1857f983fe55590b942'),
(156, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Compat-Appraiser%4Operational.evtx', '1', '2022-03-04 23:43:06', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(157, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Containers-BindFlt%4Operational.evtx', '75', '2022-03-04 23:43:06', 'eeb4c766610ea4dc2f63396d36d1f5f1fc33c8989fe2089952572357a8e5d4c4'),
(158, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Containers-Wcifs%4Operational.evtx', '77', '2022-03-04 23:43:06', 'b1978ebbbf06fe7f1f0ae86749eb92dd989800282c6d30e438908b7dba7c3ca8'),
(159, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CoreApplication%4Operational.evtx', '1', '2022-03-04 23:43:06', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(160, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CoreSystem-SmsRouter-Events%4Operational.evtx', '836', '2022-03-04 23:43:07', '5f063052273414b9447e46947980d16d608739ffa79a06606c8fc5ac6704358f'),
(161, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CorruptedFileRecovery-Client%4Operational.evtx', '1', '2022-03-04 23:43:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(162, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-CorruptedFileRecovery-Server%4Operational.evtx', '1', '2022-03-04 23:43:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(163, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Crypto-DPAPI%4BackUpKeySvc.evtx', '1', '2022-03-04 23:43:07', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(164, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Crypto-DPAPI%4Operational.evtx', '2823', '2022-03-04 23:43:08', '72c4776e7c83076e4b26b3e20cb96c2978744b6b077c4ae3d2423a773217dea4'),
(165, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Crypto-NCrypt%4Operational.evtx', '1385', '2022-03-04 23:43:08', '76bcf433c597e6c28a042d1b85ba0342003577c53185a82d6431b7834b26c01f'),
(166, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DAL-Provider%4Operational.evtx', '1', '2022-03-04 23:43:08', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(167, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DataIntegrityScan%4Admin.evtx', '1', '2022-03-04 23:43:08', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(168, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DataIntegrityScan%4CrashRecovery.evtx', '1', '2022-03-04 23:43:08', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(169, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DateTimeControlPanel%4Operational.evtx', '1', '2022-03-04 23:43:09', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(170, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceGuard%4Operational.evtx', '1', '2022-03-04 23:43:09', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(171, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider%4Admin.evtx', '287', '2022-03-04 23:43:09', '2ea287a6e38a73912decc5b5ded92726f91fc5b4718d7aae345555a4599e4166'),
(172, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider%4Autopilot.evtx', '1', '2022-03-04 23:43:09', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(173, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider%4Operational.evtx', '1587', '2022-03-04 23:43:10', '9d9d397f35b7a5a142488a552eb5929822e74d0fa21e9be1d7dc553367851a95'),
(174, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Devices-Background%4Operational.evtx', '1', '2022-03-04 23:43:10', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(175, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceSetupManager%4Admin.evtx', '3997', '2022-03-04 23:43:11', '1fb10baad7fe8ae8072b04bd669a10e1d850322d180376920ec3942de8795880'),
(176, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceSetupManager%4Operational.evtx', '569', '2022-03-04 23:43:11', 'd6fb487e6c8b558f8ce4d0b58d322c66cabb456c9a408acfad7c10207b9b0cb7'),
(177, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceSync%4Operational.evtx', '1', '2022-03-04 23:43:11', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(178, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DeviceUpdateAgent%4Operational.evtx', '1', '2022-03-04 23:43:11', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(179, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Dhcp-Client%4Admin.evtx', '3068', '2022-03-04 23:43:12', '9917aab322a109d7c8ad6e048394e6588b293bde1920728c7a6c960be87ed24b'),
(180, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Dhcpv6-Client%4Admin.evtx', '455', '2022-03-04 23:43:12', '37c763e32e3a0b8850b8cc0d7758d04e9c9ec7c4fa7423b77f8fceba632eecaa'),
(181, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-DPS%4Operational.evtx', '2451', '2022-03-04 23:43:13', 'a4ead2332062fa8f4f90466093d38431fb2b8372522e67e0e08b4cacf4954413'),
(182, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-PCW%4Operational.evtx', '2869', '2022-03-04 23:43:14', '14fc3d8ec9531e6a8f2a5a4092612a63d3d7e85792bb20dc49a72ff3e2b9a0cc'),
(183, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-PLA%4Operational.evtx', '1', '2022-03-04 23:43:14', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(184, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-Scheduled%4Operational.evtx', '183', '2022-03-04 23:43:14', 'c2d460d98b3b2782b883e3830560a9dfb54b2f8286f3353251f302239ed636f2'),
(185, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-Scripted%4Admin.evtx', '101', '2022-03-04 23:43:14', '44f82cbf2ee485eb1e2b72135d9bf71c4b324679729633bc1618d329dd2675f4'),
(186, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-Scripted%4Operational.evtx', '147', '2022-03-04 23:43:14', '903f3c86ffe50a1725a3ff95b8adad6767fab77af5249f3798471af1b392ad0a'),
(187, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnosis-ScriptedDiagnosticsProvider%4Operational.evtx', '129', '2022-03-04 23:43:14', 'ba5ad4a3fa1a3c1904146ddc482cb84c34f428fe57faf07bb97356927787c9f8'),
(188, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnostics-Networking%4Operational.evtx', '1', '2022-03-04 23:43:14', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(189, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Diagnostics-Performance%4Operational.evtx', '569', '2022-03-04 23:43:15', '62f652e95c166b486daa2357e15535d0e981a8b4227970c0460a29e5bab831ff'),
(190, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DiskDiagnostic%4Operational.evtx', '1', '2022-03-04 23:43:15', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(191, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DiskDiagnosticDataCollector%4Operational.evtx', '185', '2022-03-04 23:43:15', 'a74296fc7943329159a8ff93fe9eff80f1fdedd8deb327840a0a5ce80862fb65'),
(192, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DiskDiagnosticResolver%4Operational.evtx', '1', '2022-03-04 23:43:15', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(193, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DSC%4Admin.evtx', '1', '2022-03-04 23:43:15', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(194, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DSC%4Operational.evtx', '1', '2022-03-04 23:43:15', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(195, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DxgKrnl-Admin.evtx', '13', '2022-03-04 23:43:15', 'ac69c5d18996e5d2e3fd0fc7a1de0c70ee32c33deb25e5f46b8047a3310d290a'),
(196, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-DxgKrnl-Operational.evtx', '1067', '2022-03-04 23:43:16', 'c1945694c2897225ef2b6cc85a3f0310289c6e6742d3fb622f4b62412e5c2604'),
(197, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapHost%4Operational.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(198, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapMethods-RasChap%4Operational.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(199, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapMethods-RasTls%4Operational.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(200, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapMethods-Sim%4Operational.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(201, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EapMethods-Ttls%4Operational.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(202, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EDP-Application-Learning%4Admin.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(203, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EDP-Audit-Regular%4Admin.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(204, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EDP-Audit-TCB%4Admin.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(205, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EnhancedStorage-EhStorClass%4Operational.evtx', '120', '2022-03-04 23:43:16', '5f6442c2c57df5591cb9056e36ee11b8852d116771a15aa99083c6517b9f1af9'),
(206, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-EventCollector%4Operational.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(207, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Fault-Tolerant-Heap%4Operational.evtx', '52', '2022-03-04 23:43:16', '866c8f15ccc070ff3693af299f1568700a9e2333f0974104bbee2216ae9e06b8'),
(208, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-FeatureConfiguration%4Operational.evtx', '1', '2022-03-04 23:43:16', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(209, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-FileHistory-Core%4WHC.evtx', '32', '2022-03-04 23:43:16', '279ab2c396100b5442d2c61d73bb5bf3654f475823e96ff07c7e73259352aeeb'),
(210, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-FileHistory-Engine%4BackupLog.evtx', '1', '2022-03-04 23:43:17', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(211, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-FMS%4Operational.evtx', '1', '2022-03-04 23:43:17', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(212, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Folder Redirection%4Operational.evtx', '1', '2022-03-04 23:43:17', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(213, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Forwarding%4Operational.evtx', '1', '2022-03-04 23:43:17', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(214, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-GenericRoaming%4Admin.evtx', '1', '2022-03-04 23:43:17', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(215, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-GroupPolicy%4Operational.evtx', '14267', '2022-03-04 23:43:19', '99b212bca541a85691e67ee48dd6e8cbd861f4913b8152ee52aaf0686e426c94'),
(216, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-HelloForBusiness%4Operational.evtx', '1693', '2022-03-04 23:43:20', '7b13a83858ea3a5e8e1747fad8b8c36d7543343659585cbd3d05dd0245a6833e'),
(217, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Help%4Operational.evtx', '1', '2022-03-04 23:43:20', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(218, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-hidcfu%4Operational.evtx', '1', '2022-03-04 23:43:20', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(219, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-HomeGroup Control Panel%4Operational.evtx', '1', '2022-03-04 23:43:20', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(220, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Host-Network-Service-Admin.evtx', '1859', '2022-03-04 23:43:21', '535ef9c1c238256c70f9d1d61258571f442467a8afbb293f00e9a984bb934648'),
(221, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Host-Network-Service-Operational.evtx', '1574', '2022-03-04 23:43:22', '38cf6e69ea456bfd22740d818cd9f40dc49c9ec5e8479b0e835258924ed0801a'),
(222, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-HotspotAuth%4Operational.evtx', '1', '2022-03-04 23:43:22', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(223, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Compute-Admin.evtx', '62', '2022-03-04 23:43:22', 'f8acd20e5a4c94875e5dcaa19194a54c3fcbbdf4151f22428c8619ca123edc53'),
(224, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Compute-Operational.evtx', '4967', '2022-03-04 23:43:23', 'b62ead2693f6632c9f4762d3d87469a0bea337dffce2a0692d4fb04fd01e6710'),
(225, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Guest-Drivers%4Admin.evtx', '1', '2022-03-04 23:43:23', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(226, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Hypervisor-Admin.evtx', '122', '2022-03-04 23:43:23', '842bb5de9f53136fe73d750fe58a16100af79f73b36a5123675b4e7ecd84db53'),
(227, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Hypervisor-Operational.evtx', '604', '2022-03-04 23:43:24', '00d5a80d4242b1065233188823ecc27bed135d0928df93db80c1c6f7052ec3b2'),
(228, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-StorageVSP-Admin.evtx', '1', '2022-03-04 23:43:24', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(229, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-VID-Admin.evtx', '23', '2022-03-04 23:43:24', '509ff68b3609899eb4aebb1e2d9915f71afa64e293f746df961f8fab4f4ab6ab'),
(230, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-VmSwitch-Operational.evtx', '6549', '2022-03-04 23:43:25', 'e6bd300d027abdfb78194716a861da938ad683714109d068d2cc8579e89b30be'),
(231, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Worker-Admin.evtx', '2557', '2022-03-04 23:43:25', 'c07818c674941d85e4e3c88a9f425d8bce39ecdee34d6666f8c2c962f57d4f4d'),
(232, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Hyper-V-Worker-Operational.evtx', '1', '2022-03-04 23:43:25', 'bceba48cda194b290ee78c384c7d712139ec4205de2fa162ec9deac62d977ee6'),
(233, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-IdCtrls%4Operational.evtx', '1', '2022-03-04 23:43:25', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(234, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-IKE%4Operational.evtx', '102', '2022-03-04 23:43:25', 'b741c95865a58682a33760963af2946b99cc32470ccf9480ec059e59fc66ed08'),
(235, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-International-RegionalOptionsControlPanel%4Operational.evtx', '1', '2022-03-04 23:43:25', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(236, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Iphlpsvc%4Operational.evtx', '1', '2022-03-04 23:43:25', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(237, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-IPxlatCfg%4Operational.evtx', '1', '2022-03-04 23:43:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(238, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-KdsSvc%4Operational.evtx', '1', '2022-03-04 23:43:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(239, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-ApphelpCache%4Operational.evtx', '1', '2022-03-04 23:43:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(240, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-Boot%4Operational.evtx', '381', '2022-03-04 23:43:26', '0499a344a44ffc3eb2d2976978690dcacfdf4b323cc86396f4f415dbc256c58a'),
(241, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-Cache%4Operational.evtx', '39304', '2022-03-04 23:43:27', '080f6365e865937b91f13103dbeed3d69d694011f54d4a9f04494fbbaea6a327'),
(242, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-CPU-Starvation%4Operational.evtx', '1', '2022-03-04 23:43:27', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(243, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-Dump%4Operational.evtx', '1', '2022-03-04 23:43:27', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(244, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-EventTracing%4Admin.evtx', '661', '2022-03-04 23:43:28', '00498bee4c2a4c3668a7423b934e1ca5adeb46d00b2b268ca259f8b34b168baf'),
(245, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-IO%4Operational.evtx', '1', '2022-03-04 23:43:28', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(246, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-LiveDump%4Operational.evtx', '380', '2022-03-04 23:43:28', 'a5c0c36c9bca238eb65c47f78a3c061df4466e3f7de484afccf21e6dca26d8a6'),
(247, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-PnP%4Configuration.evtx', '1689', '2022-03-04 23:43:28', '25e207ad20e43e2cf2e2bc556a9aee199de04cfb33fc82a26d1caeb4a1699838'),
(248, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-PnP%4Device Management.evtx', '2730', '2022-03-04 23:43:30', '532af25f95b0663157cfbc2b8a674fbe626dbaf3ab99c292d36ec5c1727663c7'),
(249, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-PnP%4Driver Watchdog.evtx', '38', '2022-03-04 23:43:30', '61b66c745364afe1c8784d96d8b381ab04f5b2b0a946bd1e88fbd381559c3e90'),
(250, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-Power%4Thermal-Operational.evtx', '1', '2022-03-04 23:43:30', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(251, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-PRM%4Operational.evtx', '1', '2022-03-04 23:43:30', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(252, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-ShimEngine%4Operational.evtx', '2124', '2022-03-04 23:43:30', '310e956184d2b760fdb2316e736920d479c1cddd2b396aba4f7987459e25cb9d'),
(253, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-StoreMgr%4Operational.evtx', '1', '2022-03-04 23:43:30', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(254, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-WDI%4Operational.evtx', '1', '2022-03-04 23:43:30', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(255, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-WHEA%4Errors.evtx', '1', '2022-03-04 23:43:30', '2bc58b73fb39ce40b456ddded1a80b10b189b0ce1af9a9c75f60f1eadb5a3607'),
(256, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Kernel-WHEA%4Operational.evtx', '1486', '2022-03-04 23:43:31', '7720d4637e22b4aae19361006c574bbfa915789e382fa13aac8aa0ace5449698'),
(257, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Known Folders API Service.evtx', '2282', '2022-03-04 23:43:32', 'c7aeb27ff0a399cdbe42d03c8a7b2aa1eb3518809d9fb65b865ac0a7b447d9a1'),
(258, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-LanguagePackSetup%4Operational.evtx', '98', '2022-03-04 23:43:32', '460e2e84b7de4b07160798c2ac4c12591e5dae7388785bfc4c9bb9d51075e93c'),
(259, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-LiveId%4Operational.evtx', '884', '2022-03-04 23:43:32', '439bf5790186281b0e9dedc6e82802f658164d5cc89f4615dadb612c6a747abc'),
(260, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-MemoryDiagnostics-Results%4Debug.evtx', '1', '2022-03-04 23:43:32', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(261, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ModernDeployment-Diagnostics-Provider%4Admin.evtx', '220', '2022-03-04 23:43:32', 'f3256a0767f817b8afbca2d3c27a114bafc0ea37dda19a7251234fe45d249f66'),
(262, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ModernDeployment-Diagnostics-Provider%4Autopilot.evtx', '28', '2022-03-04 23:43:33', '57df4f0ea68695d1dd7e30e553bdb36cd6f39555cdf901f7ec601e8ef2cdd834'),
(263, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ModernDeployment-Diagnostics-Provider%4Diagnostics.evtx', '1', '2022-03-04 23:43:33', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(264, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ModernDeployment-Diagnostics-Provider%4ManagementService.evtx', '45', '2022-03-04 23:43:33', '0d3ff562f67ff42b0f78e72dfd99c8af63ffbae6664b2d3349719bd48d891a4e'),
(265, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Mprddm%4Operational.evtx', '1', '2022-03-04 23:43:33', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(266, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-MUI%4Admin.evtx', '1', '2022-03-04 23:43:33', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(267, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-MUI%4Operational.evtx', '37', '2022-03-04 23:43:33', '6c7bce4819cd7f7ced46b730a11988be74983e5e0694b393cc6c9a66d117366f'),
(268, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NcdAutoSetup%4Operational.evtx', '74', '2022-03-04 23:43:33', '3ba51d4d7d74af4f8b12a543ed701ff9fd18d89ea7bd1c2a8074bf7fb356f5d4'),
(269, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NCSI%4Operational.evtx', '6610', '2022-03-04 23:43:33', '8b1f52da589234cc677c5cfe07f4bc880fd62d42f3ed75953ef0809de6563785'),
(270, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NdisImPlatform%4Operational.evtx', '1', '2022-03-04 23:43:34', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(271, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NetworkLocationWizard%4Operational.evtx', '1', '2022-03-04 23:43:34', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(272, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NetworkProfile%4Operational.evtx', '2912', '2022-03-04 23:43:34', 'cd812cb3d3594d83960f16e0fed120501f61b24fa3b86a3ec134914b16ef14fd'),
(273, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NetworkProvider%4Operational.evtx', '1', '2022-03-04 23:43:34', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(274, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NetworkProvisioning%4Operational.evtx', '1', '2022-03-04 23:43:34', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(275, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NlaSvc%4Operational.evtx', '100', '2022-03-04 23:43:34', 'd238d9a779057b62eb6f82cfd3934fbfae7ee6044758fbbb04cbc1ace90067d6'),
(276, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Ntfs%4Operational.evtx', '854887', '2022-03-04 23:43:54', 'ac6ae68b0849dabd8dafa978664cbd52b2922b3c7acddf096af08e7217704f2d'),
(277, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Ntfs%4WHC.evtx', '76', '2022-03-04 23:43:54', '07330c34c2bbd064219460e094036a6c09dedf5bf597191831afe279bb3addbe'),
(278, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-NTLM%4Operational.evtx', '1', '2022-03-04 23:43:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(279, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-OneBackup%4Debug.evtx', '1', '2022-03-04 23:43:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(280, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-OOBE-Machine-DUI%4Operational.evtx', '1', '2022-03-04 23:43:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af');
INSERT INTO `doc_pdpa_log0_hash` (`id`, `device_name`, `os_name`, `path`, `name_file`, `total_line`, `date_now`, `value`) VALUES
(281, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ParentalControls%4Operational.evtx', '1', '2022-03-04 23:43:54', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(282, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Partition%4Diagnostic.evtx', '1706', '2022-03-04 23:43:55', '785de612d6cb4b91d73785f3c767f33879abb9af030ab5f48162dd8a38014ab8'),
(283, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PerceptionRuntime%4Operational.evtx', '1', '2022-03-04 23:43:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(284, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PerceptionSensorDataService%4Operational.evtx', '1', '2022-03-04 23:43:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(285, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PersistentMemory-Nvdimm%4Operational.evtx', '1', '2022-03-04 23:43:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(286, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PersistentMemory-PmemDisk%4Operational.evtx', '1', '2022-03-04 23:43:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(287, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PersistentMemory-ScmBus%4Certification.evtx', '1', '2022-03-04 23:43:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(288, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PersistentMemory-ScmBus%4Operational.evtx', '1', '2022-03-04 23:43:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(289, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Policy%4Operational.evtx', '1', '2022-03-04 23:43:55', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(290, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PowerShell%4Admin.evtx', '138', '2022-03-04 23:43:55', 'ef078d503c95266a0e7ea8fdef09385aedde0f7a30aa01797a976e748666b092'),
(291, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PowerShell%4Operational.evtx', '151992', '2022-03-04 23:44:05', 'd8efa930f035d6ca4a098ed4da6c335c835f38391166f438ee2b0d1a98f08359'),
(292, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PowerShell-DesiredStateConfiguration-FileDownloadManager%4Operational.evtx', '1', '2022-03-04 23:44:05', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(293, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PrintService%4Admin.evtx', '59', '2022-03-04 23:44:05', '46eb47775391a93c8ce4a10538df4ab530e88b311b737e25a2ff0cc36c524b16'),
(294, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Privacy-Auditing%4Operational.evtx', '502', '2022-03-04 23:44:05', '951fdbd3b0f97357ed2dfcb7fa917f76ae6bebef57881c28d4bf2d5993c3a8f3'),
(295, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Program-Compatibility-Assistant%4CompatAfterUpgrade.evtx', '1', '2022-03-04 23:44:05', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(296, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Provisioning-Diagnostics-Provider%4Admin.evtx', '978', '2022-03-04 23:44:06', '04e908c04142faa957e5ab8d9c9ae43b9705f5e4e9627145148a33e06f6a6485'),
(297, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Provisioning-Diagnostics-Provider%4AutoPilot.evtx', '1', '2022-03-04 23:44:06', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(298, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Provisioning-Diagnostics-Provider%4ManagementService.evtx', '1', '2022-03-04 23:44:06', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(299, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PushNotification-Platform%4Admin.evtx', '1', '2022-03-04 23:44:06', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(300, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-PushNotification-Platform%4Operational.evtx', '5242', '2022-03-04 23:44:07', '81591f4da0db546fc403a61895a63d3d8030f774265df80c9d1d81310321e557'),
(301, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ReadyBoost%4Operational.evtx', '75', '2022-03-04 23:44:07', '30e9bba49724d1400821947ec140bb69a3866fa83211b81ecdd27f3d32cb5ab9'),
(302, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ReadyBoostDriver%4Operational.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(303, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ReFS%4Operational.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(304, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Regsvr32%4Operational.evtx', '40', '2022-03-04 23:44:07', '8ebe649ba9f402221f308a98f5e0fe8e7497f5e049549806637d5c8dd6e225a0'),
(305, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteApp and Desktop Connections%4Admin.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(306, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteApp and Desktop Connections%4Operational.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(307, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteAssistance%4Admin.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(308, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteAssistance%4Operational.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(309, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteDesktopServices-RdpCoreTS%4Admin.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(310, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteDesktopServices-RdpCoreTS%4Operational.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(311, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RemoteDesktopServices-SessionServices%4Operational.evtx', '1', '2022-03-04 23:44:07', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(312, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Resource-Exhaustion-Detector%4Operational.evtx', '612', '2022-03-04 23:44:08', 'f23dab377f57b6c0097d810ed56fd29b09b67eb6a140c90954d4fec076b3df7f'),
(313, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Resource-Exhaustion-Resolver%4Operational.evtx', '151', '2022-03-04 23:44:08', '9201b26db67bade1b1dd87b74ac4011d142cf196e72e96151fe720df2f715c28'),
(314, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RestartManager%4Operational.evtx', '1', '2022-03-04 23:44:08', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(315, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RetailDemo%4Admin.evtx', '1', '2022-03-04 23:44:08', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(316, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-RetailDemo%4Operational.evtx', '1', '2022-03-04 23:44:08', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(317, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SearchUI%4Operational.evtx', '1', '2022-03-04 23:44:08', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(318, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-Audit-Configuration-Client%4Operational.evtx', '1', '2022-03-04 23:44:08', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(319, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-EnterpriseData-FileRevocationManager%4Operational.evtx', '1', '2022-03-04 23:44:09', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(320, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-LessPrivilegedAppContainer%4Operational.evtx', '2477', '2022-03-04 23:44:09', '6e2dfad3aa6dfab376152f3c4db73d39f3bbc6a5e9a0f5a7743f07c6d209647d'),
(321, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-Mitigations%4KernelMode.evtx', '2084', '2022-03-04 23:44:10', 'bd23f84f2057423f8061c11d6c1fdae9b6368c4eb966ae3c57e9efaac117ef83'),
(322, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-Mitigations%4UserMode.evtx', '1', '2022-03-04 23:44:10', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(323, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-Netlogon%4Operational.evtx', '1', '2022-03-04 23:44:10', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(324, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-SPP-UX-GenuineCenter-Logging%4Operational.evtx', '1', '2022-03-04 23:44:10', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(325, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-SPP-UX-Notifications%4ActionCenter.evtx', '11', '2022-03-04 23:44:10', '320c9d8f746fc30c821418466245085c39bb54ab9800123ffe3b18a0cdb54bf4'),
(326, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Security-UserConsentVerifier%4Audit.evtx', '1', '2022-03-04 23:44:10', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(327, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SecurityMitigationsBroker%4Operational.evtx', '1', '2022-03-04 23:44:10', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(328, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-ConnectedAccountState%4ActionCenter.evtx', '33', '2022-03-04 23:44:10', '8cc6baa7b000ec52e32aeec4c2418558ea7f132508efbc331a1c3f1e6e5788c1'),
(329, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-Core%4ActionCenter.evtx', '1', '2022-03-04 23:44:10', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(330, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-Core%4AppDefaults.evtx', '909', '2022-03-04 23:44:11', '293c71cbd63e08fbb5f26451edccd273be082a8cd755d3465bae302277b04366'),
(331, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-Core%4LogonTasksChannel.evtx', '1', '2022-03-04 23:44:11', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(332, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Shell-Core%4Operational.evtx', '2659', '2022-03-04 23:44:12', 'd6eaf2699c9a5039f550f0612408b62e86b87f0baf1e438abb068641d91c2085'),
(333, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-ShellCommon-StartLayoutPopulation%4Operational.evtx', '909', '2022-03-04 23:44:12', 'ae4ae4fc3c761d24d9e84457edc0ad5e567501750eb8f734cfa7141b23c641d6'),
(334, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmartCard-Audit%4Authentication.evtx', '1', '2022-03-04 23:44:12', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(335, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmartCard-DeviceEnum%4Operational.evtx', '1', '2022-03-04 23:44:12', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(336, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmartCard-TPM-VCard-Module%4Admin.evtx', '1', '2022-03-04 23:44:12', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(337, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmartCard-TPM-VCard-Module%4Operational.evtx', '1', '2022-03-04 23:44:12', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(338, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmbClient%4Audit.evtx', '12', '2022-03-04 23:44:12', 'a8790009c8409dd7581d726463e9a9d8d91c3042c3e9c8d9b2471368e13fe09b'),
(339, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmbClient%4Connectivity.evtx', '36405', '2022-03-04 23:44:16', 'f1625ba82a9cde9dfdb36dd171198e8e23639674e2c67889fc71611a1e0226ef'),
(340, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBClient%4Operational.evtx', '1', '2022-03-04 23:44:16', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(341, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SmbClient%4Security.evtx', '1043', '2022-03-04 23:44:17', '28e671038d4336e1d710896eebdf3b2aaeb04a3d59c03e5b78840d1d374f479f'),
(342, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBServer%4Audit.evtx', '1', '2022-03-04 23:44:17', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(343, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBServer%4Connectivity.evtx', '1', '2022-03-04 23:44:17', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(344, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBServer%4Operational.evtx', '410', '2022-03-04 23:44:18', '84795a570d148c8f9c69e0970968ce4fc565369e4c801fe958c4eeb7799f7540'),
(345, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBServer%4Security.evtx', '1', '2022-03-04 23:44:18', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(346, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBWitnessClient%4Admin.evtx', '1', '2022-03-04 23:44:18', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(347, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SMBWitnessClient%4Informational.evtx', '1', '2022-03-04 23:44:18', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(348, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StateRepository%4Operational.evtx', '9963', '2022-03-04 23:44:20', '8b5dd8a6d3aeb2aa562c76e569eea12fc522da93c2f171eb784a144557768d34'),
(349, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StateRepository%4Restricted.evtx', '1', '2022-03-04 23:44:20', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(350, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-ClassPnP%4Operational.evtx', '2858', '2022-03-04 23:44:21', '213913bdd7298437811d0294d8dacf8236652aada41e714c21063620ad204bbf'),
(351, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-NvmeDisk%4Operational.evtx', '1', '2022-03-04 23:44:21', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(352, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-Storport%4Health.evtx', '663', '2022-03-04 23:44:22', '47ff0f0d6177eb280e8afb0e603a77d507cd124e27caa6919108cd6265b44242'),
(353, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-Storport%4Operational.evtx', '123335', '2022-03-04 23:44:26', 'd814c966a7d16d21b669b071b12c2a364ed213f2712f836aa0e7679341ecf8ba'),
(354, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storage-Tiering%4Admin.evtx', '1', '2022-03-04 23:44:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(355, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageManagement%4Operational.evtx', '180', '2022-03-04 23:44:26', '11a8a1455e43de2ec989ee21f2fb469231dcc9cc5e57f83fff5424fdb3db5638'),
(356, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageManagement-PartUtil%4Operational.evtx', '1', '2022-03-04 23:44:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(357, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSettings%4Diagnostic.evtx', '1', '2022-03-04 23:44:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(358, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Api%4Operational.evtx', '1', '2022-03-04 23:44:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(359, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Driver%4Diagnostic.evtx', '1', '2022-03-04 23:44:26', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(360, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Driver%4Operational.evtx', '357', '2022-03-04 23:44:26', 'bab0fb43b22b2d781b32d093a7c5b0147ffc5dcabe1539c7b9969213c5320c46'),
(361, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-ManagementAgent%4WHC.evtx', '1', '2022-03-04 23:44:26', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(362, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Parser%4Diagnostic.evtx', '1', '2022-03-04 23:44:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(363, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-Parser%4Operational.evtx', '1', '2022-03-04 23:44:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(364, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-SpaceManager%4Diagnostic.evtx', '1', '2022-03-04 23:44:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(365, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-StorageSpaces-SpaceManager%4Operational.evtx', '1', '2022-03-04 23:44:26', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(366, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Store%4Operational.evtx', '37549', '2022-03-04 23:44:39', '8a5d58da11e34f676817d4f5ea1dbaad334efa5af455c59ae294849c68ba58d8'),
(367, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Storsvc%4Diagnostic.evtx', '623', '2022-03-04 23:44:39', '8182f3158fdd01f58a73e2a270bc005ba4a95a21b0321c574c376dfe98e1609f'),
(368, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-SystemSettingsThreshold%4Operational.evtx', '1', '2022-03-04 23:44:39', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(369, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TaskScheduler%4Maintenance.evtx', '1202', '2022-03-04 23:44:40', '92ed5f04646bd15fd523a89d3e255d060262f11a568b5abac45817295c8047f1'),
(370, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TCPIP%4Operational.evtx', '1', '2022-03-04 23:44:40', '8534891ff8f28d1628a922310641fcaf6d77bcfb976bdda9fe6774eaef5e91af'),
(371, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-ClientUSBDevices%4Admin.evtx', '188', '2022-03-04 23:44:40', 'd418ebcacafe2fdb9f15cad93bc3f34307b485d035ae8d396e38fbffb29ba2e6'),
(372, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-ClientUSBDevices%4Operational.evtx', '761', '2022-03-04 23:44:40', '53e825245fa4727c2c9858aa21196e8f88a62983d0136b53a2b9491779e8cfb2'),
(373, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-LocalSessionManager%4Admin.evtx', '1', '2022-03-04 23:44:40', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(374, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-LocalSessionManager%4Operational.evtx', '1179', '2022-03-04 23:44:41', '3af87cb58a5ef51d831189209bdcf4642ba90eb54f962bfb892bfefc12a080c4'),
(375, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-PnPDevices%4Admin.evtx', '27', '2022-03-04 23:44:41', 'ecc45b245d24bd2ec10c9d89aea087a8d60b06a033bf3b9dc370a883305a7c48'),
(376, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-PnPDevices%4Operational.evtx', '238', '2022-03-04 23:44:41', '6f9e94343e09959028b91b1ce621ac6bc6f6d0553123dc0d8b45f0fd6c925baf'),
(377, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-Printers%4Admin.evtx', '164', '2022-03-04 23:44:41', 'ab290da807804d08271ce23adc54cfe87363457ab06c03297831fdbdc47f84e3'),
(378, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-Printers%4Operational.evtx', '161', '2022-03-04 23:44:41', 'a0d1f25d008c198e13fb428c9b494aee51573d3e8a18e0dec313fe355d6c03de'),
(379, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-RDPClient%4Operational.evtx', '213', '2022-03-04 23:44:41', '0d3a4873fe73efeae8064800f0cf29307ab5fa557bb53454bd0f091b69dd4537'),
(380, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-RemoteConnectionManager%4Admin.evtx', '141', '2022-03-04 23:44:41', '2c47fe4248215c81e5b258bf8be94be53505aa8b260fa9c036a9ae933ce0b740'),
(381, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TerminalServices-RemoteConnectionManager%4Operational.evtx', '218', '2022-03-04 23:44:41', '47983dfe1a13a50f32a2a4b6c0fc98aa66018711bbd3c72758215fd3c86ca8fb'),
(382, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Time-Service%4Operational.evtx', '14244', '2022-03-04 23:44:42', 'f7c4b416ef27a0eebdb194f4a2cbdf42ad04256f9171a4b1169d4278df047c96'),
(383, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Time-Service-PTP-Provider%4PTP-Operational.evtx', '212', '2022-03-04 23:44:42', '85278b0474f7b8a01a9bca8c9d6d388b05f5e20617aa918bab90aac2cea2ec82'),
(384, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Troubleshooting-Recommended%4Admin.evtx', '436', '2022-03-04 23:44:42', '49398d7f3a299964bd830ff6912eaf5f6f4870d4b44d6351217f9c132ad4626b'),
(385, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Troubleshooting-Recommended%4Operational.evtx', '21', '2022-03-04 23:44:42', '1030ad9c72cfaa285dd6a486d6eab11b6669de477e24d89299a2854ce16cebad'),
(386, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TWinUI%4Operational.evtx', '301', '2022-03-04 23:44:43', '30d41c6914a1c36c61de5179efaca7bb6f746b9673ff99ecef384e50f2981dc7'),
(387, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TZSync%4Operational.evtx', '63', '2022-03-04 23:44:43', '890c09344e1464f91320522681243293e5fdd0434a27341930a26400e444d082'),
(388, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-TZUtil%4Operational.evtx', '1393', '2022-03-04 23:44:43', '319f6ae282312cb01dd58f71ac776a6c74ad6fb863de7b54a51e311c96513d29'),
(389, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UAC%4Operational.evtx', '377', '2022-03-04 23:44:43', '52d5e80a01cb5fc0cddb112f756beca0e0e09a7f0fdc89b10c3c682cf64680dc'),
(390, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UAC-FileVirtualization%4Operational.evtx', '188', '2022-03-04 23:44:44', 'd694c43bd55c99136b86d4cba317398d5a68066ca464ede0fca335f2da81ade2'),
(391, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UniversalTelemetryClient%4Operational.evtx', '5190', '2022-03-04 23:44:44', '1dba00969c3bdf59e5b320530f1f2c2b08ba3f5942c8712c166e3b6d24463fd2'),
(392, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-User Control Panel%4Operational.evtx', '218', '2022-03-04 23:44:44', '43184c1b5de2b08ab5a27e555826c83b08106dafaef224308ca50b47f520f235'),
(393, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-User Device Registration%4Admin.evtx', '367', '2022-03-04 23:44:45', '364733811bcff43024ddb568cdeab515c37e0dcd2942ae0c98b821a4d55eef56'),
(394, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-User Profile Service%4Operational.evtx', '1416', '2022-03-04 23:44:46', '4576c1cc2558e99a74f6db74d3c35506d02d7d93ba22cf7bead4e3dbcd985f11'),
(395, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-User-Loader%4Operational.evtx', '142', '2022-03-04 23:44:46', 'b2250822fcec82f38b1e2d0137e689f6044efacaafa7bd9f764fee4f0d9f6912'),
(396, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UserPnp%4ActionCenter.evtx', '1', '2022-03-04 23:44:46', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(397, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-UserPnp%4DeviceInstall.evtx', '653', '2022-03-04 23:44:46', '8d4b597a71bba990c9d5051b4a23e93a2f154202d68d64472657991ed169065f'),
(398, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VDRVROOT%4Operational.evtx', '224', '2022-03-04 23:44:46', 'babf01e71dd05eb56608f29d51388b4561640f7d043e5fce26b66d37b4fe9fc0'),
(399, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VerifyHardwareSecurity%4Admin.evtx', '158', '2022-03-04 23:44:46', '53568f2538dc6bbc3ea4b8e05a9c2fb399594de1e9b174e6c5af227730b7ee3c'),
(400, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VHDMP-Operational.evtx', '3244', '2022-03-04 23:44:47', '189394005726eef9c88115f4cdc7288eed432ceef79a667073068b327f0aa5c2'),
(401, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VolumeSnapshot-Driver%4Operational.evtx', '1529', '2022-03-04 23:44:48', '28a8fa5109d38bb0629332de7dabc0a2906d5e44d57e022a2e067855189e5608'),
(402, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VPN%4Operational.evtx', '1', '2022-03-04 23:44:48', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(403, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-VPN-Client%4Operational.evtx', '169', '2022-03-04 23:44:48', '553225ce7c25e0d74cb22b98f9e7079f69d676f48322b993c9c781b97a9adc33'),
(404, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Wcmsvc%4Operational.evtx', '6016', '2022-03-04 23:44:49', '9ddd54eb55d65bd76eda160f86a158b8be9d0d05fb0ba3542b5dcde1bf0a2306'),
(405, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WebAuthN%4Operational.evtx', '192', '2022-03-04 23:44:49', 'c82ac58a80537bd0bfc4c4de8fb8db08b7bd3d7f7061783b3033856525d86cce'),
(406, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WER-Diag%4Operational.evtx', '83', '2022-03-04 23:44:49', '0126d8b96cf625034f7ee1ceadb9b76867a73935baaefcd3c00ae3e02c2bb441'),
(407, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WER-PayloadHealth%4Operational.evtx', '1842', '2022-03-04 23:44:49', 'c600b6d59b5d24044d37bda06b51b2712c45eb663029c57bcce7d37f7e64964e'),
(408, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WerKernel%4Operational.evtx', '940', '2022-03-04 23:44:50', '27cf2398ad0cbb1c6606fd033fcb426ab005fa2f4d843e59fb256cd2eb70f3cb'),
(409, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WFP%4Operational.evtx', '522', '2022-03-04 23:44:50', 'ad03e5c2189733975a0829ad544716a01e09e3b0b11dc8e7c38ca7ff08e7ab99'),
(410, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Win32k%4Operational.evtx', '161', '2022-03-04 23:44:51', '40cbd21baa72535586eb00c94b68243cad7999588c0ed7328eb3feb197668154'),
(411, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Defender%4Operational.evtx', '1870', '2022-03-04 23:44:51', '85af5c987ea6522d2842e3f71667c7a92f1d31881cdc8a8686090be3a7a95ef7'),
(412, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Defender%4WHC.evtx', '1', '2022-03-04 23:44:51', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(413, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Firewall With Advanced Security%4ConnectionSecurity.evtx', '1', '2022-03-04 23:44:51', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(414, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Firewall With Advanced Security%4Firewall.evtx', '2768', '2022-03-04 23:44:52', '23ff68dbdba3b98662c330bcd7b87952b5a6e7aae4e641c9214c8ead13bb1a69'),
(415, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Windows Firewall With Advanced Security%4FirewallDiagnostics.evtx', '87', '2022-03-04 23:44:52', 'c1ef788baef8cd94c0bfc8436895156678809e9a58f4f61dc52dc09992ea6e8f'),
(416, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WindowsBackup%4ActionCenter.evtx', '32', '2022-03-04 23:44:52', 'b4571c05ab21d07d0791928eb41a456c21a26f0048cb0046242d09e299cea8ac'),
(417, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WindowsSystemAssessmentTool%4Operational.evtx', '537', '2022-03-04 23:44:53', '3a0a6b5b14a3883af65eed2fef7b6f4c6e6c635330c93bc4634263ae2a951b2f'),
(418, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WindowsUpdateClient%4Operational.evtx', '2048', '2022-03-04 23:44:53', 'b7bd0fb9c3bd4f8b839ae06d215b0db740d6501ad7ec8280e38d256453bdf446'),
(419, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WinHttp%4Operational.evtx', '412', '2022-03-04 23:44:53', '8369f09e0aec2eae2606ae0e387624a88c99900909849538d4fec0c413a0c65f'),
(420, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WinINet%4Operational.evtx', '150', '2022-03-04 23:44:53', 'f09566f9217c88c5834552c3194ecd5d47757246415c0934c8931fe0848f1058'),
(421, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WinINet-Config%4ProxyConfigChanged.evtx', '22', '2022-03-04 23:44:53', 'cf21b786996b0b41d80bedc657fb3d540dc5abbc2053579f7d497bb6c00fc032'),
(422, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Winlogon%4Operational.evtx', '3966', '2022-03-04 23:44:54', 'ffd707a79e9c4ca004ac02592f5909fd0a32389142c5984b6c17a2dd9d1642ae'),
(423, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WinRM%4Operational.evtx', '2876', '2022-03-04 23:44:55', '10c4192cd5d8678b1db30a2997d20ca476c567a221e77263774d2bc08aa1a2ed'),
(424, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Winsock-WS2HELP%4Operational.evtx', '27', '2022-03-04 23:44:55', 'ecc45b245d24bd2ec10c9d89aea087a8d60b06a033bf3b9dc370a883305a7c48'),
(425, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Wired-AutoConfig%4Operational.evtx', '97', '2022-03-04 23:44:55', 'dde853882bef91f8d4c7fd17768bc97c4805bdeb6683e5e8d0f49ff3caa807e3'),
(426, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WLAN-AutoConfig%4Operational.evtx', '2627', '2022-03-04 23:44:55', '90f4f13d729b2fa51823017495afdea2f848ec477a0ed40b87a46d71a1122d63'),
(427, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WMI-Activity%4Operational.evtx', '2107', '2022-03-04 23:44:56', 'c30ae0daba4a74b453f008594206220c1709e024b4516b3d420cd071e109211a'),
(428, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WMPNSS-Service%4Operational.evtx', '27', '2022-03-04 23:44:56', 'ecc45b245d24bd2ec10c9d89aea087a8d60b06a033bf3b9dc370a883305a7c48'),
(429, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WorkFolders%4Operational.evtx', '225', '2022-03-04 23:44:56', 'da0ecd291c7721e21df0820ba830fb6cf7ed7f0ce597f9b25a0e3a7e111e73ac'),
(430, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WorkFolders%4WHC.evtx', '33', '2022-03-04 23:44:56', '8cc6baa7b000ec52e32aeec4c2418558ea7f132508efbc331a1c3f1e6e5788c1'),
(431, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-Workplace Join%4Admin.evtx', '251', '2022-03-04 23:44:56', '01d6530a2980c00d2fc2deeb0c839ee676cb448918f814c48e9997e5b06d4cf0'),
(432, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WPD-ClassInstaller%4Operational.evtx', '47', '2022-03-04 23:44:56', 'ac7e22687b30ff6c1c3cc053724e9080989604f1f0e542dde7d6b39d45d8b435'),
(433, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WPD-CompositeClassDriver%4Operational.evtx', '142', '2022-03-04 23:44:56', 'a4c052081f80e85f2c1faea24bea46f880e351ea7e0187427f6aaf62669f0366'),
(434, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WPD-MTPClassDriver%4Operational.evtx', '375', '2022-03-04 23:44:57', 'f084829b27efd998cbe772d0641ea3049e3ef6a9c848beb67933a5240b9df75e'),
(435, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-Windows-WWAN-SVC-Events%4Operational.evtx', '248', '2022-03-04 23:44:57', '46952274482396ced2ffe874c14d578a282ddfa426aaf9ee0ea76fa8970e7e95'),
(436, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Microsoft-WindowsPhone-Connectivity-WiFiConnSvc-Channel.evtx', '1', '2022-03-04 23:44:57', 'f5f9e97a6b1ec8d46a9bd5b9d4ccae96521b85517b0337b248814d2e974a968b'),
(437, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'OAlerts.evtx', '654', '2022-03-04 23:44:57', '302ede081d1c6fd0608c23c30f4417466ab590ec75852e89b0056c5a878c7cd8'),
(438, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'OneApp_IGCC.evtx', '1254', '2022-03-04 23:44:58', 'e2ce8f3744a7f708561cbfd9cf8de58ae80b7566553442390c7b2ed1ca787490'),
(439, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'OpenSSH%4Admin.evtx', '228', '2022-03-04 23:44:58', '585261b0118ae5e2f40ef17cae38483a13baf3e05ebb6933b1f4410bae764d1d'),
(440, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'OpenSSH%4Operational.evtx', '117', '2022-03-04 23:44:58', 'ef7b3c6d450f0f236177a613d1b8a34df6c7c0613a8d3c426e789836eced5929'),
(441, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Parameters.evtx', '142', '2022-03-04 23:44:58', 'd810fc5ea4bf90d580c8f8ba382cf1aba45a4cd2037481d1794be8c3c28c595a'),
(442, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Security.evtx', '54691', '2022-03-04 23:45:10', 'b02e11b6e6a4614faa0e091755ffddbee31d4a5d71596af26db102cc73f3fbbb'),
(443, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Setup.evtx', '254', '2022-03-04 23:45:10', '72af02fd71ba88040f62c0180d157796e58945e2aa9e9287c8015424cc062b03'),
(444, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'SMSApi.evtx', '254', '2022-03-04 23:45:10', 'd203de4d76e1e6f0fc997d16b4895220164f4c9f38bb61ea70835d007d5f5931'),
(445, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'State.evtx', '54', '2022-03-04 23:45:10', '0a86db3eab61391c2d09635b996197f2d86b76c784c6145a3ccc2f820818d389'),
(446, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'System.evtx', '63557', '2022-03-04 23:45:18', '9d31e225d96ce6170180b267851d063fb98122e7437b16ee750afbe17e5d4161'),
(447, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'USER_ESRV_SVC_QUEENCREEK.evtx', '41', '2022-03-04 23:45:18', 'e80db993b1a8ca3590fd23b8cefc2677707cddc442bb68a8c619edf77fe77a6a'),
(448, 'Code_Zero_Four', 'Linux 5.10.16.3-microsoft-standard-WSL2', '/mnt/c/Windows/System32/winevt/Logs/', 'Windows PowerShell.evtx', '50732', '2022-03-04 23:45:20', 'd88f5b4426baa35d8a0c2d577e7f40ed60ff6fb0fb1a3a52b4b55f4d9438cc70');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_pattern`
--

CREATE TABLE `doc_pdpa_pattern` (
  `pattern_id` int(11) NOT NULL,
  `pattern_name` varchar(255) DEFAULT NULL,
  `doc_id` int(11) DEFAULT NULL,
  `pattern_tag` varchar(255) DEFAULT NULL,
  `pattern_label` varchar(255) DEFAULT NULL,
  `pattern_create` datetime NOT NULL DEFAULT current_timestamp(),
  `pattern_start_date` datetime DEFAULT NULL,
  `pattern_total_date` int(11) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `pattern_type_data_file` tinyint(1) DEFAULT NULL,
  `pattern_type_data_database` tinyint(1) DEFAULT NULL,
  `pattern_type_data_database_name` varchar(255) DEFAULT NULL,
  `pattern_storage_method_inside` tinyint(1) DEFAULT NULL,
  `pattern_storage_method_outside` tinyint(1) DEFAULT NULL,
  `pattern_storage_method_outside_name` varchar(255) DEFAULT NULL,
  `pattern_storage_method_outside_device` tinyint(1) DEFAULT NULL,
  `pattern_storage_method_outside_device_name` varchar(255) DEFAULT NULL,
  `pattern_storage_method_outside_agent` tinyint(1) DEFAULT NULL,
  `pattern_storage_method_outside_agent_name` varchar(255) DEFAULT NULL,
  `pattern_storage_method_outside_database_outside` tinyint(1) DEFAULT NULL,
  `pattern_storage_method_outside_database_outside_name` varchar(255) DEFAULT NULL,
  `pattern_processing_base_id` int(11) DEFAULT NULL,
  `pattern_processor_inside` tinyint(1) DEFAULT NULL,
  `pattern_processor_inside_name` varchar(255) DEFAULT NULL,
  `pattern_processor_outside` tinyint(1) DEFAULT NULL,
  `pattern_processor_outside_name` varchar(255) DEFAULT NULL,
  `pattern_set_end_date` tinyint(1) DEFAULT NULL,
  `pattern_set_end_date_total` int(11) DEFAULT NULL,
  `pattern_dpo_approve_process` tinyint(1) DEFAULT NULL,
  `pattern_dpo_approve_raw_file_out` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_pattern`
--

INSERT INTO `doc_pdpa_pattern` (`pattern_id`, `pattern_name`, `doc_id`, `pattern_tag`, `pattern_label`, `pattern_create`, `pattern_start_date`, `pattern_total_date`, `user_id`, `pattern_type_data_file`, `pattern_type_data_database`, `pattern_type_data_database_name`, `pattern_storage_method_inside`, `pattern_storage_method_outside`, `pattern_storage_method_outside_name`, `pattern_storage_method_outside_device`, `pattern_storage_method_outside_device_name`, `pattern_storage_method_outside_agent`, `pattern_storage_method_outside_agent_name`, `pattern_storage_method_outside_database_outside`, `pattern_storage_method_outside_database_outside_name`, `pattern_processing_base_id`, `pattern_processor_inside`, `pattern_processor_inside_name`, `pattern_processor_outside`, `pattern_processor_outside_name`, `pattern_set_end_date`, `pattern_set_end_date_total`, `pattern_dpo_approve_process`, `pattern_dpo_approve_raw_file_out`) VALUES
(10, 'Log จาก Server A', 297, 'g,สำคัญ,ธงแดง', 'สำรอง', '2022-03-29 23:28:58', '2022-03-29 00:00:00', 90, 'รัชชานนท์', 1, 1, 'PDPA', 1, 1, '172.16.20.111', 1, 'PC-1', 1, 'Agent_log0_hash', 1, 'doc_pdpa_Server', 7, 1, '192.168.1.100', 1, '172.16.20.111', 1, 180, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_pattern_processing_base`
--

CREATE TABLE `doc_pdpa_pattern_processing_base` (
  `pattern_processing_base_id` int(11) NOT NULL,
  `pattern_processing_base_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_pattern_processing_base`
--

INSERT INTO `doc_pdpa_pattern_processing_base` (`pattern_processing_base_id`, `pattern_processing_base_name`) VALUES
(1, 'หน้าที่ตามกฏหมาย มาตรา 24'),
(2, 'ภารกิจของรัฐ มาตรา 24'),
(3, 'ผลประโยชน์โดยชอบธรรม มาตรา 24'),
(4, 'สัญญา มาตรา 24'),
(5, 'ประโยชน์ต่อชีวิต มาตรา 24'),
(6, 'ความยินยอม มาตรา 24'),
(7, 'วิจัยและสถิติ มาตรา 24');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_position`
--

CREATE TABLE `doc_pdpa_position` (
  `position_id` int(11) NOT NULL,
  `position_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_position`
--

INSERT INTO `doc_pdpa_position` (`position_id`, `position_name`) VALUES
(1, ' เจ้าของข้อมูลส่วนบุคคล'),
(2, 'ผู้ควบคุมข้อมูลส่วนบุคคล '),
(3, 'ผู้ประมวลผลข้อมูลส่วนบุคคล ');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_tag`
--

CREATE TABLE `doc_pdpa_tag` (
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_tag`
--

INSERT INTO `doc_pdpa_tag` (`tag_id`, `tag_name`) VALUES
(13, 'สำคัญ'),
(14, 'ธงแดง'),
(15, 'g'),
(16, 'b'),
(17, 'xx'),
(18, 'oo');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_user`
--

CREATE TABLE `doc_pdpa_user` (
  `user_id` varchar(50) NOT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  `user_picture` varchar(255) DEFAULT NULL,
  `user_prefix` varchar(50) DEFAULT NULL,
  `user_fullname` varchar(255) DEFAULT NULL,
  `user_usename` varchar(255) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `user_detail` text DEFAULT NULL,
  `user_orgtel` varchar(50) DEFAULT NULL,
  `user_orgtal_ext` varchar(10) DEFAULT NULL,
  `user_mobile` varchar(50) DEFAULT NULL,
  `user_mobile_sos` varchar(50) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_line` varchar(50) DEFAULT NULL,
  `user_username` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_ck2fa` int(11) DEFAULT NULL,
  `user_ck2fa_email` int(11) DEFAULT NULL,
  `user_ck2fa_sms` int(11) DEFAULT NULL,
  `user_ck2fa_alltra` int(11) DEFAULT NULL,
  `user_ck2fa_login` int(11) DEFAULT NULL,
  `user_ck2fa_config` int(11) DEFAULT NULL,
  `user_ck2fa_edit` int(11) DEFAULT NULL,
  `user_ck2fa_del` int(11) DEFAULT NULL,
  `user_group` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_user`
--

INSERT INTO `doc_pdpa_user` (`user_id`, `user_type_id`, `user_picture`, `user_prefix`, `user_fullname`, `user_usename`, `position_id`, `user_detail`, `user_orgtel`, `user_orgtal_ext`, `user_mobile`, `user_mobile_sos`, `user_email`, `user_line`, `user_username`, `user_password`, `user_ck2fa`, `user_ck2fa_email`, `user_ck2fa_sms`, `user_ck2fa_alltra`, `user_ck2fa_login`, `user_ck2fa_config`, `user_ck2fa_edit`, `user_ck2fa_del`, `user_group`) VALUES
('ซัน', 1, '1-old.jpg', '1', 'กิตติภูมิ', '1', 1, '1', '1', '1', '1', '1', '1', '1', '2', '2', 1, 1, 1, 1, 1, 1, 1, 1, '1'),
('รัชชานนท์', 1, '2.jpg', '1', 'รัชชานนท์ สิทธิโสติ', '1', 1, '1', '1', '1', '1', '1', '1', '1', '1', '1', 1, 1, 1, 1, 1, 1, 1, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_user_type`
--

CREATE TABLE `doc_pdpa_user_type` (
  `user_type_id` int(11) NOT NULL,
  `user_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_user_type`
--

INSERT INTO `doc_pdpa_user_type` (`user_type_id`, `user_type_name`) VALUES
(1, '1'),
(2, '2'),
(3, '3');

-- --------------------------------------------------------

--
-- Table structure for table `doc_pdpa_words`
--

CREATE TABLE `doc_pdpa_words` (
  `words_id` int(11) NOT NULL,
  `words_often` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doc_pdpa_words`
--

INSERT INTO `doc_pdpa_words` (`words_id`, `words_often`) VALUES
(14, 'สำนักงาน'),
(15, 'องค์กร');

-- --------------------------------------------------------

--
-- Table structure for table `domaingroup`
--

CREATE TABLE `domaingroup` (
  `id_dg` int(11) NOT NULL,
  `namedomain_dg` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `message` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `date_dg` datetime DEFAULT current_timestamp(),
  `status_dg` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `domaingroup`
--

INSERT INTO `domaingroup` (`id_dg`, `namedomain_dg`, `message`, `date_dg`, `status_dg`) VALUES
(278, 'https://www.PDPA.com', 'PDPA', '2022-03-10 23:51:34', 1),
(279, 'https://www.E-mail.com', 'email\r\n', '2022-03-10 23:55:16', 1),
(301, 'https://www.TEST.com', 'test เพิ่มโดเมน เเล้วก็ให้สร้างไฟล์ html', '2022-03-26 05:22:38', 0),
(302, 'https://www.sgc.co.th', 'test', '2022-03-28 19:37:03', 0),
(304, 'https://www.buildingware.cf', 'www.buildingware.cf', '2022-03-30 22:55:51', 1),
(305, 'https://www.Tesssssssssss.com', 'www.tesssssssssss.com', '2022-03-31 23:07:46', 1),
(306, 'https://www.witttttttttt.com', 'https://www.witttttttttt.com', '2022-04-08 01:27:40', NULL),
(307, 'https://www.wwwwwwwwwwww.com', 'https://www.wwwwwwwwwwww.com', '2022-04-08 01:48:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `domain_setting_cookiepolicy`
--

CREATE TABLE `domain_setting_cookiepolicy` (
  `id_dsc` int(11) NOT NULL,
  `domain_id` int(11) DEFAULT NULL,
  `cookiepolicy_id` int(11) DEFAULT NULL,
  `policylog_id` int(11) DEFAULT NULL,
  `name_cookietype` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `detail_cookie` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `approve` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `domain_setting_cookiepolicy`
--

INSERT INTO `domain_setting_cookiepolicy` (`id_dsc`, `domain_id`, `cookiepolicy_id`, `policylog_id`, `name_cookietype`, `detail_cookie`, `approve`) VALUES
(1165, 278, 76, NULL, 'จำเป็น', 'คุกกี้ที่จำเป็นจะช่วยทำให้เว็บไซต์สามารถใช้งานได้โดยเปิดใช้งานฟังก์ชันพื้นฐาน เช่น ระบบนำทางของหน้าและการเข้าถึงพื้นที่ปลอดภัยของเว็บไซต์ เว็บไซต์จะไม่สามารถทำงานได้อย่างถูกต้องหากไม่มีคุกกี้เหล่านี้', 0),
(1166, 278, 77, NULL, 'การกำหนดค่า', 'คุกกี้ประเภทการตั้งค่าจะช่วยให้เว็บไซต์จดจำข้อมูลซึ่งเปลี่ยนวิธีการที่เว็บไซต์ทำงานหรือแสดงผล เช่น ภาษาที่คุณกำหนดเป็นค่าที่ชื่นชอบหรือภูมิภาคที่คุณอาศัยอยู่', 0),
(1167, 278, 78, NULL, 'สถิติ', 'คุกกี้ประเภทสถิติจะช่วยให้เจ้าของเว็บไซต์ทำความเข้าใจว่าผู้เยี่ยมชมมีปฏิสัมพันธ์กับเว็บไซต์เช่นใด โดยการเก็บและการรายงานข้อมูลโดยไม่ระบุชื่อ', 0),
(1168, 278, 79, NULL, 'การตลาด', 'คุกกี้ประเภทการตลาดใช้สำหรับการติดตามผู้ใช้ทั่วทั้งเว็บไซต์ เป้าหมายของมันคือเพื่อแสดงโฆษณาที่เกี่ยวข้องและน่าสนใจสำหรับผู้ใช้แต่ละคน และดังนั้นจึงเป็นการเพิ่มคุณค่าให้กับผู้เผยแพร่และผู้โฆษณาบุคคลที่สาม', 0),
(1169, 278, 80, NULL, 'ไม่ได้จำแนกประเภท', 'คุกกี้ที่ไม่ได้รับการจำแนกประเภทคือคุกกี้ที่เรากำลังอยู่ในขั้นตอนการจำแนกประเภท โดยร่วมมือกับผู้ให้บริการคุกกี้แต่ละรายการ', 0),
(1170, 279, 76, 2, 'จำเป็น', 'คุกกี้ที่จำเป็นจะช่วยทำให้เว็บไซต์สามารถใช้งานได้โดยเปิดใช้งานฟังก์ชันพื้นฐาน เช่น ระบบนำทางของหน้าและการเข้าถึงพื้นที่ปลอดภัยของเว็บไซต์ เว็บไซต์จะไม่สามารถทำงานได้อย่างถูกต้องหากไม่มีคุกกี้เหล่านี้', 1),
(1171, 279, 77, 2, 'การกำหนดค่า', 'คุกกี้ประเภทการตั้งค่าจะช่วยให้เว็บไซต์จดจำข้อมูลซึ่งเปลี่ยนวิธีการที่เว็บไซต์ทำงานหรือแสดงผล เช่น ภาษาที่คุณกำหนดเป็นค่าที่ชื่นชอบหรือภูมิภาคที่คุณอาศัยอยู่', 1),
(1172, 279, 78, 2, 'สถิติ', 'คุกกี้ประเภทสถิติจะช่วยให้เจ้าของเว็บไซต์ทำความเข้าใจว่าผู้เยี่ยมชมมีปฏิสัมพันธ์กับเว็บไซต์เช่นใด โดยการเก็บและการรายงานข้อมูลโดยไม่ระบุชื่อ', 1),
(1173, 279, 79, 2, 'การตลาด', 'คุกกี้ประเภทการตลาดใช้สำหรับการติดตามผู้ใช้ทั่วทั้งเว็บไซต์ เป้าหมายของมันคือเพื่อแสดงโฆษณาที่เกี่ยวข้องและน่าสนใจสำหรับผู้ใช้แต่ละคน และดังนั้นจึงเป็นการเพิ่มคุณค่าให้กับผู้เผยแพร่และผู้โฆษณาบุคคลที่สาม', 1),
(1174, 279, 80, 2, 'ไม่ได้จำแนกประเภท', 'คุกกี้ที่ไม่ได้รับการจำแนกประเภทคือคุกกี้ที่เรากำลังอยู่ในขั้นตอนการจำแนกประเภท โดยร่วมมือกับผู้ให้บริการคุกกี้แต่ละรายการ', 1),
(1386, 301, 76, NULL, 'จำเป็น', 'คุกกี้ที่จำเป็นจะช่วยทำให้เว็บไซต์สามารถใช้งานได้โดยเปิดใช้งานฟังก์ชันพื้นฐาน เช่น ระบบนำทางของหน้าและการเข้าถึงพื้นที่ปลอดภัยของเว็บไซต์ เว็บไซต์จะไม่สามารถทำงานได้อย่างถูกต้องหากไม่มีคุกกี้เหล่านี้', 1),
(1387, 301, 77, NULL, 'การกำหนดค่า', 'คุกกี้ประเภทการตั้งค่าจะช่วยให้เว็บไซต์จดจำข้อมูลซึ่งเปลี่ยนวิธีการที่เว็บไซต์ทำงานหรือแสดงผล เช่น ภาษาที่คุณกำหนดเป็นค่าที่ชื่นชอบหรือภูมิภาคที่คุณอาศัยอยู่', 1),
(1388, 301, 78, NULL, 'สถิติ', 'คุกกี้ประเภทสถิติจะช่วยให้เจ้าของเว็บไซต์ทำความเข้าใจว่าผู้เยี่ยมชมมีปฏิสัมพันธ์กับเว็บไซต์เช่นใด โดยการเก็บและการรายงานข้อมูลโดยไม่ระบุชื่อ', 1),
(1389, 301, 79, NULL, 'การตลาด', 'คุกกี้ประเภทการตลาดใช้สำหรับการติดตามผู้ใช้ทั่วทั้งเว็บไซต์ เป้าหมายของมันคือเพื่อแสดงโฆษณาที่เกี่ยวข้องและน่าสนใจสำหรับผู้ใช้แต่ละคน และดังนั้นจึงเป็นการเพิ่มคุณค่าให้กับผู้เผยแพร่และผู้โฆษณาบุคคลที่สาม', 1),
(1390, 301, 80, NULL, 'ไม่ได้จำแนกประเภท', 'คุกกี้ที่ไม่ได้รับการจำแนกประเภทคือคุกกี้ที่เรากำลังอยู่ในขั้นตอนการจำแนกประเภท โดยร่วมมือกับผู้ให้บริการคุกกี้แต่ละรายการ', 1),
(1404, 304, 76, NULL, 'จำเป็น', 'คุกกี้ที่จำเป็นจะช่วยทำให้เว็บไซต์สามารถใช้งานได้โดยเปิดใช้งานฟังก์ชันพื้นฐาน เช่น ระบบนำทางของหน้าและการเข้าถึงพื้นที่ปลอดภัยของเว็บไซต์ เว็บไซต์จะไม่สามารถทำงานได้อย่างถูกต้องหากไม่มีคุกกี้เหล่านี้', 0),
(1405, 304, 77, NULL, 'การกำหนดค่า', 'คุกกี้ประเภทการตั้งค่าจะช่วยให้เว็บไซต์จดจำข้อมูลซึ่งเปลี่ยนวิธีการที่เว็บไซต์ทำงานหรือแสดงผล เช่น ภาษาที่คุณกำหนดเป็นค่าที่ชื่นชอบหรือภูมิภาคที่คุณอาศัยอยู่', 0),
(1406, 304, 78, NULL, 'สถิติ', 'คุกกี้ประเภทสถิติจะช่วยให้เจ้าของเว็บไซต์ทำความเข้าใจว่าผู้เยี่ยมชมมีปฏิสัมพันธ์กับเว็บไซต์เช่นใด โดยการเก็บและการรายงานข้อมูลโดยไม่ระบุชื่อ', 0),
(1407, 304, 79, NULL, 'การตลาด', 'คุกกี้ประเภทการตลาดใช้สำหรับการติดตามผู้ใช้ทั่วทั้งเว็บไซต์ เป้าหมายของมันคือเพื่อแสดงโฆษณาที่เกี่ยวข้องและน่าสนใจสำหรับผู้ใช้แต่ละคน และดังนั้นจึงเป็นการเพิ่มคุณค่าให้กับผู้เผยแพร่และผู้โฆษณาบุคคลที่สาม', 0),
(1408, 304, 80, NULL, 'ไม่ได้จำแนกประเภท', 'คุกกี้ที่ไม่ได้รับการจำแนกประเภทคือคุกกี้ที่เรากำลังอยู่ในขั้นตอนการจำแนกประเภท โดยร่วมมือกับผู้ให้บริการคุกกี้แต่ละรายการ', 0),
(1409, 305, 76, NULL, 'จำเป็น', 'คุกกี้ที่จำเป็นจะช่วยทำให้เว็บไซต์สามารถใช้งานได้โดยเปิดใช้งานฟังก์ชันพื้นฐาน เช่น ระบบนำทางของหน้าและการเข้าถึงพื้นที่ปลอดภัยของเว็บไซต์ เว็บไซต์จะไม่สามารถทำงานได้อย่างถูกต้องหากไม่มีคุกกี้เหล่านี้', 0),
(1410, 305, 77, NULL, 'การกำหนดค่า', 'คุกกี้ประเภทการตั้งค่าจะช่วยให้เว็บไซต์จดจำข้อมูลซึ่งเปลี่ยนวิธีการที่เว็บไซต์ทำงานหรือแสดงผล เช่น ภาษาที่คุณกำหนดเป็นค่าที่ชื่นชอบหรือภูมิภาคที่คุณอาศัยอยู่', 1),
(1411, 305, 78, NULL, 'สถิติ', 'คุกกี้ประเภทสถิติจะช่วยให้เจ้าของเว็บไซต์ทำความเข้าใจว่าผู้เยี่ยมชมมีปฏิสัมพันธ์กับเว็บไซต์เช่นใด โดยการเก็บและการรายงานข้อมูลโดยไม่ระบุชื่อ', 0),
(1412, 305, 79, NULL, 'การตลาด', 'คุกกี้ประเภทการตลาดใช้สำหรับการติดตามผู้ใช้ทั่วทั้งเว็บไซต์ เป้าหมายของมันคือเพื่อแสดงโฆษณาที่เกี่ยวข้องและน่าสนใจสำหรับผู้ใช้แต่ละคน และดังนั้นจึงเป็นการเพิ่มคุณค่าให้กับผู้เผยแพร่และผู้โฆษณาบุคคลที่สาม', 0),
(1413, 305, 80, NULL, 'ไม่ได้จำแนกประเภท', 'คุกกี้ที่ไม่ได้รับการจำแนกประเภทคือคุกกี้ที่เรากำลังอยู่ในขั้นตอนการจำแนกประเภท โดยร่วมมือกับผู้ให้บริการคุกกี้แต่ละรายการ', 1),
(1414, 306, 76, NULL, 'จำเป็น', 'คุกกี้ที่จำเป็นจะช่วยทำให้เว็บไซต์สามารถใช้งานได้โดยเปิดใช้งานฟังก์ชันพื้นฐาน เช่น ระบบนำทางของหน้าและการเข้าถึงพื้นที่ปลอดภัยของเว็บไซต์ เว็บไซต์จะไม่สามารถทำงานได้อย่างถูกต้องหากไม่มีคุกกี้เหล่านี้', 1),
(1415, 306, 77, NULL, 'การกำหนดค่า', 'คุกกี้ประเภทการตั้งค่าจะช่วยให้เว็บไซต์จดจำข้อมูลซึ่งเปลี่ยนวิธีการที่เว็บไซต์ทำงานหรือแสดงผล เช่น ภาษาที่คุณกำหนดเป็นค่าที่ชื่นชอบหรือภูมิภาคที่คุณอาศัยอยู่', 1),
(1416, 306, 78, NULL, 'สถิติ', 'คุกกี้ประเภทสถิติจะช่วยให้เจ้าของเว็บไซต์ทำความเข้าใจว่าผู้เยี่ยมชมมีปฏิสัมพันธ์กับเว็บไซต์เช่นใด โดยการเก็บและการรายงานข้อมูลโดยไม่ระบุชื่อ', 1),
(1417, 306, 79, NULL, 'การตลาด', 'คุกกี้ประเภทการตลาดใช้สำหรับการติดตามผู้ใช้ทั่วทั้งเว็บไซต์ เป้าหมายของมันคือเพื่อแสดงโฆษณาที่เกี่ยวข้องและน่าสนใจสำหรับผู้ใช้แต่ละคน และดังนั้นจึงเป็นการเพิ่มคุณค่าให้กับผู้เผยแพร่และผู้โฆษณาบุคคลที่สาม', 1),
(1418, 306, 80, NULL, 'ไม่ได้จำแนกประเภท', 'คุกกี้ที่ไม่ได้รับการจำแนกประเภทคือคุกกี้ที่เรากำลังอยู่ในขั้นตอนการจำแนกประเภท โดยร่วมมือกับผู้ให้บริการคุกกี้แต่ละรายการ', 1),
(1419, 307, 76, NULL, 'จำเป็น', 'คุกกี้ที่จำเป็นจะช่วยทำให้เว็บไซต์สามารถใช้งานได้โดยเปิดใช้งานฟังก์ชันพื้นฐาน เช่น ระบบนำทางของหน้าและการเข้าถึงพื้นที่ปลอดภัยของเว็บไซต์ เว็บไซต์จะไม่สามารถทำงานได้อย่างถูกต้องหากไม่มีคุกกี้เหล่านี้', 1),
(1420, 307, 77, NULL, 'การกำหนดค่า', 'คุกกี้ประเภทการตั้งค่าจะช่วยให้เว็บไซต์จดจำข้อมูลซึ่งเปลี่ยนวิธีการที่เว็บไซต์ทำงานหรือแสดงผล เช่น ภาษาที่คุณกำหนดเป็นค่าที่ชื่นชอบหรือภูมิภาคที่คุณอาศัยอยู่', 1),
(1421, 307, 78, NULL, 'สถิติ', 'คุกกี้ประเภทสถิติจะช่วยให้เจ้าของเว็บไซต์ทำความเข้าใจว่าผู้เยี่ยมชมมีปฏิสัมพันธ์กับเว็บไซต์เช่นใด โดยการเก็บและการรายงานข้อมูลโดยไม่ระบุชื่อ', 1),
(1422, 307, 79, NULL, 'การตลาด', 'คุกกี้ประเภทการตลาดใช้สำหรับการติดตามผู้ใช้ทั่วทั้งเว็บไซต์ เป้าหมายของมันคือเพื่อแสดงโฆษณาที่เกี่ยวข้องและน่าสนใจสำหรับผู้ใช้แต่ละคน และดังนั้นจึงเป็นการเพิ่มคุณค่าให้กับผู้เผยแพร่และผู้โฆษณาบุคคลที่สาม', 1),
(1423, 307, 80, NULL, 'ไม่ได้จำแนกประเภท', 'คุกกี้ที่ไม่ได้รับการจำแนกประเภทคือคุกกี้ที่เรากำลังอยู่ในขั้นตอนการจำแนกประเภท โดยร่วมมือกับผู้ให้บริการคุกกี้แต่ละรายการ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `domain_setting_tag`
--

CREATE TABLE `domain_setting_tag` (
  `id_dst` int(11) NOT NULL,
  `domaingroup_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `domain_setting_tag`
--

INSERT INTO `domain_setting_tag` (`id_dst`, `domaingroup_id`, `tag_id`) VALUES
(369, 278, 93);

-- --------------------------------------------------------

--
-- Table structure for table `domain_style_policy`
--

CREATE TABLE `domain_style_policy` (
  `id_dsp` int(11) NOT NULL,
  `domaingroup_id` int(11) DEFAULT NULL,
  `dialog_id` int(11) DEFAULT NULL,
  `style_id` int(11) DEFAULT NULL,
  `detail_dialog` varchar(15000) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `domain_style_policy`
--

INSERT INTO `domain_style_policy` (`id_dsp`, `domaingroup_id`, `dialog_id`, `style_id`, `detail_dialog`) VALUES
(175, 278, 1, 124, 'เว็บไซต์นี้ใช้คุกกี้ เราใช้คุกกี้เพื่อปรับเปลี่ยนเนื้อหาและโฆษณาในแบบของคุณ เพื่อเสนอคุณลักษณะที่ใช้กับโซเชียลมีเดียและเพื่อวิเคราะห์การเข้าชมของเรา นอกจากนี้เรายังแชร์ข้อมูลการใช้งานของคุณในเว็บไซต์ของเรากับพาร์ทเนอร์ด้านโซเชียลมีเดีย โฆษณา และการวิเคราะห์ ซึ่งพาร์ทเนอร์เหล่านี้อาจนำข้อมูลดังกล่าวไปรวมกับข้อมูลอื่นๆ ที่คุณมอบให้หรือข้อมูลที่รวบรวมมาจากการที่คุณใช้บริการของพาร์ทเนอร์เองhttps://www.PDPA.com'),
(176, 279, 1, 123, 'เว็บไซต์นี้ใช้คุกกี้เราใช้คุกกี้เพื่อปรับเปลี่ยนเนื้อหาและโฆษณาในแบบของคุณ เพื่อเสนอคุณลักษณะที่ใช้กับโซเชียลมีเดียและเพื่อวิเคราะห์การเข้าชมของเรา นอกจากนี้เรายังแชร์ข้อมูลการใช้งานของคุณในเว็บไซต์ของเรากับพาร์ทเนอร์ด้านโซเชียลมีเดีย โฆษณา และการวิเคราะห์ ซึ่งพาร์ทเนอร์เหล่านี้อาจนำข้อมูลดังกล่าวไปรวมกับข้อมูลอื่นๆ ที่คุณมอบให้หรือข้อมูลที่รวบรวมมาจากการที่คุณใช้บริการของพาร์ทเนอร์เอง'),
(187, 304, 1, 145, 'แสดงข้อความในหน้าแรกของ Cookie โดยข้อความเริ่มต้น คือค่าเริ่มต้น'),
(188, 305, 1, 146, 'แสดงข้อความในหน้าแรกของ Cookie โดยข้อความเริ่มต้น คือค่าเริ่มต้น'),
(189, 306, 1, NULL, 'แสดงข้อความในหน้าแรกของ Cookie โดยข้อความเริ่มต้น คือค่าเริ่มต้น'),
(190, 307, 1, 147, 'เราใช้คุกกี้เพื่อปรับเปลี่ยนเนื้อหาและโฆษณาในแบบของคุณ เพื่อเสนอคุณลักษณะที่ใช้กับโซเชียลมีเดียและเพื่อวิเคราะห์การเข้าชมของเรา นอกจากนี้เรายังแชร์ข้อมูลการใช้งานของคุณในเว็บไซต์ของเรากับพาร์ทเนอร์ด้านโซเชียลมีเดีย โฆษณา และการวิเคราะห์ ซึ่งพาร์ทเนอร์เหล่านี้อาจนำข้อมูลดังกล่าวไปรวมกับข้อมูลอื่นๆ ที่คุณมอบให้หรือข้อมูลที่รวบรวมมาจากการที่คุณใช้บริการของพาร์ทเนอร์เอง');

-- --------------------------------------------------------

--
-- Table structure for table `exporthistory`
--

CREATE TABLE `exporthistory` (
  `exp_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `exporthistory`
--

INSERT INTO `exporthistory` (`exp_id`, `acc_id`, `file_id`, `date`) VALUES
(22, 176, 567, '2022-04-06 01:39:24'),
(23, 176, 566, '2022-04-06 01:39:38'),
(24, 176, 565, '2022-04-06 01:39:47');

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `name`, `status`, `description`) VALUES
(1, 'heart-1', 1, NULL),
(2, 'heart-2', 1, NULL),
(3, 'heart-3', 1, NULL),
(4, 'heart-4', 1, NULL),
(5, 'heart-5', 0, NULL),
(6, 'heart-6', 0, NULL),
(7, 'heart-7', 0, NULL),
(8, 'heart-8', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `device_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `date`, `name`, `hash`, `device_id`) VALUES
(565, '2022-04-06 01:31:22', '172.16.20.221,01:00,2022-04-06.log', 'f7f468f27a8615999f069c65bd5133a4e89fb4fac137b0f6f738bd16061755f7', 94),
(566, '2022-04-06 01:33:41', '103.136.249.119,20:00,2022-03-21.log', 'f6cfba07048f6bb1f93edddbb03a7bde6bb274ad81b3269641e143ec3fd106a0', 95),
(567, '2022-04-06 01:35:05', '209.97.136.164,01:00,2022-03-24.log', 'b56ced841a559b2a30703c24124945dc31ffcc907ad347aaf3b5ef73d59e4135', 96),
(569, '2022-04-06 03:32:24', '172.16.20.221,02:00,2022-04-06.log', 'dd65cdc5662c4478b79d59bff74d70d82736278b5abbd8de7897372e20c5ccad', 94),
(570, '2022-04-06 03:32:25', '172.16.20.221,03:00,2022-04-06.log', '8ae1e1a669c4691c5105a261e68f1367dd1a4198e817fc1661e39ec83732ab78', 94);

-- --------------------------------------------------------

--
-- Table structure for table `ftp`
--

CREATE TABLE `ftp` (
  `ftp_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `descrip` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `import_date` datetime NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `type_import` varchar(255) NOT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `file_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ftp`
--

INSERT INTO `ftp` (`ftp_id`, `name`, `descrip`, `create_date`, `import_date`, `path`, `type_import`, `hash`, `ip`, `username`, `password`, `file_id`) VALUES
(1, 'Remotelog', '-', '2022-03-10 08:34:00', '0000-00-00 00:00:00', '/var/log/remote.log', 'ftp', NULL, NULL, 'e46960e02bb7b109b3bfdbf5c2e41a60', '596144ef94f3aa9468c363c91b7727ef', NULL),
(2, 'Remotelog', '', '2022-03-10 08:35:00', '0000-00-00 00:00:00', '/var/log/remote.log', 'agent', NULL, NULL, '68805c6903c5060a41f7df744f34f2ae', '7e6791c9f699d66090588f2c544db012', NULL),
(5, '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, '', NULL, NULL, '693e9af84d3dfcc71e640e005bdc5e2e', '36427aa082b319d718852b29306c1754', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group` (
  `group_id` int(11) NOT NULL,
  `g_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `acc_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `msg` varchar(255) NOT NULL,
  `ht_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`acc_id`, `datetime`, `msg`, `ht_id`, `type`) VALUES
(175, '2022-04-06 01:26:24', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-6 01:26:24', 1027, 'login'),
(175, '2022-04-06 01:26:54', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-6 01:26:54', 1028, 'login'),
(175, '2022-04-06 01:27:30', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-6 01:27:30', 1029, 'login'),
(175, '2022-04-06 01:31:47', 'Wasin Wachiropakorn ออกจะระบบเมื่อ 2022-04-6 01:31:47', 1030, 'logout'),
(176, '2022-04-06 01:31:51', 'Artit Aungpraphapornchai เข้าใช้งานเมื่อ 2022-04-6 01:31:51', 1031, 'login'),
(176, '2022-04-06 01:32:47', 'Artit Aungpraphapornchai ออกจะระบบเมื่อ 2022-04-6 01:32:47', 1032, 'logout'),
(175, '2022-04-06 01:32:52', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-6 01:32:52', 1033, 'login'),
(175, '2022-04-06 01:35:10', 'Wasin Wachiropakorn ออกจะระบบเมื่อ 2022-04-6 01:35:10', 1034, 'logout'),
(176, '2022-04-06 01:35:15', 'Artit Aungpraphapornchai เข้าใช้งานเมื่อ 2022-04-6 01:35:15', 1035, 'login'),
(175, '2022-04-06 03:35:37', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-6 03:35:37', 1036, 'login'),
(175, '2022-04-06 03:39:50', 'Wasin Wachiropakorn ออกจะระบบเมื่อ 2022-04-6 03:39:50', 1037, 'logout'),
(175, '2022-04-06 03:39:51', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-6 03:39:51', 1038, 'login'),
(175, '2022-04-07 19:00:07', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-7 19:00:07', 1039, 'login'),
(175, '2022-04-07 20:37:50', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-7 20:37:50', 1040, 'login'),
(175, '2022-04-07 20:52:26', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-7 20:52:26', 1041, 'login'),
(175, '2022-04-08 16:33:37', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 16:33:37', 1042, 'login'),
(175, '2022-04-08 16:50:03', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 16:50:03', 1043, 'login'),
(175, '2022-04-08 16:50:15', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 16:50:15', 1044, 'login'),
(175, '2022-04-08 16:54:45', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 16:54:45', 1045, 'login'),
(175, '2022-04-08 16:55:50', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 16:55:50', 1046, 'login'),
(175, '2022-04-08 16:57:16', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 16:57:16', 1047, 'login'),
(175, '2022-04-08 16:59:36', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 16:59:36', 1048, 'login'),
(175, '2022-04-08 17:00:14', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 17:00:14', 1049, 'login'),
(175, '2022-04-08 17:06:34', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 17:06:34', 1050, 'login'),
(175, '2022-04-08 17:08:19', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 17:08:19', 1051, 'login'),
(175, '2022-04-08 17:37:16', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 17:37:16', 1052, 'login'),
(175, '2022-04-08 18:49:06', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 18:49:06', 1053, 'login'),
(175, '2022-04-08 18:53:32', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 18:53:32', 1054, 'login'),
(175, '2022-04-08 19:21:21', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 19:21:21', 1055, 'login'),
(175, '2022-04-08 19:22:28', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-8 19:22:28', 1056, 'login'),
(175, '2022-04-09 01:02:33', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 01:02:33', 1057, 'login'),
(175, '2022-04-09 01:05:23', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 01:05:23', 1058, 'login'),
(175, '2022-04-09 01:09:56', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 01:09:56', 1059, 'login'),
(175, '2022-04-09 01:10:06', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 01:10:06', 1060, 'login'),
(175, '2022-04-09 01:10:53', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 01:10:53', 1061, 'login'),
(175, '2022-04-09 01:13:49', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 01:13:49', 1062, 'login'),
(175, '2022-04-09 01:19:31', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 01:19:31', 1063, 'login'),
(175, '2022-04-09 02:44:47', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 02:44:47', 1064, 'login'),
(175, '2022-04-09 02:58:15', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 02:58:15', 1065, 'login'),
(175, '2022-04-09 03:54:36', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 03:54:36', 1066, 'login'),
(175, '2022-04-09 04:07:51', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:07:51', 1067, 'login'),
(175, '2022-04-09 04:10:51', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:10:51', 1068, 'login'),
(175, '2022-04-09 04:11:46', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:11:46', 1069, 'login'),
(175, '2022-04-09 04:20:47', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:20:47', 1070, 'login'),
(175, '2022-04-09 04:28:55', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:28:55', 1071, 'login'),
(175, '2022-04-09 04:30:02', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:30:02', 1072, 'login'),
(175, '2022-04-09 04:31:39', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:31:39', 1073, 'login'),
(175, '2022-04-09 04:33:24', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:33:24', 1074, 'login'),
(175, '2022-04-09 04:33:59', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:33:59', 1075, 'login'),
(175, '2022-04-09 04:35:19', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:35:19', 1076, 'login'),
(175, '2022-04-09 04:35:58', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:35:58', 1077, 'login'),
(175, '2022-04-09 04:36:36', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:36:36', 1078, 'login'),
(175, '2022-04-09 04:37:03', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:37:03', 1079, 'login'),
(175, '2022-04-09 04:37:51', 'Wasin Wachiropakorn เข้าใช้งานเมื่อ 2022-04-9 04:37:51', 1080, 'login');

-- --------------------------------------------------------

--
-- Table structure for table `input`
--

CREATE TABLE `input` (
  `input_id` int(11) NOT NULL,
  `input_ip` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `input`
--

INSERT INTO `input` (`input_id`, `input_ip`, `date`, `hostname`, `type`) VALUES
(91, '103.136.249.119', '2022-04-06', '103.136.249.119', 'upd'),
(92, '104.244.72.115', '2022-04-06', '104.244.72.115', 'upd'),
(93, '107.150.105.183', '2022-04-06', '107.150.105.183', 'upd'),
(94, '118.193.58.114', '2022-04-06', '118.193.58.114', 'upd'),
(95, '144.172.73.66', '2022-04-06', '144.172.73.66', 'upd'),
(96, '172.16.20.221', '2022-04-06', '172.16.20.221', 'upd'),
(97, '179.43.187.138', '2022-04-06', '179.43.187.138', 'upd'),
(98, '185.156.72.10', '2022-04-06', '185.156.72.10', 'upd'),
(99, '185.156.72.3', '2022-04-06', '185.156.72.3', 'upd'),
(100, '185.220.100.244', '2022-04-06', '185.220.100.244', 'upd'),
(101, '185.220.100.246', '2022-04-06', '185.220.100.246', 'upd'),
(102, '185.220.100.254', '2022-04-06', '185.220.100.254', 'upd'),
(103, '185.220.101.32', '2022-04-06', '185.220.101.32', 'upd'),
(104, '185.220.101.49', '2022-04-06', '185.220.101.49', 'upd'),
(105, '185.247.226.98', '2022-04-06', '185.247.226.98', 'upd'),
(106, '209.97.136.164', '2022-04-06', '209.97.136.164', 'upd'),
(107, '23.129.64.145', '2022-04-06', '23.129.64.145', 'upd'),
(108, '23.129.64.148', '2022-04-06', '23.129.64.148', 'upd'),
(109, '23.129.64.211', '2022-04-06', '23.129.64.211', 'upd'),
(110, '23.129.64.212', '2022-04-06', '23.129.64.212', 'upd'),
(111, '23.129.64.213', '2022-04-06', '23.129.64.213', 'upd'),
(112, '45.154.255.147', '2022-04-06', '45.154.255.147', 'upd');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `log_id` int(11) NOT NULL,
  `msg` longtext NOT NULL,
  `date` datetime NOT NULL,
  `device_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`log_id`, `msg`, `date`, `device_id`, `file_name`) VALUES
(555865, 'Apr  6 01:14:24 172.16.20.221 1649182464.193    167 172.16.10.53 TCP_MISS/503 4024 GET http://ipv6.msftconnecttest.com/connecttest.txt - HIER_DIRECT/2a01:111:2003::52 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555866, 'Apr  6 01:14:24 172.16.20.221 1649182464.277    249 172.16.10.53 TCP_MISS/200 634 GET http://www.msftconnecttest.com/connecttest.txt - HIER_DIRECT/13.107.4.52 text/plain', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555867, 'Apr  6 01:15:28 172.16.20.221 1649182528.426   2158 172.16.10.53 TCP_TUNNEL/200 5312 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/20.44.10.123 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555868, 'Apr  6 01:15:42 172.16.20.221 1649182542.167    483 172.16.10.53 TCP_TUNNEL/200 10425 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555869, 'Apr  6 01:15:42 172.16.20.221 1649182542.496    313 172.16.10.53 TCP_TUNNEL/200 13382 CONNECT smartscreen-prod.microsoft.com:443 - HIER_DIRECT/40.90.184.82 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555870, 'Apr  6 01:15:47 172.16.20.221 1649182547.002   8554 172.16.10.53 TCP_TUNNEL/200 510 CONNECT cdn.fbsbx.com:443 - HIER_DIRECT/157.240.235.1 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555871, 'Apr  6 01:15:47 172.16.20.221 1649182547.002   5318 172.16.10.53 TCP_TUNNEL/200 5329 CONNECT fonts.googleapis.com:443 - HIER_DIRECT/142.251.10.95 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555872, 'Apr  6 01:15:47 172.16.20.221 1649182547.003   5319 172.16.10.53 TCP_TUNNEL/200 4984 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555873, 'Apr  6 01:15:47 172.16.20.221 1649182547.003   4257 172.16.10.53 TCP_TUNNEL/200 4986 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555874, 'Apr  6 01:15:47 172.16.20.221 1649182547.003   5320 172.16.10.53 TCP_TUNNEL/200 5317 CONNECT oss.maxcdn.com:443 - HIER_DIRECT/23.111.8.154 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555875, 'Apr  6 01:15:47 172.16.20.221 1649182547.004   8263 172.16.10.53 TCP_TUNNEL/200 8759 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/20.42.73.24 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555876, 'Apr  6 01:15:47 172.16.20.221 1649182547.008  83077 172.16.10.53 TCP_TUNNEL/200 179607 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555877, 'Apr  6 01:15:47 172.16.20.221 1649182547.008   8668 172.16.10.53 TCP_TUNNEL/200 3033 CONNECT video.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.82 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555878, 'Apr  6 01:15:47 172.16.20.221 1649182547.009  83093 172.16.10.53 TCP_TUNNEL/200 681438 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.13.35 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555879, 'Apr  6 01:15:47 172.16.20.221 1649182547.010   8563 172.16.10.53 TCP_TUNNEL/200 795305 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.13.19 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555880, 'Apr  6 01:15:47 172.16.20.221 1649182547.010   3232 172.16.10.53 TCP_TUNNEL/200 3557 CONNECT scontent.xx.fbcdn.net:443 - HIER_DIRECT/157.240.235.1 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555881, 'Apr  6 01:15:47 172.16.20.221 1649182547.010  39757 172.16.10.53 TCP_TUNNEL/200 6411 CONNECT www.google.com:443 - HIER_DIRECT/74.125.68.103 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555882, 'Apr  6 01:15:47 172.16.20.221 1649182547.010   8830 172.16.10.53 TCP_TUNNEL/200 2231 CONNECT scontent.xx.fbcdn.net:443 - HIER_DIRECT/157.240.235.1 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555883, 'Apr  6 01:15:47 172.16.20.221 1649182547.010  83017 172.16.10.53 TCP_TUNNEL/200 245515 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555884, 'Apr  6 01:16:12 172.16.20.221 1649182572.409      3 172.16.10.53 TCP_MISS/200 3540 GET http://172.16.20.230/ - HIER_DIRECT/172.16.20.230 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555885, 'Apr  6 01:16:12 172.16.20.221 1649182572.538      4 172.16.10.53 TCP_MISS/200 3687 GET http://172.16.20.230/icons/ubuntu-logo.png - HIER_DIRECT/172.16.20.230 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555886, 'Apr  6 01:16:13 172.16.20.221 1649182572.600    184 172.16.10.53 TCP_TUNNEL/200 8522 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555887, 'Apr  6 01:16:13 172.16.20.221 1649182572.696      0 172.16.10.53 TCP_MISS/404 555 GET http://172.16.20.230/favicon.ico - HIER_DIRECT/172.16.20.230 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555888, 'Apr  6 01:16:19 172.16.20.221 1649182578.572      1 172.16.10.53 TCP_MISS/503 4251 GET http://172.16.20.230:8081/ - HIER_DIRECT/172.16.20.230 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555889, 'Apr  6 01:16:19 172.16.20.221 1649182578.658      0 172.16.10.53 TCP_MEM_HIT/200 11704 GET http://proxy:3128/squid-internal-static/icons/SN.png - HIER_NONE/- image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555890, 'Apr  6 01:16:19 172.16.20.221 1649182578.753      0 172.16.10.53 TCP_MISS/503 4214 GET http://172.16.20.230:8081/favicon.ico - HIER_DIRECT/172.16.20.230 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555891, 'Apr  6 01:16:25 172.16.20.221 1649182584.661      0 172.16.10.53 TCP_MISS/503 4251 GET http://172.16.20.230:8080/ - HIER_DIRECT/172.16.20.230 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555892, 'Apr  6 01:16:25 172.16.20.221 1649182584.730      0 172.16.10.53 TCP_MEM_HIT/200 11704 GET http://proxy:3128/squid-internal-static/icons/SN.png - HIER_NONE/- image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555893, 'Apr  6 01:16:25 172.16.20.221 1649182584.806      0 172.16.10.53 TCP_MISS/503 4214 GET http://172.16.20.230:8080/favicon.ico - HIER_DIRECT/172.16.20.230 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555894, 'Apr  6 01:16:38 172.16.20.221 1649182597.902 102966 172.16.10.53 TCP_TUNNEL/200 11353 CONNECT activity.windows.com:443 - HIER_DIRECT/20.44.229.112 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555895, 'Apr  6 01:16:42 172.16.20.221 1649182602.401    358 172.16.10.53 TCP_TUNNEL/200 8504 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/20.212.97.243 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555896, 'Apr  6 01:16:47 172.16.20.221 1649182607.011    804 172.16.10.53 TCP_TUNNEL/200 1052234 CONNECT rr1---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.44 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555897, 'Apr  6 01:16:48 172.16.20.221 1649182608.108  60661 172.16.10.53 TCP_TUNNEL/200 4985 CONNECT ssl.gstatic.com:443 - HIER_DIRECT/74.125.24.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555898, 'Apr  6 01:16:54 172.16.20.221 1649182614.435  66467 172.16.10.53 TCP_TUNNEL/200 4654997 CONNECT cdn.fbsbx.com:443 - HIER_DIRECT/157.240.235.1 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555899, 'Apr  6 01:16:57 172.16.20.221 1649182617.056  65169 172.16.10.53 TCP_TUNNEL/200 51133 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.13.19 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555900, 'Apr  6 01:17:43 172.16.20.221 1649182663.539  61535 172.16.10.53 TCP_TUNNEL/200 859 CONNECT fonts.googleapis.com:443 - HIER_DIRECT/142.251.10.95 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555901, 'Apr  6 01:17:43 172.16.20.221 1649182663.539  61526 172.16.10.53 TCP_TUNNEL/200 859 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555902, 'Apr  6 01:17:43 172.16.20.221 1649182663.539  61077 172.16.10.53 TCP_TUNNEL/200 859 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555903, 'Apr  6 01:17:48 172.16.20.221 1649182667.959 120531 172.16.10.53 TCP_TUNNEL/200 19430 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555904, 'Apr  6 01:17:48 172.16.20.221 1649182668.064 120608 172.16.10.53 TCP_TUNNEL/200 50384 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555905, 'Apr  6 01:18:54 172.16.20.221 1649182734.580 187423 172.16.10.53 TCP_TUNNEL/200 11606 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/20.42.73.24 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555906, 'Apr  6 01:19:14 172.16.20.221 1649182753.744    452 172.16.10.53 TCP_TUNNEL/200 7192 CONNECT r.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555907, 'Apr  6 01:19:14 172.16.20.221 1649182753.750    458 172.16.10.53 TCP_TUNNEL/200 7192 CONNECT r.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555908, 'Apr  6 01:19:14 172.16.20.221 1649182753.753    460 172.16.10.53 TCP_TUNNEL/200 6233 CONNECT r.msftstatic.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555909, 'Apr  6 01:19:14 172.16.20.221 1649182753.755    461 172.16.10.53 TCP_TUNNEL/200 6197 CONNECT r.msftstatic.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555910, 'Apr  6 01:19:14 172.16.20.221 1649182753.874 147473 172.16.10.53 TCP_TUNNEL/200 30044714 CONNECT rr1---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.44 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555911, 'Apr  6 01:19:14 172.16.20.221 1649182753.875 149081 172.16.10.53 TCP_TUNNEL/200 30069 CONNECT jnn-pa.googleapis.com:443 - HIER_DIRECT/142.251.10.95 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555912, 'Apr  6 01:19:14 172.16.20.221 1649182753.876   1226 172.16.10.53 TCP_TUNNEL/200 26949 CONNECT assets.msn.com:443 - HIER_DIRECT/122.155.237.225 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555913, 'Apr  6 01:19:14 172.16.20.221 1649182753.877 191918 172.16.10.53 TCP_TUNNEL/200 21641 CONNECT www.google.com:443 - HIER_DIRECT/74.125.68.103 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555914, 'Apr  6 01:19:14 172.16.20.221 1649182753.878  57148 172.16.10.53 TCP_TUNNEL/200 3642 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555915, 'Apr  6 01:19:14 172.16.20.221 1649182753.879 151877 172.16.10.53 TCP_TUNNEL/200 216749 CONNECT www.youtube.com:443 - HIER_DIRECT/74.125.130.91 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555916, 'Apr  6 01:19:14 172.16.20.221 1649182753.880 118819 172.16.10.53 TCP_TUNNEL/200 1406 CONNECT play.google.com:443 - HIER_DIRECT/74.125.24.138 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555917, 'Apr  6 01:19:14 172.16.20.221 1649182753.881 206375 172.16.10.53 TCP_TUNNEL/200 2646 CONNECT lh3.googleusercontent.com:443 - HIER_DIRECT/142.250.4.132 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555918, 'Apr  6 01:19:14 172.16.20.221 1649182753.881 206694 172.16.10.53 TCP_TUNNEL/200 28826 CONNECT ogs.google.com:443 - HIER_DIRECT/142.250.4.101 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555919, 'Apr  6 01:19:14 172.16.20.221 1649182753.881 206433 172.16.10.53 TCP_TUNNEL/200 193469 CONNECT www.gstatic.com:443 - HIER_DIRECT/142.251.10.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555920, 'Apr  6 01:19:14 172.16.20.221 1649182753.881 206067 172.16.10.53 TCP_TUNNEL/200 68450 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.7.35 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555921, 'Apr  6 01:19:14 172.16.20.221 1649182753.882 151486 172.16.10.53 TCP_TUNNEL/200 562225 CONNECT i.ytimg.com:443 - HIER_DIRECT/142.251.10.119 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555922, 'Apr  6 01:19:14 172.16.20.221 1649182753.887 149627 172.16.10.53 TCP_TUNNEL/200 26799 CONNECT yt3.ggpht.com:443 - HIER_DIRECT/172.217.194.132 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555923, 'Apr  6 01:19:14 172.16.20.221 1649182753.887 205928 172.16.10.53 TCP_TUNNEL/200 7511 CONNECT accounts.google.com:443 - HIER_DIRECT/142.251.10.84 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555924, 'Apr  6 01:19:14 172.16.20.221 1649182753.887 205854 172.16.10.53 TCP_TUNNEL/200 9147 CONNECT play.google.com:443 - HIER_DIRECT/74.125.68.100 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555925, 'Apr  6 01:19:14 172.16.20.221 1649182753.887    586 172.16.10.53 TCP_TUNNEL/200 7275 CONNECT c.msn.com:443 - HIER_DIRECT/52.231.207.240 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555926, 'Apr  6 01:19:14 172.16.20.221 1649182753.887    882 172.16.10.53 TCP_TUNNEL/200 99381 CONNECT www.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555927, 'Apr  6 01:19:14 172.16.20.221 1649182753.887    486 172.16.10.53 TCP_TUNNEL/200 6455 CONNECT sb.scorecardresearch.com:443 - HIER_DIRECT/65.9.182.60 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555928, 'Apr  6 01:19:14 172.16.20.221 1649182753.887   1236 172.16.10.53 TCP_TUNNEL/200 15650 CONNECT srtb.msn.com:443 - HIER_DIRECT/204.79.197.203 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555929, 'Apr  6 01:19:14 172.16.20.221 1649182753.887   2720 172.16.10.53 TCP_TUNNEL/200 23006 CONNECT i1.ytimg.com:443 - HIER_DIRECT/172.217.194.102 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555930, 'Apr  6 01:19:14 172.16.20.221 1649182753.887   2371 172.16.10.53 TCP_TUNNEL/200 4717 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555931, 'Apr  6 01:19:14 172.16.20.221 1649182753.889    589 172.16.10.53 TCP_TUNNEL/200 8832 CONNECT www.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555932, 'Apr  6 01:19:14 172.16.20.221 1649182754.703     37 172.16.10.53 TCP_TUNNEL/200 39 CONNECT cat.sg1.as.criteo.com:443 - HIER_DIRECT/182.161.73.132 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555933, 'Apr  6 01:19:15 172.16.20.221 1649182755.323   1567 172.16.10.53 TCP_TUNNEL/200 4018 CONNECT stas.outbrain.com:443 - HIER_DIRECT/38.133.127.31 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555934, 'Apr  6 01:19:15 172.16.20.221 1649182755.349   1609 172.16.10.53 TCP_TUNNEL/200 4018 CONNECT stas.outbrain.com:443 - HIER_DIRECT/38.133.127.31 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555935, 'Apr  6 01:19:16 172.16.20.221 1649182756.520   1503 172.16.10.53 TCP_TUNNEL/200 8469 CONNECT browser.events.data.msn.com:443 - HIER_DIRECT/52.182.143.211 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555936, 'Apr  6 01:19:17 172.16.20.221 1649182757.366    354 172.16.10.53 TCP_TUNNEL/200 8534 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555937, 'Apr  6 01:19:18 172.16.20.221 1649182757.738   3065 172.16.10.53 TCP_TUNNEL/200 574 CONNECT assets.msn.com:443 - HIER_DIRECT/122.155.237.225 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555938, 'Apr  6 01:19:18 172.16.20.221 1649182757.739   3990 172.16.10.53 TCP_TUNNEL/200 3781 CONNECT b1-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.191 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555939, 'Apr  6 01:19:18 172.16.20.221 1649182757.739   3998 172.16.10.53 TCP_TUNNEL/200 3781 CONNECT b1-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.191 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555940, 'Apr  6 01:19:18 172.16.20.221 1649182757.740   3998 172.16.10.53 TCP_TUNNEL/200 3833 CONNECT b1t-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.127 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555941, 'Apr  6 01:19:18 172.16.20.221 1649182757.741   3980 172.16.10.53 TCP_TUNNEL/200 3989 CONNECT b1t-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.127 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555942, 'Apr  6 01:19:18 172.16.20.221 1649182757.741   2746 172.16.10.53 TCP_TUNNEL/200 3677 CONNECT b1t-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.127 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555943, 'Apr  6 01:19:18 172.16.20.221 1649182758.161    385 172.16.10.53 TCP_TUNNEL/200 2148 CONNECT cdn.1112.com:443 - HIER_DIRECT/104.22.54.98 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555944, 'Apr  6 01:19:18 172.16.20.221 1649182758.162    386 172.16.10.53 TCP_TUNNEL/200 2147 CONNECT cdn.1112.com:443 - HIER_DIRECT/104.22.54.98 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555945, 'Apr  6 01:19:18 172.16.20.221 1649182758.162    379 172.16.10.53 TCP_TUNNEL/200 2148 CONNECT cdn.1112.com:443 - HIER_DIRECT/104.22.54.98 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555946, 'Apr  6 01:19:18 172.16.20.221 1649182758.162    376 172.16.10.53 TCP_TUNNEL/200 2148 CONNECT cdn.1112.com:443 - HIER_DIRECT/104.22.54.98 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555947, 'Apr  6 01:19:18 172.16.20.221 1649182758.162    373 172.16.10.53 TCP_TUNNEL/200 2147 CONNECT cdn.1112.com:443 - HIER_DIRECT/104.22.54.98 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555948, 'Apr  6 01:19:20 172.16.20.221 1649182760.195   6786 172.16.10.53 TCP_TUNNEL/200 8471 CONNECT browser.events.data.msn.com:443 - HIER_DIRECT/52.182.143.211 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555949, 'Apr  6 01:19:21 172.16.20.221 1649182761.550   6877 172.16.10.53 TCP_TUNNEL/200 7393 CONNECT browser.events.data.msn.com:443 - HIER_DIRECT/52.182.143.211 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555950, 'Apr  6 01:19:22 172.16.20.221 1649182761.792   7129 172.16.10.53 TCP_TUNNEL/200 7192 CONNECT c.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555951, 'Apr  6 01:19:22 172.16.20.221 1649182762.200   5928 172.16.10.53 TCP_TUNNEL/200 859 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555952, 'Apr  6 01:19:22 172.16.20.221 1649182762.435   6792 172.16.10.53 TCP_TUNNEL/200 185 CONNECT img-s-msn-com.akamaized.net:443 - HIER_DIRECT/122.155.236.41 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555953, 'Apr  6 01:19:22 172.16.20.221 1649182762.440   6795 172.16.10.53 TCP_TUNNEL/200 6118 CONNECT sb.scorecardresearch.com:443 - HIER_DIRECT/65.9.182.60 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555954, 'Apr  6 01:19:23 172.16.20.221 1649182762.837   5685 172.16.10.53 TCP_TUNNEL/200 5846 CONNECT srtb.msn.com:443 - HIER_DIRECT/204.79.197.203 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555955, 'Apr  6 01:19:25 172.16.20.221 1649182764.767  11005 172.16.10.53 TCP_TUNNEL/200 1137 CONNECT cat.sg1.as.criteo.com:443 - HIER_DIRECT/182.161.73.132 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555956, 'Apr  6 01:19:25 172.16.20.221 1649182764.769  10881 172.16.10.53 TCP_TUNNEL/200 2556 CONNECT pix.as.criteo.net:443 - HIER_DIRECT/182.161.73.135 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555957, 'Apr  6 01:19:25 172.16.20.221 1649182764.824  10159 172.16.10.53 TCP_TUNNEL/200 393 CONNECT www.google.com:443 - HIER_DIRECT/74.125.68.103 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555958, 'Apr  6 01:19:25 172.16.20.221 1649182765.375    129 172.16.10.53 TCP_MISS/301 651 GET http://119.81.44.155/phpmyadmin - HIER_DIRECT/119.81.44.155 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555959, 'Apr  6 01:19:26 172.16.20.221 1649182765.785    538 172.16.10.53 TCP_TUNNEL/200 8553 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555960, 'Apr  6 01:19:26 172.16.20.221 1649182765.794    314 172.16.10.53 TCP_MISS/200 5236 GET http://119.81.44.155/phpmyadmin/ - HIER_DIRECT/119.81.44.155 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555961, 'Apr  6 01:19:26 172.16.20.221 1649182766.107   3554 172.16.10.53 TCP_TUNNEL/200 2118235 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555962, 'Apr  6 01:19:26 172.16.20.221 1649182766.107   4300 172.16.10.53 TCP_TUNNEL/200 859 CONNECT ssl.gstatic.com:443 - HIER_DIRECT/74.125.24.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555963, 'Apr  6 01:19:26 172.16.20.221 1649182766.108   7813 172.16.10.53 TCP_TUNNEL/200 5424 CONNECT www.googleadservices.com:443 - HIER_DIRECT/142.251.12.157 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555964, 'Apr  6 01:19:26 172.16.20.221 1649182766.159     91 172.16.10.53 TCP_MISS/200 8887 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/jquery-ui.css - HIER_DIRECT/119.81.44.155 text/css', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555965, 'Apr  6 01:19:26 172.16.20.221 1649182766.189    584 172.16.10.53 TCP_TUNNEL/200 8587 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555966, 'Apr  6 01:19:26 172.16.20.221 1649182766.233    126 172.16.10.53 TCP_MISS/200 2943 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/lib/codemirror.css? - HIER_DIRECT/119.81.44.155 text/css', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555967, 'Apr  6 01:19:26 172.16.20.221 1649182766.371     86 172.16.10.53 TCP_MISS/200 1667 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/lint/lint.css? - HIER_DIRECT/119.81.44.155 text/css', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555968, 'Apr  6 01:19:26 172.16.20.221 1649182766.371     87 172.16.10.53 TCP_MISS/200 723 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/hint/show-hint.css? - HIER_DIRECT/119.81.44.155 text/css', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555969, 'Apr  6 01:19:26 172.16.20.221 1649182766.417    130 172.16.10.53 TCP_MISS/200 6569 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery-migrate.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555970, 'Apr  6 01:19:26 172.16.20.221 1649182766.434    145 172.16.10.53 TCP_MISS/200 1221 GET http://119.81.44.155/phpmyadmin/js/whitelist.php? - HIER_DIRECT/119.81.44.155 text/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555971, 'Apr  6 01:19:26 172.16.20.221 1649182766.465    179 172.16.10.53 TCP_MISS/200 31094 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.min.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555972, 'Apr  6 01:19:26 172.16.20.221 1649182766.496    211 172.16.10.53 TCP_MISS/200 21647 GET http://119.81.44.155/phpmyadmin/phpmyadmin.css.php? - HIER_DIRECT/119.81.44.155 text/css', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555973, 'Apr  6 01:19:26 172.16.20.221 1649182766.595     85 172.16.10.53 TCP_MISS/200 1485 GET http://119.81.44.155/phpmyadmin/js/keyhandler.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555974, 'Apr  6 01:19:26 172.16.20.221 1649182766.599     89 172.16.10.53 TCP_MISS/200 8066 GET http://119.81.44.155/phpmyadmin/js/ajax.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555975, 'Apr  6 01:19:26 172.16.20.221 1649182766.602     92 172.16.10.53 TCP_MISS/200 2964 GET http://119.81.44.155/phpmyadmin/js/vendor/sprintf.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555976, 'Apr  6 01:19:26 172.16.20.221 1649182766.636     84 172.16.10.53 TCP_MISS/200 1940 GET http://119.81.44.155/phpmyadmin/js/vendor/js.cookie.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555977, 'Apr  6 01:19:26 172.16.20.221 1649182766.641     86 172.16.10.53 TCP_MISS/200 2965 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.mousewheel.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555978, 'Apr  6 01:19:26 172.16.20.221 1649182766.723    212 172.16.10.53 TCP_MISS/200 68443 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery-ui.min.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555979, 'Apr  6 01:19:27 172.16.20.221 1649182766.813     92 172.16.10.53 TCP_MISS/200 4652 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.event.drag-2.2.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555980, 'Apr  6 01:19:27 172.16.20.221 1649182766.815     89 172.16.10.53 TCP_MISS/200 5748 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.ba-hashchange-1.3.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555981, 'Apr  6 01:19:27 172.16.20.221 1649182766.816     94 172.16.10.53 TCP_MISS/200 13947 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.validate.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555982, 'Apr  6 01:19:27 172.16.20.221 1649182766.816     88 172.16.10.53 TCP_MISS/200 942 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.debounce-1.0.5.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555983, 'Apr  6 01:19:27 172.16.20.221 1649182766.828    107 172.16.10.53 TCP_MISS/200 19346 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery-ui-timepicker-addon.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555984, 'Apr  6 01:19:27 172.16.20.221 1649182766.907     85 172.16.10.53 TCP_MISS/200 2710 GET http://119.81.44.155/phpmyadmin/js/menu-resizer.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555985, 'Apr  6 01:19:27 172.16.20.221 1649182766.911     85 172.16.10.53 TCP_MISS/200 715 GET http://119.81.44.155/phpmyadmin/js/cross_framing_protection.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555986, 'Apr  6 01:19:27 172.16.20.221 1649182766.931     86 172.16.10.53 TCP_MISS/200 3086 GET http://119.81.44.155/phpmyadmin/js/error_report.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555987, 'Apr  6 01:19:27 172.16.20.221 1649182766.936     92 172.16.10.53 TCP_MISS/200 11797 GET http://119.81.44.155/phpmyadmin/js/vendor/tracekit.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555988, 'Apr  6 01:19:27 172.16.20.221 1649182766.946    102 172.16.10.53 TCP_MISS/200 8799 GET http://119.81.44.155/phpmyadmin/js/rte.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555989, 'Apr  6 01:19:27 172.16.20.221 1649182766.990  12317 172.16.10.53 TCP_TUNNEL/200 10840 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.90 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555990, 'Apr  6 01:19:27 172.16.20.221 1649182767.024    111 172.16.10.53 TCP_MISS/200 9468 GET http://119.81.44.155/phpmyadmin/js/messages.php? - HIER_DIRECT/119.81.44.155 text/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555991, 'Apr  6 01:19:27 172.16.20.221 1649182767.148   8048 172.16.10.53 TCP_TUNNEL/200 859 CONNECT www.gstatic.com:443 - HIER_DIRECT/142.251.10.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555992, 'Apr  6 01:19:27 172.16.20.221 1649182767.236     86 172.16.10.53 TCP_MISS/200 4178 GET http://119.81.44.155/phpmyadmin/js/doclinks.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555993, 'Apr  6 01:19:27 172.16.20.221 1649182767.238     90 172.16.10.53 TCP_MISS/200 7172 GET http://119.81.44.155/phpmyadmin/js/config.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555994, 'Apr  6 01:19:27 172.16.20.221 1649182767.255     88 172.16.10.53 TCP_MISS/200 5151 GET http://119.81.44.155/phpmyadmin/js/common.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555995, 'Apr  6 01:19:27 172.16.20.221 1649182767.257     94 172.16.10.53 TCP_MISS/200 12486 GET http://119.81.44.155/phpmyadmin/js/navigation.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555996, 'Apr  6 01:19:27 172.16.20.221 1649182767.295    129 172.16.10.53 TCP_MISS/200 6630 GET http://119.81.44.155/phpmyadmin/js/indexes.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555997, 'Apr  6 01:19:27 172.16.20.221 1649182767.301    147 172.16.10.53 TCP_MISS/200 41251 GET http://119.81.44.155/phpmyadmin/js/functions.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555998, 'Apr  6 01:19:27 172.16.20.221 1649182767.388     84 172.16.10.53 TCP_MISS/200 1052 GET http://119.81.44.155/phpmyadmin/js/page_settings.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(555999, 'Apr  6 01:19:27 172.16.20.221 1649182767.396     85 172.16.10.53 TCP_MISS/200 1146 GET http://119.81.44.155/phpmyadmin/js/shortcuts_handler.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556000, 'Apr  6 01:19:27 172.16.20.221 1649182767.422     92 172.16.10.53 TCP_MISS/200 13700 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/mode/sql/sql.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556001, 'Apr  6 01:19:27 172.16.20.221 1649182767.543     85 172.16.10.53 TCP_MISS/200 3154 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/hint/sql-hint.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556002, 'Apr  6 01:19:27 172.16.20.221 1649182767.544     86 172.16.10.53 TCP_MISS/200 1454 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/runmode/runmode.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556003, 'Apr  6 01:19:27 172.16.20.221 1649182767.545     87 172.16.10.53 TCP_MISS/200 5352 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/hint/show-hint.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556004, 'Apr  6 01:19:27 172.16.20.221 1649182767.546     86 172.16.10.53 TCP_MISS/200 846 GET http://119.81.44.155/phpmyadmin/js/codemirror/addon/lint/sql-lint.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556005, 'Apr  6 01:19:27 172.16.20.221 1649182767.550    231 172.16.10.53 TCP_MISS/200 105769 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/lib/codemirror.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556006, 'Apr  6 01:19:27 172.16.20.221 1649182767.582    123 172.16.10.53 TCP_MISS/200 2997 GET http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/lint/lint.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556007, 'Apr  6 01:19:27 172.16.20.221 1649182767.646     92 172.16.10.53 TCP_MISS/200 10755 GET http://119.81.44.155/phpmyadmin/js/console.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556008, 'Apr  6 01:19:27 172.16.20.221 1649182767.681     85 172.16.10.53 TCP_MISS/200 4656 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/logo_right.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556009, 'Apr  6 01:19:28 172.16.20.221 1649182767.764     80 172.16.10.53 TCP_MISS/200 389 GET http://119.81.44.155/phpmyadmin/themes/dot.gif - HIER_DIRECT/119.81.44.155 image/gif', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556010, 'Apr  6 01:19:28 172.16.20.221 1649182767.794     87 172.16.10.53 TCP_MISS/200 1424 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/css/printview.css? - HIER_DIRECT/119.81.44.155 text/css', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556011, 'Apr  6 01:19:28 172.16.20.221 1649182767.871     85 172.16.10.53 TCP_MISS/200 1053 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_help.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556012, 'Apr  6 01:19:30 172.16.20.221 1649182770.683    154 172.16.10.53 TCP_MISS/302 1382 POST http://119.81.44.155/phpmyadmin/index.php - HIER_DIRECT/119.81.44.155 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556013, 'Apr  6 01:19:31 172.16.20.221 1649182770.971    253 172.16.10.53 TCP_MISS/200 16041 GET http://119.81.44.155/phpmyadmin/index.php - HIER_DIRECT/119.81.44.155 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556014, 'Apr  6 01:19:31 172.16.20.221 1649182771.132     70 172.16.10.53 TCP_MISS/200 21628 GET http://119.81.44.155/phpmyadmin/phpmyadmin.css.php? - HIER_DIRECT/119.81.44.155 text/css', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556015, 'Apr  6 01:19:31 172.16.20.221 1649182771.148     85 172.16.10.53 TCP_MISS/200 3062 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/logo_left.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556016, 'Apr  6 01:19:31 172.16.20.221 1649182771.199    486 172.16.10.53 TCP_TUNNEL/200 8573 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556017, 'Apr  6 01:19:31 172.16.20.221 1649182771.200    457 172.16.10.53 TCP_TUNNEL/200 8609 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556018, 'Apr  6 01:19:31 172.16.20.221 1649182771.351   4193 172.16.10.53 TCP_TUNNEL/200 5005 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556019, 'Apr  6 01:19:31 172.16.20.221 1649182771.391     39 172.16.10.53 TCP_MISS/200 1053 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_docs.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556020, 'Apr  6 01:19:31 172.16.20.221 1649182771.435     84 172.16.10.53 TCP_MISS/200 974 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_loggoff.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556021, 'Apr  6 01:19:31 172.16.20.221 1649182771.435     84 172.16.10.53 TCP_MISS/200 1082 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_home.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556022, 'Apr  6 01:19:31 172.16.20.221 1649182771.437     85 172.16.10.53 TCP_MISS/200 860 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_sqlhelp.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556023, 'Apr  6 01:19:31 172.16.20.221 1649182771.437     86 172.16.10.53 TCP_MISS/200 493 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/left_nav_bg.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556024, 'Apr  6 01:19:31 172.16.20.221 1649182771.522     85 172.16.10.53 TCP_MISS/200 933 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_newdb.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556025, 'Apr  6 01:19:31 172.16.20.221 1649182771.540     85 172.16.10.53 TCP_MISS/200 875 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_reload.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556026, 'Apr  6 01:19:31 172.16.20.221 1649182771.543     87 172.16.10.53 TCP_MISS/200 628 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_link.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556027, 'Apr  6 01:19:31 172.16.20.221 1649182771.544     88 172.16.10.53 TCP_MISS/200 676 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_db.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556028, 'Apr  6 01:19:31 172.16.20.221 1649182771.550     83 172.16.10.53 TCP_MISS/200 747 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_replication.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556029, 'Apr  6 01:19:31 172.16.20.221 1649182771.552     85 172.16.10.53 TCP_MISS/200 490 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_plus.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556030, 'Apr  6 01:19:31 172.16.20.221 1649182771.701    218 172.16.10.53 TCP_MISS/200 2942 POST http://119.81.44.155/phpmyadmin/ajax.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556031, 'Apr  6 01:19:32 172.16.20.221 1649182771.843     85 172.16.10.53 TCP_MISS/200 1028 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_sql.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556032, 'Apr  6 01:19:32 172.16.20.221 1649182771.844     84 172.16.10.53 TCP_MISS/200 904 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_import.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556033, 'Apr  6 01:19:32 172.16.20.221 1649182771.846     86 172.16.10.53 TCP_MISS/200 885 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_tblops.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556034, 'Apr  6 01:19:32 172.16.20.221 1649182771.847     88 172.16.10.53 TCP_MISS/200 993 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_tbl.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556035, 'Apr  6 01:19:32 172.16.20.221 1649182771.859     86 172.16.10.53 TCP_MISS/200 860 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_plugin.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556036, 'Apr  6 01:19:32 172.16.20.221 1649182771.884    125 172.16.10.53 TCP_MISS/200 948 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_status.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556037, 'Apr  6 01:19:32 172.16.20.221 1649182771.890    131 172.16.10.53 TCP_MISS/200 939 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_host.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556038, 'Apr  6 01:19:32 172.16.20.221 1649182771.998     84 172.16.10.53 TCP_MISS/200 862 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_export.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556039, 'Apr  6 01:19:32 172.16.20.221 1649182771.999     85 172.16.10.53 TCP_MISS/200 746 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_engine.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556040, 'Apr  6 01:19:32 172.16.20.221 1649182772.000     87 172.16.10.53 TCP_MISS/200 809 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_rights.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556041, 'Apr  6 01:19:32 172.16.20.221 1649182772.001     88 172.16.10.53 TCP_MISS/200 640 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/console.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556042, 'Apr  6 01:19:32 172.16.20.221 1649182772.037    124 172.16.10.53 TCP_MISS/200 871 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_vars.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556043, 'Apr  6 01:19:32 172.16.20.221 1649182772.042    123 172.16.10.53 TCP_MISS/200 567 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_top.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556044, 'Apr  6 01:19:32 172.16.20.221 1649182772.061    122 172.16.10.53 TCP_MISS/200 495 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_asci.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556045, 'Apr  6 01:19:32 172.16.20.221 1649182772.113    200 172.16.10.53 TCP_MISS/200 3053 POST http://119.81.44.155/phpmyadmin/ajax.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556046, 'Apr  6 01:19:32 172.16.20.221 1649182772.149     82 172.16.10.53 TCP_MISS/200 1120 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_theme.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556047, 'Apr  6 01:19:32 172.16.20.221 1649182772.151     84 172.16.10.53 TCP_MISS/200 679 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_passwd.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556048, 'Apr  6 01:19:32 172.16.20.221 1649182772.152     85 172.16.10.53 TCP_MISS/200 816 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/window-new.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556049, 'Apr  6 01:19:32 172.16.20.221 1649182772.152     86 172.16.10.53 TCP_MISS/200 1007 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_lang.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556050, 'Apr  6 01:19:32 172.16.20.221 1649182772.153     86 172.16.10.53 TCP_MISS/200 962 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_error.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556051, 'Apr  6 01:19:32 172.16.20.221 1649182772.195    119 172.16.10.53 TCP_MISS/200 790 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_cog.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556052, 'Apr  6 01:19:32 172.16.20.221 1649182772.250     85 172.16.10.53 TCP_MISS/200 458 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_more.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556053, 'Apr  6 01:19:32 172.16.20.221 1649182772.275    208 172.16.10.53 TCP_MISS/200 3643 POST http://119.81.44.155/phpmyadmin/navigation.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556054, 'Apr  6 01:19:32 172.16.20.221 1649182772.409    200 172.16.10.53 TCP_MISS/200 2939 POST http://119.81.44.155/phpmyadmin/ajax.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556055, 'Apr  6 01:19:34 172.16.20.221 1649182773.754    837 172.16.10.53 TCP_TUNNEL/200 23836 CONNECT login.live.com:443 - HIER_DIRECT/20.190.144.162 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556056, 'Apr  6 01:19:34 172.16.20.221 1649182773.813    409 172.16.10.53 TCP_MISS/200 11195 GET http://119.81.44.155/phpmyadmin/db_structure.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556057, 'Apr  6 01:19:34 172.16.20.221 1649182774.028     84 172.16.10.53 TCP_MISS/200 921 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_props.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556058, 'Apr  6 01:19:34 172.16.20.221 1649182774.031     86 172.16.10.53 TCP_MISS/200 883 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_search.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556059, 'Apr  6 01:19:34 172.16.20.221 1649182774.032     85 172.16.10.53 TCP_MISS/200 654 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_routines.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556060, 'Apr  6 01:19:34 172.16.20.221 1649182774.034     87 172.16.10.53 TCP_MISS/200 1051 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_events.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556061, 'Apr  6 01:19:34 172.16.20.221 1649182774.034     87 172.16.10.53 TCP_MISS/200 487 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_relations.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556062, 'Apr  6 01:19:34 172.16.20.221 1649182774.035     88 172.16.10.53 TCP_MISS/200 794 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_triggers.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556063, 'Apr  6 01:19:34 172.16.20.221 1649182774.131    132 172.16.10.53 TCP_MISS/200 4219 GET http://119.81.44.155/phpmyadmin/navigation.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556064, 'Apr  6 01:19:34 172.16.20.221 1649182774.132     85 172.16.10.53 TCP_MISS/200 2159 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/ajax_clock_small.gif - HIER_DIRECT/119.81.44.155 image/gif', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556065, 'Apr  6 01:19:34 172.16.20.221 1649182774.138     81 172.16.10.53 TCP_MISS/200 725 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_no_favorite.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556066, 'Apr  6 01:19:34 172.16.20.221 1649182774.142     85 172.16.10.53 TCP_MISS/200 941 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_select.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log');
INSERT INTO `log` (`log_id`, `msg`, `date`, `device_id`, `file_name`) VALUES
(556067, 'Apr  6 01:19:34 172.16.20.221 1649182774.143     86 172.16.10.53 TCP_MISS/200 834 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_browse.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556068, 'Apr  6 01:19:34 172.16.20.221 1649182774.173    118 172.16.10.53 TCP_MISS/200 493 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_asc.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556069, 'Apr  6 01:19:34 172.16.20.221 1649182774.221     82 172.16.10.53 TCP_MISS/200 869 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_empty.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556070, 'Apr  6 01:19:34 172.16.20.221 1649182774.221     85 172.16.10.53 TCP_MISS/200 504 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_insrow.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556071, 'Apr  6 01:19:34 172.16.20.221 1649182774.226     82 172.16.10.53 TCP_MISS/200 985 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_drop.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556072, 'Apr  6 01:19:34 172.16.20.221 1649182774.241     84 172.16.10.53 TCP_MISS/200 673 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/bd_select.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556073, 'Apr  6 01:19:34 172.16.20.221 1649182774.262     83 172.16.10.53 TCP_MISS/200 675 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/bd_empty.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556074, 'Apr  6 01:19:34 172.16.20.221 1649182774.265    115 172.16.10.53 TCP_MISS/200 619 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/bd_browse.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556075, 'Apr  6 01:19:34 172.16.20.221 1649182774.311     84 172.16.10.53 TCP_MISS/200 987 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_print.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556076, 'Apr  6 01:19:34 172.16.20.221 1649182774.332     88 172.16.10.53 TCP_MISS/200 4412 GET http://119.81.44.155/phpmyadmin/js/db_structure.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556077, 'Apr  6 01:19:34 172.16.20.221 1649182774.349    120 172.16.10.53 TCP_MISS/200 499 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_tblanalyse.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556078, 'Apr  6 01:19:34 172.16.20.221 1649182774.352    120 172.16.10.53 TCP_MISS/200 919 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_table_add.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556079, 'Apr  6 01:19:34 172.16.20.221 1649182774.355     89 172.16.10.53 TCP_MISS/200 7685 GET http://119.81.44.155/phpmyadmin/js/tbl_change.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556080, 'Apr  6 01:19:34 172.16.20.221 1649182774.388     84 172.16.10.53 TCP_MISS/200 470 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_minus.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556081, 'Apr  6 01:19:34 172.16.20.221 1649182774.447    178 172.16.10.53 TCP_MISS/200 448 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/arrow_ltr.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556082, 'Apr  6 01:19:35 172.16.20.221 1649182775.353    577 172.16.10.53 TCP_TUNNEL/200 23408 CONNECT login.live.com:443 - HIER_DIRECT/20.190.144.162 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556083, 'Apr  6 01:19:35 172.16.20.221 1649182775.363    583 172.16.10.53 TCP_TUNNEL/200 23940 CONNECT login.live.com:443 - HIER_DIRECT/20.190.144.162 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556084, 'Apr  6 01:19:36 172.16.20.221 1649182776.664  23359 172.16.10.53 TCP_TUNNEL/200 64880 CONNECT assets.msn.com:443 - HIER_DIRECT/122.155.237.225 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556085, 'Apr  6 01:19:38 172.16.20.221 1649182778.369    166 172.16.10.53 TCP_MISS/200 11192 GET http://119.81.44.155/phpmyadmin/db_structure.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556086, 'Apr  6 01:19:40 172.16.20.221 1649182780.156  25494 172.16.10.53 TCP_TUNNEL/200 5905 CONNECT assets.msn.com:443 - HIER_DIRECT/122.155.237.225 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556087, 'Apr  6 01:19:40 172.16.20.221 1649182780.334    257 172.16.10.53 TCP_MISS/200 14744 GET http://119.81.44.155/phpmyadmin/db_export.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556088, 'Apr  6 01:19:40 172.16.20.221 1649182780.532     88 172.16.10.53 TCP_MISS/200 6734 GET http://119.81.44.155/phpmyadmin/js/export.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556089, 'Apr  6 01:19:43 172.16.20.221 1649182782.771    701 172.16.10.53 TCP_MISS/200 199093 POST http://119.81.44.155/phpmyadmin/export.php - HIER_DIRECT/119.81.44.155 text/x-sql', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556090, 'Apr  6 01:19:43 172.16.20.221 1649182782.840    755 172.16.10.53 TCP_TUNNEL/200 8608 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556091, 'Apr  6 01:19:43 172.16.20.221 1649182782.938    179 172.16.10.53 TCP_TUNNEL/200 8607 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556092, 'Apr  6 01:19:43 172.16.20.221 1649182783.280  93369 172.16.10.53 TCP_TUNNEL/200 3139 CONNECT array605.prod.do.dsp.mp.microsoft.com:443 - HIER_DIRECT/51.104.164.114 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556093, 'Apr  6 01:20:17 172.16.20.221 1649182817.496 123725 172.16.10.53 TCP_TUNNEL/200 9217 CONNECT activity.windows.com:443 - HIER_DIRECT/20.44.229.112 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556094, 'Apr  6 01:20:45 172.16.20.221 1649182845.275    313 172.16.10.53 TCP_MISS/200 12457 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556095, 'Apr  6 01:20:45 172.16.20.221 1649182845.352  82800 172.16.10.53 TCP_TUNNEL/200 22603994 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556096, 'Apr  6 01:20:45 172.16.20.221 1649182845.379     40 172.16.10.53 TCP_MISS/200 862 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_tblexport.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556097, 'Apr  6 01:20:45 172.16.20.221 1649182845.382  89738 172.16.10.53 TCP_TUNNEL/200 53833 CONNECT www.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556098, 'Apr  6 01:20:45 172.16.20.221 1649182845.382  82543 172.16.10.53 TCP_TUNNEL/200 45603 CONNECT yt3.ggpht.com:443 - HIER_DIRECT/172.217.194.132 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556099, 'Apr  6 01:20:45 172.16.20.221 1649182845.382  69833 172.16.10.53 TCP_TUNNEL/200 43380 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556100, 'Apr  6 01:20:45 172.16.20.221 1649182845.382  87886 172.16.10.53 TCP_TUNNEL/200 4896 CONNECT lh3.google.com:443 - HIER_DIRECT/142.251.10.102 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556101, 'Apr  6 01:20:45 172.16.20.221 1649182845.382  87644 172.16.10.53 TCP_TUNNEL/200 547403 CONNECT cdn.1112.com:443 - HIER_DIRECT/104.22.54.98 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556102, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  90727 172.16.10.53 TCP_TUNNEL/200 478220 CONNECT www.google.com:443 - HIER_DIRECT/74.125.68.103 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556103, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  90455 172.16.10.53 TCP_TUNNEL/200 6941 CONNECT c.msn.com:443 - HIER_DIRECT/52.231.207.240 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556104, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  91494 172.16.10.53 TCP_TUNNEL/200 16218 CONNECT zem.outbrainimg.com:443 - HIER_DIRECT/199.232.46.132 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556105, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  49187 172.16.10.53 TCP_TUNNEL/200 3740 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556106, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  90387 172.16.10.53 TCP_TUNNEL/200 6235 CONNECT srtb.msn.com:443 - HIER_DIRECT/204.79.197.203 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556107, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  46836 172.16.10.53 TCP_TUNNEL/200 233019 CONNECT scontent.xx.fbcdn.net:443 - HIER_DIRECT/157.240.235.1 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556108, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  86888 172.16.10.53 TCP_TUNNEL/200 2759 CONNECT play.google.com:443 - HIER_DIRECT/74.125.24.138 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556109, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  91636 172.16.10.53 TCP_TUNNEL/200 5616 CONNECT obs.cheqzone.com:443 - HIER_DIRECT/50.16.211.97 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556110, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  88388 172.16.10.53 TCP_TUNNEL/200 340880 CONNECT www.1112.com:443 - HIER_DIRECT/172.67.41.32 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556111, 'Apr  6 01:20:45 172.16.20.221 1649182845.383  87579 172.16.10.53 TCP_TUNNEL/200 6697 CONNECT lh3.googleusercontent.com:443 - HIER_DIRECT/142.250.4.132 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556112, 'Apr  6 01:20:45 172.16.20.221 1649182845.384  89872 172.16.10.53 TCP_TUNNEL/200 61624 CONNECT ntp.msn.com:443 - HIER_DIRECT/204.79.197.203 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556113, 'Apr  6 01:20:45 172.16.20.221 1649182845.384  72647 172.16.10.53 TCP_TUNNEL/200 7163 CONNECT edge.activity.windows.com:443 - HIER_DIRECT/20.184.57.167 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556114, 'Apr  6 01:20:45 172.16.20.221 1649182845.384  82997 172.16.10.53 TCP_TUNNEL/200 134698 CONNECT i.ytimg.com:443 - HIER_DIRECT/142.251.10.119 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556115, 'Apr  6 01:20:45 172.16.20.221 1649182845.384  86328 172.16.10.53 TCP_TUNNEL/200 456516 CONNECT www.gstatic.com:443 - HIER_DIRECT/142.251.10.94 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556116, 'Apr  6 01:20:45 172.16.20.221 1649182845.385  31411 172.16.10.53 TCP_TUNNEL/200 54766 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.15.13 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556117, 'Apr  6 01:20:45 172.16.20.221 1649182845.385  90713 172.16.10.53 TCP_TUNNEL/200 8276 CONNECT c.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556118, 'Apr  6 01:20:45 172.16.20.221 1649182845.385  88987 172.16.10.53 TCP_TUNNEL/200 87772 CONNECT www.youtube.com:443 - HIER_DIRECT/74.125.130.91 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556119, 'Apr  6 01:20:45 172.16.20.221 1649182845.387  87092 172.16.10.53 TCP_TUNNEL/200 7826 CONNECT adservice.google.com:443 - HIER_DIRECT/74.125.130.154 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556120, 'Apr  6 01:20:45 172.16.20.221 1649182845.388  89746 172.16.10.53 TCP_TUNNEL/200 8634 CONNECT api.msn.com:443 - HIER_DIRECT/204.79.197.203 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556121, 'Apr  6 01:20:45 172.16.20.221 1649182845.388  85071 172.16.10.53 TCP_TUNNEL/200 8886 CONNECT www.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556122, 'Apr  6 01:20:45 172.16.20.221 1649182845.388  77618 172.16.10.53 TCP_TUNNEL/200 8785 CONNECT edge.microsoft.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556123, 'Apr  6 01:20:45 172.16.20.221 1649182845.393  83835 172.16.10.53 TCP_TUNNEL/200 16800 CONNECT ogs.google.com:443 - HIER_DIRECT/142.250.4.101 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556124, 'Apr  6 01:20:45 172.16.20.221 1649182845.393  87590 172.16.10.53 TCP_TUNNEL/200 6316 CONNECT static.cloudflareinsights.com:443 - HIER_DIRECT/104.18.47.230 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556125, 'Apr  6 01:20:45 172.16.20.221 1649182845.425     78 172.16.10.53 TCP_MISS/200 904 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_tblimport.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556126, 'Apr  6 01:20:45 172.16.20.221 1649182845.468    121 172.16.10.53 TCP_MISS/200 813 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_success.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556127, 'Apr  6 01:20:45 172.16.20.221 1649182845.480     84 172.16.10.53 TCP_MISS/200 797 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_chart.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556128, 'Apr  6 01:20:45 172.16.20.221 1649182845.482    123 172.16.10.53 TCP_MISS/200 725 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_edit.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556129, 'Apr  6 01:20:45 172.16.20.221 1649182845.521    126 172.16.10.53 TCP_MISS/200 1026 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_view_add.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556130, 'Apr  6 01:20:45 172.16.20.221 1649182845.586     86 172.16.10.53 TCP_MISS/200 4016 GET http://119.81.44.155/phpmyadmin/js/gis_data_editor.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556131, 'Apr  6 01:20:45 172.16.20.221 1649182845.594     93 172.16.10.53 TCP_MISS/200 1668 GET http://119.81.44.155/phpmyadmin/js/multi_column_sort.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556132, 'Apr  6 01:20:45 172.16.20.221 1649182845.595     98 172.16.10.53 TCP_MISS/200 1805 GET http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.uitablefilter.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556133, 'Apr  6 01:20:45 172.16.20.221 1649182845.602    114 172.16.10.53 TCP_MISS/200 3007 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556134, 'Apr  6 01:20:45 172.16.20.221 1649182845.628    101 172.16.10.53 TCP_MISS/200 519 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_fulltext.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556135, 'Apr  6 01:20:45 172.16.20.221 1649182845.679     87 172.16.10.53 TCP_MISS/200 10033 GET http://119.81.44.155/phpmyadmin/js/sql.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556136, 'Apr  6 01:20:45 172.16.20.221 1649182845.718    216 172.16.10.53 TCP_MISS/200 18667 GET http://119.81.44.155/phpmyadmin/js/makegrid.js? - HIER_DIRECT/119.81.44.155 application/javascript', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556137, 'Apr  6 01:20:46 172.16.20.221 1649182845.869     39 172.16.10.53 TCP_MISS/200 458 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/col_drop.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556138, 'Apr  6 01:20:46 172.16.20.221 1649182845.912     87 172.16.10.53 TCP_MISS/200 449 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/col_pointer.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556139, 'Apr  6 01:20:49 172.16.20.221 1649182849.151    131 172.16.10.53 TCP_MISS/200 5294 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556140, 'Apr  6 01:20:49 172.16.20.221 1649182849.177    145 172.16.10.53 TCP_MISS/200 1400 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556141, 'Apr  6 01:21:03 172.16.20.221 1649182863.638    124 172.16.10.53 TCP_MISS/200 473 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-bg_glass_75_dadada_1x400.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556142, 'Apr  6 01:21:03 172.16.20.221 1649182863.639    126 172.16.10.53 TCP_MISS/200 469 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-bg_highlight-soft_75_cccccc_1x100.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556143, 'Apr  6 01:21:03 172.16.20.221 1649182863.639    126 172.16.10.53 TCP_MISS/200 468 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-bg_glass_75_e6e6e6_1x400.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556144, 'Apr  6 01:21:03 172.16.20.221 1649182863.642    129 172.16.10.53 TCP_MISS/200 4114 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-icons_222222_256x240.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556145, 'Apr  6 01:21:03 172.16.20.221 1649182863.643    129 172.16.10.53 TCP_MISS/200 4114 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-icons_888888_256x240.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556146, 'Apr  6 01:21:14 172.16.20.221 1649182874.569    128 172.16.10.53 TCP_MISS/200 419 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-bg_glass_65_ffffff_1x400.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556147, 'Apr  6 01:21:31 172.16.20.221 1649182891.558    124 172.16.10.53 TCP_MISS/200 1015 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_lock.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556148, 'Apr  6 01:21:32 172.16.20.221 1649182892.169    744 172.16.10.53 TCP_MISS/200 4746 POST http://119.81.44.155/phpmyadmin/db_sql_autocomplete.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556149, 'Apr  6 01:21:32 172.16.20.221 1649182892.207    195 172.16.10.53 TCP_MISS/200 1508 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556150, 'Apr  6 01:21:36 172.16.20.221 1649182896.066    194 172.16.10.53 TCP_MISS/200 1493 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556151, 'Apr  6 01:21:36 172.16.20.221 1649182896.792    143 172.16.10.53 TCP_MISS/200 1501 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556152, 'Apr  6 01:21:38 172.16.20.221 1649182897.890    190 172.16.10.53 TCP_MISS/200 1506 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556153, 'Apr  6 01:21:39 172.16.20.221 1649182899.059    193 172.16.10.53 TCP_MISS/200 1507 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556154, 'Apr  6 01:21:40 172.16.20.221 1649182900.125    155 172.16.10.53 TCP_MISS/200 1493 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556155, 'Apr  6 01:21:41 172.16.20.221 1649182900.993    146 172.16.10.53 TCP_MISS/200 1495 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556156, 'Apr  6 01:21:42 172.16.20.221 1649182902.031    201 172.16.10.53 TCP_MISS/200 1501 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556157, 'Apr  6 01:21:43 172.16.20.221 1649182903.104    199 172.16.10.53 TCP_MISS/200 1499 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556158, 'Apr  6 01:21:44 172.16.20.221 1649182904.011    200 172.16.10.53 TCP_MISS/200 1493 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556159, 'Apr  6 01:21:46 172.16.20.221 1649182906.051    197 172.16.10.53 TCP_MISS/200 1503 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556160, 'Apr  6 01:21:46 172.16.20.221 1649182906.123    123 172.16.10.53 TCP_MISS/200 937 GET http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_unlink.png - HIER_DIRECT/119.81.44.155 image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556161, 'Apr  6 01:21:52 172.16.20.221 1649182912.328    239 172.16.10.53 TCP_MISS/200 1953 POST http://119.81.44.155/phpmyadmin/import.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556162, 'Apr  6 01:21:55 172.16.20.221 1649182915.726    207 172.16.10.53 TCP_MISS/200 1496 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556163, 'Apr  6 01:21:57 172.16.20.221 1649182917.389    216 172.16.10.53 TCP_MISS/200 1969 POST http://119.81.44.155/phpmyadmin/import.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556164, 'Apr  6 01:22:05 172.16.20.221 1649182925.352    190 172.16.10.53 TCP_MISS/200 1496 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556165, 'Apr  6 01:22:08 172.16.20.221 1649182928.558    144 172.16.10.53 TCP_MISS/200 1387 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556166, 'Apr  6 01:22:10 172.16.20.221 1649182930.504    280 172.16.10.53 TCP_MISS/200 3497 POST http://119.81.44.155/phpmyadmin/import.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556167, 'Apr  6 01:22:10 172.16.20.221 1649182930.744     69 172.16.10.53 TCP_MISS/200 2994 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556168, 'Apr  6 01:22:20 172.16.20.221 1649182940.462    233 172.16.10.53 TCP_MISS/200 8522 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556169, 'Apr  6 01:22:20 172.16.20.221 1649182940.711    112 172.16.10.53 TCP_MISS/200 3007 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556170, 'Apr  6 01:22:24 172.16.20.221 1649182944.026    153 172.16.10.53 TCP_MISS/200 6130 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556171, 'Apr  6 01:22:24 172.16.20.221 1649182944.066  65218 172.16.10.53 TCP_TUNNEL/200 2303 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.13.19 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556172, 'Apr  6 01:22:24 172.16.20.221 1649182944.272    114 172.16.10.53 TCP_MISS/200 3016 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556173, 'Apr  6 01:22:25 172.16.20.221 1649182945.214    174 172.16.10.53 TCP_MISS/200 10706 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556174, 'Apr  6 01:22:25 172.16.20.221 1649182945.433    116 172.16.10.53 TCP_MISS/200 3035 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556175, 'Apr  6 01:22:28 172.16.20.221 1649182948.190    192 172.16.10.53 TCP_MISS/200 11153 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556176, 'Apr  6 01:22:28 172.16.20.221 1649182948.442    118 172.16.10.53 TCP_MISS/200 3042 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556177, 'Apr  6 01:22:29 172.16.20.221 1649182949.677  74233 172.16.10.53 TCP_TUNNEL/200 8890 CONNECT activity.windows.com:443 - HIER_DIRECT/20.44.229.112 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556178, 'Apr  6 01:22:30 172.16.20.221 1649182950.482    186 172.16.10.53 TCP_MISS/200 9191 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556179, 'Apr  6 01:22:30 172.16.20.221 1649182950.804    116 172.16.10.53 TCP_MISS/200 3056 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556180, 'Apr  6 01:22:31 172.16.20.221 1649182951.881    158 172.16.10.53 TCP_MISS/200 8815 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556181, 'Apr  6 01:22:32 172.16.20.221 1649182952.089    117 172.16.10.53 TCP_MISS/200 3058 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556182, 'Apr  6 01:22:33 172.16.20.221 1649182953.436    173 172.16.10.53 TCP_MISS/200 9612 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556183, 'Apr  6 01:22:33 172.16.20.221 1649182953.639    118 172.16.10.53 TCP_MISS/200 3080 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556184, 'Apr  6 01:22:36 172.16.20.221 1649182956.796    233 172.16.10.53 TCP_MISS/200 3427 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556185, 'Apr  6 01:22:37 172.16.20.221 1649182956.984     68 172.16.10.53 TCP_MISS/200 3083 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556186, 'Apr  6 01:22:39 172.16.20.221 1649182959.725    342 172.16.10.53 TCP_MISS/200 3912 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556187, 'Apr  6 01:22:39 172.16.20.221 1649182959.897     68 172.16.10.53 TCP_MISS/200 3082 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556188, 'Apr  6 01:22:41 172.16.20.221 1649182961.654    172 172.16.10.53 TCP_MISS/200 8128 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556189, 'Apr  6 01:22:42 172.16.20.221 1649182961.975    112 172.16.10.53 TCP_MISS/200 3108 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556190, 'Apr  6 01:22:43 172.16.20.221 1649182963.262    160 172.16.10.53 TCP_MISS/200 8558 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556191, 'Apr  6 01:22:43 172.16.20.221 1649182963.506    113 172.16.10.53 TCP_MISS/200 3108 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556192, 'Apr  6 01:22:44 172.16.20.221 1649182964.337    152 172.16.10.53 TCP_MISS/200 8133 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556193, 'Apr  6 01:22:44 172.16.20.221 1649182964.537    114 172.16.10.53 TCP_MISS/200 3097 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556194, 'Apr  6 01:22:47 172.16.20.221 1649182967.397    177 172.16.10.53 TCP_MISS/200 8557 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556195, 'Apr  6 01:22:47 172.16.20.221 1649182967.611    114 172.16.10.53 TCP_MISS/200 3097 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556196, 'Apr  6 01:22:49 172.16.20.221 1649182969.200    166 172.16.10.53 TCP_MISS/200 9076 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556197, 'Apr  6 01:22:49 172.16.20.221 1649182969.428    118 172.16.10.53 TCP_MISS/200 3098 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556198, 'Apr  6 01:22:50 172.16.20.221 1649182970.676    156 172.16.10.53 TCP_MISS/200 11670 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556199, 'Apr  6 01:22:50 172.16.20.221 1649182970.901    116 172.16.10.53 TCP_MISS/200 3103 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556200, 'Apr  6 01:22:52 172.16.20.221 1649182972.575     80 172.16.10.53 TCP_MISS/200 5289 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556201, 'Apr  6 01:22:52 172.16.20.221 1649182972.657    145 172.16.10.53 TCP_MISS/200 1387 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556202, 'Apr  6 01:22:53 172.16.20.221 1649182973.415    252 172.16.10.53 TCP_MISS/200 4744 POST http://119.81.44.155/phpmyadmin/db_sql_autocomplete.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556203, 'Apr  6 01:22:53 172.16.20.221 1649182973.806    145 172.16.10.53 TCP_MISS/200 1387 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556204, 'Apr  6 01:22:56 172.16.20.221 1649182976.225    152 172.16.10.53 TCP_MISS/200 1389 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556205, 'Apr  6 01:22:58 172.16.20.221 1649182978.915    143 172.16.10.53 TCP_MISS/200 1375 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556206, 'Apr  6 01:22:59 172.16.20.221 1649182979.780    149 172.16.10.53 TCP_MISS/200 1385 POST http://119.81.44.155/phpmyadmin/lint.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556207, 'Apr  6 01:23:01 172.16.20.221 1649182981.514    275 172.16.10.53 TCP_MISS/200 3476 POST http://119.81.44.155/phpmyadmin/import.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556208, 'Apr  6 01:23:01 172.16.20.221 1649182981.786     71 172.16.10.53 TCP_MISS/200 3102 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556209, 'Apr  6 01:23:04 172.16.20.221 1649182984.658    195 172.16.10.53 TCP_MISS/200 11016 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556210, 'Apr  6 01:23:04 172.16.20.221 1649182984.876    116 172.16.10.53 TCP_MISS/200 3091 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556211, 'Apr  6 01:23:14 172.16.20.221 1649182994.097    276 172.16.10.53 TCP_MISS/200 11187 POST http://119.81.44.155/phpmyadmin/sql.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556212, 'Apr  6 01:23:14 172.16.20.221 1649182994.286     67 172.16.10.53 TCP_MISS/200 3097 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556213, 'Apr  6 01:23:18 172.16.20.221 1649182998.137    239 172.16.10.53 TCP_MISS/200 3588 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556214, 'Apr  6 01:23:18 172.16.20.221 1649182998.314     68 172.16.10.53 TCP_MISS/200 3097 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556215, 'Apr  6 01:23:18 172.16.20.221 1649182998.935 120109 172.16.10.53 TCP_TUNNEL/200 4574 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556216, 'Apr  6 01:23:21 172.16.20.221 1649183001.224    388 172.16.10.53 TCP_MISS/200 3903 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556217, 'Apr  6 01:23:21 172.16.20.221 1649183001.346     69 172.16.10.53 TCP_MISS/200 3092 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556218, 'Apr  6 01:23:22 172.16.20.221 1649183002.734    200 172.16.10.53 TCP_MISS/200 11711 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556219, 'Apr  6 01:23:23 172.16.20.221 1649183002.966    114 172.16.10.53 TCP_MISS/200 3099 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556220, 'Apr  6 01:23:25 172.16.20.221 1649183005.732    333 172.16.10.53 TCP_MISS/200 23275 POST http://119.81.44.155/phpmyadmin/sql.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556221, 'Apr  6 01:23:26 172.16.20.221 1649183006.132     69 172.16.10.53 TCP_MISS/200 3108 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556222, 'Apr  6 01:23:30 172.16.20.221 1649183010.564    312 172.16.10.53 TCP_MISS/200 5278 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556223, 'Apr  6 01:23:30 172.16.20.221 1649183010.815     67 172.16.10.53 TCP_MISS/200 3092 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556224, 'Apr  6 01:23:34 172.16.20.221 1649183014.477   1665 172.16.10.53 TCP_MISS/200 3918 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556225, 'Apr  6 01:23:34 172.16.20.221 1649183014.912     70 172.16.10.53 TCP_MISS/200 3099 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556226, 'Apr  6 01:23:36 172.16.20.221 1649183016.598    177 172.16.10.53 TCP_MISS/200 8594 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556227, 'Apr  6 01:23:36 172.16.20.221 1649183016.806    110 172.16.10.53 TCP_MISS/200 3096 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556228, 'Apr  6 01:23:39 172.16.20.221 1649183019.709    225 172.16.10.53 TCP_MISS/200 3380 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556229, 'Apr  6 01:23:39 172.16.20.221 1649183019.849     70 172.16.10.53 TCP_MISS/200 3095 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556230, 'Apr  6 01:23:41 172.16.20.221 1649183021.191    278 172.16.10.53 TCP_MISS/200 3882 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556231, 'Apr  6 01:23:41 172.16.20.221 1649183021.343     69 172.16.10.53 TCP_MISS/200 3093 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556232, 'Apr  6 01:23:42 172.16.20.221 1649183022.959    191 172.16.10.53 TCP_MISS/200 9683 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556233, 'Apr  6 01:23:43 172.16.20.221 1649183023.169    110 172.16.10.53 TCP_MISS/200 3092 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556234, 'Apr  6 01:23:44 172.16.20.221 1649183024.844    172 172.16.10.53 TCP_MISS/200 9266 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556235, 'Apr  6 01:23:45 172.16.20.221 1649183025.057    115 172.16.10.53 TCP_MISS/200 3102 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556236, 'Apr  6 01:23:47 172.16.20.221 1649183027.096    229 172.16.10.53 TCP_MISS/200 3398 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556237, 'Apr  6 01:23:47 172.16.20.221 1649183027.266     66 172.16.10.53 TCP_MISS/200 3089 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556238, 'Apr  6 01:23:48 172.16.20.221 1649183028.390    240 172.16.10.53 TCP_MISS/200 1952 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556239, 'Apr  6 01:23:51 172.16.20.221 1649183031.773    266 172.16.10.53 TCP_MISS/200 3891 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556240, 'Apr  6 01:23:51 172.16.20.221 1649183031.951     71 172.16.10.53 TCP_MISS/200 3087 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556241, 'Apr  6 01:23:52 172.16.20.221 1649183032.868 120035 172.16.10.53 TCP_TUNNEL/200 7515 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556242, 'Apr  6 01:23:53 172.16.20.221 1649183033.670    166 172.16.10.53 TCP_MISS/200 9244 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556243, 'Apr  6 01:23:53 172.16.20.221 1649183033.784 125482 172.16.10.53 TCP_TUNNEL/200 4575 CONNECT update.code.visualstudio.com:443 - HIER_DIRECT/20.43.132.130 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556244, 'Apr  6 01:23:53 172.16.20.221 1649183033.883    115 172.16.10.53 TCP_MISS/200 3086 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556245, 'Apr  6 01:23:56 172.16.20.221 1649183036.402 188274 172.16.10.53 TCP_TUNNEL/200 3599 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556246, 'Apr  6 01:23:57 172.16.20.221 1649183037.784    233 172.16.10.53 TCP_MISS/200 3388 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556247, 'Apr  6 01:23:57 172.16.20.221 1649183037.844  88164 172.16.10.53 TCP_TUNNEL/200 8926 CONNECT activity.windows.com:443 - HIER_DIRECT/20.44.229.112 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556248, 'Apr  6 01:23:57 172.16.20.221 1649183037.929     69 172.16.10.53 TCP_MISS/200 3093 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556249, 'Apr  6 01:24:00 172.16.20.221 1649183040.145    308 172.16.10.53 TCP_MISS/200 3916 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556250, 'Apr  6 01:24:00 172.16.20.221 1649183040.616     69 172.16.10.53 TCP_MISS/200 3084 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556251, 'Apr  6 01:24:01 172.16.20.221 1649183041.933    222 172.16.10.53 TCP_MISS/200 8797 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556252, 'Apr  6 01:24:02 172.16.20.221 1649183042.196    117 172.16.10.53 TCP_MISS/200 3093 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556253, 'Apr  6 01:24:04 172.16.20.221 1649183044.186    222 172.16.10.53 TCP_MISS/200 3408 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556254, 'Apr  6 01:24:04 172.16.20.221 1649183044.367     67 172.16.10.53 TCP_MISS/200 3095 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556255, 'Apr  6 01:24:05 172.16.20.221 1649183045.519    283 172.16.10.53 TCP_MISS/200 3893 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556256, 'Apr  6 01:24:05 172.16.20.221 1649183045.652     67 172.16.10.53 TCP_MISS/200 3090 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556257, 'Apr  6 01:24:06 172.16.20.221 1649183046.854    162 172.16.10.53 TCP_MISS/200 10856 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556258, 'Apr  6 01:24:07 172.16.20.221 1649183047.104    121 172.16.10.53 TCP_MISS/200 3089 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556259, 'Apr  6 01:24:08 172.16.20.221 1649183048.911  92053 172.16.10.53 TCP_TUNNEL/200 3138 CONNECT array614.prod.do.dsp.mp.microsoft.com:443 - HIER_DIRECT/20.54.24.231 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556260, 'Apr  6 01:24:10 172.16.20.221 1649183049.852    226 172.16.10.53 TCP_MISS/200 3358 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556261, 'Apr  6 01:24:11 172.16.20.221 1649183050.042     69 172.16.10.53 TCP_MISS/200 3090 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556262, 'Apr  6 01:24:12 172.16.20.221 1649183051.391    220 172.16.10.53 TCP_MISS/200 1960 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556263, 'Apr  6 01:24:15 172.16.20.221 1649183054.250    255 172.16.10.53 TCP_MISS/200 4025 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556264, 'Apr  6 01:24:15 172.16.20.221 1649183054.444     73 172.16.10.53 TCP_MISS/200 3091 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556265, 'Apr  6 01:24:16 172.16.20.221 1649183055.729    141 172.16.10.53 TCP_MISS/200 6103 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556266, 'Apr  6 01:24:16 172.16.20.221 1649183055.953    115 172.16.10.53 TCP_MISS/200 3083 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log');
INSERT INTO `log` (`log_id`, `msg`, `date`, `device_id`, `file_name`) VALUES
(556267, 'Apr  6 01:24:18 172.16.20.221 1649183057.256    152 172.16.10.53 TCP_MISS/200 6111 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556268, 'Apr  6 01:24:18 172.16.20.221 1649183057.488    121 172.16.10.53 TCP_MISS/200 3087 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556269, 'Apr  6 01:24:18 172.16.20.221 1649183057.993    175 172.16.10.53 TCP_MISS/200 10149 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556270, 'Apr  6 01:24:19 172.16.20.221 1649183058.242    115 172.16.10.53 TCP_MISS/200 3083 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556271, 'Apr  6 01:24:24 172.16.20.221 1649183063.023    187 172.16.10.53 TCP_MISS/200 8895 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556272, 'Apr  6 01:24:24 172.16.20.221 1649183063.253    117 172.16.10.53 TCP_MISS/200 3097 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556273, 'Apr  6 01:24:34 172.16.20.221 1649183073.186    193 172.16.10.53 TCP_MISS/200 6131 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556274, 'Apr  6 01:24:34 172.16.20.221 1649183073.436    117 172.16.10.53 TCP_MISS/200 3091 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556275, 'Apr  6 01:24:35 172.16.20.221 1649183074.190    180 172.16.10.53 TCP_MISS/200 11310 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556276, 'Apr  6 01:24:35 172.16.20.221 1649183074.484    113 172.16.10.53 TCP_MISS/200 3096 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556277, 'Apr  6 01:24:40 172.16.20.221 1649183079.370    340 172.16.10.53 TCP_MISS/200 19029 POST http://119.81.44.155/phpmyadmin/sql.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556278, 'Apr  6 01:24:41 172.16.20.221 1649183080.031     66 172.16.10.53 TCP_MISS/200 3088 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556279, 'Apr  6 01:24:44 172.16.20.221 1649183083.171    120 172.16.10.53 TCP_MISS/200 2945 GET http://119.81.44.155/phpmyadmin/export.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556280, 'Apr  6 01:24:45 172.16.20.221 1649183084.199    269 172.16.10.53 TCP_MISS/200 4940 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556281, 'Apr  6 01:24:45 172.16.20.221 1649183084.479     65 172.16.10.53 TCP_MISS/200 3101 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556282, 'Apr  6 01:24:48 172.16.20.221 1649183087.955   1426 172.16.10.53 TCP_MISS/200 3892 POST http://119.81.44.155/phpmyadmin/tbl_row_action.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556283, 'Apr  6 01:24:49 172.16.20.221 1649183088.095     71 172.16.10.53 TCP_MISS/200 3089 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556284, 'Apr  6 01:24:50 172.16.20.221 1649183089.596    196 172.16.10.53 TCP_MISS/200 10641 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556285, 'Apr  6 01:24:50 172.16.20.221 1649183089.828    114 172.16.10.53 TCP_MISS/200 3096 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556286, 'Apr  6 01:25:01 172.16.20.221 1649183100.792    329 172.16.10.53 TCP_TUNNEL/200 8532 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/20.212.97.243 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556287, 'Apr  6 01:25:12 172.16.20.221 1649183111.358 198529 172.16.10.53 TCP_TUNNEL/200 3267930 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556288, 'Apr  6 01:25:16 172.16.20.221 1649183115.998   1131 172.16.10.53 TCP_TUNNEL/200 4900 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/52.182.143.210 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556289, 'Apr  6 01:25:20 172.16.20.221 1649183119.744 206799 172.16.10.53 TCP_TUNNEL/200 17362658 CONNECT rr1---sn-npoeenle.googlevideo.com:443 - HIER_DIRECT/74.125.164.134 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556290, 'Apr  6 01:25:29 172.16.20.221 1649183128.493 120107 172.16.10.53 TCP_TUNNEL/200 2352 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556291, 'Apr  6 01:25:33 172.16.20.221 1649183132.366 286724 172.16.10.53 TCP_TUNNEL/200 9356 CONNECT edge.microsoft.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556292, 'Apr  6 01:25:49 172.16.20.221 1649183148.702    316 172.16.10.53 TCP_TUNNEL/200 4022 CONNECT settings-win.data.microsoft.com:443 - HIER_DIRECT/40.119.249.228 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556293, 'Apr  6 01:26:01 172.16.20.221 1649183160.653  60204 172.16.10.53 TCP_MISS/503 4257 GET http://119.81.44.155:8081/ - HIER_DIRECT/119.81.44.155 text/html', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556294, 'Apr  6 01:26:01 172.16.20.221 1649183160.968      0 172.16.10.53 TCP_MEM_HIT/200 11704 GET http://proxy:3128/squid-internal-static/icons/SN.png - HIER_NONE/- image/png', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556295, 'Apr  6 01:26:20 172.16.20.221 1649183179.547  12318 172.16.10.53 TCP_MISS_ABORTED/000 0 GET http://119.81.44.155:8081/ - HIER_DIRECT/119.81.44.155 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556296, 'Apr  6 01:26:20 172.16.20.221 1649183179.734  18279 172.16.10.53 TCP_MISS_ABORTED/000 0 GET http://119.81.44.155:8081/favicon.ico - HIER_DIRECT/119.81.44.155 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556297, 'Apr  6 01:26:58 172.16.20.221 1649183217.586  65206 172.16.10.53 TCP_TUNNEL/200 139924 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.13.19 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556298, 'Apr  6 01:26:59 172.16.20.221 1649183218.295 372756 172.16.10.53 TCP_TUNNEL/200 58391 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556299, 'Apr  6 01:27:14 172.16.20.221 1649183233.301 149710 172.16.10.53 TCP_TUNNEL/200 7684 CONNECT onedriveclucproddm20026.blob.core.windows.net:443 - HIER_DIRECT/20.150.95.132 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556300, 'Apr  6 01:27:41 172.16.20.221 1649183260.256 143100 172.16.10.53 TCP_TUNNEL/200 16931687 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556301, 'Apr  6 01:28:05 172.16.20.221 1649183284.576 120402 172.16.10.53 TCP_TUNNEL/200 2128 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556302, 'Apr  6 01:28:14 172.16.20.221 1649183293.556 525249 172.16.10.53 TCP_TUNNEL/200 31920 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.90 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556303, 'Apr  6 01:28:15 172.16.20.221 1649183294.837 177680 172.16.10.53 TCP_TUNNEL/200 24420904 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 01:31:22', 94, '172.16.20.221,01:00,2022-04-06.log'),
(556304, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556305, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556306, 'Mar 21 20:57:35 103.136.249.119 VޯE', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556307, 'Mar 21 20:57:35 103.136.249.119 QfaĐG`;P', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556308, 'Mar 21 20:57:35 103.136.249.119 \Z/+	', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556309, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556310, 'Mar 21 20:57:35 103.136.249.119 /', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556311, 'Mar 21 20:57:35 103.136.249.119 5', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556312, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556313, 'Mar 21 20:57:35 103.136.249.119 i3t', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556314, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556315, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556316, 'Mar 21 20:57:35 103.136.249.119', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556317, '119.81.44.155', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556318, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556319, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556320, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556321, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556322, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556323, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556324, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556325, 'Mar 21 20:57:35 103.136.249.119', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556326, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556327, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556328, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556329, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556330, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556331, 'Mar 21 20:57:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556332, 'Mar 21 20:57:55 103.136.249.119 GET / HTTP/1.1', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556333, 'Mar 21 20:57:55 103.136.249.119 Host: 119.81.44.155:514', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556334, 'Mar 21 20:57:55 103.136.249.119 Cookie: rememberMe=1', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556335, 'Mar 21 20:57:58 103.136.249.119 h2http/1.1spdy/3.1', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556336, 'Mar 21 20:58:15 103.136.249.119', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556337, 'Mar 21 20:58:15 103.136.249.119 (r', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556338, 'Mar 21 20:58:15 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556339, 'Mar 21 20:58:15 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556340, 'Mar 21 20:58:15 103.136.249.119 |', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556341, 'Mar 21 20:58:25 103.136.249.119', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556342, 'Mar 21 20:58:25 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556343, 'Mar 21 20:58:25 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556344, 'Mar 21 20:58:25 103.136.249.119 versionbind', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556345, 'Mar 21 20:58:25 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556346, 'Mar 21 20:58:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556347, 'Mar 21 20:58:35 103.136.249.119', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556348, 'Mar 21 20:58:35 103.136.249.119 ', '2022-04-06 01:33:41', 95, '103.136.249.119,20:00,2022-03-21.log'),
(556349, 'Mar 24 01:32:58 209.97.136.164 GET / HTTP/1.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556350, 'Mar 24 01:32:59 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556351, 'Mar 24 01:32:59 209.97.136.164 (r', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556352, 'Mar 24 01:32:59 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556353, 'Mar 24 01:32:59 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556354, 'Mar 24 01:32:59 209.97.136.164 |', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556355, 'Mar 24 01:33:01 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556356, 'Mar 24 01:33:01 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556357, 'Mar 24 01:33:01 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556358, 'Mar 24 01:33:01 209.97.136.164 versionbind', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556359, 'Mar 24 01:33:01 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556360, 'Mar 24 01:33:03 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556361, 'Mar 24 01:33:03 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556362, 'Mar 24 01:33:03 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556363, 'Mar 24 01:33:04 209.97.136.164 #ST', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556364, 'Mar 24 01:33:06 209.97.136.164 n', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556365, 'Mar 24 01:33:08 209.97.136.164 beio', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556366, 'Mar 24 01:33:08 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556367, 'Mar 24 01:33:08 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556368, 'Mar 24 01:33:08 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556369, 'Mar 24 01:33:08 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556370, 'Mar 24 01:33:08 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556371, 'Mar 24 01:33:09 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556372, 'Mar 24 01:33:11 209.97.136.164 OPTIONS / HTTP/1.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556373, 'Mar 24 01:33:13 209.97.136.164 OPTIONS / RTSP/1.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556374, 'Mar 24 01:33:14 209.97.136.164 HELP', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556375, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556376, 'Mar 24 01:33:16 209.97.136.164 S', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556377, 'Mar 24 01:33:16 209.97.136.164 O', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556378, 'Mar 24 01:33:16 209.97.136.164 ?G,`~', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556379, 'Mar 24 01:33:16 209.97.136.164 {Ֆw<=on', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556380, 'Mar 24 01:33:16 209.97.136.164 (', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556381, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556382, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556383, 'Mar 24 01:33:16 209.97.136.164 f', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556384, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556385, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556386, 'Mar 24 01:33:16 209.97.136.164 e', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556387, 'Mar 24 01:33:16 209.97.136.164 d', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556388, 'Mar 24 01:33:16 209.97.136.164 c', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556389, 'Mar 24 01:33:16 209.97.136.164 b', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556390, 'Mar 24 01:33:16 209.97.136.164 a', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556391, 'Mar 24 01:33:16 209.97.136.164 `', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556392, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556393, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556394, 'Mar 24 01:33:16 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556395, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556396, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556397, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556398, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556399, 'Mar 24 01:33:16 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556400, 'Mar 24 01:33:18 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556401, 'Mar 24 01:33:18 209.97.136.164 %', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556402, 'Mar 24 01:33:18 209.97.136.164 Cookie: mstshash=beio', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556403, 'Mar 24 01:33:18 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556404, 'Mar 24 01:33:18 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556405, 'Mar 24 01:33:18 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556406, 'Mar 24 01:33:19 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556407, 'Mar 24 01:33:19 209.97.136.164 i', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556408, 'Mar 24 01:33:19 209.97.136.164 eUrandom1random2random3random4', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556409, 'Mar 24 01:33:19 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556410, 'Mar 24 01:33:19 209.97.136.164 /', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556411, 'Mar 24 01:33:19 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556412, 'Mar 24 01:33:19 209.97.136.164 9', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556413, 'Mar 24 01:33:19 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556414, 'Mar 24 01:33:19 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556415, 'Mar 24 01:33:19 209.97.136.164 0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556416, 'Mar 24 01:33:19 209.97.136.164 ,', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556417, 'Mar 24 01:33:19 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556418, 'Mar 24 01:33:19 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556419, 'Mar 24 01:33:19 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556420, 'Mar 24 01:33:21 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556421, 'Mar 24 01:33:21 209.97.136.164 qjn0k', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556422, 'Mar 24 01:33:21 209.97.136.164 ^0\\', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556423, 'Mar 24 01:33:21 209.97.136.164 P', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556424, 'Mar 24 01:33:21 209.97.136.164 NM0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556425, 'Mar 24 01:33:23 209.97.136.164 0krbtgtNM19700101000000Z٨0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556426, 'Mar 24 01:33:23 209.97.136.164 SMBr', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556427, 'Mar 24 01:33:23 209.97.136.164 @', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556428, 'Mar 24 01:33:23 209.97.136.164 @', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556429, 'Mar 24 01:33:23 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556430, 'Mar 24 01:33:23 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556431, 'Mar 24 01:33:23 209.97.136.164 PC NETWORK PROGRAM 1.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556432, 'Mar 24 01:33:23 209.97.136.164 MICROSOFT NETWORKS 1.03', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556433, 'Mar 24 01:33:23 209.97.136.164 MICROSOFT NETWORKS 3.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556434, 'Mar 24 01:33:23 209.97.136.164 LANMAN1.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556435, 'Mar 24 01:33:23 209.97.136.164 LM1.2X002', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556436, 'Mar 24 01:33:23 209.97.136.164 Samba', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556437, 'Mar 24 01:33:23 209.97.136.164 NT LANMAN 1.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556438, 'Mar 24 01:33:23 209.97.136.164 NT LM 0.12', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556439, 'Mar 24 01:33:24 209.97.136.164 l', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556440, 'Mar 24 01:33:24 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556441, 'Mar 24 01:33:26 209.97.136.164 GET /nice%20ports%2C/Tri%6Eity.txt%2ebak HTTP/1.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556442, 'Mar 24 01:33:28 209.97.136.164 default', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556443, 'Mar 24 01:33:29 209.97.136.164 0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556444, 'Mar 24 01:33:29 209.97.136.164 -c', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556445, 'Mar 24 01:33:29 209.97.136.164 $', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556446, 'Mar 24 01:33:29 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556447, 'Mar 24 01:33:29 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556448, 'Mar 24 01:33:29 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556449, 'Mar 24 01:33:29 209.97.136.164 d', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556450, 'Mar 24 01:33:29 209.97.136.164 objectClass0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556451, 'Mar 24 01:33:31 209.97.136.164 0`', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556452, 'Mar 24 01:33:31 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556453, 'Mar 24 01:33:33 209.97.136.164 OPTIONS sip:nm SIP/2.0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556454, 'Mar 24 01:33:33 209.97.136.164 Via: SIP/2.0/TCP nm;branch=foo', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556455, 'Mar 24 01:33:33 209.97.136.164 From: <sip:nm@nm>;tag=root', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556456, 'Mar 24 01:33:33 209.97.136.164 To: <sip:nm2@nm2>', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556457, 'Mar 24 01:33:33 209.97.136.164 Call-ID: 50000', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556458, 'Mar 24 01:33:33 209.97.136.164 CSeq: 42 OPTIONS', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556459, 'Mar 24 01:33:33 209.97.136.164 Max-Forwards: 70', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556460, 'Mar 24 01:33:33 209.97.136.164 Content-Length: 0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556461, 'Mar 24 01:33:33 209.97.136.164 Contact: <sip:nm@nm>', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556462, 'Mar 24 01:33:33 209.97.136.164 Accept: application/sdp', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556463, 'Mar 24 01:33:34 209.97.136.164 TNMP', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556464, 'Mar 24 01:33:34 209.97.136.164 TNME', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556465, 'Mar 24 01:33:34 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556466, 'Mar 24 01:33:36 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556467, 'Mar 24 01:33:36 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556468, 'Mar 24 01:33:38 209.97.136.164 DmdT', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556469, 'Mar 24 01:33:38 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556470, 'Mar 24 01:33:38 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556471, 'Mar 24 01:33:38 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556472, 'Mar 24 01:33:39 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556473, 'Mar 24 01:33:39 209.97.136.164 :', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556474, 'Mar 24 01:33:39 209.97.136.164 /', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556475, 'Mar 24 01:33:39 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556476, 'Mar 24 01:33:39 209.97.136.164 @', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556477, 'Mar 24 01:33:39 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556478, 'Mar 24 01:33:39 209.97.136.164 =', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556479, 'Mar 24 01:33:39 209.97.136.164 /', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556480, 'Mar 24 01:33:39 209.97.136.164 @', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556481, 'Mar 24 01:33:41 209.97.136.164 JRMI', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556482, 'Mar 24 01:33:43 209.97.136.164 K', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556483, 'Mar 24 01:33:43 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556484, 'Mar 24 01:33:43 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556485, 'Mar 24 01:33:43 209.97.136.164 MMS', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556486, 'Mar 24 01:33:43 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556487, 'Mar 24 01:33:43 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556488, 'Mar 24 01:33:43 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556489, 'Mar 24 01:33:43 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556490, 'Mar 24 01:33:43 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556491, 'Mar 24 01:33:43 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556492, 'Mar 24 01:33:43 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556493, 'Mar 24 01:33:43 209.97.136.164 N', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556494, 'Mar 24 01:33:43 209.97.136.164 S', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556495, 'Mar 24 01:33:43 209.97.136.164 P', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556496, 'Mar 24 01:33:43 209.97.136.164 l', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556497, 'Mar 24 01:33:43 209.97.136.164 a', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556498, 'Mar 24 01:33:43 209.97.136.164 y', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556499, 'Mar 24 01:33:43 209.97.136.164 e', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556500, 'Mar 24 01:33:43 209.97.136.164 r', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556501, 'Mar 24 01:33:43 209.97.136.164 /', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556502, 'Mar 24 01:33:43 209.97.136.164 9', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556503, 'Mar 24 01:33:43 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556504, 'Mar 24 01:33:43 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556505, 'Mar 24 01:33:43 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556506, 'Mar 24 01:33:43 209.97.136.164 9', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556507, 'Mar 24 01:33:43 209.97.136.164 8', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556508, 'Mar 24 01:33:43 209.97.136.164 ;', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556509, 'Mar 24 01:33:43 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556510, 'Mar 24 01:33:43 209.97.136.164 {', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556511, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556512, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556513, 'Mar 24 01:33:43 209.97.136.164 -', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556514, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556515, 'Mar 24 01:33:43 209.97.136.164 -', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556516, 'Mar 24 01:33:43 209.97.136.164 a', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556517, 'Mar 24 01:33:43 209.97.136.164 -', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556518, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556519, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556520, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556521, 'Mar 24 01:33:43 209.97.136.164 -', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556522, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556523, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556524, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556525, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556526, 'Mar 24 01:33:43 209.97.136.164 A', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556527, 'Mar 24 01:33:43 209.97.136.164 }', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556528, 'Mar 24 01:33:44 209.97.136.164 m_', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556529, 'Mar 24 01:33:44 209.97.136.164 Z', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556530, 'Mar 24 01:33:44 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556531, 'Mar 24 01:33:44 209.97.136.164 6,', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556532, 'Mar 24 01:33:44 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556533, 'Mar 24 01:33:44 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556534, 'Mar 24 01:33:44 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556535, 'Mar 24 01:33:44 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556536, 'Mar 24 01:33:44 209.97.136.164 :', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556537, 'Mar 24 01:33:44 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556538, 'Mar 24 01:33:44 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556539, 'Mar 24 01:33:46 209.97.136.164 (CONNECT_DATA=(COMMAND=version))', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556540, 'Mar 24 01:33:46 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556541, 'Mar 24 01:33:46 209.97.136.164 4', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556542, 'Mar 24 01:33:46 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556543, 'Mar 24 01:33:46 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556544, 'Mar 24 01:33:46 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556545, 'Mar 24 01:33:46 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556546, 'Mar 24 01:33:46 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556547, 'Mar 24 01:33:46 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556548, 'Mar 24 01:33:46 209.97.136.164 (', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556549, 'Mar 24 01:33:46 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556550, 'Mar 24 01:33:46 209.97.136.164 U', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556551, 'Mar 24 01:33:46 209.97.136.164 MSSQLServer', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556552, 'Mar 24 01:33:46 209.97.136.164 H', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556553, 'Mar 24 01:33:48 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556554, 'Mar 24 01:33:48 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556555, 'Mar 24 01:33:48 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556556, 'Mar 24 01:33:48 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556557, 'Mar 24 01:33:49 209.97.136.164 GIOP', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556558, 'Mar 24 01:33:49 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556559, 'Mar 24 01:33:49 209.97.136.164 $', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556560, 'Mar 24 01:33:49 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556561, 'Mar 24 01:33:49 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556562, 'Mar 24 01:33:49 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556563, 'Mar 24 01:33:49 209.97.136.164 abcdef', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556564, 'Mar 24 01:33:49 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556565, 'Mar 24 01:33:49 209.97.136.164 get', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556566, 'Mar 24 01:33:51 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556567, 'Mar 24 01:33:51 209.97.136.164 \Z+<M', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556568, 'Mar 24 01:33:51 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556569, 'Mar 24 01:33:51 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556570, 'Mar 24 01:33:51 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556571, 'Mar 24 01:33:51 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556572, 'Mar 24 01:33:51 209.97.136.164 none', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556573, 'Mar 24 01:33:51 209.97.136.164 beio', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556574, 'Mar 24 01:33:53 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556575, 'Mar 24 01:33:53 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556576, 'Mar 24 01:33:53 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556577, 'Mar 24 01:33:53 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556578, 'Mar 24 01:33:53 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556579, 'Mar 24 01:33:54 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556580, 'Mar 24 01:33:54 209.97.136.164 _@', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556581, 'Mar 24 01:33:54 209.97.136.164 7', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556582, 'Mar 24 01:33:54 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556583, 'Mar 24 01:33:54 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556584, 'Mar 24 01:33:54 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556585, 'Mar 24 01:33:54 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556586, 'Mar 24 01:33:54 209.97.136.164 InitiatorName=iqn.1991-05.com.microsoft:beio-iscsi-probe', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556587, 'Mar 24 01:33:54 209.97.136.164 SessionType=Discovery', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556588, 'Mar 24 01:33:54 209.97.136.164 AuthMethod=None', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556589, 'Mar 24 01:33:56 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556590, 'Mar 24 01:33:56 209.97.136.164', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556591, 'Mar 24 01:33:56 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556592, 'Mar 24 01:33:56 209.97.136.164 ANY-SCP', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556593, 'Mar 24 01:33:56 209.97.136.164 FINDSCU', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556594, 'Mar 24 01:33:56 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556595, 'Mar 24 01:33:56 209.97.136.164 1.2.840.10008.3.1.1.1', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556596, 'Mar 24 01:33:56 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556597, 'Mar 24 01:33:56 209.97.136.164 0', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556598, 'Mar 24 01:33:56 209.97.136.164 1.2.840.10008.1.1@', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556599, 'Mar 24 01:33:56 209.97.136.164 1.2.840.10008.1.2P', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556600, 'Mar 24 01:33:56 209.97.136.164 :Q', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556601, 'Mar 24 01:33:56 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556602, 'Mar 24 01:33:56 209.97.136.164 @', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556603, 'Mar 24 01:33:56 209.97.136.164 R', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556604, 'Mar 24 01:33:56 209.97.136.164 1.2.826.0.1.3680043.2.1396.999U', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556605, 'Mar 24 01:33:58 209.97.136.164 CharruaVista', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556606, 'Mar 24 01:33:58 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556607, 'Mar 24 01:33:58 209.97.136.164 ,\'', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556608, 'Mar 24 01:33:58 209.97.136.164 Cookie: mstshash=Administrator', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556609, 'Mar 24 01:33:58 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556610, 'Mar 24 01:33:58 209.97.136.164 ', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556611, 'Mar 24 01:34:01 209.97.136.164 I20100', '2022-04-06 01:35:05', 96, '209.97.136.164,01:00,2022-03-24.log'),
(556612, 'Line1: Apr  6 02:14:08 172.16.20.221 1649186048.642 3504363 172.16.10.53 TCP_TUNNEL/200 274981 CONNECT gateway.facebook.com:443 - HIER_DIRECT/157.240.235.16 -', '2022-04-06 02:29:54', 94, '172.16.20.221,02:00,2022-04-06.log'),
(556613, 'Line2: Apr  6 02:21:50 172.16.20.221 1649186509.526 3963267 172.16.10.53 TCP_TUNNEL/200 193342 CONNECT edge-chat.facebook.com:443 - HIER_DIRECT/157.240.235.15 -', '2022-04-06 02:29:54', 94, '172.16.20.221,02:00,2022-04-06.log'),
(556614, 'Line3: Apr  6 02:21:50 172.16.20.221 1649186509.532 3966726 172.16.10.53 TCP_TUNNEL/200 23083 CONNECT edge-chat.facebook.com:443 - HIER_DIRECT/157.240.235.15 -', '2022-04-06 02:29:54', 94, '172.16.20.221,02:00,2022-04-06.log'),
(556615, 'Line4: Apr  6 02:21:55 172.16.20.221 1649186515.136    248 172.16.10.53 TCP_MISS/200 634 GET http://www.msftconnecttest.com/connecttest.txt - HIER_DIRECT/13.107.4.52 text/plain', '2022-04-06 02:29:54', 94, '172.16.20.221,02:00,2022-04-06.log'),
(556616, 'Apr  6 02:14:08 172.16.20.221 1649186048.642 3504363 172.16.10.53 TCP_TUNNEL/200 274981 CONNECT gateway.facebook.com:443 - HIER_DIRECT/157.240.235.16 -', '2022-04-06 02:29:54', 94, '172.16.20.221,02:00,2022-04-06.log'),
(556617, 'Apr  6 02:21:50 172.16.20.221 1649186509.526 3963267 172.16.10.53 TCP_TUNNEL/200 193342 CONNECT edge-chat.facebook.com:443 - HIER_DIRECT/157.240.235.15 -', '2022-04-06 02:29:54', 94, '172.16.20.221,02:00,2022-04-06.log'),
(556618, 'Apr  6 02:21:50 172.16.20.221 1649186509.532 3966726 172.16.10.53 TCP_TUNNEL/200 23083 CONNECT edge-chat.facebook.com:443 - HIER_DIRECT/157.240.235.15 -', '2022-04-06 02:29:54', 94, '172.16.20.221,02:00,2022-04-06.log'),
(556619, 'Apr  6 02:21:55 172.16.20.221 1649186515.136    248 172.16.10.53 TCP_MISS/200 634 GET http://www.msftconnecttest.com/connecttest.txt - HIER_DIRECT/13.107.4.52 text/plain', '2022-04-06 02:29:54', 94, '172.16.20.221,02:00,2022-04-06.log'),
(556620, 'Apr  6 03:22:13 172.16.20.221 1649190133.419    166 172.16.10.53 TCP_MISS/503 4024 GET http://ipv6.msftconnecttest.com/connecttest.txt - HIER_DIRECT/2a01:111:2003::52 text/html', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556621, 'Apr  6 03:22:13 172.16.20.221 1649190133.497    244 172.16.10.53 TCP_MISS/200 634 GET http://www.msftconnecttest.com/connecttest.txt - HIER_DIRECT/13.107.4.52 text/plain', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556622, 'Apr  6 03:22:18 172.16.20.221 1649190138.659    919 172.16.10.53 TCP_TUNNEL/200 2106113 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556623, 'Apr  6 03:22:18 172.16.20.221 1649190138.659   7441 172.16.10.53 TCP_TUNNEL/200 51108 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.235.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556624, 'Apr  6 03:22:18 172.16.20.221 1649190138.659   2115 172.16.10.53 TCP_TUNNEL/200 77394 CONNECT i.ytimg.com:443 - HIER_DIRECT/142.251.10.119 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556625, 'Apr  6 03:22:18 172.16.20.221 1649190138.660   6274 172.16.10.53 TCP_TUNNEL/200 6198 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556626, 'Apr  6 03:22:18 172.16.20.221 1649190138.660   9047 172.16.10.53 TCP_TUNNEL/200 2000 CONNECT www.youtube.com:443 - HIER_DIRECT/142.250.4.136 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556627, 'Apr  6 03:22:33 172.16.20.221 1649190153.668   9893 172.16.10.53 TCP_TUNNEL/200 4086896 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556628, 'Apr  6 03:22:33 172.16.20.221 1649190153.669  15730 172.16.10.53 TCP_TUNNEL/200 7537 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/13.89.178.26 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556629, 'Apr  6 03:22:33 172.16.20.221 1649190153.669   1212 172.16.10.53 TCP_TUNNEL/200 25513 CONNECT video.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.82 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556630, 'Apr  6 03:22:33 172.16.20.221 1649190153.670  23897 172.16.10.53 TCP_TUNNEL/200 133409 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.13.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556631, 'Apr  6 03:22:33 172.16.20.221 1649190153.670   1175 172.16.10.53 TCP_TUNNEL/200 6432 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556632, 'Apr  6 03:22:33 172.16.20.221 1649190153.671   3259 172.16.10.53 TCP_TUNNEL/200 84634 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.235.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556633, 'Apr  6 03:22:33 172.16.20.221 1649190153.671   1980 172.16.10.53 TCP_TUNNEL/200 17609 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556634, 'Apr  6 03:22:33 172.16.20.221 1649190153.672   1997 172.16.10.53 TCP_TUNNEL/200 987695 CONNECT video.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556635, 'Apr  6 03:22:33 172.16.20.221 1649190153.672   6895 172.16.10.53 TCP_TUNNEL/200 59571 CONNECT i.ytimg.com:443 - HIER_DIRECT/142.251.10.119 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556636, 'Apr  6 03:22:33 172.16.20.221 1649190153.672   7720 172.16.10.53 TCP_TUNNEL/200 1999 CONNECT www.youtube.com:443 - HIER_DIRECT/142.250.4.136 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log');
INSERT INTO `log` (`log_id`, `msg`, `date`, `device_id`, `file_name`) VALUES
(556637, 'Apr  6 03:22:43 172.16.20.221 1649190163.683   6983 172.16.10.53 TCP_TUNNEL/200 3649694 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556638, 'Apr  6 03:22:43 172.16.20.221 1649190163.684   6962 172.16.10.53 TCP_TUNNEL/200 3799611 CONNECT video.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556639, 'Apr  6 03:22:43 172.16.20.221 1649190163.685   2675 172.16.10.53 TCP_TUNNEL/200 1906 CONNECT www.youtube.com:443 - HIER_DIRECT/142.250.4.136 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556640, 'Apr  6 03:22:43 172.16.20.221 1649190163.686   7600 172.16.10.53 TCP_TUNNEL/200 140306 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.13.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556641, 'Apr  6 03:22:43 172.16.20.221 1649190163.686   6223 172.16.10.53 TCP_TUNNEL/200 98717 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556642, 'Apr  6 03:22:43 172.16.20.221 1649190163.687   2292 172.16.10.53 TCP_TUNNEL/200 21773 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.235.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556643, 'Apr  6 03:22:43 172.16.20.221 1649190163.688   6225 172.16.10.53 TCP_TUNNEL/200 72981 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556644, 'Apr  6 03:22:43 172.16.20.221 1649190163.688   6979 172.16.10.53 TCP_TUNNEL/200 32960 CONNECT i.ytimg.com:443 - HIER_DIRECT/142.251.10.119 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556645, 'Apr  6 03:22:43 172.16.20.221 1649190163.689   6335 172.16.10.53 TCP_TUNNEL/200 6509707 CONNECT video.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.82 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556646, 'Apr  6 03:22:49 172.16.20.221 1649190169.262    328 172.16.10.53 TCP_TUNNEL/200 8504 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/20.212.97.243 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556647, 'Apr  6 03:22:51 172.16.20.221 1649190171.535    187 172.16.10.53 TCP_TUNNEL/200 8900 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/20.212.97.243 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556648, 'Apr  6 03:22:52 172.16.20.221 1649190172.604    576 172.16.10.53 TCP_TUNNEL/200 2101969 CONNECT rr1---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.44 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556649, 'Apr  6 03:22:53 172.16.20.221 1649190173.703   4775 172.16.10.53 TCP_TUNNEL/200 859 CONNECT fonts.googleapis.com:443 - HIER_DIRECT/142.251.10.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556650, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   4776 172.16.10.53 TCP_TUNNEL/200 859 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556651, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   2789 172.16.10.53 TCP_TUNNEL/200 7944 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/13.89.178.26 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556652, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   9291 172.16.10.53 TCP_TUNNEL/200 281991 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556653, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   9298 172.16.10.53 TCP_TUNNEL/200 451836 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556654, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   1827 172.16.10.53 TCP_TUNNEL/200 49278 CONNECT jnn-pa.googleapis.com:443 - HIER_DIRECT/172.217.194.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556655, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   4413 172.16.10.53 TCP_TUNNEL/200 17163 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556656, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   2578 172.16.10.53 TCP_TUNNEL/200 21147 CONNECT yt3.ggpht.com:443 - HIER_DIRECT/172.217.194.132 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556657, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   4554 172.16.10.53 TCP_TUNNEL/200 459468 CONNECT i.ytimg.com:443 - HIER_DIRECT/142.251.10.119 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556658, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   9809 172.16.10.53 TCP_TUNNEL/200 75591 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.13.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556659, 'Apr  6 03:22:53 172.16.20.221 1649190173.704   9779 172.16.10.53 TCP_TUNNEL/200 2104774 CONNECT video.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556660, 'Apr  6 03:22:53 172.16.20.221 1649190173.706   9781 172.16.10.53 TCP_TUNNEL/200 5526402 CONNECT video.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.82 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556661, 'Apr  6 03:23:10 172.16.20.221 1649190190.911  16643 172.16.10.53 TCP_TUNNEL/200 11424 CONNECT assets.nflxext.com:443 - HIER_DIRECT/45.57.90.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556662, 'Apr  6 03:23:11 172.16.20.221 1649190190.954  15014 172.16.10.53 TCP_TUNNEL/200 132804 CONNECT assets.nflxext.com:443 - HIER_DIRECT/45.57.90.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556663, 'Apr  6 03:23:13 172.16.20.221 1649190193.108  17361 172.16.10.53 TCP_TUNNEL/200 766110 CONNECT assets.nflxext.com:443 - HIER_DIRECT/45.57.90.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556664, 'Apr  6 03:23:13 172.16.20.221 1649190193.109  17413 172.16.10.53 TCP_TUNNEL/200 4322 CONNECT codex.nflxext.com:443 - HIER_DIRECT/45.57.90.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556665, 'Apr  6 03:23:13 172.16.20.221 1649190193.376  19107 172.16.10.53 TCP_TUNNEL/200 181646 CONNECT codex.nflxext.com:443 - HIER_DIRECT/45.57.90.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556666, 'Apr  6 03:23:13 172.16.20.221 1649190193.378  17334 172.16.10.53 TCP_TUNNEL/200 1153903 CONNECT codex.nflxext.com:443 - HIER_DIRECT/45.57.90.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556667, 'Apr  6 03:23:17 172.16.20.221 1649190197.612  11837 172.16.10.53 TCP_TUNNEL/200 551254 CONNECT ipv4-c001-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.138 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556668, 'Apr  6 03:23:17 172.16.20.221 1649190197.902  12128 172.16.10.53 TCP_TUNNEL/200 323873 CONNECT ipv4-c001-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.138 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556669, 'Apr  6 03:23:20 172.16.20.221 1649190200.037  13564 172.16.10.53 TCP_TUNNEL/200 1469536 CONNECT ipv4-c001-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.138 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556670, 'Apr  6 03:23:20 172.16.20.221 1649190200.308   6916 172.16.10.53 TCP_TUNNEL/200 283 CONNECT ipv4-c002-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.142 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556671, 'Apr  6 03:23:23 172.16.20.221 1649190203.792  17319 172.16.10.53 TCP_TUNNEL/200 1671230 CONNECT ipv4-c001-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.138 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556672, 'Apr  6 03:23:23 172.16.20.221 1649190203.793  10681 172.16.10.53 TCP_TUNNEL/200 592217 CONNECT ipv4-c002-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.142 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556673, 'Apr  6 03:23:24 172.16.20.221 1649190204.126  11014 172.16.10.53 TCP_TUNNEL/200 1369690 CONNECT ipv4-c029-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.141 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556674, 'Apr  6 03:23:28 172.16.20.221 1649190208.642   4848 172.16.10.53 TCP_TUNNEL/200 282204 CONNECT ipv4-c001-dmk002-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556675, 'Apr  6 03:23:28 172.16.20.221 1649190208.685  15292 172.16.10.53 TCP_TUNNEL/200 1460519 CONNECT ipv4-c029-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.141 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556676, 'Apr  6 03:23:28 172.16.20.221 1649190208.687   8648 172.16.10.53 TCP_TUNNEL/200 1891773 CONNECT ipv4-c041-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.147 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556677, 'Apr  6 03:23:28 172.16.20.221 1649190208.806   8496 172.16.10.53 TCP_TUNNEL/200 1762539 CONNECT ipv4-c041-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.147 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556678, 'Apr  6 03:23:29 172.16.20.221 1649190209.553    865 172.16.10.53 TCP_TUNNEL/200 172956 CONNECT ipv4-c002-dmk002-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.150 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556679, 'Apr  6 03:23:30 172.16.20.221 1649190210.039    483 172.16.10.53 TCP_TUNNEL/200 373226 CONNECT ipv4-c036-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.145 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556680, 'Apr  6 03:23:30 172.16.20.221 1649190210.098     56 172.16.10.53 TCP_TUNNEL/200 283 CONNECT ipv4-c002-dmk002-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.150 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556681, 'Apr  6 03:23:30 172.16.20.221 1649190210.287   1477 172.16.10.53 TCP_TUNNEL/200 719107 CONNECT ipv4-c002-dmk002-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.150 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556682, 'Apr  6 03:23:32 172.16.20.221 1649190212.898   2608 172.16.10.53 TCP_TUNNEL/200 4724 CONNECT ipv4-c001-dmk002-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556683, 'Apr  6 03:23:32 172.16.20.221 1649190212.898   2789 172.16.10.53 TCP_TUNNEL/200 696173 CONNECT ipv4-c001-dmk002-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556684, 'Apr  6 03:23:33 172.16.20.221 1649190213.221   4531 172.16.10.53 TCP_TUNNEL/200 1932639 CONNECT ipv4-c002-dmk002-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.150 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556685, 'Apr  6 03:23:33 172.16.20.221 1649190213.221   4573 172.16.10.53 TCP_TUNNEL/200 490785 CONNECT ipv4-c002-dmk002-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.150 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556686, 'Apr  6 03:23:35 172.16.20.221 1649190215.546   2323 172.16.10.53 TCP_TUNNEL/200 283 CONNECT ipv4-c002-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.142 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556687, 'Apr  6 03:23:35 172.16.20.221 1649190215.553  25156 172.16.10.53 TCP_TUNNEL/200 1195848 CONNECT ipv4-c042-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.148 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556688, 'Apr  6 03:23:35 172.16.20.221 1649190215.764    205 172.16.10.53 TCP_TUNNEL/200 283 CONNECT ipv4-c002-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.142 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556689, 'Apr  6 03:23:37 172.16.20.221 1649190217.018   4118 172.16.10.53 TCP_TUNNEL/200 1048471 CONNECT ipv4-c002-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.142 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556690, 'Apr  6 03:23:37 172.16.20.221 1649190217.019  26160 172.16.10.53 TCP_TUNNEL/200 811591 CONNECT ipv4-c042-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.148 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556691, 'Apr  6 03:23:37 172.16.20.221 1649190217.301  19686 172.16.10.53 TCP_TUNNEL/200 229726 CONNECT ipv4-c052-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.153 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556692, 'Apr  6 03:23:39 172.16.20.221 1649190219.577   2556 172.16.10.53 TCP_TUNNEL/200 295434 CONNECT ipv4-c001-dmk001-cattelecom-isp.1.oca.nflxvideo.net:443 - HIER_DIRECT/122.155.238.138 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556693, 'Apr  6 03:23:39 172.16.20.221 1649190219.578   2556 172.16.10.53 TCP_TUNNEL/200 1483302 CONNECT ipv4-c026-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.138 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556694, 'Apr  6 03:23:39 172.16.20.221 1649190219.715  41357 172.16.10.53 TCP_TUNNEL/200 4147 CONNECT push.prod.netflix.com:443 - HIER_DIRECT/35.82.23.172 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556695, 'Apr  6 03:23:39 172.16.20.221 1649190219.805    224 172.16.10.53 TCP_TUNNEL/200 39 CONNECT outlook.live.com:443 - HIER_DIRECT/13.107.42.11 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556696, 'Apr  6 03:23:40 172.16.20.221 1649190220.059   2756 172.16.10.53 TCP_TUNNEL/200 1496275 CONNECT ipv4-c026-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.138 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556697, 'Apr  6 03:23:40 172.16.20.221 1649190220.060  22156 172.16.10.53 TCP_TUNNEL/200 686176 CONNECT ipv4-c052-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.153 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556698, 'Apr  6 03:23:40 172.16.20.221 1649190220.061  16268 172.16.10.53 TCP_TUNNEL/200 198452 CONNECT ipv4-c060-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.156 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556699, 'Apr  6 03:23:40 172.16.20.221 1649190220.062  15934 172.16.10.53 TCP_TUNNEL/200 526689 CONNECT ipv4-c060-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.156 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556700, 'Apr  6 03:23:40 172.16.20.221 1649190220.064   4516 172.16.10.53 TCP_TUNNEL/200 401183 CONNECT ipv4-c061-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.157 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556701, 'Apr  6 03:23:40 172.16.20.221 1649190220.143   4374 172.16.10.53 TCP_TUNNEL/200 1339467 CONNECT ipv4-c061-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.157 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556702, 'Apr  6 03:23:40 172.16.20.221 1649190220.645    586 172.16.10.53 TCP_TUNNEL/200 5076 CONNECT res.cdn.office.net:443 - HIER_DIRECT/122.155.247.137 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556703, 'Apr  6 03:23:40 172.16.20.221 1649190220.645    572 172.16.10.53 TCP_TUNNEL/200 5076 CONNECT res.cdn.office.net:443 - HIER_DIRECT/122.155.247.137 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556704, 'Apr  6 03:23:40 172.16.20.221 1649190220.646    573 172.16.10.53 TCP_TUNNEL/200 5076 CONNECT res.cdn.office.net:443 - HIER_DIRECT/122.155.247.137 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556705, 'Apr  6 03:23:40 172.16.20.221 1649190220.647    580 172.16.10.53 TCP_TUNNEL/200 5076 CONNECT res.cdn.office.net:443 - HIER_DIRECT/122.155.247.137 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556706, 'Apr  6 03:23:40 172.16.20.221 1649190220.647    575 172.16.10.53 TCP_TUNNEL/200 5076 CONNECT res.cdn.office.net:443 - HIER_DIRECT/122.155.247.137 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556707, 'Apr  6 03:23:47 172.16.20.221 1649190227.499   7351 172.16.10.53 TCP_TUNNEL/200 6705 CONNECT browser.pipe.aria.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556708, 'Apr  6 03:23:47 172.16.20.221 1649190227.501  14601 172.16.10.53 TCP_TUNNEL/200 827669 CONNECT ipv4-c062-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.158 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556709, 'Apr  6 03:23:47 172.16.20.221 1649190227.783  14558 172.16.10.53 TCP_TUNNEL/200 800106 CONNECT ipv4-c062-sin001-ix.1.oca.nflxvideo.net:443 - HIER_DIRECT/23.246.55.158 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556710, 'Apr  6 03:23:48 172.16.20.221 1649190228.550   1039 172.16.10.53 TCP_TUNNEL/200 39 CONNECT static2.sharepointonline.com:443 - HIER_DIRECT/23.35.251.102 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556711, 'Apr  6 03:23:48 172.16.20.221 1649190228.551   1039 172.16.10.53 TCP_TUNNEL/200 39 CONNECT static2.sharepointonline.com:443 - HIER_DIRECT/23.35.251.102 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556712, 'Apr  6 03:23:49 172.16.20.221 1649190229.302   1192 172.16.10.53 TCP_TUNNEL/200 4900 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556713, 'Apr  6 03:23:50 172.16.20.221 1649190230.109   2323 172.16.10.53 TCP_TUNNEL/200 6741 CONNECT browser.pipe.aria.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556714, 'Apr  6 03:23:50 172.16.20.221 1649190230.202    215 172.16.10.53 TCP_MISS/200 7757 GET http://download.windowsupdate.com/d/msdownload/update/others/2022/04/36555082_5014a4dbc494806713d33893aaf0b2e1114ae673.cab - HIER_DIRECT/122.155.236.25 application/vnd.ms-cab-compressed', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556715, 'Apr  6 03:23:50 172.16.20.221 1649190230.288     58 172.16.10.53 TCP_MISS/200 7792 GET http://download.windowsupdate.com/d/msdownload/update/others/2022/04/36555081_1296e0f0e1e817b84a1202678d05c6e02f593889.cab - HIER_DIRECT/122.155.236.25 application/vnd.ms-cab-compressed', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556716, 'Apr  6 03:23:50 172.16.20.221 1649190230.378     54 172.16.10.53 TCP_MISS/200 11177 GET http://download.windowsupdate.com/d/msdownload/update/others/2022/04/36555191_a70b6ef72a4431deaf87c82d4ecfa893ef4732bd.cab - HIER_DIRECT/122.155.236.25 application/vnd.ms-cab-compressed', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556717, 'Apr  6 03:23:50 172.16.20.221 1649190230.779   8733 172.16.10.53 TCP_TUNNEL/200 24126 CONNECT login.live.com:443 - HIER_DIRECT/40.126.35.129 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556718, 'Apr  6 03:23:51 172.16.20.221 1649190231.774  52533 172.16.10.53 TCP_TUNNEL/200 15550 CONNECT nrdp-sharded.prod.ftl.netflix.com:443 - HIER_DIRECT/45.57.40.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556719, 'Apr  6 03:23:51 172.16.20.221 1649190231.909  52670 172.16.10.53 TCP_TUNNEL/200 15534 CONNECT nrdp.prod.ftl.netflix.com:443 - HIER_DIRECT/45.57.41.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556720, 'Apr  6 03:23:52 172.16.20.221 1649190232.400   1082 172.16.10.53 TCP_TUNNEL/200 6741 CONNECT browser.pipe.aria.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556721, 'Apr  6 03:23:53 172.16.20.221 1649190233.002   1081 172.16.10.53 TCP_TUNNEL/200 6725 CONNECT browser.events.data.microsoft.com:443 - HIER_DIRECT/13.69.109.131 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556722, 'Apr  6 03:23:53 172.16.20.221 1649190233.004   1227 172.16.10.53 TCP_TUNNEL/200 10265 CONNECT login.live.com:443 - HIER_DIRECT/40.126.35.129 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556723, 'Apr  6 03:23:53 172.16.20.221 1649190233.008  56973 172.16.10.53 TCP_TUNNEL/200 2521588 CONNECT occ-0-2189-64.1.nflxso.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556724, 'Apr  6 03:23:53 172.16.20.221 1649190233.009  56975 172.16.10.53 TCP_TUNNEL/200 1661118 CONNECT occ-0-2189-64.1.nflxso.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556725, 'Apr  6 03:23:53 172.16.20.221 1649190233.590   3464 172.16.10.53 TCP_TUNNEL/200 62346 CONNECT attachment.outlook.live.net:443 - HIER_DIRECT/40.99.10.34 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556726, 'Apr  6 03:23:53 172.16.20.221 1649190233.604  57586 172.16.10.53 TCP_TUNNEL/200 1699878 CONNECT occ-0-2189-64.1.nflxso.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556727, 'Apr  6 03:23:53 172.16.20.221 1649190233.627  57593 172.16.10.53 TCP_TUNNEL/200 502097 CONNECT occ-0-2189-64.1.nflxso.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556728, 'Apr  6 03:23:53 172.16.20.221 1649190233.630  57594 172.16.10.53 TCP_TUNNEL/200 2610033 CONNECT occ-0-2189-64.1.nflxso.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556729, 'Apr  6 03:23:53 172.16.20.221 1649190233.729  52990 172.16.10.53 TCP_TUNNEL/200 2128643 CONNECT occ-0-2189-64.1.nflxso.net:443 - HIER_DIRECT/122.155.238.146 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556730, 'Apr  6 03:23:53 172.16.20.221 1649190233.821    806 172.16.10.53 TCP_TUNNEL/200 6725 CONNECT browser.events.data.microsoft.com:443 - HIER_DIRECT/13.69.109.131 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556731, 'Apr  6 03:23:54 172.16.20.221 1649190234.022   1008 172.16.10.53 TCP_TUNNEL/200 7129 CONNECT browser.events.data.microsoft.com:443 - HIER_DIRECT/13.69.109.131 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556732, 'Apr  6 03:23:54 172.16.20.221 1649190234.110   1081 172.16.10.53 TCP_TUNNEL/200 4900 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556733, 'Apr  6 03:23:55 172.16.20.221 1649190235.007   2002 172.16.10.53 TCP_TUNNEL/200 6741 CONNECT browser.pipe.aria.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556734, 'Apr  6 03:23:55 172.16.20.221 1649190235.640   2028 172.16.10.53 TCP_TUNNEL/200 4752 CONNECT charts-storage-notifications.tradingview.com:443 - HIER_DIRECT/44.238.190.65 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556735, 'Apr  6 03:23:55 172.16.20.221 1649190235.730   2088 172.16.10.53 TCP_TUNNEL/200 6069 CONNECT charts-storage.tradingview.com:443 - HIER_DIRECT/65.9.17.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556736, 'Apr  6 03:23:55 172.16.20.221 1649190235.839   2204 172.16.10.53 TCP_TUNNEL/200 4720 CONNECT pine-facade.tradingview.com:443 - HIER_DIRECT/52.40.106.167 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556737, 'Apr  6 03:23:56 172.16.20.221 1649190236.039  64006 172.16.10.53 TCP_TUNNEL/200 6659102 CONNECT rr1---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.44 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556738, 'Apr  6 03:23:56 172.16.20.221 1649190236.070  68554 172.16.10.53 TCP_TUNNEL/200 21102236 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556739, 'Apr  6 03:23:56 172.16.20.221 1649190236.071   2330 172.16.10.53 TCP_TUNNEL/200 10189 CONNECT s3.amazonaws.com:443 - HIER_DIRECT/52.217.234.216 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556740, 'Apr  6 03:23:56 172.16.20.221 1649190236.115  56874 172.16.10.53 TCP_TUNNEL/200 14839 CONNECT ios-sharded.prod.ftl.netflix.com:443 - HIER_DIRECT/45.57.41.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556741, 'Apr  6 03:23:56 172.16.20.221 1649190236.116   6988 172.16.10.53 TCP_TUNNEL/200 16524 CONNECT amcdn.msftauth.net:443 - HIER_DIRECT/13.107.213.59 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556742, 'Apr  6 03:23:56 172.16.20.221 1649190236.116  72242 172.16.10.53 TCP_TUNNEL/200 320166 CONNECT www.youtube.com:443 - HIER_DIRECT/142.250.4.136 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556743, 'Apr  6 03:23:56 172.16.20.221 1649190236.116  57364 172.16.10.53 TCP_TUNNEL/200 20116 CONNECT www.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556744, 'Apr  6 03:23:56 172.16.20.221 1649190236.116  61849 172.16.10.53 TCP_TUNNEL/200 82521 CONNECT cdn.cookielaw.org:443 - HIER_DIRECT/104.16.148.64 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556745, 'Apr  6 03:23:56 172.16.20.221 1649190236.134    293 172.16.10.53 TCP_TUNNEL/200 15504 CONNECT s3-symbol-logo.tradingview.com:443 - HIER_DIRECT/65.9.17.86 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556746, 'Apr  6 03:23:56 172.16.20.221 1649190236.134  56892 172.16.10.53 TCP_TUNNEL/200 14835 CONNECT ios.prod.ftl.netflix.com:443 - HIER_DIRECT/45.57.41.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556747, 'Apr  6 03:23:56 172.16.20.221 1649190236.134   9291 172.16.10.53 TCP_TUNNEL/200 7442 CONNECT static2.sharepointonline.com:443 - HIER_DIRECT/23.35.251.102 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556748, 'Apr  6 03:23:56 172.16.20.221 1649190236.134   6723 172.16.10.53 TCP_TUNNEL/200 9636 CONNECT storage.live.com:443 - HIER_DIRECT/13.105.74.49 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556749, 'Apr  6 03:23:56 172.16.20.221 1649190236.134  60211 172.16.10.53 TCP_TUNNEL/200 34472 CONNECT cdn.cookielaw.org:443 - HIER_DIRECT/104.16.148.64 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556750, 'Apr  6 03:23:56 172.16.20.221 1649190236.134   2081 172.16.10.53 TCP_TUNNEL/200 7965 CONNECT c.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556751, 'Apr  6 03:23:56 172.16.20.221 1649190236.136  61869 172.16.10.53 TCP_TUNNEL/200 1608441 CONNECT www.netflix.com:443 - HIER_DIRECT/44.242.60.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556752, 'Apr  6 03:23:56 172.16.20.221 1649190236.136  16550 172.16.10.53 TCP_TUNNEL/200 1224344 CONNECT outlook.live.com:443 - HIER_DIRECT/13.107.42.11 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556753, 'Apr  6 03:23:56 172.16.20.221 1649190236.136   1127 172.16.10.53 TCP_TUNNEL/200 6920 CONNECT www.tradingview.com:443 - HIER_DIRECT/65.9.182.125 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556754, 'Apr  6 03:23:56 172.16.20.221 1649190236.137   3734 172.16.10.53 TCP_TUNNEL/200 7054 CONNECT arc.msn.com:443 - HIER_DIRECT/52.184.81.210 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556755, 'Apr  6 03:23:56 172.16.20.221 1649190236.137  56897 172.16.10.53 TCP_TUNNEL/200 14842 CONNECT android.prod.ftl.netflix.com:443 - HIER_DIRECT/45.57.40.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556756, 'Apr  6 03:23:56 172.16.20.221 1649190236.137   9695 172.16.10.53 TCP_TUNNEL/200 71615 CONNECT res.cdn.office.net:443 - HIER_DIRECT/122.155.247.137 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556757, 'Apr  6 03:23:56 172.16.20.221 1649190236.137  10631 172.16.10.53 TCP_TUNNEL/200 17084 CONNECT res-1.cdn.office.net:443 - HIER_DIRECT/122.155.247.137 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556758, 'Apr  6 03:23:56 172.16.20.221 1649190236.137  45216 172.16.10.53 TCP_TUNNEL/200 3890 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556759, 'Apr  6 03:23:56 172.16.20.221 1649190236.137  10769 172.16.10.53 TCP_TUNNEL/200 274184 CONNECT outlook.live.com:443 - HIER_DIRECT/13.107.42.11 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556760, 'Apr  6 03:23:56 172.16.20.221 1649190236.137  57515 172.16.10.53 TCP_TUNNEL/200 6427 CONNECT edge.microsoft.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556761, 'Apr  6 03:23:56 172.16.20.221 1649190236.137  45173 172.16.10.53 TCP_TUNNEL/200 2217 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556762, 'Apr  6 03:23:56 172.16.20.221 1649190236.137   2541 172.16.10.53 TCP_TUNNEL/200 7249 CONNECT c.live.com:443 - HIER_DIRECT/52.231.207.240 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556763, 'Apr  6 03:23:56 172.16.20.221 1649190236.137  56897 172.16.10.53 TCP_TUNNEL/200 14852 CONNECT android-sharded.prod.ftl.netflix.com:443 - HIER_DIRECT/45.57.41.1 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556764, 'Apr  6 03:23:56 172.16.20.221 1649190236.137  16062 172.16.10.53 TCP_TUNNEL/200 2501029 CONNECT res.cdn.office.net:443 - HIER_DIRECT/122.155.247.137 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556765, 'Apr  6 03:23:56 172.16.20.221 1649190236.264    223 172.16.10.53 TCP_TUNNEL/200 39 CONNECT pine-facade.tradingview.com:443 - HIER_DIRECT/52.40.106.167 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556766, 'Apr  6 03:23:56 172.16.20.221 1649190236.365    225 172.16.10.53 TCP_TUNNEL/200 39 CONNECT pine-facade.tradingview.com:443 - HIER_DIRECT/52.40.106.167 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556767, 'Apr  6 03:23:57 172.16.20.221 1649190237.124   1089 172.16.10.53 TCP_TUNNEL/200 4900 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556768, 'Apr  6 03:23:57 172.16.20.221 1649190237.802    256 172.16.10.53 TCP_MISS/200 1218 GET http://emdl.ws.microsoft.com/emdl/c/doc/ph/prod5/msdownload/update/software/defu/2022/04/1024/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe.json - HIER_DIRECT/122.155.236.56 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556769, 'Apr  6 03:23:57 172.16.20.221 1649190237.815    221 172.16.10.53 TCP_TUNNEL/200 39 CONNECT scanner-alerts.tradingview.com:443 - HIER_DIRECT/209.58.134.239 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556770, 'Apr  6 03:23:57 172.16.20.221 1649190237.815    221 172.16.10.53 TCP_TUNNEL/200 39 CONNECT scanner-alerts.tradingview.com:443 - HIER_DIRECT/209.58.134.239 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556771, 'Apr  6 03:23:58 172.16.20.221 1649190238.027    159 172.16.10.53 TCP_MISS/206 488 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.59 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556772, 'Apr  6 03:23:58 172.16.20.221 1649190238.028    160 172.16.10.53 TCP_MISS/206 488 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.59 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556773, 'Apr  6 03:23:58 172.16.20.221 1649190238.436    397 172.16.10.53 TCP_MISS/206 1030106 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556774, 'Apr  6 03:23:58 172.16.20.221 1649190238.449    411 172.16.10.53 TCP_MISS/206 1049082 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556775, 'Apr  6 03:23:58 172.16.20.221 1649190238.804    260 172.16.10.53 TCP_MISS/206 1049080 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556776, 'Apr  6 03:23:58 172.16.20.221 1649190238.861    328 172.16.10.53 TCP_MISS/206 1049080 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556777, 'Apr  6 03:23:58 172.16.20.221 1649190238.861   1108 172.16.10.53 TCP_TUNNEL/200 4900 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556778, 'Apr  6 03:23:59 172.16.20.221 1649190239.144    251 172.16.10.53 TCP_MISS/206 1049080 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556779, 'Apr  6 03:23:59 172.16.20.221 1649190239.655    714 172.16.10.53 TCP_MISS/206 1049080 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556780, 'Apr  6 03:23:59 172.16.20.221 1649190239.915    693 172.16.10.53 TCP_MISS/206 1049081 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556781, 'Apr  6 03:24:00 172.16.20.221 1649190240.555    813 172.16.10.53 TCP_MISS/206 1049080 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556782, 'Apr  6 03:24:01 172.16.20.221 1649190241.230    467 172.16.10.53 TCP_MISS/206 1049074 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556783, 'Apr  6 03:24:01 172.16.20.221 1649190241.340    786 172.16.10.53 TCP_MISS/206 1049080 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556784, 'Apr  6 03:24:01 172.16.20.221 1649190241.548    207 172.16.10.53 TCP_MISS/206 1049080 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556785, 'Apr  6 03:24:01 172.16.20.221 1649190241.732    221 172.16.10.53 TCP_MISS/206 1049080 GET http://au.download.windowsupdate.com/c/msdownload/update/software/defu/2022/04/am_delta_cf7e04ca85876afd91157610e32163db191dd05e.exe - HIER_DIRECT/122.155.236.25 application/octet-stream', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556786, 'Apr  6 03:24:04 172.16.20.221 1649190244.889   1089 172.16.10.53 TCP_TUNNEL/200 4900 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556787, 'Apr  6 03:24:07 172.16.20.221 1649190247.124    109 172.16.10.53 TCP_TUNNEL/200 5260 CONNECT www.bitkub.com:443 - HIER_DIRECT/104.20.34.111 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556788, 'Apr  6 03:24:07 172.16.20.221 1649190247.334    316 172.16.10.53 TCP_TUNNEL/200 8513 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.82 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556789, 'Apr  6 03:24:08 172.16.20.221 1649190248.361  13834 172.16.10.53 TCP_TUNNEL/200 5473 CONNECT pushstream.tradingview.com:443 - HIER_DIRECT/192.108.254.120 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556790, 'Apr  6 03:24:08 172.16.20.221 1649190248.364  14736 172.16.10.53 TCP_TUNNEL/200 133439 CONNECT data.tradingview.com:443 - HIER_DIRECT/45.135.231.234 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556791, 'Apr  6 03:24:08 172.16.20.221 1649190248.371  13845 172.16.10.53 TCP_TUNNEL/200 4931 CONNECT pushstream.tradingview.com:443 - HIER_DIRECT/192.108.254.120 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556792, 'Apr  6 03:24:09 172.16.20.221 1649190249.293    246 172.16.10.53 TCP_TUNNEL/200 6338 CONNECT wsdesktop.bitkub.com:443 - HIER_DIRECT/65.9.17.116 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556793, 'Apr  6 03:24:09 172.16.20.221 1649190249.399   5772 172.16.10.53 TCP_TUNNEL/200 4948 CONNECT data.tradingview.com:443 - HIER_DIRECT/45.135.231.234 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556794, 'Apr  6 03:24:09 172.16.20.221 1649190249.410   2392 172.16.10.53 TCP_TUNNEL/200 859 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556795, 'Apr  6 03:24:09 172.16.20.221 1649190249.509    101 172.16.10.53 TCP_TUNNEL/200 2140 CONNECT api.bitkub.com:443 - HIER_DIRECT/104.18.19.36 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556796, 'Apr  6 03:24:09 172.16.20.221 1649190249.825   1291 172.16.10.53 TCP_TUNNEL/200 859 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556797, 'Apr  6 03:24:09 172.16.20.221 1649190249.828   9214 172.16.10.53 TCP_TUNNEL/200 5007 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556798, 'Apr  6 03:24:10 172.16.20.221 1649190250.252  10566 172.16.10.53 TCP_TUNNEL/200 4002129 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556799, 'Apr  6 03:24:10 172.16.20.221 1649190250.529   1836 172.16.10.53 TCP_TUNNEL/200 16717 CONNECT websdk.appsflyer.com:443 - HIER_DIRECT/122.155.236.11 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556800, 'Apr  6 03:24:10 172.16.20.221 1649190250.625  13273 172.16.10.53 TCP_TUNNEL/200 5062 CONNECT tv-dlive.tradingview.com:443 - HIER_DIRECT/44.230.75.48 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556801, 'Apr  6 03:24:10 172.16.20.221 1649190250.626  13528 172.16.10.53 TCP_TUNNEL/200 6508 CONNECT snowplow-pixel.tradingview.com:443 - HIER_DIRECT/65.9.17.68 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556802, 'Apr  6 03:24:10 172.16.20.221 1649190250.627   1219 172.16.10.53 TCP_TUNNEL/200 4584 CONNECT api.bitkub.com:443 - HIER_DIRECT/104.18.19.36 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556803, 'Apr  6 03:24:10 172.16.20.221 1649190250.627  13268 172.16.10.53 TCP_TUNNEL/200 6175 CONNECT edge.microsoft.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556804, 'Apr  6 03:24:10 172.16.20.221 1649190250.627  17613 172.16.10.53 TCP_TUNNEL/200 2952437 CONNECT www.tradingview.com:443 - HIER_DIRECT/65.9.182.125 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556805, 'Apr  6 03:24:10 172.16.20.221 1649190250.628  12658 172.16.10.53 TCP_TUNNEL/200 5765 CONNECT stats.g.doubleclick.net:443 - HIER_DIRECT/74.125.24.156 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556806, 'Apr  6 03:24:10 172.16.20.221 1649190250.628  15685 172.16.10.53 TCP_TUNNEL/200 5091 CONNECT charts-storage-notifications.tradingview.com:443 - HIER_DIRECT/44.238.190.65 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556807, 'Apr  6 03:24:10 172.16.20.221 1649190250.629   1222 172.16.10.53 TCP_TUNNEL/200 109814 CONNECT chat.bitkub.com:443 - HIER_DIRECT/104.18.18.36 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556808, 'Apr  6 03:24:10 172.16.20.221 1649190250.629  13348 172.16.10.53 TCP_TUNNEL/200 5724 CONNECT scanner-alerts.tradingview.com:443 - HIER_DIRECT/209.58.134.239 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556809, 'Apr  6 03:24:10 172.16.20.221 1649190250.630  13749 172.16.10.53 TCP_TUNNEL/200 11158 CONNECT charts-storage.tradingview.com:443 - HIER_DIRECT/65.9.17.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556810, 'Apr  6 03:24:10 172.16.20.221 1649190250.630   1222 172.16.10.53 TCP_TUNNEL/200 4435 CONNECT service.bitkub.com:443 - HIER_DIRECT/104.18.18.36 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556811, 'Apr  6 03:24:10 172.16.20.221 1649190250.632   3617 172.16.10.53 TCP_TUNNEL/200 4988 CONNECT fonts.googleapis.com:443 - HIER_DIRECT/142.251.10.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556812, 'Apr  6 03:24:10 172.16.20.221 1649190250.633  13468 172.16.10.53 TCP_TUNNEL/200 9112 CONNECT s3-symbol-logo.tradingview.com:443 - HIER_DIRECT/65.9.17.86 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556813, 'Apr  6 03:24:10 172.16.20.221 1649190250.633  12447 172.16.10.53 TCP_TUNNEL/200 5053 CONNECT tv-dlive.tradingview.com:443 - HIER_DIRECT/44.230.75.48 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556814, 'Apr  6 03:24:10 172.16.20.221 1649190250.633  14411 172.16.10.53 TCP_TUNNEL/200 118600 CONNECT www.googletagmanager.com:443 - HIER_DIRECT/74.125.24.97 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556815, 'Apr  6 03:24:10 172.16.20.221 1649190250.633  14558 172.16.10.53 TCP_TUNNEL/200 3693 CONNECT analytics.twitter.com:443 - HIER_DIRECT/104.244.42.67 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556816, 'Apr  6 03:24:10 172.16.20.221 1649190250.634    808 172.16.10.53 TCP_TUNNEL/200 4353 CONNECT cdn.bitkubnow.com:443 - HIER_DIRECT/104.18.4.28 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556817, 'Apr  6 03:24:10 172.16.20.221 1649190250.635   3011 172.16.10.53 TCP_TUNNEL/200 1255 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556818, 'Apr  6 03:24:10 172.16.20.221 1649190250.635  14894 172.16.10.53 TCP_TUNNEL/200 5805 CONNECT pine-facade.tradingview.com:443 - HIER_DIRECT/52.40.106.167 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556819, 'Apr  6 03:24:10 172.16.20.221 1649190250.635   3620 172.16.10.53 TCP_TUNNEL/200 5110 CONNECT cdn.jsdelivr.net:443 - HIER_DIRECT/104.16.86.20 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556820, 'Apr  6 03:24:10 172.16.20.221 1649190250.636    805 172.16.10.53 TCP_TUNNEL/200 7427 CONNECT cdn.taximail.com:443 - HIER_DIRECT/65.9.17.70 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556821, 'Apr  6 03:24:10 172.16.20.221 1649190250.636    800 172.16.10.53 TCP_TUNNEL/200 80101 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556822, 'Apr  6 03:24:10 172.16.20.221 1649190250.636  12391 172.16.10.53 TCP_TUNNEL/200 4740 CONNECT www.google.com:443 - HIER_DIRECT/142.251.12.104 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556823, 'Apr  6 03:24:10 172.16.20.221 1649190250.636  12390 172.16.10.53 TCP_TUNNEL/200 5588 CONNECT www.google.co.th:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556824, 'Apr  6 03:24:10 172.16.20.221 1649190250.636   9558 172.16.10.53 TCP_TUNNEL/200 2227 CONNECT www.youtube.com:443 - HIER_DIRECT/142.250.4.136 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556825, 'Apr  6 03:24:10 172.16.20.221 1649190250.638  14978 172.16.10.53 TCP_TUNNEL/200 6627 CONNECT alerts.tradingview.com:443 - HIER_DIRECT/89.43.104.102 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556826, 'Apr  6 03:24:10 172.16.20.221 1649190250.638  14562 172.16.10.53 TCP_TUNNEL/200 3545 CONNECT t.co:443 - HIER_DIRECT/104.244.42.133 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556827, 'Apr  6 03:24:10 172.16.20.221 1649190250.638   1217 172.16.10.53 TCP_TUNNEL/200 3615 CONNECT ms.bitkub.com:443 - HIER_DIRECT/104.18.19.36 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556828, 'Apr  6 03:24:10 172.16.20.221 1649190250.638  13408 172.16.10.53 TCP_TUNNEL/200 85532 CONNECT www.google-analytics.com:443 - HIER_DIRECT/74.125.24.101 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556829, 'Apr  6 03:24:13 172.16.20.221 1649190253.068   1083 172.16.10.53 TCP_TUNNEL/200 4900 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.85 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556830, 'Apr  6 03:24:15 172.16.20.221 1649190255.584   2085 172.16.10.53 TCP_TUNNEL/200 17377 CONNECT widget-mediator.zopim.com:443 - HIER_DIRECT/52.220.55.231 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556831, 'Apr  6 03:24:15 172.16.20.221 1649190255.716   6379 172.16.10.53 TCP_TUNNEL/200 220742 CONNECT wsdesktop.bitkub.com:443 - HIER_DIRECT/65.9.17.116 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556832, 'Apr  6 03:24:15 172.16.20.221 1649190255.716   6668 172.16.10.53 TCP_TUNNEL/200 3362 CONNECT ms.bitkub.com:443 - HIER_DIRECT/104.18.19.36 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556833, 'Apr  6 03:24:20 172.16.20.221 1649190259.155   8393 172.16.10.53 TCP_TUNNEL/200 5676 CONNECT af-event-logger.appsflyer.com:443 - HIER_DIRECT/52.211.47.21 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556834, 'Apr  6 03:24:20 172.16.20.221 1649190259.201   8670 172.16.10.53 TCP_TUNNEL/200 8169 CONNECT js.adsrvr.org:443 - HIER_DIRECT/65.9.178.118 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556835, 'Apr  6 03:24:20 172.16.20.221 1649190259.729   5781 172.16.10.53 TCP_TUNNEL/200 3384771 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556836, 'Apr  6 03:24:20 172.16.20.221 1649190259.776   4261 172.16.10.53 TCP_TUNNEL/200 15718 CONNECT ssl.gstatic.com:443 - HIER_DIRECT/74.125.24.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556837, 'Apr  6 03:24:20 172.16.20.221 1649190259.778   2057 172.16.10.53 TCP_TUNNEL/200 23606 CONNECT aa.google.com:443 - HIER_DIRECT/142.250.4.100 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556838, 'Apr  6 03:24:20 172.16.20.221 1649190259.778   8513 172.16.10.53 TCP_TUNNEL/200 57054 CONNECT www.google.com:443 - HIER_DIRECT/142.251.12.104 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556839, 'Apr  6 03:24:20 172.16.20.221 1649190259.779   8320 172.16.10.53 TCP_TUNNEL/200 252978 CONNECT www.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556840, 'Apr  6 03:24:20 172.16.20.221 1649190259.779   9523 172.16.10.53 TCP_TUNNEL/200 76314 CONNECT analytics.tiktok.com:443 - HIER_DIRECT/122.155.237.209 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556841, 'Apr  6 03:24:20 172.16.20.221 1649190259.779   2820 172.16.10.53 TCP_TUNNEL/200 2998 CONNECT lh3.google.com:443 - HIER_DIRECT/142.251.10.101 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556842, 'Apr  6 03:24:20 172.16.20.221 1649190259.780   2260 172.16.10.53 TCP_TUNNEL/200 8072 CONNECT clients2.google.com:443 - HIER_DIRECT/142.251.10.138 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log');
INSERT INTO `log` (`log_id`, `msg`, `date`, `device_id`, `file_name`) VALUES
(556843, 'Apr  6 03:24:20 172.16.20.221 1649190259.780   7644 172.16.10.53 TCP_TUNNEL/200 5232 CONNECT insight.adsrvr.org:443 - HIER_DIRECT/52.223.40.198 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556844, 'Apr  6 03:24:20 172.16.20.221 1649190259.780   7019 172.16.10.53 TCP_TUNNEL/200 5436 CONNECT bitkub.zendesk.com:443 - HIER_DIRECT/104.16.53.111 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556845, 'Apr  6 03:24:20 172.16.20.221 1649190259.781  12765 172.16.10.53 TCP_TUNNEL/200 2559983 CONNECT www.bitkub.com:443 - HIER_DIRECT/104.20.34.111 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556846, 'Apr  6 03:24:20 172.16.20.221 1649190259.781   8005 172.16.10.53 TCP_TUNNEL/200 4209 CONNECT ekr.zdassets.com:443 - HIER_DIRECT/104.18.70.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556847, 'Apr  6 03:24:20 172.16.20.221 1649190259.781   9109 172.16.10.53 TCP_TUNNEL/200 1711 CONNECT www.google-analytics.com:443 - HIER_DIRECT/74.125.24.101 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556848, 'Apr  6 03:24:20 172.16.20.221 1649190259.781   1372 172.16.10.53 TCP_TUNNEL/200 1561 CONNECT play.google.com:443 - HIER_DIRECT/74.125.68.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556849, 'Apr  6 03:24:20 172.16.20.221 1649190259.781   8631 172.16.10.53 TCP_TUNNEL/200 516457 CONNECT static.zdassets.com:443 - HIER_DIRECT/104.18.70.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556850, 'Apr  6 03:24:20 172.16.20.221 1649190259.781   8441 172.16.10.53 TCP_TUNNEL/200 1708 CONNECT www.google.co.th:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556851, 'Apr  6 03:24:20 172.16.20.221 1649190259.782   9019 172.16.10.53 TCP_TUNNEL/200 3224 CONNECT assets.zendesk.com:443 - HIER_DIRECT/104.18.70.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556852, 'Apr  6 03:24:20 172.16.20.221 1649190259.782   8655 172.16.10.53 TCP_TUNNEL/200 1579 CONNECT stats.g.doubleclick.net:443 - HIER_DIRECT/74.125.24.156 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556853, 'Apr  6 03:24:20 172.16.20.221 1649190259.783   7461 172.16.10.53 TCP_TUNNEL/200 4741 CONNECT match.adsrvr.org:443 - HIER_DIRECT/52.223.40.198 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556854, 'Apr  6 03:24:20 172.16.20.221 1649190259.783    624 172.16.10.53 TCP_TUNNEL/200 5755 CONNECT ssl.gstatic.com:443 - HIER_DIRECT/74.125.24.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556855, 'Apr  6 03:24:20 172.16.20.221 1649190259.784   2111 172.16.10.53 TCP_TUNNEL/200 1907 CONNECT lh3.googleusercontent.com:443 - HIER_DIRECT/142.250.4.132 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556856, 'Apr  6 03:24:20 172.16.20.221 1649190259.784   2022 172.16.10.53 TCP_TUNNEL/200 195182 CONNECT apis.google.com:443 - HIER_DIRECT/74.125.24.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556857, 'Apr  6 03:24:20 172.16.20.221 1649190259.785   9141 172.16.10.53 TCP_TUNNEL/200 5460 CONNECT cdn.taximail.com:443 - HIER_DIRECT/65.9.17.70 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556858, 'Apr  6 03:24:20 172.16.20.221 1649190259.785   9006 172.16.10.53 TCP_TUNNEL/200 152691 CONNECT www.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556859, 'Apr  6 03:24:20 172.16.20.221 1649190259.785   9109 172.16.10.53 TCP_TUNNEL/200 40214 CONNECT www.googletagmanager.com:443 - HIER_DIRECT/74.125.24.97 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556860, 'Apr  6 03:24:20 172.16.20.221 1649190259.785   2242 172.16.10.53 TCP_TUNNEL/200 3321 CONNECT fonts.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556861, 'Apr  6 03:24:20 172.16.20.221 1649190259.785   8996 172.16.10.53 TCP_TUNNEL/200 7121 CONNECT edge.microsoft.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556862, 'Apr  6 03:24:20 172.16.20.221 1649190259.785   9147 172.16.10.53 TCP_TUNNEL/200 44193 CONNECT tradingview.bitkub.com:443 - HIER_DIRECT/104.18.19.36 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556863, 'Apr  6 03:24:49 172.16.20.221 1649190288.588 128576 172.16.10.53 TCP_TUNNEL/200 8814 CONNECT activity.windows.com:443 - HIER_DIRECT/20.44.229.112 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556864, 'Apr  6 03:25:07 172.16.20.221 1649190306.844  71963 172.16.10.53 TCP_TUNNEL/200 7259 CONNECT kv601.prod.do.dsp.mp.microsoft.com:443 - HIER_DIRECT/118.214.21.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556865, 'Apr  6 03:25:07 172.16.20.221 1649190306.845  70507 172.16.10.53 TCP_TUNNEL/200 6854 CONNECT cp601.prod.do.dsp.mp.microsoft.com:443 - HIER_DIRECT/118.214.21.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556866, 'Apr  6 03:25:07 172.16.20.221 1649190306.845  72307 172.16.10.53 TCP_TUNNEL/200 6616 CONNECT geover.prod.do.dsp.mp.microsoft.com:443 - HIER_DIRECT/96.6.78.145 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556867, 'Apr  6 03:25:31 172.16.20.221 1649190330.446  67170 172.16.10.53 TCP_TUNNEL/200 930 CONNECT scontent.xx.fbcdn.net:443 - HIER_DIRECT/157.240.13.19 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556868, 'Apr  6 03:25:32 172.16.20.221 1649190331.730  65778 172.16.10.53 TCP_TUNNEL/200 1412 CONNECT scontent.xx.fbcdn.net:443 - HIER_DIRECT/157.240.13.19 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556869, 'Apr  6 03:25:33 172.16.20.221 1649190332.168  69774 172.16.10.53 TCP_TUNNEL/200 774895 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.13.19 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556870, 'Apr  6 03:25:49 172.16.20.221 1649190348.912 172892 172.16.10.53 TCP_TUNNEL/200 14863 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/13.89.178.26 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556871, 'Apr  6 03:25:49 172.16.20.221 1649190348.913  89701 172.16.10.53 TCP_TUNNEL/200 11501 CONNECT signaler-pa.clients6.google.com:443 - HIER_DIRECT/172.217.194.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556872, 'Apr  6 03:25:49 172.16.20.221 1649190348.913  88909 172.16.10.53 TCP_TUNNEL/200 11730 CONNECT peoplestack-pa.clients6.google.com:443 - HIER_DIRECT/142.251.12.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556873, 'Apr  6 03:25:49 172.16.20.221 1649190348.914  93571 172.16.10.53 TCP_TUNNEL/200 888658 CONNECT mail.google.com:443 - HIER_DIRECT/142.251.12.17 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556874, 'Apr  6 03:25:49 172.16.20.221 1649190348.914  88072 172.16.10.53 TCP_TUNNEL/200 2399 CONNECT ssl.gstatic.com:443 - HIER_DIRECT/74.125.24.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556875, 'Apr  6 03:25:49 172.16.20.221 1649190348.914  88151 172.16.10.53 TCP_TUNNEL/200 34856 CONNECT www.gstatic.com:443 - HIER_DIRECT/172.217.194.94 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556876, 'Apr  6 03:25:49 172.16.20.221 1649190348.914  86989 172.16.10.53 TCP_TUNNEL/200 1758 CONNECT play.google.com:443 - HIER_DIRECT/74.125.68.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556877, 'Apr  6 03:25:49 172.16.20.221 1649190348.914  85304 172.16.10.53 TCP_TUNNEL/200 53598 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556878, 'Apr  6 03:25:49 172.16.20.221 1649190348.914  88301 172.16.10.53 TCP_TUNNEL/200 46917 CONNECT apis.google.com:443 - HIER_DIRECT/74.125.24.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556879, 'Apr  6 03:25:49 172.16.20.221 1649190348.914  90279 172.16.10.53 TCP_TUNNEL/200 12032 CONNECT play.google.com:443 - HIER_DIRECT/74.125.68.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556880, 'Apr  6 03:25:49 172.16.20.221 1649190348.915  88993 172.16.10.53 TCP_TUNNEL/200 11714 CONNECT signaler-pa.clients6.google.com:443 - HIER_DIRECT/172.217.194.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556881, 'Apr  6 03:25:49 172.16.20.221 1649190348.916  87900 172.16.10.53 TCP_TUNNEL/200 11643 CONNECT addons-pa.clients6.google.com:443 - HIER_DIRECT/74.125.24.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556882, 'Apr  6 03:25:49 172.16.20.221 1649190348.917  91929 172.16.10.53 TCP_TUNNEL/200 1471534 CONNECT chat.google.com:443 - HIER_DIRECT/142.251.10.113 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556883, 'Apr  6 03:25:49 172.16.20.221 1649190348.917  88031 172.16.10.53 TCP_TUNNEL/200 6212 CONNECT edge.microsoft.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556884, 'Apr  6 03:25:49 172.16.20.221 1649190348.917  88281 172.16.10.53 TCP_TUNNEL/200 11070 CONNECT addons-pa.clients6.google.com:443 - HIER_DIRECT/74.125.24.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556885, 'Apr  6 03:25:49 172.16.20.221 1649190348.921  88095 172.16.10.53 TCP_TUNNEL/200 289200 CONNECT contacts.google.com:443 - HIER_DIRECT/74.125.24.100 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556886, 'Apr  6 03:25:49 172.16.20.221 1649190348.921  86779 172.16.10.53 TCP_TUNNEL/200 6014 CONNECT lh3.googleusercontent.com:443 - HIER_DIRECT/142.250.4.132 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556887, 'Apr  6 03:25:49 172.16.20.221 1649190348.921  89110 172.16.10.53 TCP_TUNNEL/200 11086 CONNECT peoplestack-pa.clients6.google.com:443 - HIER_DIRECT/142.251.12.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556888, 'Apr  6 03:25:49 172.16.20.221 1649190348.921   6514 172.16.10.53 TCP_TUNNEL/200 2162 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556889, 'Apr  6 03:25:49 172.16.20.221 1649190348.921  88943 172.16.10.53 TCP_TUNNEL/200 11844 CONNECT peoplestackwebexperiments-pa.clients6.google.com:443 - HIER_DIRECT/74.125.24.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556890, 'Apr  6 03:25:49 172.16.20.221 1649190348.921  89135 172.16.10.53 TCP_TUNNEL/200 11086 CONNECT peoplestackwebexperiments-pa.clients6.google.com:443 - HIER_DIRECT/74.125.24.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556891, 'Apr  6 03:25:49 172.16.20.221 1649190348.923  87917 172.16.10.53 TCP_TUNNEL/200 4114 CONNECT www.youtube.com:443 - HIER_DIRECT/142.250.4.136 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556892, 'Apr  6 03:25:49 172.16.20.221 1649190348.923  88993 172.16.10.53 TCP_TUNNEL/200 46861 CONNECT people-pa.clients6.google.com:443 - HIER_DIRECT/172.217.194.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556893, 'Apr  6 03:25:49 172.16.20.221 1649190348.923  86937 172.16.10.53 TCP_TUNNEL/200 3008 CONNECT lh3.google.com:443 - HIER_DIRECT/142.251.10.101 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556894, 'Apr  6 03:25:49 172.16.20.221 1649190348.923  87198 172.16.10.53 TCP_TUNNEL/200 16690 CONNECT ogs.google.com:443 - HIER_DIRECT/142.250.4.100 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556895, 'Apr  6 03:25:49 172.16.20.221 1649190348.923  86528 172.16.10.53 TCP_TUNNEL/200 677428 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556896, 'Apr  6 03:25:49 172.16.20.221 1649190348.923  20907 172.16.10.53 TCP_TUNNEL/200 72631 CONNECT i.ytimg.com:443 - HIER_DIRECT/142.251.10.119 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556897, 'Apr  6 03:25:49 172.16.20.221 1649190348.923  89185 172.16.10.53 TCP_TUNNEL/200 11105 CONNECT people-pa.clients6.google.com:443 - HIER_DIRECT/172.217.194.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556898, 'Apr  6 03:25:52 172.16.20.221 1649190351.382 129757 172.16.10.53 TCP_TUNNEL/200 2022 CONNECT discordapp.com:443 - HIER_DIRECT/162.159.130.233 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556899, 'Apr  6 03:25:59 172.16.20.221 1649190358.942  91537 172.16.10.53 TCP_TUNNEL/200 35771580 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556900, 'Apr  6 03:25:59 172.16.20.221 1649190358.943   6776 172.16.10.53 TCP_TUNNEL/200 1999 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556901, 'Apr  6 03:26:14 172.16.20.221 1649190373.361 111617 172.16.10.53 TCP_TUNNEL/200 3139 CONNECT array605.prod.do.dsp.mp.microsoft.com:443 - HIER_DIRECT/51.104.164.114 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556902, 'Apr  6 03:26:26 172.16.20.221 1649190385.563 123876 172.16.10.53 TCP_TUNNEL/200 3140 CONNECT array614.prod.do.dsp.mp.microsoft.com:443 - HIER_DIRECT/20.54.24.231 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556903, 'Apr  6 03:26:27 172.16.20.221 1649190386.529  35266 172.16.10.53 TCP_TUNNEL/200 29085476 CONNECT intellicodeprodstorage.blob.core.windows.net:443 - HIER_DIRECT/20.150.35.132 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556904, 'Apr  6 03:26:29 172.16.20.221 1649190388.988  25212 172.16.10.53 TCP_TUNNEL/200 9559685 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556905, 'Apr  6 03:26:29 172.16.20.221 1649190388.988   3130 172.16.10.53 TCP_TUNNEL/200 3525 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556906, 'Apr  6 03:26:29 172.16.20.221 1649190388.989  27923 172.16.10.53 TCP_TUNNEL/200 1906 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556907, 'Apr  6 03:26:39 172.16.20.221 1649190399.016   8622 172.16.10.53 TCP_TUNNEL/200 4711063 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556908, 'Apr  6 03:26:56 172.16.20.221 1649190415.520   2706 172.16.10.53 TCP_TUNNEL/200 5719 CONNECT v10.events.data.microsoft.com:443 - HIER_DIRECT/20.42.65.88 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556909, 'Apr  6 03:26:59 172.16.20.221 1649190419.036  16341 172.16.10.53 TCP_TUNNEL/200 5979377 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556910, 'Apr  6 03:26:59 172.16.20.221 1649190419.036   2272 172.16.10.53 TCP_TUNNEL/200 3572 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556911, 'Apr  6 03:26:59 172.16.20.221 1649190419.037   2298 172.16.10.53 TCP_TUNNEL/200 4717 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556912, 'Apr  6 03:26:59 172.16.20.221 1649190419.037   2260 172.16.10.53 TCP_TUNNEL/200 5043 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556913, 'Apr  6 03:26:59 172.16.20.221 1649190419.037  18025 172.16.10.53 TCP_TUNNEL/200 1884 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556914, 'Apr  6 03:26:59 172.16.20.221 1649190419.039   2210 172.16.10.53 TCP_TUNNEL/200 6154 CONNECT edge.microsoft.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556915, 'Apr  6 03:27:14 172.16.20.221 1649190434.066  14791 172.16.10.53 TCP_TUNNEL/200 6810549 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556916, 'Apr  6 03:27:24 172.16.20.221 1649190444.084   8470 172.16.10.53 TCP_TUNNEL/200 4207219 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556917, 'Apr  6 03:27:24 172.16.20.221 1649190444.084   3063 172.16.10.53 TCP_TUNNEL/200 1906 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556918, 'Apr  6 03:27:34 172.16.20.221 1649190454.098   7196 172.16.10.53 TCP_TUNNEL/200 3981567 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556919, 'Apr  6 03:27:34 172.16.20.221 1649190454.098   8235 172.16.10.53 TCP_TUNNEL/200 3525 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556920, 'Apr  6 03:27:54 172.16.20.221 1649190474.146   9641 172.16.10.53 TCP_TUNNEL/200 4735320 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556921, 'Apr  6 03:27:54 172.16.20.221 1649190474.147   8424 172.16.10.53 TCP_TUNNEL/200 2273 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556922, 'Apr  6 03:28:04 172.16.20.221 1649190484.159   4834 172.16.10.53 TCP_TUNNEL/200 2106113 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556923, 'Apr  6 03:28:04 172.16.20.221 1649190484.159   3116 172.16.10.53 TCP_TUNNEL/200 2226 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556924, 'Apr  6 03:28:38 172.16.20.221 1649190517.922 139155 172.16.10.53 TCP_TUNNEL/200 15443 CONNECT activity.windows.com:443 - HIER_DIRECT/20.44.229.112 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556925, 'Apr  6 03:28:59 172.16.20.221 1649190539.268  54931 172.16.10.53 TCP_TUNNEL/200 20351704 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556926, 'Apr  6 03:28:59 172.16.20.221 1649190539.269  29906 172.16.10.53 TCP_TUNNEL/200 9069 CONNECT static.xx.fbcdn.net:443 - HIER_DIRECT/157.240.7.26 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556927, 'Apr  6 03:28:59 172.16.20.221 1649190539.269  33408 172.16.10.53 TCP_TUNNEL/200 28535 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556928, 'Apr  6 03:28:59 172.16.20.221 1649190539.269  46158 172.16.10.53 TCP_TUNNEL/200 3962 CONNECT scontent.fbkk14-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.236.81 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556929, 'Apr  6 03:28:59 172.16.20.221 1649190539.269  28251 172.16.10.53 TCP_TUNNEL/200 1945 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556930, 'Apr  6 03:29:02 172.16.20.221 1649190541.682  29124 172.16.10.53 TCP_TUNNEL/200 3810 CONNECT discordapp.com:443 - HIER_DIRECT/162.159.130.233 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556931, 'Apr  6 03:29:03 172.16.20.221 1649190542.673 109992 172.16.10.53 TCP_TUNNEL/200 6796 CONNECT self.events.data.microsoft.com:443 - HIER_DIRECT/20.189.173.10 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556932, 'Apr  6 03:29:06 172.16.20.221 1649190545.936   1354 172.16.10.53 TCP_TUNNEL/200 4018 CONNECT stas.outbrain.com:443 - HIER_DIRECT/66.225.223.191 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556933, 'Apr  6 03:29:06 172.16.20.221 1649190545.964   1373 172.16.10.53 TCP_TUNNEL/200 4074 CONNECT stas.outbrain.com:443 - HIER_DIRECT/66.225.223.191 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556934, 'Apr  6 03:29:07 172.16.20.221 1649190546.583   2469 172.16.10.53 TCP_TUNNEL/200 8470 CONNECT browser.events.data.msn.com:443 - HIER_DIRECT/52.168.117.170 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556935, 'Apr  6 03:29:09 172.16.20.221 1649190549.286   4696 172.16.10.53 TCP_TUNNEL/200 3781 CONNECT b1-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.63 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556936, 'Apr  6 03:29:10 172.16.20.221 1649190549.287   4789 172.16.10.53 TCP_TUNNEL/200 3781 CONNECT b1-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.63 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556937, 'Apr  6 03:29:10 172.16.20.221 1649190549.287   2937 172.16.10.53 TCP_TUNNEL/200 5161 CONNECT b1sync.zemanta.com:443 - HIER_DIRECT/64.74.236.191 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556938, 'Apr  6 03:29:10 172.16.20.221 1649190549.287   4698 172.16.10.53 TCP_TUNNEL/200 3833 CONNECT b1t-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556939, 'Apr  6 03:29:10 172.16.20.221 1649190549.287   4697 172.16.10.53 TCP_TUNNEL/200 3989 CONNECT b1t-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556940, 'Apr  6 03:29:10 172.16.20.221 1649190549.287   3762 172.16.10.53 TCP_TUNNEL/200 3677 CONNECT b1t-sadc1.zemanta.com:443 - HIER_DIRECT/38.133.127.95 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556941, 'Apr  6 03:29:10 172.16.20.221 1649190549.288   4354 172.16.10.53 TCP_TUNNEL/200 8469 CONNECT browser.events.data.msn.com:443 - HIER_DIRECT/52.168.117.170 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556942, 'Apr  6 03:29:10 172.16.20.221 1649190549.288   3763 172.16.10.53 TCP_TUNNEL/200 8432 CONNECT browser.events.data.msn.com:443 - HIER_DIRECT/52.168.117.170 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556943, 'Apr  6 03:29:10 172.16.20.221 1649190549.289   5207 172.16.10.53 TCP_TUNNEL/200 7192 CONNECT r.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556944, 'Apr  6 03:29:10 172.16.20.221 1649190549.289   5203 172.16.10.53 TCP_TUNNEL/200 7192 CONNECT r.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556945, 'Apr  6 03:29:10 172.16.20.221 1649190549.289   5201 172.16.10.53 TCP_TUNNEL/200 6197 CONNECT r.msftstatic.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556946, 'Apr  6 03:29:10 172.16.20.221 1649190549.290   5202 172.16.10.53 TCP_TUNNEL/200 6197 CONNECT r.msftstatic.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556947, 'Apr  6 03:29:10 172.16.20.221 1649190549.290   9171 172.16.10.53 TCP_TUNNEL/200 4207219 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556948, 'Apr  6 03:29:10 172.16.20.221 1649190549.290   5311 172.16.10.53 TCP_TUNNEL/200 7129 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/52.168.117.170 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556949, 'Apr  6 03:29:10 172.16.20.221 1649190549.291   5184 172.16.10.53 TCP_TUNNEL/200 6455 CONNECT sb.scorecardresearch.com:443 - HIER_DIRECT/65.9.182.83 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556950, 'Apr  6 03:29:10 172.16.20.221 1649190549.291   4678 172.16.10.53 TCP_TUNNEL/200 3345 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556951, 'Apr  6 03:29:10 172.16.20.221 1649190549.291   5453 172.16.10.53 TCP_TUNNEL/200 71450 CONNECT assets.msn.com:443 - HIER_DIRECT/122.155.237.225 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556952, 'Apr  6 03:29:10 172.16.20.221 1649190549.291   2160 172.16.10.53 TCP_TUNNEL/200 4965 CONNECT www.google.com:443 - HIER_DIRECT/142.251.12.103 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556953, 'Apr  6 03:29:10 172.16.20.221 1649190549.292   2038 172.16.10.53 TCP_TUNNEL/200 5137 CONNECT pix.as.criteo.net:443 - HIER_DIRECT/182.161.73.135 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556954, 'Apr  6 03:29:10 172.16.20.221 1649190549.292   5206 172.16.10.53 TCP_TUNNEL/200 7582 CONNECT c.msn.com:443 - HIER_DIRECT/52.231.207.240 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556955, 'Apr  6 03:29:10 172.16.20.221 1649190549.292   5719 172.16.10.53 TCP_TUNNEL/200 15579 CONNECT srtb.msn.com:443 - HIER_DIRECT/204.79.197.203 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556956, 'Apr  6 03:29:10 172.16.20.221 1649190549.292   8263 172.16.10.53 TCP_TUNNEL/200 1808 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556957, 'Apr  6 03:29:10 172.16.20.221 1649190549.292   4701 172.16.10.53 TCP_TUNNEL/200 53985 CONNECT zem.outbrainimg.com:443 - HIER_DIRECT/199.232.46.132 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556958, 'Apr  6 03:29:10 172.16.20.221 1649190549.293   5460 172.16.10.53 TCP_TUNNEL/200 90134 CONNECT www.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556959, 'Apr  6 03:29:10 172.16.20.221 1649190549.293   5211 172.16.10.53 TCP_TUNNEL/200 8829 CONNECT www.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556960, 'Apr  6 03:29:10 172.16.20.221 1649190549.293   4703 172.16.10.53 TCP_TUNNEL/200 1096 CONNECT cat.sg1.as.criteo.com:443 - HIER_DIRECT/182.161.73.132 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556961, 'Apr  6 03:29:10 172.16.20.221 1649190549.293   4690 172.16.10.53 TCP_TUNNEL/200 8276 CONNECT c.bing.com:443 - HIER_DIRECT/13.107.21.200 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556962, 'Apr  6 03:29:10 172.16.20.221 1649190549.293   4674 172.16.10.53 TCP_TUNNEL/200 31374 CONNECT assets.msn.com:443 - HIER_DIRECT/122.155.237.225 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556963, 'Apr  6 03:29:10 172.16.20.221 1649190549.697    149 172.16.10.53 TCP_MISS/200 5233 GET http://119.81.44.155/phpmyadmin/ - HIER_DIRECT/119.81.44.155 text/html', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556964, 'Apr  6 03:29:10 172.16.20.221 1649190549.847    318 172.16.10.53 TCP_TUNNEL/200 8553 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556965, 'Apr  6 03:29:10 172.16.20.221 1649190549.858    300 172.16.10.53 TCP_TUNNEL/200 8587 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556966, 'Apr  6 03:29:10 172.16.20.221 1649190549.967     99 172.16.10.53 TCP_REFRESH_MODIFIED/200 9472 GET http://119.81.44.155/phpmyadmin/js/messages.php? - HIER_DIRECT/119.81.44.155 text/javascript', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556967, 'Apr  6 03:29:11 172.16.20.221 1649190550.626   1076 172.16.10.53 TCP_TUNNEL/200 7392 CONNECT browser.events.data.msn.com:443 - HIER_DIRECT/52.168.117.170 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556968, 'Apr  6 03:29:15 172.16.20.221 1649190554.452    145 172.16.10.53 TCP_MISS/302 1378 POST http://119.81.44.155/phpmyadmin/index.php - HIER_DIRECT/119.81.44.155 text/html', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556969, 'Apr  6 03:29:15 172.16.20.221 1649190554.494    181 172.16.10.53 TCP_TUNNEL/200 8573 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556970, 'Apr  6 03:29:15 172.16.20.221 1649190554.642    182 172.16.10.53 TCP_MISS/200 16050 GET http://119.81.44.155/phpmyadmin/index.php - HIER_DIRECT/119.81.44.155 text/html', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556971, 'Apr  6 03:29:15 172.16.20.221 1649190554.653    188 172.16.10.53 TCP_TUNNEL/200 8609 CONNECT nav.smartscreen.microsoft.com:443 - HIER_DIRECT/40.90.184.73 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556972, 'Apr  6 03:29:15 172.16.20.221 1649190554.819    112 172.16.10.53 TCP_MISS/200 21628 GET http://119.81.44.155/phpmyadmin/phpmyadmin.css.php? - HIER_DIRECT/119.81.44.155 text/css', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556973, 'Apr  6 03:29:16 172.16.20.221 1649190555.351    213 172.16.10.53 TCP_MISS/200 2938 POST http://119.81.44.155/phpmyadmin/ajax.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556974, 'Apr  6 03:29:16 172.16.20.221 1649190555.604    205 172.16.10.53 TCP_MISS/200 3650 POST http://119.81.44.155/phpmyadmin/navigation.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556975, 'Apr  6 03:29:16 172.16.20.221 1649190555.634    194 172.16.10.53 TCP_MISS/200 3031 POST http://119.81.44.155/phpmyadmin/ajax.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556976, 'Apr  6 03:29:16 172.16.20.221 1649190555.941    254 172.16.10.53 TCP_MISS/200 2933 POST http://119.81.44.155/phpmyadmin/ajax.php - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556977, 'Apr  6 03:29:18 172.16.20.221 1649190557.300    226 172.16.10.53 TCP_MISS/200 11188 GET http://119.81.44.155/phpmyadmin/db_structure.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556978, 'Apr  6 03:29:18 172.16.20.221 1649190557.537    138 172.16.10.53 TCP_MISS/200 4212 GET http://119.81.44.155/phpmyadmin/navigation.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556979, 'Apr  6 03:29:19 172.16.20.221 1649190559.293   8138 172.16.10.53 TCP_TUNNEL/200 4807126 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556980, 'Apr  6 03:29:19 172.16.20.221 1649190559.294   7380 172.16.10.53 TCP_TUNNEL/200 6278 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/52.168.117.170 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556981, 'Apr  6 03:29:19 172.16.20.221 1649190559.294   7380 172.16.10.53 TCP_TUNNEL/200 7536 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/52.168.117.170 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556982, 'Apr  6 03:29:19 172.16.20.221 1649190559.294   3392 172.16.10.53 TCP_TUNNEL/200 1999 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556983, 'Apr  6 03:29:19 172.16.20.221 1649190559.294   3998 172.16.10.53 TCP_TUNNEL/200 7259 CONNECT edge.activity.windows.com:443 - HIER_DIRECT/20.184.57.167 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556984, 'Apr  6 03:29:21 172.16.20.221 1649190560.946    157 172.16.10.53 TCP_MISS/200 9394 GET http://119.81.44.155/phpmyadmin/sql.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556985, 'Apr  6 03:29:21 172.16.20.221 1649190561.200    120 172.16.10.53 TCP_MISS/200 2985 GET http://119.81.44.155/phpmyadmin/index.php? - HIER_DIRECT/119.81.44.155 application/json', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556986, 'Apr  6 03:29:29 172.16.20.221 1649190569.296   5329 172.16.10.53 TCP_TUNNEL/200 2106113 CONNECT rr2---sn-j5u-joml.googlevideo.com:443 - HIER_DIRECT/159.192.0.45 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556987, 'Apr  6 03:29:29 172.16.20.221 1649190569.296   6475 172.16.10.53 TCP_TUNNEL/200 6722 CONNECT functional.events.data.microsoft.com:443 - HIER_DIRECT/52.168.117.170 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556988, 'Apr  6 03:29:29 172.16.20.221 1649190569.296   2704 172.16.10.53 TCP_TUNNEL/200 4475 CONNECT www.facebook.com:443 - HIER_DIRECT/157.240.235.35 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556989, 'Apr  6 03:29:29 172.16.20.221 1649190569.297   8268 172.16.10.53 TCP_TUNNEL/200 1761 CONNECT www.youtube.com:443 - HIER_DIRECT/142.251.10.93 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556990, 'Apr  6 03:29:29 172.16.20.221 1649190569.297    802 172.16.10.53 TCP_TUNNEL/200 2315 CONNECT scontent.fbkk10-1.fna.fbcdn.net:443 - HIER_DIRECT/122.155.237.145 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log'),
(556991, 'Apr  6 03:29:29 172.16.20.221 1649190569.297   7962 172.16.10.53 TCP_TUNNEL/200 6635 CONNECT edge.microsoft.com:443 - HIER_DIRECT/204.79.197.219 -', '2022-04-06 03:32:25', 94, '172.16.20.221,03:00,2022-04-06.log');

-- --------------------------------------------------------

--
-- Table structure for table `log_history`
--

CREATE TABLE `log_history` (
  `id_log` int(11) NOT NULL,
  `log_action` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `log_detail` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `log_approve` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `log_date` datetime DEFAULT current_timestamp(),
  `domain_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `settingCookiepolicy_id` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `log_history`
--

INSERT INTO `log_history` (`id_log`, `log_action`, `log_detail`, `log_approve`, `log_date`, `domain_id`, `user_id`, `settingCookiepolicy_id`) VALUES
(60, 'เพิ่มโดเมน', 'https://www.pdpa.comk', NULL, '2022-02-14 22:27:16', NULL, 1, NULL),
(61, 'ตำเเหน่ง', 'ด้านบน', NULL, '2022-02-14 22:27:27', NULL, 1, NULL),
(62, 'ตำเเหน่ง', 'ตรงกลาง', NULL, '2022-02-15 22:27:46', NULL, 1, NULL),
(63, 'ลบโดเมน', 'https://www.pdpa.comk', NULL, '2022-02-14 22:28:05', NULL, 1, NULL),
(64, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-02-14 22:29:29', NULL, 1, NULL),
(65, 'ตำเเหน่ง', 'ตรงกลาง', NULL, '2022-02-14 22:30:05', NULL, 1, NULL),
(66, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-02-14 22:30:39', NULL, 1, NULL),
(67, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-02-14 22:32:19', NULL, 1, NULL),
(68, 'สีธีม', 'ดำ', NULL, '2022-02-14 22:54:04', NULL, 1, NULL),
(69, 'เเก้ไขข้อความ', 'กฏหมายของประเทศไทย', NULL, '2022-02-16 03:52:19', NULL, 1, NULL),
(70, 'เเก้ไขข้อความ', 'กฏหมายของประเทศไทย5555+\r\n', NULL, '2022-02-16 03:52:25', NULL, 1, NULL),
(71, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-02-16 03:52:30', NULL, 1, NULL),
(72, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-02-16 03:53:02', NULL, 1, NULL),
(73, 'ตำเเหน่ง', 'ตรงกลาง', NULL, '2022-02-16 06:08:42', NULL, 1, NULL),
(74, 'เพิ่มโดเมน', 'https://www.testระบบ.com', NULL, '2022-02-16 06:10:43', NULL, 1, NULL),
(75, 'สีธีม', 'ดำ', NULL, '2022-02-16 06:12:45', NULL, 1, NULL),
(76, 'ตำเเหน่ง', 'ด้านล่าง', NULL, '2022-02-16 06:34:21', NULL, 1, NULL),
(77, 'ตำเเหน่ง', 'ด้านบน', NULL, '2022-02-16 06:38:49', NULL, 1, NULL),
(78, 'สีธีม', 'ขาว', NULL, '2022-02-16 06:39:42', NULL, 1, NULL),
(80, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-16 06:48:13', NULL, 1, NULL),
(81, 'เพิ่มประเภทคุกกี้', 'จำเป็น', NULL, '2022-02-16 06:51:10', NULL, 1, NULL),
(82, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น555', NULL, '2022-02-16 06:56:18', NULL, 1, NULL),
(83, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น', NULL, '2022-02-16 06:57:04', NULL, 1, NULL),
(84, 'ลบโดเมน', 'https://www.testระบบ.com', NULL, '2022-02-16 07:24:10', NULL, 1, NULL),
(85, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-16 07:46:24', NULL, 1, NULL),
(86, 'เพิ่มประเภทคุกกี้', 'การตลาด', NULL, '2022-02-16 07:47:16', NULL, 1, NULL),
(87, 'สีธีม', 'ดำ', NULL, '2022-02-16 22:58:11', NULL, 1, NULL),
(88, 'ตำเเหน่ง', 'ด้านบน', NULL, '2022-02-16 23:30:43', NULL, 1, NULL),
(89, 'ตำเเหน่ง', 'ด้านล่าง', NULL, '2022-02-16 23:38:59', NULL, 1, NULL),
(90, 'ตำเเหน่ง', 'ด้านบน', NULL, '2022-02-17 00:14:20', NULL, 1, NULL),
(91, 'เพิ่มโดเมน', 'https://testsdfavbad', NULL, '2022-02-17 01:10:43', NULL, 1, NULL),
(92, 'เพิ่มโดเมน', 'https://4343453', NULL, '2022-02-17 02:26:08', NULL, 1, NULL),
(93, 'ลบโดเมน', 'https://4343453', NULL, '2022-02-17 20:06:15', NULL, 1, NULL),
(94, 'ลบโดเมน', 'https://testsdfavbad', NULL, '2022-02-17 20:06:18', NULL, 1, NULL),
(95, 'ตำเเหน่ง', 'ด้านล่าง', NULL, '2022-02-17 20:13:16', NULL, 1, NULL),
(96, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-17 20:16:41', NULL, 1, NULL),
(97, 'เพิ่มประเภทคุกกี้', 'สถิติ', NULL, '2022-02-17 20:19:46', NULL, 1, NULL),
(98, 'เพิ่มโดเมน', 'https://www.test1.com', NULL, '2022-02-17 20:25:01', NULL, 1, NULL),
(99, 'เพิ่มโดเมน', 'https://www.test3.com', NULL, '2022-02-18 20:21:38', NULL, 1, NULL),
(100, 'สีธีม', 'ขาว', NULL, '2022-02-18 20:41:35', NULL, 1, NULL),
(101, 'ลบโดเมน', 'https://www.test3.com', NULL, '2022-02-18 21:17:37', NULL, 1, NULL),
(102, 'ลบโดเมน', 'https://www.test1.com', NULL, '2022-02-18 21:17:41', NULL, 1, NULL),
(103, 'เพิ่มโดเมน', 'https://www.witthaya.com', NULL, '2022-02-18 22:52:29', NULL, 1, NULL),
(104, 'ตำเเหน่ง', 'ตรงกลาง', NULL, '2022-02-19 01:06:00', NULL, 1, NULL),
(105, 'ลบโดเมน', 'https://www.witthaya.com', NULL, '2022-02-19 10:38:07', NULL, 1, NULL),
(106, 'สร้าง Tag', NULL, NULL, '2022-02-21 00:59:18', NULL, 1, NULL),
(107, 'สร้าง Tag', NULL, NULL, '2022-02-21 01:01:12', NULL, 1, NULL),
(108, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:42:03', NULL, 1, NULL),
(109, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:43:03', NULL, 1, NULL),
(110, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:43:27', NULL, 1, NULL),
(111, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:43:57', NULL, 1, NULL),
(112, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:50:37', NULL, 1, NULL),
(113, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:51:49', NULL, 1, NULL),
(114, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:52:31', NULL, 1, NULL),
(115, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:53:45', NULL, 1, NULL),
(116, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:55:09', NULL, 1, NULL),
(117, 'สร้าง Tag', NULL, NULL, '2022-02-21 20:55:45', NULL, 1, NULL),
(118, 'สร้าง Tag', NULL, NULL, '2022-02-21 21:10:40', NULL, 1, NULL),
(119, 'สร้าง Tag', NULL, NULL, '2022-02-21 21:11:24', NULL, 1, NULL),
(120, 'สร้าง Tag', NULL, NULL, '2022-02-21 21:12:15', NULL, 1, NULL),
(121, 'สร้าง Tag', NULL, NULL, '2022-02-21 21:13:41', NULL, 1, NULL),
(122, 'สร้าง Tag', NULL, NULL, '2022-02-21 21:15:52', NULL, 1, NULL),
(123, 'สร้าง Tag', NULL, NULL, '2022-02-21 21:27:43', NULL, 1, NULL),
(124, 'สร้าง Tag', NULL, NULL, '2022-02-21 21:29:51', NULL, 1, NULL),
(125, 'สร้าง Tag', NULL, NULL, '2022-02-21 22:01:15', NULL, 1, NULL),
(126, 'สร้าง Tag', NULL, NULL, '2022-02-21 22:01:26', NULL, 1, NULL),
(127, 'สร้าง Tag', NULL, NULL, '2022-02-21 22:01:42', NULL, 1, NULL),
(128, 'สร้าง Tag', NULL, NULL, '2022-02-22 22:18:47', NULL, 1, NULL),
(129, 'สร้าง Tag', NULL, NULL, '2022-02-22 22:31:18', NULL, 1, NULL),
(130, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 01:08:26', NULL, 1, NULL),
(131, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 01:08:30', NULL, 1, NULL),
(132, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น5555+', NULL, '2022-02-23 01:08:39', NULL, 1, NULL),
(133, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น5555+', NULL, '2022-02-23 01:09:44', NULL, 1, NULL),
(134, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น', NULL, '2022-02-23 01:09:50', NULL, 1, NULL),
(135, 'เพิ่มประเภทคุกกี้', '55', NULL, '2022-02-23 01:09:58', NULL, 1, NULL),
(136, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 01:10:05', NULL, 1, NULL),
(137, 'เพิ่มประเภทคุกกี้', '2', NULL, '2022-02-23 01:19:54', NULL, 1, NULL),
(138, 'เพิ่มประเภทคุกกี้', '55', NULL, '2022-02-23 01:19:57', NULL, 1, NULL),
(139, 'เพิ่มประเภทคุกกี้', '45645', NULL, '2022-02-23 01:20:03', NULL, 1, NULL),
(140, 'เพิ่มประเภทคุกกี้', '453453', NULL, '2022-02-23 01:20:08', NULL, 1, NULL),
(141, 'เพิ่มประเภทคุกกี้', '5555555', NULL, '2022-02-23 01:21:00', NULL, 1, NULL),
(142, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 01:21:45', NULL, 1, NULL),
(143, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 01:28:30', NULL, 1, NULL),
(144, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 01:28:34', NULL, 1, NULL),
(145, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 01:28:38', NULL, 1, NULL),
(146, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 01:28:42', NULL, 1, NULL),
(147, 'เพิ่มประเภทคุกกี้', 'จำเป็น1', NULL, '2022-02-23 01:29:58', NULL, 1, NULL),
(148, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-23 22:12:46', NULL, 1, NULL),
(149, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น222222', NULL, '2022-02-24 00:05:51', NULL, 1, NULL),
(150, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-24 00:09:01', NULL, 1, NULL),
(151, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น1222222222', NULL, '2022-02-24 00:10:15', NULL, 1, NULL),
(152, 'เเก้ไขข้อมูลประเภทคุกกี้', '5555555555', NULL, '2022-02-24 00:10:26', NULL, 1, NULL),
(153, 'เเก้ไขข้อมูลประเภทคุกกี้', '55555555', NULL, '2022-02-24 00:15:05', NULL, 1, NULL),
(154, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-24 00:15:40', NULL, 1, NULL),
(155, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น2', NULL, '2022-02-24 00:16:46', NULL, 1, NULL),
(156, 'เเก้ไขข้อมูลประเภทคุกกี้', 'จำเป็น222025', NULL, '2022-02-24 00:17:01', NULL, 1, NULL),
(157, 'สร้าง Tag', NULL, NULL, '2022-02-24 22:53:44', NULL, 1, NULL),
(158, 'สร้าง Tag', NULL, NULL, '2022-02-25 20:01:49', NULL, 1, NULL),
(159, 'เพิ่มโดเมน', 'https://5555', NULL, '2022-02-25 20:03:53', NULL, 1, NULL),
(160, 'เพิ่มโดเมน', 'https://453453', NULL, '2022-02-25 20:04:05', NULL, 1, NULL),
(161, 'เพิ่มโดเมน', 'https://5555', NULL, '2022-02-25 20:10:51', NULL, 1, NULL),
(162, 'สร้าง Tag', NULL, NULL, '2022-02-25 20:17:20', NULL, 1, NULL),
(163, 'ลบโดเมน', 'https://5555', NULL, '2022-02-25 20:23:06', NULL, 1, NULL),
(164, 'ลบโดเมน', 'https://453453', NULL, '2022-02-25 20:23:10', NULL, 1, NULL),
(165, 'ลบโดเมน', 'https://5555', NULL, '2022-02-25 20:23:14', NULL, 1, NULL),
(166, 'เพิ่มโดเมน', 'https://test1', NULL, '2022-02-25 20:23:20', NULL, 1, NULL),
(167, 'เเก้ไขข้อความ', 'sss', NULL, '2022-02-25 20:23:26', NULL, 1, NULL),
(168, 'ลบโดเมน', 'https://test1', NULL, '2022-02-25 20:23:34', NULL, 1, NULL),
(169, 'เพิ่มโดเมน', 'https://sdfsdf', NULL, '2022-02-25 20:24:48', NULL, 1, NULL),
(170, 'ลบโดเมน', 'https://sdfsdf', NULL, '2022-02-25 20:49:20', NULL, 1, NULL),
(171, 'เพิ่มโดเมน', 'https://test.1แนทก', NULL, '2022-02-25 20:49:41', NULL, 1, NULL),
(172, 'เเก้ไขข้อความ', '123543', NULL, '2022-02-25 20:49:47', NULL, 1, NULL),
(173, 'ลบโดเมน', 'https://test.1แนทก', NULL, '2022-02-25 20:56:46', NULL, 1, NULL),
(174, 'เพิ่มโดเมน', 'https://www.CE.com', NULL, '2022-02-25 21:12:03', NULL, 1, NULL),
(175, 'เเก้ไขข้อความ', 'CE', NULL, '2022-02-25 21:13:13', NULL, 1, NULL),
(176, 'เพิ่มโดเมน', 'https://WWW.ce3.mirot', NULL, '2022-02-25 21:21:40', NULL, 1, NULL),
(177, 'เพิ่มโดเมน', 'https://sdfsd', NULL, '2022-02-25 21:21:45', NULL, 1, NULL),
(178, 'ลบโดเมน', 'https://sdfsd', NULL, '2022-02-25 21:21:58', NULL, 1, NULL),
(179, 'ลบโดเมน', 'https://WWW.ce3.mirot', NULL, '2022-02-25 21:22:35', NULL, 1, NULL),
(180, 'สร้าง Tag', NULL, NULL, '2022-02-25 22:38:00', NULL, 1, NULL),
(181, 'เพิ่มประเภทคุกกี้', '453453', NULL, '2022-02-25 23:55:38', NULL, 1, NULL),
(182, 'เพิ่มประเภทคุกกี้', '4566', NULL, '2022-02-25 23:55:45', NULL, 1, NULL),
(183, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-25 23:55:57', NULL, 1, NULL),
(184, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-25 23:56:00', NULL, 1, NULL),
(185, 'เพิ่มประเภทคุกกี้', '55', NULL, '2022-02-25 23:56:13', NULL, 1, NULL),
(186, 'เพิ่มประเภทคุกกี้', '4523453', NULL, '2022-02-25 23:57:07', NULL, 1, NULL),
(187, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-26 00:10:53', NULL, 1, NULL),
(188, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-26 00:10:58', NULL, 1, NULL),
(189, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-26 00:11:01', NULL, 1, NULL),
(190, 'ลบโดเมน', 'https://www.CE.com', NULL, '2022-02-26 01:27:43', NULL, 1, NULL),
(191, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-02-26 01:28:35', NULL, 1, NULL),
(192, 'เพิ่มโดเมน', 'https://ww.PDPA.com', NULL, '2022-02-26 01:28:48', NULL, 1, NULL),
(193, 'เพิ่มโดเมน', 'https://www.PDPA1.com', NULL, '2022-02-26 01:43:44', NULL, 1, NULL),
(194, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-26 01:46:26', NULL, 1, NULL),
(195, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-26 01:46:32', NULL, 1, NULL),
(196, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-26 01:46:35', NULL, 1, NULL),
(197, 'เพิ่มประเภทคุกกี้', 'การตลาด', NULL, '2022-02-26 01:48:42', NULL, 1, NULL),
(198, 'เพิ่มโดเมน', 'https://www.PDPA2.COM', NULL, '2022-02-26 01:53:57', NULL, 1, NULL),
(199, 'ลบโดเมน', 'https://www.PDPA2.COM', NULL, '2022-02-26 02:01:48', NULL, 1, NULL),
(200, 'ลบโดเมน', 'https://www.PDPA1.com', NULL, '2022-02-26 02:01:55', NULL, 1, NULL),
(201, 'เพิ่มโดเมน', 'https://www.PDPA1.com', NULL, '2022-02-26 02:03:17', NULL, 1, NULL),
(202, 'เพิ่มโดเมน', 'https://www.pdpd.2.com', NULL, '2022-02-26 02:06:10', NULL, 1, NULL),
(203, 'เพิ่มประเภทคุกกี้', '1', NULL, '2022-02-26 03:32:43', NULL, 1, NULL),
(204, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-02-26 03:32:56', NULL, 1, NULL),
(205, 'สร้าง Tag', NULL, NULL, '2022-02-26 10:49:24', NULL, 1, NULL),
(206, 'ลบโดเมน', 'https://www.pdpd.2.com', NULL, '2022-02-27 22:36:41', NULL, 1, NULL),
(207, 'ลบโดเมน', 'https://www.PDPA1.com', NULL, '2022-02-27 22:39:20', NULL, 1, NULL),
(208, 'สร้าง Tag', NULL, NULL, '2022-02-27 23:16:34', NULL, 1, NULL),
(209, 'เพิ่มโดเมน', 'https://dfgdf', NULL, '2022-02-28 22:10:18', NULL, 1, NULL),
(210, 'ลบโดเมน', 'https://dfgdf', NULL, '2022-02-28 23:50:04', NULL, 1, NULL),
(211, 'เพิ่มโดเมน', 'https://4523453', NULL, '2022-02-28 23:50:59', NULL, 1, NULL),
(212, 'ลบโดเมน', 'https://4523453', NULL, '2022-02-28 23:51:07', NULL, 1, NULL),
(213, 'เพิ่มโดเมน', 'https://4534534', NULL, '2022-02-28 23:51:12', NULL, 1, NULL),
(214, 'ลบโดเมน', 'https://4534534', NULL, '2022-02-28 23:51:23', NULL, 1, NULL),
(215, 'เพิ่มโดเมน', 'https://www.CE.mirot', NULL, '2022-02-28 23:52:07', NULL, 1, NULL),
(216, 'สร้าง Tag', NULL, NULL, '2022-03-01 00:06:21', NULL, 1, NULL),
(217, 'สร้าง Tag', NULL, NULL, '2022-03-01 00:07:46', NULL, 1, NULL),
(218, 'สร้าง Tag', NULL, NULL, '2022-03-01 00:10:20', NULL, 1, NULL),
(219, 'สร้าง Tag', NULL, NULL, '2022-03-01 00:11:06', NULL, 1, NULL),
(220, 'ลบโดเมน', 'https://www.CE.mirot', NULL, '2022-03-01 02:33:14', NULL, 1, NULL),
(221, 'ลบโดเมน', 'https://ww.PDPA.com', NULL, '2022-03-01 02:51:47', NULL, 1, NULL),
(222, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-01 02:52:58', NULL, 1, NULL),
(223, 'สร้าง Tag', NULL, NULL, '2022-03-01 02:53:17', NULL, 1, NULL),
(224, 'เพิ่มโดเมน', 'https://sdfsdf', NULL, '2022-03-01 22:37:12', NULL, 1, NULL),
(225, 'เพิ่มโดเมน', 'https://4563453', NULL, '2022-03-01 22:44:54', NULL, 1, NULL),
(226, 'เพิ่มโดเมน', 'https://45343', NULL, '2022-03-01 22:52:47', NULL, 1, NULL),
(227, 'เพิ่มโดเมน', 'https://43453', NULL, '2022-03-01 22:53:12', NULL, 1, NULL),
(228, 'เพิ่มโดเมน', 'https://555', NULL, '2022-03-01 22:55:13', NULL, 1, NULL),
(229, 'เพิ่มโดเมน', 'https://88888888', NULL, '2022-03-01 23:10:19', NULL, 1, NULL),
(230, 'เพิ่มโดเมน', 'https://8588888888888888888888888888', NULL, '2022-03-01 23:16:44', NULL, 1, NULL),
(231, 'เพิ่มโดเมน', 'https://4535345', NULL, '2022-03-01 23:20:08', NULL, 1, NULL),
(232, 'เพิ่มโดเมน', 'https://3453453455555555555555555555555555555555555555555555', NULL, '2022-03-01 23:20:34', NULL, 1, NULL),
(233, 'เพิ่มโดเมน', 'https://453453453', NULL, '2022-03-01 23:47:39', NULL, 1, NULL),
(234, 'เพิ่มโดเมน', 'https://.45.45.', NULL, '2022-03-01 23:49:16', NULL, 1, NULL),
(235, 'เพิ่มโดเมน', 'https://45.45.45.', NULL, '2022-03-01 23:51:24', NULL, 1, NULL),
(236, 'เพิ่มประเภทคุกกี้', '777', NULL, '2022-03-02 00:02:44', NULL, 1, NULL),
(237, 'เพิ่มประเภทคุกกี้', '5', NULL, '2022-03-02 00:03:17', NULL, 1, NULL),
(238, 'เพิ่มประเภทคุกกี้', '4', NULL, '2022-03-02 00:03:21', NULL, 1, NULL),
(239, 'เพิ่มประเภทคุกกี้', '9', NULL, '2022-03-02 00:03:26', NULL, 1, NULL),
(240, 'เพิ่มประเภทคุกกี้', '86', NULL, '2022-03-02 00:03:33', NULL, 1, NULL),
(241, 'เพิ่มประเภทคุกกี้', '666', NULL, '2022-03-02 00:03:38', NULL, 1, NULL),
(242, 'เพิ่มประเภทคุกกี้', '66', NULL, '2022-03-02 00:03:41', NULL, 1, NULL),
(243, 'เพิ่มประเภทคุกกี้', '465456', NULL, '2022-03-02 00:03:45', NULL, 1, NULL),
(244, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 00:21:56', NULL, 1, NULL),
(245, 'เพิ่มประเภทคุกกี้', '453453', NULL, '2022-03-02 00:22:46', NULL, 1, NULL),
(246, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 00:33:47', NULL, 1, NULL),
(247, 'เพิ่มประเภทคุกกี้', '453453', NULL, '2022-03-02 00:39:57', NULL, 1, NULL),
(248, 'เพิ่มประเภทคุกกี้', '4453', NULL, '2022-03-02 00:40:01', NULL, 1, NULL),
(249, 'เพิ่มประเภทคุกกี้', '453453', NULL, '2022-03-02 00:40:06', NULL, 1, NULL),
(250, 'เพิ่มประเภทคุกกี้', '453', NULL, '2022-03-02 00:40:11', NULL, 1, NULL),
(251, 'เพิ่มประเภทคุกกี้', '453453', NULL, '2022-03-02 00:40:18', NULL, 1, NULL),
(252, 'เพิ่มประเภทคุกกี้', '453453', NULL, '2022-03-02 01:21:29', NULL, 1, NULL),
(253, 'เพิ่มประเภทคุกกี้', '1543', NULL, '2022-03-02 01:21:45', NULL, 1, NULL),
(254, 'เพิ่มประเภทคุกกี้', '4534', NULL, '2022-03-02 01:21:50', NULL, 1, NULL),
(255, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:22:24', NULL, 1, NULL),
(256, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:22:28', NULL, 1, NULL),
(257, 'เพิ่มประเภทคุกกี้', '88', NULL, '2022-03-02 01:34:48', NULL, 1, NULL),
(258, 'เพิ่มประเภทคุกกี้', '456345', NULL, '2022-03-02 01:35:03', NULL, 1, NULL),
(259, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:35:48', NULL, 1, NULL),
(260, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:35:54', NULL, 1, NULL),
(261, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:35:57', NULL, 1, NULL),
(262, 'เเก้ไขข้อมูลประเภทคุกกี้', '88888888888', NULL, '2022-03-02 01:36:04', NULL, 1, NULL),
(263, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:36:07', NULL, 1, NULL),
(264, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:36:23', NULL, 1, NULL),
(265, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:36:26', NULL, 1, NULL),
(266, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:36:29', NULL, 1, NULL),
(267, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:36:33', NULL, 1, NULL),
(268, 'เพิ่มประเภทคุกกี้', 'จำเป็น', NULL, '2022-03-02 01:42:43', NULL, 1, NULL),
(269, 'เพิ่มประเภทคุกกี้', 'การกำหนดค่า', NULL, '2022-03-02 01:42:53', NULL, 1, NULL),
(270, 'เพิ่มประเภทคุกกี้', 'สถิติ', NULL, '2022-03-02 01:43:02', NULL, 1, NULL),
(271, 'เพิ่มประเภทคุกกี้', 'การตลาด', NULL, '2022-03-02 01:43:11', NULL, 1, NULL),
(272, 'เพิ่มประเภทคุกกี้', 'ไม่ได้จำแนกประเภท', NULL, '2022-03-02 01:43:21', NULL, 1, NULL),
(273, 'เพิ่มประเภทคุกกี้', '5', NULL, '2022-03-02 01:43:28', NULL, 1, NULL),
(274, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:44:46', NULL, 1, NULL),
(275, 'เพิ่มประเภทคุกกี้', '555', NULL, '2022-03-02 01:50:24', NULL, 1, NULL),
(276, 'เเก้ไขข้อมูลประเภทคุกกี้', '555', NULL, '2022-03-02 01:50:33', NULL, 1, NULL),
(277, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-02 01:55:43', NULL, 1, NULL),
(278, 'เพิ่มโดเมน', 'https://55', NULL, '2022-03-02 02:01:30', NULL, 1, NULL),
(279, 'เพิ่มโดเมน', 'https://0000', NULL, '2022-03-02 02:02:31', NULL, 1, NULL),
(280, 'เพิ่มโดเมน', 'https://11111111', NULL, '2022-03-02 02:04:42', NULL, 1, NULL),
(281, 'เพิ่มโดเมน', 'https://1111', NULL, '2022-03-02 02:07:55', NULL, 1, NULL),
(282, 'เพิ่มโดเมน', 'https://x', NULL, '2022-03-02 02:24:58', NULL, 1, NULL),
(283, 'เพิ่มโดเมน', 'https://2', NULL, '2022-03-02 02:26:38', NULL, 1, NULL),
(284, 'เพิ่มโดเมน', 'https://000', NULL, '2022-03-02 02:39:44', NULL, 1, NULL),
(285, 'เพิ่มโดเมน', 'https://453453', NULL, '2022-03-02 03:05:29', NULL, 1, NULL),
(286, 'เพิ่มโดเมน', 'https://1', NULL, '2022-03-02 03:07:39', NULL, 1, NULL),
(287, 'ลบโดเมน', 'https://1', NULL, '2022-03-02 03:10:21', NULL, 1, NULL),
(288, 'เพิ่มประเภทคุกกี้', '45235', NULL, '2022-03-02 03:44:00', NULL, 1, NULL),
(289, 'ลบโดเมน', 'https://453453', NULL, '2022-03-02 03:57:12', NULL, 1, NULL),
(290, 'ลบโดเมน', 'https://000', NULL, '2022-03-02 03:57:16', NULL, 1, NULL),
(291, 'ลบโดเมน', 'https://2', NULL, '2022-03-02 03:57:20', NULL, 1, NULL),
(292, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:01:14', NULL, 1, NULL),
(293, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:11:16', NULL, 1, NULL),
(294, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:11:31', NULL, 1, NULL),
(295, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:12:13', NULL, 1, NULL),
(296, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:12:49', NULL, 1, NULL),
(297, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:13:31', NULL, 1, NULL),
(298, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:13:52', NULL, 1, NULL),
(299, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:16:39', NULL, 1, NULL),
(300, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:16:59', NULL, 1, NULL),
(301, 'เพิ่มโดเมน', 'https://sdf', NULL, '2022-03-02 04:17:33', NULL, 1, NULL),
(302, 'เพิ่มโดเมน', 'https://0000', NULL, '2022-03-02 04:18:48', NULL, 1, NULL),
(303, 'เพิ่มโดเมน', 'https://sdfs', NULL, '2022-03-02 04:19:43', NULL, 1, NULL),
(304, 'เพิ่มโดเมน', 'https://456', NULL, '2022-03-02 04:21:30', NULL, 1, NULL),
(305, 'เพิ่มโดเมน', 'https://+++563', NULL, '2022-03-02 04:22:24', NULL, 1, NULL),
(306, 'เพิ่มโดเมน', 'https://45345', NULL, '2022-03-02 04:28:25', NULL, 1, NULL),
(307, 'เพิ่มโดเมน', 'https://453', NULL, '2022-03-02 04:29:01', NULL, 1, NULL),
(308, 'เพิ่มโดเมน', 'https://', NULL, '2022-03-02 04:29:33', NULL, 1, NULL),
(309, 'เพิ่มโดเมน', 'https://777', NULL, '2022-03-02 04:31:48', NULL, 1, NULL),
(310, 'เพิ่มโดเมน', 'https://45545', NULL, '2022-03-02 04:33:13', NULL, 1, NULL),
(311, 'เพิ่มโดเมน', 'https://45345', NULL, '2022-03-02 04:33:55', NULL, 1, NULL),
(312, 'เพิ่มโดเมน', 'https://45345', NULL, '2022-03-02 04:34:44', NULL, 1, NULL),
(313, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:35:52', NULL, 1, NULL),
(314, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 04:38:03', NULL, 1, NULL),
(315, 'เพิ่มโดเมน', 'https://222', NULL, '2022-03-02 04:38:08', NULL, 1, NULL),
(316, 'ลบโดเมน', 'https://222', NULL, '2022-03-02 04:40:00', NULL, 1, NULL),
(317, 'เพิ่มโดเมน', 'https://55', NULL, '2022-03-02 04:40:04', NULL, 1, NULL),
(318, 'เพิ่มโดเมน', 'https://66', NULL, '2022-03-02 04:40:39', NULL, 1, NULL),
(319, 'เพิ่มโดเมน', 'https://77', NULL, '2022-03-02 04:41:29', NULL, 1, NULL),
(320, 'เพิ่มโดเมน', 'https://44534', NULL, '2022-03-02 04:43:22', NULL, 1, NULL),
(321, 'เพิ่มโดเมน', 'https://555', NULL, '2022-03-02 04:44:00', NULL, 1, NULL),
(322, 'เพิ่มโดเมน', 'https://45.4', NULL, '2022-03-02 04:44:32', NULL, 1, NULL),
(323, 'เพิ่มโดเมน', 'https://00', NULL, '2022-03-02 04:45:27', NULL, 1, NULL),
(324, 'เพิ่มโดเมน', 'https://45345345', NULL, '2022-03-02 04:46:27', NULL, 1, NULL),
(325, 'เพิ่มโดเมน', 'https://45345', NULL, '2022-03-02 04:46:45', NULL, 1, NULL),
(326, 'เพิ่มโดเมน', 'https://45.45', NULL, '2022-03-02 04:47:02', NULL, 1, NULL),
(327, 'เพิ่มโดเมน', 'https://hj,hj', NULL, '2022-03-02 04:47:23', NULL, 1, NULL),
(328, 'เพิ่มโดเมน', 'https://453', NULL, '2022-03-02 04:47:50', NULL, 1, NULL),
(329, 'เพิ่มโดเมน', 'https://45.445.45.45.', NULL, '2022-03-02 04:49:36', NULL, 1, NULL),
(330, 'เพิ่มโดเมน', 'https://453', NULL, '2022-03-02 04:50:00', NULL, 1, NULL),
(331, 'เพิ่มโดเมน', 'https://453453', NULL, '2022-03-02 04:50:48', NULL, 1, NULL),
(332, 'เพิ่มโดเมน', 'https://4534554.', NULL, '2022-03-02 04:53:38', NULL, 1, NULL),
(333, 'เพิ่มโดเมน', 'https://wittaya', NULL, '2022-03-02 04:54:12', NULL, 1, NULL),
(334, 'ลบโดเมน', 'https://wittaya', NULL, '2022-03-02 05:21:21', NULL, 1, NULL),
(335, 'ลบโดเมน', 'https://4534554.', NULL, '2022-03-02 05:21:25', NULL, 1, NULL),
(336, 'ลบโดเมน', 'https://453453', NULL, '2022-03-02 05:21:28', NULL, 1, NULL),
(337, 'ลบโดเมน', 'https://453', NULL, '2022-03-02 05:21:32', NULL, 1, NULL),
(338, 'ลบโดเมน', 'https://45.445.45.45.', NULL, '2022-03-02 05:21:37', NULL, 1, NULL),
(339, 'ลบโดเมน', 'https://453', NULL, '2022-03-02 05:21:41', NULL, 1, NULL),
(340, 'ลบโดเมน', 'https://45345', NULL, '2022-03-02 05:22:00', NULL, 1, NULL),
(341, 'ลบโดเมน', 'https://45345345', NULL, '2022-03-02 05:22:04', NULL, 1, NULL),
(342, 'ลบโดเมน', 'https://00', NULL, '2022-03-02 05:22:08', NULL, 1, NULL),
(343, 'ลบโดเมน', 'https://45.4', NULL, '2022-03-02 05:22:12', NULL, 1, NULL),
(344, 'ลบโดเมน', 'https://555', NULL, '2022-03-02 05:22:16', NULL, 1, NULL),
(345, 'ลบโดเมน', 'https://44534', NULL, '2022-03-02 05:22:20', NULL, 1, NULL),
(346, 'ลบโดเมน', 'https://77', NULL, '2022-03-02 05:22:23', NULL, 1, NULL),
(347, 'ลบโดเมน', 'https://66', NULL, '2022-03-02 05:22:27', NULL, 1, NULL),
(348, 'ลบโดเมน', 'https://55', NULL, '2022-03-02 05:22:31', NULL, 1, NULL),
(349, 'ลบโดเมน', 'https://45.45', NULL, '2022-03-02 15:22:46', NULL, 1, NULL),
(350, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-02 20:33:03', NULL, 1, NULL),
(351, 'สร้าง Tag', NULL, NULL, '2022-03-03 14:38:49', NULL, 1, NULL),
(352, 'สร้าง Tag', NULL, NULL, '2022-03-03 14:54:54', NULL, 1, NULL),
(353, 'สร้าง Tag', NULL, NULL, '2022-03-03 14:58:09', NULL, 1, NULL),
(354, 'สร้าง Tag', NULL, NULL, '2022-03-03 15:24:18', NULL, 1, NULL),
(355, 'สร้าง Tag', NULL, NULL, '2022-03-03 15:25:26', NULL, 1, NULL),
(356, 'สร้าง Tag', NULL, NULL, '2022-03-03 15:36:22', NULL, 1, NULL),
(357, 'สร้าง Tag', NULL, NULL, '2022-03-03 15:37:11', NULL, 1, NULL),
(358, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-03 19:34:01', NULL, 1, NULL),
(359, 'เพิ่มโดเมน', 'https://www.email.com', NULL, '2022-03-05 11:11:40', NULL, 1, NULL),
(360, 'เพิ่มโดเมน', 'https://testttt', NULL, '2022-03-10 21:34:41', NULL, 1, NULL),
(361, 'ลบโดเมน', 'https://testttt', NULL, '2022-03-10 21:41:55', NULL, 1, NULL),
(362, 'เพิ่มโดเมน', 'https://sdds', NULL, '2022-03-10 21:42:27', NULL, 1, NULL),
(363, 'ลบโดเมน', 'https://sdds', NULL, '2022-03-10 21:45:24', NULL, 1, NULL),
(364, 'เพิ่มโดเมน', 'https://tttttt.com', NULL, '2022-03-10 21:46:01', NULL, 1, NULL),
(365, 'ลบโดเมน', 'https://tttttt.com', NULL, '2022-03-10 21:47:33', NULL, 1, NULL),
(366, 'เพิ่มโดเมน', 'https://ddddd', NULL, '2022-03-10 21:47:38', NULL, 1, NULL),
(367, 'ลบโดเมน', 'https://ddddd', NULL, '2022-03-10 21:49:26', NULL, 1, NULL),
(368, 'เพิ่มโดเมน', 'https://wit', NULL, '2022-03-10 21:49:32', NULL, 1, NULL),
(369, 'ลบโดเมน', 'https://wit', NULL, '2022-03-10 21:53:50', NULL, 1, NULL),
(370, 'เพิ่มโดเมน', 'https://www', NULL, '2022-03-10 21:53:54', NULL, 1, NULL),
(371, 'เพิ่มโดเมน', 'https://4345', NULL, '2022-03-10 21:57:35', NULL, 1, NULL),
(372, 'ลบโดเมน', 'https://4345', NULL, '2022-03-10 22:00:06', NULL, 1, NULL),
(373, 'ลบโดเมน', 'https://www', NULL, '2022-03-10 22:00:10', NULL, 1, NULL),
(374, 'เพิ่มโดเมน', 'https://sdf', NULL, '2022-03-10 22:00:16', NULL, 1, NULL),
(375, 'ลบโดเมน', 'https://sdf', NULL, '2022-03-10 22:01:15', NULL, 1, NULL),
(376, 'เพิ่มโดเมน', 'https://sdfsd', NULL, '2022-03-10 22:01:19', NULL, 1, NULL),
(377, 'เพิ่มโดเมน', 'https://ddd', NULL, '2022-03-10 22:01:40', NULL, 1, NULL),
(378, 'เพิ่มโดเมน', 'https://55', NULL, '2022-03-10 22:02:31', NULL, 1, NULL),
(379, 'ลบโดเมน', 'https://55', NULL, '2022-03-10 22:05:36', NULL, 1, NULL),
(380, 'ลบโดเมน', 'https://ddd', NULL, '2022-03-10 22:05:40', NULL, 1, NULL),
(381, 'เพิ่มโดเมน', 'https://555', NULL, '2022-03-10 22:05:44', NULL, 1, NULL),
(382, 'ลบโดเมน', 'https://555', NULL, '2022-03-10 22:06:29', NULL, 1, NULL),
(383, 'เพิ่มโดเมน', 'https://444', NULL, '2022-03-10 22:07:14', NULL, 1, NULL),
(384, 'ลบโดเมน', 'https://444', NULL, '2022-03-10 22:08:30', NULL, 1, NULL),
(385, 'ลบโดเมน', 'https://sdfsd', NULL, '2022-03-10 22:08:34', NULL, 1, NULL),
(386, 'เพิ่มโดเมน', 'https://hhjh', NULL, '2022-03-10 22:08:38', NULL, 1, NULL),
(387, 'ลบโดเมน', 'https://hhjh', NULL, '2022-03-10 22:09:17', NULL, 1, NULL),
(388, 'เพิ่มโดเมน', 'https://55', NULL, '2022-03-10 22:09:20', NULL, 1, NULL),
(389, 'ลบโดเมน', 'https://55', NULL, '2022-03-10 22:10:14', NULL, 1, NULL),
(390, 'เพิ่มโดเมน', 'https://555', NULL, '2022-03-10 22:10:18', NULL, 1, NULL),
(391, 'ลบโดเมน', 'https://555', NULL, '2022-03-10 22:12:45', NULL, 1, NULL),
(392, 'เพิ่มโดเมน', 'https://55', NULL, '2022-03-10 22:18:52', NULL, 1, NULL),
(393, 'เพิ่มโดเมน', 'https://444', NULL, '2022-03-10 22:24:25', NULL, 1, NULL),
(394, 'เพิ่มโดเมน', 'https://5555', NULL, '2022-03-10 22:24:39', NULL, 1, NULL),
(395, 'ลบโดเมน', 'https://5555', NULL, '2022-03-10 22:26:29', NULL, 1, NULL),
(396, 'ลบโดเมน', 'https://444', NULL, '2022-03-10 22:26:34', NULL, 1, NULL),
(397, 'ลบโดเมน', 'https://55', NULL, '2022-03-10 22:26:38', NULL, 1, NULL),
(398, 'เพิ่มโดเมน', 'https://5666', NULL, '2022-03-10 22:26:43', NULL, 1, NULL),
(399, 'เพิ่มโดเมน', 'https://555', NULL, '2022-03-10 22:27:12', NULL, 1, NULL),
(400, 'เพิ่มโดเมน', 'https://444', NULL, '2022-03-10 22:29:28', NULL, 1, NULL),
(401, 'ลบโดเมน', 'https://444', NULL, '2022-03-10 22:30:18', NULL, 1, NULL),
(402, 'ลบโดเมน', 'https://555', NULL, '2022-03-10 22:30:21', NULL, 1, NULL),
(403, 'เพิ่มโดเมน', 'https://22', NULL, '2022-03-10 22:30:26', NULL, 1, NULL),
(404, 'เพิ่มโดเมน', 'https://444', NULL, '2022-03-10 22:34:06', NULL, 1, NULL),
(405, 'เพิ่มโดเมน', 'https://555', NULL, '2022-03-10 22:35:11', NULL, 1, NULL),
(406, 'เพิ่มโดเมน', 'https://44', NULL, '2022-03-10 22:35:29', NULL, 1, NULL),
(407, 'ลบโดเมน', 'https://44', NULL, '2022-03-10 22:36:03', NULL, 1, NULL),
(408, 'ลบโดเมน', 'https://555', NULL, '2022-03-10 22:36:06', NULL, 1, NULL),
(409, 'ลบโดเมน', 'https://444', NULL, '2022-03-10 22:36:11', NULL, 1, NULL),
(410, 'ลบโดเมน', 'https://22', NULL, '2022-03-10 22:36:14', NULL, 1, NULL),
(411, 'ลบโดเมน', 'https://5666', NULL, '2022-03-10 22:36:21', NULL, 1, NULL),
(412, 'เพิ่มโดเมน', 'https://55', NULL, '2022-03-10 22:36:26', NULL, 1, NULL),
(413, 'เพิ่มโดเมน', 'https://5', NULL, '2022-03-10 22:37:42', NULL, 1, NULL),
(414, 'เพิ่มโดเมน', 'https://666', NULL, '2022-03-10 22:38:11', NULL, 1, NULL),
(415, 'ลบโดเมน', 'https://666', NULL, '2022-03-10 22:39:53', NULL, 1, NULL),
(416, 'ลบโดเมน', 'https://5', NULL, '2022-03-10 22:39:57', NULL, 1, NULL),
(417, 'ลบโดเมน', 'https://55', NULL, '2022-03-10 22:40:01', NULL, 1, NULL),
(418, 'เพิ่มโดเมน', 'https://4545', NULL, '2022-03-10 22:40:05', NULL, 1, NULL),
(419, 'ลบโดเมน', 'https://4545', NULL, '2022-03-10 22:41:32', NULL, 1, NULL),
(420, 'เพิ่มโดเมน', 'https://44', NULL, '2022-03-10 22:45:29', NULL, 1, NULL),
(421, 'ลบโดเมน', 'https://44', NULL, '2022-03-10 22:46:12', NULL, 1, NULL),
(422, 'เพิ่มโดเมน', 'https://5', NULL, '2022-03-10 22:46:16', NULL, 1, NULL),
(423, 'เพิ่มโดเมน', 'https://44', NULL, '2022-03-10 22:48:34', NULL, 1, NULL),
(424, 'เพิ่มโดเมน', 'https://sdf', NULL, '2022-03-10 22:49:30', NULL, 1, NULL),
(425, 'เพิ่มโดเมน', 'https://666', NULL, '2022-03-10 22:49:56', NULL, 1, NULL),
(426, 'ลบโดเมน', 'https://666', NULL, '2022-03-10 22:51:55', NULL, 1, NULL),
(427, 'ลบโดเมน', 'https://sdf', NULL, '2022-03-10 22:51:59', NULL, 1, NULL),
(428, 'ลบโดเมน', 'https://44', NULL, '2022-03-10 22:52:04', NULL, 1, NULL),
(429, 'ลบโดเมน', 'https://5', NULL, '2022-03-10 22:52:08', NULL, 1, NULL),
(430, 'เพิ่มโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:52:21', NULL, 1, NULL),
(431, 'ลบโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:53:12', NULL, 1, NULL),
(432, 'เพิ่มโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:53:23', NULL, 1, NULL),
(433, 'ลบโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:54:34', NULL, 1, NULL),
(434, 'เพิ่มโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:54:42', NULL, 1, NULL),
(435, 'ลบโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:56:31', NULL, 1, NULL),
(436, 'เพิ่มโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:56:42', NULL, 1, NULL),
(437, 'ลบโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:58:49', NULL, 1, NULL),
(438, 'เพิ่มโดเมน', 'https://www.wit.com', NULL, '2022-03-10 22:59:01', NULL, 1, NULL),
(439, 'เพิ่มโดเมน', 'https://dd', NULL, '2022-03-10 23:01:54', NULL, 1, NULL),
(440, 'ลบโดเมน', 'https://dd', NULL, '2022-03-10 23:08:27', NULL, 1, NULL),
(441, 'ลบโดเมน', 'https://www.wit.com', NULL, '2022-03-10 23:09:06', NULL, 1, NULL),
(442, 'เพิ่มโดเมน', 'https://5666', NULL, '2022-03-10 23:10:05', NULL, 1, NULL),
(443, 'ลบโดเมน', 'https://5666', NULL, '2022-03-10 23:10:09', NULL, 1, NULL),
(444, 'เพิ่มโดเมน', 'https://www.wit.com', NULL, '2022-03-10 23:11:26', NULL, 1, NULL),
(445, 'ลบโดเมน', 'https://www.wit.com', NULL, '2022-03-10 23:11:30', NULL, 1, NULL),
(446, 'เพิ่มโดเมน', 'https://www.wittt.com', NULL, '2022-03-10 23:12:52', NULL, 1, NULL),
(447, 'ลบโดเมน', 'https://www.wittt.com', NULL, '2022-03-10 23:13:01', NULL, 1, NULL),
(448, 'ลบโดเมน', 'https://www.email.com', NULL, '2022-03-10 23:13:51', NULL, 1, NULL),
(449, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-03-10 23:14:04', NULL, 1, NULL),
(450, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-10 23:15:10', NULL, 1, NULL),
(451, 'เพิ่มโดเมน', 'https://www.Mail.com', NULL, '2022-03-10 23:29:06', NULL, 1, NULL),
(452, 'เพิ่มโดเมน', 'https://www.test.com', NULL, '2022-03-10 23:31:41', NULL, 1, NULL),
(453, 'ลบโดเมน', 'https://www.test.com', NULL, '2022-03-10 23:32:46', NULL, 1, NULL),
(454, 'เพิ่มโดเมน', 'https://www.test.com', NULL, '2022-03-10 23:32:55', NULL, 1, NULL),
(455, 'เพิ่มโดเมน', 'https://www.test2.com', NULL, '2022-03-10 23:34:47', NULL, 1, NULL),
(456, 'เพิ่มโดเมน', 'https://www.test3.com', NULL, '2022-03-10 23:40:08', NULL, 1, NULL),
(457, 'ลบโดเมน', 'https://www.test3.com', NULL, '2022-03-10 23:40:42', NULL, 1, NULL),
(458, 'เพิ่มโดเมน', 'https://test3.com', NULL, '2022-03-10 23:41:04', NULL, 1, NULL),
(459, 'เพิ่มโดเมน', 'https://sss', NULL, '2022-03-10 23:46:53', NULL, 1, NULL),
(460, 'ลบโดเมน', 'https://sss', NULL, '2022-03-10 23:50:58', NULL, 1, NULL),
(461, 'ลบโดเมน', 'https://test3.com', NULL, '2022-03-10 23:51:02', NULL, 1, NULL),
(462, 'ลบโดเมน', 'https://www.test2.com', NULL, '2022-03-10 23:51:06', NULL, 1, NULL),
(463, 'ลบโดเมน', 'https://www.test.com', NULL, '2022-03-10 23:51:11', NULL, 1, NULL),
(464, 'ลบโดเมน', 'https://www.Mail.com', NULL, '2022-03-10 23:51:16', NULL, 1, NULL),
(465, 'ลบโดเมน', 'https://www.PDPA.com', NULL, '2022-03-10 23:51:20', NULL, 1, NULL),
(466, 'เพิ่มโดเมน', 'https://www.PDPA.com', NULL, '2022-03-10 23:51:34', NULL, 1, NULL),
(467, 'เพิ่มโดเมน', 'https://www.E-mail.com', NULL, '2022-03-10 23:55:16', NULL, 1, NULL),
(468, 'เพิ่มประเภทคุกกี้', NULL, NULL, '2022-03-11 00:07:55', NULL, 1, NULL),
(469, 'เพิ่มประเภทคุกกี้', NULL, NULL, '2022-03-11 00:07:55', NULL, 1, NULL),
(470, 'เพิ่มประเภทคุกกี้', NULL, NULL, '2022-03-11 00:07:55', NULL, 1, NULL),
(471, 'เพิ่มประเภทคุกกี้', NULL, NULL, '2022-03-11 15:41:28', NULL, 1, NULL),
(472, 'สร้าง Tag', NULL, NULL, '2022-03-15 00:09:23', NULL, 1, NULL),
(473, 'ปฏิเสธประเภทคุกกี้', NULL, NULL, '2022-03-17 01:03:02', 279, NULL, NULL),
(474, 'อนุญาตประเภทคุกกี้', NULL, NULL, '2022-03-17 01:05:53', 279, NULL, NULL),
(475, 'อนุญาตประเภทคุกกี้', NULL, NULL, '2022-03-17 01:13:48', 279, NULL, NULL),
(476, 'อนุญาตประเภทคุกกี้', NULL, NULL, '2022-03-17 01:14:07', 279, NULL, NULL),
(477, 'อนุญาตประเภทคุกกี้', NULL, NULL, '2022-03-17 01:15:13', 279, NULL, NULL),
(478, 'อนุญาตประเภทคุกกี้', NULL, NULL, '2022-03-17 01:15:23', 279, NULL, NULL),
(479, 'อนุญาตประเภทคุกกี้', NULL, NULL, '2022-03-17 01:15:43', 279, NULL, NULL),
(480, 'อนุญาตประเภทคุกกี้', NULL, NULL, '2022-03-17 01:18:19', 279, NULL, NULL),
(481, 'เเก้ไขข้อมูลประเภทคุกกี้', 'null', NULL, '2022-03-20 00:39:27', NULL, 1, NULL),
(482, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-20 20:59:58', NULL, 1, NULL),
(483, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-20 21:00:01', NULL, 1, NULL),
(484, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-20 21:00:04', NULL, 1, NULL),
(485, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-20 21:00:08', NULL, 1, NULL),
(486, 'เพิ่มประเภทคุกกี้', '55', NULL, '2022-03-20 23:20:53', NULL, 1, NULL),
(487, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-21 00:17:56', NULL, 1, NULL),
(488, 'เพิ่มประเภทคุกกี้', '41', NULL, '2022-03-21 00:33:04', NULL, 1, NULL),
(489, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-21 00:34:39', NULL, 1, NULL),
(490, 'เพิ่มโดเมน', 'https://test', NULL, '2022-03-21 01:46:32', NULL, 1, NULL),
(491, 'ลบโดเมน', 'https://test', NULL, '2022-03-21 23:54:51', NULL, 1, NULL),
(492, 'เพิ่มประเภทคุกกี้', 'กก', NULL, '2022-03-22 00:33:08', NULL, 1, NULL),
(493, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 00:56:06', NULL, 1, NULL),
(494, 'เพิ่มประเภทคุกกี้', 'ทดสอบการเพิ่มประภทคุกกี้', NULL, '2022-03-22 00:56:45', NULL, 1, NULL),
(495, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 00:57:20', NULL, 1, NULL),
(496, 'เพิ่มประเภทคุกกี้', 'ทดสอบการเพิ่มประภทคุกกี้', NULL, '2022-03-22 00:57:25', NULL, 1, NULL),
(497, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 00:59:08', NULL, 1, NULL),
(498, 'เพิ่มประเภทคุกกี้', 'ทดสอบการเพิ่มประภทคุกกี้', NULL, '2022-03-22 00:59:12', NULL, 1, NULL),
(499, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 00:59:39', NULL, 1, NULL),
(500, 'เพิ่มประเภทคุกกี้', 'ทดสอบการเพิ่มประภทคุกกี้', NULL, '2022-03-22 00:59:43', NULL, 1, NULL),
(501, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:00:22', NULL, 1, NULL),
(502, 'เพิ่มประเภทคุกกี้', 'test', NULL, '2022-03-22 01:00:35', NULL, 1, NULL),
(503, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:05:48', NULL, 1, NULL),
(504, 'เพิ่มประเภทคุกกี้', 'sdf', NULL, '2022-03-22 01:05:53', NULL, 1, NULL),
(505, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:08:27', NULL, 1, NULL),
(506, 'เพิ่มประเภทคุกกี้', 'dddddd', NULL, '2022-03-22 01:08:31', NULL, 1, NULL),
(507, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:11:17', NULL, 1, NULL),
(508, 'เพิ่มประเภทคุกกี้', 'd', NULL, '2022-03-22 01:11:21', NULL, 1, NULL),
(509, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:14:00', NULL, 1, NULL),
(510, 'เพิ่มประเภทคุกกี้', 'dsf', NULL, '2022-03-22 01:14:04', NULL, 1, NULL),
(511, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:16:36', NULL, 1, NULL),
(512, 'เพิ่มประเภทคุกกี้', '1', NULL, '2022-03-22 01:16:42', NULL, 1, NULL),
(513, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:20:05', NULL, 1, NULL),
(514, 'เพิ่มประเภทคุกกี้', 'd', NULL, '2022-03-22 01:20:11', NULL, 1, NULL),
(515, 'เพิ่มประเภทคุกกี้', '88', NULL, '2022-03-22 01:21:01', NULL, 1, NULL),
(516, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:22:55', NULL, 1, NULL),
(517, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:22:59', NULL, 1, NULL),
(518, 'เพิ่มประเภทคุกกี้', '021', NULL, '2022-03-22 01:23:06', NULL, 1, NULL),
(519, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:26:48', NULL, 1, NULL),
(520, 'เพิ่มประเภทคุกกี้', '021', NULL, '2022-03-22 01:26:55', NULL, 1, NULL),
(521, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:31:56', NULL, 1, NULL),
(522, 'เพิ่มประเภทคุกกี้', '999', NULL, '2022-03-22 01:32:01', NULL, 1, NULL),
(523, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:35:38', NULL, 1, NULL),
(524, 'เพิ่มประเภทคุกกี้', 'cc', NULL, '2022-03-22 01:35:44', NULL, 1, NULL),
(525, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:36:39', NULL, 1, NULL),
(526, 'เพิ่มประเภทคุกกี้', '123123', NULL, '2022-03-22 01:36:45', NULL, 1, NULL),
(527, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:39:04', NULL, 1, NULL),
(528, 'เพิ่มประเภทคุกกี้', 'asdf', NULL, '2022-03-22 01:39:13', NULL, 1, NULL),
(529, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:40:57', NULL, 1, NULL),
(530, 'เพิ่มประเภทคุกกี้', 'ddfgdf', NULL, '2022-03-22 01:41:02', NULL, 1, NULL),
(531, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:41:31', NULL, 1, NULL),
(532, 'เพิ่มประเภทคุกกี้', '000000000000000000', NULL, '2022-03-22 01:41:37', NULL, 1, NULL),
(533, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:44:39', NULL, 1, NULL),
(534, 'เพิ่มประเภทคุกกี้', '0', NULL, '2022-03-22 01:44:43', NULL, 1, NULL),
(535, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:53:10', NULL, 1, NULL),
(536, 'เพิ่มประเภทคุกกี้', '888', NULL, '2022-03-22 01:53:15', NULL, 1, NULL),
(537, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:53:53', NULL, 1, NULL),
(538, 'เพิ่มประเภทคุกกี้', 'cccc', NULL, '2022-03-22 01:53:57', NULL, 1, NULL),
(539, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 01:55:40', NULL, 1, NULL),
(540, 'เพิ่มประเภทคุกกี้', '555', NULL, '2022-03-22 01:55:45', NULL, 1, NULL),
(541, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 12:54:59', NULL, 1, NULL),
(542, 'เพิ่มประเภทคุกกี้', 'test', NULL, '2022-03-22 12:55:05', NULL, 1, NULL),
(543, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 12:55:50', NULL, 1, NULL),
(544, 'เพิ่มประเภทคุกกี้', 'test', NULL, '2022-03-22 12:55:56', NULL, 1, NULL),
(545, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 12:56:34', NULL, 1, NULL),
(546, 'เพิ่มประเภทคุกกี้', 'test', NULL, '2022-03-22 12:56:40', NULL, 1, NULL),
(547, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 12:57:55', NULL, 1, NULL),
(548, 'เพิ่มประเภทคุกกี้', 'test', NULL, '2022-03-22 12:57:59', NULL, 1, NULL),
(549, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 12:59:57', NULL, 1, NULL),
(550, 'เพิ่มประเภทคุกกี้', 'test', NULL, '2022-03-22 13:00:02', NULL, 1, NULL),
(551, 'เพิ่มประเภทคุกกี้', 'ttttt', NULL, '2022-03-22 13:02:55', NULL, 1, NULL),
(552, 'เพิ่มประเภทคุกกี้', 'sss', NULL, '2022-03-22 13:03:33', NULL, 1, NULL),
(553, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 13:05:18', NULL, 1, NULL),
(554, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 13:05:21', NULL, 1, NULL),
(555, 'เพิ่มประเภทคุกกี้', 'ddd', NULL, '2022-03-22 13:05:26', NULL, 1, NULL),
(556, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 13:06:57', NULL, 1, NULL),
(557, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 13:07:01', NULL, 1, NULL),
(558, 'เพิ่มประเภทคุกกี้', 'ddd', NULL, '2022-03-22 13:07:05', NULL, 1, NULL),
(559, 'เพิ่มประเภทคุกกี้', 't', NULL, '2022-03-22 13:08:05', NULL, 1, NULL),
(560, 'เพิ่มประเภทคุกกี้', 'xxxx', NULL, '2022-03-22 13:08:33', NULL, 1, NULL),
(561, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 13:08:59', NULL, 1, NULL),
(562, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 13:09:02', NULL, 1, NULL),
(563, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 13:09:07', NULL, 1, NULL),
(564, 'เพิ่มประเภทคุกกี้', 'dd', NULL, '2022-03-22 13:09:11', NULL, 1, NULL),
(565, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 13:10:44', NULL, 1, NULL),
(566, 'เพิ่มประเภทคุกกี้', 'test', NULL, '2022-03-22 13:10:50', NULL, 1, NULL),
(567, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 20:51:51', NULL, 1, NULL),
(568, 'เพิ่มประเภทคุกกี้', 'ทดลองการเพิ่มข้อมูล', NULL, '2022-03-22 21:12:32', NULL, 1, NULL),
(569, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:15:47', NULL, 1, NULL),
(570, 'เพิ่มประเภทคุกกี้', 'ทดลองเพิ่มประเภทคุกกี้', NULL, '2022-03-22 21:22:53', NULL, 1, NULL),
(571, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:23:06', NULL, 1, NULL),
(572, 'เพิ่มประเภทคุกกี้', 'ทดลองเพิ่มประเภทคุกกี้', NULL, '2022-03-22 21:24:22', NULL, 1, NULL),
(573, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:24:26', NULL, 1, NULL),
(574, 'เพิ่มประเภทคุกกี้', 'ทดลองเพิ่มประเภทคุกกี้', NULL, '2022-03-22 21:25:22', NULL, 1, NULL),
(575, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:25:30', NULL, 1, NULL),
(576, 'เพิ่มประเภทคุกกี้', 'ทดลองเพิ่มประเภทคุกกี้', NULL, '2022-03-22 21:27:56', NULL, 1, NULL),
(577, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:28:05', NULL, 1, NULL),
(578, 'เพิ่มประเภทคุกกี้', 'ทดลองเพิ่มประเภทคุกกี้', NULL, '2022-03-22 21:29:18', NULL, 1, NULL),
(579, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:29:22', NULL, 1, NULL),
(580, 'เพิ่มประเภทคุกกี้', 'ทดลองเพิ่มประเภทคุกกี้', NULL, '2022-03-22 21:31:45', NULL, 1, NULL),
(581, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:31:52', NULL, 1, NULL),
(582, 'เพิ่มประเภทคุกกี้', 'ทดลองเพิ่มประเภทคุกกี้', NULL, '2022-03-22 21:32:27', NULL, 1, NULL),
(583, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:32:58', NULL, 1, NULL),
(584, 'เพิ่มประเภทคุกกี้', 'ทดลองเพิ่มประเภทคุกกี้', NULL, '2022-03-22 21:40:30', NULL, 1, NULL),
(585, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-22 21:42:26', NULL, 1, NULL),
(586, 'สีธีม', 'ดำ', NULL, '2022-03-26 02:10:50', 279, 1, NULL),
(587, 'สีธีม', 'ขาว', NULL, '2022-03-26 02:49:40', 279, 1, NULL),
(588, 'เพิ่มโดเมน', 'https://test', NULL, '2022-03-26 03:39:35', NULL, 1, NULL),
(589, 'ลบโดเมน', 'https://test', NULL, '2022-03-26 03:39:41', NULL, 1, NULL),
(590, 'สีธีม', 'ขาว', NULL, '2022-03-26 03:50:33', 279, 1, NULL),
(591, 'สีธีม', 'ดำ', NULL, '2022-03-26 03:50:42', 279, 1, NULL),
(592, 'สีธีม', 'ขาว', NULL, '2022-03-26 03:50:51', 279, 1, NULL),
(593, 'สีธีม', 'ดำ', NULL, '2022-03-26 03:51:09', 279, 1, NULL),
(594, 'สีธีม', 'ขาว', NULL, '2022-03-26 03:55:42', 279, 1, NULL),
(595, 'สีธีม', 'ดำ', NULL, '2022-03-26 04:06:35', 279, 1, NULL),
(596, 'สีธีม', 'ขาว', NULL, '2022-03-26 04:07:24', 279, 1, NULL),
(597, 'สีธีม', 'ดำ', NULL, '2022-03-26 04:07:58', 279, 1, NULL),
(598, 'สีธีม', 'ขาว', NULL, '2022-03-26 04:08:29', 279, 1, NULL),
(599, 'สีธีม', 'ดำ', NULL, '2022-03-26 04:09:38', 279, 1, NULL),
(600, 'สีธีม', 'ขาว', NULL, '2022-03-26 04:11:18', 279, 1, NULL),
(601, 'สีธีม', 'ดำ', NULL, '2022-03-26 04:12:53', 279, 1, NULL),
(602, 'สีธีม', 'ขาว', NULL, '2022-03-26 04:14:04', 279, 1, NULL),
(603, 'สีธีม', 'ดำ', NULL, '2022-03-26 04:14:19', 279, 1, NULL),
(604, 'สีธีม', 'ขาว', NULL, '2022-03-26 04:15:29', 279, 1, NULL),
(605, 'ตำเเหน่ง', 'ด้านล่าง', NULL, '2022-03-26 04:15:29', 279, 1, NULL),
(606, 'สีธีม', 'ขาว', NULL, '2022-03-26 04:23:27', 278, 1, NULL),
(607, 'ตำเเหน่ง', 'ตรงกลาง', NULL, '2022-03-26 04:23:27', 278, 1, NULL),
(608, 'เพิ่มโดเมน', 'https://www.Test.com', NULL, '2022-03-26 04:30:24', NULL, 1, NULL),
(609, 'ลบโดเมน', 'https://www.Test.com', NULL, '2022-03-26 04:39:30', NULL, 1, NULL),
(610, 'เพิ่มโดเมน', 'https://www.TEST.com', NULL, '2022-03-26 04:39:52', NULL, 1, NULL),
(611, 'เพิ่มโดเมน', 'https://www.test.com', NULL, '2022-03-26 04:42:16', NULL, 1, NULL),
(612, 'เพิ่มโดเมน', 'https://www.test.com', NULL, '2022-03-26 04:43:33', NULL, 1, NULL),
(613, 'เพิ่มโดเมน', 'https://www.test.com', NULL, '2022-03-26 04:44:46', NULL, 1, NULL),
(614, 'เพิ่มโดเมน', 'https://www.test.com', NULL, '2022-03-26 04:46:07', NULL, 1, NULL),
(615, 'เพิ่มโดเมน', 'https://www.test.com', NULL, '2022-03-26 04:47:49', NULL, 1, NULL),
(616, 'เพิ่มโดเมน', 'https://www.testss.com', NULL, '2022-03-26 04:50:33', NULL, 1, NULL),
(617, 'เพิ่มโดเมน', 'https://www.test.com', NULL, '2022-03-26 04:54:47', NULL, 1, NULL),
(618, 'เพิ่มโดเมน', 'https://www.testssssss.com', NULL, '2022-03-26 04:56:07', NULL, 1, NULL),
(619, 'เพิ่มโดเมน', 'https://www.wit.com', NULL, '2022-03-26 04:57:02', NULL, 1, NULL),
(620, 'เพิ่มโดเมน', 'https://www.dddd.com', NULL, '2022-03-26 04:59:12', NULL, 1, NULL),
(621, 'เพิ่มโดเมน', 'https://www.dfgdfgdfg.com', NULL, '2022-03-26 05:05:24', NULL, 1, NULL),
(622, 'เพิ่มโดเมน', 'https://www.sdsfsd.com', NULL, '2022-03-26 05:06:02', NULL, 1, NULL),
(623, 'เพิ่มโดเมน', 'https://www.comsdf.com', NULL, '2022-03-26 05:07:06', NULL, 1, NULL),
(624, 'เพิ่มโดเมน', 'https://www.comssss.com', NULL, '2022-03-26 05:09:33', NULL, 1, NULL),
(625, 'เพิ่มโดเมน', 'https://www.vvvvv.com', NULL, '2022-03-26 05:11:10', NULL, 1, NULL),
(626, 'เพิ่มโดเมน', 'https://www.coo.com', NULL, '2022-03-26 05:12:22', NULL, 1, NULL),
(627, 'เพิ่มโดเมน', 'https://www.vvvvvv.com', NULL, '2022-03-26 05:13:49', NULL, 1, NULL),
(628, 'ลบโดเมน', 'https://www.coo.com', NULL, '2022-03-26 05:20:41', NULL, 1, NULL),
(629, 'ลบโดเมน', 'https://www.comssss.com', NULL, '2022-03-26 05:20:48', NULL, 1, NULL),
(630, 'เพิ่มโดเมน', 'https://www.TEST.com', NULL, '2022-03-26 05:22:38', NULL, 1, NULL),
(631, 'สีธีม', 'ขาว', NULL, '2022-03-26 07:06:19', 301, 1, NULL),
(632, 'สีธีม', 'ดำ', NULL, '2022-03-26 07:24:42', 301, 1, NULL),
(633, 'สีธีม', 'ขาว', NULL, '2022-03-26 13:24:15', 301, 1, NULL),
(634, 'สีธีม', 'ดำ', NULL, '2022-03-27 05:16:20', 301, 1, NULL),
(635, 'เพิ่มประเภทคุกกี้', '45453', NULL, '2022-03-28 07:57:25', NULL, 1, NULL),
(636, 'ลบข้อมูลประเภทคุกกี้', NULL, NULL, '2022-03-28 07:57:30', NULL, 1, NULL),
(637, 'เพิ่มโดเมน', 'https://www.sgc.co.th', NULL, '2022-03-28 19:37:03', NULL, 1, NULL),
(638, 'ลบโดเมน', 'https://www.sgc.co.th', NULL, '2022-03-30 22:36:45', NULL, 1, NULL),
(639, 'เพิ่มโดเมน', 'https://buildingware.cf', NULL, '2022-03-30 22:47:10', NULL, 1, NULL),
(642, 'สีธีม', 'ดำ', NULL, '2022-03-30 22:52:20', 278, 1, NULL),
(643, 'เพิ่มโดเมน', 'https://www.buildingware.cf', NULL, '2022-03-30 22:55:51', NULL, 1, NULL),
(644, 'เพิ่มโดเมน', 'https://www.tesssssssssss.com', NULL, '2022-03-31 23:07:46', NULL, 1, NULL),
(645, 'เเก้ไขโดเมน', 'https://www.Tesssssssssss.com', NULL, '2022-04-01 03:57:24', NULL, 1, NULL),
(665, 'สร้าง Tag', '[\"sdfsdf\",\"xcxcv\"]', NULL, '2022-04-07 01:23:57', NULL, 1, NULL),
(666, 'สร้าง Tag', '[\"555\"]', NULL, '2022-04-07 02:06:27', NULL, 1, NULL),
(667, 'สร้าง Tag', '[\"tset\"]', NULL, '2022-04-07 04:33:18', NULL, 1, NULL),
(668, 'สร้าง Tag', '[\"ee\"]', NULL, '2022-04-07 04:35:52', NULL, 1, NULL),
(669, 'สร้าง Tag', '[\"t\",\"d\",\"s\"]', NULL, '2022-04-07 04:35:59', NULL, 1, NULL),
(670, 'สร้าง Tag', '[\"สำคัญ\",\"สำคัญมาก\",\"โครตสำคัญ\"]', NULL, '2022-04-07 04:38:57', NULL, 1, NULL),
(671, 'สร้าง Tag', '[\"สำคัญ\",\"สำคัญมาก\",\"สำคัญโครต\"]', NULL, '2022-04-07 14:09:20', NULL, 1, NULL),
(672, 'เพิ่มโดเมน', 'https://www.witttttttttt.com', NULL, '2022-04-08 01:27:40', NULL, 1, NULL),
(673, 'เพิ่มโดเมน', 'https://www.wwwwwwwwwwww.com', NULL, '2022-04-08 01:48:10', NULL, 1, NULL),
(674, 'สีธีม', 'ขาว', NULL, '2022-04-08 02:09:05', 307, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `muitifactor`
--

CREATE TABLE `muitifactor` (
  `acc_id` int(11) NOT NULL,
  `otp_sms` tinyint(1) NOT NULL,
  `otp_email` tinyint(1) NOT NULL,
  `otp_2fa` tinyint(1) NOT NULL,
  `otp_login` tinyint(1) NOT NULL,
  `otp_system` tinyint(1) NOT NULL,
  `policy` tinyint(1) NOT NULL,
  `m_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `muitifactor`
--

INSERT INTO `muitifactor` (`acc_id`, `otp_sms`, `otp_email`, `otp_2fa`, `otp_login`, `otp_system`, `policy`, `m_id`) VALUES
(175, 0, 0, 1, 1, 1, 1, 18),
(176, 0, 0, 1, 1, 1, 1, 22),
(177, 0, 0, 1, 1, 1, 1, 23),
(178, 0, 0, 1, 1, 1, 1, 24);

-- --------------------------------------------------------

--
-- Table structure for table `pdpa_appeal`
--

CREATE TABLE `pdpa_appeal` (
  `id_ap` int(11) NOT NULL,
  `appeal_prefix` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `appeal_firstname` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `appeal_lastname` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `appeal_address` varchar(5000) COLLATE utf8_bin DEFAULT NULL,
  `appeal_detail` varchar(5000) COLLATE utf8_bin DEFAULT NULL,
  `appeal_date` datetime DEFAULT current_timestamp(),
  `appeal_date_approve` datetime DEFAULT NULL,
  `appeal_contact` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `appeal_share` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `appeal_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `appeal_detail_summary` varchar(5000) COLLATE utf8_bin DEFAULT NULL,
  `appeal_revoke` tinyint(4) DEFAULT NULL,
  `appeal_access` tinyint(4) DEFAULT NULL,
  `appeal_edit_information` tinyint(4) DEFAULT NULL,
  `appeal_delet_information` tinyint(4) DEFAULT NULL,
  `appeal_suspend` tinyint(4) DEFAULT NULL,
  `appeal_transfer` tinyint(4) DEFAULT NULL,
  `appeal_oppose` tinyint(4) DEFAULT NULL,
  `appeal_decision` tinyint(4) DEFAULT NULL,
  `appeal_approved_complaint` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pdpa_appeal`
--

INSERT INTO `pdpa_appeal` (`id_ap`, `appeal_prefix`, `appeal_firstname`, `appeal_lastname`, `appeal_address`, `appeal_detail`, `appeal_date`, `appeal_date_approve`, `appeal_contact`, `appeal_share`, `appeal_email`, `appeal_detail_summary`, `appeal_revoke`, `appeal_access`, `appeal_edit_information`, `appeal_delet_information`, `appeal_suspend`, `appeal_transfer`, `appeal_oppose`, `appeal_decision`, `appeal_approved_complaint`) VALUES
(77, 'นาย', 'วิทยา', 'เกสรกติกา', '28/ช', 'ระบบยังไม่เสร็จดีเลย', '2022-04-02 00:00:00', '2022-04-02 23:06:39', '้611998018@crru.ac.th', NULL, '้611998018@crru.ac.th', '', 1, NULL, 1, NULL, NULL, NULL, 1, NULL, 1),
(78, 'นาง', 'AA', 'AA', '284', 'รายละเอียดร้องเรียนAAAAAAAAAA', '2022-04-03 00:00:00', '2022-04-02 23:13:56', '611998018@AAA', NULL, '611998018@AAA', '', 1, 1, 1, 1, NULL, NULL, 1, 1, 2),
(79, 'นาย', 'BB', 'BB', '28BB', 'รายละเอียดร้องเรียนBBBB', '2022-04-02 00:00:00', '2022-04-02 23:59:19', 'BBBB@BBBB', NULL, 'BBBB@BBBB', '', 1, NULL, 1, NULL, NULL, NULL, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pdpa_email`
--

CREATE TABLE `pdpa_email` (
  `id_email` int(11) NOT NULL,
  `email_from` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email_status` int(11) DEFAULT NULL,
  `email_content` varchar(10000) COLLATE utf8_bin DEFAULT NULL,
  `email_subject` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email_to` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email_date_send` datetime DEFAULT current_timestamp(),
  `email_date_consent` datetime DEFAULT NULL,
  `email_files` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `email_files_csv` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pdpa_email`
--

INSERT INTO `pdpa_email` (`id_email`, `email_from`, `email_status`, `email_content`, `email_subject`, `email_to`, `email_date_send`, `email_date_consent`, `email_files`, `email_files_csv`) VALUES
(358, NULL, 0, 'html_send', 'html_send', '611998018@crru.ac.th', '2022-03-18 23:31:55', NULL, NULL, 'a7eadcf5-502b-4d93-8013-60507c3be253_Email.csv'),
(359, NULL, 1, 'html_send', 'html_send', 'witthaya611998018@gmail.com', '2022-03-18 23:31:55', '2022-03-18 02:48:50', NULL, 'a7eadcf5-502b-4d93-8013-60507c3be253_Email.csv'),
(365, NULL, 2, 'ิิิิิิิิิิิิิิิิแแแแแแแแแแแแแแแแแแแ', 'ิิิแแแแแแแ', 'witthaya611998018@gmail.com', '2022-03-19 08:47:06', '2022-03-18 02:48:50', NULL, NULL),
(366, NULL, 1, '้่หหหหหหหหหหหหหหหหหหหหหหหห', 'หหหหหหหหห', 'witthaya611998018@gmail.com', '2022-03-19 08:53:34', '2022-03-20 02:49:21', NULL, NULL),
(367, NULL, 0, 'test', 'test', 'haam2543wittaya@gmail.com', '2022-03-19 09:48:40', NULL, NULL, 'e3a9911b-c4d1-4241-8a34-885e6e83fb4b_Email.csv'),
(368, NULL, 0, 'test', 'test', '611998018@crru.ac.th', '2022-03-19 09:48:40', NULL, NULL, 'e3a9911b-c4d1-4241-8a34-885e6e83fb4b_Email.csv'),
(369, NULL, 1, 'test', 'test', 'witthaya611998018@gmail.com', '2022-03-19 09:48:40', '2022-03-20 02:49:21', NULL, 'e3a9911b-c4d1-4241-8a34-885e6e83fb4b_Email.csv'),
(370, NULL, 0, 'ก', 'ก', 'haam2543wittaya@gmail.com', '2022-03-21 10:56:04', NULL, NULL, '0fd078dc-372c-4cd3-abfe-5858b9dd2b8a_Email.csv'),
(382, NULL, 1, 'xxxxxxxxxxxxxxxxxxxxxx', 'x', 'witthaya611998018@gmail.com', '2022-03-21 11:13:21', '2022-03-20 02:49:21', NULL, '4e003bb1-452a-423b-83f9-67a58d5af1df_Email.csv'),
(383, NULL, 1, 'd', 'd', 'haam2543wittaya@gmail.com', '2022-03-21 11:14:16', '2022-03-20 02:49:21', NULL, '8e465e29-0cb8-4799-b587-e41843183b2f_Email.csv'),
(384, NULL, 1, 'd', 'd', '611998018@crru.ac.th', '2022-03-21 11:14:16', '2022-03-20 02:49:21', NULL, '8e465e29-0cb8-4799-b587-e41843183b2f_Email.csv'),
(385, NULL, 1, 'd', 'd', 'witthaya611998018@gmail.com', '2022-03-21 11:14:16', '2022-03-20 02:49:21', NULL, '8e465e29-0cb8-4799-b587-e41843183b2f_Email.csv'),
(386, NULL, 1, 'dddddddddddddd', 'd', 'haam2543wittaya@gmail.com', '2022-03-21 11:15:25', '2022-03-20 02:49:21', NULL, '1328f9c4-7936-47e8-90af-68c83d9f0ec9_Email.csv'),
(387, NULL, 1, 'dddddddddddddd', 'd', '611998018@crru.ac.th', '2022-03-21 11:15:25', '2022-03-20 02:49:21', NULL, '1328f9c4-7936-47e8-90af-68c83d9f0ec9_Email.csv'),
(388, NULL, 1, 'ddddddddddddddd', 'ddddd', 'haam2543wittaya@gmail.com', '2022-03-21 11:17:42', '2022-03-20 02:49:21', NULL, '3d837349-f3e8-4076-ab96-712de263491c_Email.csv'),
(389, NULL, 1, 'ddddddddddddddd', 'ddddd', '611998018@crru.ac.th', '2022-03-21 11:17:42', '2022-03-21 02:49:21', NULL, '3d837349-f3e8-4076-ab96-712de263491c_Email.csv'),
(390, NULL, 1, 'dd', 's', 'haam2543wittaya@gmail.com', '2022-03-21 11:18:40', '2022-03-23 02:49:21', NULL, 'db406e00-e263-42d1-9f5c-8eecf749bca9_Email.csv'),
(391, NULL, 1, 'dd', 's', '611998018@crru.ac.th', '2022-03-21 11:18:40', '2022-03-23 02:49:21', NULL, 'db406e00-e263-42d1-9f5c-8eecf749bca9_Email.csv'),
(392, NULL, 1, 'dddddddddddddd', 'ddd', 'haam2543wittaya@gmail.com', '2022-03-21 11:19:14', '2022-03-23 02:49:21', NULL, 'f7fa1c6f-4244-4ca7-8723-093a822377c4_Email.csv'),
(393, NULL, 1, 'dddddddddddddd', 'ddd', '611998018@crru.ac.th', '2022-03-21 11:19:14', '2022-03-23 02:49:21', NULL, 'f7fa1c6f-4244-4ca7-8723-093a822377c4_Email.csv'),
(394, NULL, 1, '555', '555', 'haam2543wittaya@gmail.com', '2022-03-21 11:24:28', '2022-03-23 02:49:21', NULL, '97a07fc2-5f50-4c6e-b428-c7e02aa5282d_Email.csv'),
(395, NULL, 1, '555', '555', '611998018@crru.ac.th', '2022-03-21 11:24:28', '2022-03-23 02:49:21', NULL, '97a07fc2-5f50-4c6e-b428-c7e02aa5282d_Email.csv'),
(396, NULL, 0, '', 'ทดลองล่าสุด', 'haam2543wittaya@gmail.com', '2022-03-30 03:52:11', NULL, NULL, 'd4daed91-b514-4c83-a39e-d4724441643b_Email.csv'),
(397, NULL, 0, '', 'ทดลองล่าสุด', '611998018@crru.ac.th', '2022-03-30 03:52:11', NULL, NULL, 'd4daed91-b514-4c83-a39e-d4724441643b_Email.csv'),
(398, NULL, 0, 'ทดสอบล่าสุด', 'ทดสอบล่าสุด', 'haam2543wittaya@gmail.com', '2022-03-30 03:54:32', NULL, NULL, '090b190e-4b64-49bf-91e9-7918995e8e41_Email.csv'),
(399, NULL, 0, 'ทดสอบล่าสุด', 'ทดสอบล่าสุด', '611998018@crru.ac.th', '2022-03-30 03:54:32', NULL, NULL, '090b190e-4b64-49bf-91e9-7918995e8e41_Email.csv'),
(400, NULL, 0, 'ทดสอบล่าสุด', 'ทดสอบล่าสุด', 'haam2543wittaya@gmail.com', '2022-03-30 03:55:37', NULL, NULL, 'd3090a88-a182-4bc1-8318-cbda9faa17f3_Email.csv'),
(401, NULL, 2, 'ทดสอบล่าสุด', 'ทดสอบล่าสุด', '611998018@crru.ac.th', '2022-03-30 03:55:37', '2022-03-30 04:10:22', NULL, 'd3090a88-a182-4bc1-8318-cbda9faa17f3_Email.csv'),
(402, NULL, 2, 'กกกกกกกกกกกกกกกกกกกกกกกก', 'กกกกกก', '611998018@crru.ac.th', '2022-03-30 03:57:26', '2022-04-01 23:04:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pdpa_log_setting_cookiepolicy`
--

CREATE TABLE `pdpa_log_setting_cookiepolicy` (
  `id_lsc` int(11) NOT NULL,
  `policy_id` int(11) DEFAULT NULL,
  `domaingroup_id` int(11) DEFAULT NULL,
  `cookiepolicy` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pdpa_log_setting_cookiepolicy`
--

INSERT INTO `pdpa_log_setting_cookiepolicy` (`id_lsc`, `policy_id`, `domaingroup_id`, `cookiepolicy`) VALUES
(1, 462, 279, '01010'),
(3, 463, 278, '00000'),
(4, 506, 301, '00000'),
(6, 542, 305, '01001'),
(7, 550, 307, '11111');

-- --------------------------------------------------------

--
-- Table structure for table `pdpa_policylog`
--

CREATE TABLE `pdpa_policylog` (
  `id_policylog` int(11) NOT NULL,
  `policylog_ip` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `policylog_sessionid` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `policylog_browser` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `policylog_date` datetime DEFAULT current_timestamp(),
  `policylog_cookieconsent` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `domaingroup_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pdpa_policylog`
--

INSERT INTO `pdpa_policylog` (`id_policylog`, `policylog_ip`, `policylog_sessionid`, `policylog_browser`, `policylog_date`, `policylog_cookieconsent`, `domaingroup_id`) VALUES
(2, '192.168.2.43', 'Fd23ApDcIR4yhAoy6IyjagXquXnJUlD5', 'chrome', '2022-03-20 20:07:08', 'ปฏิเสธ', 279),
(150, '192.168.2.43', 'Fd23ApDcIR4yhAoy6IyjagXquXnJUlD6', 'chrome', '2022-03-20 20:07:08', 'ปฏิเสธ', 278),
(151, '192.168.2.33', 'JRpoN7ye3YDcmw-Bn2itNzxxQ6AS8ZN7', 'chrome', '2022-03-20 22:31:36', 'ปฏิเสธ', 279),
(152, '192.168.2.33', 'oxQgRfWb3qs-P0u48osPZQ4KCv0EL0KB', 'chrome', '2022-03-20 23:20:18', 'ปฏิเสธ', 279),
(153, '192.168.2.33', 'sBNKRU2B-TKpf5dTpdjOIHHADSjLq9SV', 'chrome', '2022-03-20 23:29:37', 'ปฏิเสธ', 279),
(154, '192.168.2.33', 'j-LvLm-xfeR-EkIuJHIvq1BH1VAbWTIJ', 'chrome', '2022-03-20 23:47:06', 'ปฏิเสธ', 279),
(155, '192.168.2.33', 'Yaf7p6x4aYdhmdmydX0RFTaOvbdy96hF', 'chrome', '2022-03-20 23:49:04', 'ปฏิเสธ', 279),
(156, '192.168.2.33', 'Ur3Jjkk-682asbcBAYxX_iX02asUjIZR', 'chrome', '2022-03-20 23:53:22', 'ปฏิเสธ', 279),
(157, '192.168.2.33', 'cinvc2YZofKuGBROmfjxOxD6LxLRyHBP', 'chrome', '2022-03-20 23:54:08', 'ปฏิเสธ', 279),
(158, '192.168.2.33', 'i6j6NpAfCUKBYllWIJI-edwVFwqbpwkK', 'chrome', '2022-03-20 23:54:13', 'ปฏิเสธ', 279),
(159, '192.168.2.33', 'OVPGU55d72j0lAwaQQqmrbsu5Gsa34mI', 'chrome', '2022-03-20 23:54:27', 'ปฏิเสธ', 279),
(160, '192.168.2.33', '_B-vYcQ2oxy-gtpSi7tAEZDHHOJr3Avf', 'chrome', '2022-03-20 23:54:43', 'ปฏิเสธ', 279),
(161, '192.168.2.33', 'lCzMXenc7nRCcoL1Tkz3YhjsRUrfZuZG', 'chrome', '2022-03-21 00:00:53', 'ปฏิเสธ', 279),
(162, '192.168.2.33', 'fbDafbYqD_V3D8n7BaLMebps4GP_y4gR', 'chrome', '2022-03-21 00:03:06', 'ปฏิเสธ', 279),
(163, '192.168.2.33', 'pccHoo6srrxGHSgedpWVcXO8bbbIfucw', 'chrome', '2022-03-21 00:10:40', 'ปฏิเสธ', 279),
(164, '192.168.2.33', '2Lwd3dXBbCsmWg8G-mIjhWxxEhw1wF3o', 'chrome', '2022-03-21 00:11:17', 'ปฏิเสธ', 279),
(165, '192.168.2.33', '85Dmnu4CtZZN-FUWF1zxSPhmElSzdMvN', 'chrome', '2022-03-21 00:11:48', 'ปฏิเสธ', 279),
(166, '192.168.2.33', 'qafD-bc7X1OB6QKtHjuIqWgpb036zsOo', 'chrome', '2022-03-21 00:13:28', 'ปฏิเสธ', 279),
(167, '192.168.2.33', 'ta97f3vIXTsDZg8-bY65nS9KrUyJ2eMe', 'chrome', '2022-03-21 00:13:39', 'ปฏิเสธ', 279),
(168, '192.168.2.33', 'StISz3CQ9WvnQfQZuVCdAR77X-46yDze', 'chrome', '2022-03-21 00:15:38', 'ปฏิเสธ', 279),
(169, '192.168.2.33', 'r0zGSbFu_EmMZmY6ejysZmW1duT-fQtD', 'chrome', '2022-03-21 00:16:19', 'ปฏิเสธ', 279),
(170, '192.168.2.33', 'NKUaYUMIOvdnwmLg1rrjd8tcLQeDfJY8', 'chrome', '2022-03-21 00:17:28', 'ปฏิเสธ', 279),
(171, '192.168.2.33', '4Dj_oyijQGXHZbKI-s75bAENb-vYB4-Q', 'chrome', '2022-03-21 00:17:40', 'ปฏิเสธ', 279),
(172, '192.168.2.33', 'XE3FSTUS7-cotZvD2aGde92Ep0P3uTcT', 'chrome', '2022-03-21 00:19:46', 'ปฏิเสธ', 279),
(173, '192.168.2.33', '9WyceD2Y7nL88YD529vwlNDQlo9Irwki', 'chrome', '2022-03-21 00:20:24', 'ปฏิเสธ', 279),
(174, '192.168.2.33', 'eV-tbibOlinfjx3F-i31gFjA8nw5IHW5', 'chrome', '2022-03-21 00:22:18', 'ปฏิเสธ', 279),
(175, '192.168.2.33', 'r6Lmz3ZuUhWNu8h4HxomkQahPTJSoKPk', 'chrome', '2022-03-21 00:22:24', 'ปฏิเสธ', 279),
(176, '192.168.2.33', 'vGEs6P70uQXiBiUxXjzNQ-7B_Y2_1tSl', 'chrome', '2022-03-21 00:22:42', 'ปฏิเสธ', 279),
(177, '192.168.2.33', 'GGUYFk-IcczkeOpCiF_I54_DmFoGTAR0', 'chrome', '2022-03-21 00:22:49', 'ปฏิเสธ', 279),
(178, '192.168.2.33', 'QwUx0omE_74w_8d9jKDIeETvr411iM25', 'chrome', '2022-03-21 00:23:31', 'ปฏิเสธ', 279),
(179, '192.168.2.33', 'C0MlX9_Z0Nj-7bG3D8M24Z6YrVr26yI3', 'chrome', '2022-03-21 00:23:48', 'ปฏิเสธ', 279),
(180, '192.168.2.33', '2wJpjWBJmjy7fU7Uvc4p5jCIiLwXp3ST', 'chrome', '2022-03-21 00:24:15', 'ปฏิเสธ', 279),
(181, '192.168.2.33', 'Zof6d9r25ufLbNB-Ru3iKOhZ6oGipqOI', 'chrome', '2022-03-21 00:24:46', 'อนุญาตตามที่เลือก', 279),
(182, '192.168.2.33', 'njfutEl9dGLj8a0ae0bf8afe3Ms9HpN-', 'chrome', '2022-03-21 00:25:08', 'อนุญาตตามที่เลือก', 279),
(183, '192.168.2.33', 'GmTHAvqIr81E0VmnnuW3xutY98viJsGN', 'chrome', '2022-03-21 00:26:48', 'อนุญาตตามที่เลือก', 278),
(184, '192.168.2.33', 'fEx_zbi5ExGGPBjqDJbUHn8DLD5O1Cj7', 'chrome', '2022-03-21 00:27:06', 'อนุญาตตามที่เลือก', 278),
(185, '192.168.2.33', 'vTr-_r5xmZwub9HWnH5mkc5ygA4RvwwT', 'chrome', '2022-03-21 00:27:57', 'อนุญาตตามที่เลือก', 278),
(186, '192.168.2.33', 'vqUMiHaSKETNgdw5KT8UKVix72rD3JJe', 'chrome', '2022-03-21 00:28:51', 'อนุญาตตามที่เลือก', 279),
(187, '192.168.2.33', 'h7r1WQcguaTk2g7GwDkTx7AY3zdzfao_', 'chrome', '2022-03-21 00:29:10', 'อนุญาตตามที่เลือก', 279),
(188, '192.168.2.33', 'UbW2TqP5_Ui5SJFVZhl4-54UZUdX4NIr', 'chrome', '2022-03-21 00:29:27', 'อนุญาตตามที่เลือก', 279),
(189, '192.168.2.33', 'CsjkYV6fVMUg5jFRSFk9YEbyTrxk37fQ', 'chrome', '2022-03-21 00:29:32', 'อนุญาตตามที่เลือก', 279),
(190, '192.168.2.33', 'REF44yq9AsXkSyhjz3R8ixNqnt9cfLDs', 'chrome', '2022-03-21 00:30:02', 'อนุญาตตามที่เลือก', 278),
(191, '192.168.2.33', 'R7K2C_zNP8oLoSF46gatGthXhSykitpC', 'chrome', '2022-03-21 00:30:34', 'อนุญาตตามที่เลือก', 278),
(192, '192.168.2.33', 'OgWiwKHtid9NC4vYgcm__h_Tuh0x10Xn', 'chrome', '2022-03-21 00:30:40', 'อนุญาตตามที่เลือก', 278),
(193, '192.168.2.33', 'JPTWuKcXDcbPlaNq0IUMiTMWl1HzXvgF', 'chrome', '2022-03-21 00:31:38', 'อนุญาตตามที่เลือก', 278),
(194, '192.168.2.33', 'NM13X_X_G7t7ostb7W-jnlvQCL44Wje2', 'chrome', '2022-03-21 00:31:38', 'อนุญาตตามที่เลือก', 278),
(195, '192.168.2.33', '3c2ptZI5r2lh9s5cqdtXW9ZrFZA0WwVX', 'chrome', '2022-03-21 00:31:57', 'อนุญาตตามที่เลือก', 278),
(196, '192.168.2.33', 'QYa_3WWv_a-LN08DRF_u_Nx8ZZYsiDhR', 'chrome', '2022-03-21 00:32:04', 'อนุญาตตามที่เลือก', 278),
(197, '192.168.2.33', 'WUg-p4I4OP9YrJx9tpryjVD-sXlemuqL', 'chrome', '2022-03-21 00:33:22', 'อนุญาตตามที่เลือก', 278),
(198, '192.168.2.33', 'ZEBb_CWhwhyHN1jUoS_qQ7Vul-WUflcX', 'chrome', '2022-03-21 00:39:58', 'อนุญาตตามที่เลือก', 279),
(199, '192.168.2.33', 'T3rlJvEx0N1EBhVa13vb2A983uhIMaHn', 'chrome', '2022-03-21 00:40:13', 'อนุญาตตามที่เลือก', 279),
(200, '192.168.2.33', 'gxhqa_heZZeK6o1Z7qZKJyDv3Qc3m9Q-', 'chrome', '2022-03-21 00:40:47', 'อนุญาตตามที่เลือก', 279),
(201, '192.168.2.33', 'ltnMi_ALdVZKGL2Olp81M_BpoePJSsTA', 'chrome', '2022-03-21 00:41:32', 'อนุญาตตามที่เลือก', 279),
(202, '192.168.2.33', 'dyFup-t7KhRzIu4CpLEVhuzS_sOxKJ4Z', 'chrome', '2022-03-21 00:42:05', 'อนุญาตตามที่เลือก', 279),
(203, '192.168.2.33', 'FYN3Fp-g-9ulsvE-d9YCfxPccveCoHe7', 'chrome', '2022-03-21 00:42:23', 'อนุญาตตามที่เลือก', 279),
(204, '192.168.2.33', 'lxtUd1HyiprwFuAUJA00pMTnqyLK9_Aa', 'chrome', '2022-03-21 00:46:55', 'อนุญาตตามที่เลือก', 279),
(205, '192.168.2.33', 'g5LDzm9RhsgiZEcqrRr4P2SnzA1zQS07', 'chrome', '2022-03-21 00:46:55', 'อนุญาตตามที่เลือก', 279),
(206, '192.168.2.33', 'Vnw00rwhvw9Iw3LHLB7V_Csw-0Io1OxU', 'chrome', '2022-03-21 00:47:07', 'อนุญาตตามที่เลือก', 279),
(207, '192.168.2.33', 'D0Z944a3MWZVMcwFB4iMwi0BmXOoyJip', 'chrome', '2022-03-21 00:47:17', 'อนุญาตตามที่เลือก', 279),
(208, '192.168.2.33', 'jdiUAVuvh2kp942Tw4hTPYMD6KdYumn9', 'chrome', '2022-03-21 00:47:29', 'อนุญาตตามที่เลือก', 278),
(209, '192.168.2.33', 'RtiP2rP2W9zJ3lrHtvpTEZNgrFaED0oC', 'chrome', '2022-03-21 00:47:43', 'อนุญาตตามที่เลือก', 278),
(210, '192.168.2.33', 'bw0cmRAkNobTF9ehgcpwdu4I9l5ZFNvF', 'chrome', '2022-03-21 00:48:25', 'อนุญาตตามที่เลือก', 278),
(211, '192.168.2.33', 'AiHclx3-mri61EHOBhlkPdM8rU1uQEzx', 'chrome', '2022-03-21 00:48:40', 'อนุญาตตามที่เลือก', 278),
(212, '192.168.2.33', 'yV-DBrKbiU1r6SRdChxaOB6Cbpwy1Ldq', 'chrome', '2022-03-21 00:49:29', 'อนุญาตตามที่เลือก', 278),
(213, '192.168.2.33', 'Dlg-tRYM-9XvMHYNPl5LWjvHJUMQ_AGl', 'chrome', '2022-03-21 00:54:45', 'อนุญาตตามที่เลือก', 278),
(214, '192.168.2.33', '4BIlomWUYxBh_sts02HYlpNfO1JYFeGj', 'chrome', '2022-03-21 00:55:04', 'อนุญาตตามที่เลือก', 278),
(215, '192.168.2.33', 'guBvhuv5oniVx2akq3LsBQn4tFwITpIR', 'chrome', '2022-03-21 00:55:09', 'อนุญาตตามที่เลือก', 278),
(216, '192.168.2.33', 'oYzctUl8W1VQv2AZgoYbbq_aZETbKKBg', 'chrome', '2022-03-21 00:55:52', 'อนุญาตตามที่เลือก', 278),
(217, '192.168.2.33', 'o0Wui1_sNp7UuuVo32v12eDXPOTNxDeJ', 'chrome', '2022-03-21 00:56:25', 'อนุญาตตามที่เลือก', 278),
(218, '192.168.2.33', 'v3X0un25JEnHDiwQVO2FUym9mjYCgpj3', 'chrome', '2022-03-21 00:56:27', 'อนุญาตตามที่เลือก', 278),
(219, '192.168.2.33', 'ew9saCkZzVc8y2Att9zU33dmrD55l_db', 'chrome', '2022-03-21 00:56:54', 'อนุญาตตามที่เลือก', 278),
(220, '192.168.2.33', 'V2Jc_twCic09wwJlgCmne5Hwni_0Z71l', 'chrome', '2022-03-21 00:57:28', 'อนุญาตตามที่เลือก', 278),
(221, '192.168.2.33', 'zlX4APtj0rYPb4PhxGWaVtq3ER2UycIl', 'chrome', '2022-03-21 00:57:34', 'อนุญาตตามที่เลือก', 278),
(222, '192.168.2.33', 'Tibgms34PQF9jwPCXC8arrjkjTcW8Oaz', 'chrome', '2022-03-21 00:57:46', 'อนุญาตตามที่เลือก', 278),
(223, '192.168.2.33', 'c6Lzy9hUlBx7IPFm8SajwFrP8hlfz9Lj', 'chrome', '2022-03-21 00:57:51', 'อนุญาตตามที่เลือก', 278),
(224, '192.168.2.33', 'GJjqO9KFG9t1QNX5BO1kLvq-BuTPR0Ap', 'chrome', '2022-03-21 00:58:23', 'อนุญาตตามที่เลือก', 278),
(225, '192.168.2.33', 'pe7h9qjvIal39VkXHJc5PVqvXAJU5O6q', 'chrome', '2022-03-21 00:58:32', 'อนุญาตตามที่เลือก', 278),
(226, '192.168.2.33', '17Gui2WSDNKqewBetk2f3v2b6ESS9XJz', 'chrome', '2022-03-21 00:59:41', 'อนุญาตตามที่เลือก', 278),
(227, '192.168.2.33', 'ne4URaI01ftgjwZv1KmcvNtt8WU3NARt', 'chrome', '2022-03-21 00:59:47', 'อนุญาตตามที่เลือก', 278),
(228, '192.168.2.33', 'f2OMBwAiwms1XQ-9Rfql6yaxCv3tMn8V', 'chrome', '2022-03-21 00:59:56', 'อนุญาตตามที่เลือก', 279),
(229, '192.168.2.33', 'Cx8rvHRDjc216-xB0-QNGKY-qTf68lpO', 'chrome', '2022-03-21 01:00:01', 'อนุญาตตามที่เลือก', 279),
(230, '192.168.2.33', 'bmsQjSJnRv2SA1YyHyAT-Hxh0v4iC7XI', 'chrome', '2022-03-21 01:01:19', 'อนุญาตตามที่เลือก', 279),
(231, '192.168.2.33', '9tYuxFsSPccqnMGDLoFmTH0OEiKzuHVX', 'chrome', '2022-03-21 01:01:36', 'อนุญาตตามที่เลือก', 279),
(232, '192.168.2.33', 'gnODEbEjWfKNTtjrRygoXB6S_K66QrwI', 'chrome', '2022-03-21 01:01:41', 'อนุญาตตามที่เลือก', 279),
(233, '192.168.2.33', 'tNtnpk-Tr8OD7yIlO2D8e0Y0x4cqKa0A', 'chrome', '2022-03-21 01:03:37', 'อนุญาตตามที่เลือก', 279),
(234, '192.168.2.33', 'QWG5nubyTqbn0UIVxf4L-GFIiev3iOkx', 'chrome', '2022-03-21 01:04:06', 'อนุญาตตามที่เลือก', 279),
(235, '192.168.2.33', 'hNf2QPZTZjHec2bj04En3C8udGkEHksh', 'chrome', '2022-03-21 01:04:33', 'อนุญาตตามที่เลือก', 279),
(236, '192.168.2.33', 'JguWFoo6HJv7PaCPYaLg8Zpr1l4ksSwW', 'chrome', '2022-03-21 01:04:49', 'อนุญาตตามที่เลือก', 279),
(237, '192.168.2.33', 'GpxwIxwRqQkeCsi1iCjUTQV9TKSQebTF', 'chrome', '2022-03-21 01:06:09', 'อนุญาตตามที่เลือก', 279),
(238, '192.168.2.33', 'JAlc8Pjzym02gPRQvzuPIFD9nISnGfhF', 'chrome', '2022-03-21 01:06:12', 'อนุญาตตามที่เลือก', 279),
(239, '192.168.2.33', 'uzNxsc6_eUj0sypf0l8k_4gvnpPXxwbG', 'chrome', '2022-03-21 01:06:16', 'อนุญาตตามที่เลือก', 279),
(240, '192.168.2.33', 'HNVRxgk4Cfxls5i3hlS8ZBehRmz3JtIy', 'chrome', '2022-03-21 01:06:22', 'อนุญาตตามที่เลือก', 279),
(241, '192.168.2.33', 'rO4wDqRwpU4Wj3cjrT6uQ9HDoH8-L-Eu', 'chrome', '2022-03-21 01:06:37', 'อนุญาตตามที่เลือก', 278),
(242, '192.168.2.33', 'aJmHZCeHpWIPLVUCmtGHxM4HHPuvVjNo', 'chrome', '2022-03-21 01:06:43', 'อนุญาตตามที่เลือก', 278),
(243, '192.168.2.33', 'XtQaADH4irK4069vz35BEipIOFL1ZSt_', 'chrome', '2022-03-21 01:07:33', 'อนุญาตตามที่เลือก', 278),
(244, '192.168.2.33', 'T5pmBJNevMnMg8Ed1zZJkOlCnJ99iIXf', 'chrome', '2022-03-21 01:09:41', 'อนุญาตตามที่เลือก', 278),
(245, '192.168.2.33', '2sx1ZlMB_SrIdJ4_aH9hGnSRLmEZKCKI', 'chrome', '2022-03-21 01:09:58', 'อนุญาตตามที่เลือก', 278),
(246, '192.168.2.33', 'tDFrM8yk7dn8N5dQROkGPhxjkLZbHxIY', 'chrome', '2022-03-21 01:11:56', 'อนุญาตตามที่เลือก', 278),
(247, '192.168.2.33', 'KLXXjbhuJ-MljeAlGsecShQEPO_JawCB', 'chrome', '2022-03-21 01:18:32', 'อนุญาตตามที่เลือก', 278),
(248, '192.168.2.33', 'KCGxrxENESrs9_uqHPabMYRNjarUuhYY', 'chrome', '2022-03-21 01:19:46', 'อนุญาตตามที่เลือก', 278),
(249, '192.168.2.33', 'R_1T8nonGG9R4KJ2jXMpqRIFrJ-Od1l-', 'chrome', '2022-03-21 01:20:04', 'อนุญาตตามที่เลือก', 278),
(250, '192.168.2.33', 'kGqwPPKZg-i33rcduyWMROyqcPJMcQeK', 'chrome', '2022-03-21 01:20:13', 'อนุญาตตามที่เลือก', 278),
(251, '192.168.2.33', 'MPk0YYoQ0Si9kyJ5tkE4T3kf4WE75-3L', 'chrome', '2022-03-21 01:20:56', 'อนุญาตตามที่เลือก', 278),
(252, '192.168.2.33', 'S_bv2mllHIgjjBU53tIwPtkLPPKShQus', 'chrome', '2022-03-21 01:21:26', 'อนุญาตตามที่เลือก', 278),
(253, '192.168.2.33', 'uFr6nJ8cdryLG_IMSLwSmgw1VtgGQVS2', 'chrome', '2022-03-21 01:21:48', 'อนุญาตตามที่เลือก', 279),
(254, '192.168.2.33', '3r8Hkb6Gc9ZDrZ4Wg8Z5WGxFtPTNOAGS', 'chrome', '2022-03-21 01:22:13', 'อนุญาตตามที่เลือก', 279),
(255, '192.168.2.33', '0Hne0aAIEbJ5LZYb7hoYIZ62bbaBJXzp', 'chrome', '2022-03-21 01:22:25', 'อนุญาตตามที่เลือก', 279),
(256, '192.168.2.33', 't3o048Dy_EEKtyBpA8SDJlFD0O1IEmPs', 'chrome', '2022-03-21 01:22:59', 'อนุญาตตามที่เลือก', 279),
(257, '192.168.2.33', 'WhotwsesrS6ZKRmOFNBUEw7V5YgqlmqJ', 'chrome', '2022-03-21 01:27:48', 'อนุญาตตามที่เลือก', 279),
(258, '192.168.2.33', '8qmrKbiuTAD0MBdYhwVPnzJR5CJeJJmG', 'chrome', '2022-03-21 01:27:58', 'อนุญาตตามที่เลือก', 279),
(259, '192.168.2.33', 'ZKKZa4keXCXnm5FcScvN52L3jnFWHoO1', 'chrome', '2022-03-21 01:29:22', 'อนุญาตตามที่เลือก', 279),
(260, '192.168.2.33', '5pivrrguHhUUNsfDHjsn7inEHxFbQPje', 'chrome', '2022-03-21 01:29:25', 'อนุญาตตามที่เลือก', 279),
(261, '192.168.2.33', 'C4v7J-RP5J7N5f_IhbtxI8-IU3C8_5GU', 'chrome', '2022-03-21 01:32:01', 'อนุญาตตามที่เลือก', 279),
(262, '192.168.2.33', 'yAbcwPDL7NwvX2_LWOgCG87L8-EqmxJI', 'chrome', '2022-03-21 01:32:52', 'อนุญาตตามที่เลือก', 279),
(263, '192.168.2.33', 'qIN2i6WOnF6T2Jw0lR8oCbPZy_v9UHKf', 'chrome', '2022-03-21 01:33:05', 'อนุญาตตามที่เลือก', 279),
(264, '192.168.2.33', 'enJYWLNovXgKUNK1JRoLiS8dlCc5bxCO', 'chrome', '2022-03-21 01:33:11', 'อนุญาตตามที่เลือก', 279),
(265, '192.168.2.33', 'IUvWgs1bp-6zrAiWYzeHeASrJdbthMrV', 'chrome', '2022-03-21 01:33:44', 'อนุญาตตามที่เลือก', 279),
(266, '192.168.2.33', 'gDlyvBKW-kmgQ7gZwT_JAzqYtXCdlUWf', 'chrome', '2022-03-21 01:33:48', 'อนุญาตตามที่เลือก', 279),
(267, '192.168.2.33', 'FEUktxmkGmKxDA4nHDg4-EByWYjb4qxQ', 'chrome', '2022-03-21 01:34:01', 'อนุญาตตามที่เลือก', 279),
(268, '192.168.2.33', 'BEWEqi_tIDpqVyAaGrSyCqggb_E4ZQgu', 'chrome', '2022-03-21 01:34:36', 'อนุญาตตามที่เลือก', 279),
(269, '192.168.2.33', 'C38zAAEfmGSmW7Z7kNhNFnbyrSdPLHLh', 'chrome', '2022-03-21 01:34:47', 'อนุญาตตามที่เลือก', 279),
(270, '192.168.2.33', 'xPNx778gOOq6NX5UwHqPmmIulsJTzqvL', 'chrome', '2022-03-21 01:35:50', 'อนุญาตตามที่เลือก', 279),
(271, '192.168.2.33', 'Pa04h28OtDuXS0D0ire_4IaJtX6c16-r', 'chrome', '2022-03-21 01:36:08', 'อนุญาตตามที่เลือก', 279),
(272, '192.168.2.33', '-omeNUouWEneDk8bnRtBRzRWhFVuVgen', 'chrome', '2022-03-21 01:36:31', 'อนุญาตตามที่เลือก', 279),
(273, '192.168.2.33', '74AnZM7ZsAYDmBvnApSOP0WaGVFjH3Pc', 'chrome', '2022-03-21 01:36:31', 'อนุญาตตามที่เลือก', 279),
(274, '192.168.2.33', 'SPXXz__HfWNoscjr15qPf0RmRtQa1mjY', 'chrome', '2022-03-21 01:36:35', 'อนุญาตตามที่เลือก', 279),
(275, '192.168.2.33', 'ppdvYbjdoMIASJPIdlfh_qL7Uiw1CG86', 'chrome', '2022-03-21 01:37:55', 'อนุญาตตามที่เลือก', 279),
(280, '192.168.2.33', '8eVF1aDH-Gl9ZImuMbQ6Zpc0DGvfYEBf', 'chrome', '2022-03-21 02:17:59', 'อนุญาตตามที่เลือก', 278),
(281, '192.168.2.33', 'nmk1TcEsJ5amgsyda_kSYerpYfAOtJA8', 'chrome', '2022-03-21 02:18:10', 'อนุญาตทั้งหมด', 278),
(282, '192.168.2.33', '5kzqemHVQSUVt9tF4F6Kt-_smRm9DKuo', 'chrome', '2022-03-21 02:18:15', 'อนุญาตทั้งหมด', 278),
(283, '192.168.2.33', 'Fs0nwqtsgxQEfGGyyhQpdA-B_NSOZpT3', 'chrome', '2022-03-21 02:19:44', 'อนุญาตทั้งหมด', 278),
(284, '192.168.2.33', 'AhZyel4D6psub9FvtD5NEOLVuq4YE0nf', 'chrome', '2022-03-21 02:20:22', 'อนุญาตทั้งหมด', 278),
(285, '192.168.2.33', 'Sm6K5QiYFsqNZgbGSWOJF2xY1lUQL491', 'chrome', '2022-03-21 02:21:25', 'อนุญาตทั้งหมด', 278),
(286, '192.168.2.33', 'QLhQk9S7tBgaX7vYX_yzYDGYzDjFsVjV', 'chrome', '2022-03-21 02:22:43', 'อนุญาตทั้งหมด', 278),
(287, '192.168.2.33', 'iIXoKGARxJI2y8yu5xZ9xojyzrc5YGOu', 'chrome', '2022-03-21 02:22:56', 'อนุญาตทั้งหมด', 278),
(288, '192.168.2.33', 'oZv5JGvQP_JAQVy7YcoQ0w0Tar9KxkCw', 'chrome', '2022-03-21 15:45:21', 'อนุญาตทั้งหมด', 278),
(289, '192.168.2.33', 'Q5dDyBgQ3i5i9JaayVB1nYgd6qAVZYWd', 'chrome', '2022-03-21 15:48:37', 'อนุญาตทั้งหมด', 278),
(290, '192.168.2.33', 'pTEaYw5bbKr1j2w0HlXc95QniQkEhPTL', 'chrome', '2022-03-21 15:49:35', 'อนุญาตทั้งหมด', 278),
(291, '192.168.2.33', '07A5XJxdxMyjJFYRhL2soCi1P8Mx54Bv', 'chrome', '2022-03-21 15:52:14', 'อนุญาตทั้งหมด', 278),
(296, '192.168.2.33', 'mCpqrnFJ3QTeIpK_M4xNzqnpQ8mwKijr', 'chrome', '2022-03-21 15:58:58', 'อนุญาตทั้งหมด', 279),
(297, '192.168.2.33', 'C0FGEk88be-sXc-guDO2gqCT5Umhk7EA', 'chrome', '2022-03-21 15:59:31', 'อนุญาตทั้งหมด', 279),
(298, '192.168.2.33', 'OuMUvVwGawIm5IcfG2CkKXoRW6ders2H', 'chrome', '2022-03-21 15:59:41', 'อนุญาตทั้งหมด', 279),
(299, '192.168.2.33', 'voQb9fGO1knfto9T1pXJRmlWqBGYXRcJ', 'chrome', '2022-03-21 16:04:15', 'อนุญาตทั้งหมด', 279),
(300, '192.168.2.33', 'rwP0JUyzwS3NfZnRSutJcTyOOaUi_e06', 'chrome', '2022-03-21 16:04:19', 'อนุญาตทั้งหมด', 279),
(305, '192.168.2.33', 'ZS6Ua3ThmoAFyUJRYsMN_aaGj4ANxU75', 'chrome', '2022-03-21 16:08:48', 'อนุญาตทั้งหมด', 279),
(306, '192.168.2.33', 'szMdIsaNknMictfJn7DydY6wFsyBumT0', 'chrome', '2022-03-21 16:08:54', 'อนุญาตทั้งหมด', 279),
(307, '192.168.2.33', 'v2VuSDQD9IH5U7A1J7tYkdPy-Y-0HCxi', 'chrome', '2022-03-21 16:15:36', 'อนุญาตทั้งหมด', 279),
(308, '192.168.2.33', 'gDYU9tvjwSnRb3P03Hl4x-YI-OHhS8Ij', 'chrome', '2022-03-21 16:18:54', 'อนุญาตทั้งหมด', 279),
(309, '192.168.2.33', 'LN15HxWA6pDuv83CprXG9ZhNqmUsyDg0', 'chrome', '2022-03-21 16:19:40', 'อนุญาตทั้งหมด', 278),
(310, '192.168.2.33', 'JAwGWRR-BFxXy9R4imyHK3RNb0so3elo', 'chrome', '2022-03-21 16:19:43', 'อนุญาตทั้งหมด', 278),
(311, '192.168.2.33', 'ItDlYksB_mqJbg1X5qxcHIAq3GQw18qE', 'chrome', '2022-03-21 16:20:40', 'อนุญาตทั้งหมด', 278),
(312, '192.168.2.33', 'wFvqGACh8G2CPL5_UIPIjNbkZcruPmiX', 'chrome', '2022-03-21 16:20:47', 'อนุญาตทั้งหมด', 278),
(313, '192.168.2.33', '1q2nuQJ3nKi5dTPTYxGCHmJflByrWVBq', 'chrome', '2022-03-21 16:21:19', 'อนุญาตทั้งหมด', 278),
(314, '192.168.2.33', 'MeCEsu9fXfH2PbbRo_apnbaQM7G9p-mL', 'chrome', '2022-03-21 16:21:32', 'อนุญาตทั้งหมด', 279),
(315, '192.168.2.33', 'cGIDgR6XFfCWTwkzoE5AnEGchtURBovQ', 'chrome', '2022-03-21 16:21:38', 'อนุญาตทั้งหมด', 279),
(316, '192.168.2.33', 'lST4WtQdwJiJGlzoXL80DuiUX-ZJ_TeL', 'chrome', '2022-03-21 16:26:59', 'อนุญาตทั้งหมด', 279),
(317, '192.168.2.33', 'MtFUdoIwsH0CI0RUvtF-d8PLCEjKz5iZ', 'chrome', '2022-03-21 16:27:05', 'อนุญาตทั้งหมด', 279),
(318, '192.168.2.33', 'Rs6i3rsEzsjzBlFZ1q5yZXc8X2B_fAvv', 'chrome', '2022-03-21 16:28:12', 'อนุญาตทั้งหมด', 279),
(319, '192.168.2.33', 'Gz4cpavNvkzySeR3OfpeukBMPfUlDXWP', 'chrome', '2022-03-21 16:28:17', 'อนุญาตทั้งหมด', 279),
(320, '192.168.2.33', 'bcbXkxXqmYS6LlZOJE6EjY1eIDHdSzHM', 'chrome', '2022-03-21 16:28:51', 'อนุญาตทั้งหมด', 279),
(321, '192.168.2.33', 'Er6K0HgRKjsrSGDAFtKrtfmWZfWmQHNK', 'chrome', '2022-03-21 16:28:56', 'อนุญาตทั้งหมด', 279),
(322, '192.168.2.33', 'SXXAIdD57TOeujqyNtbr2rHnhv4V2jXf', 'chrome', '2022-03-21 16:42:59', 'อนุญาตทั้งหมด', 279),
(323, '192.168.2.33', 'EKhDD_agQKKwS58oYr6zpMNOn8ABtK3a', 'chrome', '2022-03-21 16:43:12', 'อนุญาตทั้งหมด', 279),
(324, '192.168.2.33', 'qdvBmDX3wvQu0B-miLOQzzztnv1uhKB-', 'chrome', '2022-03-21 16:43:16', 'อนุญาตทั้งหมด', 279),
(325, '192.168.2.33', 'uQLRDsmuIyekr1XtnDWM2IBc4E6lSPaP', 'chrome', '2022-03-21 16:47:31', 'อนุญาตทั้งหมด', 279),
(326, '192.168.2.33', 'In-rf5BO5Q-oHXPgPN7gxeOjxgSVLJT6', 'chrome', '2022-03-21 16:47:40', 'อนุญาตทั้งหมด', 279),
(327, '192.168.2.33', 'Mr6ptk5u2pNS6bddHKYCrWjQgi4jQcVL', 'chrome', '2022-03-21 16:50:19', 'อนุญาตทั้งหมด', 279),
(328, '192.168.2.33', 'ZyqotLx48z3RaDSUWXoRB5n5X48ugFnH', 'chrome', '2022-03-21 16:50:19', 'อนุญาตทั้งหมด', 279),
(329, '192.168.2.33', 'fSzSaxuPH1_2g5Apu8PDDYr0e0feFIVX', 'chrome', '2022-03-21 16:50:22', 'อนุญาตทั้งหมด', 279),
(330, '192.168.2.33', 'EtyBL1wc3EZU3PrHA2sZMga4BLYothZR', 'chrome', '2022-03-21 16:51:47', 'อนุญาตทั้งหมด', 279),
(331, '192.168.2.33', 'ySGB2k6TxN1S6-SfW9T6GLxgGmP-iU4G', 'chrome', '2022-03-21 16:51:53', 'อนุญาตทั้งหมด', 279),
(332, '192.168.2.33', 'XlN4objlFPBJ6nkFHv0zm_AEiWvTnqvq', 'chrome', '2022-03-21 16:51:59', 'อนุญาตทั้งหมด', 279),
(333, '192.168.2.33', 'wrklmZC4dFYdHLeuWt_lxFzeTvms2fTr', 'chrome', '2022-03-21 16:52:11', 'อนุญาตทั้งหมด', 278),
(334, '192.168.2.33', 'xSSed-q1i7FV5cD35E5kT_lX5lKRV-kc', 'chrome', '2022-03-21 16:53:42', 'อนุญาตทั้งหมด', 278),
(335, '192.168.2.33', '8E9uGs6ORnk9eOwZZfAmbDoNEUDwviHy', 'chrome', '2022-03-21 16:53:53', 'อนุญาตทั้งหมด', 278),
(336, '192.168.2.33', '7yRdt9eTHM09ojBSrPiKqRI9IogL0Fut', 'chrome', '2022-03-21 16:53:58', 'อนุญาตทั้งหมด', 278),
(337, '192.168.2.33', 'AsYxbv1zfXj2CC5DjBhVrmfx1kCRAd7M', 'chrome', '2022-03-21 16:54:10', 'อนุญาตทั้งหมด', 278),
(338, '192.168.2.33', '2N0EeRIbVPKFf4xPk6b10f3Jbh0h-RW_', 'chrome', '2022-03-21 16:54:42', 'อนุญาตทั้งหมด', 278),
(339, '192.168.2.33', 'gOjDHN_mOcTgwc6xs0g5m9pHHW89K61r', 'chrome', '2022-03-21 16:55:15', 'อนุญาตทั้งหมด', 278),
(340, '192.168.2.33', 'Z49vK6nxLRnjhsGyx-MBeWQI62Owq5Q-', 'chrome', '2022-03-21 16:55:18', 'อนุญาตทั้งหมด', 278),
(341, '192.168.2.33', 'LWRjeS5zLQ3VLr16wxA0hpWJYRfHO6O_', 'chrome', '2022-03-21 16:55:45', 'อนุญาตทั้งหมด', 278),
(342, '192.168.2.33', '9AgGIXuUIdSaJF0o4YcuJ2RAwaR8I8m0', 'chrome', '2022-03-21 16:55:51', 'อนุญาตทั้งหมด', 278),
(343, '192.168.2.33', 'Fx4l6-9pQOcz_1BG2nPaJFm-ZN6qSSOf', 'chrome', '2022-03-21 16:55:57', 'อนุญาตทั้งหมด', 278),
(344, '192.168.2.33', 'zao56zNWexNEHaGt4cbDMCPRVPT34qvb', 'chrome', '2022-03-21 16:56:13', 'อนุญาตทั้งหมด', 279),
(345, '192.168.2.33', 'rKqNmi-wUT3xApiJNN0E5ObGNOS_imyn', 'chrome', '2022-03-21 16:56:19', 'อนุญาตทั้งหมด', 279),
(346, '192.168.2.33', 'AWF-LZGgemF7o3XGB2vx7hjXenyv3wJV', 'chrome', '2022-03-21 18:11:39', 'อนุญาตทั้งหมด', 279),
(347, '192.168.2.33', 's3udkvYxC4C-8zi4jK9jNBvxIEut8GUI', 'chrome', '2022-03-21 18:12:03', 'อนุญาตทั้งหมด', 279),
(348, '192.168.2.33', '5PXr__Ja69wia1rhA3GlbmXfplOeIOzQ', 'chrome', '2022-03-21 18:30:39', 'อนุญาตทั้งหมด', 279),
(349, '192.168.2.33', 'NBD57--2tVqpfNxcXL_JBZmIdYF6uCIZ', 'chrome', '2022-03-21 23:01:43', 'อนุญาตทั้งหมด', 279),
(350, '192.168.2.33', 'EBwDpKWy4pZyTUepPc7aHE7zIAYH8sz0', 'chrome', '2022-03-21 23:01:56', 'อนุญาตทั้งหมด', 279),
(351, '192.168.2.33', 'qfRn5kHlUpk7-W4PSaPyOsVYRVhDgCPN', 'chrome', '2022-03-21 23:30:35', 'อนุญาตทั้งหมด', 279),
(352, '192.168.2.33', 'pGMxvve_OlPAD07HCXffa0Fs7vYputaq', 'chrome', '2022-03-21 23:30:56', 'อนุญาตทั้งหมด', 279),
(353, '192.168.2.33', 'efgRW3XtaACm1q-DpaGZYMtTQz7Y1QYi', 'chrome', '2022-03-21 23:34:16', 'อนุญาตทั้งหมด', 279),
(354, '192.168.2.33', 'r_PyE7AKMSIR_ITIvQhRPqhXZWr8V237', 'chrome', '2022-03-22 00:09:20', 'อนุญาตทั้งหมด', 279),
(355, '192.168.2.33', 'Q7BaJVGz8OnLHfY4hwmtIzYPEHN05o3O', 'chrome', '2022-03-22 00:18:33', 'อนุญาตทั้งหมด', 279),
(356, '192.168.2.33', '82JhsQlBawkGCYGmP3gm04cLbAE12rys', 'chrome', '2022-03-22 00:19:01', 'อนุญาตทั้งหมด', 279),
(357, '192.168.2.33', '-GA7MZYrdU8Oi35WmH_lcLFFyjtnFQXq', 'chrome', '2022-03-22 00:19:14', 'อนุญาตทั้งหมด', 279),
(358, '192.168.2.33', 'KN-AaBqKcmLErDkF6diz6ZDD7z6b3p1E', 'chrome', '2022-03-22 00:19:20', 'อนุญาตทั้งหมด', 279),
(359, '192.168.2.33', '15DTkY5QvDO_Nk20G_Wez8mtLZviNMQZ', 'chrome', '2022-03-22 00:19:26', 'อนุญาตทั้งหมด', 279),
(360, '192.168.2.33', 'VRjBWXck7qJaZPjfI3RvTq0MG6ZPypv5', 'chrome', '2022-03-22 00:19:38', 'อนุญาตทั้งหมด', 279),
(361, '192.168.2.33', 'W3gOUyE6QwKcHPhx8hjCAulAAu6uisJ8', 'chrome', '2022-03-22 00:19:47', 'อนุญาตทั้งหมด', 279),
(362, '192.168.2.33', 'mzt0ZAgK_nnUBAw_MCaKG4m282KR_Tn5', 'chrome', '2022-03-22 00:21:42', 'อนุญาตทั้งหมด', 279),
(363, '192.168.2.33', '327ZxV-IpcB1iUITWAbtTC7nFyB6HdTI', 'chrome', '2022-03-22 00:22:34', 'อนุญาตทั้งหมด', 279),
(364, '192.168.2.33', 'RKhOv1JS9-r0eY6xzcWmnQ0UuX1ufnar', 'chrome', '2022-03-22 00:23:11', 'อนุญาตทั้งหมด', 279),
(365, '192.168.2.33', '07CvXiTZOd8oddJPtLAjJAlcYP4BKotr', 'chrome', '2022-03-22 00:23:16', 'อนุญาตทั้งหมด', 279),
(366, '192.168.2.33', '5oMXc59ivcf1lMU4nnfxX-oHCfh4nwPt', 'chrome', '2022-03-22 00:29:02', 'อนุญาตทั้งหมด', 279),
(367, '192.168.2.33', 'e_prAFnjgIZs1smZ083e5Z0qAAVyFpDD', 'chrome', '2022-03-22 00:30:18', 'อนุญาตทั้งหมด', 279),
(368, '192.168.2.33', 'FBhwNi14wyxhp-daVCWdkbMXWzUAgHwp', 'chrome', '2022-03-22 00:41:50', 'อนุญาตทั้งหมด', 278),
(369, '192.168.2.33', 'XG2Y1tr4pm7-djcKNxuzRYuc1H1iF43l', 'chrome', '2022-03-22 13:11:20', 'อนุญาตทั้งหมด', 278),
(370, '192.168.2.33', 'N6is6YYnxVkio-zROPiod9WCUFxhUH5G', 'chrome', '2022-03-22 13:11:32', 'อนุญาตทั้งหมด', 278),
(371, '192.168.2.33', 'k-u40L5Cnwf6LS4ENwN1mHhHrV7UAc9O', 'chrome', '2022-03-22 13:11:40', 'อนุญาตทั้งหมด', 278),
(372, '192.168.2.33', 'OSlRjuy6xwdqyUOQK6dO-9CcoVBywNI_', 'chrome', '2022-03-22 13:12:05', 'อนุญาตทั้งหมด', 278),
(373, '192.168.2.33', '_pidTbKn3TSWInBBlSG_-KVXv7VDDBiW', 'chrome', '2022-03-22 21:41:12', 'อนุญาตทั้งหมด', 278),
(374, '192.168.2.33', 'razXFZ32yreLvOnQyUQ60wCCX4IB7tIE', 'chrome', '2022-03-22 21:41:26', 'อนุญาตทั้งหมด', 278),
(375, '192.168.2.33', 'MRS0N2r0LANhh_1pYBBogXFdowxpjbqW', 'chrome', '2022-03-22 21:42:55', 'อนุญาตทั้งหมด', 278),
(376, '192.168.2.33', 'vF2vj0eHPxuPiDm_HfO98QpdZS74iMnH', 'chrome', '2022-03-22 21:43:04', 'อนุญาตทั้งหมด', 278),
(377, '192.168.2.33', 'WI-oVIZT6Jcxbf7ExIMHVJayG00I-tky', 'chrome', '2022-03-22 21:44:48', 'อนุญาตทั้งหมด', 279),
(378, '192.168.2.33', 'TxkVdvBGakJGIwtdQ4JBxwvSuXD-RHcy', 'chrome', '2022-03-22 22:02:42', 'อนุญาตทั้งหมด', 279),
(379, '192.168.2.33', '0vEPS6ZgLocfXqgKupGshJ3s6wywx9Dg', 'chrome', '2022-03-22 22:03:02', 'อนุญาตทั้งหมด', 279),
(386, '192.168.2.33', 'OSQT1qP2vHhnXNUTcbbQKLQCd8YiNBWs', 'chrome', '2022-03-24 23:37:38', 'อนุญาตตามที่เลือก', 279),
(387, '192.168.2.33', 'lgSVdfjgX0juxNRt21erRF9HC6ybFGb2', 'chrome', '2022-03-24 23:43:07', 'อนุญาตตามที่เลือก', 279),
(388, '192.168.2.33', 'CCD9BIAZ9yOhoLrhQF02Vi-J7lv6Y4Qa', 'chrome', '2022-03-24 23:44:28', 'อนุญาตตามที่เลือก', 279),
(389, '192.168.2.33', 'szayfbO3dfEk7-p_Ig9gxdasRkEWbxPg', 'chrome', '2022-03-24 23:46:22', 'อนุญาตตามที่เลือก', 279),
(390, '192.168.2.33', 'gQQRObsXRq90kPAaViKOPS5z1iGbeYM5', 'chrome', '2022-03-24 23:46:54', 'อนุญาตตามที่เลือก', 279),
(391, '192.168.2.33', 'cpW8pTJIf2BPUlFXzy4VFBXzmmzrNGjq', 'chrome', '2022-03-24 23:48:47', 'อนุญาตตามที่เลือก', 279),
(392, '192.168.2.33', 'KVPbt4kJhy463dAEqo5yLlhsIgYvog--', 'chrome', '2022-03-24 23:49:44', 'อนุญาตตามที่เลือก', 279),
(393, '192.168.2.33', '73cAm-3av5gWSxX5wA0L32SWFF5xRm1R', 'chrome', '2022-03-24 23:50:32', 'อนุญาตตามที่เลือก', 279),
(394, '192.168.2.33', 'DsBHDrPMaqoNO0ygOgGIo09dftkt4Adc', 'chrome', '2022-03-24 23:51:42', 'ปฏิเสธ', 279),
(395, '192.168.2.33', '0hXjx0EQA27BkG0D1yPuYg-_t_4s-p-H', 'chrome', '2022-03-24 23:54:39', 'อนุญาตทั้งหมด', 279),
(396, '192.168.2.33', 'h5IZb8IixFU4oIeGmb-6Gp-RvcDuBWGh', 'chrome', '2022-03-25 20:52:31', 'ปฏิเสธ', 279),
(397, '192.168.2.33', 'hmyyxIl_hjAS1yES8XUl2ddKRuCoiAOm', 'chrome', '2022-03-25 20:54:12', 'ปฏิเสธ', 279),
(398, '192.168.2.33', 'cAEfvW9VLUcUvOP3ZYytpnCBES7Iuvlx', 'chrome', '2022-03-25 20:55:26', 'ปฏิเสธ', 279),
(399, '192.168.2.33', 'AW-1V757hYoirwdQtHmNTd_u6F0KAhP4', 'chrome', '2022-03-25 20:55:59', 'ปฏิเสธ', 279),
(400, '192.168.2.33', 'zh6rKcVRP-KQ92oCuyfCNY1dz1ij8VhP', 'chrome', '2022-03-25 20:56:40', 'ปฏิเสธ', 279),
(401, '192.168.2.33', 'QzDn3JSQBfzp7mo17nKIEetjamSBs76R', 'chrome', '2022-03-25 21:01:13', 'ปฏิเสธ', 279),
(402, '192.168.2.33', 'mwq6_j3G5Zv-XrE6FUo16Ixd_YVZjpHw', 'chrome', '2022-03-25 21:34:53', 'ปฏิเสธ', 279),
(403, '192.168.2.33', 'Qe7GoBKN55WUdOBHAvj07X7CaEwnxTho', 'chrome', '2022-03-25 21:35:44', 'ปฏิเสธ', 279),
(404, '192.168.2.33', 'NarKKb0CYQ7u3jaLhrY2wwpd0gm0bWW3', 'chrome', '2022-03-25 21:35:54', 'อนุญาตตามที่เลือก', 279),
(405, '192.168.2.33', 'v6-2e5Hwtp9x_bfPQP2yFEr_0fsj4KBW', 'chrome', '2022-03-25 21:37:28', 'ปฏิเสธ', 279),
(406, '192.168.2.33', 'DkCTR3Bmq91pYLX0tYjE4jeGD9MBAeKq', 'chrome', '2022-03-25 21:37:34', 'อนุญาตตามที่เลือก', 279),
(407, '192.168.2.33', 'fzo6E_qSKDr4JM-cBZTI8kqgYIiHT0Wp', 'chrome', '2022-03-25 21:37:44', 'อนุญาตตามที่เลือก', 279),
(408, '192.168.2.33', '58dF2oQL8jmRcQeA9WFLq6dOrFn8Li_P', 'chrome', '2022-03-25 21:52:22', 'อนุญาตทั้งหมด', 279),
(409, '192.168.2.33', 'v_wXV8kyQRqlJn8PBaV0_0uZ47psL9SI', 'chrome', '2022-03-25 21:52:32', 'ปฏิเสธ', 279),
(410, '192.168.2.33', 'vFyOAjDLc_kcR74xm-J3s0r5w1j1dsa2', 'chrome', '2022-03-25 21:57:22', 'ปฏิเสธ', 279),
(411, '192.168.2.33', 'YjIX-yz3zZu7U6wMsqo4uP9CCpCIcUKf', 'chrome', '2022-03-25 22:07:27', 'ปฏิเสธ', 279),
(412, '192.168.2.33', '0oYMw0oQAn_WkHcxRjYz38h1J5p6HXco', 'chrome', '2022-03-25 22:07:53', 'อนุญาตทั้งหมด', 279),
(413, '192.168.2.33', '3qlSBdJk-wp_Qlg8RXMWqI9t9krWGX-C', 'chrome', '2022-03-25 22:08:00', 'อนุญาตตามที่เลือก', 279),
(414, '192.168.2.33', 'D820xaLPzjrTKFkyBQE7nQO0dTQ37V64', 'chrome', '2022-03-25 22:08:30', 'อนุญาตทั้งหมด', 279),
(415, '192.168.2.33', 'fX-MElHMJQfXfUM56PW0hrOU-JRgqiHc', 'chrome', '2022-03-25 22:08:43', 'ปฏิเสธ', 279),
(416, '192.168.2.33', '5gTmjF7FeRKP2sacSK6mGtkDmDyjNZRj', 'chrome', '2022-03-25 22:11:19', 'อนุญาตตามที่เลือก', 279),
(417, '192.168.2.33', 'WQQhK79V2EmjeZpIw-TQ1K1VigYvfTyb', 'chrome', '2022-03-25 22:11:35', 'อนุญาตตามที่เลือก', 279),
(418, '192.168.2.33', '7RFJR-j_OkswuCdxLaWBTBWur_XorD8b', 'chrome', '2022-03-25 22:12:41', 'อนุญาตทั้งหมด', 279),
(419, '192.168.2.33', 'hZfAO3IPASy36L6DiF32Ug1yQ1wsGuDh', 'chrome', '2022-03-25 22:12:47', 'ปฏิเสธ', 279),
(420, '192.168.2.33', '3ZQ7CL2YLUxZo3NYb1rRxAOSjX_326ET', 'chrome', '2022-03-25 22:14:26', 'ปฏิเสธ', 279),
(421, '192.168.2.33', 'JOhDSqZOhkkOtKDjRTOlJTknXR2-f7da', 'chrome', '2022-03-25 22:14:36', 'อนุญาตตามที่เลือก', 279),
(422, '192.168.2.33', 'dfW7U-n-GxhVzbN-cO0IDKtp42WNp-aQ', 'chrome', '2022-03-25 22:14:41', 'อนุญาตทั้งหมด', 279),
(423, '192.168.2.33', '64pQP4M33t105EAGrWf1wegG9VSe3sjW', 'chrome', '2022-03-25 23:15:44', 'ปฏิเสธ', 279),
(424, '192.168.2.33', 's8kHJMh9fxWB-GEu0oDu75wIMaC70UhN', 'chrome', '2022-03-26 01:12:13', 'อนุญาตตามที่เลือก', 279),
(425, '192.168.2.33', 'urut2xPbmjfH5QW30Jjw16hkCdZ0Xe_0', 'chrome', '2022-03-26 01:14:02', 'อนุญาตตามที่เลือก', 279),
(426, '192.168.2.33', '9uRrFkP9uYVO4MyjT_idc7-G6HLpW820', 'chrome', '2022-03-26 01:14:25', 'ปฏิเสธ', 279),
(427, '192.168.2.33', 'CSPn6d1Z_9mrydmPV492rsR_oZEss87z', 'chrome', '2022-03-26 01:16:19', 'อนุญาตตามที่เลือก', 279),
(428, '192.168.2.33', 'dKyHFndn6kZ4_rLxU_2Geox5a2Aheq1B', 'chrome', '2022-03-26 01:16:36', 'อนุญาตตามที่เลือก', 279),
(429, '192.168.2.33', 'FeEDWK0Z_UbMlaysLmzeDh988EunMkAE', 'chrome', '2022-03-26 01:16:59', 'อนุญาตตามที่เลือก', 279),
(430, '192.168.2.33', '3k3gr_uWi8VMJgbLqC970Tv435Hkeglw', 'chrome', '2022-03-26 01:36:04', 'อนุญาตทั้งหมด', 279),
(431, '192.168.2.33', 'HZqKsy3CpeMBZPn-bEX8rAnkOByJ-tys', 'chrome', '2022-03-26 01:36:19', 'อนุญาตทั้งหมด', 279),
(432, '192.168.2.33', '-4rMkEh-CL1SCrZTsuJeAo4zj8xW_L9S', 'chrome', '2022-03-26 01:36:26', 'อนุญาตทั้งหมด', 279),
(433, '192.168.2.33', 'AxCDJbUSLI-8falu2dSGbY2So_TP1YKx', 'chrome', '2022-03-26 01:36:45', 'อนุญาตทั้งหมด', 279),
(434, '192.168.2.33', 'LOky-nf-HhWHwEUIovmKWiN47PYqhJNf', 'chrome', '2022-03-26 01:37:45', 'อนุญาตทั้งหมด', 279),
(435, '192.168.2.33', 'vcGHNv1YIy70n7XjfonHLQeYTnXYimnY', 'chrome', '2022-03-26 01:38:55', 'อนุญาตทั้งหมด', 279),
(436, '192.168.2.33', '2u-lozYHaUhRvo-nps2y9ldjGixENT35', 'chrome', '2022-03-26 01:42:52', 'อนุญาตตามที่เลือก', 279),
(437, '192.168.2.33', 'fGX6AfjcEBbj7nLtxSpg4RoRzSBFfJPo', 'chrome', '2022-03-26 01:43:40', 'อนุญาตทั้งหมด', 279),
(438, '192.168.2.33', 'r1TsHMnJSMKGvSJCDEle_6mxWksdJt-Q', 'chrome', '2022-03-26 01:44:55', 'อนุญาตตามที่เลือก', 279),
(439, '192.168.2.33', 'Jg8nKO_4vwZJny4dfinD-KvDqm30qFdp', 'chrome', '2022-03-26 01:45:00', 'อนุญาตทั้งหมด', 279),
(440, '192.168.2.33', 'HyhNZQu-xguKHzKRTq0ogrH6h6Oe5gCf', 'chrome', '2022-03-26 01:46:05', 'อนุญาตทั้งหมด', 279),
(441, '192.168.2.33', '_rrY1ArQihITpsOwEQEOL9fhn9RitHZb', 'chrome', '2022-03-26 01:46:11', 'อนุญาตทั้งหมด', 279),
(442, '192.168.2.33', 'yfaR97cL7bH5JhFkg6s7h3uRVV7xbaVL', 'chrome', '2022-03-26 01:47:15', 'อนุญาตทั้งหมด', 279),
(443, '192.168.2.33', '_qEWIxtZ6yReFkPAPAFRsUmKzYpNMBxq', 'chrome', '2022-03-26 01:47:21', 'ปฏิเสธ', 279),
(444, '192.168.2.33', 'qzKvTlaQBUshZv5m1fGSByFwiI5gdPLQ', 'chrome', '2022-03-26 02:57:41', 'อนุญาตทั้งหมด', 279),
(445, '192.168.2.33', '48JEt0j72sDMhwev2H5NJV54gyIjYn5W', 'chrome', '2022-03-26 02:58:02', 'อนุญาตตามที่เลือก', 279),
(446, '192.168.2.33', 'Q2ma3bp65inperetWzXld7iUHvsj93Vw', 'chrome', '2022-03-26 03:01:22', 'ปฏิเสธ', 279),
(447, '192.168.2.33', 'EQtaoGilXWXCUjItKRP3bQ0TBrSc-XJl', 'chrome', '2022-03-26 03:20:09', 'อนุญาตทั้งหมด', 279),
(448, '192.168.2.33', 'XbFcWdyPoUhdH7hBQGCspHkpGYJ4ti5j', 'chrome', '2022-03-26 03:29:37', 'ปฏิเสธ', 279),
(449, '192.168.2.33', 'IDQMUM6ncEmyM6bQq-l6qIjVcGi_Mkps', 'chrome', '2022-03-26 03:30:57', 'ปฏิเสธ', 279),
(450, '192.168.2.33', 'UwRGzRqteHJTwGFHBSefHBF1AgmP3nV6', 'chrome', '2022-03-26 03:36:17', 'ปฏิเสธ', 279),
(451, '192.168.2.33', 'WEq4OPyOG4Ml766C2kHue2WJQVw2Ibcd', 'chrome', '2022-03-26 03:36:37', 'ปฏิเสธ', 279),
(452, '192.168.2.33', 'VW92_WiPnVvgzzh8dEmyE-2TSc-LQW9L', 'chrome', '2022-03-26 03:38:42', 'อนุญาตทั้งหมด', 279),
(453, '192.168.2.33', 'MVL-ztoiedTZffXR1YwjBT4BJPiAlwyn', 'chrome', '2022-03-26 03:38:50', 'อนุญาตตามที่เลือก', 279),
(454, '192.168.2.33', '2Lwd99iPUWSfVM_1amDmbj7cbCoY3wpJ', 'chrome', '2022-03-26 03:48:25', 'ปฏิเสธ', 279),
(455, '192.168.2.33', 'jwjKIRSatkFEc1fhAN4J_yKtKZmWBDZj', 'chrome', '2022-03-26 03:49:16', 'ปฏิเสธ', 279),
(456, '192.168.2.33', 'vDiNLcoBjKNbt__G2IyG1Lj8TD6qjM4u', 'chrome', '2022-03-26 03:51:00', 'ปฏิเสธ', 279),
(457, '192.168.2.33', 'A80iDBaIeZa2vZiFAoLUJc7Fn_pDynbJ', 'chrome', '2022-03-26 03:51:13', 'ปฏิเสธ', 279),
(458, '192.168.2.33', '6ozd3-pyniztLZPIP-7-7wSsVNkCnnqV', 'chrome', '2022-03-26 03:52:34', 'อนุญาตทั้งหมด', 279),
(459, '192.168.2.33', 'mJylkJ05zsW57DUSl-1yX4rpcqc0dVzC', 'chrome', '2022-03-26 03:52:39', 'อนุญาตตามที่เลือก', 279),
(460, '192.168.2.33', 'MI3xQLKqDvFWQ3iXxdakQKXqBxxh2lcV', 'chrome', '2022-03-26 04:13:48', 'ปฏิเสธ', 279),
(461, '192.168.2.33', 'VGorPgkzoXMwPxnK4n1dy1v2OV8U493M', 'chrome', '2022-03-26 04:18:44', 'อนุญาตทั้งหมด', 279),
(462, '192.168.2.33', 'isv4KP1SxvYGP3QpFg_GNT8Ehhd55cRA', 'chrome', '2022-03-26 04:19:38', 'อนุญาตตามที่เลือก', 279),
(463, '192.168.2.33', '3u4Tw8BTw-PqPkUS_H23ug0yNlpBahZ6', 'chrome', '2022-03-26 04:23:08', 'ปฏิเสธ', 278),
(464, '192.168.2.33', '3iCQEnYxC9R2qhnf3nbcCYI2zWO6Ql6o', 'chrome', '2022-03-26 05:23:28', 'ปฏิเสธ', 301),
(465, '192.168.2.33', '_Vs0gVZxl2_XNiQQKX04U-CLFlpU9vEb', 'chrome', '2022-03-26 05:23:33', 'อนุญาตทั้งหมด', 301),
(466, '192.168.2.33', 'U3T1ymfrv4qTXTdq2El-O4LaKmAdheii', 'chrome', '2022-03-26 05:24:47', 'ปฏิเสธ', 301),
(467, '192.168.2.33', '1XhPsZTaPfdCMBRpFgrY43fUO8ffImGB', 'chrome', '2022-03-26 05:24:55', 'อนุญาตตามที่เลือก', 301),
(468, '192.168.2.33', '2-GSHzkvh_ODFe5Bm3VjPRDUJZVgVfk0', 'chrome', '2022-03-26 07:05:53', 'อนุญาตทั้งหมด', 301),
(469, '192.168.2.33', '0_L-vuQBDEzXA7kJCdyTXs8hNQvQdyY9', 'chrome', '2022-03-26 13:24:08', 'อนุญาตทั้งหมด', 301),
(470, '192.168.2.33', 'iMNIqSE7DMCFsSIocnB3Pado1m7zvc9u', 'chrome', '2022-03-27 05:16:29', 'อนุญาตทั้งหมด', 301),
(471, '192.168.2.35', 'yOgMorgCDw3C5DURA5UcdzxOPEmNtAzz', 'chrome', '2022-03-28 19:34:12', 'อนุญาตตามที่เลือก', 301),
(472, '192.168.2.35', 'fz6tsoUGJ7lwDWzQaPx-W3Pt-j58SzsK', 'chrome', '2022-03-28 19:35:15', 'อนุญาตตามที่เลือก', 301),
(473, '192.168.2.35', '-ITJ4v7D1WnCsrtvmkciFmYC3JAEVHlR', 'chrome', '2022-03-28 19:35:22', 'อนุญาตทั้งหมด', 301),
(474, '192.168.2.35', 'kqQs4qSQEOIE6qA8A65Iq9GU02XjDr8Z', 'chrome', '2022-03-28 19:35:27', 'ปฏิเสธ', 301),
(475, '192.168.2.35', 'Y1JYhguZQG-beW9xiXvYaiKmQxcfcvVb', 'chrome', '2022-03-28 19:35:33', 'อนุญาตตามที่เลือก', 301),
(476, '192.168.2.35', 'xT-QSlu5MGqabd3-PERr8dsEcgjbWbeE', 'chrome', '2022-03-28 19:37:40', 'อนุญาตตามที่เลือก', 302),
(477, '192.168.2.35', '-0DovA0sj3rLq-ehf69mHnHu3cJqXgyq', 'chrome', '2022-03-28 19:38:24', 'ปฏิเสธ', 302),
(478, '192.168.2.35', 'uzyZDxMbnd9qwEqqwAq3u5EliJa-SE7X', 'chrome', '2022-03-28 19:38:29', 'อนุญาตทั้งหมด', 302),
(479, '192.168.2.35', '1tsLyBiHfOaRKKiQMxt2DUIUT64PGNLh', 'chrome', '2022-03-28 19:46:00', 'ปฏิเสธ', 302),
(480, '192.168.2.35', 'gfxtdFap6DyAHZVxkxPOsUvo77bnnd87', 'chrome', '2022-03-28 19:46:27', 'อนุญาตตามที่เลือก', 302),
(481, '192.168.2.35', '7fOfDjIUvs9PgcfgGFLS8pHEtsnLmpq6', 'chrome', '2022-03-28 20:08:28', 'อนุญาตทั้งหมด', 302),
(482, '192.168.2.35', 'TqPOyJ0EYwE8vlQHF1_v-1TrbrI6mcPW', 'chrome', '2022-03-28 20:09:10', 'อนุญาตตามที่เลือก', 302),
(483, '192.168.2.35', 'MokLipyHfhJsIMUb-MnBwpC-Fp5tH0cO', 'chrome', '2022-03-28 20:09:15', 'อนุญาตทั้งหมด', 302),
(484, '192.168.2.35', '9JyofwHxOvtl4THlewrz5QjZ0ICETzJp', 'chrome', '2022-03-28 20:09:22', 'อนุญาตตามที่เลือก', 302),
(485, '192.168.2.35', 'Z8r8zmcIhB1yO8JNHEpi5n06ZparHEWw', 'chrome', '2022-03-28 21:10:42', 'ปฏิเสธ', 302),
(486, '192.168.2.35', 'KZCGGkvqjJDsnFSAHC6FUupQpYlsjPHE', 'chrome', '2022-03-28 21:17:38', 'อนุญาตทั้งหมด', 302),
(487, '192.168.2.35', '8Q0JzH1GWnAqfHyleFJKGYF1Y1XRvMiJ', 'chrome', '2022-03-28 21:17:42', 'ปฏิเสธ', 302),
(488, '192.168.2.35', 'AC9nO7CNWgFbMRrCcOIegoQ5yi-mw8r_', 'chrome', '2022-03-28 21:18:02', 'อนุญาตตามที่เลือก', 302),
(489, '192.168.2.35', 'dVuZNvjAan0wTXlNHcp0kFcKyhRprRLd', 'chrome', '2022-03-28 21:58:56', 'ปฏิเสธ', 301),
(490, '192.168.2.35', '6UAB5GUy35St3EhjqlRqQOLKotTegbpo', 'chrome', '2022-03-28 22:09:08', 'อนุญาตทั้งหมด', 301),
(491, '192.168.2.35', 'uCfBIcfsr2-HD3-Jqdw9oYiNPLlkDF3m', 'chrome', '2022-03-28 22:10:01', 'ปฏิเสธ', 301),
(492, '192.168.2.35', 'ch3ENKMTNfXvpPugdCdesNkZEEZ0irFi', 'chrome', '2022-03-28 22:10:29', 'อนุญาตทั้งหมด', 301),
(493, '192.168.2.35', 'dKbEwZKLt-x2gffpK8OnyfnZya7kI0Ha', 'chrome', '2022-03-28 22:11:47', 'อนุญาตตามที่เลือก', 301),
(494, '172.16.200.5', 'teRZpQcaoGea7xAbcKNhzzZoi_sJKeXs', 'chrome', '2022-03-28 22:23:37', 'อนุญาตทั้งหมด', 301),
(495, '172.16.200.5', 'icXV38P2er2Cyx60GFWdWSUFJp1w9Xwt', 'chrome', '2022-03-28 22:24:19', 'ปฏิเสธ', 301),
(496, '172.16.200.5', 'AjJ2OLwUOrXljQIuyGhuHltjKD3upOio', 'chrome', '2022-03-28 22:26:24', 'อนุญาตทั้งหมด', 301),
(497, '172.16.200.5', 'T-vGpmHFHYcF36RNf0vdSIif3ZB3cxmP', 'chrome', '2022-03-28 22:26:31', 'ปฏิเสธ', 301),
(498, '192.168.2.35', 'GcETEtE2wffQ2HiQUZ6vSgQ5uvFRTHYt', 'chrome', '2022-03-28 22:45:40', 'อนุญาตทั้งหมด', 301),
(499, '192.168.2.35', 'wPwzuhJ5w_2Tsqxh2rlOf5N6jhms2SWw', 'chrome', '2022-03-28 23:06:22', 'ปฏิเสธ', 301),
(500, '192.168.2.35', 'Cd3Vjpo48uLB_mMCACJhPvi0AjqFacPv', 'chrome', '2022-03-28 23:07:21', 'อนุญาตตามที่เลือก', 301),
(501, '192.168.2.35', '16-a5dp6RQfzyul29CelIf4ZEYm9QN57', 'chrome', '2022-03-28 23:17:29', 'อนุญาตทั้งหมด', 301),
(502, '192.168.2.35', 'jMTTKUyvUfUi---itCh7x85osDvHf87S', 'chrome', '2022-03-28 23:17:48', 'ปฏิเสธ', 301),
(503, '192.168.2.35', 'uAM1YfpJt132jtLzmY7VaNAY4JG3pKen', 'chrome', '2022-03-28 23:27:53', 'ปฏิเสธ', 301),
(504, '192.168.2.35', '51aiDHTjdq0uzzKx2fezKvnX99Gc2XAB', 'chrome', '2022-03-29 21:13:22', 'อนุญาตตามที่เลือก', 301),
(505, '192.168.2.35', 'E3b8hBnmW5wyfSG925sHEuKbzcVsqdVO', 'firefox', '2022-03-29 21:14:47', 'อนุญาตตามที่เลือก', 301),
(506, '192.168.2.35', '2yB4MGQTHlBw9Q43rnMKmX6XiwUy7qx8', 'chrome', '2022-03-29 21:17:39', 'ปฏิเสธ', 301),
(519, '192.168.2.35', '7q0PWhbcgF268eWk4Ph97CetKOjec2uB', 'chrome', '2022-03-31 22:59:45', 'ปฏิเสธ', 302),
(520, '192.168.2.35', 'BprAjijKs3xOF4dN86W5BYAsGAIyF9g3', 'chrome', '2022-03-31 23:02:46', 'อนุญาตทั้งหมด', 279),
(521, '192.168.2.35', 'W4d0r7yiS6OnQ98v7jmVakLl2GAomkf0', 'chrome', '2022-03-31 23:04:21', 'อนุญาตทั้งหมด', 301),
(522, '192.168.2.35', 'dZeJDfuSVSRtEA9S_4050uT3NRiAMj-9', 'chrome', '2022-03-31 23:07:11', 'ปฏิเสธ', 304),
(523, '192.168.2.35', 'Cod-4gBxIGMbZOpvpNRkmUIiEY-XwRoK', 'chrome', '2022-03-31 23:08:29', 'ปฏิเสธ', 305),
(524, '192.168.2.35', 'inO2u6hYpFgJ-tbr0AtdEyj59aEnAIKt', 'chrome', '2022-03-31 23:09:18', 'ปฏิเสธ', 305),
(525, '192.168.2.35', 'qFoD24N6rLIB5vp5SrwEunN7_B8ao5Nt', 'chrome', '2022-03-31 23:09:39', 'อนุญาตทั้งหมด', 305),
(526, '192.168.2.35', 'Q5oRYI2FKSPlaPVYdMy2AEsSzB2zufj7', 'chrome', '2022-03-31 23:10:01', 'อนุญาตตามที่เลือก', 305),
(527, '192.168.2.35', 'PFfQ6XRpHHWrdT28wN3-chXii3asr0f_', 'chrome', '2022-03-31 23:10:38', 'อนุญาตทั้งหมด', 305),
(528, '192.168.2.35', 'iXn33JNtKpPrNBshtxmezayn-iR5h3Ia', 'chrome', '2022-03-31 23:11:06', 'ปฏิเสธ', 305),
(529, '192.168.2.35', 'CKJLrjrLXhvswnB8U49M2IIsWOa1EuMi', 'chrome', '2022-03-31 23:12:32', 'ปฏิเสธ', 305),
(530, '192.168.2.35', 'hfbjmmJjSF92a0D6ZpF3BddTrkop2cGt', 'chrome', '2022-03-31 23:12:47', 'อนุญาตทั้งหมด', 305),
(531, '192.168.2.35', 'JLFZo4-RqprR3LeBY7inYwk54CLR0h9j', 'chrome', '2022-03-31 23:15:25', 'ปฏิเสธ', 305),
(532, '192.168.2.35', 'TQESFNsduKEXAWq2risasBqCsKZ6N9sd', 'chrome', '2022-03-31 23:21:06', 'อนุญาตตามที่เลือก', 305),
(533, '192.168.2.35', 'fVDmP5UHe2Kx3WQcddXcFc4LM4KgeBCC', 'chrome', '2022-03-31 23:32:11', 'อนุญาตทั้งหมด', 305),
(534, '192.168.2.35', 'b9-FSKOISGCtgiV6vY-PSBGwhHmVMXwD', 'chrome', '2022-03-31 23:33:39', 'ปฏิเสธ', 305),
(535, '192.168.2.35', 'Zd5w2Vcd0R2NzpqgPzv2K_CKlSCvyIZk', 'chrome', '2022-03-31 23:34:35', 'อนุญาตทั้งหมด', 305),
(536, '192.168.2.35', 'L77NHToMf10thFYQtuxIBjFeT7X02KEw', 'chrome', '2022-03-31 23:35:30', 'อนุญาตทั้งหมด', 305),
(537, '192.168.2.35', 'm4r1OstfODuB8sRkYzAGMw-Z7VH58IMz', 'chrome', '2022-03-31 23:36:10', 'ปฏิเสธ', 305),
(538, '192.168.2.35', 'kGN0Gc7rqvDj89_u-X69kJT63fC7m4Bb', 'chrome', '2022-03-31 23:37:02', 'ปฏิเสธ', 305),
(539, '192.168.2.35', 'IN54f0Hb1RgG8y3F8uJ2E9VNbGzB6tlO', 'chrome', '2022-03-31 23:38:51', 'อนุญาตทั้งหมด', 305),
(540, '192.168.2.35', 'VYI_QwqX6MKq_tXPz9AuCVGoE8QUnRSk', 'chrome', '2022-03-31 23:48:04', 'ปฏิเสธ', 305),
(541, '192.168.2.35', '-vTdO0znzMsqq32KdkKOIYw5nUAvpeyg', 'chrome', '2022-03-31 23:49:41', 'อนุญาตทั้งหมด', 305),
(542, '192.168.2.35', '7dpoEVZjAe_TGZw8nSnhdO-4cT9qDV1s', 'chrome', '2022-03-31 23:56:05', 'อนุญาตตามที่เลือก', 305),
(543, '192.168.2.37', 'EeF8i0tJGYKxxEQcc9-L2DJXIfuwxBGG', 'chrome', '2022-04-08 01:59:19', 'อนุญาตตามที่เลือก', 307),
(544, '192.168.2.37', 'f5IGBky3GGtTRqSkE5NZRXCSlKk9QyQq', 'chrome', '2022-04-08 02:04:30', 'อนุญาตทั้งหมด', 307),
(545, '192.168.2.37', 'doE6SiTZQ_b8VJe0HjW06EfVIA29qcxU', 'chrome', '2022-04-08 02:04:37', 'ปฏิเสธ', 307),
(546, '192.168.2.37', 'buhjNg_TP8XsM46-SEpLzhMgTxSiJoMA', 'chrome', '2022-04-08 02:05:43', 'อนุญาตตามที่เลือก', 307),
(547, '192.168.2.37', '53kJEhpzCSI4XI1-cG4aZmYPSADciPyx', 'chrome', '2022-04-08 02:08:15', 'อนุญาตตามที่เลือก', 307),
(548, '192.168.2.37', 'C2w2l3NRfTiGX3j9ZghzHdPthvQR985N', 'chrome', '2022-04-08 02:08:36', 'อนุญาตทั้งหมด', 307),
(549, '192.168.2.37', 'bDEx5YbcUFKV3u2WbK7h6xWwjILM7yce', 'chrome', '2022-04-08 02:09:10', 'ปฏิเสธ', 307),
(550, '192.168.2.37', 'bFdbAZInm42YTGjus4vKjM941INXX1VM', 'chrome', '2022-04-08 02:10:01', 'อนุญาตทั้งหมด', 307);

-- --------------------------------------------------------

--
-- Table structure for table `pdpa_tag`
--

CREATE TABLE `pdpa_tag` (
  `id_tag` int(11) NOT NULL,
  `tag_name` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `tag_date` datetime DEFAULT current_timestamp(),
  `tag_styles` varchar(500) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `pdpa_tag`
--

INSERT INTO `pdpa_tag` (`id_tag`, `tag_name`, `tag_date`, `tag_styles`) VALUES
(93, 'สำคัญ', '2022-04-07 14:09:20', '#4B0268'),
(94, 'สำคัญมาก', '2022-04-07 14:09:20', '#F1F422'),
(95, 'สำคัญโครต', '2022-04-07 14:09:20', '#F92580');

-- --------------------------------------------------------

--
-- Table structure for table `performance`
--

CREATE TABLE `performance` (
  `performance_id` int(11) NOT NULL,
  `cpu` varchar(255) NOT NULL,
  `cpuused` varchar(255) NOT NULL,
  `mem` varchar(255) NOT NULL,
  `memused` varchar(255) NOT NULL,
  `storage` varchar(255) NOT NULL,
  `storageused` varchar(255) NOT NULL,
  `storageper` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `performance`
--

INSERT INTO `performance` (`performance_id`, `cpu`, `cpuused`, `mem`, `memused`, `storage`, `storageused`, `storageper`) VALUES
(1, '100.0%\'', '0.00', '3.840587', '17.22208089544645', '28G', '15G', '54%');

-- --------------------------------------------------------

--
-- Table structure for table `pw_change`
--

CREATE TABLE `pw_change` (
  `change_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pw_change`
--

INSERT INTO `pw_change` (`change_id`, `acc_id`, `date`) VALUES
(68, 175, '2022-04-22 01:20:04'),
(69, 175, '2022-04-29 10:12:15'),
(70, 175, '2022-05-06 01:26:49');

-- --------------------------------------------------------

--
-- Table structure for table `qrcode`
--

CREATE TABLE `qrcode` (
  `qr_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `image` mediumtext NOT NULL,
  `ascii` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `qrcode`
--

INSERT INTO `qrcode` (`qr_id`, `acc_id`, `image`, `ascii`) VALUES
(30, 175, 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAAAklEQVR4AewaftIAAAdySURBVO3BQW4ER5IAQfcE//9lX902TgUUusmREmFm/2CtSxzWushhrYsc1rrIYa2LHNa6yGGtixzWushhrYsc1rrIYa2LHNa6yGGtixzWushhrYsc1rrIDx9S+UsVk8qTiknlScVvUnlS8QmVNyqeqPylik8c1rrIYa2LHNa6yA9fVvFNKk8qJpVJZaqYVJ6oTBWTylTxRsUTlW+qmFSmiicV36TyTYe1LnJY6yKHtS7ywy9TeaPiExWTypOKSeWJylTxRsUTlScVk8pUMan8JpU3Kn7TYa2LHNa6yGGti/zwH6cyVXyi4onKpPJGxaQyVUwqb6hMFZPKVHGTw1oXOax1kcNaF/nhMipvqLxR8URlqvgmlali/b/DWhc5rHWRw1oX+eGXVfyliicqTyr+UsWTijdUpoqpYlKZKt6o+Dc5rHWRw1oXOax1kR++TOXfRGWqmFSeqEwVk8pUMalMFZPKVDGpTBWTylQxqUwVn1D5NzusdZHDWhc5rHWRHz5U8V+iMlVMKlPFJyomlaliUpkqJpU3Kp5UPKn4LzmsdZHDWhc5rHUR+wcfUJkq3lCZKiaVb6p4Q+WNijdUpopPqLxRMal8U8UTlaniE4e1LnJY6yKHtS7ywx9TeaPiicqTiicqU8VU8QmVJxWTypOKSeVJxaQyqbxRMalMFZPKk4pvOqx1kcNaFzmsdZEfvkzlScWk8kTlEypPKiaVqWJSmSomlaliUnmjYlJ5Q+VJxROVSeUTFb/psNZFDmtd5LDWRX74soonKlPFpDJVTCpTxROVJypPVJ6oTBVvqHyi4onKVDGpTBVvVDypmFSmim86rHWRw1oXOax1kR9+mcobFZPKGypPKiaVqeITKlPFVDGpvFExqUwVT1Smim9SeUNlqvjEYa2LHNa6yGGti/zwxyreqJhUJpVPVEwqU8Wk8qTiicpUMak8UZkqJpWpYlJ5o2JSmVSmiicqU8U3Hda6yGGtixzWuoj9gy9SmSreUHlS8QmVqeKbVKaKSeVJxROVqWJSmSomlScVb6h8U8UnDmtd5LDWRQ5rXeSHD6l8QmWqeKLypGJSeUNlqnii8kRlqphUnqhMFb9J5UnFk4pJ5UnFNx3WushhrYsc1rrID19W8UTlicqTikllUnlD5Q2VqeKJypOKT6hMFZPKVPG/VDGpTBWfOKx1kcNaFzmsdRH7Bx9QeVIxqUwVb6g8qZhUnlRMKk8qJpWp4onKk4pPqEwVk8pU8UTlScUTlScV33RY6yKHtS5yWOsi9g++SOWNiknlExVPVD5RMak8qXii8kbFGypvVEwqb1RMKlPFbzqsdZHDWhc5rHWRH/5YxRsVT1SeqEwVT1SmiicVk8qkMlVMFZ9QmSqeVHyiYlKZVKaKJypTxScOa13ksNZFDmtd5IcPqUwVn6iYVD5RMal8QuUTKm9UTCpTxRsq31QxqTxRmSq+6bDWRQ5rXeSw1kV++FDFE5UnFZPKVDGpTBVPVJ5UvFHxROWNim9SmSomlTcqJpUnFW+oTBWfOKx1kcNaFzmsdZEfvkzlDZUnKlPFpDJVPKmYVJ5UvFExqbyh8obKX6p4Q2WqmFS+6bDWRQ5rXeSw1kV++JDKVDGpTBWfUPlNFd9UMam8UfGGyhsVn1CZKt6o+KbDWhc5rHWRw1oX+eFDFZ9QeaNiUnlS8aRiUvlExRsqU8UbKm9UPFGZKt5QeUNlqvjEYa2LHNa6yGGti/zwIZWpYqp4o+KJyhOVT1RMKlPFE5U3Kp6oTBVPKp6oTBVTxaQyVTypmFSeVHzTYa2LHNa6yGGti/zwoYonKk8qnqg8qZhU3qh4Q2Wq+EsqU8Wk8k0VTyqeVPylw1oXOax1kcNaF7F/8ItU3qj4JpWp4g2VT1RMKk8qnqhMFU9UPlHxRGWqmFTeqPjEYa2LHNa6yGGti9g/+IDKJyomlX+TijdUnlQ8UZkqnqhMFU9U/lLFXzqsdZHDWhc5rHWRHz5U8U0Vb6hMFZPKVPFE5Q2VqWJSmVQ+oTJVfFPFGypvqEwV33RY6yKHtS5yWOsiP3xI5S9VPFGZKt6omFTeUJkqnqhMFW+ovFExqTxRmSqeVEwqf+mw1kUOa13ksNZFfviyim9SeVIxqTxRmSomlaliUpkqJpVJZaqYKp6o/C9VfFPFbzqsdZHDWhc5rHWRH36ZyhsVb6i8UTGpTBVPKp5UTCrfVDGpfJPKJ1SeqEwV33RY6yKHtS5yWOsiP/zHVTxR+YTKVDGpTBVTxaTypGKqmFTeqJhUvqniicoTlaniE4e1LnJY6yKHtS7yw2VUpor/JZWpYlL5SxVPVP5SxTcd1rrIYa2LHNa6yA+/rOIvVUwqn6h4UvGk4knFpPKk4g2VqWJSmSomlani3+yw1kUOa13ksNZFfvgylb+k8qTim1SmijdU3qiYVJ5UTBVPKiaVN1SmijdUpopPHNa6yGGtixzWuoj9g7UucVjrIoe1LnJY6yKHtS5yWOsih7UucljrIoe1LnJY6yKHtS5yWOsih7UucljrIoe1LnJY6yL/Bw7IyZRlEbSxAAAAAElFTkSuQmCC', 'V>8]IFuHR/scdaSTbJVURJ}<L2,xPc1c');

-- --------------------------------------------------------

--
-- Table structure for table `questionnaire`
--

CREATE TABLE `questionnaire` (
  `quest_id` int(11) NOT NULL,
  `Name1` varchar(255) NOT NULL,
  `firstName1` varchar(255) NOT NULL,
  `lastName1` varchar(255) NOT NULL,
  `emailAddress1` varchar(255) NOT NULL,
  `phone1` varchar(255) NOT NULL,
  `shortDescription1` varchar(255) DEFAULT NULL,
  `service` varchar(255) NOT NULL,
  `md_checkbox_21` tinyint(4) NOT NULL,
  `md_checkbox_22` tinyint(4) NOT NULL,
  `md_checkbox_23` tinyint(4) NOT NULL,
  `md_checkbox_24` tinyint(4) NOT NULL,
  `store` varchar(255) NOT NULL,
  `shortDescription2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questionnaire`
--

INSERT INTO `questionnaire` (`quest_id`, `Name1`, `firstName1`, `lastName1`, `emailAddress1`, `phone1`, `shortDescription1`, `service`, `md_checkbox_21`, `md_checkbox_22`, `md_checkbox_23`, `md_checkbox_24`, `store`, `shortDescription2`) VALUES
(28, 'Sense Infotech', 'Wasin', 'Wachiropakorn', 'wasin_w@sense-infotech.com', '0824819953', '-', '512', 1, 1, 1, 1, '100GB', 'Server,Firewall,Switch');

-- --------------------------------------------------------

--
-- Table structure for table `readed`
--

CREATE TABLE `readed` (
  `readed_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `classify` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `readed`
--

INSERT INTO `readed` (`readed_id`, `file_id`, `classify`) VALUES
(376, 565, 1),
(377, 566, 1),
(378, 567, 0),
(379, 568, 0),
(380, 568, 0),
(381, 570, 0);

-- --------------------------------------------------------

--
-- Table structure for table `set_network`
--

CREATE TABLE `set_network` (
  `nw_id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `netmark` varchar(255) NOT NULL,
  `gateway` varchar(255) NOT NULL,
  `dns` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `set_network`
--

INSERT INTO `set_network` (`nw_id`, `ip`, `netmark`, `gateway`, `dns`) VALUES
(78, '192.168.1.1', '255.255.255.0', '192.168.1.254', '8.8.8.8'),
(82, '192.168.137.33', '255.255.255.0', '192.168.137.254', '8.8.8.8'),
(83, '192.168.137.33', '255.255.255.0', '192.168.137.254', '8.8.8.8'),
(84, '192.168.137.33', '255.255.255.0', '192.168.137.254', '8.8.8.8');

-- --------------------------------------------------------

--
-- Table structure for table `set_system`
--

CREATE TABLE `set_system` (
  `sys_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `pw_keep` datetime NOT NULL,
  `ntp` varchar(255) NOT NULL,
  `num_admin` int(11) NOT NULL,
  `num_user` int(11) NOT NULL,
  `timezone` varchar(255) NOT NULL,
  `day_keep` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `set_system`
--

INSERT INTO `set_system` (`sys_id`, `datetime`, `pw_keep`, `ntp`, `num_admin`, `num_user`, `timezone`, `day_keep`) VALUES
(181, '2022-02-14 08:50:52', '2022-04-05 08:50:52', 'time2.nimt.or.th', 1, 1, 'Asia/Bangkok', 50),
(184, '2022-03-03 01:20:04', '2022-04-22 01:20:04', 'time2.nimt.or.th', 1, 1, 'Asia/Bangkok', 0),
(185, '2022-03-03 01:20:04', '2022-04-22 01:20:04', 'time2.nimt.or.th', 1, 1, 'Asia/Bangkok', 50),
(186, '2022-03-10 10:12:15', '2022-04-29 10:12:15', 'time2.nimt.or.th', 1, 1, 'Asia/Bangkok', 50),
(187, '2022-04-06 01:26:49', '2022-05-06 01:26:49', 'time2.nimt.or.th', 1, 1, 'Asia/Bangkok', 30);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE `styles` (
  `id_st` int(11) NOT NULL,
  `nameclass` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `height` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `namelocation` varchar(45) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id_st`, `nameclass`, `height`, `namelocation`) VALUES
(123, ' ', '', 'under'),
(124, 'btn-dark', '', 'middle'),
(142, 'btn-dark', '', ''),
(144, 'btn-dark', '', ''),
(145, 'btn-dark', '', ''),
(146, 'btn-dark', '', ''),
(147, ' ', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE `timezone` (
  `time_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `timezone`
--

INSERT INTO `timezone` (`time_id`, `name`) VALUES
(4, 'Africa/Abidjan'),
(5, 'Africa/Accra'),
(6, 'Africa/Addis_Ababa'),
(7, 'Africa/Algiers'),
(8, 'Africa/Asmara'),
(9, 'Africa/Asmera'),
(10, 'Africa/Bamako'),
(11, 'Africa/Bangui'),
(12, 'Africa/Banjul'),
(13, 'Africa/Bissau'),
(14, 'Africa/Blantyre'),
(15, 'Africa/Brazzaville'),
(16, 'Africa/Bujumbura'),
(17, 'Africa/Cairo'),
(18, 'Africa/Casablanca'),
(19, 'Africa/Ceuta'),
(20, 'Africa/Conakry'),
(21, 'Africa/Dakar'),
(22, 'Africa/Dar_es_Salaam'),
(23, 'Africa/Djibouti'),
(24, 'Africa/Douala'),
(25, 'Africa/El_Aaiun'),
(26, 'Africa/Freetown'),
(27, 'Africa/Gaborone'),
(28, 'Africa/Harare'),
(29, 'Africa/Johannesburg'),
(30, 'Africa/Juba'),
(31, 'Africa/Kampala'),
(32, 'Africa/Khartoum'),
(33, 'Africa/Kigali'),
(34, 'Africa/Kinshasa'),
(35, 'Africa/Lagos'),
(36, 'Africa/Libreville'),
(37, 'Africa/Lome'),
(38, 'Africa/Luanda'),
(39, 'Africa/Lubumbashi'),
(40, 'Africa/Lusaka'),
(41, 'Africa/Malabo'),
(42, 'Africa/Maputo'),
(43, 'Africa/Maseru'),
(44, 'Africa/Mbabane'),
(45, 'Africa/Mogadishu'),
(46, 'Africa/Monrovia'),
(47, 'Africa/Nairobi'),
(48, 'Africa/Ndjamena'),
(49, 'Africa/Niamey'),
(50, 'Africa/Nouakchott'),
(51, 'Africa/Ouagadougou'),
(52, 'Africa/Porto-Novo'),
(53, 'Africa/Sao_Tome'),
(54, 'Africa/Timbuktu'),
(55, 'Africa/Tripoli'),
(56, 'Africa/Tunis'),
(57, 'Africa/Windhoek'),
(58, 'America/Adak'),
(59, 'America/Anchorage'),
(60, 'America/Anguilla'),
(61, 'America/Antigua'),
(62, 'America/Araguaina'),
(63, 'America/Argentina/Buenos_Aires'),
(64, 'America/Argentina/Catamarca'),
(65, 'America/Argentina/ComodRivadavia'),
(66, 'America/Argentina/Cordoba'),
(67, 'America/Argentina/Jujuy'),
(68, 'America/Argentina/La_Rioja'),
(69, 'America/Argentina/Mendoza'),
(70, 'America/Argentina/Rio_Gallegos'),
(71, 'America/Argentina/Salta'),
(72, 'America/Argentina/San_Juan'),
(73, 'America/Argentina/San_Luis'),
(74, 'America/Argentina/Tucuman'),
(75, 'America/Argentina/Ushuaia'),
(76, 'America/Aruba'),
(77, 'America/Asuncion'),
(78, 'America/Atikokan'),
(79, 'America/Atka'),
(80, 'America/Bahia'),
(81, 'America/Bahia_Banderas'),
(82, 'America/Barbados'),
(83, 'America/Belem'),
(84, 'America/Belize'),
(85, 'America/Blanc-Sablon'),
(86, 'America/Boa_Vista'),
(87, 'America/Bogota'),
(88, 'America/Boise'),
(89, 'America/Buenos_Aires'),
(90, 'America/Cambridge_Bay'),
(91, 'America/Campo_Grande'),
(92, 'America/Cancun'),
(93, 'America/Caracas'),
(94, 'America/Catamarca'),
(95, 'America/Cayenne'),
(96, 'America/Cayman'),
(97, 'America/Chicago'),
(98, 'America/Chihuahua'),
(99, 'America/Coral_Harbour'),
(100, 'America/Cordoba'),
(101, 'America/Costa_Rica'),
(102, 'America/Creston'),
(103, 'America/Cuiaba'),
(104, 'America/Curacao'),
(105, 'America/Danmarkshavn'),
(106, 'America/Dawson'),
(107, 'America/Dawson_Creek'),
(108, 'America/Denver'),
(109, 'America/Detroit'),
(110, 'America/Dominica'),
(111, 'America/Edmonton'),
(112, 'America/Eirunepe'),
(113, 'America/El_Salvador'),
(114, 'America/Ensenada'),
(115, 'America/Fort_Nelson'),
(116, 'America/Fort_Wayne'),
(117, 'America/Fortaleza'),
(118, 'America/Glace_Bay'),
(119, 'America/Godtha'),
(120, 'America/Goose_Bay'),
(121, 'America/Grand_Turk'),
(122, 'America/Grenada'),
(123, 'America/Guadeloupe'),
(124, 'America/Guatemala'),
(125, 'America/Guayaquil'),
(126, 'America/Guyana'),
(127, 'America/Halifax'),
(128, 'America/Havana'),
(129, 'America/Hermosillo'),
(130, 'America/Indiana/Indianapolis'),
(131, 'America/Indiana/Knox'),
(132, 'America/Indiana/Marengo'),
(133, 'America/Indiana/Petersburg'),
(134, 'America/Indiana/Tell_City'),
(135, 'America/Indiana/Vevay'),
(136, 'America/Indiana/Vincennes'),
(137, 'America/Indiana/Winamac'),
(138, 'America/Indianapolis'),
(139, 'America/Inuvik'),
(140, 'America/Iqaluit'),
(141, 'America/Jamaica'),
(142, 'America/Jujuy'),
(143, 'America/Juneau'),
(144, 'America/Kentucky/Louisville'),
(145, 'America/Kentucky/Monticello'),
(146, 'America/Knox_IN'),
(147, 'America/Kralendijk'),
(148, 'America/La_Paz'),
(149, 'America/Lima'),
(150, 'America/Los_Angeles'),
(151, 'America/Louisville'),
(152, 'America/Lower_Princes'),
(153, 'America/Maceio'),
(154, 'America/Managua'),
(155, 'America/Manaus'),
(156, 'America/Marigot'),
(157, 'America/Martinique'),
(158, 'America/Matamoros'),
(159, 'America/Mazatlan'),
(160, 'America/Mendoza'),
(161, 'America/Menominee'),
(162, 'America/Merida'),
(163, 'America/Metlakatla'),
(164, 'America/Mexico_City'),
(165, 'America/Miquelon'),
(166, 'America/Moncton'),
(167, 'America/Monterrey'),
(168, 'America/Montevideo'),
(169, 'America/Montreal'),
(170, 'America/Montserrat'),
(171, 'America/Nassau'),
(172, 'America/New_York'),
(173, 'America/Nipigon'),
(174, 'America/Nome'),
(175, 'America/Noronha'),
(176, 'America/North_Dakota/Beulah'),
(177, 'America/North_Dakota/Center'),
(178, 'America/North_Dakota/New_Salem'),
(179, 'America/Nuuk'),
(180, 'America/Ojinaga'),
(181, 'America/Panama'),
(182, 'America/Pangnirtung'),
(183, 'America/Paramaribo'),
(184, 'America/Phoenix'),
(185, 'America/Port-au-Prince'),
(186, 'America/Port_of_Spain'),
(187, 'America/Porto_Acre'),
(188, 'America/Porto_Velho'),
(189, 'America/Puerto_Rico'),
(190, 'America/Punta_Arenas'),
(191, 'America/Rainy_River'),
(192, 'America/Rankin_Inlet'),
(193, 'America/Recife'),
(194, 'America/Regina'),
(195, 'America/Resolute'),
(196, 'America/Rio_Branco'),
(197, 'America/Rosario'),
(198, 'America/Santa_Isabel'),
(199, 'America/Santarem'),
(200, 'America/Santiago'),
(201, 'America/Santo_Domingo'),
(202, 'America/Sao_Paulo'),
(203, 'America/Scoresbysund'),
(204, 'America/Shiprock'),
(205, 'America/Sitka'),
(206, 'America/St_Barthelemy'),
(207, 'America/St_Johns'),
(208, 'America/St_Kitts'),
(209, 'America/St_Lucia'),
(210, 'America/St_Thomas'),
(211, 'America/St_Vincent'),
(212, 'America/Swift_Current'),
(213, 'America/Tegucigalpa'),
(214, 'America/Thule'),
(215, 'America/Thunder_Bay'),
(216, 'America/Tijuana'),
(217, 'America/Toronto'),
(218, 'America/Tortola'),
(219, 'America/Vancouver'),
(220, 'America/Virgin'),
(221, 'America/Whitehorse'),
(222, 'America/Winnipeg'),
(223, 'America/Yakutat'),
(224, 'America/Yellowknife'),
(225, 'Antarctica/Casey'),
(226, 'Antarctica/Davis'),
(227, 'Antarctica/DumontDUrville'),
(228, 'Antarctica/Macquarie'),
(229, 'Antarctica/Mawson'),
(230, 'Antarctica/McMurdo'),
(231, 'Antarctica/Palmer'),
(232, 'Antarctica/Rothera'),
(233, 'Antarctica/South_Pole'),
(234, 'Antarctica/Syowa'),
(235, 'Antarctica/Troll'),
(236, 'Antarctica/Vostok'),
(237, 'Arctic/Longyearbyen'),
(238, 'Asia/Aden'),
(239, 'Asia/Almaty'),
(240, 'Asia/Amman'),
(241, 'Asia/Anadyr'),
(242, 'Asia/Aqtau'),
(243, 'Asia/Aqtobe'),
(244, 'Asia/Ashgabat'),
(245, 'Asia/Ashkhabad'),
(246, 'Asia/Atyrau'),
(247, 'Asia/Baghdad'),
(248, 'Asia/Bahrain'),
(249, 'Asia/Baku'),
(250, 'Asia/Bangkok'),
(251, 'Asia/Barnaul'),
(252, 'Asia/Beirut'),
(253, 'Asia/Bishkek'),
(254, 'Asia/Brunei'),
(255, 'Asia/Calcutta'),
(256, 'Asia/Chita'),
(257, 'Asia/Choibalsan'),
(258, 'Asia/Chongqing'),
(259, 'Asia/Chungking'),
(260, 'Asia/Colombo'),
(261, 'Asia/Dacca'),
(262, 'Asia/Damascus'),
(263, 'Asia/Dhaka'),
(264, 'Asia/Dili'),
(265, 'Asia/Dubai'),
(266, 'Asia/Dushanbe'),
(267, 'Asia/Famagusta'),
(268, 'Asia/Gaza'),
(269, 'Asia/Harbin'),
(270, 'Asia/Hebron'),
(271, 'Asia/Ho_Chi_Minh'),
(272, 'Asia/Hong_Kong'),
(273, 'Asia/Hovd'),
(274, 'Asia/Irkutsk'),
(275, 'Asia/Istanbul'),
(276, 'Asia/Jakarta'),
(277, 'Asia/Jayapura'),
(278, 'Asia/Jerusalem'),
(279, 'Asia/Kabul'),
(280, 'Asia/Kamchatka'),
(281, 'Asia/Karachi'),
(282, 'Asia/Kashgar'),
(283, 'Asia/Kathmandu'),
(284, 'Asia/Katmandu'),
(285, 'Asia/Khandyga'),
(286, 'Asia/Kolkata'),
(287, 'Asia/Krasnoyarsk'),
(288, 'Asia/Kuala_Lumpur'),
(289, 'Asia/Kuching'),
(290, 'Asia/Kuwait'),
(291, 'Asia/Macao'),
(292, 'Asia/Macau'),
(293, 'Asia/Magadan'),
(294, 'Asia/Makassar'),
(295, 'Asia/Manila'),
(296, 'Asia/Muscat'),
(297, 'Asia/Nicosia'),
(298, 'Asia/Novokuznetsk'),
(299, 'Asia/Novosibirsk'),
(300, 'Asia/Omsk'),
(301, 'Asia/Oral'),
(302, 'Asia/Phnom_Penh'),
(303, 'Asia/Pontianak'),
(304, 'Asia/Pyongyang'),
(305, 'Asia/Qatar'),
(306, 'Asia/Qostanay'),
(307, 'Asia/Qyzylorda'),
(308, 'Asia/Rangoon'),
(309, 'Asia/Riyadh'),
(310, 'Asia/Saigon'),
(311, 'Asia/Sakhalin'),
(312, 'Asia/Samarkand'),
(313, 'Asia/Seoul'),
(314, 'Asia/Shanghai'),
(315, 'Asia/Singapore'),
(316, 'Asia/Srednekolymsk'),
(317, 'Asia/Taipei'),
(318, 'Asia/Tashkent'),
(319, 'Asia/Tbilisi'),
(320, 'Asia/Tehran'),
(321, 'Asia/Tel_Aviv'),
(322, 'Asia/Thimbu'),
(323, 'Asia/Thimphu'),
(324, 'Asia/Tokyo'),
(325, 'Asia/Tomsk'),
(326, 'Asia/Ujung_Pandang'),
(327, 'Asia/Ulaanbaatar'),
(328, 'Asia/Ulan_Bator'),
(329, 'Asia/Urumqi'),
(330, 'Asia/Ust-Nera'),
(331, 'Asia/Vientiane'),
(332, 'Asia/Vladivostok'),
(333, 'Asia/Yakutsk'),
(334, 'Asia/Yangon'),
(335, 'Asia/Yekaterinburg'),
(336, 'Asia/Yerevan'),
(337, 'Atlantic/Azores'),
(338, 'Atlantic/Bermuda'),
(339, 'Atlantic/Canary'),
(340, 'Atlantic/Cape_Verde'),
(341, 'Atlantic/Faeroe'),
(342, 'Atlantic/Faroe'),
(343, 'Atlantic/Jan_Mayen'),
(344, 'Atlantic/Madeira'),
(345, 'Atlantic/Reykjavik'),
(346, 'Atlantic/South_Georgia'),
(347, 'Atlantic/St_Helena'),
(348, 'Atlantic/Stanley'),
(349, 'Australia/ACT'),
(350, 'Australia/Adelaide'),
(351, 'Australia/Brisbane'),
(352, 'Australia/Broken_Hill'),
(353, 'Australia/Canberra'),
(354, 'Australia/Currie'),
(355, 'Australia/Darwin'),
(356, 'Australia/Eucla'),
(357, 'Australia/Hobart'),
(358, 'Australia/LHI'),
(359, 'Australia/Lindeman'),
(360, 'Australia/Lord_Howe'),
(361, 'Australia/Melbourne'),
(362, 'Australia/NSW'),
(363, 'Australia/North'),
(364, 'Australia/Perth'),
(365, 'Australia/Queensland'),
(366, 'Australia/South'),
(367, 'Australia/Sydney'),
(368, 'Australia/Tasmania'),
(369, 'Australia/Victoria'),
(370, 'Australia/West'),
(371, 'Australia/Yancowinna'),
(372, 'Brazil/Acre'),
(373, 'Brazil/DeNoronha'),
(374, 'Brazil/East'),
(375, 'Brazil/West'),
(376, 'CET'),
(377, 'CST6CDT'),
(378, 'Canada/Atlantic'),
(379, 'Canada/Central'),
(380, 'Canada/Eastern'),
(381, 'Canada/Mountain'),
(382, 'Canada/Newfoundland'),
(383, 'Canada/Pacific'),
(384, 'Canada/Saskatchewan'),
(385, 'Canada/Yukon'),
(386, 'Chile/Continental'),
(387, 'Chile/EasterIsland'),
(388, 'Cuba'),
(389, 'EET'),
(390, 'EST'),
(391, 'EST5EDT'),
(392, 'Egypt'),
(393, 'Eire'),
(394, 'Etc/GMT'),
(395, 'Etc/GMT+0'),
(396, 'Etc/GMT+1'),
(397, 'Etc/GMT+10'),
(398, 'Etc/GMT+11'),
(399, 'Etc/GMT+12'),
(400, 'Etc/GMT+2'),
(401, 'Etc/GMT+3'),
(402, 'Etc/GMT+4'),
(403, 'Etc/GMT+5'),
(404, 'Etc/GMT+6'),
(405, 'Etc/GMT+7'),
(406, 'Etc/GMT+8'),
(407, 'Etc/GMT+9'),
(408, 'Etc/GMT-0'),
(409, 'Etc/GMT-1'),
(410, 'Etc/GMT-10'),
(411, 'Etc/GMT-11'),
(412, 'Etc/GMT-12'),
(413, 'Etc/GMT-13'),
(414, 'Etc/GMT-14'),
(415, 'Etc/GMT-2'),
(416, 'Etc/GMT-3'),
(417, 'Etc/GMT-4'),
(418, 'Etc/GMT-5'),
(419, 'Etc/GMT-6'),
(420, 'Etc/GMT-7'),
(421, 'Etc/GMT-8'),
(422, 'Etc/GMT-9'),
(423, 'Etc/GMT0'),
(424, 'Etc/Greenwich'),
(425, 'Etc/UCT'),
(426, 'Etc/UTC'),
(427, 'Etc/Universal'),
(428, 'Etc/Zulu'),
(429, 'Europe/Amsterdam'),
(430, 'Europe/Andorra'),
(431, 'Europe/Astrakhan'),
(432, 'Europe/Athens'),
(433, 'Europe/Belfast'),
(434, 'Europe/Belgrade'),
(435, 'Europe/Berlin'),
(436, 'Europe/Bratislava'),
(437, 'Europe/Brussels'),
(438, 'Europe/Bucharest'),
(439, 'Europe/Budapest'),
(440, 'Europe/Busingen'),
(441, 'Europe/Chisinau'),
(442, 'Europe/Copenhagen'),
(443, 'Europe/Dublin'),
(444, 'Europe/Gibraltar'),
(445, 'Europe/Guernsey'),
(446, 'Europe/Helsinki'),
(447, 'Europe/Isle_of_Man'),
(448, 'Europe/Istanbul'),
(449, 'Europe/Jersey'),
(450, 'Europe/Kaliningrad'),
(451, 'Europe/Kiev'),
(452, 'Europe/Kirov'),
(453, 'Europe/Lisbon'),
(454, 'Europe/Ljubljana'),
(455, 'Europe/London'),
(456, 'Europe/Luxembourg'),
(457, 'Europe/Madrid'),
(458, 'Europe/Malta'),
(459, 'Europe/Mariehamn'),
(460, 'Europe/Minsk'),
(461, 'Europe/Monaco'),
(462, 'Europe/Moscow'),
(463, 'Europe/Nicosia'),
(464, 'Europe/Oslo'),
(465, 'Europe/Paris'),
(466, 'Europe/Podgorica'),
(467, 'Europe/Prague'),
(468, 'Europe/Riga'),
(469, 'Europe/Rome'),
(470, 'Europe/Samara'),
(471, 'Europe/San_Marino'),
(472, 'Europe/Sarajevo'),
(473, 'Europe/Saratov'),
(474, 'Europe/Simferopol'),
(475, 'Europe/Skopje'),
(476, 'Europe/Sofia'),
(477, 'Europe/Stockholm'),
(478, 'Europe/Tallinn'),
(479, 'Europe/Tirane'),
(480, 'Europe/Tiraspol'),
(481, 'Europe/Ulyanovsk'),
(482, 'Europe/Uzhgorod'),
(483, 'Europe/Vaduz'),
(484, 'Europe/Vatican'),
(485, 'Europe/Vienna'),
(486, 'Europe/Vilnius'),
(487, 'Europe/Volgograd'),
(488, 'Europe/Warsaw'),
(489, 'Europe/Zagre'),
(490, 'Europe/Zaporozhye'),
(491, 'Europe/Zurich'),
(492, 'Factory'),
(493, 'GB'),
(494, 'GB-Eire'),
(495, 'GMT'),
(496, 'GMT+0'),
(497, 'GMT-0'),
(498, 'GMT0'),
(499, 'Greenwich'),
(500, 'HST'),
(501, 'Hongkong'),
(502, 'Iceland'),
(503, 'Indian/Antananarivo'),
(504, 'Indian/Chagos'),
(505, 'Indian/Christmas'),
(506, 'Indian/Cocos'),
(507, 'Indian/Comoro'),
(508, 'Indian/Kerguelen'),
(509, 'Indian/Mahe'),
(510, 'Indian/Maldives'),
(511, 'Indian/Mauritius'),
(512, 'Indian/Mayotte'),
(513, 'Indian/Reunion'),
(514, 'Iran'),
(515, 'Israel'),
(516, 'Jamaica'),
(517, 'Japan'),
(518, 'Kwajalein'),
(519, 'Libya'),
(520, 'MET'),
(521, 'MST'),
(522, 'MST7MDT'),
(523, 'Mexico/BajaNorte'),
(524, 'Mexico/BajaSur'),
(525, 'Mexico/General'),
(526, 'NZ'),
(527, 'NZ-CHAT'),
(528, 'Navajo'),
(529, 'PRC'),
(530, 'PST8PDT'),
(531, 'Pacific/Apia'),
(532, 'Pacific/Auckland'),
(533, 'Pacific/Bougainville'),
(534, 'Pacific/Chatham'),
(535, 'Pacific/Chuuk'),
(536, 'Pacific/Easter'),
(537, 'Pacific/Efate'),
(538, 'Pacific/Enderbury'),
(539, 'Pacific/Fakaofo'),
(540, 'Pacific/Fiji'),
(541, 'Pacific/Funafuti'),
(542, 'Pacific/Galapagos'),
(543, 'Pacific/Gambier'),
(544, 'Pacific/Guadalcanal'),
(545, 'Pacific/Guam'),
(546, 'Pacific/Honolulu'),
(547, 'Pacific/Johnston'),
(548, 'Pacific/Kanton'),
(549, 'Pacific/Kiritimati'),
(550, 'Pacific/Kosrae'),
(551, 'Pacific/Kwajalein'),
(552, 'Pacific/Majuro'),
(553, 'Pacific/Marquesas'),
(554, 'Pacific/Midway'),
(555, 'Pacific/Nauru'),
(556, 'Pacific/Niue'),
(557, 'Pacific/Norfolk'),
(558, 'Pacific/Noumea'),
(559, 'Pacific/Pago_Pago'),
(560, 'Pacific/Palau'),
(561, 'Pacific/Pitcairn'),
(562, 'Pacific/Pohnpei'),
(563, 'Pacific/Ponape'),
(564, 'Pacific/Port_Moresby'),
(565, 'Pacific/Rarotonga'),
(566, 'Pacific/Saipan'),
(567, 'Pacific/Samoa'),
(568, 'Pacific/Tahiti'),
(569, 'Pacific/Tarawa'),
(570, 'Pacific/Tongatapu'),
(571, 'Pacific/Truk'),
(572, 'Pacific/Wake'),
(573, 'Pacific/Wallis'),
(574, 'Pacific/Yap'),
(575, 'Poland'),
(576, 'Portugal'),
(577, 'ROC'),
(578, 'ROK'),
(579, 'Singapore'),
(580, 'SystemV/AST4'),
(581, 'SystemV/AST4ADT'),
(582, 'SystemV/CST6'),
(583, 'SystemV/CST6CDT'),
(584, 'SystemV/EST5'),
(585, 'SystemV/EST5EDT'),
(586, 'SystemV/HST10'),
(587, 'SystemV/MST7'),
(588, 'SystemV/MST7MDT'),
(589, 'SystemV/PST8'),
(590, 'SystemV/PST8PDT'),
(591, 'SystemV/YST9'),
(592, 'SystemV/YST9YDT'),
(593, 'Turkey'),
(594, 'UCT'),
(595, 'US/Alaska'),
(596, 'US/Aleutian'),
(597, 'US/Arizona'),
(598, 'US/Central'),
(599, 'US/East-Indiana'),
(600, 'US/Eastern'),
(601, 'US/Hawaii'),
(602, 'US/Indiana-Starke'),
(603, 'US/Michigan'),
(604, 'US/Mountain'),
(605, 'US/Pacific'),
(606, 'US/Samoa'),
(607, 'UTC'),
(608, 'Universal'),
(609, 'W-SU'),
(610, 'WET'),
(611, 'Zulu');

-- --------------------------------------------------------

--
-- Table structure for table `traffic`
--

CREATE TABLE `traffic` (
  `traffic_id` int(11) NOT NULL,
  `iface` varchar(255) NOT NULL,
  `rxpck/s` varchar(255) NOT NULL,
  `txpck/s` varchar(255) NOT NULL,
  `rxkB/s` varchar(255) NOT NULL,
  `txkB/s` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `traffic`
--

INSERT INTO `traffic` (`traffic_id`, `iface`, `rxpck/s`, `txpck/s`, `rxkB/s`, `txkB/s`) VALUES
(2, 'lo', '4.00', '4.00', '0.26', '0.26'),
(4, 'lo', '4.00', '4.00', '0.26', '0.26'),
(6, 'lo', '4.00', '4.00', '0.26', '0.26'),
(7, 'eth0', '0.00', '0.00', '0.00', '0.00'),
(8, 'eth1', '0.00', '1.00', '0.00', '0.07'),
(9, 'lo', '4.00', '4.00', '0.26', '0.26'),
(10, 'eth0', '0.00', '0.00', '0.00', '0.00'),
(11, 'eth1', '7.00', '7.00', '1.19', '1.50'),
(12, 'lo', '4.00', '4.00', '0.26', '0.26'),
(13, 'eth0', '0.00', '0.00', '0.00', '0.00'),
(14, 'eth1', '3.00', '5.00', '0.31', '0.69'),
(15, 'lo', '3.00', '3.00', '0.21', '0.21'),
(16, 'eth0', '0.00', '0.00', '0.00', '0.00'),
(17, 'eth1', '2.97', '3.96', '0.31', '0.40'),
(18, 'lo', '8.91', '8.91', '0.77', '0.77'),
(19, 'eth0', '0.00', '0.00', '0.00', '0.00'),
(20, 'eth1', '4.00', '4.00', '0.21', '0.37'),
(21, 'lo', '9.00', '9.00', '0.78', '0.78'),
(22, 'eth0', '0.00', '0.00', '0.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `undevice`
--

CREATE TABLE `undevice` (
  `undevice_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `ip` varchar(255) NOT NULL,
  `data_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_u` int(11) NOT NULL,
  `firstname_u` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `lastname_u` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `application_date` datetime DEFAULT current_timestamp(),
  `login_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_u`, `firstname_u`, `lastname_u`, `application_date`, `login_id`) VALUES
(1, 'วิทยา', 'เกสรกติกา', '2022-02-04 00:22:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_member`
--

CREATE TABLE `user_member` (
  `um_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_member`
--

INSERT INTO `user_member` (`um_id`, `acc_id`, `group_id`) VALUES
(144, 176, 38),
(145, 178, 41);

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `views_id` int(11) NOT NULL,
  `timestamp` varchar(255) NOT NULL,
  `source` mediumtext NOT NULL,
  `destination` mediumtext NOT NULL,
  `route` mediumtext NOT NULL,
  `packet` mediumtext NOT NULL,
  `time` mediumtext NOT NULL,
  `protocol` mediumtext NOT NULL,
  `other` mediumtext NOT NULL,
  `msg` mediumtext NOT NULL,
  `device_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `views`
--

INSERT INTO `views` (`views_id`, `timestamp`, `source`, `destination`, `route`, `packet`, `time`, `protocol`, `other`, `msg`, `device_id`, `date`) VALUES
(145109, '1649182464.193', '172.16.10.53', 'http://ipv6.msftconnecttest.com/connecttest.txt', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145110, '1649182464.277', '172.16.10.53', 'http://www.msftconnecttest.com/connecttest.txt', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145111, '1649182528.426', '172.16.10.53', 'v10.events.data.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145112, '1649182542.167', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145113, '1649182542.496', '172.16.10.53', 'smartscreen-prod.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145114, '1649182547.002', '172.16.10.53', 'cdn.fbsbx.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145115, '1649182547.002', '172.16.10.53', 'fonts.googleapis.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145116, '1649182547.003', '172.16.10.53', 'fonts.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145117, '1649182547.003', '172.16.10.53', 'fonts.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145118, '1649182547.003', '172.16.10.53', 'oss.maxcdn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145119, '1649182547.004', '172.16.10.53', 'functional.events.data.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145120, '1649182547.008', '172.16.10.53', 'scontent.fbkk10-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145121, '1649182547.008', '172.16.10.53', 'video.fbkk14-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145122, '1649182547.009', '172.16.10.53', 'www.facebook.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145123, '1649182547.010', '172.16.10.53', 'static.xx.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145124, '1649182547.010', '172.16.10.53', 'scontent.xx.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145125, '1649182547.010', '172.16.10.53', 'www.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145126, '1649182547.010', '172.16.10.53', 'scontent.xx.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145127, '1649182547.010', '172.16.10.53', 'scontent.fbkk14-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145128, '1649182572.409', '172.16.10.53', 'http://172.16.20.230/', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145129, '1649182572.538', '172.16.10.53', 'http://172.16.20.230/icons/ubuntu-logo.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145130, '1649182572.600', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145131, '1649182572.696', '172.16.10.53', 'http://172.16.20.230/favicon.ico', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145132, '1649182578.572', '172.16.10.53', 'http://172.16.20.230:8081/', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145133, '1649182578.658', '172.16.10.53', 'http://proxy:3128/squid-internal-static/icons/SN.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145134, '1649182578.753', '172.16.10.53', 'http://172.16.20.230:8081/favicon.ico', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145135, '1649182584.661', '172.16.10.53', 'http://172.16.20.230:8080/', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145136, '1649182584.730', '172.16.10.53', 'http://proxy:3128/squid-internal-static/icons/SN.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145137, '1649182584.806', '172.16.10.53', 'http://172.16.20.230:8080/favicon.ico', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145138, '1649182597.902', '172.16.10.53', 'activity.windows.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145139, '1649182602.401', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145140, '1649182607.011', '172.16.10.53', 'rr1---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145141, '1649182608.108', '172.16.10.53', 'ssl.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145142, '1649182614.435', '172.16.10.53', 'cdn.fbsbx.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145143, '1649182617.056', '172.16.10.53', 'static.xx.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145144, '1649182663.539', '172.16.10.53', 'fonts.googleapis.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145145, '1649182663.539', '172.16.10.53', 'fonts.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145146, '1649182663.539', '172.16.10.53', 'fonts.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145147, '1649182667.959', '172.16.10.53', 'scontent.fbkk14-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145148, '1649182668.064', '172.16.10.53', 'scontent.fbkk10-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145149, '1649182734.580', '172.16.10.53', 'functional.events.data.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145150, '1649182753.744', '172.16.10.53', 'r.bing.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145151, '1649182753.750', '172.16.10.53', 'r.bing.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145152, '1649182753.753', '172.16.10.53', 'r.msftstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145153, '1649182753.755', '172.16.10.53', 'r.msftstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145154, '1649182753.874', '172.16.10.53', 'rr1---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145155, '1649182753.875', '172.16.10.53', 'jnn-pa.googleapis.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145156, '1649182753.876', '172.16.10.53', 'assets.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145157, '1649182753.877', '172.16.10.53', 'www.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145158, '1649182753.878', '172.16.10.53', 'scontent.fbkk14-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145159, '1649182753.879', '172.16.10.53', 'www.youtube.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145160, '1649182753.880', '172.16.10.53', 'play.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145161, '1649182753.881', '172.16.10.53', 'lh3.googleusercontent.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145162, '1649182753.881', '172.16.10.53', 'ogs.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145163, '1649182753.881', '172.16.10.53', 'www.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145164, '1649182753.881', '172.16.10.53', 'www.facebook.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145165, '1649182753.882', '172.16.10.53', 'i.ytimg.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145166, '1649182753.887', '172.16.10.53', 'yt3.ggpht.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145167, '1649182753.887', '172.16.10.53', 'accounts.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145168, '1649182753.887', '172.16.10.53', 'play.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145169, '1649182753.887', '172.16.10.53', 'c.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145170, '1649182753.887', '172.16.10.53', 'www.bing.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145171, '1649182753.887', '172.16.10.53', 'sb.scorecardresearch.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145172, '1649182753.887', '172.16.10.53', 'srtb.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145173, '1649182753.887', '172.16.10.53', 'i1.ytimg.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145174, '1649182753.887', '172.16.10.53', 'scontent.fbkk10-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145175, '1649182753.889', '172.16.10.53', 'www.bing.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145176, '1649182754.703', '172.16.10.53', 'cat.sg1.as.criteo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145177, '1649182755.323', '172.16.10.53', 'stas.outbrain.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145178, '1649182755.349', '172.16.10.53', 'stas.outbrain.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145179, '1649182756.520', '172.16.10.53', 'browser.events.data.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145180, '1649182757.366', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145181, '1649182757.738', '172.16.10.53', 'assets.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145182, '1649182757.739', '172.16.10.53', 'b1-sadc1.zemanta.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145183, '1649182757.739', '172.16.10.53', 'b1-sadc1.zemanta.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145184, '1649182757.740', '172.16.10.53', 'b1t-sadc1.zemanta.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145185, '1649182757.741', '172.16.10.53', 'b1t-sadc1.zemanta.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145186, '1649182757.741', '172.16.10.53', 'b1t-sadc1.zemanta.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145187, '1649182758.161', '172.16.10.53', 'cdn.1112.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145188, '1649182758.162', '172.16.10.53', 'cdn.1112.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145189, '1649182758.162', '172.16.10.53', 'cdn.1112.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145190, '1649182758.162', '172.16.10.53', 'cdn.1112.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145191, '1649182758.162', '172.16.10.53', 'cdn.1112.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145192, '1649182760.195', '172.16.10.53', 'browser.events.data.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145193, '1649182761.550', '172.16.10.53', 'browser.events.data.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145194, '1649182761.792', '172.16.10.53', 'c.bing.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145195, '1649182762.200', '172.16.10.53', 'fonts.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145196, '1649182762.435', '172.16.10.53', 'img-s-msn-com.akamaized.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145197, '1649182762.440', '172.16.10.53', 'sb.scorecardresearch.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145198, '1649182762.837', '172.16.10.53', 'srtb.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145199, '1649182764.767', '172.16.10.53', 'cat.sg1.as.criteo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145200, '1649182764.769', '172.16.10.53', 'pix.as.criteo.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145201, '1649182764.824', '172.16.10.53', 'www.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145202, '1649182765.375', '172.16.10.53', 'http://119.81.44.155/phpmyadmin', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145203, '1649182765.785', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145204, '1649182765.794', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145205, '1649182766.107', '172.16.10.53', 'rr2---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145206, '1649182766.107', '172.16.10.53', 'ssl.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145207, '1649182766.108', '172.16.10.53', 'www.googleadservices.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145208, '1649182766.159', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/jquery-ui.css', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145209, '1649182766.189', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145210, '1649182766.233', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/lib/codemirror.css?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145211, '1649182766.371', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/lint/lint.css?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145212, '1649182766.371', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/hint/show-hint.css?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145213, '1649182766.417', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery-migrate.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145214, '1649182766.434', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/whitelist.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145215, '1649182766.465', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.min.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145216, '1649182766.496', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/phpmyadmin.css.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145217, '1649182766.595', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/keyhandler.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145218, '1649182766.599', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/ajax.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145219, '1649182766.602', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/sprintf.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145220, '1649182766.636', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/js.cookie.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145221, '1649182766.641', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.mousewheel.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145222, '1649182766.723', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery-ui.min.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145223, '1649182766.813', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.event.drag-2.2.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145224, '1649182766.815', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.ba-hashchange-1.3.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145225, '1649182766.816', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.validate.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145226, '1649182766.816', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.debounce-1.0.5.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145227, '1649182766.828', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery-ui-timepicker-addon.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145228, '1649182766.907', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/menu-resizer.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145229, '1649182766.911', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/cross_framing_protection.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145230, '1649182766.931', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/error_report.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145231, '1649182766.936', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/tracekit.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145232, '1649182766.946', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/rte.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145233, '1649182766.990', '172.16.10.53', 'functional.events.data.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145234, '1649182767.024', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/messages.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145235, '1649182767.148', '172.16.10.53', 'www.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145236, '1649182767.236', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/doclinks.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145237, '1649182767.238', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/config.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145238, '1649182767.255', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/common.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145239, '1649182767.257', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/navigation.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145240, '1649182767.295', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/indexes.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145241, '1649182767.301', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/functions.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145242, '1649182767.388', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/page_settings.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145243, '1649182767.396', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/shortcuts_handler.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145244, '1649182767.422', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/mode/sql/sql.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145245, '1649182767.543', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/hint/sql-hint.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145246, '1649182767.544', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/runmode/runmode.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145247, '1649182767.545', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/hint/show-hint.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145248, '1649182767.546', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/codemirror/addon/lint/sql-lint.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145249, '1649182767.550', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/lib/codemirror.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145250, '1649182767.582', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/codemirror/addon/lint/lint.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145251, '1649182767.646', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/console.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145252, '1649182767.681', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/logo_right.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145253, '1649182767.764', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/dot.gif', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145254, '1649182767.794', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/css/printview.css?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145255, '1649182767.871', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_help.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145256, '1649182770.683', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145257, '1649182770.971', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145258, '1649182771.132', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/phpmyadmin.css.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145259, '1649182771.148', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/logo_left.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145260, '1649182771.199', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145261, '1649182771.200', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145262, '1649182771.351', '172.16.10.53', 'rr2---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145263, '1649182771.391', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_docs.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145264, '1649182771.435', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_loggoff.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145265, '1649182771.435', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_home.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145266, '1649182771.437', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_sqlhelp.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145267, '1649182771.437', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/left_nav_bg.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145268, '1649182771.522', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_newdb.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145269, '1649182771.540', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_reload.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145270, '1649182771.543', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_link.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145271, '1649182771.544', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_db.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145272, '1649182771.550', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_replication.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145273, '1649182771.552', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_plus.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145274, '1649182771.701', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/ajax.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145275, '1649182771.843', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_sql.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145276, '1649182771.844', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_import.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145277, '1649182771.846', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_tblops.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145278, '1649182771.847', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_tbl.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145279, '1649182771.859', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_plugin.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145280, '1649182771.884', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_status.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145281, '1649182771.890', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_host.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145282, '1649182771.998', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_export.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145283, '1649182771.999', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_engine.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145284, '1649182772.000', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_rights.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145285, '1649182772.001', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/console.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145286, '1649182772.037', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_vars.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145287, '1649182772.042', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_top.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145288, '1649182772.061', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_asci.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145289, '1649182772.113', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/ajax.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145290, '1649182772.149', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_theme.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145291, '1649182772.151', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_passwd.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145292, '1649182772.152', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/window-new.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145293, '1649182772.152', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_lang.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145294, '1649182772.153', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_error.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145295, '1649182772.195', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_cog.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145296, '1649182772.250', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_more.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145297, '1649182772.275', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/navigation.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145298, '1649182772.409', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/ajax.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145299, '1649182773.754', '172.16.10.53', 'login.live.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145300, '1649182773.813', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/db_structure.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145301, '1649182774.028', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_props.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145302, '1649182774.031', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_search.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145303, '1649182774.032', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_routines.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145304, '1649182774.034', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_events.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145305, '1649182774.034', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_relations.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145306, '1649182774.035', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_triggers.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145307, '1649182774.131', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/navigation.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145308, '1649182774.132', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/ajax_clock_small.gif', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145309, '1649182774.138', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_no_favorite.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145310, '1649182774.142', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_select.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145311, '1649182774.143', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_browse.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145312, '1649182774.173', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_asc.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145313, '1649182774.221', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_empty.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145314, '1649182774.221', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_insrow.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145315, '1649182774.226', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_drop.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145316, '1649182774.241', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/bd_select.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145317, '1649182774.262', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/bd_empty.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145318, '1649182774.265', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/bd_browse.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145319, '1649182774.311', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_print.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145320, '1649182774.332', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/db_structure.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145321, '1649182774.349', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_tblanalyse.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145322, '1649182774.352', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_table_add.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145323, '1649182774.355', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/tbl_change.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145324, '1649182774.388', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_minus.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145325, '1649182774.447', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/arrow_ltr.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145326, '1649182775.353', '172.16.10.53', 'login.live.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145327, '1649182775.363', '172.16.10.53', 'login.live.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145328, '1649182776.664', '172.16.10.53', 'assets.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145329, '1649182778.369', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/db_structure.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145330, '1649182780.156', '172.16.10.53', 'assets.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145331, '1649182780.334', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/db_export.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145332, '1649182780.532', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/export.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145333, '1649182782.771', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/export.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145334, '1649182782.840', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145335, '1649182782.938', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145336, '1649182783.280', '172.16.10.53', 'array605.prod.do.dsp.mp.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145337, '1649182817.496', '172.16.10.53', 'activity.windows.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145338, '1649182845.275', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145339, '1649182845.352', '172.16.10.53', 'rr2---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145340, '1649182845.379', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_tblexport.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145341, '1649182845.382', '172.16.10.53', 'www.bing.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145342, '1649182845.382', '172.16.10.53', 'yt3.ggpht.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145343, '1649182845.382', '172.16.10.53', 'www.facebook.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145344, '1649182845.382', '172.16.10.53', 'lh3.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145345, '1649182845.382', '172.16.10.53', 'cdn.1112.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145346, '1649182845.383', '172.16.10.53', 'www.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145347, '1649182845.383', '172.16.10.53', 'c.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145348, '1649182845.383', '172.16.10.53', 'zem.outbrainimg.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145349, '1649182845.383', '172.16.10.53', 'scontent.fbkk10-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145350, '1649182845.383', '172.16.10.53', 'srtb.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145351, '1649182845.383', '172.16.10.53', 'scontent.xx.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145352, '1649182845.383', '172.16.10.53', 'play.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145353, '1649182845.383', '172.16.10.53', 'obs.cheqzone.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145354, '1649182845.383', '172.16.10.53', 'www.1112.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145355, '1649182845.383', '172.16.10.53', 'lh3.googleusercontent.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145356, '1649182845.384', '172.16.10.53', 'ntp.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145357, '1649182845.384', '172.16.10.53', 'edge.activity.windows.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145358, '1649182845.384', '172.16.10.53', 'i.ytimg.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145359, '1649182845.384', '172.16.10.53', 'www.gstatic.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145360, '1649182845.385', '172.16.10.53', 'static.xx.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145361, '1649182845.385', '172.16.10.53', 'c.bing.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145362, '1649182845.385', '172.16.10.53', 'www.youtube.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145363, '1649182845.387', '172.16.10.53', 'adservice.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145364, '1649182845.388', '172.16.10.53', 'api.msn.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145365, '1649182845.388', '172.16.10.53', 'www.bing.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145366, '1649182845.388', '172.16.10.53', 'edge.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145367, '1649182845.393', '172.16.10.53', 'ogs.google.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145368, '1649182845.393', '172.16.10.53', 'static.cloudflareinsights.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145369, '1649182845.425', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_tblimport.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145370, '1649182845.468', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_success.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145371, '1649182845.480', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_chart.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145372, '1649182845.482', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_edit.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145373, '1649182845.521', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/b_view_add.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145374, '1649182845.586', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/gis_data_editor.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145375, '1649182845.594', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/multi_column_sort.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145376, '1649182845.595', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/vendor/jquery/jquery.uitablefilter.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145377, '1649182845.602', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145378, '1649182845.628', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_fulltext.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145379, '1649182845.679', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/sql.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145380, '1649182845.718', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/js/makegrid.js?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145381, '1649182845.869', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/col_drop.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145382, '1649182845.912', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/col_pointer.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145383, '1649182849.151', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145384, '1649182849.177', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145385, '1649182863.638', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-bg_glass_75_dadada_1x400.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145386, '1649182863.639', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-bg_highlight-soft_75_cccccc_1x100.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145387, '1649182863.639', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-bg_glass_75_e6e6e6_1x400.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145388, '1649182863.642', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-icons_222222_256x240.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145389, '1649182863.643', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-icons_888888_256x240.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145390, '1649182874.569', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/jquery/images/ui-bg_glass_65_ffffff_1x400.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145391, '1649182891.558', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_lock.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145392, '1649182892.169', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/db_sql_autocomplete.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145393, '1649182892.207', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145394, '1649182896.066', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145395, '1649182896.792', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145396, '1649182897.890', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145397, '1649182899.059', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145398, '1649182900.125', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145399, '1649182900.993', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145400, '1649182902.031', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145401, '1649182903.104', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145402, '1649182904.011', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145403, '1649182906.051', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145404, '1649182906.123', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/themes/pmahomme/img/s_unlink.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145405, '1649182912.328', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/import.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145406, '1649182915.726', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145407, '1649182917.389', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/import.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145408, '1649182925.352', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145409, '1649182928.558', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145410, '1649182930.504', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/import.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145411, '1649182930.744', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145412, '1649182940.462', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145413, '1649182940.711', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145414, '1649182944.026', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145415, '1649182944.066', '172.16.10.53', 'static.xx.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145416, '1649182944.272', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145417, '1649182945.214', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145418, '1649182945.433', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145419, '1649182948.190', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145420, '1649182948.442', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145421, '1649182949.677', '172.16.10.53', 'activity.windows.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145422, '1649182950.482', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145423, '1649182950.804', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145424, '1649182951.881', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145425, '1649182952.089', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145426, '1649182953.436', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145427, '1649182953.639', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145428, '1649182956.796', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145429, '1649182956.984', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145430, '1649182959.725', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145431, '1649182959.897', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145432, '1649182961.654', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145433, '1649182961.975', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145434, '1649182963.262', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145435, '1649182963.506', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145436, '1649182964.337', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145437, '1649182964.537', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145438, '1649182967.397', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145439, '1649182967.611', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145440, '1649182969.200', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145441, '1649182969.428', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145442, '1649182970.676', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145443, '1649182970.901', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145444, '1649182972.575', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145445, '1649182972.657', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145446, '1649182973.415', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/db_sql_autocomplete.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145447, '1649182973.806', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145448, '1649182976.225', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145449, '1649182978.915', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145450, '1649182979.780', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/lint.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145451, '1649182981.514', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/import.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145452, '1649182981.786', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54');
INSERT INTO `views` (`views_id`, `timestamp`, `source`, `destination`, `route`, `packet`, `time`, `protocol`, `other`, `msg`, `device_id`, `date`) VALUES
(145453, '1649182984.658', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145454, '1649182984.876', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145455, '1649182994.097', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145456, '1649182994.286', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145457, '1649182998.137', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145458, '1649182998.314', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145459, '1649182998.935', '172.16.10.53', 'scontent.fbkk14-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145460, '1649183001.224', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145461, '1649183001.346', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145462, '1649183002.734', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145463, '1649183002.966', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145464, '1649183005.732', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145465, '1649183006.132', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145466, '1649183010.564', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145467, '1649183010.815', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145468, '1649183014.477', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145469, '1649183014.912', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145470, '1649183016.598', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145471, '1649183016.806', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145472, '1649183019.709', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145473, '1649183019.849', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145474, '1649183021.191', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145475, '1649183021.343', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145476, '1649183022.959', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145477, '1649183023.169', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145478, '1649183024.844', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145479, '1649183025.057', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145480, '1649183027.096', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145481, '1649183027.266', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145482, '1649183028.390', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145483, '1649183031.773', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145484, '1649183031.951', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145485, '1649183032.868', '172.16.10.53', 'rr2---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145486, '1649183033.670', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145487, '1649183033.784', '172.16.10.53', 'update.code.visualstudio.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145488, '1649183033.883', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145489, '1649183036.402', '172.16.10.53', 'scontent.fbkk10-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145490, '1649183037.784', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145491, '1649183037.844', '172.16.10.53', 'activity.windows.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145492, '1649183037.929', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145493, '1649183040.145', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145494, '1649183040.616', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145495, '1649183041.933', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145496, '1649183042.196', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145497, '1649183044.186', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145498, '1649183044.367', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145499, '1649183045.519', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145500, '1649183045.652', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145501, '1649183046.854', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145502, '1649183047.104', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145503, '1649183048.911', '172.16.10.53', 'array614.prod.do.dsp.mp.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145504, '1649183049.852', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145505, '1649183050.042', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145506, '1649183051.391', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145507, '1649183054.250', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145508, '1649183054.444', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145509, '1649183055.729', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145510, '1649183055.953', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145511, '1649183057.256', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145512, '1649183057.488', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145513, '1649183057.993', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145514, '1649183058.242', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145515, '1649183063.023', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145516, '1649183063.253', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145517, '1649183073.186', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145518, '1649183073.436', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145519, '1649183074.190', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145520, '1649183074.484', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145521, '1649183079.370', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145522, '1649183080.031', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145523, '1649183083.171', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/export.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145524, '1649183084.199', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145525, '1649183084.479', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145526, '1649183087.955', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/tbl_row_action.php', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145527, '1649183088.095', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145528, '1649183089.596', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/sql.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145529, '1649183089.828', '172.16.10.53', 'http://119.81.44.155/phpmyadmin/index.php?', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145530, '1649183100.792', '172.16.10.53', 'nav.smartscreen.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145531, '1649183111.358', '172.16.10.53', 'rr2---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145532, '1649183115.998', '172.16.10.53', 'v10.events.data.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145533, '1649183119.744', '172.16.10.53', 'rr1---sn-npoeenle.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145534, '1649183128.493', '172.16.10.53', 'scontent.fbkk14-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145535, '1649183132.366', '172.16.10.53', 'edge.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145536, '1649183148.702', '172.16.10.53', 'settings-win.data.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145537, '1649183160.653', '172.16.10.53', 'http://119.81.44.155:8081/', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145538, '1649183160.968', '172.16.10.53', 'http://proxy:3128/squid-internal-static/icons/SN.png', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145539, '1649183179.547', '172.16.10.53', 'http://119.81.44.155:8081/', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145540, '1649183179.734', '172.16.10.53', 'http://119.81.44.155:8081/favicon.ico', '-', '-', '-', '-', '-', '-', 94, '2022-04-06 03:29:54'),
(145541, '1649183217.586', '172.16.10.53', 'static.xx.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145542, '1649183218.295', '172.16.10.53', 'www.facebook.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145543, '1649183233.301', '172.16.10.53', 'onedriveclucproddm20026.blob.core.windows.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145544, '1649183260.256', '172.16.10.53', 'rr2---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145545, '1649183284.576', '172.16.10.53', 'scontent.fbkk14-1.fna.fbcdn.net', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145546, '1649183293.556', '172.16.10.53', 'functional.events.data.microsoft.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54'),
(145547, '1649183294.837', '172.16.10.53', 'rr2---sn-j5u-joml.googlevideo.com', '-', '-', '-', '443', '-', '-', 94, '2022-04-06 03:29:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`acc_id`);

--
-- Indexes for table `alert`
--
ALTER TABLE `alert`
  ADD PRIMARY KEY (`alert_id`);

--
-- Indexes for table `blocked`
--
ALTER TABLE `blocked`
  ADD PRIMARY KEY (`b_id`),
  ADD KEY `input_id` (`input_id`);

--
-- Indexes for table `checktime`
--
ALTER TABLE `checktime`
  ADD PRIMARY KEY (`check_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Indexes for table `cookiepolicy`
--
ALTER TABLE `cookiepolicy`
  ADD PRIMARY KEY (`id_cp`),
  ADD KEY `cookiepolicy_key_domaingroup_idx` (`domaingroup_id`);

--
-- Indexes for table `data_out`
--
ALTER TABLE `data_out`
  ADD PRIMARY KEY (`out_id`);

--
-- Indexes for table `del_acc`
--
ALTER TABLE `del_acc`
  ADD PRIMARY KEY (`del_id`);

--
-- Indexes for table `del_device`
--
ALTER TABLE `del_device`
  ADD PRIMARY KEY (`del_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`device_id`),
  ADD KEY `input_id` (`input_id`);

--
-- Indexes for table `device_member`
--
ALTER TABLE `device_member`
  ADD PRIMARY KEY (`dm_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `de_id` (`de_id`);

--
-- Indexes for table `dialog`
--
ALTER TABLE `dialog`
  ADD PRIMARY KEY (`id_dl`);

--
-- Indexes for table `doc_pdpa_chat`
--
ALTER TABLE `doc_pdpa_chat`
  ADD PRIMARY KEY (`chat_id`),
  ADD KEY `doc_pdpa_chat1_idx` (`ref_id`),
  ADD KEY `doc_pdpa_chat2_idx` (`user_id`);

--
-- Indexes for table `doc_pdpa_classification`
--
ALTER TABLE `doc_pdpa_classification`
  ADD PRIMARY KEY (`classify_id`),
  ADD KEY `pattern_id` (`pattern_id`),
  ADD KEY `event_process_id` (`event_process_id`),
  ADD KEY `pattern_processing_base_id` (`pattern_processing_base_id`),
  ADD KEY `classification_special_conditions_id` (`classification_special_conditions_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `doc_pdpa_classification_special_conditions`
--
ALTER TABLE `doc_pdpa_classification_special_conditions`
  ADD PRIMARY KEY (`classification_special_conditions_id`);

--
-- Indexes for table `doc_pdpa_data`
--
ALTER TABLE `doc_pdpa_data`
  ADD PRIMARY KEY (`data_id`,`data_level_id`),
  ADD KEY `doc_pdpa_data_idx` (`data_type_id`),
  ADD KEY `doc_pdpa_data1_idx` (`data_level_id`);

--
-- Indexes for table `doc_pdpa_database_check`
--
ALTER TABLE `doc_pdpa_database_check`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_pdpa_data_type`
--
ALTER TABLE `doc_pdpa_data_type`
  ADD PRIMARY KEY (`data_type_id`);

--
-- Indexes for table `doc_pdpa_document`
--
ALTER TABLE `doc_pdpa_document`
  ADD PRIMARY KEY (`doc_id`),
  ADD KEY `doc_pdpa_document_idx` (`doc_type_id`),
  ADD KEY `doc_pdpa_document1_idx` (`user_id`);

--
-- Indexes for table `doc_pdpa_document_log`
--
ALTER TABLE `doc_pdpa_document_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `doc_pdpa_document_log_idx` (`doc_id`),
  ADD KEY `doc_pdpa_document_log1_idx` (`user_id`),
  ADD KEY `doc_pdpa_document_log2_idx` (`user_id_dest`);

--
-- Indexes for table `doc_pdpa_document_page`
--
ALTER TABLE `doc_pdpa_document_page`
  ADD PRIMARY KEY (`page_id`),
  ADD KEY `doc_pdpa_document_page_idx` (`doc_id`);

--
-- Indexes for table `doc_pdpa_document_type`
--
ALTER TABLE `doc_pdpa_document_type`
  ADD PRIMARY KEY (`doc_type_id`);

--
-- Indexes for table `doc_pdpa_event_process`
--
ALTER TABLE `doc_pdpa_event_process`
  ADD PRIMARY KEY (`event_process_id`);

--
-- Indexes for table `doc_pdpa_file_dir`
--
ALTER TABLE `doc_pdpa_file_dir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_pdpa_level`
--
ALTER TABLE `doc_pdpa_level`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `doc_pdpa_log0_hash`
--
ALTER TABLE `doc_pdpa_log0_hash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_pdpa_pattern`
--
ALTER TABLE `doc_pdpa_pattern`
  ADD PRIMARY KEY (`pattern_id`),
  ADD KEY `pattern_processing_base_id` (`pattern_processing_base_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `doc_id` (`doc_id`);

--
-- Indexes for table `doc_pdpa_pattern_processing_base`
--
ALTER TABLE `doc_pdpa_pattern_processing_base`
  ADD PRIMARY KEY (`pattern_processing_base_id`);

--
-- Indexes for table `doc_pdpa_position`
--
ALTER TABLE `doc_pdpa_position`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `doc_pdpa_tag`
--
ALTER TABLE `doc_pdpa_tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `doc_pdpa_user`
--
ALTER TABLE `doc_pdpa_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `doc_pdpa_user_type_idx` (`user_type_id`),
  ADD KEY `position_id` (`position_id`);

--
-- Indexes for table `doc_pdpa_user_type`
--
ALTER TABLE `doc_pdpa_user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- Indexes for table `doc_pdpa_words`
--
ALTER TABLE `doc_pdpa_words`
  ADD PRIMARY KEY (`words_id`);

--
-- Indexes for table `domaingroup`
--
ALTER TABLE `domaingroup`
  ADD PRIMARY KEY (`id_dg`);

--
-- Indexes for table `domain_setting_cookiepolicy`
--
ALTER TABLE `domain_setting_cookiepolicy`
  ADD PRIMARY KEY (`id_dsc`),
  ADD KEY `setting_domain_key_idx` (`domain_id`),
  ADD KEY `setting_cookiepolicy_key_idx` (`cookiepolicy_id`),
  ADD KEY `setting_policylog_key_idx` (`policylog_id`);

--
-- Indexes for table `domain_setting_tag`
--
ALTER TABLE `domain_setting_tag`
  ADD PRIMARY KEY (`id_dst`),
  ADD KEY `setting_tag_domain_idx` (`domaingroup_id`),
  ADD KEY `setting_tag_tag_idx` (`tag_id`);

--
-- Indexes for table `domain_style_policy`
--
ALTER TABLE `domain_style_policy`
  ADD PRIMARY KEY (`id_dsp`),
  ADD KEY `center_style_idx` (`style_id`),
  ADD KEY `center_domaingroup_key_idx` (`domaingroup_id`),
  ADD KEY `center_dialog_key_idx` (`dialog_id`);

--
-- Indexes for table `exporthistory`
--
ALTER TABLE `exporthistory`
  ADD PRIMARY KEY (`exp_id`),
  ADD KEY `acc_id` (`acc_id`),
  ADD KEY `file_id` (`file_id`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Indexes for table `ftp`
--
ALTER TABLE `ftp`
  ADD PRIMARY KEY (`ftp_id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `um_id` (`g_name`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`ht_id`),
  ADD KEY `acc_id` (`acc_id`);

--
-- Indexes for table `input`
--
ALTER TABLE `input`
  ADD PRIMARY KEY (`input_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `device_id` (`device_id`);

--
-- Indexes for table `log_history`
--
ALTER TABLE `log_history`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `log_domain_key_idx` (`domain_id`),
  ADD KEY `log_user_key_idx` (`user_id`);

--
-- Indexes for table `muitifactor`
--
ALTER TABLE `muitifactor`
  ADD PRIMARY KEY (`m_id`),
  ADD KEY `acc_id` (`acc_id`);

--
-- Indexes for table `pdpa_appeal`
--
ALTER TABLE `pdpa_appeal`
  ADD PRIMARY KEY (`id_ap`);

--
-- Indexes for table `pdpa_email`
--
ALTER TABLE `pdpa_email`
  ADD PRIMARY KEY (`id_email`);

--
-- Indexes for table `pdpa_log_setting_cookiepolicy`
--
ALTER TABLE `pdpa_log_setting_cookiepolicy`
  ADD PRIMARY KEY (`id_lsc`),
  ADD KEY `policy_key_idx` (`policy_id`),
  ADD KEY `cookiepolicy_key_idx` (`domaingroup_id`);

--
-- Indexes for table `pdpa_policylog`
--
ALTER TABLE `pdpa_policylog`
  ADD PRIMARY KEY (`id_policylog`),
  ADD KEY `policylog_seeting_domaingroup_idx` (`domaingroup_id`);

--
-- Indexes for table `pdpa_tag`
--
ALTER TABLE `pdpa_tag`
  ADD PRIMARY KEY (`id_tag`);

--
-- Indexes for table `performance`
--
ALTER TABLE `performance`
  ADD PRIMARY KEY (`performance_id`);

--
-- Indexes for table `pw_change`
--
ALTER TABLE `pw_change`
  ADD PRIMARY KEY (`change_id`),
  ADD KEY `acc_id` (`acc_id`);

--
-- Indexes for table `qrcode`
--
ALTER TABLE `qrcode`
  ADD PRIMARY KEY (`qr_id`),
  ADD KEY `acc_id` (`acc_id`);

--
-- Indexes for table `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`quest_id`);

--
-- Indexes for table `readed`
--
ALTER TABLE `readed`
  ADD PRIMARY KEY (`readed_id`),
  ADD KEY `readed_ibfk_1` (`file_id`);

--
-- Indexes for table `set_network`
--
ALTER TABLE `set_network`
  ADD PRIMARY KEY (`nw_id`);

--
-- Indexes for table `set_system`
--
ALTER TABLE `set_system`
  ADD PRIMARY KEY (`sys_id`);

--
-- Indexes for table `styles`
--
ALTER TABLE `styles`
  ADD PRIMARY KEY (`id_st`);

--
-- Indexes for table `timezone`
--
ALTER TABLE `timezone`
  ADD PRIMARY KEY (`time_id`);

--
-- Indexes for table `traffic`
--
ALTER TABLE `traffic`
  ADD PRIMARY KEY (`traffic_id`);

--
-- Indexes for table `undevice`
--
ALTER TABLE `undevice`
  ADD PRIMARY KEY (`undevice_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_u`),
  ADD KEY `login_id_key_idx` (`login_id`);

--
-- Indexes for table `user_member`
--
ALTER TABLE `user_member`
  ADD PRIMARY KEY (`um_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `acc_id` (`acc_id`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`views_id`),
  ADD KEY `device_id` (`device_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `alert`
--
ALTER TABLE `alert`
  MODIFY `alert_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=555;

--
-- AUTO_INCREMENT for table `blocked`
--
ALTER TABLE `blocked`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `checktime`
--
ALTER TABLE `checktime`
  MODIFY `check_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `cookiepolicy`
--
ALTER TABLE `cookiepolicy`
  MODIFY `id_cp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `data_out`
--
ALTER TABLE `data_out`
  MODIFY `out_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `del_acc`
--
ALTER TABLE `del_acc`
  MODIFY `del_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `del_device`
--
ALTER TABLE `del_device`
  MODIFY `del_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `device_member`
--
ALTER TABLE `device_member`
  MODIFY `dm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `dialog`
--
ALTER TABLE `dialog`
  MODIFY `id_dl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `doc_pdpa_chat`
--
ALTER TABLE `doc_pdpa_chat`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `doc_pdpa_classification`
--
ALTER TABLE `doc_pdpa_classification`
  MODIFY `classify_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `doc_pdpa_classification_special_conditions`
--
ALTER TABLE `doc_pdpa_classification_special_conditions`
  MODIFY `classification_special_conditions_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `doc_pdpa_data`
--
ALTER TABLE `doc_pdpa_data`
  MODIFY `data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `doc_pdpa_database_check`
--
ALTER TABLE `doc_pdpa_database_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `doc_pdpa_data_type`
--
ALTER TABLE `doc_pdpa_data_type`
  MODIFY `data_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `doc_pdpa_document`
--
ALTER TABLE `doc_pdpa_document`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=300;

--
-- AUTO_INCREMENT for table `doc_pdpa_document_log`
--
ALTER TABLE `doc_pdpa_document_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=849;

--
-- AUTO_INCREMENT for table `doc_pdpa_document_page`
--
ALTER TABLE `doc_pdpa_document_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=551;

--
-- AUTO_INCREMENT for table `doc_pdpa_document_type`
--
ALTER TABLE `doc_pdpa_document_type`
  MODIFY `doc_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;

--
-- AUTO_INCREMENT for table `doc_pdpa_event_process`
--
ALTER TABLE `doc_pdpa_event_process`
  MODIFY `event_process_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `doc_pdpa_file_dir`
--
ALTER TABLE `doc_pdpa_file_dir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2178;

--
-- AUTO_INCREMENT for table `doc_pdpa_level`
--
ALTER TABLE `doc_pdpa_level`
  MODIFY `level_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `doc_pdpa_log0_hash`
--
ALTER TABLE `doc_pdpa_log0_hash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=449;

--
-- AUTO_INCREMENT for table `doc_pdpa_pattern`
--
ALTER TABLE `doc_pdpa_pattern`
  MODIFY `pattern_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `doc_pdpa_pattern_processing_base`
--
ALTER TABLE `doc_pdpa_pattern_processing_base`
  MODIFY `pattern_processing_base_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `doc_pdpa_position`
--
ALTER TABLE `doc_pdpa_position`
  MODIFY `position_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `doc_pdpa_tag`
--
ALTER TABLE `doc_pdpa_tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `doc_pdpa_user_type`
--
ALTER TABLE `doc_pdpa_user_type`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `doc_pdpa_words`
--
ALTER TABLE `doc_pdpa_words`
  MODIFY `words_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `domaingroup`
--
ALTER TABLE `domaingroup`
  MODIFY `id_dg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=308;

--
-- AUTO_INCREMENT for table `domain_setting_cookiepolicy`
--
ALTER TABLE `domain_setting_cookiepolicy`
  MODIFY `id_dsc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1424;

--
-- AUTO_INCREMENT for table `domain_setting_tag`
--
ALTER TABLE `domain_setting_tag`
  MODIFY `id_dst` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=372;

--
-- AUTO_INCREMENT for table `domain_style_policy`
--
ALTER TABLE `domain_style_policy`
  MODIFY `id_dsp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `exporthistory`
--
ALTER TABLE `exporthistory`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=571;

--
-- AUTO_INCREMENT for table `ftp`
--
ALTER TABLE `ftp`
  MODIFY `ftp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `ht_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1081;

--
-- AUTO_INCREMENT for table `input`
--
ALTER TABLE `input`
  MODIFY `input_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=556992;

--
-- AUTO_INCREMENT for table `log_history`
--
ALTER TABLE `log_history`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=675;

--
-- AUTO_INCREMENT for table `muitifactor`
--
ALTER TABLE `muitifactor`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pdpa_appeal`
--
ALTER TABLE `pdpa_appeal`
  MODIFY `id_ap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `pdpa_email`
--
ALTER TABLE `pdpa_email`
  MODIFY `id_email` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=403;

--
-- AUTO_INCREMENT for table `pdpa_log_setting_cookiepolicy`
--
ALTER TABLE `pdpa_log_setting_cookiepolicy`
  MODIFY `id_lsc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pdpa_policylog`
--
ALTER TABLE `pdpa_policylog`
  MODIFY `id_policylog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=551;

--
-- AUTO_INCREMENT for table `pdpa_tag`
--
ALTER TABLE `pdpa_tag`
  MODIFY `id_tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `performance`
--
ALTER TABLE `performance`
  MODIFY `performance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pw_change`
--
ALTER TABLE `pw_change`
  MODIFY `change_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `qrcode`
--
ALTER TABLE `qrcode`
  MODIFY `qr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `quest_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `readed`
--
ALTER TABLE `readed`
  MODIFY `readed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=382;

--
-- AUTO_INCREMENT for table `set_network`
--
ALTER TABLE `set_network`
  MODIFY `nw_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `set_system`
--
ALTER TABLE `set_system`
  MODIFY `sys_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT for table `styles`
--
ALTER TABLE `styles`
  MODIFY `id_st` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT for table `timezone`
--
ALTER TABLE `timezone`
  MODIFY `time_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=612;

--
-- AUTO_INCREMENT for table `traffic`
--
ALTER TABLE `traffic`
  MODIFY `traffic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `undevice`
--
ALTER TABLE `undevice`
  MODIFY `undevice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_u` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_member`
--
ALTER TABLE `user_member`
  MODIFY `um_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `views_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145548;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cookiepolicy`
--
ALTER TABLE `cookiepolicy`
  ADD CONSTRAINT `cookiepolicy_key_domaingroup` FOREIGN KEY (`domaingroup_id`) REFERENCES `domaingroup` (`id_dg`);

--
-- Constraints for table `del_device`
--
ALTER TABLE `del_device`
  ADD CONSTRAINT `del_device_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`device_id`);

--
-- Constraints for table `doc_pdpa_chat`
--
ALTER TABLE `doc_pdpa_chat`
  ADD CONSTRAINT `doc_pdpa_chat1` FOREIGN KEY (`ref_id`) REFERENCES `doc_pdpa_document` (`doc_id`),
  ADD CONSTRAINT `doc_pdpa_chat2` FOREIGN KEY (`user_id`) REFERENCES `doc_pdpa_user` (`user_id`),
  ADD CONSTRAINT `doc_pdpa_chat3` FOREIGN KEY (`user_id`) REFERENCES `doc_pdpa_user` (`user_id`);

--
-- Constraints for table `doc_pdpa_classification`
--
ALTER TABLE `doc_pdpa_classification`
  ADD CONSTRAINT `doc_pdpa_classification_ibfk_1` FOREIGN KEY (`pattern_id`) REFERENCES `doc_pdpa_pattern` (`pattern_id`),
  ADD CONSTRAINT `doc_pdpa_classification_ibfk_2` FOREIGN KEY (`event_process_id`) REFERENCES `doc_pdpa_event_process` (`event_process_id`),
  ADD CONSTRAINT `doc_pdpa_classification_ibfk_3` FOREIGN KEY (`pattern_processing_base_id`) REFERENCES `doc_pdpa_pattern_processing_base` (`pattern_processing_base_id`),
  ADD CONSTRAINT `doc_pdpa_classification_ibfk_4` FOREIGN KEY (`classification_special_conditions_id`) REFERENCES `doc_pdpa_classification_special_conditions` (`classification_special_conditions_id`),
  ADD CONSTRAINT `doc_pdpa_classification_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `doc_pdpa_user` (`user_id`);

--
-- Constraints for table `doc_pdpa_data`
--
ALTER TABLE `doc_pdpa_data`
  ADD CONSTRAINT `doc_pdpa_data` FOREIGN KEY (`data_type_id`) REFERENCES `doc_pdpa_data_type` (`data_type_id`),
  ADD CONSTRAINT `doc_pdpa_data1` FOREIGN KEY (`data_level_id`) REFERENCES `doc_pdpa_level` (`level_id`);

--
-- Constraints for table `doc_pdpa_document`
--
ALTER TABLE `doc_pdpa_document`
  ADD CONSTRAINT `doc_pdpa_document1` FOREIGN KEY (`user_id`) REFERENCES `doc_pdpa_user` (`user_id`),
  ADD CONSTRAINT `doc_pdpa_document2` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_pdpa_document_type` (`doc_type_id`);

--
-- Constraints for table `doc_pdpa_document_log`
--
ALTER TABLE `doc_pdpa_document_log`
  ADD CONSTRAINT `doc_pdpa_document_log1` FOREIGN KEY (`user_id`) REFERENCES `doc_pdpa_user` (`user_id`),
  ADD CONSTRAINT `doc_pdpa_document_log2` FOREIGN KEY (`user_id_dest`) REFERENCES `doc_pdpa_user` (`user_id`),
  ADD CONSTRAINT `doc_pdpa_document_log_ibfk_1` FOREIGN KEY (`doc_id`) REFERENCES `doc_pdpa_document` (`doc_id`);

--
-- Constraints for table `doc_pdpa_document_page`
--
ALTER TABLE `doc_pdpa_document_page`
  ADD CONSTRAINT `doc_pdpa_document_page_ibfk_1` FOREIGN KEY (`doc_id`) REFERENCES `doc_pdpa_document` (`doc_id`);

--
-- Constraints for table `doc_pdpa_pattern`
--
ALTER TABLE `doc_pdpa_pattern`
  ADD CONSTRAINT `doc_pdpa_pattern_ibfk_1` FOREIGN KEY (`pattern_processing_base_id`) REFERENCES `doc_pdpa_pattern_processing_base` (`pattern_processing_base_id`),
  ADD CONSTRAINT `doc_pdpa_pattern_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `doc_pdpa_user` (`user_id`),
  ADD CONSTRAINT `doc_pdpa_pattern_ibfk_3` FOREIGN KEY (`doc_id`) REFERENCES `doc_pdpa_document` (`doc_id`);

--
-- Constraints for table `doc_pdpa_user`
--
ALTER TABLE `doc_pdpa_user`
  ADD CONSTRAINT `doc_pdpa_user_ibfk_1` FOREIGN KEY (`user_type_id`) REFERENCES `doc_pdpa_user_type` (`user_type_id`),
  ADD CONSTRAINT `doc_pdpa_user_ibfk_2` FOREIGN KEY (`position_id`) REFERENCES `doc_pdpa_position` (`position_id`);

--
-- Constraints for table `domain_setting_cookiepolicy`
--
ALTER TABLE `domain_setting_cookiepolicy`
  ADD CONSTRAINT `setting_cookiepolicy_key` FOREIGN KEY (`cookiepolicy_id`) REFERENCES `cookiepolicy` (`id_cp`),
  ADD CONSTRAINT `setting_domain_key` FOREIGN KEY (`domain_id`) REFERENCES `domaingroup` (`id_dg`),
  ADD CONSTRAINT `setting_policylog_key` FOREIGN KEY (`policylog_id`) REFERENCES `pdpa_policylog` (`id_policylog`);

--
-- Constraints for table `domain_setting_tag`
--
ALTER TABLE `domain_setting_tag`
  ADD CONSTRAINT `setting_tag_domain` FOREIGN KEY (`domaingroup_id`) REFERENCES `domaingroup` (`id_dg`),
  ADD CONSTRAINT `setting_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `pdpa_tag` (`id_tag`);

--
-- Constraints for table `domain_style_policy`
--
ALTER TABLE `domain_style_policy`
  ADD CONSTRAINT `center_dialog_key` FOREIGN KEY (`dialog_id`) REFERENCES `dialog` (`id_dl`),
  ADD CONSTRAINT `center_domaingroup_key` FOREIGN KEY (`domaingroup_id`) REFERENCES `domaingroup` (`id_dg`),
  ADD CONSTRAINT `center_style_key` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id_st`);

--
-- Constraints for table `log_history`
--
ALTER TABLE `log_history`
  ADD CONSTRAINT `log_domain_key` FOREIGN KEY (`domain_id`) REFERENCES `domaingroup` (`id_dg`),
  ADD CONSTRAINT `log_user_key` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_u`);

--
-- Constraints for table `pdpa_log_setting_cookiepolicy`
--
ALTER TABLE `pdpa_log_setting_cookiepolicy`
  ADD CONSTRAINT `domaingroup_key` FOREIGN KEY (`domaingroup_id`) REFERENCES `domaingroup` (`id_dg`),
  ADD CONSTRAINT `policylog_key` FOREIGN KEY (`policy_id`) REFERENCES `pdpa_policylog` (`id_policylog`);

--
-- Constraints for table `pdpa_policylog`
--
ALTER TABLE `pdpa_policylog`
  ADD CONSTRAINT `policylog_seeting_domaingroup` FOREIGN KEY (`domaingroup_id`) REFERENCES `domaingroup` (`id_dg`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `login_id_user` FOREIGN KEY (`login_id`) REFERENCES `login` (`id_lo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
